# Amperfii

## About the Project

We create Amperfii for managing Opportuntity, Expert, Task. The motivation behind it is [here](https://www.amperfii.com/).

The Project was setup by [Create React App](https://github.com/facebook/create-react-app).

## How to Run and Build the Project:

### 1.Run dev environment

```sh
  yarn start
```

### 2.Build the project

```sh
  yarn build
```
