import * as Yup from 'yup';

declare module 'yup' {
  interface ArraySchema<T> {
    unique(mapper: (a: T) => T, message?: any): ArraySchema<T>;
  }
}
