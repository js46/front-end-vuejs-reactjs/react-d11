import axios from 'axios';
import httpStatus from './httpStatus';
// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = "http://45.77.170.201:7999/api";
// axios.defaults.headers.common["Authorization"] = window.localStorage.getItem("jwt");
// axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
const CancelToken = axios.CancelToken;
const source = CancelToken.source();
let config = {
  baseURL: process.env.REACT_APP_API_URL,
  validateStatus: (status: number) => {
    return (
      (status >= httpStatus.StatusOK &&
        status < httpStatus.StatusMultipleChoices) ||
      status === httpStatus.StatusBadRequest
    );
  },
  cancelToken: source.token,
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true // Check cross-site Access-Control
};
const _axios = axios.create(config);
_axios.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (window.localStorage.getItem('jwt'))
      config.headers.Authorization = window.localStorage.getItem('jwt');
    return config;
  },
  error => {
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
_axios.interceptors.response.use(
  response => {
    // Do something with response data
    return response;
  },
  error => {
    // Do something with response error
    return Promise.reject(error.response);
  },
);

export default _axios;
