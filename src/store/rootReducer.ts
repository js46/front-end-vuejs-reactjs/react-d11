import { combineReducers } from 'redux';
import { configReducer } from './config/reducers';
import { authReducer } from './auth/reducers';
import {
  userMenuReducer,
  userPermissionReducer,
  userProfileReducer,
} from './user/reducers';
import { opportunityReducer } from './opportunity/reducers';
import { commonReducer } from './common/reducers';
import { playbookReducer } from './playbook/reducers';

const rootReducer = combineReducers({
  auth: authReducer,
  config: configReducer,
  common: commonReducer,
  opportunity: opportunityReducer,
  playbook: playbookReducer,
  userMenu: userMenuReducer,
  userPermission: userPermissionReducer,
  userProfile: userProfileReducer,
});
export default rootReducer;
