import { createAction } from 'redux-actions';
import { AuthActionTypes } from './types';

export const RequestUserLogin = createAction(
  AuthActionTypes.REQUEST_LOGIN_USER,
);
export const RequestUserLoginGoogle = createAction<string>(
  AuthActionTypes.REQUEST_LOGIN_USER_GOOGLE,
);
export const RequestUserLoginOkta = createAction<string>(
  AuthActionTypes.REQUEST_LOGIN_USER_OKTA,
);
export const UserLoginRequestError = createAction(
  AuthActionTypes.LOGIN_USER_REQUEST_ERROR,
);

export const UserLoginRequestSuccess = createAction(
  AuthActionTypes.LOGIN_USER_REQUEST_SUCCESS,
);
export const UserLogout = createAction(AuthActionTypes.REQUEST_USER_LOGOUT);
