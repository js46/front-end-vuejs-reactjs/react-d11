import { handleActions } from 'redux-actions';
import { produce } from 'immer';
import { AuthActionTypes } from './types';

const initData: TAuthState = {
  is_loading: false,
  logged_in: false,
  user: null,
  err: null,
};
export interface TAuthState {
  is_loading: boolean;
  logged_in: boolean;
  user: any;
  err: any;
}
export const authReducer = handleActions<TAuthState, any>(
  {
    [AuthActionTypes.REQUEST_LOGIN_USER_OKTA]: state =>
      produce(state, draft => {
        draft.is_loading = true;
        draft.err = null;
      }),
    [AuthActionTypes.REQUEST_LOGIN_USER_GOOGLE]: state =>
      produce(state, draft => {
        draft.is_loading = true;
        draft.err = null;
      }),
    [AuthActionTypes.REQUEST_LOGIN_USER]: state =>
      produce(state, draft => {
        draft.is_loading = true;
        draft.err = null;
      }),
    [AuthActionTypes.LOGIN_USER_REQUEST_SUCCESS]: state =>
      produce(state, draft => {
        draft.logged_in = true;
        draft.is_loading = false;
      }),
    [AuthActionTypes.LOGIN_USER_REQUEST_ERROR]: (state, action) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.logged_in = false;
        draft.err = action.payload;
      }),
    [AuthActionTypes.USER_LOGOUT]: state =>
      produce(state, draft => {
        draft.logged_in = false;
        draft.is_loading = false;
        draft.err = null;
      }),
  },
  initData,
);
