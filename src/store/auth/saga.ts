import { put, call, takeLatest, takeEvery } from 'redux-saga/effects';
import { UserLoginRequestSuccess, UserLoginRequestError } from './actions';
import { AuthActionTypes } from './types';
import history from '../history';
import {
  oktaAuthentication,
  googleAuth,
  loginAuth,
  IParamLogin,
  logOut,
} from '../../services/auth.services';

export const authSagas = [
  takeLatest(AuthActionTypes.REQUEST_LOGIN_USER_OKTA, loginOkta),
  takeLatest(AuthActionTypes.REQUEST_LOGIN_USER_GOOGLE, loginGoogle),
  takeLatest(AuthActionTypes.REQUEST_LOGIN_USER, loginNormal),
  takeEvery(
    AuthActionTypes.LOGIN_USER_REQUEST_SUCCESS,
    processingUserLoginSuccess,
  ),
  takeEvery(AuthActionTypes.REQUEST_USER_LOGOUT, doLogOut),
];

interface IAction {
  type: string;
  payload: IParamLogin | string;
}
function* loginGoogle(action: IAction) {
  try {
    const payload = yield call(
      async () => await googleAuth(action.payload as string),
    );

    if (payload.data && payload.success) {
      yield put(UserLoginRequestSuccess(payload.data.token));
    }
    yield put(UserLoginRequestError('Password or email is invaild'));
  } catch (error) {
    yield call(forwardTo, '/');
    return yield put(UserLoginRequestError(error.data.message));
  }
}
function* loginNormal(action: IAction) {
  const payload = yield call(
    async () => await loginAuth(action.payload as IParamLogin),
  );
  if (payload.data && payload.success) {
    const token = payload.data.token;
    yield put(UserLoginRequestSuccess(token));
  } else {
    yield put(UserLoginRequestError(payload.message));
  }
}
function* loginOkta(action: IAction) {
  try {
    const payload = yield call(
      async () => await oktaAuthentication(action.payload as string),
    );
    if (payload.data && payload.success) {
      const token = payload.data.token;
      yield put(UserLoginRequestSuccess(token));
    }
  } catch (error) {
    return yield put(UserLoginRequestError(error.data.message));
  }
}
interface IActionLoginSuccess {
  type: string;
  payload: string;
}
function* processingUserLoginSuccess(action: IActionLoginSuccess) {
  window.localStorage.setItem('jwt', action.payload);
  yield call(forwardTo, '/');
}
function forwardTo(location: string) {
  history.push(location);
}

function* doLogOut() {
  try {
    yield call(async () => await logOut());
    window.localStorage.removeItem('jwt');
    yield put({ type: AuthActionTypes.USER_LOGOUT });
  } catch (e) {
    console.log('Error logging out', e);
  }
}
