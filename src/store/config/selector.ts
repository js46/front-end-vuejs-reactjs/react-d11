import { TRootState } from '../index';
import { TPrivateConfig, TConfigState } from './actions';
import { ES_INDEX_KEYS, PUBLIC_CONFIG } from '../../config';

export const ConfigFullState = (state: TRootState): TConfigState =>
  state.config;
export const ConfigESIndex = (state: TRootState): ES_INDEX_KEYS =>
  state.config.private.es_indexs;
export const ConfigPubic = (state: TRootState): PUBLIC_CONFIG =>
  state.config.public;
export const ConfigPrivate = (state: TRootState): TPrivateConfig =>
  state.config.private;
