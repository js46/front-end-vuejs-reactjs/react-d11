import { createAction } from 'redux-actions';

import { ES_INDEX_KEYS, GOOGLE_CONFIG, PUBLIC_CONFIG } from '../../config';
import { ConfigActionTypes } from './types';

export type ConfigPayload = TPrivateConfig | PUBLIC_CONFIG | string;
export interface TPrivateConfig {
  es_indexs: ES_INDEX_KEYS;
  google?: GOOGLE_CONFIG;
  google_app_id: string;
  tinymce_api_key: string;
}
export interface TConfigState {
  private: TPrivateConfig;
  is_loading: boolean;
  is_fetch_private: boolean;
  is_fetch_public: boolean;
  public: PUBLIC_CONFIG;
}
export const initialConfig: TConfigState = {
  private: {
    es_indexs: {
      opportunity_index_name: '',
      event_log_index_name_prefix: '',
      comment_index_name: '',
      favorite_index_name: '',
      attachment_index_name: '',
      user_profile_index_name: '',
      opportunity_task_index_name: '',
      opportunity_expert_index_name: '',
      user_note_index_name: '',
      opportunity_index_finding: '',
      link_index_name: '',
      list_index_name: '',
      tag_index_name: '',
      issue_index_name: '',
    },
    google: undefined,
    google_app_id: '',
    tinymce_api_key: '',
  },
  public: {
    google_client_id: '',
    okta_client_id: '',
    okta_login_url: '',
    okta_redirect_uri: '',
    default_login: 'account',
  },
  is_fetch_private: false,
  is_fetch_public: false,
  is_loading: false,
};

export const RequestConfigPublic = createAction(
  ConfigActionTypes.REQUEST_CONFIG_PUBLIC,
);
export const ConfigPublicRequestError = createAction(
  ConfigActionTypes.CONFIG_PUBLIC_REQUEST_ERROR,
);

export const ConfigPublicRequestSuccess = createAction<PUBLIC_CONFIG>(
  ConfigActionTypes.CONFIG_PUBLIC_REQUEST_SUCCESS,
);

export const RequestConfigPrivate = createAction(
  ConfigActionTypes.REQUEST_CONFIG_PRIVATE,
);
export const ConfigPrivateRequestError = createAction(
  ConfigActionTypes.CONFIG_PRIVATE_REQUEST_ERROR,
);

export const ConfigPrivateRequestSuccess = createAction<TPrivateConfig>(
  ConfigActionTypes.CONFIG_PRIVATE_REQUEST_SUCCESS,
);
