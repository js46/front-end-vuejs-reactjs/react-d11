import { put, call, takeLatest, select } from 'redux-saga/effects';
import {
  ConfigPrivateRequestError,
  ConfigPrivateRequestSuccess,
  ConfigPublicRequestError,
  ConfigPublicRequestSuccess,
} from './actions';
import { ConfigActionTypes } from './types';
import { ConfigFullState } from './selector';
import {
  getConfigEnv as getConfigPrivate,
  getConfigPublic,
} from '../../services/config.services';
export const configSagas = [
  takeLatest(ConfigActionTypes.REQUEST_CONFIG_PUBLIC, fetchPublicConfig),
  takeLatest(ConfigActionTypes.REQUEST_CONFIG_PRIVATE, fetchPrivateConfig),
];
function* fetchPublicConfig() {
  try {
    const payload = yield call(async () => await getConfigPublic());

    yield put(ConfigPublicRequestSuccess(payload.data));
  } catch (error) {
    return yield put(ConfigPublicRequestError(error));
  }
}
function* fetchPrivateConfig() {
  try {
    const oldConfig = yield select(ConfigFullState);
    if (oldConfig.is_fetch_private) return;
    const payload = yield call(async () => await getConfigPrivate());
    yield put(ConfigPrivateRequestSuccess(payload.data));
  } catch (error) {
    return yield put(ConfigPrivateRequestError(error));
  }
}
