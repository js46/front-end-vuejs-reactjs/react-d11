import { handleActions } from 'redux-actions';
import { produce } from 'immer';
import { ConfigActionTypes } from './types';
import {
  ConfigPayload,
  initialConfig,
  TConfigState,
  TPrivateConfig,
} from './actions';
import { PUBLIC_CONFIG } from '../../config';

export const configReducer = handleActions<TConfigState, ConfigPayload>(
  {
    [ConfigActionTypes.REQUEST_CONFIG_PUBLIC]: state =>
      produce(state, draft => {
        if (!draft.is_fetch_public) {
          draft.is_loading = false;
        }
      }),
    [ConfigActionTypes.CONFIG_PUBLIC_REQUEST_ERROR]: state =>
      produce(state, draft => {
        draft.is_loading = false;
      }),
    [ConfigActionTypes.CONFIG_PUBLIC_REQUEST_SUCCESS]: (state, action) => ({
      ...state,
      is_loading: false,
      is_fetch_public: true,
      public: action.payload as PUBLIC_CONFIG,
    }),

    [ConfigActionTypes.REQUEST_CONFIG_PRIVATE]: state =>
      produce(state, draft => {
        draft.is_loading = false;
      }),
    [ConfigActionTypes.CONFIG_PRIVATE_REQUEST_ERROR]: state =>
      produce(state, draft => {
        draft.is_loading = false;
      }),
    [ConfigActionTypes.CONFIG_PRIVATE_REQUEST_SUCCESS]: (state, action) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.is_fetch_private = true;
        draft.private = action.payload as TPrivateConfig;
      }),
    /*  ({
      ...state,
      is_loading: false,
      private: action.payload as TPrivateConfig,
    }), */
  },
  initialConfig,
);
