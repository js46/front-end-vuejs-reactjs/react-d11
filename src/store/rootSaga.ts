import { all } from 'redux-saga/effects';
import { menuSagas } from './user/saga';
import { configSagas } from './config/saga';
import { authSagas } from './auth/saga';
import { opportunitySagas } from './opportunity/saga';
import { playbookSagas } from './playbook/saga';
export default function* rootSaga() {
  return yield all([
    ...menuSagas,
    ...configSagas,
    ...authSagas,
    ...opportunitySagas,
    ...playbookSagas,
  ]);
}
