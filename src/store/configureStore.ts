import { applyMiddleware, createStore, Middleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { enableMapSet } from 'immer';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './rootSaga';
import rootReducer from './rootReducer';
import { AuthActionTypes } from './auth/types';

const configStorage = {
  key: 'root',
  storage: storage,
  whitelist: ['auth', 'userProfile', 'config', 'userMenu'],
  debug: false, //to get useful logging
};

const appReducer = (state: any, action: any) => {
  if (action.type === AuthActionTypes.USER_LOGOUT) {
    storage.removeItem('persist:root');
    state = undefined;
  }
  return rootReducer(state, action);
};

export type TRootState = ReturnType<typeof rootReducer>;

const loggerMiddleware: Middleware<
  {},
  TRootState
> = store => next => action => {
  if (process.env.NODE_ENV !== 'production') {
    console.log('store', store.getState());
    console.log('action', action);
  }
  next(action);
};

const configureStore = () => {
  enableMapSet();
  const sagaMiddleware = createSagaMiddleware();
  const middlewareEnhancer = applyMiddleware(sagaMiddleware, loggerMiddleware);
  const reducers = persistReducer(configStorage, appReducer);
  const store = createStore(reducers, undefined, middlewareEnhancer);
  sagaMiddleware.run(rootSaga);
  const persistor = persistStore(store);
  return { store, persistor };
};
export const { store, persistor } = configureStore();
