import { createAction } from 'redux-actions';
import { OpportunityActionTypes } from './types';
import { IOpportunity } from '../../services/opportunity.services';

export const RequestDetailOpportunity = createAction<number>(
  OpportunityActionTypes.REQUEST_OPPORTUNITY_DETAIL,
);

export const OpportunityDetailRequestError = createAction(
  OpportunityActionTypes.OPPORTUNITY_DETAIL_REQUEST_ERROR,
);

export const OpportunityDetailRequestSuccess = createAction(
  OpportunityActionTypes.OPPORTUNITY_DETAIL_REQUEST_SUCCESS,
);
export const RequestOpportunityPermission = createAction<number>(
  OpportunityActionTypes.REQUEST_OPPORTUNITY_PERMISSION,
);

export const OpportunityPermissionRequestError = createAction(
  OpportunityActionTypes.OPPORTUNITY_DETAIL_REQUEST_ERROR,
);

export const OpportunityPermissionRequestSuccess = createAction(
  OpportunityActionTypes.OPPORTUNITY_PERMISSION_REQUEST_SUCCESS,
);
export const UpdatedOpportunityDetail = createAction<IOpportunity>(
  OpportunityActionTypes.UPDATED_OPPORTUNITY_DETAIL,
);
