import { TRootState } from '../index';
import { TOpportunityActions } from './reducers';
import {
  IOpportunity,
  IOpportunityPermission,
  IOpportunityInRole,
} from '../../services/opportunity.services';
import { OpportunityAction } from '../../services/user.services';

export const OpportunityState = (state: TRootState): IOpportunity | undefined =>
  state.opportunity.detail;

export const OpportunityPermissionState = (
  state: TRootState,
): IOpportunityPermission[] => state.opportunity.opportunity_permission;

export const OpportunityMapActionState = (
  state: TRootState,
): TOpportunityActions => state.opportunity.list_actions;
export const OpportunityPhaseState = (state: TRootState): string =>
  state.opportunity.detail?.opportunity_phase?.name.toLocaleLowerCase() ?? '';

export const OpportunityPossibleActions = (
  state: TRootState,
): OpportunityAction[] => state.opportunity.possible_actions;

export const AnalysePermissionState = (state: TRootState): string[] =>
  state.opportunity.opportunity_permission
    .find(item => item.phase === 'analyse')
    ?.permissions?.map(pm => pm.casbin_name) ?? [];
export const CurrentExpertsState = (state: TRootState) =>
  state.opportunity.detail?.current_experts;

export const IsAdminOpportunityState = (state: TRootState): boolean =>
  !!state.opportunity.list_actions
    ?.get(`${state.opportunity.detail?.opportunity_phase?.name}_all_state`)
    ?.find(adminRole => {
      return (
        adminRole.casbin_name ===
        `role:${state.opportunity.detail?.opportunity_phase?.name}_admin`
      );
    });

export const ResourceRequirementState = (
  state: TRootState,
): IOpportunityInRole[] => state.opportunity.detail?.opportunity_roles ?? [];

export const idOpportunityState = (state: TRootState): number =>
  state.opportunity.detail?.id ?? 0;
export const completionDateState = (state: TRootState) =>
  state.opportunity.detail?.completion_date;
export const AddExpertToTeamState = (state: TRootState) =>
  !!state.opportunity.list_actions
    ?.get(`${state.opportunity.detail?.opportunity_phase?.name}_all_state`)
    ?.find(userAddExpertToTeam => {
      return (
        userAddExpertToTeam.casbin_name ===
        `role:${state.opportunity.detail?.opportunity_phase?.name}_add_experts_to_team`
      );
    });
