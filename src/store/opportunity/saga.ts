import { all, call, put, takeLatest } from 'redux-saga/effects';
import { OpportunityActionTypes } from './types';
import {
  OpportunityDetailRequestSuccess,
  OpportunityDetailRequestError,
  OpportunityPermissionRequestSuccess,
  OpportunityPermissionRequestError,
} from './actions';
import {
  getOpportunityPermissions,
  getAnOpportunity,
} from '../../services/opportunity.services';

export const opportunitySagas = [
  takeLatest(
    OpportunityActionTypes.REQUEST_OPPORTUNITY_DETAIL,
    getOpportunityDetail,
  ),
  takeLatest(
    OpportunityActionTypes.REQUEST_OPPORTUNITY_PERMISSION,
    getPermissionOpp,
  ),
];
interface IActionOpp {
  type: string;
  payload: number;
}
function* getOpportunityDetail(action: IActionOpp) {
  try {
    const id = Number(action.payload);
    const { opportunityRes, permissionRes } = yield all({
      opportunityRes: call(async () => await getAnOpportunity(id)),
      permissionRes: call(async () => await getOpportunityPermissions(id)),
    });

    if (opportunityRes.success && permissionRes.success) {
      yield put(
        OpportunityDetailRequestSuccess({
          opportunity: opportunityRes.data,
          permission: permissionRes.data,
        }),
      );
    } else {
      OpportunityDetailRequestError('No found Opp');
    }
  } catch (ex) {
    const message = ex && ex.data ? ex.data.message : 'Cannot connect server';
    OpportunityDetailRequestError(message);
  }

  // do something with results
}
function* getPermissionOpp(action: IActionOpp) {
  const id = Number(action.payload);

  const payload = yield call(async () => await getOpportunityPermissions(id));
  if (payload.success) {
    yield put(OpportunityPermissionRequestSuccess(payload.data));
  } else {
    OpportunityPermissionRequestError(payload.message);
  }
}
