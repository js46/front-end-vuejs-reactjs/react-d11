import { handleActions } from 'redux-actions';
import { produce } from 'immer';
import { OpportunityActionTypes } from './types';
import {
  IOpportunity,
  IOpportunityPermission,
} from '../../services/opportunity.services';
import { OpportunityAction } from '../../services/user.services';
const initData: TOpportunityState = {
  is_loading: false,
  opportunity_permission: [],
  possible_actions: [],
  list_actions: new Map<string, []>(),
  err: null,
};
export type TOpportunityActions = Map<string, OpportunityAction[]>;
export interface TOpportunityState {
  is_loading: boolean;
  detail?: IOpportunity;
  opportunity_permission: IOpportunityPermission[];
  possible_actions: OpportunityAction[];
  list_actions: TOpportunityActions;
  err: any;
}
export const opportunityReducer = handleActions<TOpportunityState, any>(
  {
    [OpportunityActionTypes.REQUEST_OPPORTUNITY_DETAIL]: state =>
      produce(state, draft => {
        draft.is_loading = true;
        draft.err = null;
      }),
    [OpportunityActionTypes.OPPORTUNITY_DETAIL_REQUEST_SUCCESS]: (
      state,
      action,
    ) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.opportunity_permission = action.payload.permission;
        draft.detail = { ...action.payload.opportunity };
        draft.list_actions = processedOpportunityActionMap(
          action.payload.permission,
        );
        draft.possible_actions = getPossibleActions(
          draft.list_actions,
          draft.detail?.opportunity_phase?.name,
          draft.detail?.opportunity_state?.name,
        );
      }),
    [OpportunityActionTypes.OPPORTUNITY_DETAIL_REQUEST_ERROR]: (
      state,
      action,
    ) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.err = action.payload;
      }),
    [OpportunityActionTypes.REQUEST_OPPORTUNITY_PERMISSION]: state =>
      produce(state, draft => {
        draft.is_loading = true;
        draft.err = null;
      }),
    [OpportunityActionTypes.OPPORTUNITY_PERMISSION_REQUEST_SUCCESS]: (
      state,
      action,
    ) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.opportunity_permission = action.payload;
        draft.list_actions = processedOpportunityActionMap(action.payload);
        draft.possible_actions = getPossibleActions(
          draft.list_actions,
          draft.detail?.opportunity_phase?.name,
          draft.detail?.opportunity_state?.name,
        );
      }),
    [OpportunityActionTypes.UPDATED_OPPORTUNITY_DETAIL]: (state, action) =>
      produce(state, draft => {
        draft.detail = action.payload;
        draft.possible_actions = getPossibleActions(
          draft.list_actions,
          draft.detail?.opportunity_phase?.name,
          draft.detail?.opportunity_state?.name,
        );
      }),

    [OpportunityActionTypes.OPPORTUNITY_PERMISSION_REQUEST_ERROR]: (
      state,
      action,
    ) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.err = action.payload;
      }),
  },
  initData,
);

const processedOpportunityActionMap = (
  permissions: IOpportunityPermission[],
) => {
  const opportunityActionMap = new Map<string, OpportunityAction[]>();
  const permissionFull = permissions
    .map(item => item.permissions)
    .reduce((phase, all) => [...phase, ...all], []);
  permissionFull?.forEach(action => {
    const actionKey = `${action.phase}_${action.state}`;
    if (opportunityActionMap.has(actionKey)) {
      opportunityActionMap.get(actionKey)?.push(action);
    } else {
      opportunityActionMap.set(actionKey, [action]);
    }
  });

  return opportunityActionMap;
};
const getPossibleActions = (
  permission: TOpportunityActions,
  phase?: string,
  state?: string,
) => {
  if (!phase || !state) return [];
  const keyOfOpportunity = `${phase.toLowerCase()}_${state.toLowerCase()}`;

  return permission.get(keyOfOpportunity) ?? [];
};
