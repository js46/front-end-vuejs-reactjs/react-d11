import { takeLatest, takeEvery, put, call, select } from 'redux-saga/effects';
import { PlaybookActionTypes } from './types';
import {
  getPlayBookOppDetail,
  removePlaybookItem,
  addPlaybookItem,
  TNewPlaybookItemForm,
  IUpdateState,
  updateStatePlaybookItem,
} from '../../services/playbook.services';
import {
  OppPlaybookDetailSuccess,
  OppPlaybookDetailError,
  RequestOppPlaybookDetail,
  CloseDeleteDialogPb,
  CloseAddingDialogPb,
  CloseChangeStateDialogPb,
} from './actions';
import { PlaybookDetailState, PbItemChangeStateId } from './selector';

export const playbookSagas = [
  takeEvery(
    PlaybookActionTypes.REQUEST_OPP_PLAYBOOK_DETAIL,
    getOppPlaybookDetail,
  ),
  takeLatest(
    PlaybookActionTypes.REQUEST_DELETE_PLAYBOOK_ITEM,
    deletePlaybookItem,
  ),
  takeLatest(
    PlaybookActionTypes.REQUEST_ADDING_PLAYBOOK_ITEM,
    addingPlaybookItem,
  ),
  takeLatest(
    PlaybookActionTypes.REQUEST_CHANGE_STATE_PLAYBOOK_ITEM,
    changeStatePbItem,
  ),
];
interface IActionPb {
  type: string;
  payload: number;
}
function* getOppPlaybookDetail(action: IActionPb) {
  const id = Number(action.payload);
  const payload = yield call(async () => await getPlayBookOppDetail(id));
  if (payload.success) {
    yield put(OppPlaybookDetailSuccess(payload.data));
  } else {
    OppPlaybookDetailError(payload.message);
  }
}

function* deletePlaybookItem(action: IActionPb) {
  const id = Number(action.payload);
  const payload = yield call(async () => await removePlaybookItem(id));
  if (payload.success) {
    yield call(reloadPlaybook);
  } else {
    OppPlaybookDetailError(payload.message);
  }

  yield put(CloseDeleteDialogPb());
}

interface IActionFormPb {
  type: string;
  payload: TNewPlaybookItemForm;
}
function* addingPlaybookItem(action: IActionFormPb) {
  const playbookDetail = yield select(PlaybookDetailState);
  const payload = yield call(
    async () => await addPlaybookItem(action.payload, playbookDetail.id),
  );
  if (payload.success) {
    yield put(CloseAddingDialogPb());
    yield call(reloadPlaybook);
  } else {
    OppPlaybookDetailError(payload.message);
  }
}
interface IActionFormChangeStatePbItem {
  type: string;
  payload: IUpdateState;
}
function* changeStatePbItem(action: IActionFormChangeStatePbItem) {
  const playbookItemIdSelected = yield select(PbItemChangeStateId);
  const payload = yield call(
    async () =>
      await updateStatePlaybookItem(action.payload, playbookItemIdSelected),
  );
  if (payload.success) {
    yield put(CloseChangeStateDialogPb());
    yield call(reloadPlaybook);
  } else {
    OppPlaybookDetailError(payload.message);
  }
}
function* reloadPlaybook() {
  const playbookDetail = yield select(PlaybookDetailState);
  yield put(RequestOppPlaybookDetail(playbookDetail.id));
}
