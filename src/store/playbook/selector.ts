import { TRootState } from '../index';

export const PlaybookDetailState = (state: TRootState) =>
  state.playbook.playbookDetail;
export const AddPbItemFormState = (state: TRootState) =>
  state.playbook.formAdding.data;
export const IsOpenAddingDialogPbItemState = (state: TRootState): boolean =>
  state.playbook.formAdding.open;
export const IsOpenChangeStateDialogPbItemState = (
  state: TRootState,
): boolean => state.playbook.formChangeState.open;
export const PbItemDeleteIDState = (state: TRootState) =>
  state.playbook.formDelete.itemID;
export const PbItemChangeStateId = (state: TRootState) =>
  state.playbook.formChangeState.playbook_item_id;
export const PbItemChangeStateIdState = (state: TRootState) =>
  state.playbook.formChangeState.playbook_item_state_id;
export const IsOpenDeleteDialogPbItemState = (state: TRootState): boolean =>
  state.playbook.formDelete.open;
export const PlaybookItemTaskId = (state: TRootState): number =>
  state.playbook.playbook_item_task_id;
export const PlaybookItemIssueId = (state: TRootState): number =>
  state.playbook.playbook_item_issue_id;
