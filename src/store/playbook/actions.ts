import { createAction } from 'redux-actions';
import { PlaybookActionTypes } from './types';

export const RequestOppPlaybookDetail = createAction<number>(
  PlaybookActionTypes.REQUEST_OPP_PLAYBOOK_DETAIL,
);

export const OppPlaybookDetailSuccess = createAction(
  PlaybookActionTypes.OPP_PLAYBOOK_DETAIL_SUCCESS,
);

export const OppPlaybookDetailError = createAction(
  PlaybookActionTypes.OPP_PLAYBOOK_DETAIL_ERROR,
);

export const AddPlaybookItem = createAction(
  PlaybookActionTypes.ADD_PLAYBOOK_ITEM,
);
export const RequestAddingPbItem = createAction(
  PlaybookActionTypes.REQUEST_ADDING_PLAYBOOK_ITEM,
);

export const CloseAddingDialogPb = createAction(
  PlaybookActionTypes.CLOSE_ADDING_DIALOG_PLAYBOOK_ITEM,
);
export const RequestDeletePbItem = createAction(
  PlaybookActionTypes.REQUEST_DELETE_PLAYBOOK_ITEM,
);
export const OpenDeleteDialogPb = createAction(
  PlaybookActionTypes.OPEN_DELETE_DIALOG_PLAYBOOK_ITEM,
);
export const CloseDeleteDialogPb = createAction(
  PlaybookActionTypes.CLOSE_DELETE_DIALOG_PLAYBOOK_ITEM,
);
export const ChangeStatePbItem = createAction(
  PlaybookActionTypes.CHANGE_STATE_PLAYBOOK_ITEM,
);
export const RequestChangeStatePbItem = createAction(
  PlaybookActionTypes.REQUEST_CHANGE_STATE_PLAYBOOK_ITEM,
);
export const CloseChangeStateDialogPb = createAction(
  PlaybookActionTypes.CLOSE_CHANGE_STATE_DIALOG_PLAYBOOK_ITEM,
);
export const GetPlaybookItemTaskIndex = createAction(
  PlaybookActionTypes.GET_INDEX_TASK_PLAYBOOK_ID,
);
export const GetPlaybookItemIssueIndex = createAction(
  PlaybookActionTypes.GET_INDEX_ISSUE_PLAYBOOK_ID,
);
