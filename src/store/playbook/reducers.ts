import { handleActions } from 'redux-actions';
import { TPlaybook, PlaybookActionTypes } from './types';
import { produce } from 'immer';

const initPlaybook: TPlaybook = {
  formAdding: {
    open: false,
  },
  loading: false,
  formDelete: {
    open: false,
  },
  formChangeState: {
    open: false,
  },
  playbook_item_task_id: 0,
  playbook_item_issue_id: 0,
};
export const playbookReducer = handleActions<TPlaybook, any>(
  {
    [PlaybookActionTypes.REQUEST_OPP_PLAYBOOK_DETAIL]: (state, action) =>
      produce(state, draft => {
        draft.loading = true;
      }),
    [PlaybookActionTypes.OPP_PLAYBOOK_DETAIL_SUCCESS]: (state, action) =>
      produce(state, draft => {
        draft.loading = false;
        draft.playbookDetail = action.payload;
      }),
    [PlaybookActionTypes.OPP_PLAYBOOK_DETAIL_ERROR]: (state, action) =>
      produce(state, draft => {
        draft.loading = false;
        draft.error = action.payload;
      }),
    [PlaybookActionTypes.ADD_PLAYBOOK_ITEM]: (state, action) =>
      produce(state, draft => {
        draft.formAdding.data = action.payload;
        draft.formAdding.open = true;
      }),
    [PlaybookActionTypes.CLOSE_ADDING_DIALOG_PLAYBOOK_ITEM]: state =>
      produce(state, draft => {
        draft.formAdding = { open: false };
      }),
    [PlaybookActionTypes.OPEN_DELETE_DIALOG_PLAYBOOK_ITEM]: (state, action) =>
      produce(state, draft => {
        draft.formDelete.itemID = action.payload;
        draft.formDelete.open = true;
      }),
    [PlaybookActionTypes.CLOSE_DELETE_DIALOG_PLAYBOOK_ITEM]: (state, action) =>
      produce(state, draft => {
        draft.formDelete = { open: false };
      }),
    [PlaybookActionTypes.CHANGE_STATE_PLAYBOOK_ITEM]: (state, action) =>
      produce(state, draft => {
        draft.formChangeState.playbook_item_id = action.payload;
        draft.formChangeState.open = true;
      }),
    [PlaybookActionTypes.CLOSE_CHANGE_STATE_DIALOG_PLAYBOOK_ITEM]: (
      state,
      action,
    ) =>
      produce(state, draft => {
        draft.formChangeState = { open: false };
      }),
    [PlaybookActionTypes.GET_INDEX_TASK_PLAYBOOK_ID]: (state, action) =>
      produce(state, draft => {
        draft.playbook_item_task_id = action.payload;
      }),
    [PlaybookActionTypes.GET_INDEX_ISSUE_PLAYBOOK_ID]: (state, action) =>
      produce(state, draft => {
        draft.playbook_item_issue_id = action.payload;
      }),
  },
  initPlaybook,
);
