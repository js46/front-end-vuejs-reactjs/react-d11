import { OpportunityPlaybookT } from '../../services/playbook.services';

export enum PlaybookActionTypes {
  REQUEST_OPP_PLAYBOOK_DETAIL = '@playbook/REQUEST_OPP_PLAYBOOK_DETAIL',
  OPP_PLAYBOOK_DETAIL_SUCCESS = '@playbook/OPP_PLAYBOOK_DETAIL_SUCCESS',
  OPP_PLAYBOOK_DETAIL_ERROR = '@playbook/OPP_PLAYBOOK_DETAIL_ERROR',
  ADD_PLAYBOOK_ITEM = '@playbook/ADD_PLAYBOOK_ITEM',
  REQUEST_ADDING_PLAYBOOK_ITEM = '@playbook/REQUEST_ADDING_PLAYBOOK_ITEM',
  CLOSE_ADDING_DIALOG_PLAYBOOK_ITEM = '@playbook/CLOSE_ADDING_DIALOG_PLAYBOOK_ITEM',
  OPEN_DELETE_DIALOG_PLAYBOOK_ITEM = '@playbook/OPEN_DELETE_DIALOG_PLAYBOOK_ITEM',
  CLOSE_DELETE_DIALOG_PLAYBOOK_ITEM = '@playbook/CLOSE_DELETE_DIALOG_PLAYBOOK_ITEM',
  REQUEST_DELETE_PLAYBOOK_ITEM = '@playbook/REQUEST_DELETE_PLAYBOOK_ITEM',
  CHANGE_STATE_PLAYBOOK_ITEM = '@playbook/CHANGE_STATE_PLAYBOOK_ITEM',
  REQUEST_CHANGE_STATE_PLAYBOOK_ITEM = '@playbook/REQUEST_CHANGE_STATE_PLAYBOOK_ITEM',
  CLOSE_CHANGE_STATE_DIALOG_PLAYBOOK_ITEM = '@playbook/CLOSE_CHANGE_STATE_DIALOG_PLAYBOOK_ITEM',
  GET_INDEX_TASK_PLAYBOOK_ID = '@playbook/GET_INDEX_TASK_PLAYBOOK_ID',
  GET_INDEX_ISSUE_PLAYBOOK_ID = '@playbook/GET_INDEX_ISSUE_PLAYBOOK_ID',
}
export interface TPlaybook {
  playbookDetail?: OpportunityPlaybookT;
  formDelete: {
    itemID?: number;
    open: boolean;
  };
  playbook_item_task_id: number;
  playbook_item_issue_id: number;
  loading: boolean;
  error?: any;
  formAdding: {
    open: boolean;
    data?: {
      parent: number;
      position: number[];
      item_order: number;
    };
  };
  formChangeState: {
    open: boolean;
    playbook_item_id?: number;
    playbook_item_state_id?: number;
  };
}
