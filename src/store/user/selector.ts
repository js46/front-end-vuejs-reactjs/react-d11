import { TRootState } from '../index';
import {
  TUserProfileState,
  TUserProfileFullState,
  TUserMenuFullState,
  TUserMenuState,
} from './actions';

export const UserProfileFullState = (
  state: TRootState,
): TUserProfileFullState => state.userProfile;
export const UserID = (state: TRootState): number =>
  state.userProfile.profile.id;
export const UserProfileState = (state: TRootState): TUserProfileState =>
  state.userProfile.profile;
export const IsSuperAdmin = (state: TRootState): boolean =>
  state.userProfile.profile.internal;
//super admin or admin
export const IsAdmin = (state: TRootState): boolean =>
  state.userProfile.profile.isAdmin || state.userProfile.profile.internal;
export const IsTeamlead = (state: TRootState): boolean =>
  state.userProfile.profile.team_lead_flg;
export const UserMenuFullState = (state: TRootState): TUserMenuFullState =>
  state.userMenu;
export const UserMenuState = (state: TRootState): TUserMenuState =>
  state.userMenu.menus;
