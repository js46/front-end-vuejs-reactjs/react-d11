import { createAction } from 'redux-actions';
import { UserActionTypes } from './types';
import { UserProfile } from '../../services/user.services';

//user profile Type
export type TUserProfileState = typeof initProfileState;
export type TUserProfileFullState = typeof initProfileFullState;
const initProfileState = {
  full_name: '',
  profile_name: '',
  location_id: 0,
  profile_role: {
    id: 0,
    profile_role_id: 0,
    profile_role_name: '',
    proficiency_level_id: 0,
  },
  last_updated_at: '',
  email: '',
  id: 0,
  user_name: '',
  resource_team: {
    resource_team_id: 0,
    resource_team_name: '',
  },
  business_unit: {
    business_unit_id: 0,
    business_unit_name: '',
  },
  casbin_role: ['role:user'],
  isAdmin: false,
  team_lead_flg: false,
  internal: false,
};
export const initProfileFullState = {
  profile: initProfileState,
  is_loading: false,
  is_new: false,
};
//menu permission Type

export const RequestUserProfile = createAction(
  UserActionTypes.REQUEST_USER_PROFILE,
);
export const RequestNewUserProfile = createAction(
  UserActionTypes.REQUEST_NEW_USER_PROFILE,
);

export const UserProfileRequestSuccess = createAction<UserProfile>(
  UserActionTypes.USER_PROFILE_REQUEST_SUCCESS,
);

export const UserProfileRequestError = createAction(
  UserActionTypes.USER_PROFILE_REQUEST_ERROR,
);
export interface TUserMenuFullState {
  menus: string[][];
  is_loading: boolean;
  is_fetch: boolean;
}
export type TUserMenuState = string[][];
export const initUserMenuState: TUserMenuFullState = {
  menus: [],
  is_loading: false,
  is_fetch: false,
};
export const RequestUserMenu = createAction(UserActionTypes.REQUEST_USER_MENU);
export const UserMenuRequestSuccess = createAction<UserProfile>(
  UserActionTypes.USER_MENU_REQUEST_SUCCESS,
);
export const UserMenuRequestError = createAction(
  UserActionTypes.USER_MENU_REQUEST_ERROR,
);
