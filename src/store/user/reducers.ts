import { handleActions } from 'redux-actions';
import { produce } from 'immer';
import { UserActionTypes } from './types';
import { UserProfile } from '../../services/user.services';
import {
  initUserMenuState,
  TUserMenuState,
  TUserMenuFullState,
  TUserProfileFullState,
  initProfileFullState,
} from './actions';

export const userProfileReducer = handleActions<TUserProfileFullState, any>(
  {
    [UserActionTypes.REQUEST_USER_PROFILE]: state => ({
      ...state,
      is_loading: true,
    }),
    [UserActionTypes.REQUEST_USER_PROFILE]: state =>
      produce(state, draft => {
        draft.is_loading = true;
        draft.is_new = true;
      }),
    [UserActionTypes.USER_MENU_REQUEST_ERROR]: state => ({
      ...state,
      is_loading: false,
    }),

    [UserActionTypes.USER_PROFILE_REQUEST_SUCCESS]: (state, action) =>
      produce(state, draft => {
        draft.is_loading = false;
        draft.is_new = false;
        draft.profile = action.payload as UserProfile;
        draft.profile.isAdmin = draft.profile.casbin_role?.includes(
          'role:admin',
        );
        draft.profile.team_lead_flg = draft.profile.casbin_role?.includes(
          'role:teamlead',
        );
      }),
  },
  initProfileFullState,
);
export const userMenuReducer = handleActions<
  TUserMenuFullState,
  TUserMenuState
>(
  {
    [UserActionTypes.REQUEST_USER_MENU]: state => ({
      ...state,
      is_loading: true,
    }),
    [UserActionTypes.USER_MENU_REQUEST_ERROR]: state =>
      produce(state, draft => {
        draft.is_loading = false;
      }),
    [UserActionTypes.USER_MENU_REQUEST_SUCCESS]: (state, action) => ({
      ...state,
      is_loading: false,
      is_fetch: true,
      menus: action.payload,
    }),
  },
  initUserMenuState,
);
export const userPermissionReducer = (): string[][] => [];
