import { put, call, takeLatest, select } from 'redux-saga/effects';
import {
  UserProfileRequestSuccess,
  UserProfileRequestError,
  UserMenuRequestError,
  UserMenuRequestSuccess,
} from './actions';
import { UserActionTypes } from './types';
import { UserProfileFullState, UserMenuState } from './selector';
import { getUserProfile, getPermission } from '../../services/user.services';

export const menuSagas = [
  takeLatest(UserActionTypes.REQUEST_USER_PROFILE, fetchProfileUser),
  takeLatest(UserActionTypes.REQUEST_NEW_USER_PROFILE, fetchNewProfileUser),
  takeLatest(UserActionTypes.REQUEST_USER_MENU, fetchPermissionUser),
];

function* fetchProfileUser() {
  const payloadUserProfile = yield call(async () => await getUserProfile());
  if (payloadUserProfile.success) {
    const oldUserProfileState = yield select(UserProfileFullState);
    if (
      payloadUserProfile.data.last_updated_at !==
      oldUserProfileState.profile.last_updated_at
    ) {
      yield put(UserProfileRequestSuccess(payloadUserProfile.data));
    }

    yield call(fetchPermissionUser);
  }
}

function* fetchNewProfileUser() {
  const payloadUserProfile = yield call(async () => await getUserProfile());
  if (payloadUserProfile.success) {
    yield put(UserProfileRequestSuccess(payloadUserProfile.data));
  } else {
    return yield put(UserProfileRequestError(payloadUserProfile.error));
  }
}

export function* fetchPermissionUser() {
  try {
    const oldUserMenu = yield select(UserMenuState);
    if (oldUserMenu.length) return;
    const payload = yield call(async () => await getPermission());
    const permissions = payload.data.permissions;
    //filter menu access
    const userMenus = permissions.filter((item: any) => item[3] === '1');
    yield put(UserMenuRequestSuccess(userMenus));
  } catch (error) {
    return yield put(UserMenuRequestError(error));
  }
}
