import {
  TCommon,
  CommonTypes,
  TNotification,
  NotificationTypeEnum,
} from './types';
import { createAction } from 'redux-actions';

export const initialCommon: TCommon = {
  error: {
    open: false,
    message: '',
    action: '',
  },
  notification: {
    type: NotificationTypeEnum.error,
    open: false,
    message: '',
  },
};

export const commonRequestError = createAction(
  CommonTypes.COMMON_REQUEST_ERROR,
);
export const commonCloseError = createAction(CommonTypes.COMMON_CLOSE_ERROR);

export const commonCreateNotification = createAction<TNotification>(
  CommonTypes.COMMON_CREATE_NOTIFY,
);
export const notifySuccess = createAction<any>(
  CommonTypes.COMMON_NOTIFY_SUCCESS,
);
export const notifyError = createAction<any>(CommonTypes.COMMON_NOTIFY_ERROR);

export const closeNotify = createAction(CommonTypes.COMMON_CLOSE_NOTIFY);
