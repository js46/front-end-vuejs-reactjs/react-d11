export enum CommonTypes {
  COMMON_REQUEST_ERROR = '@common/COMMON_REQUEST_ERROR',
  COMMON_CLOSE_ERROR = '@common/COMMON_CLOSE_ERROR',
  COMMON_CREATE_NOTIFY = '@common/COMMON_CREATE_NOTIFY',
  COMMON_NOTIFY_SUCCESS = '@common/COMMON_NOTIFY_SUCCESS',
  COMMON_NOTIFY_ERROR = '@common/COMMON_NOTIFY_ERROR',
  COMMON_CLOSE_NOTIFY = '@common/COMMON_CLOSE_NOTIFY',
}
export interface TCommon {
  error: {
    open: boolean;
    message: string;
    action: string;
  };
  notification: TNotification;
}
export type TNotification = {
  type: NotificationTypeEnum;
  open?: boolean;
  message: string;
  duration?: number;
};
export enum NotificationTypeEnum {
  error = 'error',
  warning = 'warning',
  info = 'info',
  success = 'success',
}
