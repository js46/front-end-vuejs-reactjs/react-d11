import { handleActions } from 'redux-actions';
import { TCommon, CommonTypes, NotificationTypeEnum } from './types';
import { produce } from 'immer';
import { initialCommon } from './actions';

export const commonReducer = handleActions<TCommon, any>(
  {
    [CommonTypes.COMMON_REQUEST_ERROR]: (state, action) =>
      produce(state, draft => {
        draft.error.open = true;
        draft.error.message = action.payload.message;
        draft.error.action = action.payload.action;
      }),
    [CommonTypes.COMMON_CLOSE_ERROR]: state =>
      produce(state, draft => {
        draft.error.open = false;
        draft.error.message = '';
        draft.error.action = '';
      }),
    [CommonTypes.COMMON_CREATE_NOTIFY]: (state, action) =>
      produce(state, draft => {
        draft.notification = { ...action.payload };
        draft.notification.open = true;
      }),
    [CommonTypes.COMMON_NOTIFY_SUCCESS]: (state, action) =>
      produce(state, draft => {
        draft.notification = { ...action.payload };
        draft.notification.open = true;
        draft.notification.type = NotificationTypeEnum.success;
      }),
    [CommonTypes.COMMON_NOTIFY_ERROR]: (state, action) =>
      produce(state, draft => {
        draft.notification = { ...action.payload };
        draft.notification.open = true;
        draft.notification.type = NotificationTypeEnum.error;
      }),
    [CommonTypes.COMMON_CLOSE_NOTIFY]: state =>
      produce(state, draft => {
        draft.notification.open = false;
        draft.notification.type = NotificationTypeEnum.error;
        delete draft.notification.duration;
      }),
  },
  initialCommon,
);
