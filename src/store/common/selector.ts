import { TRootState } from '../index';
import { TNotification } from './types';

export const ErrorState = (state: TRootState) => state.common.error;
export const NotificationState = (state: TRootState): TNotification =>
  state.common.notification;
