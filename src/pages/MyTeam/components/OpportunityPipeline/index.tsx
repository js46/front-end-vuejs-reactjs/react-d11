import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';

const OpportunityPipeline: React.FC = () => {
  const { i18n } = useTranslation();
  return (
    <Box mt={2}>
      <Box mb={2}>
        <Typography variant="h3">{i18n.t('opp_pipeline')}</Typography>
      </Box>
    </Box>
  );
};

export default OpportunityPipeline;
