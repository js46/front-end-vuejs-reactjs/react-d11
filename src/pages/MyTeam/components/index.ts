import loadable from '@loadable/component';

export * from './Sidebar';
export const Assignment = loadable(() => import('./Assignment'));
export const Calendar = loadable(() => import('./Calendar'));
export const OpportunityPipeline = loadable(() =>
  import('./OpportunityPipeline'),
);
