import React from 'react';
import {
  Drawer,
  DrawerProps,
  Grid,
  List,
  ListItem,
  Toolbar,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  ListAltOutlined,
  CalendarTodayOutlined,
  Assignment,
} from '@material-ui/icons';
import { IMyTeamList } from '../../type';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 256,
    flexShrink: 0,
  },
  paperAnchorDockedLeft: {
    borderRight: `1px solid ${theme.palette.grey[500]}`,
  },
  paper: {
    width: 256,
    backgroundColor: 'white',
    padding: '10px',
  },
  drawerPaper: {
    width: 256,
    backgroundColor: 'white',
  },
  upperButton: {
    padding: '5px 15px',
    fontWeight: 'bold',
    color: theme.palette.grey[800],
  },
  titleDialog: {
    color: theme.palette.text.primary,
  },
  dialog: {
    padding: '15px 0px',
  },
  item: {
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      background: theme.palette.grey[100],
    },
  },
  active: {
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightBold,
    background: theme.palette.primary.light,
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
}));

const sidebarList = [
  {
    name: IMyTeamList.Pipeline,
    icon: <ListAltOutlined />,
  },
  {
    name: IMyTeamList.Assign,
    icon: <Assignment />,
  },
  {
    name: IMyTeamList.Calendar,
    icon: <CalendarTodayOutlined />,
  },
];

type ISideBarProps = {
  menuSelected: string;
  setMenuSelected: React.Dispatch<React.SetStateAction<IMyTeamList>>;
} & DrawerProps;

export const MyTeamSidebar: React.FC<ISideBarProps> = ({
  menuSelected,
  setMenuSelected,
  variant,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{
        paper: classes.paper,
        paperAnchorDockedLeft: classes.paperAnchorDockedLeft,
      }}
      {...rest}
    >
      <Toolbar />
      <Grid container spacing={2}>
        <Grid item container justify="flex-start">
          <List
            component="nav"
            aria-label="opportunity sidebar"
            className={classes.drawerPaper}
          >
            {sidebarList.map((sideBar, index) => (
              <ListItem
                button
                className={
                  sideBar.name === menuSelected ? classes.active : classes.item
                }
                key={index}
                onClick={() => setMenuSelected(sideBar.name)}
              >
                <div className={classes.icon}>{sideBar.icon}</div>
                {sideBar.name}
              </ListItem>
            ))}
          </List>
        </Grid>
      </Grid>
    </Drawer>
  );
};
