import React, { useState } from 'react';
import {
  MyTeamSidebar,
  Assignment,
  Calendar,
  OpportunityPipeline,
} from './components';
import { Breadcrumb } from '../../layouts/main/components';
import { IMyTeamList } from './type';
import { SectionBox, TStringBoolean } from '../../components/styled';
import { Grid } from '@material-ui/core';

const MyTeam: React.FC = () => {
  const [menuSelected, setMenuSelected] = useState<IMyTeamList>(
    IMyTeamList.Assign,
  );
  const renderStepView = (currentStep: IMyTeamList) => {
    switch (currentStep) {
      case IMyTeamList.Assign:
        return <Assignment />;
      case IMyTeamList.Pipeline:
        return <OpportunityPipeline />;
      default:
        return <Calendar />;
    }
  };

  return (
    <Grid container>
      <MyTeamSidebar
        menuSelected={menuSelected}
        setMenuSelected={setMenuSelected}
      />
      <SectionBox
        data-has-sidebar={TStringBoolean.TRUE}
        component="section"
        style={{ paddingTop: 0, width: 'calc(100% - 256px)', marginTop: -42 }}
      >
        <Breadcrumb />
        {renderStepView(menuSelected)}
      </SectionBox>
    </Grid>
  );
};

export default MyTeam;
