import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
  MenuItem,
  Select,
  Checkbox,
} from '@material-ui/core';
import { useFormik } from 'formik';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { IconWarning } from '../../../../../../assets';
import {
  DeleteDialog,
  MixTitle,
  ConfirmDialog,
  FormDialog,
  TableAction,
} from '../../../../../../components';
import {
  IBusinessUnit,
  getBusinessUnits,
  addBusinessUnit,
  editBusinessUnit,
  deleteBusinessUnit,
  IResourceTeam,
  getResourceTeam,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';

interface Data {
  id: string;
  name: string;
  description: string;
  resource_team: string;
  business_unit_parent_id?: number;
  created_at: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'BU#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
  {
    id: 'business_unit_parent_id',
    label: 'Parent',
    disablePadding: true,
  },
  {
    id: 'resource_team',
    label: 'Resource team',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  archive: {
    width: 50,
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Archive</TableCell>
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
interface IDataForm {
  id?: number;
  name: string;
  description?: string;
  business_unit_parent_id?: number;
  resource_team_id?: number;
  archive?: boolean;
}
const BusinessUnit: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [businessUnits, setBusinessUnits] = useState<IBusinessUnit[]>([]);
  const [resourceTeams, setResourceTeams] = useState<IResourceTeam[]>([]);
  const [businessToDeleteId, setBusinessToDeleteId] = useState<number>();
  const [confirmOpen, setConfirmOpen] = useState<boolean>(false);
  const [confirmTitle, setConfirmTitle] = useState<string>('');
  const { i18n } = useTranslation();
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const handleSubmit = (item: IDataForm) => {
    if (!item.id) {
      addBusinessUnit(item).then(({ data }) => {
        setBusinessUnits([data, ...businessUnits]);
        setOpenDialog(false);
      });
    } else {
      editBusinessUnit(item.id, item).then(({ data }) => {
        let businessUnitEdit = businessUnits.filter(function(businessUnit) {
          return businessUnit.id !== data.id;
        });
        setBusinessUnits([data, ...businessUnitEdit]);
        setOpenDialog(false);
      });
    }
  };

  const initialDataForm: IDataForm = {
    name: '',
    description: '',
  };

  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      name: Yup.string().required('This field is required'),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });
  const isParentBU = (id: number | undefined) => {
    return !!businessUnits.find(
      (item: IBusinessUnit) => item.business_unit_parent_id === id,
    );
  };
  const getBusinessUnit = (id: number | undefined) => {
    if (!id) return;
    return businessUnits.find((item: IBusinessUnit) => item.id === id);
  };
  const editBusinessUnitDiaLog = (item: IBusinessUnit) => {
    if (item && formik.values.id !== item.id) {
      formik.setValues({
        id: item?.id,
        name: item.name,
        description: item.description,
        business_unit_parent_id: item.business_unit_parent_id,
        resource_team_id: item.resource_team?.id,
        archive: item.archive,
      });
    }

    setOpenDialog(true);
  };
  const handleConfirmDelete = (id: number) => {
    let item = getBusinessUnit(id);
    if (!item) return;

    if (item?.has_opportunity) {
      setConfirmTitle(i18n.t('reference_data.del_refer'));
      setConfirmOpen(true);
    } else if (isParentBU(id)) {
      setConfirmTitle(i18n.t('reference_data.del_parent'));
      setConfirmOpen(true);
    } else {
      setOpenDeleteDialog(true);
    }
  };
  const handleRemoveBusinessUnit = (id: number) => {
    deleteBusinessUnit(id).then(({ data }) => {
      let newBusinessUnit = businessUnits.filter(function(businessUnit) {
        return businessUnit.id !== data.id;
      });
      setBusinessUnits(newBusinessUnit);
      setOpenDeleteDialog(false);
    });
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      const [businessUnitsRes, resourceTeamRes] = await Promise.all([
        getBusinessUnits(),
        getResourceTeam(),
      ]);
      if (businessUnitsRes.data.list) {
        setBusinessUnits(businessUnitsRes.data.list);
      }
      if (resourceTeamRes.data.list) {
        setResourceTeams(resourceTeamRes.data.list);
      }
    };
    getData();
  });

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (!isAddingDialog) {
                formik.resetForm();
              }
              setAddingDialog(true);
              setOpenDialog(true);
            }}
            startIcon={<AddCircleOutlineIcon />}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {businessUnits.map((item: any, index: number) => (
                <TableRow key={index} hover classes={{ root: classes.rowLink }}>
                  <TableCell component="td" scope="row">
                    {item?.id}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.name}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.description}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {getBusinessUnit(item?.business_unit_parent_id)?.name}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.resource_team?.name}
                  </TableCell>
                  <TableCell
                    component="td"
                    scope="row"
                    padding="none"
                    className={classes.archive}
                  >
                    <Box display="flex" justifyContent="center">
                      <Checkbox
                        checked={item?.archive ? true : false}
                        disabled
                        color="default"
                      />
                    </Box>
                  </TableCell>
                  <TableCell component="td" scope="row" padding="none">
                    <TableAction
                      hiddenItem={['view']}
                      onClickEdit={() => {
                        setAddingDialog(false);
                        editBusinessUnitDiaLog(item);
                      }}
                      onClickDelete={() => {
                        handleConfirmDelete(item.id);
                        setBusinessToDeleteId(item.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <FormDialog
        title="Business Unit"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Business Unit Name" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="name"
                id="name"
                autoComplete="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && !!formik.errors.name}
                helperText={formik.touched.name ? formik.errors.name : ''}
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Business Unit Parent" />
              <Select
                fullWidth
                variant="outlined"
                name="business_unit_parent_id"
                id="business_unit_parent_id"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.business_unit_parent_id}
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                <MenuItem value={0}>
                  <em>None</em>
                </MenuItem>
                {businessUnits?.map(state => (
                  <MenuItem key={state.id} value={state.id}>
                    {state.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Resource Team" />
              <Select
                fullWidth
                name="resource_team_id"
                id="resource_team_id"
                variant="outlined"
                autoComplete="resource_team_id"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.resource_team_id}
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                <MenuItem value={0}>
                  <em>None </em>
                </MenuItem>
                {resourceTeams?.map(state => (
                  <MenuItem key={state.id} value={state.id}>
                    {state.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Business Unit Description" />
              <TextField
                multiline
                rows={4}
                fullWidth
                variant="outlined"
                name="description"
                id="description"
                autoComplete="description"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.description}
              />
            </Box>
            {!isAddingDialog && (
              <Box mt={1}>
                <Box component="span">
                  <Typography className={classes.fieldTitle}>
                    {i18n.t('archive')}
                  </Typography>
                  <Checkbox
                    value={formik.values.archive}
                    checked={!!formik.values.archive}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    color="primary"
                    name="archive"
                    id="archive"
                  />
                </Box>
              </Box>
            )}
          </Box>
        }
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Business Unit"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('delete_business_unit_title')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (businessToDeleteId) {
            handleRemoveBusinessUnit(businessToDeleteId);
          }
        }}
      />
      <ConfirmDialog
        open={confirmOpen}
        setOpen={setConfirmOpen}
        header={
          <Box>
            <Box mr={1} component="span">
              <img src={IconWarning} alt="delete" />
            </Box>
            <Typography component="span" variant="h4">
              {i18n.t('delete_business_unit')}
            </Typography>
          </Box>
        }
        content={confirmTitle}
      />
    </Grid>
  );
};
export default BusinessUnit;
