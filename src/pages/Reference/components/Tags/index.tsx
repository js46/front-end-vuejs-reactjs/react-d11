import React, { useState } from 'react';
import {
  TableRow,
  TableCell,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableData,
  TableAction,
} from '../../../../components';

import {
  addTag,
  ITag,
  updateTag,
  getListTags,
  deleteTag,
} from '../../../../services/references.services';

interface Data {
  id: string;
  name: string;
  description: string;
  group_skill_id?: number;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'T#',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
    disableSorting: true,
  },
];

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  first: {
    width: 50,
  },
  action: {
    width: 120,
  },
}));
interface IDataForm {
  id?: number;
  name: string;
  description?: string;
}
const ReferenceTags: React.FC = () => {
  const classes = useStyles();
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [tags, setTags] = useState<ITag[]>([]);
  const [tagToDeleteId, setTagToDeleteId] = useState<number>();
  const { i18n } = useTranslation();
  const handleSubmit = async (item: IDataForm) => {
    if (!item.id) {
      let response = await addTag(item);
      if (response.success) {
        setTags([response.data, ...tags]);
        setOpenDialog(false);
      }
    } else {
      let response = await updateTag(item.id, item);
      if (response.success) {
        let newTags = tags.filter(function(tag) {
          return tag.id !== item.id;
        });
        setTags([response.data, ...newTags]);
        setOpenDialog(false);
      }
    }
  };

  const initialDataForm: IDataForm = {
    name: '',
    description: '',
  };
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      name: Yup.string().required(i18n.t('reference_data.name_required')),
    }),
    onSubmit: (values, { resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });

  const editTagDialog = (item: ITag) => {
    if (item && item.id !== formik.values.id) {
      formik.setValues({
        id: item?.id,
        name: item.name,
        description: item.description,
      });
    }
    setOpenDialog(true);
  };
  const handleRemove = async (id: number) => {
    await deleteTag(id).then(({ data }) => {
      let newTags = tags.filter(tag => {
        return tag.id !== id;
      });
      setTags(newTags);
      setOpenDeleteDialog(false);
    });
  };

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (!isAddingDialog) {
                formik.resetForm();
              }
              setAddingDialog(true);
              setOpenDialog(true);
            }}
            startIcon={<AddCircleOutlineIcon />}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableData
          headCells={headCells}
          fetchData={getListTags}
          action={true}
          setData={setTags}
          body={tags?.map((item: ITag, index: number) => (
            <TableRow key={index} hover classes={{ root: classes.rowLink }}>
              <TableCell component="td" scope="row" className={classes.first}>
                {item?.id}
              </TableCell>
              <TableCell component="td" scope="row">
                {item?.name}
              </TableCell>
              <TableCell component="td" scope="row">
                {item?.description}
              </TableCell>
              <TableCell
                component="td"
                scope="row"
                className={classes.action}
                padding="none"
              >
                <TableAction
                  hiddenItem={['view']}
                  onClickEdit={() => {
                    setAddingDialog(false);
                    editTagDialog(item);
                  }}
                  onClickDelete={() => {
                    setOpenDeleteDialog(true);
                    setTagToDeleteId(item.id);
                  }}
                />
              </TableCell>
            </TableRow>
          ))}
        />
      </Grid>
      <FormDialog
        title="Tags"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Name" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="name"
                id="name"
                autoComplete="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && !!formik.errors.name}
                helperText={formik.touched.name ? formik.errors.name : ''}
              />
            </Box>

            <Box mb={3} mt={3}>
              <MixTitle title="Description" />
              <TextField
                multiline
                rows={4}
                fullWidth
                variant="outlined"
                name="description"
                id="description"
                autoComplete="description"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.description}
              />
            </Box>
          </Box>
        }
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header={i18n.t('reference_data.tag.delete_label')}
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('reference_data.tag.delete_tag_description')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (tagToDeleteId) {
            handleRemove(tagToDeleteId);
          }
        }}
      />
    </Grid>
  );
};
export default ReferenceTags;
