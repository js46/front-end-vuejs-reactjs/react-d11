import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableAction,
} from '../../../../../../components';
import {
  IGroupSkill,
  getGroupSkills,
  updateGroupSkill,
  deleteGroupSkill,
  addGroupSkill,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
interface Data {
  id: string;
  name: string;
  description: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'SG#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
interface IDataForm {
  id?: number;
  name: string;
  description?: string;
}
const SkillGroupings: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [groupSkills, setGroupSkills] = useState<IGroupSkill[]>([]);
  const [groupSkillToDeleteId, setGroupSkillToDeleteId] = useState<number>();
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const handleSubmit = async (item: IDataForm) => {
    if (!item.id) {
      await addGroupSkill(item).then(({ data }) => {
        setGroupSkills([data, ...groupSkills]);
        setOpenDialog(false);
      });
    } else {
      await updateGroupSkill(item.id, item).then(({ data }) => {
        let groupSkillsEdit = groupSkills.filter(function(groupSkill) {
          return groupSkill.id !== data.id;
        });
        setGroupSkills([data, ...groupSkillsEdit]);
        setOpenDialog(false);
      });
    }
  };
  const { i18n } = useTranslation();
  const initialDataForm: IDataForm = {
    name: '',
    description: '',
  };
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      name: Yup.string().required(i18n.t('group_skill.name_required')),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });

  const editGroupSkillDiaLog = (item: IGroupSkill) => {
    if (item && item.id !== formik.values.id) {
      formik.setValues({
        id: item?.id,
        name: item.name,
        description: item.description,
      });
    }
    setOpenDialog(true);
  };
  const handleRemove = async (id: number) => {
    await deleteGroupSkill(id).then(({ data }) => {
      let newGroupSkill = groupSkills.filter(function(groupSkill) {
        return groupSkill.id !== id;
      });
      setGroupSkills(newGroupSkill);
      setOpenDeleteDialog(false);
    });
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      const groupSkillRes = await getGroupSkills();
      if (groupSkillRes.data.list) {
        setGroupSkills(groupSkillRes.data.list);
      }
    };
    getData();
  });

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (!isAddingDialog) {
                formik.resetForm();
              }
              setAddingDialog(true);
              setOpenDialog(true);
            }}
            startIcon={<AddCircleOutlineIcon />}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {groupSkills.map((item: any, index: number) => (
                <TableRow key={index} hover classes={{ root: classes.rowLink }}>
                  <TableCell component="td" scope="row">
                    {item?.id}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.name}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.description}
                  </TableCell>
                  <TableCell component="td" scope="row" padding="none">
                    <TableAction
                      hiddenItem={['view']}
                      onClickEdit={() => {
                        setAddingDialog(false);
                        editGroupSkillDiaLog(item);
                      }}
                      onClickDelete={() => {
                        setOpenDeleteDialog(true);
                        setGroupSkillToDeleteId(item.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <FormDialog
        title="Skill Groupings"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Skill Grouping Name" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="name"
                id="name"
                autoComplete="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && !!formik.errors.name}
                helperText={formik.touched.name ? formik.errors.name : ''}
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Skill Grouping Description" />
              <TextField
                multiline
                rows={4}
                fullWidth
                variant="outlined"
                name="description"
                id="description"
                autoComplete="description"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.description}
              />
            </Box>
          </Box>
        }
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Skills Group"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('delete_skills_group')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (groupSkillToDeleteId) {
            handleRemove(groupSkillToDeleteId);
          }
        }}
      />
    </Grid>
  );
};
export default SkillGroupings;
