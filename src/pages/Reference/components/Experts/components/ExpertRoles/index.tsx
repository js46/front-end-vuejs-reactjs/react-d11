import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Box,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  getExpertRoles,
  IExpertRoles,
  deleteExpertRole,
  addExpertRole,
  updateExpertRole,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
import {
  DeleteDialog,
  AddDialog,
  EditDialog,
  MixTitle,
  TableAction,
} from '../../../../../../components';
import { useTranslation } from 'react-i18next';

interface Data {
  id: string;
  name: string;
  description: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'ER#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr th:last-child': {
      width: 120,
    },
    '& tr th:first-child': {
      width: 50,
    },
  },
}));

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

const ExpertRoles: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [expertRoles, setExpertRoles] = useState<IExpertRoles[]>([]);
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isAddDeleteDialog, setOpenAddDialog] = useState(false);
  const [isOpenEditDialog, setOpenEditDialog] = useState(false);
  const [expertToDeleteId, setExpertToDeleteId] = useState<number>();
  const [expertToEditItem, setExpertToEditItem] = useState<IExpertRoles>();
  const { t } = useTranslation();

  let disableAddButton = name === undefined || name === '';

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      await getDataExpertRoles();
    };
    getData();
  });

  const getDataExpertRoles = async () => {
    const response = await getExpertRoles();
    if (response.data.list) {
      setExpertRoles(response.data.list);
    }
  };

  const handleRemoveExpertRole = async (id: number) => {
    await deleteExpertRole(id).then(({ data }) => {
      let newExpertRoles = expertRoles.filter(function(expert) {
        return expert.id !== id;
      });
      setExpertRoles(newExpertRoles);
      setOpenDeleteDialog(false);
    });
  };

  const handleEditExpertRole = async () => {
    if (expertToEditItem && name) {
      await updateExpertRole(expertToEditItem.id, {
        name: name,
        description: description,
      }).then(({ data }) => {
        let ExpertRoleEdit = expertRoles.filter(function(expert) {
          return expert.id !== data.id;
        });
        setExpertRoles([data, ...ExpertRoleEdit]);
        setOpenEditDialog(false);
      });
    }
  };

  const handleAddExpertRole = async () => {
    await addExpertRole({
      name: name,
      description: description,
    }).then(({ data }) => {
      setExpertRoles([data, ...expertRoles]);
      setOpenAddDialog(false);
    });
  };

  const setExpertToEdit = (item: IExpertRoles) => {
    setName(item.name);
    setDescription(item.description);
  };

  const handleChangeName = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setName(event.target.value);
  };

  const handleChangeDescription = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setDescription(event.target.value);
  };

  return (
    <Box>
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            startIcon={<AddCircleOutlineIcon />}
            onClick={() => {
              setOpenAddDialog(true);
            }}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {expertRoles.map(item => {
                return (
                  <TableRow
                    key={item.id}
                    hover
                    classes={{ root: classes.rowLink }}
                  >
                    <TableCell component="th" scope="row">
                      {item?.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item?.name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item?.description}
                    </TableCell>
                    <TableCell component="th" scope="row" padding="none">
                      <TableAction
                        hiddenItem={['view']}
                        onClickEdit={() => {
                          setOpenEditDialog(true);
                          setExpertToEditItem(item);
                          setExpertToEdit(item);
                        }}
                        onClickDelete={() => {
                          setOpenDeleteDialog(true);
                          setExpertToDeleteId(item.id);
                        }}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>

      <AddDialog
        isOpen={isAddDeleteDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('expert_role')}
                </Typography>
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Name" isRequired />
                <TextField
                  id="name"
                  name="name"
                  fullWidth
                  variant="outlined"
                  value={name}
                  onChange={handleChangeName}
                />
              </Box>
              <Box>
                <MixTitle title="Description" />
                <TextField
                  id="description"
                  name="description"
                  rows={6}
                  multiline
                  fullWidth
                  variant="outlined"
                  value={description}
                  onChange={handleChangeDescription}
                />
              </Box>
            </Box>
          </Box>
        }
        handleClose={() => {
          setOpenAddDialog(false);
          setName('');
          setDescription('');
        }}
        handleAdd={() => {
          handleAddExpertRole();
        }}
        disable={disableAddButton}
      />

      <EditDialog
        isOpen={isOpenEditDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('expert_role')}
                </Typography>
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Name" isRequired />
                <TextField
                  id="name"
                  name="name"
                  fullWidth
                  variant="outlined"
                  value={name}
                  onChange={handleChangeName}
                />
              </Box>
              <Box>
                <MixTitle title="Description" />
                <TextField
                  id="description"
                  name="description"
                  rows={6}
                  multiline
                  fullWidth
                  variant="outlined"
                  value={description}
                  onChange={handleChangeDescription}
                />
              </Box>
            </Box>
          </Box>
        }
        handleClose={() => setOpenEditDialog(false)}
        handleSubmit={() => {
          handleEditExpertRole();
        }}
        // disable={disableEditButton}
      />

      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Expert Role"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_expert_role')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (expertToDeleteId) {
            handleRemoveExpertRole(expertToDeleteId);
          }
        }}
      />
    </Box>
  );
};
export default ExpertRoles;
