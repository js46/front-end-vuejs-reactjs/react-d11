import React, { useState } from 'react';
import {
  TableRow,
  TableCell,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
  Select,
  MenuItem,
  FormHelperText,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableData,
  TableAction,
} from '../../../../../../components';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  IGroupSkill,
  getGroupSkills,
  getExpertSkills,
  addExpertSkill,
  updateExpertSkill,
  deleteExpertSkill,
  IExpertSkill,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';

interface Data {
  id: string;
  name: string;
  description: string;
  group_skill_id?: number;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'ES#',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'group_skill_id',
    label: 'Skills group',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
    disableSorting: true,
  },
];

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  first: {
    width: 50,
  },
  action: {
    width: 120,
  },
}));
interface IDataForm {
  id?: number;
  name: string;
  description?: string;
  group_skill_id?: number;
}
const ExpertSkills: React.FC = () => {
  const classes = useStyles();
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [groupSkills, setGroupSkills] = useState<IGroupSkill[]>([]);
  const [expertSkills, setExpertSkills] = useState<IExpertSkill[]>([]);
  const [expertSkillToDeleteId, setExpertSkillToDeleteIdd] = useState<number>();
  const { i18n } = useTranslation();
  const handleSubmit = async (item: IDataForm) => {
    if (!item.id) {
      let newExpertSkills = [...expertSkills];
      await addExpertSkill(item).then(({ data }) => {
        newExpertSkills.push(data);
        setExpertSkills(newExpertSkills);
        setOpenDialog(false);
      });
    } else {
      await updateExpertSkill(item.id, item).then(({ data }) => {
        let expertSkillsEdit = expertSkills.filter(function(expertSkill) {
          return expertSkill.id !== data.id;
        });
        setExpertSkills([data, ...expertSkillsEdit]);
        setOpenDialog(false);
      });
    }
  };

  const initialDataForm: IDataForm = {
    name: '',
    description: '',
  };
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      name: Yup.string().required(i18n.t('reference_data.name_required')),
      group_skill_id: Yup.number().required(
        i18n.t('reference_data.expert_skill.group_skill_required'),
      ),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });

  const editExpertSkillDialog = (item: IExpertSkill) => {
    if (item && item.id !== formik.values.id) {
      formik.setValues({
        id: item?.id,
        name: item.name,
        description: item.description,
        group_skill_id: item?.group_skill?.id,
      });
    }
    setOpenDialog(true);
  };
  const handleRemove = async (id: number) => {
    await deleteExpertSkill(id).then(({ data }) => {
      let newExpertSkill = expertSkills.filter(function(expertSkill) {
        return expertSkill.id !== id;
      });
      setExpertSkills(newExpertSkill);
      setOpenDeleteDialog(false);
    });
  };

  useEffectOnlyOnce(() => {
    getGroupSkills().then(res => {
      if (res.data.list) {
        setGroupSkills(res.data.list);
      }
    });
  });

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          {groupSkills.length > 0 && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                if (!isAddingDialog) {
                  formik.resetForm();
                }
                setAddingDialog(true);
                setOpenDialog(true);
              }}
              startIcon={<AddCircleOutlineIcon />}
            >
              Add
            </Button>
          )}
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableData
          headCells={headCells}
          fetchData={getExpertSkills}
          action={true}
          setData={setExpertSkills}
          body={expertSkills?.map((item: any, index: number) => (
            <TableRow key={index} hover classes={{ root: classes.rowLink }}>
              <TableCell component="td" scope="row" className={classes.first}>
                {item?.id}
              </TableCell>
              <TableCell component="td" scope="row">
                {item?.name}
              </TableCell>
              <TableCell component="td" scope="row">
                {item?.group_skill?.title}
              </TableCell>
              <TableCell component="td" scope="row">
                {item?.description}
              </TableCell>
              <TableCell
                component="td"
                scope="row"
                className={classes.action}
                padding="none"
              >
                <TableAction
                  hiddenItem={['view']}
                  onClickEdit={() => {
                    setAddingDialog(false);
                    editExpertSkillDialog(item);
                  }}
                  onClickDelete={() => {
                    setOpenDeleteDialog(true);
                    setExpertSkillToDeleteIdd(item.id);
                  }}
                />
              </TableCell>
            </TableRow>
          ))}
        />
      </Grid>
      <FormDialog
        title="Expert Skills"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Expert Skill Name" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="name"
                id="name"
                autoComplete="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && !!formik.errors.name}
                helperText={formik.touched.name ? formik.errors.name : ''}
              />
            </Box>

            <Box mb={3} mt={3}>
              <MixTitle title="Skills Group" isRequired />
              <Select
                fullWidth
                variant="outlined"
                name="group_skill_id"
                id="group_skill_id"
                onChange={formik.handleChange}
                onBlur={event => {
                  event.target.name = 'group_skill_id';
                  formik.handleBlur(event);
                }}
                //onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.group_skill_id}
                error={
                  formik.touched.group_skill_id &&
                  !!formik.errors.group_skill_id
                }
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {groupSkills?.map(state => (
                  <MenuItem key={state.id} value={state.id}>
                    {state.name}
                  </MenuItem>
                ))}
              </Select>

              <FormHelperText>
                {formik.touched.group_skill_id
                  ? formik.errors.group_skill_id
                  : ''}
              </FormHelperText>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Expert Skill Description" />
              <TextField
                multiline
                rows={4}
                fullWidth
                variant="outlined"
                name="description"
                id="description"
                autoComplete="description"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.description}
              />
            </Box>
          </Box>
        }
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Expert skin"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('delete_expert_skill')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (expertSkillToDeleteId) {
            handleRemove(expertSkillToDeleteId);
          }
        }}
      />
    </Grid>
  );
};
export default ExpertSkills;
