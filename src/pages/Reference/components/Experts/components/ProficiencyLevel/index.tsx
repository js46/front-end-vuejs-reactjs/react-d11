import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  TextField,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';

import {
  MixTitle,
  FormDialog,
  TableAction,
} from '../../../../../../components';
import {
  getProficiencyLevels,
  updateProficiencyLevel,
  IProficiencyLevel,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
import { useTranslation } from 'react-i18next';
interface Data {
  id?: number;
  name: string;
  description: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'PL#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
interface IDataForm {
  id?: number;
  name: string;
  description?: string;
}
const ProficiencyLevel: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [proficiencyLevels, setProficiencyLevels] = useState<
    IProficiencyLevel[]
  >([]);

  const { i18n } = useTranslation();
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const handleSubmit = (item: IDataForm) => {
    if (item.id) {
      updateProficiencyLevel(item.id, item).then(({ data }) => {
        let proficiencyLevelEdit = proficiencyLevels.filter(function(
          proficiencyLevel,
        ) {
          return proficiencyLevel.id !== data.id;
        });
        setProficiencyLevels([data, ...proficiencyLevelEdit]);
        setOpenDialog(false);
      });
    }
  };

  const initialDataForm: IDataForm = {
    name: '',
    description: '',
  };
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      name: Yup.string().required(i18n.t('reference_data.name_required')),
      description: Yup.string().required(
        i18n.t('reference_data.desc_required'),
      ),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });

  const editProficiencyLevelDialog = (item: IProficiencyLevel) => {
    if (item && item.id !== formik.values.id) {
      formik.setValues({
        id: item?.id,
        name: item.name,
        description: item.description,
      });
    }
    setOpenDialog(true);
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      const proficiencyLevelRes = await getProficiencyLevels();
      if (proficiencyLevelRes.data.list) {
        setProficiencyLevels(proficiencyLevelRes.data.list);
      }
    };
    getData();
  });
  return (
    <Box mt={4}>
      <Grid container direction="column">
        <Grid container alignItems="center" justify="center">
          <TableContainer>
            <Table className={classes.table}>
              <EnhancedTableHead
                classes={classes}
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
              />
              <TableBody>
                {proficiencyLevels.map((item: any, index: number) => (
                  <TableRow
                    key={index}
                    hover
                    classes={{ root: classes.rowLink }}
                  >
                    <TableCell component="td" scope="row">
                      {item?.id}
                    </TableCell>
                    <TableCell component="td" scope="row">
                      {item?.name}
                    </TableCell>

                    <TableCell component="td" scope="row">
                      {item?.description}
                    </TableCell>
                    <TableCell component="td" scope="row" padding="none">
                      <TableAction
                        hiddenItem={['delete', 'view']}
                        onClickEdit={() => {
                          editProficiencyLevelDialog(item);
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <FormDialog
          title="Proficiency Level"
          submit={formik.handleSubmit}
          open={openDialog}
          setOpen={setOpenDialog}
          isAddingForm={false}
          disable={!(formik.isValid && formik.dirty)}
          formContent={
            <Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Proficiency Level Name" isRequired />
                <TextField
                  fullWidth
                  variant="outlined"
                  name="name"
                  id="name"
                  disabled
                  autoComplete="name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                  error={formik.touched.name && !!formik.errors.name}
                  helperText={formik.touched.name ? formik.errors.name : ''}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Proficiency Level Description" />
                <TextField
                  multiline
                  rows={4}
                  fullWidth
                  variant="outlined"
                  name="description"
                  id="description"
                  autoComplete="description"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.description}
                />
              </Box>
            </Box>
          }
        />
      </Grid>
    </Box>
  );
};
export default ProficiencyLevel;
