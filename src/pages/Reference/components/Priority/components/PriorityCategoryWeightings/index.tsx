import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Box,
  Typography,
  MenuItem,
  Select,
  OutlinedInput,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  getListPriorityCategoryWeighting,
  IPriorityCategoryWeighting,
  getPriorityCategories,
  IPriorityCategories,
  addPriorityCategoryWeighting,
  updatePriorityCategoryWeighting,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
import {
  AddDialog,
  EditDialog,
  MixTitle,
  TableAction,
} from '../../../../../../components';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import NumberFormat from 'react-number-format';
import { useTranslation } from 'react-i18next';

interface Data {
  id: string;
  category: string;
  weight: number;
}

interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'PCW#',
    disablePadding: true,
  },
  {
    id: 'category',
    label: 'Category',
    disablePadding: true,
  },
  {
    id: 'weight',
    label: 'Weight',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}

interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr th:last-child': {
      width: 120,
    },
    '& tr th:first-child': {
      width: 50,
    },
  },
  error: {
    color: theme.palette.secondary.main,
    fontWeight: 'bold',
    marginTop: 10,
  },
}));

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

const PriorityCategoryWeightings: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [priorityCategories, setPriorityCategories] = useState<
    IPriorityCategories[]
  >([]);
  const [priorityCategoryWeightings, setPriorityCategoryWeightings] = useState<
    IPriorityCategoryWeighting[]
  >([]);
  const [categorySelected, setCategorySelected] = useState<number>(0);
  const [weight, setWeight] = useState<number>(0);
  //const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isAddDeleteDialog, setOpenAddDialog] = useState(false);
  const [isOpenEditDialog, setOpenEditDialog] = useState(false);
  //const [categoryToDeleteId, setCategoryToDeleteId] = useState<number>();
  const [priorityCategoryToEditItem, setPriorityToEditItem] = useState<
    IPriorityCategoryWeighting
  >();
  const [showError, setShowError] = useState<boolean>(false);
  const { t } = useTranslation();
  // let disableAddButton = name === undefined || name === '';

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      await getDataPriorityCategoryWeighting();
      await getDataPriorityCategories();
    };
    getData();
  });

  const getDataPriorityCategoryWeighting = async () => {
    const response = await getListPriorityCategoryWeighting();
    setPriorityCategoryWeightings(response.data.list);
  };

  const getDataPriorityCategories = async () => {
    const response = await getPriorityCategories();
    setPriorityCategories(response.data.list);
  };

  /*  const handleRemovePriorityCategory = async (id: number) => {
    await deletePriorityCategoryWeighting(id).then(({ data }) => {
      let newPriorityCategoryWeightings = priorityCategoryWeightings.filter(
        function(category) {
          return category.id !== data.id;
        },
      );
      setPriorityCategoryWeightings(newPriorityCategoryWeightings);
      setOpenDeleteDialog(false);
    });
  }; */

  const handleEditPriorityCategory = async () => {
    if (priorityCategoryToEditItem) {
      let currentWeight = 0;
      priorityCategoryWeightings.map(category => {
        return (currentWeight = category.weighting + currentWeight);
      });
      if (currentWeight + weight <= 100) {
        await updatePriorityCategoryWeighting(priorityCategoryToEditItem.id, {
          weighting: weight,
          category_id: categorySelected,
        }).then(({ data }) => {
          let priorityCategoryEdit = priorityCategoryWeightings.filter(function(
            category,
          ) {
            return category.id !== data.id;
          });
          setPriorityCategoryWeightings([data, ...priorityCategoryEdit]);
          setOpenEditDialog(false);
        });
      } else {
        setShowError(true);
      }
    }
  };

  const handleAddPriorityCategory = async () => {
    let currentWeight = 0;
    priorityCategoryWeightings.map(category => {
      return (currentWeight = category.weighting + currentWeight);
    });
    if (currentWeight + weight <= 100) {
      await addPriorityCategoryWeighting({
        weighting: weight,
        category_id: categorySelected,
      }).then(({ data }) => {
        setPriorityCategoryWeightings([data, ...priorityCategoryWeightings]);
        setOpenAddDialog(false);
      });
    } else {
      setShowError(true);
    }
  };

  const setPriorityToEdit = (item: IPriorityCategoryWeighting) => {
    setCategorySelected(item.category.id);
    setWeight(item.weighting);
  };

  const onSelectCategory = (value: unknown) => {
    setCategorySelected(Number(value));
  };

  const handleChangeWeighting = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setWeight(Number(event.target.value));
  };

  return (
    <Box>
      <Box
        display="flex"
        mt={3}
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {priorityCategoryWeightings?.map(item => {
                return (
                  <TableRow
                    key={item.id}
                    hover
                    classes={{ root: classes.rowLink }}
                  >
                    <TableCell component="th" scope="row">
                      {item.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.category?.category_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.weighting}
                    </TableCell>
                    <TableCell component="th" scope="row" padding="none">
                      <TableAction
                        hiddenItem={['delete', 'view']}
                        onClickEdit={() => {
                          setOpenEditDialog(true);
                          setPriorityToEditItem(item);
                          setPriorityToEdit(item);
                        }}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>

      <AddDialog
        isOpen={isAddDeleteDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('priority_Category_weightings')}
                </Typography>
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Category" />
                <Select
                  fullWidth
                  variant="outlined"
                  value={categorySelected ? categorySelected : 0}
                  id="category"
                  name="category"
                  onChange={e => {
                    onSelectCategory(e.target.value);
                  }}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                >
                  <MenuItem value={0}>
                    <em> None </em>
                  </MenuItem>
                  {priorityCategories?.map(category => {
                    return (
                      <MenuItem key={category.id} value={category.id}>
                        {category.category_name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </Box>
              <Box>
                <MixTitle title="Weight" />
                <OutlinedInput
                  value={weight}
                  name="weight"
                  type="text"
                  placeholder={'%'}
                  onChange={handleChangeWeighting}
                  fullWidth
                  inputComponent={PercentInput}
                />
              </Box>
              {showError && (
                <Box mt={1}>
                  <Typography className={classes.error}>
                    {t('total_weight_max_100%')}
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        }
        handleClose={() => {
          setOpenAddDialog(false);
          setCategorySelected(0);
          setWeight(0);
          setShowError(false);
        }}
        handleAdd={() => {
          handleAddPriorityCategory();
        }}
        disable={false}
      />

      <EditDialog
        isOpen={isOpenEditDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('priority_Category_weightings')}
                </Typography>
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Category" />
                <Select
                  fullWidth
                  variant="outlined"
                  value={categorySelected}
                  id="category"
                  name="category"
                  onChange={e => {
                    onSelectCategory(e.target.value);
                  }}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                >
                  <MenuItem value={0}>
                    <em> None </em>
                  </MenuItem>
                  {priorityCategories?.map(category => {
                    return (
                      <MenuItem key={category.id} value={category.id}>
                        {category.category_name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </Box>
              <Box>
                <MixTitle title="Weight" />
                <OutlinedInput
                  value={weight}
                  name="weight"
                  type="text"
                  placeholder={'%'}
                  onChange={handleChangeWeighting}
                  fullWidth
                  inputComponent={PercentInput}
                />
              </Box>
              {showError && (
                <Box mt={1}>
                  <Typography className={classes.error}>
                    {t('total_weight_max_100')}
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        }
        handleClose={() => {
          setOpenEditDialog(false);
          setCategorySelected(0);
          setWeight(0);
          setShowError(false);
        }}
        handleSubmit={() => {
          handleEditPriorityCategory();
        }}
        // disable={disableEditButton}
      />

      {/* <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Priority Category Weightings"
        message={
          <Box>
            <Typography component="p" variant="body1">
              Are you sure you want to delete this priority category weightings?
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (categoryToDeleteId) {
            handleRemovePriorityCategory(categoryToDeleteId);
          }
        }}
      /> */}
    </Box>
  );
};

function PercentInput(props: any) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator
      isNumericString
      suffix="%"
    />
  );
}

export default PriorityCategoryWeightings;
