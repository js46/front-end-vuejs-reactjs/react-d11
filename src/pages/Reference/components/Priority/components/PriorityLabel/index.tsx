import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Box,
  Button,
  Typography,
  TextField,
  Dialog,
  DialogActions,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  getPriorityLabel,
  IPriorityLabel,
  addPriorityLabel,
  updatePriorityLabel,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
import { AddDialog, MixTitle, TableAction } from '../../../../../../components';
import { useTranslation } from 'react-i18next';

interface Data {
  id?: number;
  label: string;
  score_from: number;
  score_to: number;
  description: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'PL#',
    disablePadding: true,
  },
  {
    id: 'label',
    label: 'Label',
    disablePadding: true,
  },
  {
    id: 'score_from',
    label: 'Score from',
    disablePadding: true,
  },
  {
    id: 'score_to',
    label: 'Score to',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr th:last-child': {
      width: 120,
    },
    '& tr th:first-child': {
      width: 50,
    },
  },
}));

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
const initialDataForm: Data = {
  label: '',
  score_from: 0,
  score_to: 500,
  description: '',
};
const PriorityLabel: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [priorityLabel, setPriorityLabel] = useState<IPriorityLabel[]>([]);
  const [label, setLabel] = useState<string>('');
  const [scoreFrom, setScoreFrom] = useState<number>(0);
  const [scoreTo, setScoreTo] = useState<number>(0);
  const [description, setDescription] = useState<string>('');
  //const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isAddDeleteDialog, setOpenAddDialog] = useState(false);
  const [isOpenEditDialog, setOpenEditDialog] = useState(false);
  //const [priorityToDeleteId, setPriorityLabelToDeleteId] = useState<number>();
  /* const [priorityLabelToEditItem, setPriorityLabelToEditItem] = useState<
    IPriorityLabel
  >(); */
  const { t } = useTranslation();
  let disableAddButton = label === undefined || label === '';

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      await getDataPriorityLabel();
    };
    getData();
  });

  const getDataPriorityLabel = async () => {
    const response = await getPriorityLabel();
    setPriorityLabel(response.data.list);
  };

  /* const handleRemovePriorityLabel = async (id: number) => {
    await deletePriorityLabel(id).then(({ data }) => {
      let newPriorityLabel = priorityLabel.filter(function(priority) {
        return priority.id !== data.id;
      });
      setPriorityLabel(newPriorityLabel);
      setOpenDeleteDialog(false);
    });
  }; */
  const handleSubmit = (item: Data) => {
    if (!item.id) {
      addPriorityLabel(item).then(({ data }) => {
        setPriorityLabel([data, ...priorityLabel]);
        setOpenAddDialog(false);
      });
    } else {
      updatePriorityLabel(item.id, item).then(({ data }) => {
        let priorityLabelEdit = priorityLabel.filter(function(label) {
          return label.id !== data.id;
        });
        setPriorityLabel([data, ...priorityLabelEdit]);
        setOpenEditDialog(false);
      });
    }
  };

  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      label: Yup.string().required('This label is required'),
      score_from: Yup.number()
        .required('This score from is required')
        .max(100, 'The score from  is 0 to 100!')
        .min(0, 'The score from  is 0 to 100!'),
      score_to: Yup.number()
        .required('This score to is required')

        .when('score_from', (st: number, schema: any) => {
          return Yup.number()
            .min(st, 'score from is less than ' + st)
            .max(100, 'The score is greater than 100!');
        }),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });
  const setPriorityLabelToEdit = (item: IPriorityLabel) => {
    if (item && item.id !== formik.values.id) {
      formik.setValues({
        id: item?.id,
        label: item.label,
        score_from: item?.score_from,
        score_to: item?.score_to,
        description: item?.description,
      });
    }

    setOpenEditDialog(true);
  };

  const handleChangePriorityLabel = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setLabel(event.target.value);
  };

  const handleChangePriorityDescription = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setDescription(event.target.value);
  };

  const handleChangeScoreFrom = (event: React.ChangeEvent<any>) => {
    setScoreFrom(Number(event.target.value.replace(/[^0-9]/gm, '')));
  };

  const handleChangeScoreTo = (event: React.ChangeEvent<any>) => {
    setScoreTo(Number(event.target.value.replace(/[^0-9]/gm, '')));
  };

  return (
    <Box>
      <Box
        display="flex"
        mt={3}
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {priorityLabel?.map(item => {
                return (
                  <TableRow
                    key={item.id}
                    hover
                    classes={{ root: classes.rowLink }}
                  >
                    <TableCell component="th" scope="row">
                      {item.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.label}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.score_from}
                    </TableCell>
                    <TableCell>{item.score_to}</TableCell>
                    <TableCell>{item.description}</TableCell>
                    <TableCell component="th" scope="row" padding="none">
                      <TableAction
                        hiddenItem={['delete', 'view']}
                        onClickEdit={() => {
                          setPriorityLabelToEdit(item);
                        }}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>

      <AddDialog
        isOpen={isAddDeleteDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('priority_label')}
                </Typography>
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title={t('priority_label')} />
                <TextField
                  id="label"
                  name="label"
                  fullWidth
                  variant="outlined"
                  value={label}
                  onChange={handleChangePriorityLabel}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Score From" />
                <TextField
                  id="points"
                  name="points"
                  fullWidth
                  type="text"
                  variant="outlined"
                  value={scoreFrom}
                  onChange={handleChangeScoreFrom}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Score To" />
                <TextField
                  id="points"
                  name="points"
                  fullWidth
                  type="text"
                  variant="outlined"
                  value={scoreTo}
                  onChange={handleChangeScoreTo}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Description" />
                <TextField
                  id="description"
                  name="description"
                  rows={6}
                  multiline
                  fullWidth
                  variant="outlined"
                  value={description}
                  onChange={handleChangePriorityDescription}
                />
              </Box>
            </Box>
          </Box>
        }
        handleClose={() => {
          setOpenAddDialog(false);
          setScoreFrom(0);
          setLabel('');
          setScoreTo(0);
          setDescription('');
        }}
        handleAdd={() => {
          //handleAddPriorityLabel();
        }}
        disable={disableAddButton}
      />

      <Dialog
        open={isOpenEditDialog}
        onClose={() => {
          setOpenEditDialog(false);
        }}
        fullWidth
        maxWidth="sm"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Box padding={4} minWidth="578px">
          <Box mb={3}>
            <Box textAlign="center">
              <Typography component="p" variant="h4">
                {t('priority_label')}
              </Typography>
            </Box>
            <form onSubmit={formik.handleSubmit}>
              <Box mb={3} mt={3}>
                <MixTitle title={t('priority_label')} />
                <TextField
                  id="label"
                  name="label"
                  fullWidth
                  variant="outlined"
                  value={formik.values.label}
                  autoComplete="name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.label && !!formik.errors.label}
                  helperText={formik.touched.label ? formik.errors.label : ''}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Score From" />
                <TextField
                  id="points"
                  name="score_from"
                  fullWidth
                  type="text"
                  variant="outlined"
                  autoComplete="score_from"
                  onChange={e => {
                    e.preventDefault();
                    const { value } = e.target;
                    const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                    if (regex.test(value.toString()) || value === '') {
                      formik.setFieldValue(
                        'score_from',
                        value ? Number(value) : '',
                      );
                    }
                  }}
                  onBlur={formik.handleBlur}
                  value={formik.values.score_from}
                  error={
                    formik.touched.score_from && !!formik.errors.score_from
                  }
                  helperText={
                    formik.touched.score_from ? formik.errors.score_from : ''
                  }
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Score To" />
                <TextField
                  id="score_to"
                  name="score_to"
                  fullWidth
                  type="text"
                  variant="outlined"
                  autoComplete="score_to"
                  onChange={e => {
                    e.preventDefault();
                    const { value } = e.target;
                    const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                    if (regex.test(value.toString()) || value === '') {
                      formik.setFieldValue(
                        'score_to',
                        value ? Number(value) : '',
                      );
                    }
                  }}
                  onBlur={formik.handleBlur}
                  value={formik.values.score_to}
                  error={formik.touched.score_to && !!formik.errors.score_to}
                  helperText={
                    formik.touched.score_to ? formik.errors.score_to : ''
                  }
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Description" />
                <TextField
                  id="description"
                  name="description"
                  fullWidth
                  rows={6}
                  multiline
                  variant="outlined"
                  autoComplete="description"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.description}
                  error={
                    formik.touched.description && !!formik.errors.description
                  }
                  helperText={
                    formik.touched.description ? formik.errors.description : ''
                  }
                />
              </Box>

              <DialogActions>
                <Button
                  onClick={() => {
                    setOpenEditDialog(false);
                  }}
                  variant="outlined"
                >
                  Cancel
                </Button>
                <Button
                  type="submit"
                  disabled={!formik.dirty}
                  color="primary"
                  variant="contained"
                >
                  Update
                </Button>
              </DialogActions>
            </form>
          </Box>
        </Box>
      </Dialog>

      {/* <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Priority Label"
        message={
          <Box>
            <Typography component="p" variant="body1">
              Are you sure you want to delete this priority label?
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (priorityToDeleteId) {
            handleRemovePriorityLabel(priorityToDeleteId);
          }
        }}
      /> */}
    </Box>
  );
};
export default PriorityLabel;
