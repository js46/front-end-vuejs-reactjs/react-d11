import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
  Checkbox,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableAction,
} from '../../../../../../components';
import {
  IBenefitFocusCategory,
  getBenefitFocusCategories,
  addBenefitFocusCategory,
  updateBenefitFocusCategory,
  deleteBenefitFocusCategory,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';

interface HeadCell {
  id: keyof IData;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

interface IData {
  id?: number;
  name: string;
  description?: string;
  points?: number;
  archive?: boolean;
}
const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'BFC#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
  {
    id: 'points',
    label: 'Point',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },

  number: {
    '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
  },

  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  archive: {
    width: 50,
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof IData) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Archive</TableCell>
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
const BenefitFocusCategory: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof IData>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [benefitFocusCategories, setBenefitFocusCategories] = useState<
    IBenefitFocusCategory[]
  >([]);
  const [opportunityTypeToDeleteId, setOpportunityTypeToDeleteId] = useState<
    number
  >();
  const { i18n } = useTranslation();
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSubmit = async (item: IData) => {
    if (!item.id) {
      await addBenefitFocusCategory(item).then(({ data }) => {
        setBenefitFocusCategories([data, ...benefitFocusCategories]);
        setOpenDialog(false);
      });
    } else {
      await updateBenefitFocusCategory(item.id, item).then(({ data }) => {
        let opportunityTypeEdit = benefitFocusCategories.filter(function(
          opportunityType,
        ) {
          return opportunityType.id !== data.id;
        });
        setBenefitFocusCategories([data, ...opportunityTypeEdit]);
        setOpenDialog(false);
      });
    }
  };

  const initialDataForm: IData = {
    name: '',
  };
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      name: Yup.string().required(i18n.t('reference_data.name_required')),
      points: Yup.number()
        .required(i18n.t('reference_data.point_required'))
        .max(5, i18n.t('reference_data.range_point'))
        .min(1, i18n.t('reference_data.range_point')),
      archive: Yup.boolean(),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });

  const editOpportunityTypeDiaLog = (item: IBenefitFocusCategory) => {
    if (item && formik.values.id !== item.id) {
      formik.setValues({
        name: item.name,
        description: item.description,
        points: item.points,
        archive: item.archive,
        id: item.id,
      });
    }
  };

  const handleRemove = async (id: number) => {
    await deleteBenefitFocusCategory(id).then(({ data }) => {
      let newBenefitFocusCategories = benefitFocusCategories.filter(function(
        benefitFocusCategory,
      ) {
        return benefitFocusCategory.id !== data.id;
      });
      setBenefitFocusCategories(newBenefitFocusCategories);
      setOpenDeleteDialog(false);
    });
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      const benefitFocusCategoriesRes = await getBenefitFocusCategories();
      if (benefitFocusCategoriesRes.data.list) {
        setBenefitFocusCategories(benefitFocusCategoriesRes.data.list);
      }
    };
    getData();
  });

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (!isAddingDialog) {
                formik.resetForm();
              }
              setAddingDialog(true);
              setOpenDialog(true);
            }}
            startIcon={<AddCircleOutlineIcon />}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {benefitFocusCategories.map((item: any, index: number) => (
                <TableRow key={index} hover classes={{ root: classes.rowLink }}>
                  <TableCell component="td" scope="row">
                    {item?.id}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.name}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.description}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.points}
                  </TableCell>
                  <TableCell
                    component="td"
                    scope="row"
                    padding="none"
                    className={classes.archive}
                  >
                    <Box display="flex" justifyContent="center">
                      <Checkbox
                        checked={item?.archive ? true : false}
                        disabled
                        color="default"
                      />
                    </Box>
                  </TableCell>
                  <TableCell component="td" scope="row" padding="none">
                    <TableAction
                      hiddenItem={['view']}
                      onClickEdit={() => {
                        setAddingDialog(false);
                        setOpenDialog(true);
                        editOpportunityTypeDiaLog(item);
                      }}
                      onClickDelete={() => {
                        setOpenDeleteDialog(true);
                        setOpportunityTypeToDeleteId(item.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <FormDialog
        title="Benefit Focus Category"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Benefit Focus Category Name" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="name"
                id="name"
                autoComplete="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && !!formik.errors.name}
                helperText={formik.touched.name ? formik.errors.name : ''}
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Benefit Focus Category Description" />
              <TextField
                fullWidth
                multiline
                rows={4}
                variant="outlined"
                name="description"
                id="description"
                autoComplete="description"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.description}
                error={
                  formik.touched.description && !!formik.errors.description
                }
                helperText={
                  formik.touched.description ? formik.errors.description : ''
                }
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Point" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="points"
                id="points"
                type="number"
                className={classes.number}
                InputLabelProps={{
                  shrink: true,
                }}
                autoComplete="points"
                onChange={e => {
                  e.preventDefault();
                  const { value } = e.target;
                  const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                  if (regex.test(value.toString()) || value === '') {
                    formik.setFieldValue('points', value ? Number(value) : '');
                  }
                }}
                onBlur={formik.handleBlur}
                value={formik.values.points}
                error={formik.touched.points && !!formik.errors.points}
                helperText={formik.touched.points ? formik.errors.points : ''}
              />
            </Box>
            {!isAddingDialog && (
              <Box mt={1}>
                <Box component="span">
                  <Typography className={classes.fieldTitle} component="p">
                    {i18n.t('archive')}
                  </Typography>
                  <Checkbox
                    value={formik.values.archive}
                    checked={!!formik.values.archive}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    color="primary"
                    name="archive"
                    id="archive"
                  />
                </Box>
              </Box>
            )}
          </Box>
        }
      />

      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Benefit Focus Category"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('delete_benefit_focus_category')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (opportunityTypeToDeleteId) {
            handleRemove(opportunityTypeToDeleteId);
          }
        }}
      />
    </Grid>
  );
};
export default BenefitFocusCategory;
