import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useTranslation } from 'react-i18next';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableAction,
} from '../../../../../../components';
import {
  IBenefitFocusValue,
  getBenefitFocusValues,
  addBenefitFocusValue,
  updateBenefitFocusValue,
  deleteBenefitFocusValue,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';

interface HeadCell {
  id: keyof IData;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

interface IData {
  id?: number;
  band: string;
  nominal_value?: number;
  points?: number;
}
const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'BFV#',
    disablePadding: true,
  },
  {
    id: 'band',
    label: 'BFV band',
    disablePadding: true,
  },
  {
    id: 'nominal_value',
    label: 'Nominal value',
    disablePadding: true,
  },
  {
    id: 'points',
    label: 'Point',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  number: {
    '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
  },

  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof IData) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
export const BenefitFocusValue: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof IData>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [benefitFocusValues, setBenefitFocusValues] = useState<
    IBenefitFocusValue[]
  >([]);
  const [opportunityTypeToDeleteId, setOpportunityTypeToDeleteId] = useState<
    number
  >();

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const handleSubmit = async (item: IData) => {
    if (!item.id) {
      await addBenefitFocusValue(item).then(({ data }) => {
        setBenefitFocusValues([data, ...benefitFocusValues]);
        setOpenDialog(false);
      });
    } else {
      await updateBenefitFocusValue(item.id, item).then(({ data }) => {
        let benefitFocusValuesEdit = benefitFocusValues.filter(function(
          benefitFocusValue,
        ) {
          return benefitFocusValue.id !== data.id;
        });
        setBenefitFocusValues([data, ...benefitFocusValuesEdit]);
        setOpenDialog(false);
      });
    }
  };

  const initialDataForm: IData = {
    band: '',
  };
  const { i18n } = useTranslation();
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      band: Yup.string().required(
        i18n.t('reference_data.benefit_focus_value.band_required'),
      ),
      nominal_value: Yup.number()
        .min(0)
        .required(
          i18n.t('reference_data.benefit_focus_value.nominal_value_required'),
        ),
      points: Yup.number()
        .required(i18n.t('reference_data.point_required'))
        .max(5, i18n.t('reference_data.range_point'))
        .min(1, i18n.t('reference_data.range_point')),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });
  const editOpportunityTypeDiaLog = (item: IBenefitFocusValue) => {
    if (item && formik.values.id !== item.id) {
      formik.setValues({
        band: item.band,
        nominal_value: item.nominal_value,
        points: item.points,
        id: item.id,
      });
    }
  };
  const handleRemove = async (id: number) => {
    await deleteBenefitFocusValue(id).then(({ data }) => {
      let newBenefitFocusValues = benefitFocusValues.filter(function(
        benefitFocusValue,
      ) {
        return benefitFocusValue.id !== data.id;
      });
      setBenefitFocusValues(newBenefitFocusValues);
      setOpenDeleteDialog(false);
    });
  };
  useEffectOnlyOnce(() => {
    const getData = async () => {
      const benefitFocusValuesRes = await getBenefitFocusValues();
      if (benefitFocusValuesRes.data.list) {
        setBenefitFocusValues(benefitFocusValuesRes.data.list);
      }
    };
    getData();
  });

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (!isAddingDialog) {
                formik.resetForm();
              }
              setAddingDialog(true);
              setOpenDialog(true);
            }}
            startIcon={<AddCircleOutlineIcon />}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {benefitFocusValues.map((item: any, index: number) => (
                <TableRow key={index} hover classes={{ root: classes.rowLink }}>
                  <TableCell component="td" scope="row">
                    {item?.id}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.band}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.nominal_value}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.points}
                  </TableCell>

                  <TableCell component="td" scope="row" padding="none">
                    <TableAction
                      hiddenItem={['view']}
                      onClickEdit={() => {
                        setAddingDialog(false);
                        setOpenDialog(true);
                        editOpportunityTypeDiaLog(item);
                      }}
                      onClickDelete={() => {
                        setOpenDeleteDialog(true);
                        setOpportunityTypeToDeleteId(item.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <FormDialog
        title="Benefit Focus Value"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Benefit Focus Value Band" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="band"
                id="band"
                autoComplete="band"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.band}
                error={formik.touched.band && !!formik.errors.band}
                helperText={formik.touched.band ? formik.errors.band : ''}
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Nominal Value" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="nominal_value"
                id="nominal_value"
                type="number"
                className={classes.number}
                InputLabelProps={{
                  shrink: true,
                }}
                autoComplete="nominal_value"
                onChange={e => {
                  e.preventDefault();
                  const { value } = e.target;
                  const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                  if (regex.test(value.toString()) || value === '') {
                    formik.setFieldValue(
                      'nominal_value',
                      value ? Number(value) : '',
                    );
                  }
                }}
                onBlur={formik.handleBlur}
                value={formik.values.nominal_value}
                error={
                  formik.touched.nominal_value && !!formik.errors.nominal_value
                }
                helperText={
                  formik.touched.nominal_value
                    ? formik.errors.nominal_value
                    : ''
                }
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Point" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="points"
                id="points"
                type="number"
                className={classes.number}
                InputLabelProps={{
                  shrink: true,
                }}
                autoComplete="points"
                onChange={e => {
                  e.preventDefault();
                  const { value } = e.target;
                  const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                  if (regex.test(value.toString()) || value === '') {
                    formik.setFieldValue('points', value ? Number(value) : '');
                  }
                }}
                onBlur={formik.handleBlur}
                value={formik.values.points}
                error={formik.touched.points && !!formik.errors.points}
                helperText={formik.touched.points ? formik.errors.points : ''}
              />
            </Box>
          </Box>
        }
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Benefit Focus Value"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('delete_benefit_focus_value')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (opportunityTypeToDeleteId) {
            handleRemove(opportunityTypeToDeleteId);
          }
        }}
      />
    </Grid>
  );
};
export default BenefitFocusValue;
