import React, { useState, useEffect, useCallback } from 'react';
import {
  Table,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
  CircularProgress,
  Dialog,
  IconButton,
  FormControl,
} from '@material-ui/core';
import { Formik, FieldArray } from 'formik';
// import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';
import { useTranslation } from 'react-i18next';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableAction,
} from '../../../../components';
import PlayBookItem from './components/PlayBookItem';
import {
  getPlaybookList,
  deletePlaybookInReference,
  addNewPlaybookInReference,
  editPlaybookInReference,
  getPlayBookDetail,
  PlaybookT,
} from '../../../../services/playbook.services';
import { getOpportunityTypes } from '../../../../services/references.services';
import { useEffectOnlyOnce, useTableHeadSorter } from '../../../../hooks';
// import { IconWarning } from '../../../../assets';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
// import { nonWhiteSpace } from 'html2canvas/dist/types/css/syntax/parser';
// import CorporateObjective from '../CorporateObjective';
import moment from 'moment';
import {
  CustomSelect,
  TableHeadSorter,
  HeadCellProps,
  ESLoader,
} from '../../../../components';
import { OpportunityTable } from './components/OpportunityTable';
import { getComparator, stableSort } from '../../../../commons/util';
import { Pagination } from '@material-ui/lab';
import { IQueryParam } from '../../../../services/common.services';

// import {
//   DragDropContext,
//   Draggable,
//   DragStart,
//   Droppable,
//   DropResult,
// } from 'react-beautiful-dnd';

interface HeadCell {
  id: keyof IData;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}
interface OpportunityType {
  id: number;
  name: string;
  archive?: boolean;
}
interface IData {
  id?: number;
  name: string;
  description?: string;
  opportunityType?: OpportunityType;
  opportunity_count?: number;
  author?: string;
  updated_date: string;
  points?: number;
  archive?: number;
  status?: string;
  action?: any;
}
const PAGE_SIZE = 10;
const status = [
  { id: 1, name: 'Draft' },
  { id: 2, name: 'Active' },
  { id: 3, name: 'Archive' },
];
const tableFields = [
  'id',
  'name',
  'description',
  'opportunity_type.name',
  'author',
  'opportunity_count',
  'updated_date',
  'archive',
  'action',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: 'PB#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Playbook name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'opportunity_type.name',
    label: 'Opportunity type',
    disablePadding: true,
  },
  {
    id: 'author',
    label: 'Author',
    disablePadding: true,
  },
  {
    id: 'opportunity_count',
    label: 'Used counter',
    disablePadding: true,
  },
  {
    id: 'updated_date',
    label: 'Last updated',
    disablePadding: true,
  },
  {
    id: 'archive',
    label: 'Status',
    disablePadding: true,
  },
  {
    id: 'action',
    label: 'Action',
    disablePadding: true,
    disableSorting: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  number: {
    '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
  },
  button: {
    display: 'none',
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 16,
  },
  archive: {
    width: 50,
  },
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  shadowBox: {
    border: '1px solid #ccc',
    boxShadow: '5px 8px #eee',
    borderRadius: 5,
    maxWidth: '90%',
  },
}));
// function EnhancedTableHead(props: EnhancedTableProps) {
//   const { classes, order, orderBy } = props;
//   const createSortHandler = (property: keyof IData) => (
//     event: React.MouseEvent<unknown>,
//   ) => {
//     //onRequestSort(event, property);
//   };

//   return (
//     <TableHead>
//       <TableRow>
//         {headCells.map(headCell => (
//           <TableCell
//             key={headCell.id}
//             sortDirection={orderBy === headCell.id ? order : false}
//           >
//             <TableSortLabel
//               active={orderBy === headCell.id}
//               direction={orderBy === headCell.id ? order : 'asc'}
//               onClick={createSortHandler(headCell.id)}
//             >
//               {headCell.label}
//               {orderBy === headCell.id ? (
//                 <span className={classes.visuallyHidden}>
//                   {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
//                 </span>
//               ) : null}
//             </TableSortLabel>
//           </TableCell>
//         ))}
//         <TableCell align="center">Status</TableCell>
//         <TableCell align="center">Action</TableCell>
//       </TableRow>
//     </TableHead>
//   );
// }

export interface children {
  id?: number;
  title: string;
  name: string;
  description: string;
  is_deleted: boolean;
  playbook_item_parent_id?: number;
  item_order: number;
  children: children[];
}
export interface ItemProps {
  title: string;
  detail: string;
  children: ItemProps[];
}
export interface InitValueT {
  playbook_items: children[];
  isOpenCollapse: number | undefined;
  // opportunityTypes: OpportunityType[];
  opportunity_type: OpportunityType;
  isOpenCollapseItemLevel2: string | undefined;
  isOpenCollapseItemLevel3: string | undefined;
  playbookName: string;
  playbookDetail: string;
  archive?: number;
}

const PlayBooks: React.FC = () => {
  const classes = useStyles();
  // const [order, setOrder] = useState<Order>('desc');
  // const [orderBy, setOrderBy] = useState<keyof IData>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [playBookDeleteId, setPlayBookDeleteId] = useState<number>();
  const [playBookEditId, setPlayBookEditId] = useState<number>(0);
  const [playBookIndex, setPlayBookIndex] = useState<number>(0);
  const [opportunityDialog, setOpportunityDialog] = useState<boolean>(false);
  const [opportunityTypes, setOpportunityTypes] = useState<OpportunityType[]>(
    [],
  );
  const [playbooks, setPlayBooks] = useState<PlaybookT[]>([]);
  const [raw, setRaw] = useState<PlaybookT[]>([]);

  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    '',
  );

  useEffectOnlyOnce(() => {
    Promise.all([getData(), getAllOpportunityTypes()]).then(() => {
      setLoading(false);
    });
  });

  useEffect(() => {
    handleRawData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [order, orderBy, page, raw]);

  // useEffect(() => {
  //   let list = playbooks;
  //   if (list.length > PAGE_SIZE) {
  //     list = list.slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE);
  //     console.log(list);
  //   }
  //   setPlayBooks(list);
  // }, [page, playbooks]);

  const handleRawData = useCallback(() => {
    let list = raw;
    setTotal(list.length);
    if (list.length > PAGE_SIZE) {
      list = list.slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE);
    }
    if (orderBy !== '') {
      list = stableSort(list as any[], getComparator(order, orderBy)) as any[];
    }
    setPlayBooks(list);
  }, [order, orderBy, page, raw]);

  // const [confirmOpen, setConfirmOpen] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState(false);
  // const [opportunityTypeToDeleteId, setOpportunityTypeToDeleteId] = useState<
  //   number
  // >();
  // const [confirmOpen, setConfirmOpen] = useState<boolean>(false);
  // const [confirmTitle, setConfirmTitle] = useState<string>('');

  const { i18n } = useTranslation();

  // const handleRequestSort = (
  //   event: React.MouseEvent<unknown>,
  //   property: keyof IData,
  // ) => {
  //   console.log(property);
  //   const isAsc = orderBy === property && order === 'asc';
  //   setOrder(isAsc ? 'desc' : 'asc');
  //   setOrderBy(property);
  // };
  const handleRemovePlayBook = async (id: number) => {
    const response = await deletePlaybookInReference(id);
    if (response.success) {
      let newPlaybookList = playbooks.filter(e => {
        return e.id !== id;
      });
      setPlayBooks(newPlaybookList);
    }
    setOpenDeleteDialog(false);
  };
  const handleSubmit = async (value: InitValueT, playBookId: number) => {
    if (playBookId === 0) {
      value.playbook_items?.forEach(e => {
        delete e.id;
        delete e.item_order;
        delete e.playbook_item_parent_id;
        delete e.is_deleted;
        e.children?.forEach(children => {
          delete children.id;
          delete children.item_order;
          delete children.playbook_item_parent_id;
          delete children.is_deleted;
          children.children?.forEach(child => {
            delete child.id;
            delete child.item_order;
            delete child.playbook_item_parent_id;
            delete child.is_deleted;
          });
        });
      });
      const payload = {
        name: value.playbookName,
        description: value.playbookDetail ? value.playbookDetail : '',
        playbook_items: value.playbook_items,
        opportunity_type_id: value.opportunity_type,
        archive: value.archive,
      };
      const response = await addNewPlaybookInReference(payload);
      if (response.success) {
        const getPlaybookTemplates = await getPlaybookList();
        if (getPlaybookTemplates.data.list) {
          setRaw(getPlaybookTemplates.data.list ?? []);
        }
      }
      setOpenDialog(false);
    } else {
      const payload = {
        name: value.playbookName,
        description: value.playbookDetail ? value.playbookDetail : '',
        playbook_items: value.playbook_items,
        opportunity_type_id: value.opportunity_type,
        archive: value.archive,
      };
      const response = await editPlaybookInReference(payload, playBookId);
      if (response.success) {
        const getPlaybookTemplates = await getPlaybookList();
        if (getPlaybookTemplates.data.list) {
          setRaw(getPlaybookTemplates.data.list);
        }
      }
      setOpenDialog(false);
    }
  };
  const initialValues: InitValueT = {
    playbook_items: [],
    isOpenCollapse: undefined,

    opportunity_type: { id: 0, name: '' },
    isOpenCollapseItemLevel2: undefined,
    isOpenCollapseItemLevel3: undefined,
    playbookName: '',
    playbookDetail: '',
    archive: 2,
  };
  // const grid = 8;
  // const getItemStyle = (draggableStyle: any, isDragging: boolean) => ({
  //   // some basic styles to make the items look a bit nicer
  //   userSelect: 'none',
  //   padding: grid * 2,
  //   marginBottom: grid,

  //   // change background colour if dragging
  //   background: isDragging ? 'lightgreen' : 'grey',

  //   // styles we need to apply on draggables
  //   ...draggableStyle,
  // });
  // const getListStyle = (isDraggingOver: boolean) => ({
  //   background: isDraggingOver ? 'lightblue' : 'lightgrey',
  //   padding: grid,
  //   width: 250,
  // });

  // function onDragEnd(result: DropResult) {
  //   // const { source, destination } = result;
  //   // setDraggingState(undefined);
  //   // setDropDisabled([]);
  //   // if (!destination || source.droppableId === destination.droppableId) return;
  //   // const taskID = +result.draggableId;
  //   // if (!taskID) return;
  //   // onStateUpdated?.(taskID, destination.droppableId as TaskState);
  // }

  // const onDragStart = useCallback((start: DragStart) => {
  // console.log('Dragging item: ', start);
  // setDraggingState(start.source.droppableId as TaskState);
  // if (disabledDragIDs.includes(+start.draggableId)) {
  //   setDropDisabled([
  //     TaskState.Assigned,
  //     TaskState.InProgress,
  //     TaskState.Completed,
  //     TaskState.Cancelled,
  //     TaskState.Timeout,
  //   ]);
  // } else {
  //   setDropDisabled([TaskState.Timeout]);
  // }
  // }, []);

  // const handleConfirmDelete = (item: ICorporateObjective) => {};
  // const editOpportunityTypeDiaLog = (item: ICorporateObjective) => {};

  // const handleRemovePlayBook = async (id: number) => {};
  const onChangePage = useCallback((e: object, newPage: number) => {
    setPage(newPage);
  }, []);

  const getData = useCallback(async () => {
    let params: IQueryParam[] = [
      { key: 'page', value: page },
      { key: 'limit', value: PAGE_SIZE },
    ];
    const getPlaybookTemplates = await getPlaybookList(params);
    if (getPlaybookTemplates.data.list) {
      setRaw(getPlaybookTemplates.data.list);
      setTotal(getPlaybookTemplates.data.total);
    }
  }, [page]);

  const getAllOpportunityTypes = useCallback(async () => {
    const opportunityTypes = await getOpportunityTypes();
    if (opportunityTypes.success) {
      setOpportunityTypes(opportunityTypes.data.list);
    }
  }, []);

  const numberOfPages = Math.ceil(total / PAGE_SIZE);
  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={values => {
        handleSubmit(values, playBookEditId);
      }}
    >
      {({
        values,
        handleChange,
        setFieldValue,
        resetForm,
        handleSubmit,
        isValid,
        dirty,
        handleBlur,
      }) => {
        const getOnePlayBook = async (id: number) => {
          setIsLoading(true);
          let parentIndex: number;
          let newPlayBook = [...playbooks];
          const playBookDetail = await getPlayBookDetail(id);
          if (playBookDetail.success) {
            setIsLoading(false);
            playBookDetail.data.playbook_items?.map(item => {
              item.is_deleted = false;
            });
            let indexPlayBook = newPlayBook.findIndex(e => e.id === id);
            newPlayBook[indexPlayBook].playbook_items =
              playBookDetail.data.playbook_items;
          }

          let data = newPlayBook.filter(e => {
            return e.id === id;
          });
          for (let i = 0; i < data[0].playbook_items?.length; i++) {
            parentIndex = data[0].playbook_items?.findIndex(
              item =>
                item.id === data[0].playbook_items[i].playbook_item_parent_id,
            );
            if (parentIndex !== -1) {
              if (!data[0].playbook_items[parentIndex].children?.length) {
                data[0].playbook_items[parentIndex].children = [
                  data[0].playbook_items[i],
                ];
              } else {
                data[0].playbook_items[parentIndex].children?.push(
                  data[0].playbook_items[i],
                );
              }
            }
          }

          let structurizedPlayBookItem = data[0].playbook_items?.filter(
            item => item.playbook_item_parent_id === 0,
          );
          setFieldValue('playbook_items', structurizedPlayBookItem);
          setFieldValue('opportunity_type', data[0].opportunity_type.id);
          setFieldValue('playbookName', data[0].name);
          setFieldValue('playbookDetail', data[0].description);
          setFieldValue('archive', data[0].archive);
        };
        return (
          <Grid container direction="column">
            <Box mt={3}>
              <Box display="flex" justifyContent="flex-end" mb={3}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    // if (!isAddingDialog) {
                    resetForm();
                    // }
                    setAddingDialog(true);
                    setOpenDialog(true);
                    setPlayBookEditId(0);
                  }}
                  startIcon={<AddCircleOutlineIcon />}
                >
                  Add
                </Button>
              </Box>
            </Box>
            <Grid container alignItems="center" justify="center">
              {loading ? (
                <ESLoader />
              ) : (
                <TableContainer>
                  <Table className={classes.table}>
                    <TableHeadSorter
                      onRequestSort={handleRequestSort}
                      order={order}
                      orderBy={orderBy}
                      cells={headCells}
                    />
                    <TableBody>
                      {playbooks.map((item: any, index: number) => (
                        <TableRow
                          key={index}
                          hover
                          // classes={{ root: classes.rowLink }}
                        >
                          <TableCell component="td" scope="row">
                            {item?.id}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {item?.name}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {item?.description}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {item?.opportunity_type?.name}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {item?.created_by_user?.name}
                          </TableCell>
                          <TableCell
                            classes={{ root: classes.rowLink }}
                            component="td"
                            scope="row"
                            onClick={() => {
                              setOpportunityDialog(true);
                              setPlayBookIndex(index);
                            }}
                          >
                            {item?.opportunity_count}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {moment(item?.updated_date).format(
                              'DD/MM/YYYY HH:mm:ss',
                            )}
                          </TableCell>
                          <TableCell
                            component="td"
                            scope="row"
                            padding="none"
                            // className={classes.archive}
                          >
                            <Box display="flex" justifyContent="center">
                              {item?.archive === 1
                                ? 'Draft'
                                : item?.archive === 2
                                ? 'Active'
                                : 'Archive'}
                            </Box>
                          </TableCell>
                          <TableCell component="td" scope="row" padding="none">
                            <TableAction
                              hiddenItem={['view']}
                              onClickEdit={() => {
                                resetForm();
                                setAddingDialog(false);
                                setOpenDialog(true);
                                setPlayBookEditId(item.id);
                                getOnePlayBook(item.id);
                              }}
                              onClickDelete={() => {
                                setOpenDeleteDialog(true);
                                setPlayBookDeleteId(item?.id);
                                // handleConfirmDelete(item);
                                // // setOpportunityTypeToDeleteId(item.id);
                              }}
                              children={
                                <IconButton
                                  onClick={() => {
                                    resetForm();
                                    setAddingDialog(true);
                                    setOpenDialog(true);
                                    getOnePlayBook(item.id);
                                    setPlayBookEditId(0);
                                  }}
                                >
                                  <FileCopyOutlinedIcon />
                                </IconButton>
                              }
                            />
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              )}

              {numberOfPages > 1 && (
                <Box display="flex" justifyContent="center" p={3}>
                  <Pagination
                    page={page}
                    count={numberOfPages}
                    variant="outlined"
                    shape="rounded"
                    onChange={onChangePage}
                  />
                </Box>
              )}
            </Grid>
            <FormDialog
              title="Playbook"
              submit={handleSubmit}
              open={openDialog}
              setOpen={setOpenDialog}
              isAddingForm={isAddingDialog}
              disable={
                values.playbookName === '' ||
                values.opportunity_type.id === 0 ||
                values.archive === undefined
              }
              maxWidth="md"
              formContent={
                isLoading ? (
                  <Box
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}
                  >
                    <CircularProgress size={50} color="primary" />
                  </Box>
                ) : (
                  <Box>
                    <Box mb={3} mt={3}>
                      <MixTitle title="Playbook name" isRequired />
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="playbookName"
                        id="playbookName"
                        autoComplete="playbookName"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.playbookName}
                        // error={values.touched.name && !!values.errors.name}
                        // helperText={
                        //   formik.touched.name ? formik.errors.name : ''
                        // }
                      />
                    </Box>
                    <Box mb={3} mt={3}>
                      <MixTitle title="Description" />
                      <TextField
                        fullWidth
                        multiline
                        rows={4}
                        variant="outlined"
                        name="playbookDetail"
                        id="playbookDetail"
                        autoComplete="playbookDetail"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.playbookDetail}
                        // error={
                        //   formik.touched.description &&
                        //   !!formik.errors.description
                        // }
                        // helperText={
                        //   formik.touched.description
                        //     ? formik.errors.description
                        //     : ''
                        // }
                      />
                    </Box>
                    <Box mb={1} mt={3}>
                      <MixTitle title="Opportunity Type" isRequired />
                      <CustomSelect
                        value={values.opportunity_type || ''}
                        onChange={handleChange}
                        id="opportunity_type"
                        name="opportunity_type"
                        options={
                          opportunityTypes
                            ?.filter(item => !item.archive)
                            .map(item => ({
                              id: item.id!,
                              name: item.name!,
                            })) || []
                        }
                        fullWidth
                      />
                    </Box>
                    <FieldArray name="playbook_items">
                      {arrayHelpers => (
                        <Box
                        // ref={provided.innerRef}
                        // style={getListStyle(snapshot.isDraggingOver)}
                        >
                          {values.playbook_items?.map((item, index) => {
                            if (item.is_deleted === false) {
                              const animatedStyle = {
                                marginRight: '5px',
                                transition: '0.3s all',
                                transform:
                                  values.isOpenCollapse === index
                                    ? 'rotate(90deg)'
                                    : 'rotate(0deg)',
                                '&:hover': {
                                  cursor: 'poiter',
                                },
                              };

                              return (
                                // <Draggable
                                //   key={index.toString()}
                                //   draggableId={index.toString()}
                                // >
                                //   {(provided, snapshot) => (
                                <Box
                                  mb={3}
                                  key={index}

                                  // ref={provided.innerRef}
                                  // style={getItemStyle(
                                  //   provided.draggableStyle,
                                  //   snapshot.isDragging,
                                  // )}
                                >
                                  <PlayBookItem
                                    animatedStyle={animatedStyle}
                                    isOpenCollapse={values.isOpenCollapse}
                                    openCollapseField={'isOpenCollapse'}
                                    conditionPopUp={index}
                                    item={values}
                                    subItem={values.playbook_items}
                                    index={index}
                                    indexPosition={index}
                                    itemName={`Step ${index + 1}`}
                                    textFieldNameTitle={`playbook_items.${index}.title`}
                                    textFieldNameDetail={`playbook_items.${index}.description`}
                                    handleChange={handleChange}
                                    setFieldValue={setFieldValue}
                                  />
                                  {/* subItem level 2 */}
                                  {values.playbook_items[index]?.children
                                    ?.length > 0 && (
                                    <Box>
                                      {values.playbook_items[
                                        index
                                      ]?.children?.map(
                                        (e: children, indexSub2) => {
                                          if (e.is_deleted === false) {
                                            const animatedStyleItemLevel2 = {
                                              marginRight: '5px',
                                              transition: '0.3s all',
                                              transform:
                                                values.isOpenCollapseItemLevel2 ===
                                                `${index + 1}.${indexSub2 + 1}`
                                                  ? 'rotate(90deg)'
                                                  : 'rotate(0deg)',
                                              '&:hover': {
                                                cursor: 'poiter',
                                              },
                                            };
                                            return (
                                              <Box>
                                                <PlayBookItem
                                                  animatedStyle={
                                                    animatedStyleItemLevel2
                                                  }
                                                  isOpenCollapse={
                                                    values.isOpenCollapseItemLevel2
                                                  }
                                                  openCollapseField={
                                                    'isOpenCollapseItemLevel2'
                                                  }
                                                  conditionPopUp={`${index +
                                                    1}.${indexSub2 + 1}`}
                                                  item={values}
                                                  subItem={
                                                    values.playbook_items[index]
                                                      .children
                                                  }
                                                  index={indexSub2}
                                                  indexPosition={`${index +
                                                    1}.${indexSub2 + 1}`}
                                                  itemName={`Step ${index +
                                                    1}.${indexSub2 + 1}`}
                                                  textFieldNameTitle={`playbook_items.${index}.children.${indexSub2}.title`}
                                                  textFieldNameDetail={`playbook_items.${index}.children.${indexSub2}.description`}
                                                  marginLeft={7}
                                                  handleChange={handleChange}
                                                  setFieldValue={setFieldValue}
                                                />
                                                <Box>
                                                  {values.playbook_items[index]
                                                    ?.children[indexSub2]
                                                    .children?.length > 0 && (
                                                    <Box>
                                                      {values.playbook_items[
                                                        index
                                                      ]?.children[
                                                        indexSub2
                                                      ].children.map(
                                                        (
                                                          itemlv3,
                                                          indexSub3,
                                                        ) => {
                                                          if (
                                                            itemlv3.is_deleted ===
                                                            false
                                                          ) {
                                                            const animatedStyleItemLevel3 = {
                                                              marginRight:
                                                                '5px',
                                                              transition:
                                                                '0.3s all',
                                                              transform:
                                                                values.isOpenCollapseItemLevel3 ===
                                                                `${index +
                                                                  1}.${indexSub2 +
                                                                  1}.${indexSub3 +
                                                                  1}`
                                                                  ? 'rotate(90deg)'
                                                                  : 'rotate(0deg)',
                                                              '&:hover': {
                                                                cursor:
                                                                  'poiter',
                                                              },
                                                            };
                                                            return (
                                                              <PlayBookItem
                                                                animatedStyle={
                                                                  animatedStyleItemLevel3
                                                                }
                                                                isOpenCollapse={
                                                                  values.isOpenCollapseItemLevel3
                                                                }
                                                                openCollapseField={
                                                                  'isOpenCollapseItemLevel3'
                                                                }
                                                                conditionPopUp={`${index +
                                                                  1}.${indexSub2 +
                                                                  1}.${indexSub3 +
                                                                  1}`}
                                                                item={values}
                                                                subItem={
                                                                  values
                                                                    .playbook_items[
                                                                    index
                                                                  ]?.children[
                                                                    indexSub2
                                                                  ].children
                                                                }
                                                                index={
                                                                  indexSub3
                                                                }
                                                                indexPosition={`${index +
                                                                  1}.${indexSub2 +
                                                                  1}.${indexSub3 +
                                                                  1}`}
                                                                itemName={`Step ${index +
                                                                  1}.${indexSub2 +
                                                                  1}.${indexSub3 +
                                                                  1}`}
                                                                textFieldNameTitle={`playbook_items.${index}.children.${indexSub2}.children.${indexSub3}.title`}
                                                                textFieldNameDetail={`playbook_items.${index}.children.${indexSub2}.children.${indexSub3}.description`}
                                                                marginLeft={12}
                                                                handleChange={
                                                                  handleChange
                                                                }
                                                                setFieldValue={
                                                                  setFieldValue
                                                                }
                                                              />
                                                            );
                                                          }
                                                        },
                                                      )}
                                                    </Box>
                                                  )}
                                                </Box>
                                              </Box>
                                            );
                                          }
                                        },
                                      )}
                                    </Box>
                                  )}
                                </Box>

                                //   )}
                                // </Draggable>
                              );
                            }
                          })}
                          <Button
                            variant="outlined"
                            color="primary"
                            startIcon={<ControlPointIcon />}
                            onClick={() => {
                              arrayHelpers.push({
                                title: '',
                                name: '',
                                description: '',
                                is_deleted: false,
                                playbook_item_parent_id: 0,
                                item_order: 0,
                                children: [],
                              });
                            }}
                          >
                            Add step
                          </Button>
                        </Box>
                      )}
                    </FieldArray>

                    <Box
                      mt={3}
                      mb={3}
                      component="span"
                      display="flex"
                      alignItems="center"
                    >
                      <MixTitle title="Status" isRequired />
                      <Box width="150px" ml={2}>
                        <FormControl
                          fullWidth
                          margin="normal"
                          variant="outlined"
                        >
                          <CustomSelect
                            fullWidth
                            value={values.archive || ''}
                            onChange={handleChange}
                            id="archive"
                            name="archive"
                            options={
                              status?.map(item => ({
                                id: item.id!,
                                name: item.name!,
                              })) || []
                            }
                          />
                        </FormControl>
                      </Box>
                    </Box>
                  </Box>
                )
              }
            />
            <DeleteDialog
              isOpen={isOpenDeleteDialog}
              header="Delete Playbook"
              message={
                <Box>
                  <Typography component="p" variant="body1">
                    {i18n.t('delete_playbook')}
                  </Typography>
                </Box>
              }
              handleClose={() => setOpenDeleteDialog(false)}
              handleDelete={() => {
                if (playBookDeleteId) {
                  handleRemovePlayBook(playBookDeleteId);
                }
              }}
            />
            <Dialog
              open={opportunityDialog}
              onClose={() => {
                setOpportunityDialog(false);
              }}
              maxWidth={'md'}
              fullWidth
            >
              <Box padding={4}>
                <Box textAlign="center" mb={2}>
                  <Typography component="p" variant="h4">
                    Opportunities
                  </Typography>
                </Box>
                <OpportunityTable
                  data={playbooks[playBookIndex]?.opportunity ?? []}
                  toggleModal={() => setOpportunityDialog(false)}
                />
              </Box>
            </Dialog>
          </Grid>
        );
      }}
    </Formik>
  );
};
export default PlayBooks;
