import React, { useState } from 'react';
import { Box, Typography, TextField } from '@material-ui/core';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles } from '@material-ui/core/styles';
import { InitValueT, children } from '../index';
import { CustomContextMenu, MessageDialog } from '../../../../../components';
import { useTranslation } from 'react-i18next';
interface PlayBookItemProps {
  animatedStyle: {
    marginRight: string;
    transition: string;
    transform: string;
    '&:hover': { cursor: string };
  };
  isOpenCollapse: number | string | undefined;
  openCollapseField: string;
  conditionPopUp: string | number;
  item: InitValueT;
  index: number;
  indexPosition: string | number;
  itemName: String;
  subItem: children[];
  textFieldNameTitle: String;
  textFieldNameDetail: String;
  handleChange: {
    (e: React.ChangeEvent<any>): void;
    <T = string | React.ChangeEvent<any>>(
      field: T,
    ): T extends React.ChangeEvent<any>
      ? void
      : (e: string | React.ChangeEvent<any>) => void;
  };
  setFieldValue: (
    field: string,
    value: any,
    shouldValidate?: boolean | undefined,
  ) => void;
  marginLeft?: number;
}
const useStyles = makeStyles(theme => ({
  shadowBox: {
    border: '1px solid #ccc',
    boxShadow: '5px 8px #eee',
    borderRadius: 5,
    maxWidth: '90%',
  },
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));
const PlayBookItem: React.FC<PlayBookItemProps> = props => {
  const classes = useStyles();
  const { i18n } = useTranslation();
  const [success, setSuccess] = useState(false);
  const {
    animatedStyle,
    isOpenCollapse,
    conditionPopUp,
    item,
    subItem,
    index,
    indexPosition,
    itemName,
    textFieldNameTitle,
    textFieldNameDetail,
    openCollapseField,
    handleChange,
    setFieldValue,
    marginLeft,
  } = props;
  return (
    <Box
      mt={2}
      className={classes.shadowBox}
      p={2}
      ml={marginLeft}
      key={indexPosition}
    >
      <Box display="flex" alignItems="center">
        <Box width="70px">
          <Typography component="span" variant="caption">
            {itemName}
          </Typography>
        </Box>
        <Box ml={1} display="flex" alignItems="center">
          <ArrowForwardIosIcon
            style={animatedStyle}
            className={classes.icon}
            fontSize="small"
            onClick={() => {
              if (isOpenCollapse === indexPosition) {
                setFieldValue(openCollapseField, undefined);
              } else {
                setFieldValue(openCollapseField, indexPosition);
              }
            }}
          />
        </Box>

        <Box mr={1} display="flex">
          <Typography component="span" variant="caption">
            Title:
          </Typography>
        </Box>

        <TextField
          variant="outlined"
          fullWidth
          name={`${textFieldNameTitle}`}
          value={subItem[index].title ?? subItem[index].name}
          onChange={handleChange}
        />
        <Box>
          {openCollapseField !== 'isOpenCollapseItemLevel3' ? (
            <CustomContextMenu
              options={[
                {
                  name: 'Delete',
                  onClick: () => {
                    // subItem.splice(index, 1);
                    let unDeletedItem = subItem[index].children?.filter(e => {
                      return e.is_deleted === false;
                    });
                    if (unDeletedItem?.length > 0) {
                      setSuccess(true);
                    } else {
                      subItem[index].is_deleted = true;
                      setFieldValue('playbook_items', item.playbook_items);
                    }
                  },
                },
                {
                  name: 'Add Sub Step',
                  onClick: () => {
                    if (!subItem[index].children) subItem[index].children = [];
                    subItem[index].children.push({
                      title: '',
                      name: '',
                      description: '',
                      is_deleted: false,
                      item_order:
                        openCollapseField === 'isOpenCollapse' ? 1 : 2,
                      children: [],
                    });
                    setFieldValue('playbook_items', item.playbook_items);
                  },
                },
              ]}
              size={'small'}
            />
          ) : (
            <CustomContextMenu
              options={[
                {
                  name: 'Delete',
                  onClick: () => {
                    subItem[index].is_deleted = true;
                    setFieldValue('playbook_items', item.playbook_items);
                  },
                },
              ]}
              size={'small'}
            />
          )}
        </Box>
      </Box>
      {isOpenCollapse === conditionPopUp && (
        <Box pl={9} mt={1}>
          <Box>
            <Box mb={1}>
              <Typography component="p" variant="caption">
                Detail:
              </Typography>
            </Box>
            <TextField
              fullWidth
              multiline
              rows={5}
              name={`${textFieldNameDetail}`}
              onChange={handleChange}
              variant="outlined"
              value={subItem[index].description ?? ''}
            />
          </Box>
        </Box>
      )}
      <MessageDialog
        mode="warning"
        message={i18n.t('playbook_warning')}
        open={success}
        onClose={() => setSuccess(false)}
      />
    </Box>
  );
};

export default PlayBookItem;
