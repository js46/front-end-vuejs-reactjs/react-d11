import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Box,
  Button,
} from '@material-ui/core';
import {
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from '../../../../../components';
// import { IOpportunity } from '../../../../../services/opportunity.services';
import { useHistory } from 'react-router-dom';
import { useTableHeadSorter } from '../../../../../hooks';
import moment from 'moment';
const useStyles = makeStyles(theme => ({
  selectBox: {
    height: '42px!important',
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '0px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
        fontSize: '14px!important',
        fontFamily: 'Helvetica Neue,Arial',
      },
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  roundedBorder: {
    borderRadius: 6,
    '& .MuiOutlinedInput-root': {
      borderRadius: 6,
    },
    '& .MuiOutlinedInput-input': {
      padding: '10px 14px',
    },
    height: '36px',
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));
const tableFields = [
  'id',
  'title.keyword',
  'opp_priority_score.priority_level.keyword',
  'opp_priority_score.priority_score',
  'opportunity_phase.display_name.keyword',
  'created_by.name.keyword',
  'assignees',
  'created_at',
  'time',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: 'Opp#',
    disablePadding: true,
  },
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'opportunity_phase.display_name.keyword',
    label: 'Phase',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Created by',
    disablePadding: true,
  },
  {
    id: 'time',
    label: 'Last updated',
    disablePadding: true,
  },
];
interface Opportunity {
  id: number;
  title: string;
  opportunity_phase: {
    id: number;
    name: string;
    display_name: string;
  };
  date: string;
  priority_score: number;
  priority_level: string;
  created_by_user: { id: number; name: string };
}
interface OpportunityTableProps {
  data: Opportunity[];
  toggleModal: () => void;
}
export const OpportunityTable: React.FC<OpportunityTableProps> = props => {
  const { data, toggleModal } = props;
  const classes = useStyles();
  let history = useHistory();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'time',
    SortOrder.DESC,
  );
  const handleClick = (onClickOpp: Opportunity) => {
    history.push(`/opportunity/${onClickOpp.id}`);
  };
  // const getAssigneeNames = useCallback((item: Opportunity) => {
  //   let names: string[] = [];
  //   if (item.current_expert_review?.expert_review?.name) {
  //     names.push(item.current_expert_review.expert_review.name);
  //   }
  //   if (item.current_experts) {
  //     names = [...names, ...item.current_experts.map(item => item.name)];
  //   }
  //   return names.join(', ');
  // }, []);
  return (
    <Box>
      <TableContainer>
        <Table style={{ minWidth: 850 }}>
          <TableHeadSorter
            onRequestSort={handleRequestSort}
            order={order}
            orderBy={orderBy}
            cells={headCells}
          />
          <TableBody>
            {data?.map((item: Opportunity) => (
              <TableRow
                key={item.id}
                hover
                onClick={() => handleClick(item)}
                classes={{ root: classes.rowLink }}
              >
                <TableCell component="th" scope="row">
                  {item?.id}
                </TableCell>
                <TableCell component="th" scope="row">
                  {item?.title}
                </TableCell>

                <TableCell component="th" scope="row">
                  {item?.opportunity_phase?.display_name}
                </TableCell>
                <TableCell component="th" scope="row">
                  {item?.created_by_user?.name}
                </TableCell>

                <TableCell component="th" scope="row">
                  {moment(item.date).format('DD/MM/YYYY HH:mm:ss')}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Box pb={2} pt={2} display="flex" justifyContent="center">
        <Button onClick={toggleModal} color="primary" variant="contained">
          OK
        </Button>
      </Box>
      {/* <Box display="flex" justifyContent="center" p={3}>
        <Pagination
          page={1}
          count={2}
          variant="contained"
          shape="rounded"
          color="secondary"
          // onChange={handleChangePage}
        />
      </Box> */}
    </Box>
  );
};
