import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Box,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  getOppSuccessProbability,
  IOpportunitySuccessProbability,
  deleteOppSuccessProbability,
  addOppSuccessProbability,
  updateOppSuccessProbability,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
import {
  DeleteDialog,
  AddDialog,
  EditDialog,
  MixTitle,
  TableAction,
} from '../../../../../../components';
import { useTranslation } from 'react-i18next';

interface Data {
  id: string;
  band: string;
  points: number;
  description: string;
}
interface HeadCell {
  id: keyof Data;
  band: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    band: 'CL#',
    disablePadding: true,
  },
  {
    id: 'band',
    band: 'Band',
    disablePadding: true,
  },
  {
    id: 'points',
    band: 'Points',
    disablePadding: true,
  },
  {
    id: 'description',
    band: 'Description',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr th:last-child': {
      width: 120,
    },
    '& tr th:first-child': {
      width: 50,
    },
  },
}));

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.band}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

const ConfidenceLevel: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [confidenceLevel, setConfidenceLevel] = useState<
    IOpportunitySuccessProbability[]
  >([]);
  const [band, setBand] = useState<string>('');
  const [points, setPoints] = useState<number>(0);
  const [description, setDescription] = useState<string>('');
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isAddDeleteDialog, setOpenAddDialog] = useState(false);
  const [isOpenEditDialog, setOpenEditDialog] = useState(false);
  const [oppToDeleteId, setOppToDeleteId] = useState<number>();
  const [confidenceLevelToEditItem, setConfidenceLevelToEditItem] = useState<
    IOpportunitySuccessProbability
  >();
  const { t } = useTranslation();
  let disableAddButton = band === undefined || band === '';

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      await getDataOppSuccessProbability();
    };
    getData();
  });

  const getDataOppSuccessProbability = async () => {
    const response = await getOppSuccessProbability();
    setConfidenceLevel(response.data.list);
  };

  const handleRemoveOpp = async (id: number) => {
    await deleteOppSuccessProbability(id).then(({ data }) => {
      let newOppSuccessProbability = confidenceLevel.filter(function(opp) {
        return opp.id !== data.id;
      });
      setConfidenceLevel(newOppSuccessProbability);
      setOpenDeleteDialog(false);
    });
  };

  const handleEditOpp = async () => {
    if (!confidenceLevelToEditItem) {
      //
    } else {
      await updateOppSuccessProbability(confidenceLevelToEditItem.id, {
        band: band,
        points: points,
        description: description,
      }).then(({ data }) => {
        let confidenceLevelEdit = confidenceLevel.filter(function(band) {
          return band.id !== data.id;
        });
        setConfidenceLevel([data, ...confidenceLevelEdit]);
        setOpenEditDialog(false);
        setPoints(0);
        setBand('');
        setDescription('');
      });
    }
  };

  const handleAddOppSuccessProbability = async () => {
    await addOppSuccessProbability({
      band: band,
      points: points,
      description: description,
    }).then(({ data }) => {
      setConfidenceLevel([data, ...confidenceLevel]);
      setOpenAddDialog(false);
      setPoints(0);
      setBand('');
      setDescription('');
    });
  };

  const setConfidenceLevelToEdit = (item: IOpportunitySuccessProbability) => {
    setBand(item.band);
    setPoints(item.points);
    setDescription(item.description);
  };

  const handleChangeBand = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setBand(event.target.value);
  };

  const handleChangePriorityDescription = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setDescription(event.target.value);
  };

  const handleChangePoints = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setPoints(Number(event.target.value.replace(/[^0-9]/gm, '')));
  };

  return (
    <Box>
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            startIcon={<AddCircleOutlineIcon />}
            onClick={() => {
              setOpenAddDialog(true);
            }}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {confidenceLevel?.map(item => {
                return (
                  <TableRow
                    key={item.id}
                    hover
                    classes={{ root: classes.rowLink }}
                  >
                    <TableCell component="th" scope="row">
                      {item.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.band}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.points}
                    </TableCell>
                    <TableCell>{item.description}</TableCell>
                    <TableCell component="th" scope="row" padding="none">
                      <TableAction
                        hiddenItem={['view']}
                        onClickEdit={() => {
                          setOpenEditDialog(true);
                          setConfidenceLevelToEditItem(item);
                          setConfidenceLevelToEdit(item);
                        }}
                        onClickDelete={() => {
                          setOpenDeleteDialog(true);
                          setOppToDeleteId(item.id);
                        }}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>

      <AddDialog
        isOpen={isAddDeleteDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('confidence_level')}
                </Typography>
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Band" isRequired />
                <TextField
                  id="band"
                  name="band"
                  fullWidth
                  variant="outlined"
                  value={band}
                  onChange={handleChangeBand}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Points" />
                <TextField
                  id="points"
                  name="points"
                  fullWidth
                  type="text"
                  variant="outlined"
                  value={points}
                  onChange={handleChangePoints}
                />
              </Box>
              <Box mb={3} mt={3}>
                <MixTitle title="Description" />
                <TextField
                  id="description"
                  name="description"
                  rows={6}
                  multiline
                  fullWidth
                  variant="outlined"
                  value={description}
                  onChange={handleChangePriorityDescription}
                />
              </Box>
            </Box>
          </Box>
        }
        handleClose={() => {
          setOpenAddDialog(false);
          setPoints(0);
          setBand('');
          setDescription('');
        }}
        handleAdd={() => {
          handleAddOppSuccessProbability();
        }}
        disable={disableAddButton}
      />

      <EditDialog
        isOpen={isOpenEditDialog}
        message={
          <Box>
            <Box mb={3}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {t('confidence_level')}
                </Typography>
              </Box>
              <Box mb={3}>
                <MixTitle title="Band" isRequired />
                <TextField
                  id="band"
                  name="band"
                  fullWidth
                  variant="outlined"
                  value={band}
                  onChange={handleChangeBand}
                />
              </Box>
              <Box mb={3}>
                <MixTitle title="Points" />
                <TextField
                  id="points"
                  name="points"
                  fullWidth
                  type="text"
                  variant="outlined"
                  value={points}
                  onChange={handleChangePoints}
                />
              </Box>
              <Box mb={3}>
                <MixTitle title="Description" />
                <TextField
                  id="description"
                  name="description"
                  fullWidth
                  rows={6}
                  multiline
                  variant="outlined"
                  value={description}
                  onChange={handleChangePriorityDescription}
                />
              </Box>
            </Box>
          </Box>
        }
        handleClose={() => {
          setOpenEditDialog(false);
          setPoints(0);
          setBand('');
          setDescription('');
        }}
        handleSubmit={() => {
          handleEditOpp();
        }}
        // disable={disableEditButton}
      />

      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Opportunity Success Probability"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_confidence_level')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (oppToDeleteId) {
            handleRemoveOpp(oppToDeleteId);
          }
        }}
      />
    </Box>
  );
};
export default ConfidenceLevel;
