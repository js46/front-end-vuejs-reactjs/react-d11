import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableAction,
} from '../../../../../../components';
import {
  IOpportunitySizes,
  getOpportunitySizes,
  addOpportunitySize,
  updateOpportunitySize,
  deleteOpportunitySize,
} from '../../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../../hooks';
interface Data {
  id?: number;
  t_shirt_size?: string;
  duration?: number;
  points?: number;
  weighting?: number;
  action?: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}
const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'OS#',
    disablePadding: true,
  },
  {
    id: 't_shirt_size',
    label: 'Opp shirt size',
    disablePadding: true,
  },
  {
    id: 'duration',
    label: 'Duration',
    disablePadding: true,
  },
  {
    id: 'points',
    label: 'Points',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}
const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  number: {
    '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}
interface IDataForm {
  id?: number;
  t_shirt_size?: string;
  duration?: number;
  points?: number;
  weighting?: number;
}
const OpportunitySize: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [opportunitySizes, setOpportunitySizes] = useState<IOpportunitySizes[]>(
    [],
  );
  const { i18n } = useTranslation();
  const [oppToDeleteId, setOppToDeleteId] = useState<number>();
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const handleSubmit = (item: IDataForm) => {
    if (!item.id) {
      addOpportunitySize(item).then(({ data }) => {
        setOpportunitySizes([data, ...opportunitySizes]);
        setOpenDialog(false);
      });
    } else {
      updateOpportunitySize(item.id, item).then(({ data }) => {
        let opportunitySizeEdit = opportunitySizes.filter(function(
          opportunitySize,
        ) {
          return opportunitySize.id !== data.id;
        });
        setOpportunitySizes([data, ...opportunitySizeEdit]);
        setOpenDialog(false);
      });
    }
  };

  const initialDataForm: IDataForm = {
    t_shirt_size: '',
  };
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      t_shirt_size: Yup.string().required(
        i18n.t('reference_data.opportunity_size.shirt_size_required'),
      ),
      duration: Yup.number()
        .min(1)
        .integer()
        .required(i18n.t('reference_data.opportunity_size.duration_required')),
      points: Yup.number()
        .required(i18n.t('reference_data.point_required'))
        .max(5, i18n.t('reference_data.range_point'))
        .min(1, i18n.t('reference_data.range_point')),
    }),
    onSubmit: (values, { setSubmitting, setErrors, resetForm }) => {
      handleSubmit(values);
      resetForm({});
    },
  });
  const editBusinessUnitDiaLog = (item: IOpportunitySizes) => {
    if (item && item.id !== formik.values.id) {
      formik.setValues({
        id: item?.id,
        t_shirt_size: item?.t_shirt_size,
        duration: item?.duration,
        points: item?.points,
        weighting: item?.weighting,
      });
    }

    setOpenDialog(true);
  };
  const handleRemove = async (id: number) => {
    await deleteOpportunitySize(id).then(({ data }) => {
      let newOpportunitySize = opportunitySizes.filter(function(
        opportunitySize,
      ) {
        return opportunitySize.id !== data.id;
      });
      setOpportunitySizes(newOpportunitySize);
      setOpenDeleteDialog(false);
    });
  };
  useEffectOnlyOnce(() => {
    const getData = async () => {
      const opportunitySizesRes = await getOpportunitySizes();
      if (opportunitySizesRes.data.list) {
        setOpportunitySizes(opportunitySizesRes.data.list);
      }
    };
    getData();
  });

  return (
    <Grid container direction="column">
      <Box mt={3}>
        <Box display="flex" justifyContent="flex-end" mb={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (!isAddingDialog) {
                formik.resetForm();
              }
              setAddingDialog(true);
              setOpenDialog(true);
            }}
            startIcon={<AddCircleOutlineIcon />}
          >
            Add
          </Button>
        </Box>
      </Box>
      <Grid container alignItems="center" justify="center">
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {opportunitySizes.map((item: any, index: number) => (
                <TableRow key={index} hover classes={{ root: classes.rowLink }}>
                  <TableCell component="td" scope="row">
                    {item?.id}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.t_shirt_size}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.duration}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item?.points}
                  </TableCell>

                  <TableCell component="td" scope="row" padding="none">
                    <TableAction
                      hiddenItem={['view']}
                      onClickEdit={() => {
                        setAddingDialog(false);
                        editBusinessUnitDiaLog(item);
                      }}
                      onClickDelete={() => {
                        setOpenDeleteDialog(true);
                        setOppToDeleteId(item.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <FormDialog
        title="Opportunity Size"
        submit={formik.handleSubmit}
        open={openDialog}
        setOpen={setOpenDialog}
        isAddingForm={isAddingDialog}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            {' '}
            <Box mb={3} mt={3}>
              <MixTitle title="Opportunity Shirt Size" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="t_shirt_size"
                id="t_shirt_size"
                autoComplete="t_shirt_size"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.t_shirt_size}
                error={
                  formik.touched.t_shirt_size && !!formik.errors.t_shirt_size
                }
                helperText={
                  formik.touched.t_shirt_size ? formik.errors.t_shirt_size : ''
                }
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Duration" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="duration"
                id="duration"
                type="number"
                className={classes.number}
                autoComplete="duration"
                onChange={e => {
                  e.preventDefault();
                  const { value } = e.target;
                  const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                  if (regex.test(value.toString()) || value === '') {
                    formik.setFieldValue(
                      'duration',
                      value ? Number(value) : '',
                    );
                  }
                }}
                onBlur={formik.handleBlur}
                value={formik.values.duration}
                error={formik.touched.duration && !!formik.errors.duration}
                helperText={
                  formik.touched.duration ? formik.errors.duration : ''
                }
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Point" />
              <TextField
                fullWidth
                variant="outlined"
                name="points"
                id="points"
                type="number"
                className={classes.number}
                InputLabelProps={{
                  shrink: true,
                }}
                autoComplete="points"
                onChange={e => {
                  e.preventDefault();
                  const { value } = e.target;
                  const regex = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/;
                  if (regex.test(value.toString()) || value === '') {
                    formik.setFieldValue('points', value ? Number(value) : '');
                  }
                }}
                onBlur={formik.handleBlur}
                value={formik.values.points}
                error={formik.touched.points && !!formik.errors.points}
                helperText={formik.touched.points ? formik.errors.points : ''}
              />
            </Box>
          </Box>
        }
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Opportunity Size"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('delete_opp_size')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (oppToDeleteId) {
            handleRemove(oppToDeleteId);
          }
        }}
      />
    </Grid>
  );
};
export default OpportunitySize;
