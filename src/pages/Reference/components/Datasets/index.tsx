import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Grid,
  Box,
  Button,
  Typography,
  TextField,
  Dialog,
} from '@material-ui/core';
import { Formik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useTranslation } from 'react-i18next';
import {
  DeleteDialog,
  MixTitle,
  FormDialog,
  TableAction,
  OpportunityTable,
} from '../../../../components';
import {
  getDataSets,
  IDataSet,
  addDataset,
  deleteDataset,
  updateDataset,
} from '../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../hooks';
interface HeadCell {
  id: keyof IData;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}
interface OpportunityType {
  id: number;
  name: string;
  archive?: boolean;
}
interface IData {
  id?: number;
  name: string;
  description?: string;
  current_opportunities?: string;
}
const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'DS#',
    disablePadding: true,
  },
  {
    id: 'name',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'description',
    label: 'Description',
    disablePadding: true,
  },
  {
    id: 'current_opportunities',
    label: 'Current opportunities #',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => void;
  order: Order;
  orderBy: string;
}
interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  number: {
    '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
  },
  button: {
    display: 'none',
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 120,
    },
    '& tr td:first-child': {
      width: 50,
    },
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  archive: {
    width: 50,
  },
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  shadowBox: {
    border: '1px solid #ccc',
    boxShadow: '5px 8px #eee',
    borderRadius: 5,
    maxWidth: '90%',
  },
  selectItem: {
    color: '#666',
    backgroundColor: '#e0e0e0',
  },
}));
function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof IData) => (
    event: React.MouseEvent<unknown>,
  ) => {};

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

export interface InitValueT {
  manager_id: number;
  name: string;
  description: string;
  current_opportunities: number[];
}

const Datasets: React.FC = () => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof IData>('id');
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isAddingDialog, setAddingDialog] = useState<boolean>(true);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [datasetDeleteId, setDatasetDeleteId] = useState<number>();
  const [datasetEditId, setDatasetEditId] = useState<number>(0);
  const [datasetsIndex, setDatasetIndex] = useState<number>(0);
  const [opportunityDialog, setOpportunityDialog] = useState<boolean>(false);
  const [datasets, setDataSets] = useState<IDataSet[]>([]);
  const { i18n } = useTranslation();

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof IData,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const handleRemovePlayBook = async (id: number) => {
    const response = await deleteDataset(id);
    if (response.success) {
      let newDatasets = datasets.filter(e => {
        return e.id !== id;
      });
      setDataSets(newDatasets);
    }
    setOpenDeleteDialog(false);
  };
  const handleSubmit = async (value: InitValueT, datasetId: number) => {
    if (datasetId === 0) {
      const response = await addDataset(value);
      if (response.success) {
        const getDatasetList = await getDataSets();
        if (getDatasetList.data.list) {
          setDataSets(getDatasetList.data.list);
        }
      }
      setOpenDialog(false);
    } else {
      const response = await updateDataset(value, datasetId);
      if (response.success) {
        const getDatasetList = await getDataSets();
        if (getDatasetList.data.list) {
          setDataSets(getDatasetList.data.list);
        }
      }
      setOpenDialog(false);
    }
  };
  const initialValues: InitValueT = {
    name: '',
    description: '',
    manager_id: 0,
    current_opportunities: [],
  };

  useEffectOnlyOnce(() => {
    const getData = async () => {
      const response = await getDataSets();
      if (response.data.list) {
        setDataSets(response.data.list);
      }
    };
    getData();
  });
  return (
    <>
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={values => {
          handleSubmit(values, datasetEditId);
        }}
      >
        {({
          values,
          handleChange,
          setFieldValue,
          resetForm,
          handleSubmit,
          handleBlur,
        }) => {
          const getOneDataset = async (datasetSelected: IDataSet) => {
            setFieldValue('name', datasetSelected.name);
            setFieldValue('description', datasetSelected.description);
          };
          return (
            <Grid container direction="column">
              <Box mt={3}>
                <Box display="flex" justifyContent="flex-end" mb={3}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      resetForm();
                      setAddingDialog(true);
                      setOpenDialog(true);
                      setDatasetEditId(0);
                    }}
                    startIcon={<AddCircleOutlineIcon />}
                  >
                    Add
                  </Button>
                </Box>
              </Box>
              <Grid container alignItems="center" justify="center">
                <TableContainer>
                  <Table className={classes.table}>
                    <EnhancedTableHead
                      classes={classes}
                      order={order}
                      orderBy={orderBy}
                      onRequestSort={handleRequestSort}
                    />
                    <TableBody>
                      {datasets.map((item: any, index: number) => (
                        <TableRow key={index} hover>
                          <TableCell component="td" scope="row">
                            {item?.id}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {item?.name}
                          </TableCell>
                          <TableCell component="td" scope="row">
                            {item?.description}
                          </TableCell>
                          <TableCell
                            classes={{ root: classes.rowLink }}
                            component="td"
                            scope="row"
                            onClick={() => {
                              setOpportunityDialog(true);
                              setDatasetIndex(index);
                            }}
                          >
                            {item?.current_opportunities?.length ?? 0}
                          </TableCell>
                          <TableCell component="td" scope="row" padding="none">
                            <TableAction
                              hiddenItem={['view']}
                              onClickEdit={() => {
                                resetForm();
                                setAddingDialog(false);
                                setOpenDialog(true);
                                setDatasetEditId(item.id);
                                getOneDataset(item);
                              }}
                              onClickDelete={() => {
                                setOpenDeleteDialog(true);
                                setDatasetDeleteId(item?.id);
                              }}
                            />
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>

              <FormDialog
                title="Dataset"
                submit={handleSubmit}
                open={openDialog}
                setOpen={setOpenDialog}
                isAddingForm={isAddingDialog}
                disable={values.name === ''}
                maxWidth="md"
                formContent={
                  <Box>
                    <Box mb={3} mt={3}>
                      <MixTitle title="Dataset name" isRequired />
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="name"
                        id="name"
                        autoComplete="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                    </Box>
                    <Box mb={3} mt={3}>
                      <MixTitle title="Description" />
                      <TextField
                        fullWidth
                        multiline
                        rows={4}
                        variant="outlined"
                        name="description"
                        id="description"
                        autoComplete="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                      />
                    </Box>
                  </Box>
                }
              />
              <DeleteDialog
                isOpen={isOpenDeleteDialog}
                header={i18n.t('dataset.delete_dataset_dialog_header')}
                message={
                  <Box>
                    <Typography component="p" variant="body1">
                      {i18n.t('dataset.delete_dataset_message')}
                    </Typography>
                  </Box>
                }
                handleClose={() => setOpenDeleteDialog(false)}
                handleDelete={() => {
                  if (datasetDeleteId) {
                    handleRemovePlayBook(datasetDeleteId);
                  }
                }}
              />
              <Dialog
                open={opportunityDialog}
                onClose={() => {
                  setOpportunityDialog(false);
                }}
                maxWidth={'md'}
                fullWidth
              >
                <Box padding={4}>
                  <Box textAlign="center" mb={2}>
                    <Typography component="p" variant="h4">
                      Opportunities
                    </Typography>
                  </Box>
                  <OpportunityTable
                    data={datasets[datasetsIndex]?.current_opportunities ?? []}
                    toggleModal={() => setOpportunityDialog(false)}
                  />
                </Box>
              </Dialog>
            </Grid>
          );
        }}
      </Formik>
    </>
  );
};
export default Datasets;
