import React from 'react';
import { BusinessUnit, BusinessProcess } from './components/Business';
import {
  BenefitFocusValue,
  BenefitFocusCategory,
} from './components/BenefitFocus';
import CorporateObjective from './components/CorporateObjective';
import {
  PriorityLabel,
  PriorityCategoryWeightings,
} from './components/Priority';
import {
  ExpertRoles,
  ExpertSkills,
  ResourceTeam,
  SkillGroupings,
  ProficiencyLevel,
} from './components/Experts';
import {
  ConfidenceLevel,
  OpportunitySize,
  OpportunityApproach,
} from './components/Opportunity';
import PlayBooks from './components/PlayBooks';
import Datasets from './components/Datasets';
import { Route, Switch, Redirect } from 'react-router-dom';
import ReferenceTags from './components/Tags';
import ReferenceListTypes from './components/ListTypes';

const Reference: React.FC = () => {
  return (
    <Switch>
      <Redirect
        from="/reference-data/"
        to="/reference-data/business-unit"
        exact
      />
      <Route
        exact
        path="/reference-data/business-unit"
        component={BusinessUnit}
      />
      <Route
        exact
        path="/reference-data/business-process"
        component={BusinessProcess}
      />
      <Route
        exact
        path="/reference-data/benefit-focus-value"
        component={BenefitFocusValue}
      />
      <Route
        exact
        path="/reference-data/corporate-objective"
        component={CorporateObjective}
      />
      <Route
        exact
        path="/reference-data/benefit-focus-category"
        component={BenefitFocusCategory}
      />
      <Route
        exact
        path="/reference-data/priority-label"
        component={PriorityLabel}
      />
      <Route
        exact
        path="/reference-data/priority-category-weightings"
        component={PriorityCategoryWeightings}
      />
      <Route
        exact
        path="/reference-data/experts-roles"
        component={ExpertRoles}
      />
      <Route
        exact
        path="/reference-data/experts-skills"
        component={ExpertSkills}
      />
      <Route
        exact
        path="/reference-data/resource-team"
        component={ResourceTeam}
      />
      <Route
        exact
        path="/reference-data/skill-groups"
        component={SkillGroupings}
      />
      <Route
        exact
        path="/reference-data/opportunity-approach"
        component={OpportunityApproach}
      />
      <Route
        exact
        path="/reference-data/opportunity-size"
        component={OpportunitySize}
      />
      <Route
        exact
        path="/reference-data/confidence-level"
        component={ConfidenceLevel}
      />

      <Route
        exact
        path="/reference-data/proficiency-level"
        component={ProficiencyLevel}
      />
      <Route
        exact
        path="/reference-data/analyse-playbook-templates"
        component={PlayBooks}
      />
      <Route exact path="/reference-data/datasets" component={Datasets} />
      <Route exact path="/reference-data/tags" component={ReferenceTags} />
      <Route
        exact
        path="/reference-data/list-types"
        component={ReferenceListTypes}
      />
    </Switch>
  );
};
export default Reference;
