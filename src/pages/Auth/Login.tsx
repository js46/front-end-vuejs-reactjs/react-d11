import React from 'react';
import {
  CircularProgress,
  TextField,
  Button,
  Grid,
  Box,
} from '@material-ui/core';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import {
  GoogleLogin,
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from 'react-google-login';
import { useSelector } from 'react-redux';
import { ConfigPubic } from '../../store/config/selector';
import { AuthState } from '../../store/auth/selector';
import {
  RequestUserLoginGoogle,
  RequestUserLogin,
} from '../../store/auth/actions';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

export const Login: React.FC = () => {
  const classes = useStyles();
  const publicConfig = useSelector(ConfigPubic);
  const authState = useSelector(AuthState);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Invalid email address')
        .required('This field is required'),
      password: Yup.string().required('This field is required'),
    }),
    onSubmit: values => {
      submit(values);
    },
  });

  const submit = async ({
    email,
    password,
  }: {
    email: string;
    password: string;
  }) => {
    dispatch(RequestUserLogin({ email: email, password: password }));
  };

  const responseGoogle = async (
    response: GoogleLoginResponse | GoogleLoginResponseOffline,
  ) => {
    const idToken = (response as GoogleLoginResponse).tokenId;
    if (idToken) {
      dispatch(RequestUserLoginGoogle(idToken));
    }
  };

  return (
    <React.Fragment>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <Box mb={3}>
          {authState.err && <Alert severity="error">{authState.err}</Alert>}
        </Box>
        <Box mb={2}>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
            error={formik.touched.email && !!formik.errors.email}
            helperText={formik.touched.email ? formik.errors.email : ''}
          />
        </Box>
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          required
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.password}
          error={formik.touched.password && !!formik.errors.password}
          helperText={formik.touched.password ? formik.errors.password : ''}
        />
        <Box mt={3} mb={1}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            disabled={authState.is_loading}
          >
            {authState && authState.is_loading ? (
              <CircularProgress size={24} />
            ) : (
              'Login'
            )}
          </Button>
        </Box>
      </form>
      {publicConfig && publicConfig.google_client_id && (
        <GoogleLogin
          clientId={publicConfig.google_client_id}
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          render={({ onClick, disabled }) => (
            <Box mt={3} mb={1} width="100%">
              <Button
                variant="contained"
                color="secondary"
                onClick={onClick}
                disabled={disabled}
                fullWidth
              >
                Login with google
              </Button>
            </Box>
          )}
        />
      )}
      <Box mt={3} mb={1} width="100%">
        <Link
          href={publicConfig.okta_redirect_uri}
          style={{ textDecoration: 'none' }}
        >
          <Button
            type="submit"
            fullWidth
            variant="contained"
            style={{
              backgroundColor: 'purple',
              color: '#fff',
            }}
          >
            Login with Okta
          </Button>
        </Link>
      </Box>
      <Box mt={1} width="100%">
        <Grid container direction="row" justify="flex-end">
          <Grid item>
            <Link component={RouterLink} to="/register" variant="body2">
              Don't have an account? Register now
            </Link>
          </Grid>
        </Grid>
      </Box>
    </React.Fragment>
  );
};

export default Login;
