import React, { useState } from 'react';
import {
  CircularProgress,
  TextField,
  Button,
  Grid,
  Link,
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from '../../commons/axios';
import httpStatus from '../../commons/httpStatus';
import { Link as RouterLink } from 'react-router-dom';
import { MessageDialog } from '../../components';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

export const Register: React.FC = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  let history = useHistory();

  const submit = async ({
    email,
    password,
    full_name,
  }: {
    email: string;
    password: string;
    full_name: string;
  }) => {
    setIsLoading(true);
    try {
      const res = await axios.post<{ data: any; message?: string }>(
        '/api/v1/auth/register',
        {
          email,
          password,
          full_name,
        },
      );
      setIsLoading(false);
      if (res.status === httpStatus.StatusOK) {
        setSuccess(true);
      } else {
        setErrorMessage(`${res.data.message}`);
      }
    } catch (e) {
      setIsLoading(false);
      setErrorMessage(`${e.message}`);
    }
  };

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      full_name: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Invalid email address')
        .required('This field is required'),
      password: Yup.string().required('This field is required'),
      full_name: Yup.string().required('This field is required'),
    }),
    onSubmit: values => {
      submit(values);
    },
  });

  return (
    <form className={classes.form} onSubmit={formik.handleSubmit}>
      {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
      <Box mb={2}>
        <TextField
          disabled={isLoading}
          variant="outlined"
          margin="normal"
          fullWidth
          required
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.email}
          error={formik.touched.email && !!formik.errors.email}
          helperText={formik.touched.email ? formik.errors.email : ''}
        />
      </Box>
      <Box mb={2}>
        <TextField
          disabled={isLoading}
          variant="outlined"
          margin="normal"
          fullWidth
          required
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.password}
          error={formik.touched.password && !!formik.errors.password}
          helperText={formik.touched.password ? formik.errors.password : ''}
        />
      </Box>

      <TextField
        disabled={isLoading}
        variant="outlined"
        margin="normal"
        fullWidth
        required
        name="full_name"
        label="Full Name"
        id="full_name"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.full_name}
        error={formik.touched.full_name && !!formik.errors.full_name}
        helperText={formik.touched.full_name ? formik.errors.full_name : ''}
      />
      <Box mt={3} mb={1}>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          disabled={isLoading}
        >
          {isLoading ? <CircularProgress size={24} /> : 'Register'}
        </Button>
      </Box>
      <Grid container>
        <Grid item xs>
          <Link component={RouterLink} to="/login" variant="body2">
            Back to login
          </Link>
        </Grid>
      </Grid>

      <MessageDialog
        mode="success"
        message={t('auth.successful_registration')}
        open={success}
        onClose={() => history.push('/login')}
      />
    </form>
  );
};

export default Register;
