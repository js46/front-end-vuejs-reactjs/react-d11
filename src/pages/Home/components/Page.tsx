import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
  },
  navigationWrapper: {
    position: 'relative',
    height: 250,
    width: '100%',
  },

  contentWrapper: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: '100',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '14px',
  },
  card: {
    width: '100%',
    height: 250,
    backgroundColor: '#eee',
    transition: 'all 0.3s',
    '&:hover': {
      backgroundColor: '#ddd',
      transform: 'scale(1.1)',
      boxShadow: '1px 1px 1px 5px #eee',
    },
  },
  elementImage: {
    textAlign: 'center',
  },
  elementName: {
    textAlign: 'center',
    whiteSpace: 'nowrap',
  },
}));

interface IProps {
  linkTo: string;
  name: string;
  imageSrc: string;
}

export const Page: React.FC<IProps> = ({ linkTo, name, imageSrc }) => {
  const classes = useStyles();

  return (
    <Button component={Link} to={linkTo} className={classes.navigationWrapper}>
      <Card className={classes.card}>
        <div className={classes.contentWrapper}>
          <div className={classes.elementImage}>
            <img src={imageSrc} alt={name} />
          </div>
          <div className={classes.elementName}>
            <CardContent>{name}</CardContent>
          </div>
        </div>
      </Card>
    </Button>
  );
};
