import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Grid, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Page } from './components';
import { listPage } from './shared';

import { UserMenuState } from '../../store/user/selector';
import { TUserMenuState } from '../../store/user/actions';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    maxWidth: '1024',
  },
}));

export const Dashboard: React.FC = () => {
  const [listRenderedMenu, setListRenderedMenu] = useState<typeof listPage>([]);
  const userMenu = useSelector(UserMenuState);
  const classes = useStyles();

  useEffect(() => {
    if (userMenu.length) {
      getRenderedMenu(userMenu);
    }
  }, [userMenu]);

  const getRenderedMenu = (userMenu: TUserMenuState) => {
    const listAuthorizedPath =
      userMenu && userMenu.map((item: string[]) => item[1]);
    setListRenderedMenu(
      listPage.filter(page => {
        return (
          listAuthorizedPath.length &&
          listAuthorizedPath.includes(page.routePath)
        );
      }),
    );
  };

  return (
    <div className={classes.root}>
      <Container>
        <Grid container spacing={5}>
          {listRenderedMenu.map((page, index) => {
            return (
              <Grid key={index} item xl={4} lg={4} sm={6} xs={12}>
                <Page
                  linkTo={page.routePath}
                  name={page.name}
                  imageSrc={page.imageSrc}
                />
              </Grid>
            );
          })}
        </Grid>
      </Container>
    </div>
  );
};

export default Dashboard;
