import {
  Approvals,
  DashboardMenu,
  GroupPermissions,
  Languages,
  Logs,
  Settings,
  CaptureOpp,
  UserGroups,
  UserPermissions,
  Users,
  Idea,
  Experts,
  Backlog,
  Report,
  Dataset,
  Reference,
  Audit,
  Satisfaction,
} from '../../../assets';

export const listPage = [
  {
    name: 'CAPTURE OPPORTUNITY',
    routePath: '/capture-opportunity',
    imageSrc: CaptureOpp,
  },
  {
    name: 'PIPELINE',
    routePath: '/opportunity-pipeline',
    imageSrc: Backlog,
  },
  {
    name: 'MY TEAM',
    routePath: '/my-team',
    imageSrc: Satisfaction,
  },
  {
    name: 'MY STUFF',
    routePath: '/my-stuff',
    imageSrc: DashboardMenu,
  },
  {
    name: 'SEARCH',
    routePath: '/search',
    imageSrc: Idea,
  },
  {
    name: 'EXPERTS',
    routePath: '/experts',
    imageSrc: Experts,
  },
  {
    name: 'DASHBOARD',
    routePath: '/dashboard',
    imageSrc: Report,
  },
  {
    name: 'DATASET',
    routePath: '/dataset',
    imageSrc: Dataset,
  },
  {
    name: 'USERS',
    routePath: '/users',
    imageSrc: Users,
  },
  {
    name: 'ROLES',
    routePath: '/roles',
    imageSrc: GroupPermissions,
  },
  {
    name: 'POLICY',
    routePath: '/policies',
    imageSrc: UserPermissions,
  },
  {
    name: 'REFERENCE DATA',
    routePath: '/reference-data',
    imageSrc: Reference,
  },
  {
    name: 'AUDIT',
    routePath: '/audit',
    imageSrc: Audit,
  },
  {
    name: 'SETTINGS',
    routePath: '/setting',
    imageSrc: Settings,
  },
  // {
  //   name: 'opportunities',
  //   routePath: '/opportunities',
  //   imageSrc: Idea,
  // },
  {
    name: 'USER GROUP',
    routePath: '/usergroup',
    imageSrc: UserGroups,
  },
  {
    name: 'LOGS',
    routePath: '/log',
    imageSrc: Logs,
  },
  {
    name: 'system',
    routePath: '/system',
    imageSrc: Settings,
  },
  {
    name: 'approvals',
    routePath: '/approval',
    imageSrc: Approvals,
  },
  {
    name: 'language',
    routePath: '/language',
    imageSrc: Languages,
  },
  {
    name: 'dashboard menu',
    routePath: '/dashboardmenu',
    imageSrc: DashboardMenu,
  },

  // {
  //   name: 'setting',
  //   routePath: '/setting',
  //   imageSrc: Settings,
  // },
];
