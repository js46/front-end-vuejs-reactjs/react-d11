import React from 'react';
import { Typography, Box } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import { MainShape } from './component';

// const useStyles = makeStyles(theme => ({
//   root: {
//     marginTop: theme.spacing(3),
//   },
// }));

export const Shape: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Box mt={2}>
      <Box mb={2}>
        <Typography variant="h3">{t('opp_pipeline')}</Typography>
      </Box>
      <MainShape />
    </Box>
  );
};

export default Shape;
