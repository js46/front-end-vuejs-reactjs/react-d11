import React from 'react';
import {
  Typography,
  Box,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  StyledMultiList,
  StyledDateRange,
  StyledRangSlider,
  StyledSingleList,
} from '../../../components/styled';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { UserProfileState } from '../../../store/user/selector';
import { useQueryOpportunity } from '../../../hooks';

export const FilterSidebar = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const userProfileState = useSelector(UserProfileState);
  const { defaultQuery } = useQueryOpportunity(false);

  return (
    <React.Fragment>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{t('phase_list')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Phase list"
              dataField="opportunity_phase.display_name.keyword"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'State list',
                  'Priority list',
                  'Priority score',
                  'Keyword',
                  'Date range',
                  'Resource team',
                  'Privacy flag',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{t('status_waiting')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="State list"
              dataField="opportunity_state.display_name.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Phase list',
                  'Priority list',
                  'Priority score',
                  'Keyword',
                  'Date range',
                  'Resource team',
                  'Privacy flag',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {userProfileState.resource_team.resource_team_name && (
        <ExpansionPanel classes={{ root: classes.panel }}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            classes={{
              root: classes.filterSummary,
              content: classes.filterSummaryContent,
              expandIcon: classes.expandIcon,
            }}
          >
            <Typography variant="h6">{t('resource_team')}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.filterDetail}>
            <Box width="100%">
              <StyledMultiList
                defaultValue={
                  userProfileState.resource_team.resource_team_name &&
                  !userProfileState.isAdmin
                    ? [userProfileState.resource_team.resource_team_name]
                    : undefined
                }
                componentId="Resource team"
                dataField="resource_team.name.keyword"
                showCheckbox={true}
                showSearch={false}
                queryFormat="or"
                innerClass={{
                  label: 'multiList',
                }}
                react={{
                  and: [
                    'State list',
                    'Priority list',
                    'Priority score',
                    'Keyword',
                    'Date range',
                    'Phase list',
                    'Privacy flag',
                  ],
                }}
                defaultQuery={defaultQuery}
              />
            </Box>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      )}
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{t('date_submitted')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledDateRange
              componentId="Date range"
              dataField="created_at"
              innerClass={{
                'input-container': 'input-date',
              }}
              placeholder={{
                start: 'From',
                end: 'To',
              }}
              queryFormat="date"
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={
            <ExpandMoreIcon style={{ alignItems: 'center!important' }} />
          }
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{t('priority_score')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetailSlider}>
          <Box width="400px">
            <StyledRangSlider
              dataField="opp_priority_score.priority_score"
              componentId="Priority score"
              rangeLabels={{
                start: '0',
                end: '100',
              }}
              range={{
                start: 0,
                end: 100,
              }}
              showHistogram={true}
              tooltipTrigger="hover"
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{t('priority_level')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Priority list"
              filterLabel="Priority level"
              dataField="opp_priority_score.priority_level.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'State list',
                  'Phase list',
                  'Priority list',
                  'Priority score',
                  'Keyword',
                  'Date range',
                  'Resource team',
                  'Privacy flag',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {(userProfileState.isAdmin || userProfileState.internal) && (
        <ExpansionPanel classes={{ root: classes.panel }}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            classes={{
              root: classes.filterSummary,
              content: classes.filterSummaryContent,
              expandIcon: classes.expandIcon,
            }}
          >
            <Typography variant="h6">{t('privacy')}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.filterDetail}>
            <Box width="100%">
              <StyledSingleList
                componentId="Privacy flag"
                filterLabel="Privacy"
                showSearch={false}
                dataField="private_flag"
                showFilter
                defaultQuery={defaultQuery}
                customQuery={function(value?: string) {
                  if (!value) return undefined;
                  return {
                    query: {
                      bool: {
                        must: {
                          term: {
                            private_flag: value === 'Private',
                          },
                        },
                      },
                    },
                  };
                }}
                transformData={(data: any[]) => {
                  if (data) {
                    return data.map(item => ({
                      key: item.key === 1 ? 'Private' : 'Public',
                      doc_count: item.doc_count,
                    }));
                  }
                  return data;
                }}
                react={{
                  and: [
                    'State list',
                    'Phase list',
                    'Priority score',
                    'Priority list',
                    'Keyword',
                    'Date range',
                    'Resource team',
                    'Privacy flag',
                  ],
                }}
              />
            </Box>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      )}
    </React.Fragment>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  filterSummary: {
    padding: '0px 5px 0px 0px',
    margin: '0px',
  },
  filterDetailSlider: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
  filterSummaryContent: {
    margin: 0,
    padding: 0,
    // '& $expanded': {
    //   margin: 'auto',
    //   padding: '0px',
    // },
  },
  filterDetail: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
  },
  expandIcon: {
    margin: '0px!important',
  },
  panel: {
    margin: '0px!important',
    boxShadow: 'none',
    '&::before': {
      background: 'none!important',
    },
  },
  selectStyle: {
    width: 200,
    marginLeft: 20,
  },
}));
