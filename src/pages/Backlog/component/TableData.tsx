import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import moment from 'moment';
import {
  StyledReactiveList,
  StyledReactiveComponent,
  StyledDataSearch,
} from '../../../components/styled';
import {
  Grid,
  Link,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Button,
  Box,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  ESLoader,
  ESNoResult,
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from '../../../components';
import { generateOpportunityLink } from '../../../utils';
import {
  useTableHeadSorter,
  useQueryOpportunity,
  useCustomSearch,
} from '../../../hooks';

import { SelectedFiltersCustom } from '../../../components';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        // marginTop: '-50px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
        fontSize: '14px!important',
        fontFamily: 'Helvetica Neue,Arial',
      },
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
}));

const tableFields = [
  'title.keyword',
  'created_by.name.keyword',
  'opp_priority_score.priority_level',
  'opp_priority_score.priority_score',
  'capture_submit_date',
  'resource_team.name.keyword',
  'opportunity_phase.display_name.keyword',
  'opportunity_state.name.keyword',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Proposer',
    disablePadding: true,
  },
  {
    id: 'opp_priority_score.priority_level',
    label: 'Priority',
    disablePadding: true,
  },
  {
    id: 'opp_priority_score.priority_score',
    label: 'Priority score',
    disablePadding: true,
  },
  {
    id: 'capture_submit_date',
    label: 'Date submitted',
    disablePadding: true,
  },
  {
    id: 'resource_team.name.keyword',
    label: 'Team',
    disablePadding: true,
  },
  {
    id: 'opportunity_phase.display_name.keyword',
    label: 'Phase',
    disablePadding: true,
  },
  {
    id: 'opportunity_state.name.keyword',
    label: 'Status - waiting',
    disablePadding: true,
  },
];

interface TableDataProps {
  isFilter?: boolean;
}

export const TableData: React.FC<TableDataProps> = ({ isFilter }) => {
  const classes = useStyles();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'opp_priority_score.priority_score',
    SortOrder.DESC,
  );
  const extraQuery = {
    filter: {
      range: {
        'opportunity_phase.id': {
          gte: 1,
        },
      },
    },
  };
  const { defaultQuery, privateQuery } = useQueryOpportunity(true, extraQuery);
  const {
    searchBtnRef,
    value,
    onClearSearch,
    onValueSelected,
    onChange,
    onValueChange,
    onClickSearch,
    searchQuery,
  } = useCustomSearch(
    [
      'title',
      'description',
      'opportunity_type.name',
      'created_by.name',
      'resource_team.name',
      'opp_priority_score.priority_score',
    ],
    'and',
    [{ term: { del_flag: false } }, ...privateQuery],
    extraQuery,
  );

  return (
    <Grid container>
      <Grid item xs={12}>
        <>
          <Grid container>
            <Grid item lg={10} xl={10} xs={12} sm={12}>
              <StyledDataSearch
                componentId="SearchInput"
                dataField={[
                  'title',
                  'description',
                  'opportunity_type.name',
                  'created_by.name',
                  'resource_team.name',
                  'opp_priority_score.priority_score',
                ]}
                showDistinctSuggestions
                queryFormat="or"
                placeholder="Search for opportunity..."
                iconPosition="left"
                showClear={true}
                showFilter={false}
                innerClass={{
                  input: 'searchInput',
                  title: 'titleSearch',
                }}
                defaultQuery={() => searchQuery}
                parseSuggestion={suggestion => {
                  return {
                    label: (
                      <div>
                        <Grid container>
                          <Grid item xs={12}>
                            <Box fontWeight="fontWeightBold">
                              {suggestion.source.title}
                            </Box>
                          </Grid>
                        </Grid>
                        <Grid container>
                          <Grid item xs={12}>
                            <span style={{ fontStyle: 'italic' }}>
                              Completion date at:{' '}
                            </span>
                            {suggestion.source.completion_date !==
                              '0001-01-01T00:00:00Z' &&
                              moment(suggestion.source.completion_date).format(
                                'DD/MM/YYYY',
                              )}
                            |{' '}
                            <span style={{ fontStyle: 'italic' }}>
                              Opportunity type:{' '}
                            </span>
                            {suggestion.source.opportunity_type.name}
                          </Grid>
                        </Grid>
                      </div>
                    ),
                    value: suggestion.source.title,
                    source: suggestion.source,
                  };
                }}
                onChange={onChange}
                value={value}
                onValueChange={onValueChange}
                onValueSelected={onValueSelected}
              />
            </Grid>
            <Grid item lg={2} xl={2} xs={12} sm={12}>
              <Box
                display="flex"
                justifyContent={{
                  xs: 'flex-start',
                  sm: 'flex-start',
                  lg: 'flex-end',
                  xl: 'flex-end',
                }}
              >
                <StyledReactiveComponent
                  componentId="Search"
                  filterLabel="Keyword"
                  showFilter={true}
                  render={({ setQuery }) => (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => onClickSearch(setQuery)}
                      ref={searchBtnRef}
                    >
                      Search
                    </Button>
                  )}
                />
              </Box>
            </Grid>
          </Grid>
          <Box mt={2} mb={1}>
            <SelectedFiltersCustom onClearSearch={onClearSearch} />
          </Box>
          <StyledReactiveList
            componentId="SearchOpportunities"
            dataField={orderBy}
            size={10}
            sortBy={order}
            paginationAt="bottom"
            pages={5}
            pagination={!isFilter}
            infiniteScroll={!isFilter}
            react={{
              and: [
                'Search',
                'Phase list',
                'State list',
                'Resource team',
                'Date range',
                'Priority list',
                'Priority score',
                'Privacy flag',
              ],
            }}
            showLoader
            loader={<ESLoader />}
            renderNoResults={() => <ESNoResult msgKey="no_opp" />}
            renderResultStats={function(stats) {
              return (
                <Grid container>
                  <Grid item xs>
                    <Typography component="h1" align="left">
                      {stats.numberOfResults.toLocaleString()} Results
                    </Typography>
                  </Grid>
                </Grid>
              );
            }}
            render={({ data, loading }) => {
              if (loading) return null;
              return (
                <>
                  {data.length > 0 && (
                    <TableContainer
                      classes={{ root: classes.rootTableContainer }}
                    >
                      <Table classes={{ root: classes.rootTable }}>
                        <TableHeadSorter
                          onRequestSort={handleRequestSort}
                          order={order}
                          orderBy={orderBy}
                          cells={headCells}
                        />
                        <TableBody>
                          {data.map((item: any, index: number) => (
                            <TableRow key={index}>
                              <TableCell component="th" scope="row">
                                <Link
                                  component={RouterLink}
                                  color="inherit"
                                  to={generateOpportunityLink(
                                    item.id,
                                    item.opportunity_phase?.name,
                                    item.opportunity_state?.name,
                                  )}
                                >
                                  {item?.title}
                                </Link>
                              </TableCell>
                              <TableCell component="th" scope="row">
                                <Link
                                  component={RouterLink}
                                  color="inherit"
                                  to={`/experts/${item?.created_by?.id}`}
                                >
                                  {item?.created_by?.name}
                                </Link>
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item.opp_priority_score?.priority_level === ''
                                  ? 'Low'
                                  : item.opp_priority_score?.priority_level}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item.opp_priority_score?.priority_score}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item?.capture_submit_date &&
                                  item.capture_submit_date !==
                                    '0001-01-01T00:00:00Z' &&
                                  moment(item.capture_submit_date).format(
                                    'DD/MM/YYYY',
                                  )}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item?.resource_team?.name}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item?.opportunity_phase?.display_name}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item?.opportunity_state?.display_name}
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  )}
                </>
              );
            }}
            innerClass={{
              button: classes.buttonPagination,
              resultsInfo: classes.resultsInfo,
              sortOptions: classes.sortSelect,
              select: classes.sortSelect,
            }}
            defaultQuery={defaultQuery}
          />
        </>
      </Grid>
    </Grid>
  );
};
