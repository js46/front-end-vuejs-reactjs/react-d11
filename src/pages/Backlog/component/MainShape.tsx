import React from 'react';
import { Grid, Box } from '@material-ui/core';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { SectionBox, TStringBoolean } from '../../../components/styled';
import { FilterSidebar } from './FilterSidebar';
import { TableData } from './TableData';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../store/config/selector';

export const MainShape: React.FC = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  console.log('ES', ES_INDICES);
  return (
    <>
      {!!ES_INDICES && (
        <ReactiveBase
          app={ES_INDICES.opportunity_index_name}
          url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
          headers={{ Authorization: window.localStorage.getItem('jwt') }}
        >
          <Grid container>
            <Box position="relative" width="100%">
              <Box position="absolute" width={256} paddingTop={3}>
                <FilterSidebar />
              </Box>
              <SectionBox
                data-has-sidebar={TStringBoolean.TRUE}
                style={{ paddingRight: 0, marginLeft: '256px' }}
              >
                <Box paddingTop={3}>
                  <TableData />
                </Box>
              </SectionBox>
            </Box>
          </Grid>
        </ReactiveBase>
      )}
    </>
  );
};
