import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Box, Typography } from '@material-ui/core';
import {
  ExpansionPanelDetailFilter,
  ExpansionPanelFilter,
  ExpansionPanelSummaryFilter,
  StyledMultiList,
} from '../../components/styled';
import { useTranslation } from 'react-i18next';

interface SidebarFilterProps {
  defaultQuery?: () => object;
}
const SidebarFilter: React.FC<SidebarFilterProps> = ({ defaultQuery }) => {
  const { t } = useTranslation();

  return (
    <>
      {/*Filter by Role*/}
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="role-content"
          id="role-header"
        >
          <Typography variant="h6">{t('roles')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledMultiList
              componentId="Role list"
              dataField="profile_role.profile_role_name.keyword"
              filterLabel="Roles"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Keyword',
                  'Skill list',
                  'Location list',
                  'Resource team list',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
      {/*End Filter by Role*/}
      {/*Filter by Skill*/}
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="skill-content"
          id="skill-header"
        >
          <Typography variant="h6">{t('skills')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledMultiList
              componentId="Skill list"
              filterLabel="Skills"
              dataField="skills.skill_name.keyword"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Keyword',
                  'Role list',
                  'Location list',
                  'Resource team list',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
      {/*End Filter by Skill*/}
      {/*Filter by Location*/}
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="location-content"
          id="location-header"
        >
          <Typography variant="h6">{t('locations')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledMultiList
              componentId="Location list"
              filterLabel="Locations"
              dataField="location.name.keyword"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Keyword',
                  'Role list',
                  'Skill list',
                  'Resource team list',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
      {/*End Filter by Location*/}
      {/*Filter by Resource Team*/}
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="resource-team-content"
          id="resource-team-header"
        >
          <Typography variant="h6">{t('resource_team')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledMultiList
              componentId="Resource team list"
              filterLabel="Resource team"
              dataField="resource_team.resource_team_name.keyword"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: ['Keyword', 'Role list', 'Skill list', 'Locations list'],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
      {/*End Filter by Resource Team*/}
    </>
  );
};

export default SidebarFilter;
