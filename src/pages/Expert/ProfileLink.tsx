import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { Link } from '@material-ui/core';
import { UserProfileState } from '../../store/user/selector';

interface ProfileLinkProps {
  id: number;
  title: string | number;
}
export const ProfileLink: React.FC<ProfileLinkProps> = React.memo(
  ({ id, title }) => {
    const userProfileState = useSelector(UserProfileState);
    const profileLink = useMemo(() => {
      return id === userProfileState.id
        ? `/my-stuff`
        : `/profile/${id}/opportunity`;
    }, [id, userProfileState.id]);
    return (
      <Link component={RouterLink} color="inherit" to={profileLink}>
        {title}
      </Link>
    );
  },
);
