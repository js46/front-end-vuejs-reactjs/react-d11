import React from 'react';
import { Box, Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import SidebarFilter from './SidebarFilter';
import ExpertTable from './ExpertTable';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../store/config/selector';
import { useTranslation } from 'react-i18next';

export const Expert: React.FC<{}> = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const defaultQuery = () => ({
    query: {
      bool: {
        must: [{ term: { casbin_role: 'role:expert' } }],
      },
    },
  });
  const { t } = useTranslation();
  return (
    <>
      <Box mt={2}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="h4">{t('experts')}</Typography>
            <br />
          </Grid>
          <Grid item xs={12}>
            <ReactiveBase
              app={ES_INDICES.user_profile_index_name}
              url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
              headers={{ Authorization: window.localStorage.getItem('jwt') }}
            >
              <Grid container>
                <Grid item lg={2} xl={2} sm={12} xs={12}>
                  <SidebarFilter defaultQuery={defaultQuery} />
                </Grid>
                <Grid item lg={10} xl={10} xs={12} sm={12}>
                  <ExpertTable defaultQuery={defaultQuery} />
                </Grid>
              </Grid>
            </ReactiveBase>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
export default Expert;
