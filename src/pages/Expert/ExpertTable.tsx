import React, { useState } from 'react';
import {
  Box,
  Button,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import {
  StyledDataSearch,
  StyledReactiveComponent,
  StyledReactiveList,
} from '../../components/styled';
import {
  ESLoader,
  ESNoResult,
  HeadCellProps,
  SelectedFiltersCustom,
  TableHeadSorter,
} from '../../components';
import { makeStyles } from '@material-ui/core/styles';
import { useCustomSearch, useTableHeadSorter } from '../../hooks';
import { OpportunityCountByExpert } from '../../components/opportunity/OpportunityCountByExpert';
import { ProfileLink } from './ProfileLink';

const useStyles = makeStyles({
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  rootTable: {
    minWidth: 732,
  },
  rootTableContainer: {
    marginTop: 15,
  },
});
const tableFields = [
  'id',
  'name.keyword',
  'profile_role.profile_role_name.keyword',
  'location.name.keyword',
  'resource_team.resource_team_name.keyword',
  'created_at',
  'current_opportunity',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: '#',
    disablePadding: true,
  },
  {
    id: 'name.keyword',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'profile_role.profile_role_name.keyword',
    label: 'Role',
    disablePadding: true,
  },
  {
    id: 'location.name.keyword',
    label: 'Location',
    disablePadding: true,
  },
  {
    id: 'resource_team.resource_team_name.keyword',
    label: 'Resource team',
    disablePadding: true,
  },
  {
    id: 'current_opportunity',
    label: 'Opportunity count #',
    disablePadding: true,
    disableSorting: true,
  },
];
interface ExpertTableProps {
  defaultQuery: () => Object;
}

const ExpertTable: React.FC<ExpertTableProps> = ({ defaultQuery }) => {
  const [opportunityCount, setOpportunityCount] = useState<
    Map<number, number>
  >();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'name.keyword',
  );
  const classes = useStyles();
  const {
    searchBtnRef,
    value,
    onClearSearch,
    onValueSelected,
    onChange,
    onValueChange,
    onClickSearch,
    searchQuery,
  } = useCustomSearch(['name', 'profile_role.profile_role_name'], 'and', [
    { term: { casbin_role: 'role:expert' } },
  ]);

  return (
    <>
      <Grid item container>
        <Grid item lg={10} xl={10} xs={12} sm={12}>
          <StyledDataSearch
            componentId="SearchInput"
            dataField={['name', 'profile_role.profile_role_name']}
            queryFormat="and"
            placeholder="Search for experts..."
            iconPosition="left"
            showIcon={false}
            showFilter={false}
            showClear
            showDistinctSuggestions
            innerClass={{
              input: 'searchInput',
              title: 'titleSearch',
            }}
            showVoiceSearch
            defaultQuery={() => searchQuery}
            parseSuggestion={suggestion => ({
              label: (
                <Grid container>
                  <Grid item xs={12}>
                    <Box fontWeight="fontWeightBold">
                      {suggestion.source.name}
                    </Box>
                  </Grid>
                  <Grid item xs={12}>
                    <span style={{ fontStyle: 'italic' }}>Role: </span>
                    {suggestion.source.profile_role?.profile_role_name} |{' '}
                    <span style={{ fontStyle: 'italic' }}>Location: </span>
                    {suggestion.source.location?.name || 'N/A'}
                  </Grid>
                </Grid>
              ),
              value: suggestion.source.name,
              source: suggestion.source,
            })}
            onChange={onChange}
            value={value}
            onValueChange={onValueChange}
            onValueSelected={onValueSelected}
          />
        </Grid>
        <Grid item lg={2} xl={2} xs={12} sm={12}>
          <Box
            display="flex"
            justifyContent={{
              xs: 'flex-start',
              sm: 'flex-start',
              lg: 'flex-end',
              xl: 'flex-end',
            }}
          >
            <StyledReactiveComponent
              componentId="Search"
              filterLabel="Keyword"
              showFilter={true}
              render={({ setQuery }) => (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => onClickSearch(setQuery)}
                  ref={searchBtnRef}
                >
                  Search
                </Button>
              )}
            />
          </Box>
        </Grid>
      </Grid>
      <Grid item container>
        <Box pt={2} pb={3}>
          <SelectedFiltersCustom onClearSearch={onClearSearch} />
        </Box>
      </Grid>
      <Grid container item>
        <Grid item xs={12}>
          <OpportunityCountByExpert
            setOpportunityCount={setOpportunityCount}
            isExpertMenu
          />
          <StyledReactiveList
            componentId="Experts"
            react={{
              and: [
                'Search',
                'Role list',
                'Skill list',
                'Location list',
                'Resource team list',
              ],
            }}
            dataField={orderBy}
            sortBy={order}
            size={10}
            paginationAt="bottom"
            pages={5}
            pagination={true}
            defaultQuery={defaultQuery}
            showLoader
            loader={<ESLoader />}
            renderNoResults={() => <ESNoResult msgKey="no_expert" />}
            renderResultStats={function(stats) {
              return (
                <Grid container>
                  <Grid item xs>
                    <Typography component="h1" align="left">
                      {stats.numberOfResults.toLocaleString()} Results
                    </Typography>
                  </Grid>
                </Grid>
              );
            }}
            render={({ data, loading }) => {
              if (loading) return null;
              return (
                <React.Fragment>
                  {data.length > 0 && (
                    <TableContainer
                      classes={{ root: classes.rootTableContainer }}
                    >
                      <Table classes={{ root: classes.rootTable }}>
                        <TableHeadSorter
                          onRequestSort={handleRequestSort}
                          order={order}
                          orderBy={orderBy}
                          cells={headCells}
                        />

                        <TableBody>
                          {data.map((item: any, index: number) => (
                            <TableRow key={index}>
                              <TableCell component="th" scope="row">
                                {item.id}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                <ProfileLink id={item.id} title={item.name} />
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item.profile_role?.profile_role_name}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item.location?.name}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {item.resource_team.resource_team_name}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {opportunityCount ? (
                                  <ProfileLink
                                    id={item.id}
                                    title={opportunityCount.get(item.id) ?? 0}
                                  />
                                ) : (
                                  <BeatLoader size={5} color={'grey'} loading />
                                )}
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  )}
                </React.Fragment>
              );
            }}
            innerClass={{
              button: classes.buttonPagination,
              resultsInfo: classes.resultsInfo,
              sortOptions: classes.sortSelect,
            }}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default ExpertTable;
