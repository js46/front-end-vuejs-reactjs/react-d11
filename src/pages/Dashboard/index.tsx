import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import {
  DashBoardSideBar,
  OpportunityReport,
  SidebarList,
  ExpertReport,
} from './component';
import { SectionBox, TStringBoolean } from '../../components/styled';
import { Breadcrumb } from '../../layouts/main/components';

export const Report = () => {
  const [menuSelected, setMenuSelected] = useState<SidebarList>(
    SidebarList.Experts,
  );

  const renderStepView = (currentStep: SidebarList) => {
    switch (currentStep) {
      case SidebarList.Experts:
        return <ExpertReport />;
      case SidebarList.Opportunities:
        return <OpportunityReport />;
      default:
        return <ExpertReport />;
    }
  };

  return (
    <Grid container>
      <DashBoardSideBar
        menuSelected={menuSelected}
        setMenuSelected={setMenuSelected}
      />
      <SectionBox
        data-has-sidebar={TStringBoolean.TRUE}
        component="section"
        style={{ paddingTop: 0, width: 'calc(100% - 256px)', marginTop: -42 }}
      >
        <Breadcrumb />
        {renderStepView(menuSelected)}
      </SectionBox>
    </Grid>
  );
};

export default Report;
