import React, { useState } from 'react';
import { UtilizationReport, DemandReports } from '..';
import { Grid, Box, Select, MenuItem, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

enum SelectRenderer {
  DemandReports = 'Demand Backlog',
  UtilizationReport = 'Utilization Report',
}

export const ExpertReport = () => {
  const [currentSelected, setCurrentSelected] = useState(
    SelectRenderer.DemandReports,
  );

  const handleChange = (
    event: React.ChangeEvent<{ name?: string; value: any }>,
  ) => {
    setCurrentSelected(event.target.value);
  };

  const renderStepView = (currentStep: SelectRenderer) => {
    switch (currentStep) {
      case SelectRenderer.DemandReports:
        return <DemandReports />;
      case SelectRenderer.UtilizationReport:
        return <UtilizationReport />;
      default:
        return <DemandReports />;
    }
  };

  return (
    <Grid container alignItems="center">
      <Grid item xs={6}>
        <Box pt={2} pb={3}>
          <Typography variant="h4">{currentSelected}</Typography>
        </Box>
      </Grid>
      <Grid item xs={6} justify="flex-end" container>
        <Select
          variant="outlined"
          value={currentSelected}
          id="role_name_select"
          name="role_name_select"
          onChange={handleChange}
          MenuProps={{
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'left',
            },
            getContentAnchorEl: null,
          }}
          IconComponent={ExpandMoreIcon}
        >
          <MenuItem key={1} value={SelectRenderer.DemandReports}>
            Demand Backlog
          </MenuItem>
          <MenuItem key={0} value={SelectRenderer.UtilizationReport}>
            Utilization
          </MenuItem>
        </Select>
      </Grid>
      {renderStepView(currentSelected)}
    </Grid>
  );
};
