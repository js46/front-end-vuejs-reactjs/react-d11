import React, { useState, useCallback, useEffect } from 'react';
import { Box, Grid, Typography, Select, MenuItem } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { uniqBy } from 'lodash';
import { useHolidayFetch } from '../../../../../hooks';

import {
  IResourceTeam,
  IBusinessUnit,
  IExpertRoles,
} from '../../../../../services/references.services';
import {
  getDemandDashboard,
  getCapacityDashboard,
  IDemandData,
  ICapacityData,
} from '../../../../../services/dashboard';
import { IQueryParam } from '../../../../../services/common.services';
import { useEffectOnlyOnce } from '../../../../../hooks';
import { TabPanelDetail, LineChartCustom } from './components';
import { IDemandProcessed, ICapacityProcessed, IFilterDemand } from './type';
import { DemandDashboardContext } from './DemandDashboardContext';
import { useTranslation } from 'react-i18next';

const dateData = [
  {
    key: 28,
    value: '4 weeks',
  },
  {
    key: 56,
    value: '8 weeks',
  },
  {
    key: 84,
    value: '12 weeks',
  },
];

const DemandReports: React.FC = () => {
  const [teamList, setTeamList] = useState<IResourceTeam[]>([]);
  const [expertRoles, setExpertRoles] = useState<IExpertRoles[]>([]);
  const [isFetchData, setFetchData] = useState<boolean>(false);
  const [businessUnits, setBusinessUnits] = useState<IBusinessUnit[]>([]);
  const { holidays } = useHolidayFetch();
  const [filter, setFilter] = useState<IFilterDemand>({
    resource_team_id: 0,
    sponsor_business_unit_id: 0,
    profile_role_id: 0,
    date_range: 28,
  });
  const [capacity, setCapacity] = useState<ICapacityProcessed[]>([]);
  const [dataCapacityFilter, setCapacityFilter] = useState<
    ICapacityProcessed[]
  >([]);
  const [data, setData] = useState<IDemandProcessed[]>([]);
  const [demandData, setDemandData] = useState<IDemandData[]>([]);
  const { t } = useTranslation();

  useEffectOnlyOnce(() => {
    getCapacityDashboardList();
  });
  //filter capacity
  useEffect(() => {
    const capacityFilter = capacity.filter(item => {
      return (
        (!filter.profile_role_id ||
          filter.profile_role_id === item.profile_role.id) &&
        (!filter.resource_team_id ||
          filter.resource_team_id === item.resource_team.id) &&
        (!filter.sponsor_business_unit_id ||
          filter.sponsor_business_unit_id === item.sponsor_business_unit.id)
      );
    });
    setCapacityFilter(capacityFilter);
  }, [
    capacity,
    filter.profile_role_id,
    filter.resource_team_id,
    filter.sponsor_business_unit_id,
  ]);
  useEffect(() => {
    const getDemandDashboardList = async () => {
      let params: IQueryParam[] = [];
      if (filter.resource_team_id) {
        params.push({
          key: 'resource_team_id',
          value: filter.resource_team_id,
        });
      }
      if (filter.sponsor_business_unit_id) {
        params.push({
          key: 'sponsor_business_unit_id',
          value: filter.sponsor_business_unit_id,
        });
      }
      if (filter.profile_role_id) {
        params.push({ key: 'profile_role_id', value: filter.profile_role_id });
      }
      const json = await getDemandDashboard(params);
      if (json.success) {
        setDemandData(json.data);
        setFetchData(true);
      }
    };
    getDemandDashboardList();
  }, [
    filter.profile_role_id,
    filter.resource_team_id,
    filter.sponsor_business_unit_id,
  ]);
  //get data filter
  useEffect(() => {
    if (!isFetchData) {
      setTeamList(
        uniqBy(
          capacity?.map(item => item.resource_team),
          'id',
        ) as IResourceTeam[],
      );
      setExpertRoles(
        uniqBy(
          capacity?.map(item => item.profile_role),
          'id',
        ) as IExpertRoles[],
      );
      setBusinessUnits(
        uniqBy(
          capacity?.map(item => item.sponsor_business_unit),
          'id',
        ),
      );
    }
  }, [capacity, isFetchData]);
  const getCapacityDashboardList = useCallback(async () => {
    const json = await getCapacityDashboard();
    if (json.success && json.data) {
      let dataGroup = Object.values(
        json.data
          ?.filter((item: ICapacityData) => item.profile_role.id)
          ?.filter((item: ICapacityData) => item.resource_team.id)
          ?.filter((item: ICapacityData) => item.profile_role.id)
          ?.reduce((r: any, e) => {
            let key =
              e.resource_team.id +
              '|sp' +
              e.sponsor_business_unit.id +
              '|pr' +
              e.profile_role.id;

            if (!r[key]) {
              r[key] = {
                resource_team: e.resource_team,
                sponsor_business_unit: e.sponsor_business_unit,
                profile_role: e.profile_role,
                capacity: 1,
              };
            } else {
              r[key].capacity += 1;
            }
            return r;
          }, {}) ?? {},
      ) as ICapacityProcessed[];
      setCapacity(dataGroup);
    }
  }, []);

  useEffect(() => {
    let dataGroup = Object.values(
      demandData?.reduce((r: any, e) => {
        let key =
          e.date +
          '|rt' +
          e.resource_team.name +
          '|bu' +
          e.sponsor_business_unit.name +
          '|pr' +
          e.profile_role.name;

        if (!r[key]) {
          r[key] = {
            date: e.date,
            resource_team: e.resource_team,
            sponsor_business_unit: e.sponsor_business_unit,
            profile_role: e.profile_role,
            demand: e.effort_by_day,
            capacity: 0,
          };
        } else {
          r[key].demand += e.effort_by_day;
        }
        return r;
      }, {}) ?? {},
    ) as IDemandProcessed[];
    setData(dataGroup);
  }, [demandData]);

  const selectFilter = (type: number, value: number) => {
    let filterTemp = { ...filter };

    switch (type) {
      case 1:
        filterTemp.sponsor_business_unit_id = value;
        setFilter(filterTemp);
        break;
      case 2:
        filterTemp.resource_team_id = value;
        setFilter(filterTemp);
        break;
      case 4:
        filterTemp.date_range = value;
        setFilter(filterTemp);
        break;
      default:
        filterTemp.profile_role_id = value;
        setFilter(filterTemp);
        break;
    }
  };

  return (
    <DemandDashboardContext.Provider
      value={{ data, capacity: dataCapacityFilter, filter, holidays }}
    >
      <Grid container>
        <Grid item xs={12}>
          <Box
            flexDirection="row"
            display="flex"
            alignItems="center"
            pt={3}
            pb={3}
          >
            <Box mr={1}>
              <Typography variant="h6">{t('business_units')}</Typography>
            </Box>
            <Box minWidth={'16.5%'} mr={1}>
              <Select
                id="business_unit"
                name="business_unit"
                value={filter.sponsor_business_unit_id}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => selectFilter(1, Number(event.target.value))}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem value={0}>All</MenuItem>
                {businessUnits.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
            <Box mr={1} ml={2}>
              <Typography variant="h6">{t('resource_team')}</Typography>
            </Box>
            <Box minWidth={'16.5%'} mr={1}>
              <Select
                id="resource_team"
                name="resource_team"
                value={filter.resource_team_id}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => selectFilter(2, Number(event.target.value))}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem value={0}>All</MenuItem>
                {teamList.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
            <Box mr={1} ml={2}>
              <Typography variant="h6">{t('roles')}</Typography>
            </Box>
            <Box minWidth={'16.5%'} mr={1}>
              <Select
                id="profile_role_id"
                name="profile_role_id"
                value={filter.profile_role_id}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => selectFilter(3, Number(event.target.value))}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem value={0}>All</MenuItem>
                {expertRoles.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
            <Box mr={1} ml={2}>
              <Typography variant="h6">{t('duration_filter')}</Typography>
            </Box>
            <Box mr={1} minWidth={'16.5%'}>
              <Select
                id="date_range"
                name="date_range"
                value={filter.date_range}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => selectFilter(4, Number(event.target.value))}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                {dateData.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item.key}>
                      {item.value}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
          </Box>

          <Box mt={4} mb={4} display="flex" justifyContent="center">
            <LineChartCustom />
          </Box>
        </Grid>
        <Grid container item xs={12}>
          <Box mt={6} mb={6} display="flex" width="100%">
            <TabPanelDetail />
          </Box>
        </Grid>
      </Grid>
    </DemandDashboardContext.Provider>
  );
};

export default DemandReports;
