export interface PieChartProps {
  data: IChartItem[];
  color?: string;
  type?: string;
  title: string;
}
export interface BarChartProps {
  title: string;
  data: IChartItem[];
}

export interface IChartItem {
  name: string;
  demand: number;
  capacity: number;
}
export interface IPieChartItem {
  name: string;
  value: number;
}

export interface CustomLabelProps {
  x: number;
  y: number;
  fill: string;
  value: number;
}
export interface TabPanelItemProps {
  title: string;
  type: number;
}
export interface LineChartItem {
  date: string;
  disabled?: boolean; // Weekend | holiday
  demand: number;
  capacity: number;
  holiday?: boolean;
}

export interface IDemandProcessed {
  date: Date | string;
  profile_role: {
    name: string;
    id: number;
  };
  resource_team: {
    id: number;
    name: string;
  };
  sponsor_business_unit: {
    id: number;
    name: string;
  };
  demand: number;
  capacity: number;
}
export interface ICapacityProcessed {
  sponsor_business_unit: ICommonObj;
  profile_role: ICommonObj;
  resource_team: ICommonObj;
  capacity: number;
}
export interface IFilterDemand {
  resource_team_id?: number;
  sponsor_business_unit_id?: number;
  profile_role_id?: number;
  date_range?: number;
}

interface ICommonObj {
  id: number;
  name: string;
}
export interface ICapacityGroup {
  name: string;
  value: number;
}
