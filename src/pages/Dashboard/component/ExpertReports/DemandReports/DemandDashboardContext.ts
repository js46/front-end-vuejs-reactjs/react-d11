import { createContext } from 'react';
import { IDemandProcessed, IFilterDemand, ICapacityProcessed } from './type';
interface IDemandDashboard {
  data: IDemandProcessed[];
  filter: IFilterDemand;
  capacity: ICapacityProcessed[];
  holidays?: Map<string, string>;
}
const initState: IDemandDashboard = {
  data: [],
  filter: {
    resource_team_id: 0,
    sponsor_business_unit_id: 0,
    profile_role_id: 0,
  },
  capacity: [],
};
export const DemandDashboardContext = createContext(initState);
