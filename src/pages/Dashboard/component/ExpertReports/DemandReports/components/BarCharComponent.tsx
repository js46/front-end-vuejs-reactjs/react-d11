import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Cell,
  CartesianGrid,
  Tooltip,
} from 'recharts';
import { BarChartProps } from '../type';
import { COLORS } from '../../../../../../constants';
import { Box } from '@material-ui/core';
//import { DemandDashboardContext } from '../DemandDashboardContext';

interface CustomLabelProps {
  x?: string | number;
  y?: string | number;
  stroke?: string;
  name?: string;
  value?: string;
}

const CustomizedLabel: React.FC<CustomLabelProps> = ({
  x,
  y,
  stroke,
  name,
  value,
}) => {
  return (
    <text
      x={x}
      y={y}
      dy={-10}
      dx={8}
      fontSize={13}
      fill={stroke}
      textAnchor="initial"
    >
      {name}
    </text>
  );
};

let tooltip: any;
const CustomTooltip: React.FC<{
  active?: any;
  payload?: any;
}> = ({ active, payload }) => {
  if (!active || !tooltip) return null;
  for (const bar of payload)
    if (bar.dataKey === tooltip)
      return (
        <div style={{ background: '#fff', padding: 10 }}>
          {bar.name}:
          <br />
          {bar.value.toFixed(1)}
        </div>
      );
  return null;
};

const height = window.innerHeight;

export const BarChartComponent: React.FC<BarChartProps> = ({ data, title }) => {
  return (
    <Box display="flex">
      <BarChart
        width={data.length * 200}
        height={height / 1.5}
        data={data}
        margin={{ top: 5, right: 0, left: 0, bottom: 40 }}
      >
        <XAxis dataKey="name" dy={25} />
        <YAxis domain={[dataMin => 0, dataMax => dataMax * 2]} />
        <Tooltip content={CustomTooltip} cursor={true} />
        <CartesianGrid vertical={false} stroke="#ebf3f0" />
        <Bar
          onMouseOver={() => (tooltip = 'demand')}
          dataKey="demand"
          barSize={70}
          name="Demand"
          label={<CustomizedLabel name="Demand" value="demand" stroke="#333" />}
          isAnimationActive={false}
        >
          {data.map((entry, index) => (
            <Cell key={index} opacity={0.7} fill={COLORS[index] ?? '#ededed'} />
          ))}
        </Bar>
        <Bar
          onMouseOver={() => (tooltip = 'capacity')}
          dataKey="capacity"
          barSize={70}
          name="Capacity"
          isAnimationActive={false}
          label={
            <CustomizedLabel name="Capacity" value="capacity" stroke="#333" />
          }
        >
          {data.map((entry, index) => (
            <Cell key={index} fill={COLORS[index] ?? '#ededed'} />
          ))}
        </Bar>
      </BarChart>
    </Box>
  );
};
