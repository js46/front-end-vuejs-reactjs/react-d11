import React, { useContext, useEffect, useState } from 'react';
import { PieChartComponent } from './PieChartComponent';
import { BarChartComponent } from './BarCharComponent';
import { Grid, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { chain, sumBy } from 'lodash';
import moment, { unitOfTime } from 'moment';
import { TabPanelItemProps, IChartItem } from '../type';
import { DemandDashboardContext } from '../DemandDashboardContext';
import { isWeekend } from '../../../../../../utils';
const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    height: '100%',
    marginTop: 20,
    paddingBottom: 40,
    paddingLeft: 25,
  },
}));

export const TabPanelItem: React.FC<TabPanelItemProps> = ({ title, type }) => {
  const classes = useStyles();
  const { data, filter, capacity, holidays } = useContext(
    DemandDashboardContext,
  );
  const [dataChart, setDataChart] = useState<IChartItem[]>([]);

  useEffect(() => {
    let field = '';

    switch (type) {
      case 1:
        field = 'sponsor_business_unit.name';
        break;
      case 2:
        field = 'resource_team.name';
        break;
      default:
        field = 'profile_role.name';
        break;
    }
    //sum capacity;
    const start = moment().startOf('isoweek' as unitOfTime.StartOf);
    const endDate = moment()
      .startOf('isoweek' as unitOfTime.StartOf)
      .add(filter.date_range, 'day');
    let workingDay = 0;

    while (start < endDate) {
      if (!isWeekend(start) && !holidays?.has(start.format('DD/MM'))) {
        workingDay += 1;
      }
      start.add(1, 'day');
    }
    let dataFilterDate = data.filter(item => {
      return moment(item.date) < endDate;
    });

    let dataDemandGroup = chain(dataFilterDate)
      .groupBy(field)
      .map((dataOrigin, name) => ({
        name: name,
        demand: Number(sumBy(dataOrigin, 'demand').toFixed(1)) ?? 0,
        capacity: 0,
      }))
      .value();

    let capacityGroup = chain(capacity)
      .groupBy(field)
      .map((dataOrigin, name) => {
        let demandFound = dataDemandGroup.find(item => item.name === name);
        return {
          name: name,
          demand: demandFound?.demand ?? 0,
          capacity: sumBy(dataOrigin, 'capacity') * workingDay,
        };
      })
      .value();

    if (capacityGroup.length) {
      setDataChart(capacityGroup as IChartItem[]);
    } else {
      setDataChart(dataDemandGroup as IChartItem[]);
    }
  }, [capacity, data, filter.date_range, holidays, type]);
  return (
    <Grid container className={classes.root}>
      <Grid item xs={4}>
        <Box display="flex" flexDirection="column">
          <Box mb={6}>
            <PieChartComponent
              key="demand"
              data={dataChart}
              type="demand"
              title={'Demand by ' + title}
            />

            <Box mt={4}>
              <PieChartComponent
                key="capacity"
                data={dataChart}
                title={'Capacity by ' + title}
              />
            </Box>
          </Box>
        </Box>
      </Grid>
      <Grid item xs={8}>
        <Box
          alignItems="flex-end"
          display="flex"
          height="100%"
          style={{
            maxWidth: '100%',
            overflowX: 'scroll',
          }}
        >
          <BarChartComponent key={title} title={title} data={dataChart} />
        </Box>
      </Grid>
    </Grid>
  );
};
