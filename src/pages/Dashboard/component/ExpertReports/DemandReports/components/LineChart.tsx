import React, { useContext, useEffect, useState } from 'react';
import { Box, Paper, Typography } from '@material-ui/core';
import {
  XAxis,
  YAxis,
  Legend,
  CartesianGrid,
  Tooltip,
  Line,
  LineChart,
  Label,
  ResponsiveContainer,
} from 'recharts';
import moment, { unitOfTime } from 'moment';

import { chain, sumBy, minBy } from 'lodash';
import { DemandDashboardContext } from '../DemandDashboardContext';
import { IDemandProcessed, LineChartItem } from '../type';
import { isWeekend, FormatNumber } from '../../../../../../utils';
import { COLORS } from '../../../../../../constants';

const CustomTooltip = React.memo(({ active, payload, label, today }: any) => {
  if (active) {
    let startWeek = payload[0].payload.start_week;
    let holidays = payload[0].payload.holidays;
    if (today.isAfter(startWeek, 'day')) {
      startWeek = today
        .startOf('isoweek' as unitOfTime.StartOf)
        .format('DD-MM-YYYY');
    }
    return (
      <Paper elevation={3} style={{ marginTop: -25 }}>
        <Box p={2} textAlign="left" minWidth="180px">
          <Typography variant="body1" component="p">
            <b>Demand on {label}</b>
          </Typography>
          <Box mt={1}>
            <Typography variant="body2" component="p">
              Start Week: <b>{startWeek}</b>
            </Typography>
            {!!holidays && holidays.length > 0 && (
              <Typography variant="body2" component="p">
                Holidays: <b>{holidays.join(', ')}</b>
              </Typography>
            )}
            <Typography
              variant="body2"
              style={{ color: COLORS[3] }}
              component="p"
            >
              Demand: {FormatNumber(payload[0].payload.demand, 1)}
            </Typography>
            <Typography
              variant="body2"
              style={{ color: COLORS[4] }}
              component="p"
            >
              Capacity: {payload[0].payload.capacity}
            </Typography>
          </Box>
        </Box>
      </Paper>
    );
  }
  return null;
});

export const LineChartCustom: React.FC = () => {
  const { data, filter, capacity, holidays } = useContext(
    DemandDashboardContext,
  );
  const [dataChart, setDataChart] = useState<LineChartItem[]>([]);
  const today = moment();
  useEffect(() => {
    const endDate = moment()
      .startOf('isoweek' as unitOfTime.StartOf)
      .add(filter.date_range, 'day');
    let start = moment().startOf('isoweek' as unitOfTime.StartOf);
    let dataGroup = groupDataByDays(data);

    let totalCapacity = sumBy(capacity, 'capacity');
    let chartDataProcessing: LineChartItem[] = [];
    while (start < endDate) {
      let itemChart = {
        date: start.format('YYYY-MM-DD'),
        demand: 0,
        capacity:
          isWeekend(start) || holidays?.has(start.format('DD/MM'))
            ? 0
            : totalCapacity ?? 0,
        holiday: holidays?.has(start.format('DD/MM')),
      };
      for (let item of dataGroup) {
        if (
          start.format('L') === moment(item.date).format('L') &&
          item.demand
        ) {
          itemChart.demand += Number(item.demand);
        }
      }

      chartDataProcessing.push(itemChart);

      start.add(1, 'day');
    }

    //group week
    let groupDataByWeekOfMonth = chain(chartDataProcessing)
      .groupBy(item => getLabelWeekInMonth(item.date))
      .map((dataOrigin, date) => {
        let startWeek = minBy(dataOrigin, function(o) {
          return new Date(o.date).getTime();
        })?.date;
        return {
          date: date,
          demand: sumBy(dataOrigin, 'demand'),
          capacity: sumBy(dataOrigin, 'capacity'),
          start_week: startWeek,
          holidays: dataOrigin
            .filter(item => item.holiday)
            ?.map(item => item.date),
        };
      })
      .value();
    setDataChart(groupDataByWeekOfMonth);
    //setDataChart(chartDataProcessing);
  }, [
    capacity,
    data,
    filter.date_range,
    filter.profile_role_id,
    filter.resource_team_id,
    filter.sponsor_business_unit_id,
    holidays,
  ]);
  function getLabelWeekInMonth(date: string) {
    // Copy date so don't affect original
    var start = new Date(date);
    // Move to previous Monday
    let monday;

    if (start.getDay() === 0) {
      monday = new Date(
        start.getFullYear(),
        start.getMonth(),
        start.getDate() - 6,
      );
    } else {
      monday = new Date(
        start.getFullYear(),
        start.getMonth(),
        start.getDate() - start.getDay() + 1,
      );
    }

    return (
      'Week ' +
      Math.ceil(monday.getDate() / 7) +
      ' - ' +
      moment(monday).format('MMM')
    );
  }

  const groupDataByDays = (data: IDemandProcessed[]) => {
    return chain(data)
      .groupBy('date')
      .map((data, id) => ({
        date: id,
        demand: sumBy(data, 'demand').toFixed(1),
        capacity: sumBy(data, 'capacity'),
      }))
      .value();
  };

  return (
    <ResponsiveContainer height={400} width="100%">
      <LineChart
        data={dataChart}
        margin={{ top: 25, right: 30, left: 20, bottom: 5 }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date" tickCount={5} />
        <YAxis
          type="number"
          domain={[dataMin => 0, dataMax => Math.floor(dataMax * 2)]}
        >
          <Label
            value="Person Day"
            position="insideLeft"
            angle={-90}
            style={{ textAnchor: 'middle' }}
          />
        </YAxis>
        <Tooltip content={<CustomTooltip today={today} />} />
        <Legend margin={{ top: 20 }} />
        <Line
          type="monotone"
          dataKey="demand"
          stroke={COLORS[3]}
          strokeWidth={2}
        />
        <Line
          type="monotone"
          dataKey="capacity"
          stroke={COLORS[4]}
          strokeWidth={2}
        />
      </LineChart>
    </ResponsiveContainer>
  );
};
