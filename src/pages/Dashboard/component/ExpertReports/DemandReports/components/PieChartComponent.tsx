import React, { useEffect, useState } from 'react';
import { Pie, PieChart, ResponsiveContainer, Cell, Tooltip } from 'recharts';
import { Box, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { PieChartProps, IPieChartItem } from '../type';
import { COLORS } from '../../../../../../constants';

const RADIAN = Math.PI / 180;
interface labelProps {
  name: string;
  percent?: number;
  stroke: string;
  index?: number;
  textAnchor: string;
  x: number;
  y: number;
  [key: string]: any;
}
const renderCustomizedLabel: React.FC<labelProps> = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN) * 1.1;
  const y = cy + radius * Math.sin(-midAngle * RADIAN) * 1.4;

  if (!percent) {
    return <></>;
  }
  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline="central"
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

export const PieChartComponent: React.FC<PieChartProps> = ({
  data,
  color,
  type,
  title,
}) => {
  const { i18n } = useTranslation();
  const [dataChart, setDataChart] = useState<IPieChartItem[]>([]);
  useEffect(() => {
    setDataChart(
      data
        .map(item => ({
          name: item.name,
          value: type ? item.demand : item.capacity,
        }))
        .filter(item => item.value),
    );
  }, [data, type]);
  return (
    <Box mt={2} mb={2}>
      {dataChart.length ? (
        <ResponsiveContainer height={200} width="100%">
          <PieChart>
            <Tooltip />
            <Pie
              data={data}
              dataKey={type ? 'demand' : 'capacity'}
              labelLine={false}
              label={renderCustomizedLabel}
              outerRadius={100}
              fill={color ?? '#8884d8'}
            >
              {dataChart.map((entry, index) => (
                <Cell
                  key={`cell-${index}`}
                  opacity={type ? 0.7 : 1}
                  fill={COLORS[index]}
                />
              ))}
            </Pie>
          </PieChart>
        </ResponsiveContainer>
      ) : (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          minHeight={180}
        >
          {i18n.t('no_data')}
        </Box>
      )}
      <Box mt={2}>
        <Typography color="inherit" component="h6" align="center">
          {title}
        </Typography>
      </Box>
    </Box>
  );
};
