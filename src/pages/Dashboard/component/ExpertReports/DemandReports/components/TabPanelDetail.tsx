import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Tabs } from '@material-ui/core';
import { TabPanel } from '../../../../../../components';
import { TabFluid } from '../../../../../../components/styled';
import { a11yProps } from '../../../../../../utils';
import { TabPanelItem } from './TabPanelItem';
const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    height: '100%',
    width: '100%',
  },
}));

export const TabPanelDetail: React.FC = () => {
  const classes = useStyles();
  const [activeSidebarTab, setSidebarActiveTab] = useState(0);
  const onChangeSidebarTab = (
    event: React.ChangeEvent<{}>,
    newValue: number,
  ) => {
    setSidebarActiveTab(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        value={activeSidebarTab}
        onChange={onChangeSidebarTab}
        aria-label="sidebar tabs"
      >
        <TabFluid label="Business Units" {...a11yProps('sidebar-tab', 0)} />
        <TabFluid label="Resource Teams" {...a11yProps('sidebar-tab', 1)} />
        <TabFluid label="Roles" {...a11yProps('sidebar-tab', 2)} />
      </Tabs>
      <TabPanel value={activeSidebarTab} index={0}>
        <TabPanelItem title="Business Units" key={0} type={1} />
      </TabPanel>
      <TabPanel value={activeSidebarTab} index={1}>
        <TabPanelItem title="Resource Teams" key={1} type={2} />
      </TabPanel>
      <TabPanel value={activeSidebarTab} index={2}>
        <TabPanelItem title="Roles" key={2} type={3} />
      </TabPanel>
    </div>
  );
};
