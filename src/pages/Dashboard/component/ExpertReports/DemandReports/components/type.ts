export interface PieChartProps {
  title: string;
  data: PieChartItem[];
  color?: string;
  type?: string;
}
export interface BarChartProps {
  title: string;
  data: BarChartItem[];
}

export interface PieChartItem {
  name: string;
  value: number;
  color?: string;
}

export interface BarChartItem {
  name: string;
  demand: number;
  colorByDemand: string;
  capacity: number;
  colorByCapacity: string;
}

export interface CustomLabelProps {
  x: number;
  y: number;
  fill: string;
  value: number;
}
export interface TabPanelItemProps {
  title: string;
  barChartData: BarChartItem[];
  demandPieChart: PieChartItem[];
  capacityPieChart: PieChartItem[];
}
