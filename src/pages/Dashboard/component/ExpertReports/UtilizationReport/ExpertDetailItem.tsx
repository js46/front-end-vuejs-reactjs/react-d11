import React, { useCallback, useState, createRef, useEffect } from 'react';
import { Box, IconButton, TableCell, TableRow } from '@material-ui/core';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { StyledReactiveList } from '../../../../../components/styled';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles } from '@material-ui/core/styles';
import { useToggleValue } from '../../../../../hooks';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import UtilizationFilterContext, {
  UtilizationFilter,
} from './UtilizationFilterContext';
import { first } from 'lodash';
import { IOpportunityExpert } from '../../../../../services/opportunity.services';
import {
  CustomGanttChart,
  CustomGanttChartDataT,
} from '../../../../../components';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';

interface ExpertDetailItemProps {
  id: number;
  name: string;
  team?: string;
  holidays?: Map<string, string>;
  businessColors?: Map<any, any>;
}
interface AggregationData {
  duration: {
    buckets: {
      key: number;
      doc_count: number;
      total?: { value: number };
      range?: { value: number };
      avg?: { value: number };
    }[];
  };
}
export const ExpertDetailItem: React.FC<ExpertDetailItemProps> = React.memo(
  props => {
    const ES_INDICES = useSelector(ConfigESIndex);
    const { id, name, team, holidays, businessColors } = props;
    const [isExpanded, toggleExpanded] = useToggleValue(false);
    const [workloads, setWorkloads] = useState<[number, string | number]>([
      0,
      0,
    ]);

    const animatedStyle = {
      transition: '0.3s all',
      transform: isExpanded ? 'rotate(90deg)' : 'rotate(0deg)',
    };

    const classes = useStyles({ isExpanded });
    const { t } = useTranslation();
    const tableRef = createRef<HTMLTableElement>();
    const [chartWidth, setChartWidth] = useState<number>();
    useEffect(() => {
      if (
        tableRef.current &&
        !chartWidth &&
        chartWidth !== tableRef.current.clientWidth - 50
      ) {
        setChartWidth(tableRef.current.clientWidth - 50);
      }
    }, [chartWidth, tableRef]);
    const defaultQuery = useCallback(
      (filters: UtilizationFilter) => () => {
        return {
          query: {
            bool: {
              must: [
                {
                  term: {
                    delete_flg: false,
                  },
                },
                {
                  term: {
                    'expert.id': id,
                  },
                },
                {
                  range: {
                    assignment_date: {
                      gte: moment(filters.from).format('YYYY-MM-DD'),
                      lte: moment(filters.to).format('YYYY-MM-DD'),
                    },
                  },
                },
              ],
            },
          },
          aggs: aggregationQuery,
        };
      },
      [id],
    );

    return (
      <UtilizationFilterContext.Consumer>
        {value => {
          return (
            <>
              <TableRow ref={tableRef}>
                <TableCell component="th" scope="row">
                  <Box display="flex">
                    <IconButton size="small" onClick={toggleExpanded}>
                      <ArrowForwardIosIcon
                        style={animatedStyle}
                        className={classes.icon}
                      />
                    </IconButton>
                    {name}
                  </Box>
                </TableCell>
                <TableCell component="th" scope="row">
                  {team}
                </TableCell>
                <TableCell component="th" scope="row">
                  {workloads[0]}
                </TableCell>
                <TableCell component="th" scope="row">
                  {workloads[1]}
                </TableCell>
              </TableRow>
              <TableRow className={classes.detailRow}>
                <TableCell colSpan={4}>
                  <ReactiveBase
                    app={ES_INDICES.opportunity_expert_index_name}
                    url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                    headers={{
                      Authorization: window.localStorage.getItem('jwt'),
                    }}
                  >
                    <StyledReactiveList
                      componentId="Opportunity List by expert"
                      dataField="opportunity.title.keyword"
                      sortBy={'asc'}
                      renderResultStats={() => null}
                      size={50}
                      pagination={false}
                      showResultStats={false}
                      renderNoResults={() => {
                        return (
                          <Box m={3} display="flex" justifyContent="center">
                            {t('no_data')}
                          </Box>
                        );
                      }}
                      defaultQuery={defaultQuery(value)}
                      onData={({ data, rawData }) => {
                        const aggregationData = rawData.aggregations as AggregationData;
                        const item = first(aggregationData.duration.buckets);
                        setWorkloads([
                          item?.doc_count ?? data.length,
                          item?.avg?.value ?? 0,
                        ]);
                      }}
                      render={({ data }) => {
                        if (!data.length || !isExpanded) return null;
                        const chartData: CustomGanttChartDataT[] = data.map(
                          (item: IOpportunityExpert) => {
                            let start = moment(item.assignment_date);
                            let activeDates: string[] = [];
                            while (
                              start.isSameOrBefore(
                                item.un_assignment_date,
                                'day',
                              )
                            ) {
                              activeDates.push(start.format('DD/MM'));
                              start.add(1, 'day');
                            }
                            return {
                              title: item.opportunity.title,
                              activeColor: businessColors?.get(
                                item.opportunity.sponsor_business_unit.name,
                              ),
                              activeDates,
                              customTooltip: [
                                {
                                  label: 'Duration',
                                  value: item.duration + ' days',
                                },
                                {
                                  label: 'Effort',
                                  value: item.effort + ' days',
                                },
                              ],
                            };
                          },
                        );
                        return (
                          <CustomGanttChart
                            data={chartData}
                            maxWidth={chartWidth}
                            holidays={holidays}
                          />
                        );
                      }}
                    />
                  </ReactiveBase>
                </TableCell>
              </TableRow>
            </>
          );
        }}
      </UtilizationFilterContext.Consumer>
    );
  },
);

const aggregationQuery = {
  duration: {
    terms: {
      field: 'expert.id',
    },
    aggs: {
      from: {
        min: {
          field: 'assignment_date',
        },
      },
      to: {
        max: {
          field: 'un_assignment_date',
        },
      },
      start: {
        sum: {
          field: 'assignment_date',
        },
      },
      end: {
        sum: {
          field: 'un_assignment_date',
        },
      },
      range: {
        bucket_script: {
          buckets_path: {
            from: 'from',
            to: 'to',
          },
          script: 'Math.round(params.to-params.from)/(60*60*24*1000)',
        },
      },
      total: {
        bucket_script: {
          buckets_path: {
            start: 'start',
            end: 'end',
          },
          script: 'Math.round(params.end-params.start)/(60*60*24*1000)',
        },
      },
      // total: {
      //   sum: {
      //     field: 'duration',
      //   },
      // },
      avg: {
        bucket_script: {
          buckets_path: {
            range: 'range',
            total: 'total',
          },
          script:
            'BigDecimal.valueOf(params.total/params.range).setScale(2, RoundingMode.HALF_UP)',
        },
      },
    },
  },
};

const useStyles = makeStyles<any, { isExpanded: boolean }>({
  icon: {
    fontSize: 15,
  },
  detailRow: {
    display: props => (props.isExpanded ? 'table-row' : 'none'),
  },
});
