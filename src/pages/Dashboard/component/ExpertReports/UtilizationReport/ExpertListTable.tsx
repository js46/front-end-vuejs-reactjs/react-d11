import React from 'react';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { StyledReactiveList } from '../../../../../components/styled';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { ExpertDetailItem } from './ExpertDetailItem';
import { useHolidayFetch } from '../../../../../hooks';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';

interface ExpertListTableProps {
  filterTeam: number;
  filterExpertIDs: number[];
  businessColors?: Map<any, any>;
}
export const ExpertListTable: React.FC<ExpertListTableProps> = React.memo(
  ({ filterTeam, filterExpertIDs, businessColors }) => {
    const ES_INDICES = useSelector(ConfigESIndex);
    const classes = useStyles();
    const { t } = useTranslation();
    const { holidays } = useHolidayFetch();

    const defaultQuery = () => {
      let queryFilter: {}[] = [
        {
          terms: {
            id: filterExpertIDs,
          },
        },
      ];
      if (filterTeam > 0) {
        queryFilter.push({
          term: {
            'resource_team.resource_team_id': filterTeam,
          },
        });
      }
      return {
        query: {
          bool: {
            must: queryFilter,
          },
        },
      };
    };

    return (
      <ReactiveBase
        app={ES_INDICES.user_profile_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <StyledReactiveList
          componentId="Expert list by opportunity"
          dataField="name.keyword"
          react={{
            and: ['Date'],
          }}
          sortBy={'asc'}
          renderResultStats={() => null}
          // size={10}
          // pages={5}
          pagination={true}
          showResultStats={false}
          renderNoResults={() => {
            return (
              <Box m={3} display="flex" justifyContent="center">
                <Typography>{t('no_data')}</Typography>
              </Box>
            );
          }}
          innerClass={{
            button: classes.buttonPagination,
            // resultsInfo: classes.resultsInfo,
            // sortOptions: classes.sortSelect,
          }}
          defaultQuery={defaultQuery}
          render={({ data }) => {
            if (!data.length) return null;
            return (
              <TableContainer className={classes.rootTableContainer}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Expert</TableCell>
                      <TableCell>Team</TableCell>
                      <TableCell>Total Workload</TableCell>
                      <TableCell>Avg Daily Workload</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data.map((item: any) => (
                      <ExpertDetailItem
                        key={item.id}
                        id={item.id}
                        name={item.name}
                        team={item.resource_team?.resource_team_name}
                        holidays={holidays}
                        businessColors={businessColors}
                      />
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            );
          }}
        />
      </ReactiveBase>
    );
  },
);

const useStyles = makeStyles(theme => ({
  rootTableContainer: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    overflowY: 'hidden',
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: `1px solid ${theme.palette.grey[400]}!important`,
    '&:active': {
      backgroundColor: `${theme.palette.primary.main}!important`,
      color: `${theme.palette.common.white}!important`,
    },
  },
  icon: {
    fontSize: 15,
  },
}));
