import React from 'react';
export type UtilizationFilter = {
  from: Date;
  to: Date;
};
const UtilizationFilterContext = React.createContext<UtilizationFilter>({
  from: new Date(),
  to: new Date(),
});

export default UtilizationFilterContext;
