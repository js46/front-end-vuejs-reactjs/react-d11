import React, { useCallback, useState } from 'react';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { Box, Grid, MenuItem, Select, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  StyledDateRange,
  StyledMultiDropdownList,
} from '../../../../../components/styled';
import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';
import { DateUtils } from 'react-day-picker';
import { compact, isEqual } from 'lodash';
import { ExpertListTable } from './ExpertListTable';
import moment from 'moment';
import UtilizationFilterContext from './UtilizationFilterContext';
import {
  getResourceTeam,
  IResourceTeam,
} from '../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../hooks';
import { PieChartComponent, IChartItem } from '../../../../../components';
import { COLORS } from '../../../../../constants';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { useTranslation } from 'react-i18next';

const UtilizationReport = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const { t } = useTranslation();
  const [teamList, setTeamList] = useState<IResourceTeam[]>([]);
  const [teamData, setTeamData] = useState<IChartItem[]>([]);
  const [teamSelected, setTeamSelected] = useState(0);
  const [businessUnitData, setBusinessUnitData] = useState<IChartItem[]>([]);
  const [expertData, setExpertData] = useState<IChartItem[]>([]);
  const [expertIDs, setExpertIDs] = useState<number[]>([]);
  const [businessColors, setBusinessColors] = useState<Map<any, any>>();
  const [dateRange, setDateRange] = useState<[Date, Date]>([
    moment()
      .subtract(0, 'days')
      .toDate(),
    moment()
      .add(1, 'month')
      .toDate(),
  ]);

  useEffectOnlyOnce(() => {
    getResourceTeamList();
  });

  const getResourceTeamList = useCallback(async () => {
    const json = await getResourceTeam();
    if (json.success && json.data) {
      setTeamList(json.data.list as IResourceTeam[]);
    }
  }, []);

  const defaultQuery = useCallback(() => {
    let queryTeam: {}[] = [];
    if (teamSelected > 0) {
      queryTeam = [
        {
          term: {
            'expert.resource_team.id': teamSelected,
          },
        },
      ];
    }
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                delete_flg: false,
              },
            },
            ...queryTeam,
          ],
        },
      },
    };
  }, [teamSelected]);
  return (
    <ReactiveBase
      app={ES_INDICES.opportunity_expert_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{ Authorization: window.localStorage.getItem('jwt') }}
    >
      <Grid container>
        <Grid item xs={12}>
          {/* Start Filter */}
          <Box flexDirection="row" display="flex" alignItems="center">
            <Typography variant="h6">{t('resource_team')}</Typography>
            <Box width="20%" ml={2} mr={2}>
              <Select
                id="resource_team"
                name="resource_team"
                value={teamSelected}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => setTeamSelected(event.target.value)}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem value={0}>All</MenuItem>
                {teamList.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
            <Typography variant="h6">{t('assignment_date')} </Typography>
            <Box mt={2} mb={2} ml={2}>
              <StyledDateRange
                flexRow={true}
                componentId="Date"
                innerClass={{
                  'input-container': 'input-date',
                }}
                defaultValue={{ start: dateRange[0], end: dateRange[1] }}
                onValueChange={({ start, end }) => setDateRange([start, end])}
                placeholder={{
                  start: 'From',
                  end: 'To',
                }}
                queryFormat="date"
                dataField="assignment_date"
                dayPickerInputProps={{
                  formatDate,
                  format: FORMAT,
                  parseDate,
                }}
              />
            </Box>
          </Box>
          {/*  Start pie charts */}
          {(!!businessUnitData.length ||
            !!teamData.length ||
            !!expertData.length) && (
            <Grid container>
              <Grid item xs={12} sm={6} md={4}>
                <PieChartComponent
                  data={businessUnitData}
                  type="value"
                  title={'Oppo# by BUs'}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <PieChartComponent
                  data={teamData}
                  type="value"
                  title={'Oppo# By Resource Teams'}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <PieChartComponent
                  data={expertData}
                  type="value"
                  title={'Oppo# By Experts (Top 10)'}
                  pieLimit={10}
                />
              </Grid>
            </Grid>
          )}
        </Grid>
        {/*  Start list Table */}
        <Grid item xs={12}>
          <Grid item container direction="row">
            <Grid item xs={12}>
              <UtilizationFilterContext.Provider
                value={{
                  from: dateRange[0],
                  to: dateRange[1],
                }}
              >
                <ExpertListTable
                  filterTeam={teamSelected}
                  filterExpertIDs={expertIDs}
                  businessColors={businessColors}
                />
              </UtilizationFilterContext.Provider>
            </Grid>
          </Grid>
        </Grid>

        {/* Start hidden data query */}
        <div style={{ visibility: 'hidden', display: 'none' }}>
          <StyledMultiDropdownList
            componentId="Resource Team"
            dataField="expert.resource_team.name.keyword"
            showCount={false}
            react={{ and: ['Date'] }}
            transformData={data => {
              let arr: IChartItem[] = data.map((item: any) => ({
                name: item.key,
                value: item.doc_count,
              }));
              if (!isEqual(arr, teamData)) setTeamData(arr);
              return data;
            }}
            defaultQuery={defaultQuery}
            render={() => null}
          />
          <StyledMultiDropdownList
            componentId="Business Units"
            react={{ and: ['Date'] }}
            dataField="opportunity.sponsor_business_unit.name.keyword"
            showSearch={false}
            innerClass={{
              label: 'multiList',
            }}
            transformData={data => {
              let businessUnitColors = new Map();
              let arr: IChartItem[] = data.map((item: any, index: number) => {
                businessUnitColors.set(item.key, COLORS[index]);
                return {
                  name: item.key,
                  value: item.doc_count,
                  color: COLORS[index],
                };
              });
              if (!isEqual(arr, businessUnitData)) {
                setBusinessUnitData(arr);
                setBusinessColors(businessUnitColors);
              }
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery}
            size={Math.pow(10, 5)}
          />
          <StyledMultiDropdownList
            componentId="Experts"
            react={{ and: ['Date'] }}
            dataField="expert.name.keyword"
            showSearch={false}
            transformData={data => {
              let arr: IChartItem[] = data.map((item: any) => ({
                name: item.key,
                value: item.doc_count,
              }));
              if (!isEqual(arr, expertData)) setExpertData(arr);
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery}
            size={Math.pow(10, 5)}
          />
          <StyledMultiDropdownList
            componentId="Expert IDs"
            react={{ and: ['Date'] }}
            dataField="expert.id"
            showSearch={false}
            showCount={false}
            transformData={data => {
              let IDs: number[] = data.map((item: any) => parseInt(item.key));
              IDs = compact(IDs);
              if (!isEqual(expertIDs, IDs)) setExpertIDs(IDs);
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery}
            size={Math.pow(10, 5)}
          />
        </div>
      </Grid>
    </ReactiveBase>
  );
};

const FORMAT = 'dd-MM-yyyy';

function formatDate(date: Date, format: string, locale: Locale) {
  return dateFnsFormat(date, format, { locale });
}
interface IParseDateProps {
  str: string;
  format: string;
  locale: number | Date;
}
function parseDate(date: IParseDateProps) {
  const parsed = dateFnsParse(date.str, date.format, date.locale);
  if (DateUtils.isDate(parsed)) {
    return parsed;
  }
  return undefined;
}

export default UtilizationReport;
