import loadable from '@loadable/component';

export * from './OpportunityReports/ScopingBacklog';
export * from './Sidebar';
export const InFlightOpportunity = loadable(() =>
  import('./OpportunityReports/InFlightOpportunity'),
);
export const UtilizationReport = loadable(() =>
  import('./ExpertReports/UtilizationReport'),
);
export const DemandReports = loadable(() =>
  import('./ExpertReports/DemandReports'),
);
export * from './OpportunityReports';
export * from './ExpertReports';
