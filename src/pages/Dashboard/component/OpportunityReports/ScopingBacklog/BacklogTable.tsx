import React from 'react';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@material-ui/core';
import { StyledReactiveList } from '../../../../../components/styled';
import { makeStyles } from '@material-ui/core/styles';
import { IOpportunity } from '../../../../../services/opportunity.services';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { HeadCellProps, TableHeadSorter } from '../../../../../components';
import { useTableHeadSorter } from '../../../../../hooks';

interface BacklogTableProps {
  filterBusiness: string;
}
const tableFields = [
  'title.keyword',
  'sponsor_business_unit.name.keyword',
  'created_by.name.keyword',
  'priority_level.keyword',
  'created_at',
  'requested_completion_date',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'title.keyword',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'sponsor_business_unit.name.keyword',
    label: 'Business unit',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Proposer',
    disablePadding: true,
  },
  {
    id: 'priority_level.keyword',
    label: 'Priority',
    disablePadding: true,
  },
  {
    id: 'created_at',
    label: 'Date submitted',
    disablePadding: true,
  },
  {
    id: 'requested_completion_date',
    label: 'Completion date',
    disablePadding: true,
  },
];
export const BacklogTable: React.FC<BacklogTableProps> = React.memo(
  ({ filterBusiness }) => {
    const classes = useStyles();
    const { order, orderBy, handleRequestSort } = useTableHeadSorter(
      tableFields,
      'title.keyword',
    );

    const { t } = useTranslation();

    const defaultQuery = () => {
      let queryBusiness =
        filterBusiness !== 'all'
          ? [
              {
                term: {
                  'sponsor_business_unit.name.keyword': filterBusiness,
                },
              },
            ]
          : [];
      return {
        query: {
          bool: {
            must: [
              {
                term: {
                  del_flag: false,
                },
              },
              ...queryBusiness,
            ],
          },
        },
      };
    };

    return (
      <StyledReactiveList
        componentId="OpportunityList"
        dataField={orderBy}
        sortBy={order}
        renderResultStats={() => null}
        // size={10}
        // pages={5}
        pagination={true}
        showResultStats={false}
        renderNoResults={() => {
          return (
            <Box m={3} display="flex" justifyContent="center">
              {t('no_data')}
            </Box>
          );
        }}
        innerClass={{
          button: classes.buttonPagination,
          // resultsInfo: classes.resultsInfo,
          // sortOptions: classes.sortSelect,
        }}
        defaultQuery={defaultQuery}
        render={({ data }) => {
          if (data.length > 0) {
            return (
              <TableContainer classes={{ root: classes.rootTableContainer }}>
                <Table classes={{ root: classes.rootTable }}>
                  <TableHeadSorter
                    onRequestSort={handleRequestSort}
                    order={order}
                    orderBy={orderBy}
                    cells={headCells}
                  />
                  <TableBody>
                    {data.map((item: IOpportunity) => {
                      return (
                        <TableRow key={item.id}>
                          <TableCell>{item.title}</TableCell>
                          <TableCell>
                            {item.sponsor_business_unit.name}
                          </TableCell>
                          <TableCell>{item.created_by?.name}</TableCell>
                          <TableCell>
                            {item.opp_priority_score?.priority_level}
                          </TableCell>
                          <TableCell>
                            {moment(item.created_at).format('DD-MM-YYYY')}
                          </TableCell>
                          <TableCell>
                            {moment(item.requested_completion_date).format(
                              'DD-MM-YYYY',
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            );
          } else {
            return (
              <Box display="flex" justifyContent="center" mt={2} mb={2}>
                {t('no_opp')}
              </Box>
            );
          }
        }}
      />
    );
  },
);

const useStyles = makeStyles(theme => ({
  rootTableContainer: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    overflowY: 'hidden',
  },
  rootTable: {
    minWidth: 732,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: `1px solid ${theme.palette.grey[400]}!important`,
    '&:active': {
      backgroundColor: `${theme.palette.primary.main}!important`,
      color: `${theme.palette.common.white}!important`,
    },
  },
  icon: {
    fontSize: 15,
  },
}));
