import React, { useState } from 'react';
import { Box, Grid, MenuItem, Select, Typography } from '@material-ui/core';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { isEqual } from 'lodash';

import { StyledMultiDropdownList } from '../../../../../components/styled';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BacklogTable } from './BacklogTable';
import { PieChartComponent, IChartItem } from '../../../../../components';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { useTranslation } from 'react-i18next';

export const ScopingBacklog = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const [businessUnits, setBusinessUnits] = useState<string[]>([]);
  const [businessUnitData, setBusinessUnitData] = useState<IChartItem[]>([]);
  const [teamData, setTeamData] = useState<IChartItem[]>([]);
  const [businessSelected, setBusinessSelected] = useState<string>('all');
  const { t } = useTranslation();
  const defaultQuery = () => {
    let queryBusiness =
      businessSelected !== 'all'
        ? [
            {
              term: {
                'sponsor_business_unit.name.keyword': businessSelected,
              },
            },
          ]
        : [];
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                del_flag: false,
              },
            },
            ...queryBusiness,
          ],
        },
      },
    };
  };

  return (
    <ReactiveBase
      app={ES_INDICES.opportunity_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{ Authorization: window.localStorage.getItem('jwt') }}
    >
      <Grid container direction="column" spacing={2}>
        <Grid item xs={12}>
          <Box flexDirection="row" display="flex" alignItems="center">
            <Box width={165} mr={1}>
              <Typography variant="h6">{t('business_units')}</Typography>
            </Box>
            <Box width={'20%'}>
              <Select
                id="business_unit"
                name="business_unit"
                value={businessSelected}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => setBusinessSelected(event.target.value)}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem value={'all'}>All</MenuItem>
                {businessUnits.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item}>
                      {item}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
          </Box>
        </Grid>
        <div style={{ visibility: 'hidden' }}>
          <StyledMultiDropdownList
            componentId="Sponsor Business Units"
            dataField="sponsor_business_unit.name.keyword"
            showSearch={false}
            innerClass={{
              label: 'multiList',
            }}
            transformData={data => {
              let arr: IChartItem[] = data.map((item: any) => ({
                name: item.key,
                value: item.doc_count,
              }));
              if (!businessUnits.length && data?.length) {
                const list = data.map((item: any) => item.key);
                setBusinessUnits(list);
              }
              if (!isEqual(arr, businessUnitData)) setBusinessUnitData(arr);
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery}
          />
          <StyledMultiDropdownList
            componentId="Resource Team"
            dataField="resource_team.name.keyword"
            showSearch={false}
            innerClass={{
              label: 'multiList',
            }}
            react={{
              and: ['Date'],
            }}
            placeholder="Select Resource team"
            transformData={data => {
              let arr: IChartItem[] = data.map((item: any) => ({
                name: item.key,
                value: item.doc_count,
              }));
              if (!isEqual(arr, teamData)) setTeamData(arr);
              return data;
            }}
            defaultQuery={defaultQuery}
          />
        </div>
        <Grid item container>
          <Grid item container direction="row" spacing={3}>
            <Grid item xs={12} sm={6}>
              <PieChartComponent
                data={businessUnitData}
                type="value"
                title={'Oppo# by BUs'}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <PieChartComponent
                data={teamData}
                type="value"
                title={'Oppo# by Resource Teams'}
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <BacklogTable filterBusiness={businessSelected} />
          </Grid>
        </Grid>
      </Grid>
    </ReactiveBase>
  );
};
