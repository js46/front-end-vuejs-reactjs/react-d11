import React, { useState } from 'react';
import { ScopingBacklog, InFlightOpportunity } from '..';
import { Grid, Box, Select, MenuItem, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useTranslation } from 'react-i18next';

enum SelectRenderer {
  ScopingBacklog = 'ScopingBacklog',
  InFlightOpportunity = 'InFlightOpportunity',
}

export const OpportunityReport = () => {
  const [currentSelected, setCurrentSelected] = useState(
    SelectRenderer.ScopingBacklog,
  );
  const { t } = useTranslation();

  const handleChange = (
    event: React.ChangeEvent<{ name?: string; value: any }>,
  ) => {
    setCurrentSelected(event.target.value);
  };

  const renderStepView = (currentStep: SelectRenderer) => {
    switch (currentStep) {
      case SelectRenderer.ScopingBacklog:
        return <ScopingBacklog />;
      case SelectRenderer.InFlightOpportunity:
        return <InFlightOpportunity />;
      default:
        return <ScopingBacklog />;
    }
  };

  return (
    <Grid container alignItems="center">
      <Grid item xs={6}>
        <Box pt={2} pb={3}>
          <Typography variant="h4">{t('opp_report')}</Typography>
        </Box>
      </Grid>
      <Grid item xs={6} justify="flex-end" container>
        <Select
          variant="outlined"
          value={currentSelected}
          id="role_name_select"
          name="role_name_select"
          onChange={handleChange}
          MenuProps={{
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'left',
            },
            getContentAnchorEl: null,
          }}
          IconComponent={ExpandMoreIcon}
        >
          <MenuItem key={0} value={SelectRenderer.ScopingBacklog}>
            Scoping Backlog
          </MenuItem>
          <MenuItem key={1} value={SelectRenderer.InFlightOpportunity}>
            Inflight Opportunity
          </MenuItem>
        </Select>
      </Grid>
      {renderStepView(currentSelected)}
    </Grid>
  );
};
