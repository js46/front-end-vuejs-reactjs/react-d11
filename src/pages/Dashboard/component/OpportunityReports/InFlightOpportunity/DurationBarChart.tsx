import React from 'react';
import { COLORS } from '../../../../../constants';
import { Box, Typography } from '@material-ui/core';
import {
  BarChart,
  CartesianGrid,
  YAxis,
  XAxis,
  Tooltip,
  Legend,
  Bar,
} from 'recharts';
import { useTranslation } from 'react-i18next';

interface DurationBarChartProps {
  data: {
    key: number;
    doc_count: number;
    from?: { value: number; value_as_string: string };
    to?: { value: number; value_as_string: string };
    durationCustom: { value: number };
  }[];
}
interface ChartDataProps {
  label: string;
  from: number;
  to?: number;
  count: number;
}
export const DurationBarChart: React.FC<DurationBarChartProps> = React.memo(
  props => {
    const { data } = props;
    const { t } = useTranslation();
    const handleData = () => {
      let chartData: ChartDataProps[] = [
        {
          label: '1-5',
          from: 1,
          to: 5,
          count: 0,
        },
        {
          label: '6-10',
          from: 6,
          to: 10,
          count: 0,
        },
        {
          label: '11-15',
          from: 11,
          to: 15,
          count: 0,
        },
        {
          label: '16-20',
          from: 16,
          to: 20,
          count: 0,
        },
        {
          label: '> 20',
          from: 21,
          count: 0,
        },
      ];
      let barChartData: {
        name: string;
        'Duration (days), Opps#': number;
      }[] = [];
      chartData.map(item => {
        for (let opportunityDuration of data) {
          let duration = opportunityDuration.durationCustom.value;
          if (item.from <= duration && (!item.to || item.to >= duration)) {
            item.count += 1;
          }
        }
        return barChartData.push({
          name: item.label,
          'Duration (days), Opps#': item.count,
        });
      });

      return barChartData;
    };

    return (
      <Box
        style={{
          maxWidth: '100%',
          overflowX: 'scroll',
        }}
      >
        <Box mb={3}>
          <Typography
            variant="body1"
            style={{ fontSize: 13, color: '#000', fontWeight: 'bold' }}
          >
            {t('opp_duration')}
          </Typography>
        </Box>
        <BarChart
          width={handleData().length * 100}
          height={250}
          data={handleData()}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend wrapperStyle={{ fontSize: 12, marginTop: 10 }} />
          <Bar dataKey="Duration (days), Opps#" fill={COLORS[0]} />
        </BarChart>
      </Box>
    );
  },
);
