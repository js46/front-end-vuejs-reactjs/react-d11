import React, { useState } from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@material-ui/core';
import {
  getOpportunitySizes,
  IOpportunitySizes,
} from '../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../hooks';

interface OpportunitySizeTableProps {
  data: { key: string; doc_count: number }[];
}
export const OpportunitySizeTable: React.FC<OpportunitySizeTableProps> = ({
  data,
}) => {
  const [loading, setLoading] = useState(true);
  const [opportunitySizes, setOpportunitySizes] = useState<IOpportunitySizes[]>(
    [],
  );

  useEffectOnlyOnce(() => {
    getOpportunitySizeLists();
  });

  const getOpportunitySizeLists = async () => {
    const response = await getOpportunitySizes();
    if (response.success) {
      setOpportunitySizes(response.data.list);
    }
    setLoading(false);
  };

  if (loading) return null;
  let total = 0;
  for (let item of data) {
    total += item.doc_count;
  }
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            {opportunitySizes.map((item, key) => (
              <TableCell
                style={{ whiteSpace: 'nowrap', padding: '16px 10px' }}
                key={key}
              >
                {item.t_shirt_size}
              </TableCell>
            ))}
            <TableCell style={{ whiteSpace: 'nowrap', padding: '16px 6px' }}>
              <strong>Total count</strong>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            {opportunitySizes.map((item, key) => (
              <TableCell key={key}>
                {data.find(datum => parseInt(datum.key) === item.id)
                  ?.doc_count || 0}
              </TableCell>
            ))}
            <TableCell>
              <strong>{total}</strong>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};
