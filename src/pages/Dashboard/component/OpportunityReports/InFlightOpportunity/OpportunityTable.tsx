import React, { useCallback, useState } from 'react';
import { Box, Table, TableBody, TableContainer } from '@material-ui/core';
import { OpportunityRowGanttChart } from './OpportunityRowGanttChart';
import { IOpportunity } from '../../../../../services/opportunity.services';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import {
  StyledMultiDropdownList,
  StyledReactiveList,
} from '../../../../../components/styled';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import {
  HeadCellProps,
  TableHeadSorter,
  IChartItem,
} from '../../../../../components';
import { useHolidayFetch, useTableHeadSorter } from '../../../../../hooks';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';

interface AggregationData {
  opportunityDurations: {
    buckets: {
      key: number;
      doc_count: number;
      from: { value: number; value_as_string: string };
      to: { value: number; value_as_string: string };
    }[];
  };
}
interface OpportunityTableProps {
  team: number;
  from: Date;
  to: Date;
  setBusinessUnitData: (data: IChartItem[]) => void;
  setSizeData: (data: { key: string; doc_count: number }[]) => void;
  expertColors?: Map<any, any>;
}
const tableFields = [
  'title.keyword',
  'created_by.name.keyword',
  'opportunity_phase.name.keyword',
  'priority_level.keyword',
  'current_experts.assignment_date',
  'current_experts.un_assignment_date',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'title.keyword',
    label: 'Opportunity',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Proposer',
    disablePadding: true,
  },
  {
    id: 'opportunity_phase.name.keyword',
    label: 'Phase',
    disablePadding: true,
  },
  {
    id: 'priority_level.keyword',
    label: 'Priority',
    disablePadding: true,
  },
  {
    id: 'current_experts.assignment_date',
    label: 'Start date',
    disablePadding: true,
  },
  {
    id: 'current_experts.un_assignment_date',
    label: 'End date',
    disablePadding: true,
  },
];
export const OpportunityTable: React.FC<OpportunityTableProps> = React.memo(
  ({ team, from, to, setBusinessUnitData, setSizeData, expertColors }) => {
    const { holidays } = useHolidayFetch();
    const ES_INDICES = useSelector(ConfigESIndex);
    const [opportunityDuration, setOpportunityDuration] = useState<
      Map<number, [string, string]>
    >();
    const classes = useStyles();
    const { order, orderBy, handleRequestSort } = useTableHeadSorter(
      tableFields,
      'title.keyword',
    );

    const defaultQuery = useCallback(
      (queryAgg: boolean) => () => {
        let queryTeam: {}[] = [],
          queryAggregation = queryAgg ? { aggs: aggregationQuery } : {};
        if (team > 0) {
          queryTeam = [
            {
              term: {
                'current_experts.resource_team.id': team,
              },
            },
          ];
        }
        return {
          query: {
            bool: {
              must: [
                {
                  term: {
                    del_flag: false,
                  },
                },
                {
                  range: {
                    'current_experts.assignment_date': {
                      gte: moment(from).format('YYYY-MM-DD'),
                      lte: moment(to).format('YYYY-MM-DD'),
                    },
                  },
                },
                ...queryTeam,
              ],
            },
          },
          sort: [
            {
              [orderBy]: {
                order,
              },
            },
          ],
          ...queryAggregation,
        };
      },
      [from, order, orderBy, team, to],
    );

    const handleAggregationData = useCallback(
      (aggs: AggregationData) => {
        let durationMap: typeof opportunityDuration = new Map();
        for (let item of aggs.opportunityDurations.buckets) {
          durationMap.set(item.key, [
            item.from.value_as_string,
            item.to.value_as_string,
          ]);
        }
        setOpportunityDuration(durationMap);
      },
      [opportunityDuration],
    );

    return (
      <ReactiveBase
        app={ES_INDICES.opportunity_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <Box display="none">
          <StyledMultiDropdownList
            componentId="Sponsor Business Unit"
            dataField="sponsor_business_unit.name.keyword"
            showSearch={false}
            innerClass={{
              label: 'multiList',
            }}
            transformData={data => {
              let arr: IChartItem[] = data.map((item: any) => ({
                name: item.key,
                value: item.doc_count,
              }));
              setBusinessUnitData(arr);
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery(false)}
          />
          <StyledMultiDropdownList
            componentId="Opportunity Size report"
            dataField="opportunity_size.id"
            showSearch={false}
            innerClass={{
              label: 'multiList',
            }}
            transformData={data => {
              setSizeData(data);
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery(false)}
          />
        </Box>
        <StyledReactiveList
          componentId="Opportunity List"
          dataField={'id'}
          renderResultStats={() => null}
          pagination={true}
          showResultStats={false}
          scrollOnChange={false}
          renderNoResults={() => null}
          defaultQuery={defaultQuery(true)}
          onData={({ rawData }) => {
            handleAggregationData(rawData.aggregations);
          }}
          render={({ data }) => {
            if (!data.length) return <></>;
            return (
              <TableContainer classes={{ root: classes.rootTableContainer }}>
                <Table classes={{ root: classes.rootTable }}>
                  <TableHeadSorter
                    onRequestSort={handleRequestSort}
                    order={order}
                    orderBy={orderBy}
                    cells={headCells}
                  />
                  <TableBody>
                    {data.map((item: IOpportunity, key: number) => {
                      let duration = opportunityDuration?.get(item.id);
                      if (duration) {
                        duration = duration.map(item =>
                          moment(item).format('DD/MM/YYYY'),
                        ) as [string, string];
                      }
                      return (
                        <OpportunityRowGanttChart
                          key={key}
                          data={item}
                          duration={duration}
                          holidays={holidays}
                          expertColors={expertColors}
                        />
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            );
          }}
          innerClass={{
            button: classes.buttonPagination,
          }}
        />
      </ReactiveBase>
    );
  },
);
const aggregationQuery = {
  opportunityDurations: {
    terms: {
      field: 'id',
    },
    aggs: {
      from: {
        min: {
          field: 'current_experts.assignment_date',
        },
      },
      to: {
        max: {
          field: 'current_experts.un_assignment_date',
        },
      },
    },
  },
};
const useStyles = makeStyles({
  rootTableContainer: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    overflowY: 'hidden',
  },
  rootTable: {
    minWidth: 732,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
});
