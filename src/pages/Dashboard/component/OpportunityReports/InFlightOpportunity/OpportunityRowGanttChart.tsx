import React, { useState, createRef, useEffect } from 'react';
import { IOpportunity } from '../../../../../services/opportunity.services';
import { Box, IconButton, TableCell, TableRow } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import {
  CustomGanttChart,
  CustomGanttChartDataT,
} from '../../../../../components';
import moment from 'moment';

interface OpportunityRowGanttChartProps {
  data: IOpportunity;
  duration?: [string, string];
  holidays?: Map<string, string>;
  expertColors?: Map<any, any>;
}
const useStyles = makeStyles({
  cell: {
    whiteSpace: 'nowrap',
  },
  icon: {
    fontSize: 15,
  },
});

export const OpportunityRowGanttChart: React.FC<OpportunityRowGanttChartProps> = ({
  data,
  duration,
  holidays,
  expertColors,
}) => {
  const classes = useStyles();
  const tableRef = createRef<HTMLTableElement>();
  const [isCollapse, setCollapse] = useState(false);
  const animatedStyle = {
    transition: '0.3s all',
    transform: isCollapse ? 'rotate(90deg)' : 'rotate(0deg)',
  };
  const [chartWidth, setChartWidth] = useState<number>();
  useEffect(() => {
    if (
      tableRef.current &&
      !chartWidth &&
      chartWidth !== tableRef.current.clientWidth - 32
    ) {
      setChartWidth(tableRef.current.clientWidth - 32);
    }
  }, [chartWidth, tableRef]);

  let chartData: CustomGanttChartDataT[] = [];
  if (data.current_experts) {
    chartData = data.current_experts.map((item, index) => {
      let start = moment(item.assignment_date);
      let activeDates: string[] = [];
      while (start.isSameOrBefore(item.un_assignment_date, 'day')) {
        activeDates.push(start.format('DD/MM'));
        start.add(1, 'day');
      }
      return {
        title: item.name,
        activeDates,
        activeColor: expertColors?.get(item.name),
        customTooltip: [
          {
            label: 'Duration',
            value: item.duration + ' days',
          },
          {
            label: 'Effort',
            value: item.effort + ' days',
          },
        ],
      };
    });
  }

  return (
    <>
      <TableRow ref={tableRef}>
        <TableCell className={classes.cell}>
          <Box display="flex">
            <IconButton size="small" onClick={() => setCollapse(!isCollapse)}>
              <ArrowForwardIosIcon
                style={animatedStyle}
                className={classes.icon}
              />
            </IconButton>
            {data.title}
          </Box>
        </TableCell>
        <TableCell className={classes.cell}>{data.created_by?.name}</TableCell>
        <TableCell>{data.opportunity_phase?.display_name}</TableCell>
        <TableCell>{data.opp_priority_score?.priority_level || ''}</TableCell>
        <TableCell className={classes.cell}>{duration?.[0]}</TableCell>
        <TableCell className={classes.cell}>{duration?.[1]}</TableCell>
      </TableRow>
      {isCollapse && (
        <TableRow>
          <TableCell colSpan={6}>
            {chartWidth && chartData.length > 0 ? (
              <Box>
                <CustomGanttChart
                  data={chartData}
                  maxWidth={chartWidth}
                  holidays={holidays}
                />
              </Box>
            ) : (
              <Box textAlign="center">Empty</Box>
            )}
          </TableCell>
        </TableRow>
      )}
    </>
  );
};
