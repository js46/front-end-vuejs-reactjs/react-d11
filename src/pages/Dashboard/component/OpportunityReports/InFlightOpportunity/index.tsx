import React, { useCallback, useState } from 'react';
import {
  Box,
  Grid,
  MenuItem,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import moment from 'moment';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import {
  StyledDateRange,
  StyledMultiDropdownList,
  StyledReactiveList,
} from '../../../../../components/styled';
import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';
import { DateUtils } from 'react-day-picker';
import { first, isEqual, last } from 'lodash';
import { OpportunityTable } from './OpportunityTable';
import { DurationBarChart } from './DurationBarChart';
import { OpportunitySizeTable } from './OpportunitySizeTable';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { useTranslation } from 'react-i18next';
import {
  getResourceTeam,
  IResourceTeam,
} from '../../../../../services/references.services';
import { useEffectOnlyOnce } from '../../../../../hooks';
import NumberFormat from 'react-number-format';
import { PieChartComponent, IChartItem } from '../../../../../components';
import { COLORS } from '../../../../../constants';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';

const FORMAT = 'dd-MM-yyyy';
function formatDate(date: Date, format: string, locale: Locale) {
  return dateFnsFormat(date, format, { locale });
}
interface IParseDateProps {
  str: string;
  format: string;
  locale: number | Date;
}
interface AggregationData {
  opportunitySizes: {
    buckets: { key: number; doc_count: number; count: { value: number } }[];
  };
  opportunityDurations: {
    buckets: {
      key: number;
      doc_count: number;
      from: { value: number; value_as_string: string };
      to: { value: number; value_as_string: string };
      durationCustom: { value: number };
    }[];
  };
  avgDuration: {
    value: number | null;
  };
}
function parseDate(date: IParseDateProps) {
  const parsed = dateFnsParse(date.str, date.format, date.locale);
  if (DateUtils.isDate(parsed)) {
    return parsed;
  }
  return undefined;
}

const InFlightOpportunity: React.FC = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const [loading, setLoading] = useState(true);
  const [teamList, setTeamList] = useState<IResourceTeam[]>([]);
  const [durationData, setDurationData] = useState({ min: 0, max: 0, avg: 0 });
  const [dateRange, setDateRange] = useState<[Date, Date]>([
    moment().toDate(),
    moment()
      .add(1, 'month')
      .toDate(),
  ]);
  const [businessUnitData, setBusinessUnitData] = useState<IChartItem[]>([]);
  const [expertData, setExpertData] = useState<IChartItem[]>([]);
  const [teamData, setTeamData] = useState<IChartItem[]>([]);
  const [sizeData, setSizeData] = useState<
    { key: string; doc_count: number }[]
  >([]);
  const [durationTableData, setDurationTableData] = useState<
    {
      key: number;
      doc_count: number;
      durationCustom: { value: number };
    }[]
  >([]);
  const [teamSelected, setTeamSelected] = useState(0);
  const [expertColors, setExpertColors] = useState<Map<any, any>>();

  useEffectOnlyOnce(() => {
    getResourceTeamList();
  });

  const getResourceTeamList = useCallback(async () => {
    const json = await getResourceTeam();
    if (json.success && json.data) {
      setTeamList(json.data.list as IResourceTeam[]);
    }
  }, []);

  const handleAggregationData = async (aggData: AggregationData) => {
    const { avgDuration, opportunityDurations } = aggData;
    let min = first(opportunityDurations.buckets)?.durationCustom.value ?? 0,
      max = last(opportunityDurations.buckets)?.durationCustom.value ?? 0;
    setDurationData({
      min,
      max,
      avg: avgDuration.value ?? 0,
    });
    setDurationTableData(opportunityDurations.buckets);
  };

  const defaultQuery = useCallback(
    (queryAgg: boolean) => () => {
      let queryTeam: {}[] = [],
        aggs = queryAgg ? { aggs: aggregationQuery } : {};
      if (teamSelected > 0) {
        queryTeam = [
          {
            term: {
              'expert.resource_team.id': teamSelected,
            },
          },
        ];
      }
      return {
        query: {
          bool: {
            must: [
              {
                term: {
                  delete_flg: false,
                },
              },
              ...queryTeam,
            ],
          },
        },
        ...aggs,
      };
    },
    [teamSelected],
  );

  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <ReactiveBase
      app={ES_INDICES.opportunity_expert_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{ Authorization: window.localStorage.getItem('jwt') }}
    >
      <Grid container>
        {/* Start Filter */}
        <Grid item xs={12}>
          <Box
            flexDirection="row"
            display="flex"
            alignItems="center"
            pt={3}
            pb={3}
          >
            <Box mr={1}>
              <Typography variant="h6">{t('resource_team')}</Typography>
            </Box>
            <Box minWidth={'20%'}>
              <Select
                id="resource_team"
                name="resource_team"
                value={teamSelected}
                fullWidth
                onChange={(
                  event: React.ChangeEvent<{ name?: string; value: any }>,
                ) => setTeamSelected(event.target.value)}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem value={0}>All</MenuItem>
                {teamList.map((item, index: number) => {
                  return (
                    <MenuItem key={index} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Box>
            <Box mr={1} ml={2}>
              <Typography variant="h6">{t('assignment_date')}</Typography>
            </Box>
            <Box display="flex">
              <StyledDateRange
                flexRow={true}
                componentId="Date"
                innerClass={{
                  'input-container': 'input-date',
                }}
                placeholder={{
                  start: 'From',
                  end: 'To',
                }}
                queryFormat="date"
                dataField="assignment_date"
                showClear={false}
                dayPickerInputProps={{
                  formatDate,
                  format: FORMAT,
                  parseDate,
                }}
                defaultValue={{ start: dateRange[0], end: dateRange[1] }}
                onValueChange={({ start, end }) => setDateRange([start, end])}
              />
            </Box>
          </Box>
        </Grid>
        {/* Start hidden data query */}
        <div style={{ visibility: 'hidden', display: 'none' }}>
          <StyledMultiDropdownList
            componentId="Resource Team"
            dataField="expert.resource_team.name.keyword"
            showCount={false}
            react={{ and: ['Date'] }}
            transformData={data => {
              let arr: IChartItem[] = data.map((item: any) => ({
                name: item.key,
                value: item.doc_count,
              }));
              if (!isEqual(arr, teamData)) setTeamData(arr);
              return data;
            }}
            defaultQuery={defaultQuery(false)}
            render={() => null}
          />
          <StyledMultiDropdownList
            componentId="Experts"
            react={{ and: ['Date'] }}
            dataField="expert.name.keyword"
            showSearch={false}
            transformData={data => {
              let expertColors = new Map();
              let arr: IChartItem[] = data.map((item: any, index: number) => {
                expertColors.set(item.key, COLORS[index]);
                return {
                  name: item.key,
                  value: item.doc_count,
                  color: COLORS[index],
                };
              });
              if (!isEqual(arr, expertData)) {
                setExpertData(arr);
                setExpertColors(expertColors);
              }
              return data;
            }}
            render={() => null}
            defaultQuery={defaultQuery(false)}
          />
        </div>
        {/* Start Charts */}
        <Grid container item spacing={4} justify="center" alignItems="center">
          <Grid
            item
            container
            xs={12}
            sm={6}
            lg={5}
            direction="column"
            spacing={3}
          >
            <Grid item xs>
              <Box mb={2}>
                <Typography variant="body1" className={classes.tableTitle}>
                  {t('opp_size')}
                </Typography>
              </Box>
              {!loading && <OpportunitySizeTable data={sizeData} />}
            </Grid>
            <Grid item xs>
              <Box mb={2}>
                <Typography variant="body1" className={classes.tableTitle}>
                  {t('opportunity_duration')}
                </Typography>
              </Box>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell className={classes.durationCell}>
                        {t('avg_duration')}
                      </TableCell>
                      <TableCell className={classes.durationCell}>
                        {t('min')}
                      </TableCell>
                      <TableCell className={classes.durationCell}>
                        {t('max')}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell>
                        <NumberFormat
                          value={durationData.avg}
                          displayType={'text'}
                          decimalScale={2}
                        />
                      </TableCell>
                      <TableCell>{durationData.min}</TableCell>
                      <TableCell>{durationData.max}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
          <Grid item xs sm={6} lg={7}>
            <Box pt={3} pb={3}>
              {!loading && <DurationBarChart data={durationTableData} />}
            </Box>
          </Grid>
          {!loading && (
            <>
              <Grid item xs={12} sm={6} md={4}>
                <PieChartComponent
                  data={businessUnitData}
                  type="value"
                  title={'Oppo# by BUs'}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <PieChartComponent
                  data={teamData}
                  type="value"
                  title={'Oppo# by Resource Teams'}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <PieChartComponent
                  data={expertData}
                  type="value"
                  title={'Oppo# by Experts (Top 10)'}
                  pieLimit={10}
                />
              </Grid>
            </>
          )}
        </Grid>
        {/* Start Table */}
        <Grid item xs={12}>
          <StyledReactiveList
            componentId="OpportunityExpert"
            dataField="opportunity.id"
            // aggregationField="opportunity.id"
            react={{ and: ['Date'] }}
            renderResultStats={() => null}
            // size={2}
            pages={5}
            pagination={false}
            showResultStats={false}
            scrollOnChange={false}
            renderNoResults={() => {
              return (
                <Box m={3} display="flex" justifyContent="center">
                  {t('no_data')}
                </Box>
              );
            }}
            defaultQuery={defaultQuery(true)}
            onData={({ rawData }) => {
              handleAggregationData(rawData.aggregations).then(() => {
                setLoading(false);
              });
            }}
            render={({ data }) => {
              if (loading || !data.length) return null;
              return (
                <Grid item container direction="row">
                  <Grid item xs={12}>
                    <OpportunityTable
                      team={teamSelected}
                      from={dateRange[0]}
                      to={dateRange[1]}
                      setBusinessUnitData={(result: IChartItem[]) => {
                        if (!isEqual(result, businessUnitData))
                          setBusinessUnitData(result);
                      }}
                      setSizeData={result => {
                        if (!isEqual(result, sizeData)) setSizeData(result);
                      }}
                      expertColors={expertColors}
                    />
                  </Grid>
                </Grid>
              );
            }}
          />
        </Grid>
      </Grid>
    </ReactiveBase>
  );
};
const useStyles = makeStyles({
  tableTitle: {
    fontSize: 13,
    color: '#000',
    fontWeight: 'bold',
  },
  durationCell: {
    width: '33%',
  },
});
const aggregationQuery = {
  opportunityDurations: {
    terms: {
      field: 'opportunity.id',
    },
    aggs: {
      from: {
        min: {
          field: 'assignment_date',
        },
      },
      to: {
        max: {
          field: 'un_assignment_date',
        },
      },
      durationCustom: {
        bucket_script: {
          buckets_path: {
            startTime: 'from',
            endTime: 'to',
          },
          script:
            'Math.round(params.endTime - params.startTime)/(24*60*60*1000)',
        },
      },
      final_sort: {
        bucket_sort: {
          sort: [
            {
              durationCustom: {
                order: 'asc',
              },
            },
          ],
        },
      },
    },
  },
  // opportunitySizes: {
  //   terms: {
  //     field: 'opportunity_size.id',
  //   },
  //   aggs: {
  //     count: {
  //       cardinality: {
  //         field: 'opportunity.id',
  //       },
  //     },
  //   },
  // },
  avgDuration: {
    avg_bucket: {
      buckets_path: 'opportunityDurations>durationCustom',
    },
  },
};
export default InFlightOpportunity;
