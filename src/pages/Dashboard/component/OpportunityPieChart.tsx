import React from 'react';
import Chart from 'react-google-charts';
export type PieChartData = [string | number, number][];
interface OpportunityPieChartProps {
  data: PieChartData;
  title: string;
  pieLimit?: number;
  entityLabels: [string, string];
}
export const OpportunityPieChart: React.FC<OpportunityPieChartProps> = React.memo(
  props => {
    const { title, data, pieLimit, entityLabels } = props;
    let displayData = [];
    if (pieLimit && pieLimit < data.length) {
      let total = 0,
        limitSum = 0;
      for (let i = 0; i < pieLimit; i++) {
        limitSum += data[i][1];
        displayData.push(data[i]);
      }
      for (let item of data) {
        total += item[1];
      }
      displayData.push(['Other', total - limitSum]);
    } else {
      displayData = data;
    }

    return (
      <Chart
        width={'100%'}
        height={'300px'}
        chartType="PieChart"
        loader={<div />}
        data={[entityLabels, ...displayData]}
        options={{
          title,
          legend: { maxLines: 1 },
          chartArea: {
            left: 5,
            right: 5,
          },
          titleTextStyle: {
            fontFamily: 'inherit',
            fontSize: 13,
          },
        }}
        chartEvents={[
          {
            eventName: 'select',
            callback: props => console.log('chart click', props),
          },
        ]}
      />
    );
  },
);
