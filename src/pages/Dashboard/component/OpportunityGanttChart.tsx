import React from 'react';
import moment from 'moment';
import Chart from 'react-google-charts';
import { Box } from '@material-ui/core';
interface OpportunityGanttChartProps {
  data?: {
    id: number;
    title: string;
    assignment_date: Date;
    un_assignment_date: Date;
  }[];
  addRow?: boolean;
}
const columns = [
  { type: 'string', label: 'ID' },
  { type: 'string', label: 'Title' },
  { type: 'string', label: 'Resource' },
  { type: 'date', label: 'Start Date' },
  { type: 'date', label: 'End Date' },
  { type: 'number', label: 'Duration' },
  { type: 'number', label: 'Percentage Done' },
  { type: 'string', label: 'Dependencies' },
];
export const OpportunityGanttChart: React.FC<OpportunityGanttChartProps> = ({
  data,
  addRow,
}) => {
  // console.log(totalOpportunityExpert);
  const calculatePercentageDone = (start: Date, end: Date): number => {
    if (moment().isBefore(moment(start))) return 0;
    if (moment().isAfter(moment(end))) return 100;
    const duration = moment(end).diff(moment(start), 'd');
    return Math.round((moment().diff(moment(start), 'days') / duration) * 100);
  };
  const chartData =
    data?.map((item, index: number) => [
      item.id,
      item.title,
      null,
      moment(item.assignment_date)
        .startOf('d')
        .toDate(),
      moment(item.un_assignment_date)
        .startOf('d')
        .toDate(),
      null,
      calculatePercentageDone(item.assignment_date, item.un_assignment_date),
      null,
    ]) || [];

  let rowHeight = 58;
  if (addRow) {
    rowHeight += 50;
  }
  const chartHeight =
    chartData.length === 1 ? '116px' : `${42 * chartData.length + rowHeight}px`;

  return (
    <>
      <Box>
        <Chart
          width={'100%'}
          height={chartHeight}
          chartType="Gantt"
          loader={<div>Loading Chart</div>}
          data={[columns, ...chartData]}
          options={{
            gantt: {
              criticalPathEnabled: false,
              defaultStartDate: new Date(),
            },
          }}
        />
      </Box>
    </>
  );
};
