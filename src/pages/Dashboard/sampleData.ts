export const dataOpportunityByUsers = [
  ['OppByUser', 'Opportunity per Business Unit'],
  ['opp 1', 13],
  ['opp 2', 3],
  ['opp 3', 1.4],
  ['opp 4', 2.3],
];

export const dataOpportunityByResourceTeams = [
  ['OppByResourceTeam', 'Opportunity per Resource Team'],
  ['opp 1', 13],
  ['opp 2', 9],
  ['opp 3', 24],
  ['opp 4', 53],
];
export const dataOpportunityByExperts = [
  ['OppByExpert', 'Opportunity per Expert'],
  ['opp 1', 33],
  ['opp 2', 11],
  ['opp 3', 14],
  ['opp 4', 23],
];
