import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Opportunity, Notes, TodoList, Attachments } from './component';
const MyProfile = React.lazy(() => import('../MyProfile'));
const MyStuff: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/my-stuff" component={Opportunity} />
      <Route exact path="/my-stuff/notes" component={Notes} />
      <Route exact path="/my-stuff/todo-list" component={TodoList} />
      <Route exact path="/my-stuff/attachments" component={Attachments} />
      <Route exact path="/profile" component={MyProfile} />
    </Switch>
  );
};
export default MyStuff;
