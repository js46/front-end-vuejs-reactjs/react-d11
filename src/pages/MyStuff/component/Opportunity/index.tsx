import React, { useCallback, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  MenuItem,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@material-ui/core';
import {
  ReactiveBase,
  ReactiveComponent,
  SelectedFilters,
} from '@appbaseio/reactivesearch';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { ConfigESIndex } from '../../../../store/config/selector';
import {
  StyledReactiveList,
  StyledSingleDropdownList,
} from '../../../../components/styled';
import { useHistory, useParams } from 'react-router-dom';
import { getComparator, stableSort } from '../../../../commons/util';
import { IOpportunity } from '../../../../services/opportunity.services';
import { TimeLineChartOpp } from '../TimeLineChartOpp';
import {
  ESLoader,
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from '../../../../components';
import { useTableHeadSorter } from '../../../../hooks';
import { UserProfileState } from '../../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  selectBox: {
    height: '42px!important',
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '0px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
        fontSize: '14px!important',
        fontFamily: 'Helvetica Neue,Arial',
      },
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  roundedBorder: {
    borderRadius: 6,
    '& .MuiOutlinedInput-root': {
      borderRadius: 6,
    },
    '& .MuiOutlinedInput-input': {
      padding: '10px 14px',
    },
    height: '36px',
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));
interface TimeLineChartItemOpp {
  title: string;
  name: string;
  assignment_date?: string;
  un_assignment_date?: string;
}

const tableFields = [
  'id',
  'title.keyword',
  'opp_priority_score.priority_level.keyword',
  'opp_priority_score.priority_score',
  'opportunity_phase.display_name.keyword',
  'created_by.name.keyword',
  'assignees',
  'created_at',
  'time',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: 'Opp',
    disablePadding: true,
  },
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'opp_priority_score.priority_level.keyword',
    label: 'Priority',
    disablePadding: true,
  },
  {
    id: 'opp_priority_score.priority_score',
    label: 'Priority score',
    disablePadding: true,
  },
  {
    id: 'opportunity_phase.display_name.keyword',
    label: 'Phase',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Created by',
    disablePadding: true,
  },
  {
    id: 'assignees',
    label: 'Assignees',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'time',
    label: 'Last updated',
    disablePadding: true,
  },
];

interface ProcessProps {
  classes: ReturnType<typeof useStyles>;
  setQuery: any;
}

function ProgressSelect(props: ProcessProps) {
  const { classes, setQuery } = props;

  const [valueSelectProgress, setValueSelectProgress] = useState('in_progress');
  const handleChangeProgress = (
    event: React.ChangeEvent<{ value: unknown }>,
  ) => {
    if (event.target.value === 'drafts') {
      setQuery({
        query: {
          bool: {
            must: [
              {
                term: {
                  phase: 'shape',
                },
              },
              {
                term: {
                  phase: 'capture',
                },
              },
            ],
          },
        },
      });
      setValueSelectProgress(event.target.value as string);
    }
    if (event.target.value === 'completed') {
      setQuery({
        query: {
          bool: {
            must_not: [
              {
                term: {
                  completion_date: '0001-01-01T00:00:00Z',
                },
              },
            ],
          },
        },
      });
      setValueSelectProgress(event.target.value as string);
    }
    if (event.target.value === 'in_progress') {
      setQuery({
        query: {
          bool: {
            must_not: [
              {
                term: {
                  phase: 'shape',
                },
              },
              {
                term: {
                  phase: 'capture',
                },
              },
            ],
            must: [
              {
                term: {
                  completion_date: '0001-01-01T00:00:00Z',
                },
              },
            ],
          },
        },
      });
      setValueSelectProgress(event.target.value as string);
    }
  };
  return (
    <Select
      fullWidth
      id="sort_by"
      name="sort_by"
      value={valueSelectProgress}
      // placeholder="Title A-Z"
      onChange={handleChangeProgress}
      className={classes.roundedBorder}
      variant="outlined"
    >
      <MenuItem value="in_progress">In Progress</MenuItem>
      <MenuItem value="drafts">Drafts</MenuItem>
      <MenuItem value="completed">Completed</MenuItem>
    </Select>
  );
}

export const Opportunity: React.FC = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'time',
    SortOrder.DESC,
  );
  const [dataOppTable, setDataOppTable] = useState<any>();
  const { t } = useTranslation();

  const userProfileState = useSelector(UserProfileState);
  let { id } = useParams();
  let history = useHistory();

  function handleClick(onClickOpp: IOpportunity) {
    if (onClickOpp.opportunity_phase?.id === 1) {
      history.push(`/capture-opportunity/${onClickOpp.id}`);
    } else {
      history.push(`/opportunity/${onClickOpp.id}`);
    }
  }
  const userId = id ? id : userProfileState?.id;

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                del_flag: false,
              },
            },
            {
              bool: {
                should: [
                  {
                    term: {
                      'created_by.id': userId,
                    },
                  },
                  {
                    term: {
                      'current_expert_review.expert_review.id': userId,
                    },
                  },
                  {
                    bool: {
                      filter: {
                        query_string: {
                          query: 'current_experts.user_id:(' + userId + ')',
                        },
                      },
                    },
                  },
                ],
              },
            },
          ],
        },
      },
      sort: {
        last_updated_at: { order: 'desc' },
      },
    };
  };

  const getAssigneeNames = useCallback((item: IOpportunity) => {
    let names: string[] = [];
    if (item.current_expert_review?.expert_review?.name) {
      names.push(item.current_expert_review.expert_review.name);
    }
    if (item.current_experts) {
      names = [...names, ...item.current_experts.map(item => item.name)];
    }
    return names.join(', ');
  }, []);

  return (
    <Box mt={2}>
      <ReactiveBase
        app={ES_INDICES.opportunity_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <Grid container direction="column" spacing={2}>
          <Grid item container justify="space-between">
            <Grid item md={4}>
              <Typography variant="h4">{t('opportunities')}</Typography>
            </Grid>
            <Grid
              item
              container
              justify="flex-end"
              md={8}
              lg={8}
              xl={8}
              alignItems="center"
            >
              <Box flexDirection="row" display="flex" alignItems="center">
                <Typography color="textSecondary">
                  {t('sponsor_business_unit')}
                </Typography>
                <Box minWidth={185} m={1}>
                  <StyledSingleDropdownList
                    componentId="sponsorBusiness"
                    dataField="sponsor_business_unit.name.keyword"
                    placeholder="All"
                    showCount={false}
                  />
                </Box>
                <Typography color="textSecondary">{t('status')}</Typography>
                <Box minWidth={120} m={2} className={classes.selectBox}>
                  <ReactiveComponent
                    componentId="status" // a unique id we will refer to later
                    react={{
                      and: ['sponsorBusiness'],
                    }}
                    render={({ setQuery }) => (
                      <ProgressSelect classes={classes} setQuery={setQuery} />
                    )}
                  />
                </Box>
              </Box>
            </Grid>
          </Grid>
          <Grid item container justify="space-between">
            <Grid item md={6}>
              <Typography color="textSecondary" variant="body1">
                {t('click_opp_view_details')}
              </Typography>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <SelectedFilters />
            <StyledReactiveList
              react={{
                and: ['sponsorBusiness', 'status'],
              }}
              size={10}
              componentId="backlogOpp"
              paginationAt="bottom"
              pages={5}
              pagination={true}
              dataField={orderBy}
              sortBy={order}
              showLoader
              loader={<ESLoader />}
              renderNoResults={() => ''}
              renderResultStats={function(stats) {
                return (
                  <Grid container>
                    <Grid item xs={10}>
                      <Typography component="h6" align="left">
                        {stats.numberOfResults.toLocaleString()} results found
                        in {stats.time.toLocaleString()}ms
                      </Typography>
                    </Grid>
                    <Grid item xs={1} />
                  </Grid>
                );
              }}
              onData={({ data }) => {
                let dataFilter = stableSort(data, getComparator(order, orderBy))
                  .map((item: any, index: number) => {
                    return {
                      title: item?.title,
                      current_experts: item?.current_experts,
                    };
                  })
                  .reduce((prev: any, currObj: any) => {
                    let dataExpert = [];
                    if (currObj.current_experts) {
                      for (
                        let i = 0;
                        i <= currObj.current_experts.length;
                        i++
                      ) {
                        dataExpert.push({
                          title: currObj.title,
                          name: currObj?.current_experts[i]?.name,
                          assignment_date:
                            currObj?.current_experts[i]?.assignment_date,
                          un_assignment_date:
                            currObj?.current_experts[i]?.un_assignment_date,
                        });
                      }
                    }
                    return prev.concat(dataExpert);
                  }, [])
                  .filter((item: TimeLineChartItemOpp) => {
                    let startDate = moment(item.assignment_date);
                    let endDate = moment(item.un_assignment_date);

                    return (
                      !!item.name &&
                      startDate.isValid() &&
                      endDate.isValid() &&
                      endDate.unix() > startDate.unix()
                    );
                  });
                setDataOppTable(dataFilter);
              }}
              render={({ data, loading }) => {
                if (loading) return null;
                return (
                  <TableContainer>
                    <Table style={{ minWidth: 850 }}>
                      <TableHeadSorter
                        onRequestSort={handleRequestSort}
                        order={order}
                        orderBy={orderBy}
                        cells={headCells}
                      />
                      <TableBody>
                        {data.map((item: IOpportunity) => (
                          <TableRow
                            key={item.id}
                            hover
                            onClick={() => handleClick(item)}
                            classes={{ root: classes.rowLink }}
                          >
                            <TableCell component="th" scope="row">
                              {item?.id}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {item?.title}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {item?.opp_priority_score?.priority_level === ''
                                ? 'Low'
                                : item?.opp_priority_score?.priority_level}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {item?.opp_priority_score?.priority_score}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {item?.opportunity_phase?.display_name}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {item?.created_by?.name}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {getAssigneeNames(item)}
                            </TableCell>
                            <TableCell component="th" scope="row">
                              {moment(item.last_updated_at).format(
                                'DD/MM/YYYY HH:mm:ss',
                              )}
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                );
              }}
              innerClass={{
                button: classes.buttonPagination,
                resultsInfo: classes.resultsInfo,
                sortOptions: classes.sortSelect,
                select: classes.sortSelect,
              }}
              defaultQuery={customQuery}
            />
          </Grid>
          <Grid item container spacing={2}>
            {dataOppTable && dataOppTable.length > 0 && (
              <TimeLineChartOpp data={dataOppTable} />
            )}
          </Grid>
        </Grid>
      </ReactiveBase>
    </Box>
  );
};

export default Opportunity;
