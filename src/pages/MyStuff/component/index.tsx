import loadable from '@loadable/component';
export * from './TimeLineChartOpp';
export const Attachments = loadable(() => import('./Attachments'));
export const Notes = loadable(() => import('./Notes'));
export const Opportunity = loadable(() => import('./Opportunity'));
export const TodoList = loadable(() => import('./TodoList'));
