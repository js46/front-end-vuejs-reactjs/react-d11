import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Box,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  Table,
  TableBody,
  Typography,
  Button,
  Dialog,
  DialogActions,
} from '@material-ui/core';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import InfoIcon from '@material-ui/icons/Info';
import moment from 'moment';
import { ConfigESIndex } from '../../../../store/config/selector';
import { NoteResponseT } from '../../../../services/playbook.services';
import { NoteDialog } from '../../../../components/NoteDialog';
import { DeleteDialog, TableAction } from '../../../../components';
import {
  addNoteForUser,
  updateNoteOfUser,
  deleteNoteOfUser,
} from '../../../../services/notes.services';
import { useDisableMultipleClick, useUnmounted } from '../../../../hooks';
import { Message } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../../store/user/selector';
import { notifyError } from '../../../../store/common/actions';

const useStyles = makeStyles(theme => ({
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  headTable: {
    minWidth: 732,
  },
  headCell: {
    whiteSpace: 'nowrap',
  },
  tableRow: {
    transition: '0.3s all',
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: '#eee',
    },
  },
}));
interface Data {
  note_id: number;
  name: string;
  created_by: string;
  created_at: string;
  action: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'note_id',
    label: 'Id',
  },
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'created_by',
    label: 'Created by',
  },
  {
    id: 'created_at',
    label: 'Created at',
  },
  {
    id: 'action',
    label: 'Action',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            className={classes.headCell}
            align={headCell.id === 'action' ? 'center' : undefined}
            style={headCell.id === 'action' ? { width: '140px' } : undefined}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface NotesProps {}
export const Notes: React.FC<NotesProps> = () => {
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState);
  const dispatch = useDispatch();

  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const isUnmounted = useUnmounted();

  const emptyNoteContent = {
    name: '',
    author: userProfileState.full_name,
    content: '',
    date: new Date(),
  };
  const initNoteContent = {
    name: '',
    author: userProfileState.full_name,
    content: '',
    date: new Date(),
  };
  // const { t } = useTranslation();
  const [userNotes, setUserNotes] = useState<NoteResponseT[]>();
  const [currentTime, setCurrentTime] = useState(1);
  const [isOpenAddNote, setOpenAddNote] = useState(false);

  const [noteContent, setNoteContent] = useState(initNoteContent);
  const [currentEditNoteId, setCurrentEditNoteId] = useState<number>();
  const [noteToDeleteId, setNoteToDeleteId] = useState<number>();

  const [isInformationDialogOpen, setInformationDialogOpen] = useState(false);
  const [informationDialog, setInformationDialog] = useState<NoteResponseT>();

  const handleChangeNoteContent = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    event.persist();
    switch (event.target.name) {
      case 'note_name': {
        setNoteContent({ ...noteContent, name: event.target.value });
        break;
      }
      case 'note_detail_content': {
        setNoteContent({ ...noteContent, content: event.target.value });
        break;
      }
    }
  };

  const handleRefetchNote = () => {
    setTimeout(() => {
      if (isUnmounted.current) return;
      setCurrentTime(currentTime + 1);
    }, 1000);
  };

  const handleAddNote = async (content: string) => {
    if (isSubmitting) return;
    await debounceFn();
    const payload = {
      title: noteContent.name,
      content: content,
    };
    const response = await addNoteForUser(payload, userProfileState.id);
    if (response.success) {
      setOpenAddNote(false);
      setNoteContent(emptyNoteContent);
      handleRefetchNote();
    }
    endRequest();
  };
  const handleEditNote = async (content: any) => {
    if (!currentEditNoteId || isSubmitting) return;
    await debounceFn();
    const payload = {
      title: noteContent.name,
      content: content,
      created_by_user_id: userProfileState.id,
    };
    const response = await updateNoteOfUser(payload, currentEditNoteId);
    if (response.success) {
      setNoteContent(emptyNoteContent);
      setCurrentEditNoteId(undefined);
      handleRefetchNote();
    } else {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
    endRequest();
  };

  const handleRemoveNote = async (idNote: number) => {
    if (isSubmitting) return;
    await debounceFn();
    const response = await deleteNoteOfUser(idNote);
    if (response.success) {
      setNoteToDeleteId(undefined);
      handleRefetchNote();
    } else {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
    endRequest();
  };
  const handleClickNote = () => {
    setOpenAddNote(true);
  };

  const customQuery = () => {
    return {
      query: {
        bool: {
          filter: [
            {
              term: {
                delete_flg: false,
              },
            },
            {
              term: {
                'created_by.id': userProfileState.id,
              },
            },
          ],
          should: [
            {
              term: {
                dummy: currentTime,
              },
            },
          ],
        },
      },
    };
  };

  const visualizeNoteDialog = (
    event: React.MouseEvent<any, MouseEvent>,
    note?: NoteResponseT,
  ) => {
    setInformationDialog(note as NoteResponseT);
    setInformationDialogOpen(true);
  };
  const { t } = useTranslation();

  return (
    <Box mt={3}>
      <Box display="flex" justifyContent="flex-end" mb={3}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleClickNote}
          startIcon={<AddCircleOutlineIcon />}
        >
          Add
        </Button>
      </Box>
      <NoteQueryFromES setUserNotes={setUserNotes} defaultQuery={customQuery} />
      <Box>
        {userNotes && userNotes.length > 0 && (
          <TableContainer>
            <Table>
              <EnhancedTableHead classes={classes} />
              <TableBody>
                {userNotes.map((note, index) => {
                  return (
                    <TableRow
                      key={index}
                      className={classes.tableRow}
                      onClick={event => visualizeNoteDialog(event, note)}
                    >
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.id}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.title}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.created_by?.name}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.time &&
                            moment(note?.time).format('DD/MM/YYYY hh:mma')}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        <TableAction
                          hiddenItem={['view']}
                          onClickEdit={event => {
                            event?.stopPropagation();
                            setCurrentEditNoteId(note.id);
                            setNoteContent({
                              ...noteContent,
                              name: note?.title,
                              content: note?.content,
                            });
                          }}
                          onClickDelete={event => {
                            event?.stopPropagation();
                            setNoteToDeleteId(note.id);
                          }}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        )}
        {(!userNotes || userNotes.length === 0) && (
          <Box display="flex" justifyContent="center">
            No note found
          </Box>
        )}
      </Box>
      <Dialog
        open={isInformationDialogOpen}
        onClose={() => setInformationDialogOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="md"
      >
        <Box padding={3} minWidth="500px">
          <Box mb={3} display="flex" alignItems="center">
            <Box mr={1} display="flex" alignItems="center">
              <InfoIcon color="primary" />
            </Box>
            <Typography component="span" variant="h4">
              {t('note_info')}
            </Typography>
          </Box>
          <Box mb={1}>
            <Typography component="span" variant="caption">
              {t('title')}
            </Typography>
            <Typography component="span" variant="body1">
              {informationDialog?.title}
            </Typography>
          </Box>

          <Box>
            <Typography component="p" variant="caption">
              {t('content')}
            </Typography>
            <Box
              mb={2}
              dangerouslySetInnerHTML={{
                __html: informationDialog?.content ?? '',
              }}
            ></Box>
          </Box>
          <DialogActions>
            <Button
              onClick={() => setInformationDialogOpen(false)}
              color="default"
              variant="outlined"
            >
              {t('close')}
            </Button>
          </DialogActions>
        </Box>
      </Dialog>
      <DeleteDialog
        isOpen={!!noteToDeleteId}
        header="Delete Note"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_this_note')}
            </Typography>
          </Box>
        }
        handleClose={() => setNoteToDeleteId(undefined)}
        handleDelete={() => {
          if (noteToDeleteId) {
            handleRemoveNote(noteToDeleteId);
          }
        }}
        disabled={isSubmitting}
      />
      <NoteDialog
        type="add"
        isOpen={isOpenAddNote}
        noteContent={noteContent}
        handleClose={() => {
          setNoteContent(emptyNoteContent);
          setOpenAddNote(false);
        }}
        handleChange={handleChangeNoteContent}
        handleSubmit={handleAddNote}
        disabled={isSubmitting}
      />
      <NoteDialog
        type="edit"
        isOpen={!!currentEditNoteId}
        noteContent={noteContent}
        handleClose={() => {
          setCurrentEditNoteId(undefined);
        }}
        handleChange={handleChangeNoteContent}
        handleSubmit={handleEditNote}
        disabled={isSubmitting}
      />
    </Box>
  );
};

export default Notes;

interface NoteQueryFromESProps {
  setUserNotes: any;
  defaultQuery: (...args: any[]) => any;
}

const NoteQueryFromES: React.FC<NoteQueryFromESProps> = ({
  setUserNotes,
  defaultQuery,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  return (
    <ReactiveBase
      app={ES_INDICES.user_note_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{
        Authorization: window.localStorage.getItem('jwt'),
      }}
    >
      <ReactiveList
        componentId="Notes"
        renderResultStats={() => null}
        renderNoResults={() => <></>}
        defaultQuery={defaultQuery}
        size={10000}
        dataField="id"
        sortBy="desc"
        onData={({ data }) => {
          setUserNotes(data);
        }}
        loader={<></>}
        render={() => {
          return <></>;
        }}
      />
    </ReactiveBase>
  );
};
