import React from 'react';
import { Grid } from '@material-ui/core';
import { Chart } from 'react-google-charts';

const timeLineChartHeading = [
  { type: 'string', label: 'Title' },
  { type: 'string', label: 'User' },
  { type: 'date', label: 'Start Date' },
  { type: 'date', label: 'End Date' },
];

interface TimeLineChartProps {
  data: {
    title: string;
    name: string;
    assignment_date?: string;
    un_assignment_date?: string;
  }[];
}
export const TimeLineChartOpp: React.FC<TimeLineChartProps> = React.memo(
  props => {
    const { data } = props;

    const chartHeight = `${35 * data.length + 90}px`;
    const handleData = () => {
      return data.map((item: any) => {
        return [
          item.title,
          item.name,
          new Date(item.assignment_date),
          new Date(item.un_assignment_date),
        ];
      });
    };

    return (
      <Grid item xs={12}>
        <Chart
          width={'100%'}
          height={chartHeight}
          chartType="Timeline"
          loader={<div>Loading Chart</div>}
          data={[timeLineChartHeading, ...handleData()]}
          rootProps={{ 'data-testid': '3' }}
        />
      </Grid>
    );
  },
);
