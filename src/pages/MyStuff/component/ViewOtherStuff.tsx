import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Opportunity } from '.';
const MyProfile = React.lazy(() => import('../../MyProfile'));
const ViewOtherStuff: React.FC = () => {
  return (
    <Switch>
      <Route
        exact
        path={['/experts/:id', '/profile/:id/opportunity']}
        component={Opportunity}
      />
      <Route exact path="/profile/:id" component={MyProfile} />
    </Switch>
  );
};
export default ViewOtherStuff;
