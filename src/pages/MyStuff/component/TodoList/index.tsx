import React, { useState } from 'react';
import { Button, Grid, Typography, Box } from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { TaskState, TaskStateID } from '../../../../constants';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TaskKanban } from '../../../../components';
import {
  IOpportunityTask,
  updateTaskState,
} from '../../../../services/task.services';
import { ConfigESIndex } from '../../../../store/config/selector';
import { UserProfileState } from '../../../../store/user/selector';

export const TodoList: React.FC = () => {
  // const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const userProfile = useSelector(UserProfileState);
  const [data, setData] = useState<IOpportunityTask[]>([]);

  const handleStateUpdated = async (id: number, newState: TaskState) => {
    let newData = data.map(item => {
      if (item.id !== id) return item;
      return { ...item, task_state: newState };
    });
    setData(newData);
    const json = await updateTaskState(id, TaskStateID[newState]);
    if (json.success && json.data) {
      newData = data.map(item => {
        if (item.id !== id) return item;
        return json.data;
      });
      setData(newData);
    }
  };

  const customQuery = () => {
    return {
      query: {
        match: {
          'assignee_user.id': userProfile?.id,
        },
      },
    };
  };
  const { t } = useTranslation();
  return (
    <ReactiveBase
      app={ES_INDICES.opportunity_task_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{
        Authorization: window.localStorage.getItem('jwt'),
      }}
    >
      <Grid container justify="space-between">
        <Box mb={3} mt={2} width="100%">
          <Grid item container justify="space-between">
            <Grid item md={4}>
              <Typography variant="h4">{t('to_do_list')}</Typography>
            </Grid>
            <Button variant="outlined" startIcon={<DeleteOutlineIcon />}>
              Archive
            </Button>
          </Grid>
        </Box>
      </Grid>
      <Box marginRight="-15px" marginLeft="-15px">
        <ReactiveList
          componentId="SearchTask"
          dataField="task"
          renderResultStats={() => null}
          renderNoResults={() => <></>}
          size={100}
          defaultQuery={customQuery}
          onData={({ data }) => setData(data)}
        >
          {({ loading }) => {
            if (loading) return null;
            return (
              <TaskKanban
                data={data}
                visibleStates={[
                  TaskState.Assigned,
                  TaskState.InProgress,
                  TaskState.Completed,
                ]}
                onStateUpdated={handleStateUpdated}
                linkOpportunity={true}
              />
            );
          }}
        </ReactiveList>
      </Box>
    </ReactiveBase>
  );
};

export default TodoList;
