export const todoList = {
  todo: [
    {
      id: '#456789',
      title: 'Peer Review Feedback',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Analyse',
    },
    {
      id: '#456789',
      title: 'Obtain Dataset Approval',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Analyse',
    },
  ],
  progress: [
    {
      id: '#456789',
      title: 'Conduct Expert Review',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Shape',
    },
    {
      id: '#456789',
      title: 'Obtain Dataset Approval',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Analyse',
    },
    {
      id: '#456789',
      title: 'Obtain Dataset Approval',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Analyse',
    },
  ],
  completed: [
    {
      id: '#123456',
      title: 'Conduct Expert Review',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Shape',
    },
    {
      id: '#123456',
      title: 'Obtain Dataset Approval',
      content: 'HFC Modern Flaps',
      date_assigned: '09/03/2020',
      status: 'Analyse',
    },
  ],
};
