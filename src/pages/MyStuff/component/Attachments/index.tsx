import React, { useState } from 'react';
import {
  Box,
  TableHead,
  TableRow,
  TableCell,
  Typography,
  Grid,
  TableContainer,
  Table,
  TableBody,
  Button,
  IconButton,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CloudDownloadOutlined from '@material-ui/icons/CloudDownloadOutlined';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { ConfigESIndex } from '../../../../store/config/selector';
import {
  AttachmentResponse,
  deleteAttachments,
} from '../../../../services/opportunity.services';
import { AttachmentDialog } from '../../../../components/AttachmentDialog';
import { DeleteDialog, TableAction } from '../../../../components';
import { useDisableMultipleClick, useUnmounted } from '../../../../hooks';
import { Message } from '../../../../constants';
import { UserProfileState } from '../../../../store/user/selector';
import { notifyError } from '../../../../store/common/actions';

const useStyles = makeStyles(theme => ({
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
}));
interface Data {
  description?: string;
  file_name?: string;
  created_by?: string;
  date?: string;
  action?: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'file_name',
    label: 'File name/URL link',
  },
  {
    id: 'description',
    label: 'Description',
  },
  {
    id: 'created_by',
    label: 'Created by',
  },
  {
    id: 'date',
    label: 'Date',
  },
  {
    id: 'action',
    label: 'Action',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.id === 'action' ? 'center' : undefined}
            style={headCell.id === 'action' ? { width: '140px' } : undefined}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface AttachmentsProps {}

export const Attachments: React.FC<AttachmentsProps> = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const userProfileState = useSelector(UserProfileState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const isUnmounted = useUnmounted();
  const dispatch = useDispatch();

  const [userAttachment, setUserAttachment] = useState<AttachmentResponse[]>(
    [],
  );

  const [currentTime, setCurrentTime] = useState(1);

  const [isOpenAddAttachment, setOpenAddAttachment] = useState(false);
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);

  const [isOpenEditAttachment, setOpenEditAttachment] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState<number>();
  const [attachmentEditType, setAttachmentEditType] = useState('');

  const [attachmentToDeleteId, setAttachmentToDeleteId] = useState<number>();

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                del_flg: false,
              },
            },
            {
              term: {
                'created_by_user.id': userProfileState.id,
              },
            },
          ],
          should: [
            {
              term: {
                dummy: currentTime,
              },
            },
          ],
        },
      },
    };
  };

  const handleRefetchAttachment = () => {
    setTimeout(() => {
      if (isUnmounted.current) return;
      setCurrentTime(currentTime + 1);
    }, 1000);
  };
  const onClickDownload = (item: AttachmentResponse) => {
    const urlDownload = `${item.storage_prefix}/${item.attachment_name}`;
    window.open(urlDownload, '_blank');
  };
  const handleRemoveAttachment = async (id: number) => {
    if (isSubmitting) return;
    await debounceFn();
    const response = await deleteAttachments(id);
    if (response.success) {
      setOpenDeleteDialog(false);
      handleRefetchAttachment();
    } else {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
    endRequest();
  };

  return (
    <Box mt={3}>
      <Box display="flex" justifyContent="flex-end" mb={3}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setOpenAddAttachment(true)}
          startIcon={<AddCircleOutlineIcon />}
        >
          Add
        </Button>
      </Box>
      <AttachmentFromES
        setUserAttachment={setUserAttachment}
        defaultQuery={customQuery}
      />
      {userAttachment.length === 0 ? (
        <Box m={3} display="flex" justifyContent="center">
          <Typography>{t('no_attach_file')}</Typography>
        </Box>
      ) : (
        <Grid container>
          <Grid item xs={12}>
            <TableContainer className={classes.rootTableContainer}>
              <Table className={classes.rootTable}>
                <EnhancedTableHead classes={classes} />
                <TableBody>
                  {userAttachment?.map((item, index) => (
                    <TableRow key={index}>
                      <TableCell>{item?.client_file_name}</TableCell>
                      <TableCell>{item?.description}</TableCell>
                      <TableCell>{item?.created_by_user?.name}</TableCell>
                      <TableCell>
                        {item?.time && moment(item.time).format('DD/MM/YYYY')}
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        <TableAction
                          hiddenItem={['view']}
                          onClickEdit={() => {
                            setCurrentEditIndex(index);
                            setAttachmentEditType(item.entity_type);
                            setOpenEditAttachment(true);
                          }}
                          onClickDelete={() => {
                            setOpenDeleteDialog(true);
                            setAttachmentToDeleteId(item.id);
                          }}
                        >
                          <IconButton
                            onClick={() => onClickDownload(item)}
                            disabled={item?.storage_type !== 's3'}
                          >
                            <CloudDownloadOutlined
                              fill={
                                item?.storage_type !== 's3' ? 'gray' : undefined
                              }
                            />
                          </IconButton>
                        </TableAction>

                        <Box component="span"></Box>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
      )}
      <AttachmentDialog
        isAddingDialog={true}
        isOpen={isOpenAddAttachment}
        data={{
          id: userProfileState.id,
          title: '',
          description: '',
          attachments: undefined,
        }}
        attachmentsProp={undefined}
        handleClose={() => {
          setOpenAddAttachment(false);
          handleRefetchAttachment();
        }}
        entity_type="user"
      />
      <AttachmentDialog
        isAddingDialog={false}
        isOpen={isOpenEditAttachment}
        data={{
          id: userProfileState.id,
          title: '',
          description: '',
          attachments: userAttachment,
        }}
        attachmentsProp={true}
        index={currentEditIndex}
        handleClose={() => {
          setOpenEditAttachment(false);
          handleRefetchAttachment();
        }}
        entity_type={attachmentEditType}
      />
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Attachment"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_this_attachment')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (attachmentToDeleteId) {
            handleRemoveAttachment(attachmentToDeleteId);
          }
        }}
        disabled={isSubmitting}
      />
    </Box>
  );
};

export default Attachments;

interface AttachmentFromESProps {
  setUserAttachment: React.Dispatch<React.SetStateAction<AttachmentResponse[]>>;
  defaultQuery: (...args: any[]) => any;
}
const AttachmentFromES: React.FC<AttachmentFromESProps> = ({
  setUserAttachment,
  defaultQuery,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  return (
    <ReactiveBase
      app={ES_INDICES.attachment_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{
        Authorization: window.localStorage.getItem('jwt'),
      }}
    >
      <ReactiveList
        componentId="SearchAttachment"
        dataField="attachment"
        renderResultStats={() => null}
        renderNoResults={() => <></>}
        loader={<></>}
        onData={({ data }) => {
          setUserAttachment(data);
        }}
        defaultQuery={defaultQuery}
        render={() => <></>}
      />
    </ReactiveBase>
  );
};
