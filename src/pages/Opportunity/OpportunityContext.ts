import React from 'react';
import { TGroupStateAction } from './index';
import { Status } from '../../constants';
import {
  IListLocations,
  IListRole,
  ISkillsInRole,
} from '../../services/profile.services';
import { IResourceTeam } from '../../services/references.services';
const initState: TGroupStateAction = {
  isOpenActionDialog: false,
  isShareActionDialog: false,
  selectedAction: '',
  isClickNext: false,
  isSubmitting: Status.Idle,
};
export type OpportunityContextT = {
  setGroupState: (state: TGroupStateAction) => void;
  groupState: TGroupStateAction;
  onRefresh: () => void;
  roleList?: IListRole[];
  locationList?: IListLocations[];
  teamList?: IResourceTeam[];
  skillList?: ISkillsInRole[];
};
export const OpportunityContext = React.createContext<OpportunityContextT>({
  setGroupState: () => {},
  groupState: initState,
  onRefresh: () => {},
});

export default OpportunityContext;
