import React from 'react';
import {
  Box,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { useTranslation } from 'react-i18next';

import { ConfigESIndex } from '../../../store/config/selector';
import { IOpportunity } from '../../../services/opportunity.services';

const useStyles = makeStyles(theme => ({
  listItem: {
    paddingLeft: 0,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    marginLeft: 8,
  },
}));
export interface ActivityTabProps {
  opportunity: IOpportunity;
}

export const ActivityTab: React.FC<ActivityTabProps> = ({ opportunity }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const ES_INDICES = useSelector(ConfigESIndex);

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              match: { 'response_body.data.id': opportunity?.id },
            },
            {
              match: {
                object_type: 'opportunity',
              },
            },
            {
              range: {
                'response_body.data.opportunity_phase.id': {
                  gte: 2,
                },
              },
            },
          ],
        },
      },
      sort: {
        time: 'desc',
      },
    };
  };

  const event_log_index = ES_INDICES.event_log_index_name_prefix + '-*';
  return (
    <Box m={2}>
      <ReactiveBase
        app={event_log_index}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <ReactiveList
          componentId="event_log"
          dataField="event_log"
          loader={<></>}
          scrollOnChange={false}
          showResultStats={false}
          renderNoResults={() => null}
          defaultQuery={customQuery}
        >
          {({ data, loading }) => {
            if (loading) {
              return <></>;
            }
            return (
              <>
                {data.length > 0 ? (
                  <List>
                    {data.map((item: any, index: number) => (
                      <ListItem
                        key={index}
                        alignItems="flex-start"
                        divider={index < data.length - 1}
                        className={classes.listItem}
                      >
                        <ListItemAvatar>
                          <Avatar className={classes.avatar}>
                            {item?.user_name?.charAt(0)?.toUpperCase() ?? ''}
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary={
                            <Box display="inline-block">
                              <Typography component="span" variant="caption">
                                {item?.user_name}:{' '}
                              </Typography>
                              <Typography component="span">
                                {item?.action_name}
                              </Typography>
                            </Box>
                          }
                          secondary={
                            <div>
                              {item.time !== '0001-01-01T00:00:00Z' && (
                                <Typography component="i" display="block">
                                  {moment(item.time).fromNow()}
                                </Typography>
                              )}
                            </div>
                          }
                        />
                      </ListItem>
                    ))}
                  </List>
                ) : (
                  <Typography>{t('no_data')}</Typography>
                )}
              </>
            );
          }}
        </ReactiveList>
      </ReactiveBase>
    </Box>
  );
};
