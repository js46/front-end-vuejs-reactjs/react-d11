import React, { useState } from 'react';
import {
  deletePlaybookItemFinding,
  PlaybookItemFindingT,
} from '../../../services/playbook.services';
import {
  Box,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { AnalysePermission } from '../types';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { AnalysePermissionState } from '../../../store/opportunity/selector';
import {
  CollapseIconButton,
  DeleteDialog,
  TableAction,
} from '../../../components';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { FindingFormDialog } from '../../../components/FindingFormDialog';
import { useDisableMultipleClick, useEffectOnlyOnce } from '../../../hooks';

interface FindingsWrapperProps {
  data: PlaybookItemFindingT[];
  editable?: boolean;
  itemID: number;
  onRefresh?: () => void;
  entity_type?: string;
  getOptionFindings?: (option_id: number) => Promise<void>;
}

const FindingsWrapper: React.FC<FindingsWrapperProps> = ({
  data,
  editable,
  itemID,
  onRefresh,
  entity_type,
  getOptionFindings,
}) => {
  const { t } = useTranslation();
  const analysePermission = useSelector(AnalysePermissionState);
  const [collapse, setCollapse] = useState(true);
  const [selected, setSelected] = useState<PlaybookItemFindingT>();
  const [showFormDialog, setFormDialog] = useState(false);
  const [deleted, setDeleted] = useState<number>();
  const classes = useStyles();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const onDeleteFinding = async () => {
    if (!deleted) return;
    await debounceFn();
    try {
      await deletePlaybookItemFinding(deleted);
      await getOptionFindings?.(itemID);
    } catch (e) {
      console.log('Error deleting finding', e);
    }
    endRequest();
    setDeleted(undefined);
    await handleDialogSuccess();
  };

  const handleDialogSuccess = async () => {
    setFormDialog(false);
    onRefresh?.();
  };

  useEffectOnlyOnce(() => {
    if (getOptionFindings) {
      getOptionFindings?.(itemID);
    }
  });

  return (
    <>
      <Box display="flex" alignItems="center" mb={2} mt={2}>
        <Typography component="span" variant="caption">
          {t('finding.plural')}
        </Typography>
        <Box ml={2} display="flex">
          <CollapseIconButton
            onToggle={collapse => setCollapse(collapse)}
            disabled={
              !analysePermission?.includes(
                AnalysePermission.PlaybookItemNoteRead,
              )
            }
          />
        </Box>
        <IconButton onClick={() => setFormDialog(true)}>
          <AddCircleOutlineIcon />
        </IconButton>
      </Box>
      {!collapse && data.length > 0 && (
        <Box>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell>Update User</TableCell>
                  <TableCell>Last Update Time</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((item, index) => {
                  return (
                    <TableRow key={index} className={classes.tableRow}>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {item.title}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {item.created_by_user.name}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {item.date !== '0001-01-01T00:00:00Z' && (
                          <Typography component="span" variant="body1">
                            {moment(item.date).format('DD/MM/YYYY hh:mma')}
                          </Typography>
                        )}
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        <TableAction
                          hiddenItem={['view']}
                          onClickEdit={() => {
                            setSelected(item);
                            setFormDialog(true);
                          }}
                          disabledEdit={
                            !analysePermission?.includes(
                              AnalysePermission.PlaybookItemNoteUpdate,
                            )
                          }
                          onClickDelete={() => setDeleted(item.id)}
                          disabledDelete={
                            !analysePermission?.includes(
                              AnalysePermission.PlaybookItemNoteDelete,
                            )
                          }
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      )}
      <FindingFormDialog
        itemID={itemID}
        id={selected?.id}
        title={selected?.title ?? ''}
        description={selected?.description ?? ''}
        open={showFormDialog}
        onClose={() => {
          setFormDialog(false);
          setSelected(undefined);
        }}
        onSuccess={handleDialogSuccess}
        entity_type={entity_type}
        getOptionFindings={getOptionFindings}
      />
      <DeleteDialog
        isOpen={!!deleted}
        header={t('finding.delete')}
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('finding.delete-warning')}
            </Typography>
          </Box>
        }
        handleClose={() => setDeleted(undefined)}
        handleDelete={onDeleteFinding}
        disabled={isSubmitting}
      />
    </>
  );
};
const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  headTable: {
    minWidth: 732,
  },
  tableRow: {
    transition: '0.3s all',
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: '#eee',
    },
  },
}));

export default FindingsWrapper;
