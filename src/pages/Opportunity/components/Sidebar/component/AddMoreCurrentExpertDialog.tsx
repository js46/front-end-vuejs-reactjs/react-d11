import React, { useCallback, useEffect, useState, useContext } from 'react';
import {
  Grid,
  Button,
  DialogContent,
  Typography,
  Box,
  FormControl,
  Select,
  MenuItem,
  ExpansionPanel,
  ExpansionPanelSummary,
  CircularProgress,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { ArrowBack } from '@material-ui/icons';

import {
  IOpportunityInRole,
  IOpportunityExpert,
  addOpportunityCurrentExpertsInTeam,
} from '../../../../../services/opportunity.services';
import {
  StyledDataSearch,
  StyledMultiList,
  ExpansionPanelDetailFilter,
  StyledSingleList,
} from '../../../../../components/styled';
import { IProfileData } from '../../../../../services/profile.services';
import { addTaskComment } from '../../../../../services/comment.services';
import { AssignExpertDialog } from '.';
import { SelectedFiltersCustom } from '../../../../../components';
import { isWeekend } from '../../../../../utils';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { ExpertTable } from './ExpertTable';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import OpportunityContext from '../../../OpportunityContext';

type RoleFilterData = {
  role?: string;
  skills: string[];
  team?: string;
  location?: string;
};
interface IAddMoreCurrentExpertDialogProps {
  opportunityExperts?: IOpportunityExpert[];
  statusAddMoreCurrentExpert: boolean;
  statusAssignExpertDialog: boolean;
  holidays?: Map<string, string>;
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
  setStatusAssignExpertDialog: React.Dispatch<React.SetStateAction<boolean>>;
  setStatusAddMoreCurrentExpert: React.Dispatch<React.SetStateAction<boolean>>;
  onAddRole: (data: RoleFilterData) => void;
}

export const AddMoreCurrentExpertDialog: React.FC<IAddMoreCurrentExpertDialogProps> = ({
  opportunityExperts,
  setStatusAssignExpertDialog,
  setLoading,
  setStatusAddMoreCurrentExpert,
  statusAddMoreCurrentExpert,
  statusAssignExpertDialog,
  holidays,
  onAddRole,
}) => {
  const isInitialMount = React.useRef(true);
  const ES_INDICES = useSelector(ConfigESIndex);
  const opportunity = useSelector(OpportunityState);
  const classes = useStyles();
  const { onRefresh } = useContext(OpportunityContext);
  const currentExperts = opportunityExperts?.map(currentExpert => {
    return currentExpert.expert.id;
  });
  const { t } = useTranslation();
  const [userFocus, setUserFocus] = useState<any>();
  const [selectedRole, setSelectedRole] = useState<number>();
  const [selectedUser, setSelectedUser] = useState<IProfileData>();
  const [duration, setDuration] = useState<number>();
  const [effort, setEffort] = useState<number>();
  const [filterFields, setFilterFields] = useState<RoleFilterData>({
    skills: [],
  });

  const [assignmentDate, setAssignmentDate] = useState<Date | null>(null);
  const defaultFilterQuery = useCallback(
    () => ({
      query: {
        bool: {
          must: {
            term: {
              casbin_role: 'role:expert',
            },
          },
        },
      },
    }),
    [],
  );
  const customQuery = useCallback(() => {
    let queryTeamRole: {}[] = [];
    const role = opportunity?.opportunity_roles?.find(
      item => item.id === selectedRole,
    );
    if (role) {
      if (role.profile_role?.id) {
        queryTeamRole.push({
          term: {
            'profile_role.profile_role_id': role.profile_role.id,
          },
        });
      }
      if (role.location?.id) {
        queryTeamRole.push({
          term: {
            'location.id': role.location.id,
          },
        });
      }
      if (role.resource_team?.id) {
        queryTeamRole.push({
          term: {
            'resource_team.resource_team_id': role.resource_team.id,
          },
        });
      }
      if (role.skills) {
        for (let skill of role.skills) {
          queryTeamRole.push({
            term: {
              'skills.skill_id': skill.skill_id,
            },
          });
        }
      }
    }
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                casbin_role: 'role:expert',
              },
            },
            ...queryTeamRole,
          ],
          must_not: {
            match: {
              id: opportunity?.current_expert_review?.expert_review?.id,
            },
          },
        },
      },
    };
  }, [opportunity, selectedRole]);

  // ** Handle Role selection changed
  useEffect(() => {
    const role = opportunity?.opportunity_roles?.find(
      item => item.id === selectedRole,
    );
    if (role) {
      setFilterFields({
        role: role.role_name,
        skills: role.skills?.map(item => item.skill_name) ?? [],
        team: role.resource_team?.name,
        location: role.location?.name,
      });
      setDuration(role.day);
      setEffort(role.effort);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedRole]);

  // ** Handle New Role added --> Auto select the latest role
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      setSelectedRole(undefined);
      setFilterFields({ skills: [] });
      setDuration(undefined);
      setEffort(undefined);
    }
  }, [opportunity]);

  const listRequiredResources: IOpportunityInRole[] = React.useMemo(() => {
    if (!opportunity || !opportunity.opportunity_roles) return [];
    const assignRoleIDs = opportunity.current_experts?.map(
      item => item.opportunity_team_role.id,
    );
    if (!assignRoleIDs) return opportunity.opportunity_roles;
    return opportunity.opportunity_roles.filter(
      role => !assignRoleIDs.includes(role.id!),
    );
  }, [opportunity]);

  const handleAddComment = useCallback(async (id: number, comment?: string) => {
    const bodyRequest = {
      entity_id: id,
      entity_type: 'task',
      comment_type: '',
      comment_body: comment,
      comment_parent_id: 0,
    };
    try {
      await addTaskComment(bodyRequest);
    } catch (err) {
      console.log(err);
    }
  }, []);
  const handleAssignExpert = useCallback(
    async (
      emailId: number | undefined,
      teamRoleSelected?: IOpportunityInRole,
      assignmentDate?: Date,
      effort?: number,
      comment?: string,
      duration?: number,
    ) => {
      if (opportunity?.id && comment !== '') {
        await handleAddComment(opportunity?.id, comment);
      }

      if (emailId && opportunity?.id) {
        addOpportunityCurrentExpertsInTeam({
          opportunity_id: opportunity?.id,
          user_id: emailId,
          opportunity_team_role_id: teamRoleSelected?.id,
          assignment_date: assignmentDate?.toISOString(),
          effort,
          duration,
        })
          .then(async () => {
            setSelectedUser(undefined);
            onRefresh();
            setLoading(false);
            setStatusAssignExpertDialog(false);
            setStatusAddMoreCurrentExpert(true);
          })
          .catch(err => console.log(err));
      }
    },
    [
      handleAddComment,
      onRefresh,
      opportunity,
      setLoading,
      setStatusAddMoreCurrentExpert,
      setStatusAssignExpertDialog,
    ],
  );

  const onDurationChanged = useCallback(
    (from?: Date, to?: Date) => {
      setAssignmentDate(from ?? null);
      const role = opportunity?.opportunity_roles?.find(
        item => item.id === selectedRole,
      );
      if (role) {
        if (from) setStatusAssignExpertDialog(true);
        return;
      }
      if (from && to) {
        let count = 0,
          start = moment(from);
        while (start.isSameOrBefore(to, 'days')) {
          if (!isWeekend(start) && !holidays?.has(start.format('DD/MM'))) {
            count += 1;
          }
          start.add(1, 'day');
        }
        Promise.all([setDuration(count), setEffort(count)]).then(() => {
          setStatusAssignExpertDialog(true);
        });
      } else {
        setDuration(undefined);
        setEffort(undefined);
      }
    },
    [holidays, opportunity, selectedRole, setStatusAssignExpertDialog],
  );

  const getExpertDefaultRange = useCallback(
    (id: number): number | undefined => {
      const role = opportunity?.opportunity_roles?.find(
        item => item.id === selectedRole,
      );
      if (role && selectedUser?.id === id) {
        return role.day;
      }
      return undefined;
    },
    [opportunity, selectedRole, selectedUser],
  );

  const onSelectRole = (
    e: React.ChangeEvent<{ name?: string; value: unknown }>,
  ) => {
    if (typeof e.target.value !== 'number') {
      setSelectedRole(undefined);
      setFilterFields({ skills: [] });
      setDuration(undefined);
      setEffort(undefined);
    } else {
      if (e.target.value === 0) return onAddRole(filterFields);
      setSelectedRole(e.target.value);
    }
  };

  const onChangeFilter = (
    name: 'role' | 'skills' | 'team' | 'location',
    value: string | string[] | null,
  ) => {
    if (selectedRole) setSelectedRole(undefined);
    switch (name) {
      case 'role':
      case 'location':
      case 'team':
        return setFilterFields(prevState => {
          if (value && value === prevState[name]) {
            return { ...prevState, [name]: undefined };
          }
          return { ...prevState, [name]: value as string };
        });
      case 'skills':
        return setFilterFields(prevState => {
          return {
            ...prevState,
            skills: (value as string[]) ?? [],
          };
        });
      default:
        return;
    }
  };

  return (
    <React.Fragment>
      <ReactiveBase
        app={ES_INDICES.user_profile_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{
          Authorization: window.localStorage.getItem('jwt'),
        }}
      >
        {!statusAssignExpertDialog && (
          <Box mt={2} mb={2} className={classes.dialog}>
            <Grid container className={classes.titleDialog}>
              <Grid item xs={12} sm={4} md={3}>
                <Box pr={{ md: 5 }}>
                  <Select
                    fullWidth
                    variant="outlined"
                    value={selectedRole ?? 'none'}
                    id="role_name_select"
                    name="role_name_select"
                    onChange={onSelectRole}
                    MenuProps={{
                      anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'left',
                      },
                      transformOrigin: {
                        vertical: 'top',
                        horizontal: 'left',
                      },
                      getContentAnchorEl: null,
                    }}
                    IconComponent={ExpandMoreIcon}
                  >
                    <MenuItem value={'none'}>
                      <em> Select predefined role</em>
                    </MenuItem>
                    {/*{(filterFields.role ||
                      filterFields.location ||
                      filterFields.team ||
                      filterFields.skills.length !== 0) &&
                      !selectedRole && (
                        <MenuItem value={0}>
                          <em>&lt; Add role &gt;</em>
                        </MenuItem>
                      )}*/}
                    {listRequiredResources.map(role => {
                      return (
                        <MenuItem key={role.id} value={role.id}>
                          {role.role_name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </Box>
              </Grid>
              <Grid item xs={12} sm={4} md={6}>
                <Box>
                  <FormControl fullWidth>
                    <StyledDataSearch
                      componentId="Keyword"
                      dataField={['name']}
                      queryFormat="or"
                      autosuggest={false}
                      placeholder="Search for name..."
                      iconPosition="left"
                      showClear={true}
                      innerClass={{
                        input: 'searchInput',
                        title: 'titleSearch',
                      }}
                    />
                  </FormControl>
                </Box>
              </Grid>
              <Grid item xs={12} sm={4} md={3}>
                <Box mr={1} display="flex" justifyContent="flex-end">
                  <Button
                    onClick={() =>
                      setStatusAddMoreCurrentExpert(!statusAddMoreCurrentExpert)
                    }
                    variant="text"
                    startIcon={<ArrowBack />}
                  >
                    Back To Team List
                  </Button>
                </Box>
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              className={classes.filter}
              container
              direction="row"
              justify="space-between"
            >
              {selectedRole && (
                <Box paddingLeft={1} display="flex" alignItems="center">
                  <Button
                    disableRipple
                    className={classes.selectedItems}
                    size="small"
                  >
                    <Box component="span" className={classes.textSelectorItem}>
                      Duration: {duration}
                    </Box>
                  </Button>
                  <Button
                    disableRipple
                    className={classes.selectedItems}
                    size="small"
                  >
                    <Box component="span" className={classes.textSelectorItem}>
                      Effort: {effort}
                    </Box>
                  </Button>
                </Box>
              )}
              <Box display="flex" flex={1}>
                <SelectedFiltersCustom />
              </Box>
            </Grid>
            <DialogContent>
              <Grid container>
                <Grid item xs={12} sm={3}>
                  <Grid container direction="column">
                    <Grid item>
                      <ExpansionPanel classes={{ root: classes.panel }}>
                        <ExpansionPanelSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="roles-content"
                          id="roles-header"
                          classes={{
                            root: classes.filterSummary,
                            content: classes.filterSummaryContent,
                            expandIcon: classes.expandIcon,
                          }}
                        >
                          <Typography variant="h6">{t('roles')}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetailFilter
                          className={classes.filterDetail}
                        >
                          <Box width="100%">
                            <StyledSingleList
                              componentId="Role list"
                              dataField="profile_role.profile_role_name.keyword"
                              filterLabel="Roles"
                              showSearch={false}
                              innerClass={{
                                label: 'multiList',
                              }}
                              react={{
                                and: [
                                  'Keyword',
                                  'Skill list',
                                  'Location list',
                                  'Resource team list',
                                ],
                              }}
                              defaultQuery={defaultFilterQuery}
                              value={filterFields.role ?? ''}
                              onChange={value => {
                                onChangeFilter('role', value);
                              }}
                            />
                          </Box>
                        </ExpansionPanelDetailFilter>
                      </ExpansionPanel>
                    </Grid>
                    <Grid item>
                      <ExpansionPanel classes={{ root: classes.panel }}>
                        <ExpansionPanelSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="skills-content"
                          id="skills-header"
                          classes={{
                            root: classes.filterSummary,
                            content: classes.filterSummaryContent,
                            expandIcon: classes.expandIcon,
                          }}
                        >
                          <Typography variant="h6">{t('skills')}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetailFilter
                          className={classes.filterDetail}
                        >
                          <Box width="100%">
                            <StyledMultiList
                              componentId="Skill list"
                              filterLabel="Skills"
                              dataField="skills.skill_name.keyword"
                              queryFormat="and"
                              showCheckbox={true}
                              showSearch={false}
                              innerClass={{
                                label: 'multiList',
                              }}
                              react={{
                                and: [
                                  'Keyword',
                                  'Role list',
                                  'Location list',
                                  'Resource team list',
                                ],
                              }}
                              defaultQuery={defaultFilterQuery}
                              value={filterFields.skills}
                              onChange={(value: null | string[]) => {
                                onChangeFilter('skills', value);
                              }}
                            />
                          </Box>
                        </ExpansionPanelDetailFilter>
                      </ExpansionPanel>
                    </Grid>
                    <Grid item>
                      <ExpansionPanel classes={{ root: classes.panel }}>
                        <ExpansionPanelSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="locations-content"
                          id="locations-header"
                          classes={{
                            root: classes.filterSummary,
                            content: classes.filterSummaryContent,
                            expandIcon: classes.expandIcon,
                          }}
                        >
                          <Typography variant="h6">{t('locations')}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetailFilter
                          className={classes.filterDetail}
                        >
                          <StyledSingleList
                            componentId="Location list"
                            filterLabel="Locations"
                            dataField="location.name.keyword"
                            // showCheckbox={true}
                            showSearch={false}
                            innerClass={{
                              label: 'multiList',
                            }}
                            react={{
                              and: [
                                'Keyword',
                                'Skill list',
                                'Role list',
                                'Resource team list',
                              ],
                            }}
                            defaultQuery={defaultFilterQuery}
                            value={filterFields.location ?? ''}
                            onChange={(value: string | null) => {
                              onChangeFilter('location', value);
                            }}
                          />
                        </ExpansionPanelDetailFilter>
                      </ExpansionPanel>
                    </Grid>
                    <Grid item>
                      <ExpansionPanel classes={{ root: classes.panel }}>
                        <ExpansionPanelSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="resource-team-content"
                          id="resource-team-header"
                          classes={{
                            root: classes.filterSummary,
                            content: classes.filterSummaryContent,
                            expandIcon: classes.expandIcon,
                          }}
                        >
                          <Typography variant="h6">
                            {t('resource_team')}
                          </Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetailFilter
                          className={classes.filterDetail}
                        >
                          <StyledSingleList
                            componentId="Resource team list"
                            filterLabel="Resource Team"
                            dataField="resource_team.resource_team_name.keyword"
                            // showCheckbox={true}
                            showSearch={false}
                            innerClass={{
                              label: 'multiList',
                            }}
                            defaultQuery={defaultFilterQuery}
                            value={filterFields.team ?? ''}
                            onChange={(value: string | null) => {
                              onChangeFilter('team', value);
                            }}
                            react={{
                              and: [
                                'Keyword',
                                'Skill list',
                                'Role list',
                                'Role list',
                              ],
                            }}
                          />
                        </ExpansionPanelDetailFilter>
                      </ExpansionPanel>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item xs={12} sm={9}>
                  {holidays ? (
                    <ExpertTable
                      defaultQuery={customQuery}
                      holidays={holidays}
                      currentExperts={currentExperts}
                      handleSelectedUser={setSelectedUser}
                      userFocus={userFocus}
                      setUserFocus={setUserFocus}
                      onDurationChange={onDurationChanged}
                      getExpertDefaultRange={getExpertDefaultRange}
                    />
                  ) : (
                    <CircularProgress />
                  )}
                </Grid>
              </Grid>
            </DialogContent>
          </Box>
        )}
      </ReactiveBase>
      {statusAssignExpertDialog && (
        <AssignExpertDialog
          teamRoleSelected={opportunity?.opportunity_roles?.find(
            item => item.id === selectedRole,
          )}
          selectedUser={selectedUser}
          handleAssignExpert={handleAssignExpert}
          setStatusAssignExpertDialog={setStatusAssignExpertDialog}
          defaultFilterValue={{ duration, effort, assignmentDate }}
        />
      )}
    </React.Fragment>
  );
};

const useStyles = makeStyles(theme => ({
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '16px',
  },
  filter: {
    paddingLeft: 16,
    paddingRight: 16,
  },
  panel: {
    margin: '0px!important',
    boxShadow: 'none',
    '&::before': {
      background: 'none!important',
    },
  },
  filterSummary: {
    padding: '0px 5px 0px 0px',
    margin: '0px',
  },
  filterSummaryContent: {
    margin: 0,
    padding: 0,
    '&$expanded': {
      margin: 'auto',
      padding: '0px',
    },
  },
  filterDetail: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
  },
  expandIcon: {
    margin: '0px!important',
  },
  ul: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
    maxHeight: 240,
    position: 'relative',
    overflowY: 'auto',
    paddingBottom: 12,
    boxSizing: 'border-box',
  },
  li: {
    display: 'flex',
    cursor: 'pointer',
    width: '100%',
  },
  label: {
    color: theme.palette.text.primary,
    flex: 1,
  },
  dialog: {
    height: 700,
  },
  selectedItems: {
    backgroundColor: theme.palette.grey[100],
    marginRight: 3,
    paddingRight: 8,
    borderRadius: 5,
    '&:hover': {
      cursor: 'default',
    },
  },
  textSelectorItem: {
    // color: theme.palette.grey.A100,
    // fontSize: 13.6,
    fontFamily:
      '-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Noto Sans", sans-serif',
  },
}));
