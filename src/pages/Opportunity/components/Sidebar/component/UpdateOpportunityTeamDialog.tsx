import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  Box,
  Grid,
  TextField,
  Button,
  Typography,
  InputAdornment,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import DateFnsUtils from '@date-io/date-fns';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from 'moment';

import {
  IOpportunityExpert,
  IOpportunity,
  updateOpportunityRoles,
  updateExpertOpportunity,
} from '../../../../../services/opportunity.services';
import { MixTitle } from '../../../../../components';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { StyledReactiveList } from '../../../../../components/styled';
import { ConfigESIndex } from '../../../../../store/config/selector';

const useStyle = makeStyles(theme => ({
  dialog: {
    padding: '15px 0',
    minWidth: 732,
  },
}));

interface IUpdateOpportunityTeamDialogProps {
  statusUpdateDialog: boolean;
  setStatusUpdateDialog: () => void;
  teamOpportunitySelected?: IOpportunityExpert;
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}

interface GroupStateT {
  opportunity: IOpportunity;
}

export const UpdateOpportunityTeamDialog: React.FC<IUpdateOpportunityTeamDialogProps> = ({
  statusUpdateDialog,
  setStatusUpdateDialog,
  teamOpportunitySelected,
  setLoading,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyle();
  const { t } = useTranslation();
  const [teamRole, setTeamRole] = useState<string | undefined>(
    teamOpportunitySelected?.opportunity_team_role.name,
  );
  const [duration, setDuration] = useState<number | undefined>(
    teamOpportunitySelected?.duration,
  );
  const [effort, setEffort] = useState<number | undefined>(
    teamOpportunitySelected?.effort,
  );
  const [opportunity, setOpportunity] = useState<IOpportunity>();
  const [assignmentDate, setAssignmentDate] = useState<Date | null | undefined>(
    teamOpportunitySelected?.assignment_date,
  );
  const [endDate, setEndDate] = useState<string>(
    moment(teamOpportunitySelected?.un_assignment_date).format('DD/MM/YYYY'),
  );
  let disableButton =
    teamOpportunitySelected?.duration === Number(duration) &&
    teamOpportunitySelected?.effort === Number(effort) &&
    teamOpportunitySelected?.assignment_date === assignmentDate &&
    teamOpportunitySelected?.opportunity_team_role.name === teamRole
      ? true
      : false;

  const defaultQuery = () => {
    return {
      query: {
        match: {
          id: teamOpportunitySelected?.opportunity.id,
        },
      },
    };
  };

  const handleChangeDuration = (e: React.ChangeEvent<any>) => {
    e.persist();
    let updatedDurationValue = e.target.value.replace(/[^0-9]/gm, '');

    setDuration(updatedDurationValue);
    setEndDate(
      moment(teamOpportunitySelected?.assignment_date)
        .add(Number(e.target.value), 'days')
        .format('DD/MM/YYYY'),
    );
  };

  const handleChangeEffort = (e: React.ChangeEvent<any>) => {
    e.persist();
    let updatedEffortValue = e.target.value.replace(/[^0-9]/gm, '');

    setEffort(updatedEffortValue);
  };

  const onUpdate = async () => {
    await updateOpportunityRole();
    await handleExpertOpportunity();
  };

  const updateOpportunityRole = async () => {
    if (opportunity) {
      const opp = opportunity.opportunity_roles?.find(
        opportunityRole =>
          opportunityRole.id ===
          (teamOpportunitySelected && teamOpportunitySelected?.id + 1),
      );

      if (opp && teamRole) {
        //params for update team role name
        await updateOpportunityRoles(opportunity.id, {
          id: opp.id,
          role_name: teamRole,
          profile_role_id: opp.profile_role?.id,
          profile_role_level_id: opp.profile_role_level_id,
          day: opp.day,
          location_id: opp.location?.id,
          resource_team_id: opp.resource_team?.id,
          opportunity_role_skills: opp.skills,
        });
      }
    }
  };

  const handleExpertOpportunity = async () => {
    if (duration && assignmentDate && teamOpportunitySelected && effort) {
      const assignment_date = new Date(assignmentDate);
      await updateExpertOpportunity(teamOpportunitySelected?.id, {
        assignment_date: assignment_date.toISOString(),
        duration: Number(duration),
        effort: Number(effort),
      }).then(() => {
        setStatusUpdateDialog();
        setLoading(false);
      });
    }
  };
  return (
    <React.Fragment>
      <ReactiveBase
        app={ES_INDICES.opportunity_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <StyledReactiveList
          componentId="Opportunity"
          dataField="id"
          renderResultStats={() => null}
          showResultStats={false}
          renderNoResults={() => <></>}
          loader={<></>}
          onData={({ data }) => {
            setOpportunity(data[0]);
          }}
          render={({ data }) => <></>}
          defaultQuery={defaultQuery}
        />
      </ReactiveBase>
      <Dialog
        open={statusUpdateDialog}
        onClose={setStatusUpdateDialog}
        className={classes.dialog}
      >
        <Box pt={3} pl={3} display="flex" justifyContent="center">
          <Typography variant="h2">{t('update_opp_team')}</Typography>
        </Box>
        <Box pt={3} display="flex" justifyContent="center">
          <Typography variant="caption" style={{ color: 'red' }}>
            {duration &&
              effort &&
              Number(duration) < Number(effort) &&
              t('effort_should_less_than_or_qual_duration')}
          </Typography>
        </Box>
        <DialogContent>
          <Grid container>
            <Grid item xs={12}>
              <Box mt={2}>
                <MixTitle title="Name" />
                <TextField
                  id="name"
                  name="name"
                  value={teamOpportunitySelected?.expert.name}
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  variant="outlined"
                  disabled={true}
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <MixTitle title="Team Role" />
                <TextField
                  id="team_role"
                  name="team_role"
                  value={teamRole || ''}
                  onChange={(e: React.ChangeEvent<any>) =>
                    setTeamRole(e.target.value)
                  }
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  disabled
                  variant="outlined"
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <MixTitle title="Duration" />
                <TextField
                  id="duration"
                  name="duration"
                  value={duration || ''}
                  onChange={handleChangeDuration}
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  variant="outlined"
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <MixTitle title="Effort" />
                <TextField
                  id="effort"
                  name="effort"
                  value={effort || ''}
                  onChange={handleChangeEffort}
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  variant="outlined"
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <MixTitle title="Assignment Date" />
                  <DatePicker
                    disableToolbar
                    allowKeyboardControl
                    inputVariant="outlined"
                    variant="inline"
                    margin="normal"
                    name="assignment_date"
                    id="assignment_date"
                    format="d MMMM yyyy"
                    autoOk={true}
                    onChange={setAssignmentDate}
                    value={assignmentDate ? assignmentDate : ''}
                    animateYearScrolling={false}
                    fullWidth
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <ExpandMoreIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <MixTitle title="End Date" />
                <TextField
                  id="end_date"
                  name="end_date"
                  value={endDate}
                  type="text"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  variant="outlined"
                  disabled={true}
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={3} mb={2} justifyContent="flex-end" display="flex">
                <Box mr={2}>
                  <Button
                    onClick={() => setStatusUpdateDialog()}
                    variant="outlined"
                  >
                    Cancel
                  </Button>
                </Box>
                {duration && effort && Number(duration) >= Number(effort) ? (
                  <Box>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => onUpdate()}
                    >
                      Save
                    </Button>
                  </Box>
                ) : (
                  <Box>
                    <Button variant="contained" disabled={disableButton}>
                      Save
                    </Button>
                  </Box>
                )}
              </Box>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};
