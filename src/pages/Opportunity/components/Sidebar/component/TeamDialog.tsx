/* eslint-disable array-callback-return */
import React, { useState, useEffect, createRef, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { groupBy } from 'lodash';
import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  IOpportunityExpert,
  deleteOpportunityCurrentExpertsInTeam,
  getExpertsOpportunity,
} from '../../../../../services/opportunity.services';
import {
  useToggleValue,
  useDisableMultipleClick,
  useHolidayFetch,
} from '../../../../../hooks';
import {
  AddTeamRoleDialog,
  AddMoreCurrentExpertDialog,
  UpdateOpportunityTeamDialog,
} from '.';
import {
  CommonTableCell,
  DeleteDialog,
  TableAction,
} from '../../../../../components';
import { UpdateCurrentExpertReviewer } from './UpdateCurrentExpertReviewer';
import { useSelector, useDispatch } from 'react-redux';
import {
  CustomGanttChart,
  CustomGanttChartDataT,
} from '../../../../../components';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../../../store/user/selector';
import {
  AddExpertToTeamState,
  OpportunityState,
} from '../../../../../store/opportunity/selector';
import { RequestDetailOpportunity } from '../../../../../store/opportunity/actions';

const EmptyTableCell = () => {
  return (
    <TableCell>
      <Box display="flex" alignItems="center">
        -
      </Box>
    </TableCell>
  );
};

export interface Data {
  name: string;
  role: string;
  duration: number;
  location: string;
  effort: number;
  resource_team: string;
  assignment_date: string;
  end_date: string;
}
export interface HeadCell {
  id: keyof Data;
  label: string;
}

const expertHeadCells: HeadCell[] = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'role',
    label: 'Team role',
  },
  {
    id: 'duration',
    label: 'Duration',
  },
  {
    id: 'effort',
    label: 'Effort',
  },
  {
    id: 'assignment_date',
    label: 'Assignment date',
  },
  {
    id: 'end_date',
    label: 'End date',
  },
];

export type Order = 'asc' | 'desc';
interface OppEnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function ExpertEnhancedTableHead(props: OppEnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {expertHeadCells.map(headCell => (
          <TableCell key={headCell.id}>{headCell?.label ?? ''}</TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

interface IExpertTeamDialogProps {
  openModal: boolean;
  toggleModal: () => void;
}

export const ExpertTeamDialog: React.FC<IExpertTeamDialogProps> = ({
  openModal,
  toggleModal,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const tableRef = createRef<HTMLTableElement>();
  const { id } = useParams();
  const dispatch = useDispatch();
  const userProfileState = useSelector(UserProfileState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const { holidays } = useHolidayFetch();

  const [statusAddMoreCurrentExpert, setStatusAddMoreCurrentExpert] = useState(
    false,
  );
  const [statusAssignExpertDialog, setStatusAssignExpertDialog] = useState(
    false,
  );
  const [statusAddTeamRoleDialog, setStatusAddTeamRoleDialog] = useToggleValue(
    false,
  );
  const [statusUpdateDialog, setStatusUpdateDialog] = useToggleValue(false);
  const [expertReviewerDialogStatus, setExpertReviewerDialogStatus] = useState(
    false,
  );
  const [newRole, setNewRole] = useState<{
    role?: string;
    skills?: string[];
    location?: string;
    team?: string;
  }>();
  const userAddExpertToTeamState = useSelector(AddExpertToTeamState);
  const opportunity = useSelector(OpportunityState);
  const [opportunityExperts, setOpportunityExpert] = useState<
    IOpportunityExpert[]
  >();
  const [loading, setLoading] = useState<boolean>(false);
  const [teamOpportunitySelected, setTeamOpportunitySelected] = useState<
    IOpportunityExpert
  >();
  const [expertEliminationId, setExpertEliminationId] = useState<number>();
  const [chartWidth, setChartWidth] = useState<number>();
  useEffect(() => {
    if (
      tableRef.current &&
      !chartWidth &&
      chartWidth !== tableRef.current.clientWidth
    ) {
      setChartWidth(tableRef.current.clientWidth);
    }
  }, [chartWidth, tableRef]);

  useEffect(() => {
    if (opportunity && !loading) {
      getExpertsOpportunityList(opportunity.id);
    }
  }, [loading, opportunity]);
  useEffect(() => {
    if (openModal) {
      setStatusAddMoreCurrentExpert(false);
    }
  }, [openModal]);

  useEffect(() => {
    if (!statusAddMoreCurrentExpert && statusAssignExpertDialog) {
      setStatusAssignExpertDialog(false);
    }
  }, [statusAddMoreCurrentExpert, statusAssignExpertDialog]);

  const getExpertsOpportunityList = async (id: number) => {
    try {
      const expertsOpportunityRes = await getExpertsOpportunity(id);

      let opportunityExperts: IOpportunityExpert[] = [];
      expertsOpportunityRes.data?.filter(expert => {
        // let un_assignment_date = new Date(expert.un_assignment_date);
        // if (un_assignment_date.getTime() > Date.now())
        opportunityExperts.push(expert);
      });
      setOpportunityExpert(opportunityExperts);
      setLoading(true);
    } catch (err) {
      console.log(err);
    }
  };

  const onRemoveCurrentExpert = async (currentExpertId: number) => {
    if (isSubmitting) return;
    await debounceFn();
    const data = await deleteOpportunityCurrentExpertsInTeam(currentExpertId);
    if (data.success) {
      setOpportunityExpert(
        opportunityExperts?.filter(expert => expert.id !== currentExpertId),
      );
      setExpertEliminationId(undefined);
      handleUpdateOpportunity();
    }
    endRequest();
  };
  const handleUpdateOpportunity = () => {
    dispatch(RequestDetailOpportunity(id));
  };
  const onClickAddRole = useCallback(
    (data: typeof newRole) => {
      // console.log('Add role', data);
      setNewRole(data);
      setStatusAddTeamRoleDialog();
    },
    [newRole, setStatusAddTeamRoleDialog],
  );

  let chartData: CustomGanttChartDataT[] = [];
  if (opportunityExperts) {
    let groupExpertData = groupBy(opportunityExperts, 'expert.id');
    let startDate: Date = new Date();
    chartData = Object.values(groupExpertData).map((expert, index) => {
      let activeDates: string[] = [];
      let duration: number[] = [1];
      let effort: number[] = [1];
      let expertData = expert[0];
      expert.map(expertDetail => {
        if (startDate) {
          if (moment(startDate).isAfter(moment(expertDetail.assignment_date))) {
            startDate = expertDetail.assignment_date;
          }
        }

        duration.push(expertDetail.duration);
        effort.push(expertDetail.effort);
        let start = moment(expertDetail.assignment_date);
        while (start.isSameOrBefore(expertDetail.un_assignment_date, 'day')) {
          activeDates.push(start.format('DD/MM'));
          start.add(1, 'day');
        }
      });

      return {
        title: expertData?.expert.name ?? '',
        activeDates,
        startDate: startDate,
        customTooltip: [
          {
            label: 'Duration',
            value:
              duration.reduce((total, currentDuration) => {
                return total + currentDuration;
              }) + ' days',
          },
          {
            label: 'Effort',
            value:
              effort.reduce((total, currentEffort) => {
                return total + currentEffort;
              }) + ' days',
          },
        ],
      };
    });
  }
  console.log('objectopportunityExperts', opportunityExperts);
  const verifyExpertReviewerEditable = () => {
    if (opportunity?.opportunity_phase?.name !== 'shape') {
      return true;
    }
    if (!userProfileState.team_lead_flg && !userProfileState.isAdmin) {
      return true;
    }
    return false;
  };
  return (
    <React.Fragment>
      <Dialog
        open={openModal}
        onClose={toggleModal}
        maxWidth={statusAssignExpertDialog ? 'sm' : 'lg'}
        aria-labelledby="scroll-dialog-title"
        className={statusAddMoreCurrentExpert ? classes.dialog : undefined}
      >
        {!statusAddMoreCurrentExpert && !statusUpdateDialog && (
          <Box>
            <Box
              padding={2}
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography variant="h2">{t('opp_team')}</Typography>
              <Box ml={2}>
                {userAddExpertToTeamState && (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() =>
                      setStatusAddMoreCurrentExpert(!statusAddMoreCurrentExpert)
                    }
                  >
                    Add Expert
                  </Button>
                )}
              </Box>
            </Box>
            <DialogContent className={classes.borderNone}>
              <Grid container>
                <Grid item xs={12}>
                  <TableContainer className={classes.rootTableContainer}>
                    <Table className={classes.rootTable}>
                      <ExpertEnhancedTableHead classes={classes} />

                      <TableBody>
                        <TableRow
                          style={{
                            backgroundColor: '#cfd4d4',
                          }}
                          ref={tableRef}
                        >
                          <CommonTableCell
                            value={opportunity?.created_by?.name}
                          />
                          <CommonTableCell value="Proposer" />
                          <EmptyTableCell />
                          <CommonTableCell value="0" />
                          <CommonTableCell
                            value={opportunity?.created_at}
                            isDateValue
                          />
                          <EmptyTableCell />
                          <EmptyTableCell />
                        </TableRow>
                        {opportunity?.current_expert_review?.expert_review &&
                          opportunity?.current_expert_review?.expert_review
                            ?.id > 0 && (
                            <TableRow>
                              <CommonTableCell
                                value={
                                  opportunity?.current_expert_review
                                    ?.expert_review?.name
                                }
                              />
                              <CommonTableCell value="Expert Reviewer" />
                              <EmptyTableCell />
                              <CommonTableCell value="0" />
                              <CommonTableCell
                                value={
                                  opportunity?.current_expert_review
                                    ?.expert_review_assign_date
                                }
                                isDateValue
                              />
                              <CommonTableCell
                                value={
                                  opportunity?.current_expert_review
                                    ?.expert_review_unassign_date
                                }
                                isDateValue
                              />
                              <TableCell
                                padding="none"
                                scope="row"
                                component="th"
                              >
                                <TableAction
                                  hiddenItem={['delete', 'view']}
                                  onClickEdit={() => {
                                    setExpertReviewerDialogStatus(true);
                                  }}
                                  disabledEdit={verifyExpertReviewerEditable()}
                                />
                              </TableCell>
                            </TableRow>
                          )}
                        {opportunityExperts &&
                          opportunityExperts?.length > 0 &&
                          opportunityExperts?.map(opportunityExpert => (
                            <TableRow key={opportunityExpert.id}>
                              <CommonTableCell
                                value={opportunityExpert.expert.name}
                              />
                              <CommonTableCell
                                value={
                                  opportunityExpert.opportunity_team_role.name
                                }
                              />
                              <CommonTableCell
                                value={opportunityExpert.duration}
                              />
                              <CommonTableCell
                                value={opportunityExpert.effort}
                              />
                              <CommonTableCell
                                value={opportunityExpert.assignment_date}
                                isDateValue
                              />
                              <CommonTableCell
                                value={opportunityExpert.un_assignment_date}
                                isDateValue
                              />
                              <TableCell
                                scope="row"
                                component="th"
                                padding="none"
                              >
                                <Box display="flex" justifyContent="center">
                                  {userAddExpertToTeamState && (
                                    <TableAction
                                      hiddenItem={['view']}
                                      onClickEdit={() => {
                                        setStatusUpdateDialog();
                                        setTeamOpportunitySelected(
                                          opportunityExpert,
                                        );
                                      }}
                                      onClickDelete={() => {
                                        setExpertEliminationId(
                                          opportunityExpert.id,
                                        );
                                      }}
                                      disabledDelete={isSubmitting}
                                    />
                                  )}
                                </Box>
                              </TableCell>
                            </TableRow>
                          ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </Grid>

              {opportunityExperts && opportunityExperts.length > 0 && (
                <Box mt={4} mb={1}>
                  <Box mb={2}>
                    <Typography variant="h4">{t('time_line')}</Typography>
                  </Box>
                  {chartWidth && chartData.length > 0 && (
                    <Box>
                      <CustomGanttChart
                        data={chartData}
                        maxWidth={chartWidth}
                        holidays={holidays}
                      />
                    </Box>
                  )}
                </Box>
              )}
              <Box pb={2} pt={2} display="flex" justifyContent="center">
                <Button
                  onClick={toggleModal}
                  color="primary"
                  variant="contained"
                >
                  {t('ok')}
                </Button>
              </Box>
            </DialogContent>
          </Box>
        )}
        {statusAddMoreCurrentExpert && (
          <AddMoreCurrentExpertDialog
            setLoading={setLoading}
            opportunityExperts={opportunityExperts}
            holidays={holidays}
            statusAddMoreCurrentExpert={statusAddMoreCurrentExpert}
            statusAssignExpertDialog={statusAssignExpertDialog}
            setStatusAssignExpertDialog={setStatusAssignExpertDialog}
            setStatusAddMoreCurrentExpert={setStatusAddMoreCurrentExpert}
            onAddRole={onClickAddRole}
          />
        )}

        {statusAddTeamRoleDialog && newRole && (
          <AddTeamRoleDialog
            statusAddTeamRoleDialog={statusAddTeamRoleDialog}
            setStatusAddTeamRoleDialog={setStatusAddTeamRoleDialog}
            roleName={newRole.role}
            skillsName={newRole.skills}
            location={newRole.location}
            teamName={newRole.team}
          />
        )}
      </Dialog>
      <DeleteDialog
        isOpen={!!expertEliminationId}
        header={t('expert.unassign_title')}
        message={t('expert.unassign_confirm')}
        handleClose={() => setExpertEliminationId(undefined)}
        handleDelete={() =>
          expertEliminationId && onRemoveCurrentExpert(expertEliminationId)
        }
        confirmText={'Unassign'}
      />
      {statusUpdateDialog && (
        <UpdateOpportunityTeamDialog
          statusUpdateDialog={statusUpdateDialog}
          setStatusUpdateDialog={setStatusUpdateDialog}
          teamOpportunitySelected={teamOpportunitySelected}
          setLoading={setLoading}
        />
      )}

      {expertReviewerDialogStatus && (
        <UpdateCurrentExpertReviewer
          expertReviewerDialogStatus={expertReviewerDialogStatus}
          handleClose={() => setExpertReviewerDialogStatus(false)}
          handleUpdateOpportunity={handleUpdateOpportunity}
        />
      )}
    </React.Fragment>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
    '& tr td:last-child': {
      width: 120,
    },
  },
  headTable: {
    minWidth: 732,
  },
  dialog: {
    '& .MuiPaper-root': {
      width: '90%',
    },
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
}));
