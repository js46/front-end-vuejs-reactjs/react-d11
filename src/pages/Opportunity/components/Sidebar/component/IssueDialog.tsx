import React from 'react';
import {
  Grid,
  Table,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
  Link,
  Chip,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { StyledReactiveList } from '../../../../../components/styled';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { useTableHeadSorter } from '../../../../../hooks';
import {
  ESLoader,
  HeadCellProps,
  TableHeadSorter,
} from '../../../../../components';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '24px 24px 0px 24px',
  },
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
  pointer: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
  chipOpen: {
    background: '#4EE24E',
    marginLeft: 8,
  },
  chipClose: {
    background: '#E1E1E1',
    marginLeft: 8,
  },
}));

const tableFields = [
  'title.keyword',
  'description.keyword',
  'created_by_user.name.keyword',
  'state.keyword',
] as const;

const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'title.keyword',
    label: 'Title',
  },
  {
    id: 'created_by_user.name.keyword',
    label: 'Created by',
  },
  {
    id: 'description.keyword',
    label: 'Description',
  },
  {
    id: 'state.keyword',
    label: 'State',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

interface IssuesDialogProps {
  openModal: boolean;
  toggleModal?: () => void;
  customQueryES?: () => void;
  setOpen?: React.Dispatch<React.SetStateAction<boolean>>;
}

export const IssuesDialog: React.FC<IssuesDialogProps> = ({
  openModal,
  toggleModal,
  customQueryES,
  setOpen,
}) => {
  const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'title.keyword',
  );
  const defaultQuery = () => {
    return {
      query: {
        match: {
          'opportunity.id': opportunity?.id,
        },
      },
    };
  };

  return (
    <>
      <Dialog
        open={openModal}
        onClose={!!toggleModal ? toggleModal : () => setOpen?.(false)}
        maxWidth="lg"
        aria-labelledby="scroll-dialog-title"
        className={classes.dialog}
      >
        <Box
          pt={2}
          className={classes.titleDialog}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Typography variant="h2">Issues</Typography>
        </Box>
        <DialogContent className={classes.borderNone}>
          <Grid container>
            <Grid item xs={12}>
              <ReactiveBase
                app={ES_INDICES.issue_index_name}
                url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                headers={{
                  Authorization: window.localStorage.getItem('jwt'),
                }}
              >
                <StyledReactiveList
                  componentId="SearchIssues"
                  dataField={orderBy}
                  sortBy={order}
                  renderResultStats={() => null}
                  size={5}
                  paginationAt="bottom"
                  pages={5}
                  pagination={true}
                  renderNoResults={() => <></>}
                  loader={<ESLoader />}
                  defaultQuery={customQueryES ? customQueryES : defaultQuery}
                  render={({ data, loading }) => {
                    if (loading) return null;
                    if (!data?.length)
                      return (
                        <Box mt={3}>
                          <Typography>
                            {customQueryES
                              ? t('issue.no_issue_found_in_playground')
                              : t('opp_has_not_issues')}
                          </Typography>
                        </Box>
                      );

                    return (
                      <>
                        {data?.length === 0 ? (
                          <Box mt={2}>
                            <Typography>
                              {customQueryES
                                ? t('issue.no_issue_found_in_playground')
                                : t('opp_has_not_issues')}
                            </Typography>
                          </Box>
                        ) : (
                          <TableContainer
                            className={classes.rootTableContainer}
                          >
                            <Table className={classes.rootTable}>
                              <TableHeadSorter
                                onRequestSort={handleRequestSort}
                                order={order}
                                orderBy={orderBy}
                                cells={headCells}
                              />
                              <TableBody>
                                {data?.map((issue: any) => {
                                  return (
                                    <TableRow key={issue.id}>
                                      <TableCell component="th" scope="row">
                                        <Link
                                          component={RouterLink}
                                          color="inherit"
                                          to={`/opportunity/${issue.opportunity.id}/playbook-${issue.opportunity_playbook.id}?step=${issue.opportunity_playbook_item.id}`}
                                        >
                                          {issue.title}
                                        </Link>
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {issue.created_by_user.name}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {issue.description}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        <Chip
                                          label={issue.state}
                                          size="small"
                                          className={
                                            issue.state === 'open'
                                              ? classes.chipOpen
                                              : classes.chipClose
                                          }
                                        />
                                      </TableCell>
                                    </TableRow>
                                  );
                                })}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        )}
                      </>
                    );
                  }}
                  innerClass={{
                    button: classes.buttonPagination,
                    resultsInfo: classes.resultsInfo,
                    sortOptions: classes.sortSelect,
                  }}
                />
              </ReactiveBase>
            </Grid>
          </Grid>
          <Box pb={2} pt={2} display="flex" justifyContent="flex-end">
            <Button
              onClick={!!toggleModal ? toggleModal : () => setOpen?.(false)}
              color="primary"
              variant="contained"
            >
              OK
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};
