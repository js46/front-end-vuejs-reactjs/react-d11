import React from 'react';
import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { OpportunityState } from '../../../../../store/opportunity/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '24px 24px 0px 24px',
  },
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
}));

interface Data {
  id: number;
  resource_team: string;
  phase: string;
  status: string;
  priority: string;
  estimated_completion: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'Opp #',
  },
  {
    id: 'resource_team',
    label: 'Resource team',
  },
  {
    id: 'phase',
    label: 'Phase',
  },
  {
    id: 'status',
    label: 'Status - waiting',
  },
  {
    id: 'priority',
    label: 'Priority',
  },
  {
    id: 'estimated_completion',
    label: 'Estimated completion',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell key={headCell.id}>{headCell?.label ?? ''}</TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface IExpertSummaryDialogProps {
  openModal: boolean;
  toggleModal: () => void;
}

export const ExpertSummaryDialog: React.FC<IExpertSummaryDialogProps> = ({
  openModal,
  toggleModal,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  return (
    <Dialog
      open={openModal}
      onClose={toggleModal}
      maxWidth="lg"
      aria-labelledby="scroll-dialog-title"
      className={classes.dialog}
    >
      <Box
        pt={2}
        className={classes.titleDialog}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography variant="h2">{t('summary')}</Typography>
      </Box>
      <DialogContent className={classes.borderNone}>
        <Grid container>
          <Grid item xs={12}>
            <TableContainer className={classes.rootTableContainer}>
              <Table className={classes.rootTable}>
                <EnhancedTableHead classes={classes} />
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      {opportunity?.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {opportunity?.resource_team?.name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {opportunity?.opportunity_phase?.display_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {opportunity?.opportunity_state?.display_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Typography
                        variant="body1"
                        children={
                          opportunity?.opp_priority_score?.priority_level +
                          '(' +
                          `${opportunity?.opp_priority_score?.priority_score}/100` +
                          ')'
                        }
                      />
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {opportunity?.completion_date !== '0001-01-01T00:00:00Z'
                        ? moment(opportunity?.completion_date).format(
                            'DD/MM/YYYY',
                          )
                        : ''}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
        <Box pb={2} pt={2} display="flex" justifyContent="center">
          <Button onClick={toggleModal} color="primary" variant="contained">
            OK
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};
