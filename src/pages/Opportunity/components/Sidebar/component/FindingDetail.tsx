import React, { useState, useEffect } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Box,
  Typography,
  Button,
  DialogActions,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ShareOutlined } from '@material-ui/icons';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { useSelector } from 'react-redux';
import { UserID } from '../../../../../store/user/selector';
import {
  PlaybookItemFindingT,
  getFindingDetail,
} from '../../../../../services/playbook.services';
import {
  deleteFavoriteItem,
  addFavoriteItem,
  EntityType,
} from '../../../../../services/favorite.services';
import { useDispatch } from 'react-redux';
import { notifyError } from '../../../../../store/common/actions';
import { Message } from '../../../../../constants';
import { ExportFinding } from '../../../../../components';

const useStyles = makeStyles(theme => ({
  dialog: {
    padding: '15px 0px',
  },
  content: {
    borderRadius: 5,
    border: '1px solid #ccc',
    minHeight: 120,
    padding: '0 15px',
  },
}));

interface FindingDetailProps {
  openModal: boolean;
  toggleModal: React.Dispatch<React.SetStateAction<boolean>>;
  findingSelected?: PlaybookItemFindingT;
}

export const FindingDetail: React.FC<FindingDetailProps> = ({
  openModal,
  toggleModal,
  findingSelected,
}) => {
  const classes = useStyles();
  const currentUserID = useSelector(UserID);
  const dispatch = useDispatch();
  const [favorite, setFavorite] = useState<boolean>(false);
  const [favoriteCounts, setFavoritesCount] = useState<number>(0);
  const [finding, setFinding] = useState<PlaybookItemFindingT>();
  const [isShare, setIsShare] = useState<boolean>(false);

  const handleLikeAction = async (event?: React.ChangeEvent<{}>) => {
    event?.stopPropagation();

    if (!finding?.id) return;
    try {
      const favoriteItem = finding.favorites?.find(
        item => item.user_id === currentUserID,
      );
      if (favorite) {
        setFavoritesCount(favoriteCounts - 1);
      } else {
        setFavoritesCount(favoriteCounts + 1);
      }

      if (favoriteItem) {
        await deleteFavoriteItem(favoriteItem.id);
        getFinding(finding.id);
      } else {
        await addFavoriteItem(EntityType.findings, finding.id);
        getFinding(finding.id);
      }
      setFavorite(!favorite);
    } catch (e) {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
  };

  const getFinding = async (id: number) => {
    await getFindingDetail(id).then(({ data }) => {
      setFinding(data);
      if (data.favorites !== null) {
        setFavorite(true);
        setFavoritesCount(data.favorites?.length ?? 0);
      } else {
        setFavorite(false);
      }
    });
  };

  useEffect(() => {
    if (findingSelected) {
      getFinding(findingSelected.id);
    }
  }, [findingSelected]);

  const renderTitleIcons = (findingSelected?: PlaybookItemFindingT) => {
    return (
      <>
        <Button
          startIcon={
            finding?.favorites?.find(item => item.user_id === currentUserID) ? (
              <ThumbUpIcon />
            ) : (
              <ThumbUpAltOutlinedIcon />
            )
          }
          onClick={handleLikeAction}
        >
          <b>{favoriteCounts ?? 0}</b>
        </Button>

        <Box component="span">
          <Button
            startIcon={<ShareOutlined />}
            onClick={() => setIsShare(true)}
          />
        </Box>
      </>
    );
  };
  return (
    <>
      <Dialog
        open={openModal}
        onClose={() => toggleModal(false)}
        className={classes.dialog}
        maxWidth="md"
        fullWidth
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Box display="flex" alignContent="center" pt={2}>
            <Typography variant="h3">{finding?.title}</Typography>
            <Box display="flex" alignItems="center" ml={2}>
              {renderTitleIcons(finding)}
            </Box>
          </Box>
        </DialogTitle>
        <DialogContent>
          <Box mt={2} mb={2}>
            <Box
              mt={2}
              mb={2}
              dangerouslySetInnerHTML={{
                __html: finding?.description
                  ? finding?.description.replace(/\n/g, '<br />')
                  : '',
              }}
              className={classes.content}
            />
            <Typography variant="h4">Comments</Typography>
          </Box>
        </DialogContent>
        <DialogActions>
          <Box display="flex" justifyContent="center" flex={1} mt={2} mb={2}>
            <Button
              onClick={() => toggleModal(false)}
              color="primary"
              variant="contained"
            >
              OK
            </Button>
          </Box>
        </DialogActions>
      </Dialog>

      <ExportFinding
        open={isShare}
        close={() => {
          setIsShare(false);
        }}
        finding={finding}
      />
    </>
  );
};
