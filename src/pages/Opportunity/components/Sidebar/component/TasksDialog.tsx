import React, { useState } from 'react';
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import {
  SortOrder,
  TaskKanban,
  TaskTable,
  TaskFormDialog,
  MixTitle,
} from '../../../../../components';
import { StyledSingleDropdownList } from '../../../../../components/styled';
import {
  addTaskNew,
  IOpportunityTask,
  updateTaskState,
} from '../../../../../services/task.services';
import {
  TaskState,
  TaskStateID,
  TaskSubTypeID,
  TaskTypeID,
} from '../../../../../constants';
import { useTableHeadSorter, useToggleValue } from '../../../../../hooks';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { OpportunityState } from '../../../../../store/opportunity/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    height: 52,
  },
  dialog: {
    minWidth: '50vw',
  },
  borderNone: {
    border: 'none',
    overflowY: 'initial',
  },
}));

interface IExpertTasksDialogProps {
  openModal: boolean;
  toggleModal: () => void;
}
const tableFields = [
  'title.keyword',
  'task_type.keyword',
  'entity_type.keyword',
  'task_state.keyword',
  'created_at',
  'created_by_user.name.keyword',
  'assignee_user.name.keyword',
  'last_updated_at',
  'requested_completion_date',
] as const;

export const ExpertTasksDialog: React.FC<IExpertTasksDialogProps> = ({
  openModal,
  toggleModal,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const { t } = useTranslation();
  const [data, setData] = useState<IOpportunityTask[]>([]);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'last_updated_at',
    SortOrder.DESC,
  );
  const [lastRefresh, setLastRefresh] = useState(0);
  const [isTableView, toggleTableView] = useToggleValue(false);
  const [showFormDialog, toggleFormDialog] = useToggleValue(false);
  const opportunity = useSelector(OpportunityState);

  const handleStateUpdated = async (id: number, newState: TaskState) => {
    let newData = data.map(item => {
      if (item.id !== id) return item;
      return { ...item, task_state: newState };
    });
    setData(newData);
    const json = await updateTaskState(id, TaskStateID[newState]);
    if (json.success && json.data) {
      newData = data.map(item => {
        if (item.id !== id) return item;
        return json.data;
      });
      setData(newData);
    }
  };

  const handleTaskAdded = async (
    title: string,
    assignee: number,
    description?: string,
    dueDate?: Date,
  ) => {
    await addTaskNew({
      entity_id: opportunity?.id || 0,
      entity_type: 'opportunity',
      title,
      description: description || '',
      assignee_user_id: assignee,
      task_state_id: TaskStateID[TaskState.Assigned],
      task_sub_type_id: TaskSubTypeID.Review,
      task_type_id: TaskTypeID.User,
      requested_completion_date: dueDate,
    });
    if (showFormDialog) toggleFormDialog();
    setTimeout(() => setLastRefresh(Date.now()), 1000);
  };

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'opportunity.id': opportunity?.id,
              },
            },
          ],
          should: [
            {
              term: {
                dummy: lastRefresh || 0,
              },
            },
          ],
        },
      },
    };
  };
  return (
    <>
      <Dialog
        open={openModal}
        onClose={toggleModal}
        maxWidth="lg"
        fullWidth
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
        className={classes.dialog}
      >
        <ReactiveBase
          app={ES_INDICES.opportunity_task_index_name}
          url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
          headers={{
            Authorization: window.localStorage.getItem('jwt'),
          }}
        >
          <DialogTitle id="scroll-dialog-title">
            <Box
              className={classes.titleDialog}
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              flexWrap="wrap"
            >
              <Typography variant="h2">{t('tasks')}</Typography>
              {isTableView && (
                <Box display="flex" flexDirection="row" flexWrap="wrap">
                  <Box
                    m={1}
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                  >
                    <MixTitle title="Type" />
                    <Box width={185} ml={2}>
                      <StyledSingleDropdownList
                        componentId="taskType"
                        dataField="task_type.keyword"
                        placeholder="All"
                        showCount={false}
                      />
                    </Box>
                  </Box>
                  <Box
                    m={1}
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                  >
                    <MixTitle title="Assignee" />
                    <Box width={185} ml={2}>
                      <StyledSingleDropdownList
                        componentId="assignee"
                        dataField="assignee_user.name.keyword"
                        placeholder="All"
                        showCount={false}
                      />
                    </Box>
                  </Box>
                  <Box
                    m={1}
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                  >
                    <MixTitle title="State" />
                    <Box width={185} ml={2}>
                      <StyledSingleDropdownList
                        componentId="taskState"
                        dataField="task_state.keyword"
                        placeholder="All"
                        showCount={false}
                      />
                    </Box>
                  </Box>
                </Box>
              )}
              <Box display="flex" flexDirection="row">
                <Box mr={2}>
                  <Button
                    color="primary"
                    variant="text"
                    onClick={toggleTableView}
                  >
                    {`View in ${isTableView ? 'swimlanes' : 'table'}`}
                  </Button>
                </Box>

                <Button
                  color="primary"
                  variant="contained"
                  onClick={toggleFormDialog}
                >
                  Add
                </Button>
              </Box>
            </Box>
          </DialogTitle>
          <DialogContent className={classes.borderNone}>
            <ReactiveList
              react={{
                and: ['taskState', 'taskType', 'assignee'],
              }}
              componentId="opportunity-task"
              dataField={orderBy}
              sortBy={order}
              renderResultStats={() => null}
              renderNoResults={() => null}
              defaultQuery={customQuery}
              onData={({ data }) => setData(data)}
              size={100}
              showLoader={false}
            >
              {({ loading }) =>
                loading ? (
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    height="calc(100vh - 189px - 60px)"
                  >
                    <CircularProgress />
                  </Box>
                ) : (
                  <>
                    {isTableView ? (
                      <TaskTable
                        data={data}
                        onRequestSort={handleRequestSort}
                        order={order}
                        orderBy={orderBy}
                        styleContainer={{
                          minHeight: `calc(100vh - 189px - 60px)`,
                        }}
                      />
                    ) : (
                      <TaskKanban
                        data={data}
                        todoTitle="Assigned"
                        onStateUpdated={handleStateUpdated}
                        inDialog
                      />
                    )}
                  </>
                )
              }
            </ReactiveList>
          </DialogContent>
        </ReactiveBase>
        <DialogActions style={{ justifyContent: 'center' }}>
          <Button onClick={toggleModal} color="default" variant="outlined">
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <TaskFormDialog
        open={showFormDialog}
        toggle={toggleFormDialog}
        onSubmit={handleTaskAdded}
      />
    </>
  );
};
