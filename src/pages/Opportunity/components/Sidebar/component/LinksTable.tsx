import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Box,
  Link,
  Button,
} from '@material-ui/core';
import {
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from '../../../../../components';
import { useTableHeadSorter } from '../../../../../hooks';

const tableFields = [
  'name.keyword',
  'description.keyword',
  'url.keyword',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'name.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'description.keyword',
    label: 'Description',
    disablePadding: true,
  },
  {
    id: 'url.keyword',
    label: 'URL #',
    disablePadding: true,
  },
];
interface Opportunity {
  id: number;
  title: string;
  opportunity_phase: {
    id: number;
    name: string;
    display_name: string;
  };
  date: string;
  priority_score: number;
  priority_level: string;
  created_by_user: { id: number; name: string };
}
interface LinksTableProps {
  data: Opportunity[];
  toggleModal: () => void;
}
export const LinksTable: React.FC<LinksTableProps> = props => {
  const { data, toggleModal } = props;
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    '',
    SortOrder.DESC,
  );

  return (
    <Box>
      <TableContainer>
        <Table style={{ minWidth: 850 }}>
          <TableHeadSorter
            onRequestSort={handleRequestSort}
            order={order}
            orderBy={orderBy}
            cells={headCells}
          />
          <TableBody>
            {data?.map((item: any) => (
              <TableRow key={item.id} hover>
                <TableCell component="th" scope="row">
                  {item?.name}
                </TableCell>

                <TableCell component="th" scope="row">
                  {item?.description}
                </TableCell>
                <TableCell component="th" scope="row">
                  <Link color="inherit" href={item?.url} target="_blank">
                    {item?.url}
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Box pt={2} display="flex" justifyContent="center">
        <Button onClick={toggleModal} color="primary" variant="contained">
          OK
        </Button>
      </Box>
    </Box>
  );
};
