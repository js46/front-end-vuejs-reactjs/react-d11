import React, { createRef, useCallback, useEffect, useState } from 'react';
import {
  Box,
  TableCell,
  TableRow,
  IconButton,
  Typography,
} from '@material-ui/core';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

import { StyledReactiveList } from '../../../../../components/styled';
import { IProfileData } from '../../../../../services/profile.services';
import { useToggleValue } from '../../../../../hooks';
import {
  CapacityBarChart,
  CapacityBarChartGroup,
  MixTitle,
} from '../../../../../components';
import { isWeekend } from '../../../../../utils';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { OpportunityState } from '../../../../../store/opportunity/selector';

interface ExpertCapacityChartProps {
  id: number;
  item: any;
  currentExperts?: number[];
  userFocus: any;
  setUserFocus: any;
  handleSelectedUser(user: IProfileData): void;
  onDurationChange?: (from?: Date, to?: Date) => void;
  defaultRange?: number;
  holidays: Map<string, string>;
  opportunityCount?: number;
}
type ChartSummary = {
  assignmentDate: string;
  duration: number;
  effort: number;
};
const BAR_CHART_DAYS = 90;
export const ExpertCapacityChart: React.FC<ExpertCapacityChartProps> = ({
  id,
  item,
  currentExperts,
  handleSelectedUser,
  userFocus,
  setUserFocus,
  onDurationChange,
  defaultRange,
  holidays,
  opportunityCount,
}) => {
  const tableRef = createRef<HTMLTableElement>();
  const ES_INDICES = useSelector(ConfigESIndex);
  const [isExpanded, toggleExpanded] = useToggleValue(false);
  const [chartData, setChartData] = useState<CapacityBarChart>();
  const [chartWidth, setChartWidth] = useState<number>();
  const [chartSummary, setChartSummary] = useState<ChartSummary>();
  const [range, setRange] = useState(defaultRange);
  const [activeStartIndex, setActiveStartIndex] = useState<number>();
  const opportunity = useSelector(OpportunityState);

  useEffect(() => {
    if (
      tableRef.current &&
      !chartWidth &&
      chartWidth !== tableRef.current.clientWidth - 32
    ) {
      setChartWidth(tableRef.current.clientWidth - 32);
    }
  }, [tableRef, chartWidth]);

  const animatedStyle = {
    transition: '0.3s all',
    transform: isExpanded ? 'rotate(90deg)' : 'rotate(0deg)',
  };
  const classes = useStyles();
  const defaultQuery = useCallback(() => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                delete_flg: false,
              },
            },
            {
              term: {
                'expert.id': id,
              },
            },
            {
              range: {
                assignment_date: {
                  lte: moment()
                    .add(90, 'days')
                    .format('YYYY-MM-DD'),
                },
              },
            },
            {
              range: {
                un_assignment_date: {
                  gte: moment().format('YYYY-MM-DD'),
                },
              },
            },
          ],
        },
      },
      aggs: aggregationQuery,
    };
  }, [id]);

  const existingExpert = currentExperts?.includes(item.id);
  let isAssigned: boolean | undefined = false;
  if (opportunityCount && opportunityCount > 0 && existingExpert) {
    isAssigned = existingExpert;
  } else {
    isAssigned = false;
  }

  const handleAggregationData = (aggs?: AggregationData) => {
    const end = moment().add(BAR_CHART_DAYS, 'days');
    let chartData: CapacityBarChart = [];
    let day = moment();
    // ** Generate Chart Data
    while (day.isSameOrBefore(end, 'day')) {
      let opportunityCount = 0;
      const holiday = holidays.get(day.format('DD/MM'));
      // ** case 1: No effort or holidays or weekends
      if (!aggs?.capacities.buckets?.length || isWeekend(day) || holiday) {
        let chartItem: CapacityBarChart[number] = {
          name: day.format('DD/MM'),
          occupied: 0,
          occupiedLabel: 0,
          idle: 100,
          value: day.toDate(),
          count: 0,
        };
        if (isWeekend(day) || holiday) {
          chartItem.disabled = true;
          chartItem.customTooltip = holiday || 'Weekend';
        }
        chartData.push(chartItem);
        day.add(1, 'day');
        continue;
      }
      // ** case 2: Working days
      let utilized = 0;
      for (let item of aggs.capacities.buckets) {
        if (
          day.isSameOrAfter(moment(item.from.value), 'day') &&
          day.isSameOrBefore(moment(item.to.value), 'day')
        ) {
          utilized += item.utilized.value;
          opportunityCount += 1;
        }
      }
      const idle = 100 - utilized * 100;
      const occupied = Math.round(utilized * 100);
      chartData.push({
        name: day.format('DD/MM'),
        occupied: occupied > 100 ? 100 : occupied,
        occupiedLabel: occupied,
        idle: idle >= 0 ? idle : 0,
        value: day.toDate(),
        count: opportunityCount,
      });
      day.add(1, 'day');
    }
    setChartData(chartData);
    // ** Generate Chart Summary
    if (isAssigned && aggs?.capacities.buckets) {
      const statistics = aggs.capacities.buckets.find(
        item => item.key === opportunity?.id,
      );
      if (statistics) {
        setChartSummary({
          assignmentDate: statistics.from.value_as_string,
          duration: statistics.d.value,
          effort: statistics.e.value,
        });
        // ** Check Default highlight range in chart
        setRange(statistics.d.value);
        const startIndex = chartData.findIndex(
          datum => datum.name === moment(statistics.from.value).format('DD/MM'),
        );
        if (startIndex >= 0) setActiveStartIndex(startIndex);
      }
    }
  };

  const onFocus = (item: any) => {
    handleSelectedUser(item);
    setUserFocus(item);
  };

  return (
    <>
      <TableRow
        hover
        title={isAssigned ? 'This user was assigned!' : 'Click to assign'}
        classes={{
          root: isAssigned ? classes.rowDisable : classes.rowLink,
          selected: classes.focus,
        }}
        selected={userFocus?.id === item.id}
        onClick={!isAssigned ? () => onFocus(item) : undefined}
        ref={tableRef}
      >
        <TableCell>
          <Box onClick={toggleExpanded}>
            <IconButton size="small">
              <ArrowForwardIosIcon
                style={animatedStyle}
                className={classes.icon}
              />
            </IconButton>
            {item.name !== '' ? item.name : '-'}
          </Box>
        </TableCell>
        <TableCell component="th" scope="row">
          {item.profile_role?.profile_role_name
            ? item.profile_role?.profile_role_name
            : '-'}
        </TableCell>
        <TableCell component="th" scope="row">
          {item.location?.name ? item.location?.name : '-'}
        </TableCell>
        <TableCell component="th" scope="row">
          {item.resource_team?.resource_team_name
            ? item.resource_team?.resource_team_name
            : '-'}
        </TableCell>
        <TableCell component="th" scope="row">
          {opportunityCount ?? 0}
        </TableCell>
      </TableRow>
      <TableRow className={isExpanded ? classes.collRow : classes.detailRow}>
        <TableCell colSpan={5}>
          <Box display="none">
            <ReactiveBase
              app={ES_INDICES.opportunity_expert_index_name}
              url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
              headers={{
                Authorization: window.localStorage.getItem('jwt'),
              }}
            >
              <StyledReactiveList
                componentId="Opportunity List by expert"
                dataField="expert.id"
                aggregationField="opportunity.id"
                renderResultStats={() => null}
                size={0}
                pagination={false}
                showResultStats={false}
                renderNoResults={() => <></>}
                defaultQuery={defaultQuery}
                onData={({ rawData }) => {
                  const aggs: AggregationData = rawData.aggregations;
                  handleAggregationData(aggs);
                }}
                onError={() => handleAggregationData()}
                render={() => null}
              />
            </ReactiveBase>
          </Box>

          {isAssigned && chartSummary && (
            <Box mt={1}>
              <Box display="flex" alignItems="baseline">
                <MixTitle textSmall title={'Assignment Date:'} noMargin />
                <Box ml={1} pr={3}>
                  <Typography
                    variant="body1"
                    children={moment(chartSummary.assignmentDate).format(
                      'DD/MM/YYYY',
                    )}
                  />
                </Box>
                <MixTitle textSmall title={'Duration:'} noMargin />
                <Box ml={1} pr={3}>
                  <Typography
                    variant="body1"
                    children={chartSummary.duration}
                  />
                </Box>
                <MixTitle textSmall title={'Effort:'} noMargin />
                <Box ml={1} pr={3}>
                  <Typography variant="body1" children={chartSummary.effort} />
                </Box>
              </Box>
            </Box>
          )}
          {chartWidth && chartData && (
            <CapacityBarChartGroup
              maxWidth={chartWidth}
              dataGroup={[
                {
                  data: chartData,
                  onDurationChange,
                  disabledSelect: isAssigned,
                  defaultRange: range,
                  defaultActiveIndex: activeStartIndex,
                },
              ]}
            />
          )}
        </TableCell>
      </TableRow>
    </>
  );
};
interface AggregationData {
  capacities: {
    buckets?: {
      key: number;
      utilized: {
        value: number;
      };
      d: { value: number };
      e: { value: number };
      from: { value: number; value_as_string: string };
      to: { value: number; value_as_string: string };
    }[];
  };
}
const aggregationQuery = {
  capacities: {
    terms: {
      field: 'opportunity.id',
    },
    aggs: {
      from: {
        min: {
          field: 'assignment_date',
        },
      },
      to: {
        max: {
          field: 'un_assignment_date',
        },
      },
      d: {
        avg: {
          field: 'duration',
        },
      },
      e: {
        avg: {
          field: 'effort',
        },
      },
      utilized: {
        bucket_script: {
          buckets_path: {
            d: 'd',
            e: 'e',
          },
          script:
            'BigDecimal.valueOf(params.e / params.d).setScale(2, RoundingMode.HALF_UP)',
        },
      },
    },
  },
};

const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  detailRow: {
    display: 'none',
  },
  collRow: {
    display: 'table-row',
  },
  rowDisable: {
    backgroundColor: theme.palette.grey[100],
    '&:hover': {
      backgroundColor: `${theme.palette.grey[100]} !important`,
    },
    '& > .MuiTableCell-body': {
      color: theme.palette.text.disabled,
    },
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: `${theme.palette.grey[100]} !important`,
    },
  },
  focus: {
    backgroundColor: `${theme.palette.grey[300]}!important`,
  },
}));
