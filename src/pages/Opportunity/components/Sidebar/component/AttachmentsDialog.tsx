import React, { useState } from 'react';
import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
  IconButton,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  IOpportunity,
  AttachmentResponse,
  deleteAttachments,
} from '../../../../../services/opportunity.services';
import { useSelector } from 'react-redux';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import CloudDownloadOutlined from '@material-ui/icons/CloudDownloadOutlined';
import moment from 'moment';
import {
  DeleteDialog,
  AddAttachmentDialog,
  TableAction,
} from '../../../../../components';
import { useDisableMultipleClick, useUnmounted } from '../../../../../hooks';

import { useTranslation } from 'react-i18next';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { UserProfileState } from '../../../../../store/user/selector';
import { OpportunityState } from '../../../../../store/opportunity/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '16px 24px 0px 16px',
  },
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
}));

interface Data {
  description?: string;
  file_name?: string;
  created_by?: string;
  date?: string;
  phase?: string;
  action?: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'file_name',
    label: 'File name/URL link',
  },
  {
    id: 'description',
    label: 'Description',
  },
  {
    id: 'created_by',
    label: 'Created by',
  },
  {
    id: 'date',
    label: 'Date',
  },
  {
    id: 'phase',
    label: 'Phase',
  },
  {
    id: 'action',
    label: 'Action',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell key={headCell.id} align="center">
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface IExpertAttachmentsDialogProps {
  openModal: boolean;
  toggleModal: () => void;
}

export const ExpertAttachmentsDialog: React.FC<IExpertAttachmentsDialogProps> = ({
  openModal,
  toggleModal,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [currentTime, setCurrentTime] = useState(1);
  const opportunity = useSelector(OpportunityState);
  const [isOpenAddAttachmentDialog, setOpenAddAttachmentDialog] = useState(
    false,
  );
  const isUnmounted = useUnmounted();

  const handleAddAttachment = () => {
    setOpenAddAttachmentDialog(true);
  };

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: {
            bool: {
              must: [
                {
                  term: {
                    opportunity_id: opportunity?.id,
                  },
                },
              ],
              should: [
                {
                  terms: {
                    entity_type: ['opportunity', 'opportunity_playbook_item'],
                  },
                },
              ],
            },
          },
          filter: {
            term: {
              del_flg: false,
            },
          },
          should: [
            {
              term: {
                dummy: currentTime,
              },
            },
          ],
        },
      },
    };
  };
  return (
    <React.Fragment>
      <Dialog
        open={openModal}
        onClose={toggleModal}
        maxWidth="lg"
        aria-labelledby="scroll-dialog-title"
        className={classes.dialog}
      >
        <Box
          pt={2}
          className={classes.titleDialog}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
        >
          <Typography variant="h2">{t('attachment')}</Typography>
          <Button
            variant="contained"
            color="primary"
            onClick={handleAddAttachment}
          >
            {t('add')}
          </Button>
        </Box>
        <DialogContent className={classes.borderNone}>
          <AttachmentTable
            opportunity={opportunity}
            style={classes}
            defaultQuery={customQuery}
            currentTime={currentTime}
            setCurrentTime={setCurrentTime}
          />
          <Box pb={2} pt={2} display="flex" justifyContent="center">
            <Button onClick={toggleModal} color="primary" variant="contained">
              {t('ok')}
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
      {isOpenAddAttachmentDialog && (
        <AddAttachmentDialog
          isOpen={isOpenAddAttachmentDialog}
          handleClose={() => {
            setOpenAddAttachmentDialog(false);
            setTimeout(() => {
              if (isUnmounted.current) return;
              setCurrentTime(currentTime + 1);
            }, 1000);
          }}
          entity_id={opportunity?.id ? opportunity?.id : 0}
          entity_type="opportunity"
        />
      )}
    </React.Fragment>
  );
};

interface AttachmentTableProps {
  opportunity?: IOpportunity;
  style: any;
  defaultQuery: (...args: any[]) => any;
  currentTime: number;
  setCurrentTime: React.Dispatch<React.SetStateAction<number>>;
}

const AttachmentTable: React.FC<AttachmentTableProps> = ({
  opportunity,
  style,
  defaultQuery,
  currentTime,
  setCurrentTime,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = style;
  const userProfileState = useSelector(UserProfileState);
  const { t } = useTranslation();
  const isUnmounted = useUnmounted();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [attachmentToDeleteId, setAttachmentToDeleteId] = useState<number>();
  const handleDeleteAttachment = async (idAttachment: number) => {
    if (isSubmitting) return;
    await debounceFn();
    const response = await deleteAttachments(idAttachment);
    if (response.success) {
      setTimeout(() => {
        if (isUnmounted.current) return;
        setCurrentTime(currentTime + 1);
      }, 1000);
      setOpenDeleteDialog(false);
    }
    endRequest();
  };
  const onClickDownload = (item: AttachmentResponse) => {
    const urlDownload = `${item.storage_prefix}/${item.attachment_name}`;
    window.open(urlDownload, '_blank');
  };
  return (
    <>
      <ReactiveBase
        app={ES_INDICES.attachment_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{
          Authorization: window.localStorage.getItem('jwt'),
        }}
      >
        <ReactiveList
          componentId="SearchAttachment"
          dataField="attachment"
          renderResultStats={() => null}
          loader={<></>}
          renderNoResults={() => <></>}
          defaultQuery={defaultQuery}
        >
          {({ data }) => {
            return (
              <Box mt={3}>
                {data.length === 0 ? (
                  <Typography>{t('opp_has_not_attachment')}</Typography>
                ) : (
                  <Grid container>
                    <Grid item xs={12}>
                      <TableContainer className={classes.rootTableContainer}>
                        <Table className={classes.rootTable}>
                          <EnhancedTableHead classes={classes} />
                          <TableBody>
                            {data?.map(
                              (item: AttachmentResponse, index: number) => (
                                <TableRow key={index}>
                                  <TableCell>
                                    {item?.client_file_name}
                                  </TableCell>
                                  <TableCell>{item?.description}</TableCell>
                                  <TableCell>
                                    {item?.created_by_user?.name}
                                  </TableCell>
                                  <TableCell>
                                    {item?.time &&
                                      moment(item.time).format(
                                        'DD/MM/YYYY hh:mma',
                                      )}
                                  </TableCell>
                                  <TableCell>
                                    {item?.opportunity_phase?.display_name}
                                  </TableCell>
                                  <TableCell>
                                    <TableAction
                                      hiddenItem={['edit', 'view']}
                                      onClickDelete={() => {
                                        setOpenDeleteDialog(true);
                                        setAttachmentToDeleteId(item.id);
                                      }}
                                      disabledDelete={
                                        item.created_by_user?.id !==
                                        userProfileState.id
                                      }
                                    >
                                      <IconButton
                                        onClick={() => onClickDownload(item)}
                                        disabled={item?.storage_type !== 's3'}
                                      >
                                        <CloudDownloadOutlined
                                          fill={
                                            item?.storage_type !== 's3'
                                              ? 'gray'
                                              : undefined
                                          }
                                        />
                                      </IconButton>
                                    </TableAction>
                                  </TableCell>
                                </TableRow>
                              ),
                            )}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </Grid>
                  </Grid>
                )}
              </Box>
            );
          }}
        </ReactiveList>
      </ReactiveBase>
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Attachment"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_this_attachment')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (attachmentToDeleteId) {
            handleDeleteAttachment(attachmentToDeleteId);
          }
        }}
        disabled={isSubmitting}
      />
    </>
  );
};
