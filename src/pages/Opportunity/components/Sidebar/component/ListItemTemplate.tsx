import React from 'react';
import { Box, TextField } from '@material-ui/core';
import { IChildListTree } from '../../../../Playbook/types';
import { MixTitle } from '../../../../../components';
interface RenderListItemTemplateProps {
  item: IChildListTree;
  position: number[];
}
export const ListItemTemplate: React.FC<RenderListItemTemplateProps> = props => {
  const { item, position } = props;
  const label = position.join('.');
  return (
    <Box>
      <Box
        display="flex"
        flex="1"
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        flexWrap="wrap"
        key={item.id}
        ml={position.length === 1 ? 0 : position.length}
      >
        <Box mt={1} mr={1}>
          <MixTitle title={label} />
        </Box>
        <Box flex="1" mt={1}>
          <TextField
            variant="outlined"
            required
            fullWidth
            margin="normal"
            disabled={true}
            value={item.title}
          />
        </Box>
      </Box>
      <Box>
        {item.children &&
          item.children?.length > 0 &&
          item.children.map((child, index) => {
            return (
              <ListItemTemplate
                key={child.id}
                item={child}
                position={[...position, index + 1]}
              />
            );
          })}
      </Box>
    </Box>
  );
};
