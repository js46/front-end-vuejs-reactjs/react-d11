import React, { useState } from 'react';
import moment from 'moment';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Grid,
  Checkbox,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useTableHeadSorter } from '../../../../../hooks';
import {
  // getAllPlaybookOfOpportunity,
  PlaybookItemFindingT,
} from '../../../../../services/playbook.services';
import {
  HeadCellProps,
  TableHeadSorter,
  ESLoader,
} from '../../../../../components';
import { Link as RouterLink } from 'react-router-dom';
import { Link } from '@material-ui/core';
import { FindingDetail } from './FindingDetail';
// import { getComparator, stableSort } from '../../../../../commons/util';
// import { FindingTable } from '../../../../Opportunity/components/AnalysisTab/components/ProgressUpdate/components/FindingTable';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { idOpportunityState } from '../../../../../store/opportunity/selector';
const tableFields = [
  'title.keyword',
  'opportunity_playbook.name.keyword',
  'opportunity_playbook_item.title.keyword',
  'last_updated_at',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'opportunity_playbook.name.keyword',
    label: 'Playbook name',
    disablePadding: true,
  },
  {
    id: 'opportunity_playbook_item.title.keyword',
    label: 'Step title',
    disablePadding: true,
  },
  {
    id: 'last_updated_at',
    label: 'Last updated',
    disablePadding: true,
  },
];
const useStyles = makeStyles(theme => ({
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
  title: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));
interface Finding {
  id: number;
  finding: PlaybookItemFindingT;
  playbookName: string;
  stepTitle: string;
  updateData: string;
  playbook_id: number;
  current_step: number;
  opportunity_id: number;
  public?: boolean;
  del_flg?: boolean;
}
interface IExpertFindingsDialog {
  openModal: boolean;
  toggleModal?: () => void;
  customQueryES?: () => void;
  setOpen?: React.Dispatch<React.SetStateAction<boolean>>;
}
export const ExpertFindingsDialog: React.FC<IExpertFindingsDialog> = ({
  openModal,
  toggleModal,
  customQueryES,
  setOpen,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const ES_INDICES = useSelector(ConfigESIndex);
  const opportunityID = useSelector(idOpportunityState);
  // const [totalPages, setTotalPages] = useState<number>(0);
  const [isOpenModal, setOpenModal] = useState<boolean>(false);
  const [findingSelected, setFindingSelected] = useState<
    PlaybookItemFindingT
  >();

  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'last_updated_at',
  );
  const defaultQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'opportunity.id': opportunityID,
              },
            },
            {
              term: {
                entity_type: 'opportunity_playbook_item',
              },
            },
            {
              term: {
                public_flg: true,
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  };
  const onSeeDetailFinding = (item: PlaybookItemFindingT) => {
    setOpenModal(true);
    setFindingSelected(item);
  };
  return (
    <>
      <Dialog
        open={openModal}
        onClose={!!toggleModal ? toggleModal : () => setOpen?.(false)}
        className={classes.dialog}
        maxWidth="md"
        fullWidth
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Box
            pt={2}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <Typography variant="h2">{t('findings')}</Typography>
          </Box>
        </DialogTitle>
        <DialogContent className={classes.borderNone}>
          <Grid container>
            <Grid item xs={12}>
              <ReactiveBase
                app={ES_INDICES.opportunity_index_finding}
                url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                headers={{
                  Authorization: window.localStorage.getItem('jwt'),
                }}
              >
                <ReactiveList
                  componentId="SearchFinding"
                  dataField={orderBy}
                  sortBy={order}
                  renderResultStats={() => null}
                  renderNoResults={() => <></>}
                  loader={<ESLoader />}
                  size={5}
                  paginationAt="bottom"
                  pages={5}
                  pagination={true}
                  // onData={({ data }) => {
                  //   console.log('data', data);
                  // }}
                  defaultQuery={customQueryES ? customQueryES : defaultQuery}
                  render={({ data, loading }) => {
                    if (loading) return <></>;
                    if (!data?.length)
                      return (
                        <Box mt={3}>
                          <Typography>
                            {customQueryES
                              ? t('finding.no_finding_found_in_playground')
                              : t('opp_has_not_list_items')}
                          </Typography>
                        </Box>
                      );
                    return (
                      <>
                        <TableContainer>
                          <Table>
                            <TableHeadSorter
                              onRequestSort={handleRequestSort}
                              order={order}
                              orderBy={orderBy}
                              cells={headCells}
                              public_status={true}
                            />
                            <TableBody>
                              {data.map((item: any, index: number) => (
                                <TableRow key={item.id} hover>
                                  <TableCell component="th" scope="row">
                                    <Box
                                      onClick={() => onSeeDetailFinding(item)}
                                      className={classes.title}
                                    >
                                      {item?.title}
                                    </Box>
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {item?.opportunity_playbook?.name}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    <Link
                                      component={RouterLink}
                                      color="inherit"
                                      to={`/opportunity/${item?.opportunity?.id}/playbook-${item?.opportunity_playbook.id}`}
                                      // ?step=${item.current_step}
                                    >
                                      {item?.opportunity_playbook_item?.title}
                                    </Link>
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {moment(item?.last_updated_at).format(
                                      'DD/MM/YYYY HH:mm:ss',
                                    )}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    <Box display="flex" justifyContent="center">
                                      <Checkbox
                                        checked={item?.public_flg}
                                        disabled
                                        color="default"
                                      />
                                    </Box>
                                  </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </>
                    );
                  }}
                  innerClass={{
                    button: classes.buttonPagination,
                    resultsInfo: classes.resultsInfo,
                    sortOptions: classes.sortSelect,
                  }}
                />
              </ReactiveBase>
            </Grid>
          </Grid>
          {isOpenModal && (
            <FindingDetail
              openModal={isOpenModal}
              toggleModal={setOpenModal}
              findingSelected={findingSelected}
            />
          )}

          <Box display="flex" justifyContent="flex-end" flex={1} mb={2} mt={2}>
            <Button
              onClick={!!toggleModal ? toggleModal : () => setOpen?.(false)}
              color="primary"
              variant="contained"
            >
              OK
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};
