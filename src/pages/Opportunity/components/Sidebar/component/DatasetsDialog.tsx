import React, { useState, useContext, useEffect } from 'react';
import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
  Select,
  MenuItem,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { UserProfileState } from '../../../../../store/user/selector';
import { useSelector } from 'react-redux';
import {
  getDataSets,
  IDataSet,
} from '../../../../../services/references.services';
import {
  addDatasetInOpportunity,
  removeDatasetInOpportunity,
} from '../../../../../services/opportunity.services';
import { MixTitle, TableAction, DeleteDialog } from '../../../../../components';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import OpportunityContext from '../../../OpportunityContext';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '16px 24px 0px 16px',
  },
  dialog: {
    padding: '15px 0px',
    '& .MuiPaper-root': {
      minWidth: '40%',
    },
  },
  borderNone: {
    border: 'none',
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

interface Data {
  name?: string;
  description?: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell key={headCell.id} align="center">
            {headCell?.label ?? ''}
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

interface IDatasets {
  name?: string;
  description?: string;
  created_by?: string;
  date?: string;
  phase?: string;
}

interface IExpertDatasetsDialogProps {
  openModal: boolean;
  toggleModal: () => void;
}

export const ExpertDatasetsDialog: React.FC<IExpertDatasetsDialogProps> = ({
  openModal,
  toggleModal,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [datasets, setDataSets] = useState<IDataSet[]>([]);
  const [listDataset, setListDataset] = useState<IDataSet[]>([]);
  const opportunity = useSelector(OpportunityState);
  const userProfileState = useSelector(UserProfileState);
  const [isAddDataset, setIsAddDataSet] = useState(false);
  const [datasetId, setDatasetId] = useState<number>();
  const [removeID, setRemoveID] = useState<number>();
  const { onRefresh } = useContext(OpportunityContext);

  const isTeamLead =
    userProfileState.team_lead_flg &&
    userProfileState.resource_team.resource_team_id ===
      opportunity?.resource_team?.id;

  const handleAddDataSet = async () => {
    if (opportunity && datasetId) {
      const response = await addDatasetInOpportunity(opportunity.id, {
        data_sets: [datasetId],
      });
      if (response.success) {
        let data: IDataSet[] = [];
        listDataset.map(dataset => {
          return response.data.data_sets?.map(dataInOpp => {
            if (dataInOpp.id === dataset.id) {
              data.push(dataset);
            }
          });
        });

        setDataSets(data);
        onRefresh();
        setIsAddDataSet(false);
      }
    }
  };

  const handleRemoveDateSet = async (id: number) => {
    if (opportunity) {
      const response = await removeDatasetInOpportunity(opportunity.id, [id]);
      if (response.success) {
        let data: IDataSet[] = [];
        listDataset.map(dataset => {
          return response.data.data_sets?.map(dataInOpp => {
            if (dataInOpp.id === dataset.id) {
              data.push(dataset);
            }
          });
        });
        setRemoveID(undefined);
        onRefresh();
        setDataSets(data);
      }
    }
  };

  useEffect(() => {
    const getData = async () => {
      const response = await getDataSets();
      if (response.data.list && opportunity) {
        setListDataset(response.data.list);
        let data: IDataSet[] = [];
        response.data.list.map(dataset => {
          return opportunity.data_sets?.map(dataInOpp => {
            if (dataInOpp.id === dataset.id) {
              data.push(dataset);
            }
          });
        });
        setDataSets(data);
      }
    };
    getData();
  }, [opportunity]);
  return (
    <>
      <Dialog
        open={openModal}
        onClose={toggleModal}
        maxWidth="lg"
        aria-labelledby="scroll-dialog-title"
        className={classes.dialog}
      >
        <Box
          pt={2}
          className={classes.titleDialog}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
        >
          {!isAddDataset ? (
            <Typography variant="h2">{t('datasets')}</Typography>
          ) : (
            <Typography variant="h2">Add Dataset</Typography>
          )}
          <Box ml={2}>
            {(isTeamLead || userProfileState.isAdmin) && !isAddDataset && (
              <Button
                variant="contained"
                color="primary"
                onClick={() => setIsAddDataSet(true)}
              >
                Add Dataset
              </Button>
            )}
          </Box>
        </Box>

        <DialogContent className={classes.borderNone}>
          {!isAddDataset ? (
            <>
              {datasets.length === 0 ? (
                <Box mt={2}>
                  <Typography>{t('opp_has_not_datasets')}</Typography>
                </Box>
              ) : (
                <Grid container>
                  <Grid item xs={12}>
                    <TableContainer className={classes.rootTableContainer}>
                      <Table className={classes.rootTable}>
                        <EnhancedTableHead classes={classes} />
                        <TableBody>
                          {datasets.map((dataset, index) => (
                            <TableRow key={index}>
                              <TableCell>{dataset.name}</TableCell>
                              <TableCell>{dataset.description}</TableCell>
                              <TableCell
                                scope="row"
                                component="th"
                                padding="none"
                              >
                                <Box display="flex" justifyContent="center">
                                  {(isTeamLead || userProfileState.isAdmin) && (
                                    <TableAction
                                      hiddenItem={['edit', 'view']}
                                      onClickDelete={() => {
                                        setRemoveID(Number(dataset.id));
                                      }}
                                    />
                                  )}
                                </Box>
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </Grid>
                </Grid>
              )}
              <Box pb={2} pt={3} display="flex" justifyContent="center">
                <Button
                  onClick={toggleModal}
                  color="primary"
                  variant="contained"
                >
                  OK
                </Button>
              </Box>
            </>
          ) : (
            <>
              <Grid container>
                <Grid item xs={12}>
                  <Box mb={3} mt={3}>
                    <MixTitle title="Dataset" isRequired />
                    <Select
                      fullWidth
                      name="add_dataset"
                      id="add_dataset"
                      variant="outlined"
                      onChange={event =>
                        setDatasetId(Number(event.target.value))
                      }
                      value={datasetId ?? ''}
                      IconComponent={ExpandMoreIcon}
                      MenuProps={{
                        anchorOrigin: {
                          vertical: 'bottom',
                          horizontal: 'left',
                        },
                        transformOrigin: {
                          vertical: 'top',
                          horizontal: 'left',
                        },
                        getContentAnchorEl: null,
                      }}
                    >
                      {listDataset
                        .filter(
                          item =>
                            !datasets.filter(dt => dt.id === item.id).length,
                        )
                        .map(item => (
                          <MenuItem key={item.id} value={item.id}>
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </Box>
                </Grid>
              </Grid>
              <Box pb={2} pt={2} display="flex" justifyContent="center">
                <Box mr={2}>
                  <Button
                    variant="outlined"
                    onClick={() => setIsAddDataSet(false)}
                  >
                    Cancel
                  </Button>
                </Box>
                <Button
                  disabled={datasetId === undefined}
                  onClick={handleAddDataSet}
                  color="primary"
                  variant="contained"
                >
                  Add
                </Button>
              </Box>
            </>
          )}
        </DialogContent>
      </Dialog>
      <DeleteDialog
        isOpen={!!removeID}
        header={t('dataset.delete_dataset_dialog_header')}
        message={t('dataset.delete_dataset_message')}
        handleClose={() => setRemoveID(undefined)}
        handleDelete={() => removeID && handleRemoveDateSet(removeID)}
        confirmText={'Delete'}
      />
    </>
  );
};
