import React, { useContext } from 'react';
import {
  Dialog,
  DialogContent,
  Box,
  Button,
  FormControl,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';
import { FieldArray, Formik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { SelectInRole } from '../../SelectInRole';
import { SelectInSkill } from '../../SelectInSkill';
import {
  addOpportunityRole,
  IOpportunityInRole,
} from '../../../../../services/opportunity.services';

import OpportunityContext from '../../../OpportunityContext';
import { MixTitle } from '../../../../../components';
import { UserProfileState } from '../../../../../store/user/selector';
import { UpdatedOpportunityDetail } from '../../../../../store/opportunity/actions';
import { OpportunityState } from '../../../../../store/opportunity/selector';

const useStyle = makeStyles(theme => ({
  subMenuInRole: {
    fontWeight: 'bold',
  },
  dialog: {
    padding: '15px 0',
  },
  textRoleName: {
    fontWeight: 'bold',
  },
  numberInput: {
    paddingRight: 6,
  },
  iconMinimize: {
    borderRadius: 15,
    backgroundColor: theme.palette.grey[300],
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
}));

interface AddTeamRoleDialogProps {
  statusAddTeamRoleDialog: boolean;
  roleName?: string;
  skillsName?: string[];
  location?: string;
  teamName?: string;
  setStatusAddTeamRoleDialog: () => void;
}

export const AddTeamRoleDialog: React.FC<AddTeamRoleDialogProps> = ({
  statusAddTeamRoleDialog,
  roleName,
  skillsName,
  location,
  teamName,
  setStatusAddTeamRoleDialog,
}) => {
  const classes = useStyle();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userProfileState = useSelector(UserProfileState);
  const opportunity = useSelector(OpportunityState);
  const { locationList, skillList, roleList, teamList } = useContext(
    OpportunityContext,
  );
  const roleNameSelected = roleList?.find(role => role.name === roleName);
  const locationSelected = locationList?.find(item => item.name === location);
  const teamSelected = teamList?.find(item => item.name === teamName);
  const skillsSelected =
    skillList?.filter(item => skillsName?.includes(item.name)) ?? [];

  const onAddTeamRole = async (values: IOpportunityInRole) => {
    if (opportunity) {
      const teamRoleSelected = roleList?.find(role => {
        return role?.id === values.profile_role?.id;
      });
      const params = {
        role_name: teamRoleSelected ? teamRoleSelected?.name : values.role_name,
        profile_role_id: values.profile_role?.id,
        profile_role_level_id: values.profile_role_level_id,
        day: Number(values.day),
        effort: Number(values.effort),
        location_id: values.location?.id,
        resource_team_id: values.resource_team?.id,
        opportunity_role_skills: values.skills,
      };
      await addOpportunityRole(opportunity.id, params)
        .then(async ({ data }) => {
          dispatch(UpdatedOpportunityDetail(data));
          setStatusAddTeamRoleDialog();
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  return (
    <Dialog
      open={statusAddTeamRoleDialog}
      onClose={setStatusAddTeamRoleDialog}
      maxWidth="md"
      className={classes.dialog}
    >
      <Box pt={3} pl={3}>
        <Typography variant="h2">{t('add_new_team_role')} </Typography>
      </Box>
      <DialogContent>
        <Formik
          enableReinitialize
          initialValues={{
            opportunity_roles: {
              id: opportunity?.opportunity_roles?.length,
              role_name: userProfileState.profile_name,
              profile_role: roleNameSelected,
              profile_role_level_id: 1,
              day: 1,
              effort: 1,
              location: locationSelected,
              resource_team: teamSelected,
              skills: skillsSelected.map((item, index) => {
                return {
                  id: index + 1,
                  skill_id: item.id,
                  proficiency_level_id: 1,
                  skill_name: '',
                };
              }),
            },
          }}
          onSubmit={async values => {
            await onAddTeamRole(values.opportunity_roles);
          }}
        >
          {({ values, handleChange, setFieldValue, handleSubmit }) => (
            <FieldArray name="opportunity_roles">
              {() => {
                return (
                  <Grid item xs={12}>
                    <Box m={1}>
                      <Grid container>
                        <SelectInRole
                          name="Role"
                          field={`opportunity_roles.profile_role.id`}
                          fieldForProfileRole={`opportunity_roles.profile_role_level_id`}
                          item={values.opportunity_roles.profile_role}
                          profileRoleLevelId={
                            values.opportunity_roles.profile_role_level_id
                          }
                          profileRole={roleList}
                          hasRoleProfiency
                          handleChange={e => handleChange(e)}
                          handleChangeRating={(event, value) => {
                            setFieldValue(
                              `opportunity_roles.profile_role_level_id`,
                              value,
                            );
                          }}
                          helperText={t('help_icon.role')}
                        />
                        <Grid container alignItems="center">
                          <Grid item xs={6}>
                            <Box mt={1} flex={1}>
                              <Grid container item spacing={1} direction="row">
                                <Grid xs={5} item container justify="flex-end">
                                  <Box component="span" mt={1} mr={'10px'}>
                                    <MixTitle
                                      title={t('duration')}
                                      isHelper
                                      helperText={t('help_icon.duration')}
                                    />
                                  </Box>
                                </Grid>
                                <Grid item xs={7}>
                                  <FormControl
                                    fullWidth
                                    className={classes.numberInput}
                                    variant="outlined"
                                    margin="normal"
                                  >
                                    <TextField
                                      name={`opportunity_roles.day`}
                                      value={values.opportunity_roles.day}
                                      fullWidth
                                      variant="outlined"
                                      onChange={event => {
                                        setFieldValue(
                                          `opportunity_roles.day`,
                                          event.target.value.replace(
                                            /[^0-9]/gm,
                                            '',
                                          ),
                                        );
                                      }}
                                      type="text"
                                    />
                                  </FormControl>
                                </Grid>
                              </Grid>
                            </Box>
                          </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                          <Grid item xs={6}>
                            <Box mt={1} flex={1}>
                              <Grid container item spacing={1} direction="row">
                                <Grid xs={5} item container justify="flex-end">
                                  <Box component="span" mt={1} mr={'10px'}>
                                    <MixTitle
                                      title={t('effort')}
                                      isHelper
                                      helperText={t('help_icon.effort')}
                                    />
                                  </Box>
                                </Grid>
                                <Grid item xs={7}>
                                  <FormControl
                                    fullWidth
                                    className={classes.numberInput}
                                    variant="outlined"
                                    margin="normal"
                                  >
                                    <TextField
                                      name={`opportunity_roles.effort`}
                                      value={values.opportunity_roles.effort}
                                      fullWidth
                                      variant="outlined"
                                      onChange={event => {
                                        setFieldValue(
                                          `opportunity_roles.effort`,
                                          event.target.value.replace(
                                            /[^0-9]/gm,
                                            '',
                                          ),
                                        );
                                      }}
                                      type="text"
                                    />
                                  </FormControl>
                                </Grid>
                              </Grid>
                            </Box>
                          </Grid>
                          <Grid item xs={6}>
                            <Box display="flex" pl={3}>
                              <Typography
                                variant="caption"
                                style={{ color: 'red' }}
                              >
                                {Number(values.opportunity_roles.day) <
                                  Number(values.opportunity_roles.effort) &&
                                  t('effort_should_less_than_or_qual_duration')}
                              </Typography>
                            </Box>
                          </Grid>
                        </Grid>

                        <SelectInRole
                          name="Location"
                          field={`opportunity_roles.location.id`}
                          item={locationSelected}
                          locations={locationList}
                          handleChange={e => handleChange(e)}
                          helperText={t('help_icon.location')}
                        />
                        <SelectInRole
                          name="Resource Team"
                          field={`opportunity_roles.resource_team.id`}
                          item={teamSelected}
                          resourceTeam={teamList}
                          handleChange={e => handleChange(e)}
                          helperText={t('help_icon.resource_team')}
                        />
                        <FormControl fullWidth className={classes.numberInput}>
                          <FieldArray
                            name={`opportunity_roles.skills`}
                            render={skillHelper => (
                              <Box>
                                <Box>
                                  {values.opportunity_roles.skills.map(
                                    (skill, indexSkill) => (
                                      <SelectInSkill
                                        key={'Skill - ' + indexSkill}
                                        name={`Skill ${indexSkill + 1}`}
                                        field={`opportunity_roles.skills[${indexSkill}].skill_id`}
                                        item={skill}
                                        handleChange={e => handleChange(e)}
                                        handleChangeRatingInSkill={(
                                          event,
                                          value,
                                        ) => {
                                          setFieldValue(
                                            `opportunity_roles.skills[${indexSkill}].proficiency_level_id`,
                                            value,
                                          );
                                        }}
                                        index={indexSkill}
                                        skillList={skillList}
                                        removable
                                        handleRemove={() =>
                                          skillHelper.remove(indexSkill)
                                        }
                                      />
                                    ),
                                  )}
                                </Box>
                                <Grid container>
                                  <Grid item xs={6}>
                                    <Grid container>
                                      <Grid item xs={5} />
                                      <Grid item xs={7}>
                                        <Box
                                          justifyContent="center"
                                          display="flex"
                                          mt={1}
                                          mb={1}
                                        >
                                          <Button
                                            variant="outlined"
                                            onClick={() => {
                                              skillHelper.push({
                                                id:
                                                  opportunity?.opportunity_roles
                                                    ?.length,
                                                proficiency_level_id: 1,
                                                skill_id: 1,
                                                skill_name: 'Problem Solving-1',
                                              });
                                            }}
                                          >
                                            Add
                                          </Button>
                                        </Box>
                                      </Grid>
                                    </Grid>
                                  </Grid>
                                </Grid>
                              </Box>
                            )}
                          />
                        </FormControl>
                      </Grid>

                      <Box display="flex" justifyContent="flex-end">
                        <Box mr={2}>
                          <Button
                            onClick={() => setStatusAddTeamRoleDialog()}
                            variant="outlined"
                          >
                            Cancel
                          </Button>
                        </Box>
                        {Number(values.opportunity_roles.day) >=
                        Number(values.opportunity_roles.effort) ? (
                          <Button
                            variant="contained"
                            color="primary"
                            className={classes.subMenuInRole}
                            onClick={() => handleSubmit()}
                          >
                            Add team role
                          </Button>
                        ) : (
                          <Button
                            variant="contained"
                            disabled
                            className={classes.subMenuInRole}
                          >
                            Add team role
                          </Button>
                        )}
                      </Box>
                    </Box>
                  </Grid>
                );
              }}
            </FieldArray>
          )}
        </Formik>
      </DialogContent>
    </Dialog>
  );
};
