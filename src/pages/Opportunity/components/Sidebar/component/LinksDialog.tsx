import React from 'react';
import {
  Grid,
  Table,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
  Link,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { StyledReactiveList } from '../../../../../components/styled';
import { useTableHeadSorter } from '../../../../../hooks';
import {
  ESLoader,
  HeadCellProps,
  TableHeadSorter,
} from '../../../../../components';
import moment from 'moment';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '24px 24px 0px 24px',
  },
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
  pointer: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));

const tableFields = [
  'title.keyword',
  'opportunity_playbook.name.keyword',
  'opportunity_playbook_item.title.keyword',
  'last_updated_at',
  'item_link.name.keyword',
  'item_link.url.keyword',
] as const;

const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'title.keyword',
    label: 'List title',
  },
  {
    id: 'opportunity_playbook.name.keyword',
    label: 'Playbook name',
  },
  {
    id: 'opportunity_playbook_item.title.keyword',
    label: 'Step title',
  },
  {
    id: 'item_link.name.keyword',
    label: 'Link name',
  },

  {
    id: 'item_link.url.keyword',
    label: 'URL',
  },
  {
    id: 'last_updated_at',
    label: 'Last updated',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

interface ILinksDialogProps {
  openModal: boolean;
  setOpen?: React.Dispatch<React.SetStateAction<boolean>>;
  customQueryES?: () => void;
  toggleModal?: () => void;
}

export const LinksDialog: React.FC<ILinksDialogProps> = ({
  openModal,
  setOpen,
  customQueryES,
  toggleModal,
}) => {
  const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'last_updated_at',
  );
  const defaultQuery = () => {
    return {
      query: {
        match: { 'opportunity.id': opportunity?.id },
      },
    };
  };

  return (
    <>
      <Dialog
        open={openModal}
        onClose={!!toggleModal ? toggleModal : () => setOpen?.(false)}
        maxWidth="lg"
        aria-labelledby="scroll-dialog-title"
        className={classes.dialog}
      >
        <Box
          pt={2}
          className={classes.titleDialog}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Typography variant="h2">Links</Typography>
        </Box>
        <DialogContent className={classes.borderNone}>
          <Grid container>
            <Grid item xs={12}>
              <ReactiveBase
                app={ES_INDICES.link_index_name}
                url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                headers={{
                  Authorization: window.localStorage.getItem('jwt'),
                }}
              >
                <StyledReactiveList
                  componentId="SearchLinks"
                  dataField={orderBy}
                  sortBy={order}
                  renderResultStats={() => null}
                  renderNoResults={() => <></>}
                  loader={<ESLoader />}
                  size={5}
                  paginationAt="bottom"
                  pages={5}
                  pagination={true}
                  defaultQuery={customQueryES ? customQueryES : defaultQuery}
                  render={({ data, loading }) => {
                    if (loading) return <></>;
                    if (!data?.length)
                      return (
                        <Box mt={3}>
                          <Typography>
                            {customQueryES
                              ? t('link.no_link_found_in_playground')
                              : t('opp_has_not_links')}
                          </Typography>
                        </Box>
                      );
                    return (
                      <>
                        {data?.length === 0 ? (
                          <Box mt={2}>
                            <Typography>
                              {customQueryES
                                ? t('link.no_link_found_in_playground')
                                : t('opp_has_not_links')}
                            </Typography>
                          </Box>
                        ) : (
                          <TableContainer
                            className={classes.rootTableContainer}
                          >
                            <Table className={classes.rootTable}>
                              <TableHeadSorter
                                onRequestSort={handleRequestSort}
                                order={order}
                                orderBy={orderBy}
                                cells={headCells}
                              />
                              <TableBody>
                                {data?.map((linkParent: any) => {
                                  return linkParent.item_link?.map(
                                    (item: any) => {
                                      return (
                                        <TableRow key={item.id}>
                                          <TableCell component="th" scope="row">
                                            {linkParent.title}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {
                                              linkParent.opportunity_playbook
                                                .name
                                            }
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            <Link
                                              component={RouterLink}
                                              color="inherit"
                                              to={`/opportunity/${linkParent.opportunity.id}/playbook-${linkParent.opportunity_playbook.id}?step=${linkParent.opportunity_playbook_item.id}`}
                                            >
                                              {
                                                linkParent
                                                  .opportunity_playbook_item
                                                  .title
                                              }
                                            </Link>
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {item?.name}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            <Link
                                              color="inherit"
                                              href={item?.url}
                                              target="_blank"
                                            >
                                              {item?.url}
                                            </Link>
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {moment(
                                              linkParent.last_updated_at,
                                            ).format('DD/MM/YYYY')}
                                          </TableCell>
                                        </TableRow>
                                      );
                                    },
                                  );
                                })}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        )}
                      </>
                    );
                  }}
                  innerClass={{
                    button: classes.buttonPagination,
                    resultsInfo: classes.resultsInfo,
                    sortOptions: classes.sortSelect,
                  }}
                />
              </ReactiveBase>
            </Grid>
          </Grid>
          <Box pb={2} pt={2} display="flex" justifyContent="flex-end">
            <Button
              onClick={!!toggleModal ? toggleModal : () => setOpen?.(false)}
              color="primary"
              variant="contained"
            >
              OK
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};
