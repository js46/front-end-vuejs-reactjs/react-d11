import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { IOpportunity } from '../../../../../services/opportunity.services';
import { NoteResponseT } from '../../../../../services/playbook.services';
import { NoteDialog } from '../../../../../components/NoteDialog';
import {
  deleteNoteOfUser,
  addOpportunityNote,
  updateOpportunityNote,
} from '../../../../../services/notes.services';
import { DeleteDialog, TableAction } from '../../../../../components';
import { useDisableMultipleClick, useUnmounted } from '../../../../../hooks';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { UserProfileState } from '../../../../../store/user/selector';
import { OpportunityState } from '../../../../../store/opportunity/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '16px 24px 0px 16px',
  },
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
}));

interface Data {
  note_id: number;
  name: string;
  created_by: string;
  created_at: string;
  action: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'note_id',
    label: 'Id',
  },
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'created_by',
    label: 'Created by',
  },
  {
    id: 'created_at',
    label: 'Created at',
  },
  {
    id: 'action',
    label: 'Action',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell key={headCell.id} align="center">
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface NotesDialogProps {
  openModal: boolean;
  toggleModal: () => void;
}
export const NotesDialog: React.FC<NotesDialogProps> = ({
  openModal,
  toggleModal,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  const [userNotes, setUserNotes] = useState<NoteResponseT[]>();
  const [currentTime, setCurrentTime] = useState(1);
  const [isOpenAddNote, setOpenAddNote] = useState(false);

  const customQuery = () => {
    return {
      query: {
        bool: {
          filter: [
            {
              term: {
                delete_flg: false,
              },
            },
            {
              query_string: {
                query: 'opportunities.id:(' + opportunity?.id + ')',
              },
            },
          ],
          should: [
            {
              term: {
                dummy: currentTime,
              },
            },
          ],
        },
      },
    };
  };

  const handleClickNote = () => {
    setOpenAddNote(true);
  };

  return (
    <Dialog
      open={openModal}
      onClose={toggleModal}
      maxWidth="lg"
      aria-labelledby="scroll-dialog-title"
      className={classes.dialog}
    >
      <Box
        pt={2}
        className={classes.titleDialog}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography variant="h2">{t('notes')}</Typography>
        <Button variant="contained" color="primary" onClick={handleClickNote}>
          {t('add')}
        </Button>
      </Box>
      <DialogContent className={classes.borderNone}>
        <NoteTable
          opportunity={opportunity}
          userNotes={userNotes}
          setUserNotes={setUserNotes}
          currentTime={currentTime}
          setCurrentTime={setCurrentTime}
          isOpenAddNote={isOpenAddNote}
          setOpenAddNote={setOpenAddNote}
          defaultQuery={customQuery}
        />
        <Box pb={2} pt={2} display="flex" justifyContent="center">
          <Button onClick={toggleModal} color="primary" variant="contained">
            {t('ok')}
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

interface NoteTableProps {
  opportunity?: IOpportunity;
  userNotes?: NoteResponseT[];
  setUserNotes: React.Dispatch<
    React.SetStateAction<NoteResponseT[] | undefined>
  >;
  currentTime: number;
  setCurrentTime: React.Dispatch<React.SetStateAction<number>>;
  isOpenAddNote: boolean;
  setOpenAddNote: React.Dispatch<React.SetStateAction<boolean>>;
  defaultQuery: (...args: any[]) => any;
}

const NoteTable: React.FC<NoteTableProps> = ({
  opportunity,
  userNotes,
  setUserNotes,
  currentTime,
  setCurrentTime,
  isOpenAddNote,
  setOpenAddNote,
  defaultQuery,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const { t } = useTranslation();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const userProfileState = useSelector(UserProfileState);
  const emptyNoteContent = {
    name: '',
    author: userProfileState.full_name,
    content: '',
    date: new Date(),
  };
  const initNoteContent = {
    name: '',
    author: userProfileState.full_name,
    content: '',
    date: new Date(),
  };

  const [noteContent, setNoteContent] = useState(initNoteContent);
  const [currentEditNoteId, setCurrentEditNoteId] = useState<number>();
  const [noteToDeleteId, setNoteToDeleteId] = useState<number>();
  const isUnmounted = useUnmounted();

  const handleChangeNoteContent = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    event.persist();
    switch (event.target.name) {
      case 'note_name': {
        setNoteContent({ ...noteContent, name: event.target.value });
        break;
      }
      case 'note_detail_content': {
        setNoteContent({ ...noteContent, content: event.target.value });
        break;
      }
    }
  };

  const handleRefetchNote = () => {
    setTimeout(() => {
      if (isUnmounted.current) return;
      setCurrentTime(currentTime + 1);
    }, 1000);
  };

  const handleAddNote = async (content: string) => {
    if (isSubmitting || !opportunity?.id) return;
    await debounceFn();
    const payload = {
      title: noteContent.name,
      content: content,
      opportunity_id: opportunity?.id,
    };
    const response = await addOpportunityNote(payload, userProfileState.id);
    if (response.success) {
      setOpenAddNote(false);
      handleRefetchNote();
    }
    endRequest();
  };

  const handleEditNote = async (content: any) => {
    if (!currentEditNoteId || isSubmitting || !opportunity?.id) return;
    await debounceFn();
    const payload = {
      title: noteContent.name,
      content: content,
      opportunity_id: opportunity?.id,
      // created_by_user_id: userProfileState.id,
    };
    const response = await updateOpportunityNote(payload, currentEditNoteId);
    if (response.success) {
      setNoteContent(emptyNoteContent);
      setCurrentEditNoteId(undefined);
      handleRefetchNote();
    }
    endRequest();
  };

  const handleRemoveNote = async (idNote: number | undefined) => {
    if (isSubmitting || !idNote) return;
    await debounceFn();
    const response = await deleteNoteOfUser(idNote);
    if (response.success) {
      handleRefetchNote();
      setNoteToDeleteId(undefined);
    }
    endRequest();
  };

  return (
    <>
      <ReactiveBase
        app={ES_INDICES.user_note_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{
          Authorization: window.localStorage.getItem('jwt'),
        }}
      >
        <ReactiveList
          componentId="Notes"
          dataField="notes"
          renderResultStats={() => null}
          renderNoResults={() => <></>}
          defaultQuery={defaultQuery}
          loader={<></>}
          onData={({ data }) => {
            setUserNotes(data);
          }}
          render={({ data }) => {
            return <></>;
          }}
        />
      </ReactiveBase>
      <Box mt={3}>
        {userNotes && userNotes.length > 0 && (
          <TableContainer>
            <Table>
              <EnhancedTableHead classes={classes} />
              <TableBody>
                {userNotes.map((note, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.id}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.title}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.created_by?.name}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {note?.time &&
                            moment(note?.time).format('DD/MM/YYYY hh:mma')}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        <TableAction
                          hiddenItem={['view']}
                          onClickEdit={() => {
                            setCurrentEditNoteId(note.id);
                            setNoteContent({
                              ...noteContent,
                              name: note?.title,
                              content: note?.content,
                            });
                          }}
                          onClickDelete={() => {
                            setNoteToDeleteId(note.id);
                          }}
                          disabledDelete={
                            note.created_by.id !== userProfileState.id
                              ? true
                              : false
                          }
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        )}
        {(userNotes == null || userNotes.length === 0) && (
          <Typography component="p" variant="body1">
            {t('opp_has_not_any_note_yet')}
          </Typography>
        )}
      </Box>
      <DeleteDialog
        isOpen={!!noteToDeleteId}
        header="Delete Note"
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_this_note')}
            </Typography>
          </Box>
        }
        handleClose={() => setNoteToDeleteId(undefined)}
        handleDelete={() => handleRemoveNote(noteToDeleteId)}
        disabled={isSubmitting}
      />
      <NoteDialog
        type="add"
        isOpen={isOpenAddNote}
        noteContent={noteContent}
        handleClose={() => {
          setNoteContent(emptyNoteContent);
          setOpenAddNote(false);
        }}
        handleChange={handleChangeNoteContent}
        handleSubmit={handleAddNote}
        disabled={isSubmitting}
      />
      <NoteDialog
        type="edit"
        isOpen={!!currentEditNoteId}
        noteContent={noteContent}
        handleClose={() => {
          setNoteContent(emptyNoteContent);
          setCurrentEditNoteId(undefined);
        }}
        handleChange={handleChangeNoteContent}
        handleSubmit={handleEditNote}
        disabled={isSubmitting}
      />
    </>
  );
};

export default NotesDialog;
