import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  Box,
  Grid,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { updateOpportunityCurrentExpert } from '../../../../../services/opportunity.services';

import { useDisableMultipleClick } from '../../../../../hooks';
import { IProfileData } from '../../../../../services/profile.services';
import { MixTitle } from '../../../../../components';
import { addTaskComment } from '../../../../../services/comment.services';
import { Message } from '../../../../../constants';
import { ConfigESIndex } from '../../../../../store/config/selector';
import {
  notifyError,
  notifySuccess,
} from '../../../../../store/common/actions';
import { OpportunityState } from '../../../../../store/opportunity/selector';

const useStyle = makeStyles(theme => ({
  autocomplete: {
    '& input': {
      padding: '0!important',
    },
  },
}));

interface UpdateCurrentExpertReviewerProps {
  expertReviewerDialogStatus: boolean;
  handleClose: () => void;
  handleUpdateOpportunity: () => void;
}

export const UpdateCurrentExpertReviewer: React.FC<UpdateCurrentExpertReviewerProps> = ({
  expertReviewerDialogStatus,
  handleClose,
  handleUpdateOpportunity,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyle();
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [newExpertReviewer, setNewExpertReviewer] = useState<number>();
  const [teamExperts, setTeamExperts] = useState<IProfileData[]>([]);
  const [comment, setComment] = useState('');
  const dispatch = useDispatch();

  const handleChangeComment = (event: React.ChangeEvent<HTMLInputElement>) => {
    setComment(event.target.value);
  };

  const handleAddComment = (
    entityType: string,
    commentType: string,
    commentValue: string,
  ) => {
    if (commentValue !== '') {
      const bodyRequest = {
        entity_id: opportunity?.id,
        entity_type: entityType,
        comment_type: commentType,
        comment_body: commentValue,
        comment_parent_id: 0,
      };
      addTaskComment(bodyRequest);
    }
  };

  const handleUpdateNewReviewer = async () => {
    if (!opportunity?.id || !newExpertReviewer || isSubmitting) return;
    debounceFn();
    handleAddComment('opportunity', 'UPDATE_EXPERT_REVIEWER', comment);
    const response = await updateOpportunityCurrentExpert(opportunity?.id, {
      expert: { user_id: newExpertReviewer },
    });
    if (response.success) {
      dispatch(
        notifySuccess({
          message: `Update ${response.data?.current_expert_review?.expert_review?.name} to be current Reviewer`,
        }),
      );
      handleUpdateOpportunity();
      handleClose();
    } else {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
    endRequest();
  };

  const handleChangeExpert = (event: object, value: IProfileData | null) => {
    setNewExpertReviewer(value?.id as number);
  };

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'resource_team.resource_team_id':
                  opportunity?.resource_team?.id,
              },
            },
            {
              terms: {
                casbin_role: ['role:expert', 'role:teamlead'],
              },
            },
          ],
        },
      },
    };
  };
  return (
    <Dialog open={expertReviewerDialogStatus} onClose={handleClose}>
      <ReactiveBase
        app={ES_INDICES.user_profile_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <ReactiveList
          componentId="SearchExpert"
          dataField="search_expert"
          loader={<></>}
          showResultStats={false}
          renderNoResults={() => <></>}
          defaultQuery={customQuery}
          onData={({ data }) => {
            setTeamExperts(data);
          }}
          render={() => <></>}
        />
      </ReactiveBase>
      <Box p={2} width="500px">
        <Box display="flex" justifyContent="center" mb={4}>
          <Typography variant="h4" component="p">
            {t('update_expert_reviewer')}
          </Typography>
        </Box>
        <DialogContent>
          <Grid container>
            <Grid item xs={12}>
              <Box>
                <Box mb={2}>
                  <Typography component="p" variant="body1">
                    {t('choose_an_expert')}
                  </Typography>
                </Box>

                <Autocomplete
                  id="update-expert-review"
                  options={teamExperts.filter(
                    item =>
                      item.id !==
                      opportunity?.current_expert_review?.expert_review?.id,
                  )}
                  getOptionLabel={option => option.name}
                  className={classes.autocomplete}
                  renderInput={params => (
                    <TextField {...params} fullWidth variant="outlined" />
                  )}
                  onChange={handleChangeExpert}
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <MixTitle title="Comment" moreText="Max 300 words" />
                <TextField
                  type="text"
                  id="comment"
                  name="comment"
                  variant="outlined"
                  fullWidth
                  multiline
                  rows={8}
                  value={comment}
                  onChange={handleChangeComment}
                  inputProps={{ maxLength: 300 }}
                />
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box mt={3} mb={2} justifyContent="flex-end" display="flex">
                <Box mr={2}>
                  <Button onClick={handleClose} variant="outlined">
                    Cancel
                  </Button>
                </Box>
                <Box>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleUpdateNewReviewer}
                    disabled={!newExpertReviewer || isSubmitting}
                  >
                    Save
                  </Button>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </DialogContent>
      </Box>
    </Dialog>
  );
};
