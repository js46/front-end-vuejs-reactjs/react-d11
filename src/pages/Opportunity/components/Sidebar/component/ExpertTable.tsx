import React, { useState } from 'react';
import {
  ESLoader,
  ESNoResult,
  HeadCellProps,
  TableHeadSorter,
} from '../../../../../components';
import { Table, TableBody, TableContainer } from '@material-ui/core';
import { ExpertCapacityChart } from './ExpertCapacityChart';
import { StyledReactiveList } from '../../../../../components/styled';
import { makeStyles } from '@material-ui/core/styles';
import { IProfileData } from '../../../../../services/profile.services';
import { useTableHeadSorter } from '../../../../../hooks';
import { OpportunityCountByExpert } from '../../../../../components/opportunity/OpportunityCountByExpert';

type ExpertTableProps = {
  defaultQuery?: () => object;
  holidays: Map<string, string>;
  currentExperts?: number[];
  userFocus: any;
  setUserFocus: any;
  handleSelectedUser(user: IProfileData): void;
  onDurationChange?: (from?: Date, to?: Date) => void;
  getExpertDefaultRange: (id: number) => number | undefined;
};
const tableFields = [
  'name.keyword',
  'profile_role.profile_role_name.keyword',
  'location.name.keyword',
  'resource_team.resource_team_name.keyword',
  'assigned_opportunity_count',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'name.keyword',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'profile_role.profile_role_name.keyword',
    label: 'Role',
    disablePadding: true,
  },
  {
    id: 'location.name.keyword',
    label: 'Location',
    disablePadding: true,
  },
  {
    id: 'resource_team.resource_team_name.keyword',
    label: 'Resource team',
    disablePadding: true,
  },
  {
    id: 'assigned_opportunity_count',
    label: 'Current opportunity #',
    disablePadding: true,
  },
];
export const ExpertTable: React.FC<ExpertTableProps> = ({
  defaultQuery,
  holidays,
  currentExperts,
  userFocus,
  setUserFocus,
  handleSelectedUser,
  onDurationChange,
  getExpertDefaultRange,
}) => {
  const classes = useStyles();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'assigned_opportunity_count',
  );
  const [opportunityCount, setOpportunityCount] = useState<
    Map<number, number>
  >();

  return (
    <>
      <OpportunityCountByExpert
        setOpportunityCount={setOpportunityCount}
        opportunityCount={opportunityCount}
      />
      <StyledReactiveList
        componentId="SearchResult"
        react={{
          and: [
            'Keyword',
            'Role',
            'Skill list',
            'Location list',
            'Role list',
            'Resource team list',
          ],
        }}
        dataField={orderBy}
        sortBy={order}
        renderResultStats={() => null}
        size={10}
        paginationAt="bottom"
        pages={5}
        pagination
        showResultStats={false}
        showLoader
        loader={<ESLoader />}
        renderNoResults={() => <ESNoResult msgKey={'no_expert_match'} />}
        render={({ data, loading }) => {
          if (!data?.length || loading) return null;
          return (
            <TableContainer className={classes.rootTableContainer}>
              <Table className={classes.rootTable}>
                <TableHeadSorter
                  onRequestSort={handleRequestSort}
                  order={order}
                  orderBy={orderBy}
                  cells={headCells}
                />
                <TableBody>
                  {data.map((item: IProfileData, index: number) => {
                    return (
                      <ExpertCapacityChart
                        key={`${item.id}-${index}`}
                        id={item.id}
                        item={item}
                        opportunityCount={opportunityCount?.get(item.id)}
                        currentExperts={currentExperts}
                        handleSelectedUser={handleSelectedUser}
                        userFocus={userFocus}
                        setUserFocus={setUserFocus}
                        onDurationChange={onDurationChange}
                        defaultRange={getExpertDefaultRange(item.id)}
                        holidays={holidays}
                      />
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          );
        }}
        innerClass={{
          button: classes.buttonPagination,
          resultsInfo: classes.resultsInfo,
          sortOptions: classes.sortSelect,
        }}
        defaultQuery={defaultQuery}
      />
    </>
  );
};
const useStyles = makeStyles(theme => ({
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
}));
