import React, { useState } from 'react';
import { CustomNumberInput, MixTitle } from '../../../../../components';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  Button,
  Typography,
  Box,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import {
  IOpportunityInRole,
  updateOpportunityRoles,
} from '../../../../../services/opportunity.services';
import { IProfileData } from '../../../../../services/profile.services';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useDisableMultipleClick } from '../../../../../hooks';
import { delay } from '../../../../../utils';
import { useTranslation } from 'react-i18next';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(() => ({
  description: {
    fontSize: 14,
  },
}));

interface AssignExpertDialogProps {
  teamRoleSelected?: IOpportunityInRole;
  selectedUser?: IProfileData;
  handleAssignExpert: (
    emailId: number | undefined,
    teamRoleSelected?: IOpportunityInRole,
    assignmentDate?: Date,
    effort?: number,
    comment?: string,
    duration?: number,
  ) => Promise<void>;
  setStatusAssignExpertDialog: React.Dispatch<React.SetStateAction<boolean>>;
  defaultFilterValue: {
    duration?: number;
    effort?: number;
    assignmentDate?: Date | null;
  };
}

export const AssignExpertDialog: React.FC<AssignExpertDialogProps> = ({
  teamRoleSelected,
  selectedUser,
  handleAssignExpert,
  setStatusAssignExpertDialog,
  defaultFilterValue,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [duration, setDuration] = useState<number>(
    defaultFilterValue.duration ?? 1,
  );
  const opportunity = useSelector(OpportunityState);
  const [effort, setEffort] = useState<number>(defaultFilterValue.effort ?? 1);
  const [assignmentDate, setAssignmentDate] = useState<Date | null>(
    defaultFilterValue.assignmentDate
      ? new Date(defaultFilterValue.assignmentDate)
      : new Date(),
  );
  const [comment, setComment] = useState<string>('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    switch (event.target.name) {
      case 'comment':
        return setComment(event.target.value);
      case 'duration':
        return setDuration(parseInt(event.target.value));
      case 'effort':
        return setEffort(parseInt(event.target.value));
    }
  };

  const onAddTeamRole = async () => {
    if (isSubmitting) return;
    if (selectedUser && opportunity) {
      await debounceFn();
      if (teamRoleSelected && assignmentDate !== null) {
        await updateOpportunityRoles(opportunity.id, {
          id: teamRoleSelected.id,
          effort: teamRoleSelected.effort,
          role_name: teamRoleSelected.role_name,
          profile_role_id: teamRoleSelected.profile_role?.id,
          profile_role_level_id: teamRoleSelected.profile_role_level_id,
          location_id: teamRoleSelected.location?.id,
          resource_team_id: teamRoleSelected.resource_team?.id,
          day: duration,
          opportunity_role_skills: teamRoleSelected.skills || [],
        }).then(async ({ data }) => {
          await delay(200);
          await handleAssignExpert(
            selectedUser?.id,
            teamRoleSelected,
            assignmentDate,
            effort,
            comment,
          );
        });
      } else {
        if (assignmentDate) {
          await handleAssignExpert(
            selectedUser?.id,
            undefined,
            assignmentDate,
            effort,
            comment,
            duration,
          );
        }
      }
      endRequest();
    }
  };
  return (
    <Box mt={2} mb={2} ml={5} mr={5}>
      <Grid container>
        <Grid item xs={12} container justify="center">
          <Box mt={2} mb={4}>
            <Typography variant="subtitle1" component="p">
              {t('assign_an_expert_to_team')}
            </Typography>
          </Box>
        </Grid>
        {!teamRoleSelected && (
          <>
            <span className={classes.description}>
              {t('note_in_assign_expert')}
            </span>
          </>
        )}
        <Grid item xs={12}>
          <Box mt={2}>
            <MixTitle title="Team Role Name" />
            <TextField
              id="opportunity_roles.role_name"
              name="opportunity_roles.role_name"
              variant="outlined"
              fullWidth
              disabled
              value={selectedUser?.profile_role.profile_role_name ?? ''}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box mt={2}>
            <MixTitle title="Duration" />
            <TextField
              type="text"
              id="duration"
              name="duration"
              variant="outlined"
              fullWidth
              value={duration}
              onChange={handleChange}
              InputProps={{
                inputComponent: CustomNumberInput,
              }}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box mt={2}>
            <MixTitle title="Effort" />
            <TextField
              type="text"
              id="effort"
              name="effort"
              variant="outlined"
              fullWidth
              value={effort}
              onChange={handleChange}
              InputProps={{
                inputComponent: CustomNumberInput,
              }}
              error={effort > duration}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box mt={2}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <MixTitle title="Assignment Date" />
              <DatePicker
                disableToolbar
                allowKeyboardControl
                inputVariant="outlined"
                variant="inline"
                margin="normal"
                name="assignment_date"
                id="assignment_date"
                format="d MMMM yyyy"
                autoOk={true}
                onChange={setAssignmentDate}
                value={assignmentDate ? assignmentDate : ''}
                animateYearScrolling={false}
                fullWidth
                disabled
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <ExpandMoreIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </MuiPickersUtilsProvider>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box mt={2}>
            <MixTitle title="Email" />
            <TextField
              type="email"
              id="email"
              name="email"
              variant="outlined"
              fullWidth
              value={selectedUser?.email ?? ''}
              inputProps={{
                readOnly: true,
              }}
              disabled={true}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box mt={2}>
            <MixTitle title="Comment" moreText="Max 300 words" />
            <TextField
              type="text"
              id="comment"
              name="comment"
              variant="outlined"
              fullWidth
              multiline
              rows={6}
              value={comment}
              onChange={handleChange}
              inputProps={{ maxLength: 300 }}
            />
          </Box>
        </Grid>
        <Grid item xs={12} container justify="flex-end">
          <Box mt={2} display="flex" flexDirection="row">
            <Box mr={2}>
              <Button
                onClick={() => {
                  setStatusAssignExpertDialog(false);
                }}
                variant="outlined"
              >
                Cancel
              </Button>
            </Box>
            <Button
              variant="contained"
              color="primary"
              onClick={onAddTeamRole}
              disabled={isSubmitting || effort > duration}
            >
              Assign Expert
            </Button>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
