import React, { useState } from 'react';
import {
  Grid,
  Table,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Button,
  Dialog,
  DialogContent,
  Typography,
  Box,
  Link,
} from '@material-ui/core';
import { IChildListTree } from '../../../../Playbook/types';
import { makeStyles } from '@material-ui/core/styles';
import { ESLoader, TableHeadSorter, MixTitle } from '../../../../../components';
import { ConfigESIndex } from '../../../../../store/config/selector';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { useTableHeadSorter } from '../../../../../hooks';
import { IChildrenListItem } from '../../../../../services/playbook.services';
import { ListItemTemplate } from './ListItemTemplate';
import { useSelector } from 'react-redux';
import { StyledReactiveList } from '../../../../../components/styled';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { Link as RouterLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  titleDialog: {
    color: theme.palette.text.primary,
    padding: '24px 24px 0px 24px',
  },
  dialog: {
    padding: '15px 0px',
  },
  borderNone: {
    border: 'none',
    minWidth: 732,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));

interface Data {
  title: string;
  step: string;
  last_updated_at: string;
  'name.keyword'?: string;
  playbook_name: string;
  type: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'name.keyword',
    label: 'Title',
  },
  {
    id: 'playbook_name',
    label: 'Playbook name',
  },
  {
    id: 'step',
    label: 'Step title',
  },
  {
    id: 'type',
    label: 'Type',
  },
  {
    id: 'last_updated_at',
    label: 'Last updated',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}
const tableFields = [
  'name.keyword',
  'opportunity_playbook.name.keyword',
  'opportunity_playbook_item.title.keyword',
  'last_updated_at',
] as const;

interface IListsDialogProps {
  openModal: boolean;
  toggleModal?: () => void;
  customQueryES?: () => void;
  setOpen?: React.Dispatch<React.SetStateAction<boolean>>;
}

export const ListsDialog: React.FC<IListsDialogProps> = ({
  openModal,
  customQueryES,
  toggleModal,
  setOpen,
}) => {
  const classes = useStyles();
  const opportunity = useSelector(OpportunityState);
  const ES_INDICES = useSelector(ConfigESIndex);
  const { i18n } = useTranslation();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'last_updated_at',
  );
  const [showListDialog, setShowListDialog] = useState<boolean>(false);
  const [childListTree, setChildListTree] = useState<IChildListTree[]>([]);
  const defaultQuery = () => {
    return {
      query: {
        bool: {
          must: [
            { match: { 'opportunity.id': opportunity?.id } },
            { match: { del_flg: false } },
          ],
        },
      },
    };
  };
  function convertItemTree(items: IChildrenListItem[], parentId: number) {
    let itemFilter = items?.filter(child => child.parent_id === parentId);
    if (!itemFilter) return [];
    let list: IChildListTree[] = [];

    for (let i = 0; i < itemFilter?.length; i++) {
      list.push({
        ...itemFilter[i],
        children: convertItemTree(items, itemFilter[i]?.id),
      });
    }

    return list;
  }
  return (
    <>
      <Dialog
        open={openModal}
        onClose={!!toggleModal ? toggleModal : () => setOpen?.(false)}
        maxWidth="lg"
        aria-labelledby="scroll-dialog-title"
        className={classes.dialog}
      >
        <Box
          pt={2}
          className={classes.titleDialog}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Typography variant="h2">Lists</Typography>
        </Box>
        <DialogContent className={classes.borderNone}>
          <Grid container>
            <Grid item xs={12}>
              <ReactiveBase
                app={ES_INDICES.list_index_name}
                url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                headers={{
                  Authorization: window.localStorage.getItem('jwt'),
                }}
              >
                <StyledReactiveList
                  componentId="SearchList"
                  dataField={orderBy}
                  sortBy={order}
                  renderResultStats={() => null}
                  size={5}
                  paginationAt="bottom"
                  pages={5}
                  pagination={true}
                  renderNoResults={() => <></>}
                  loader={<ESLoader />}
                  defaultQuery={customQueryES ? customQueryES : defaultQuery}
                  render={({ data, loading }) => {
                    if (loading) return null;
                    if (!data?.length)
                      return (
                        <Box mt={3}>
                          <Typography>
                            {customQueryES
                              ? i18n.t(
                                  'item_list.no_item_list_found_in_playground',
                                )
                              : i18n.t('opp_has_not_list_items')}
                          </Typography>
                        </Box>
                      );
                    return (
                      <>
                        {data.length === 0 ? (
                          <Box mt={2}>
                            <Typography>
                              {customQueryES
                                ? i18n.t(
                                    'item_list.no_item_list_found_in_playground',
                                  )
                                : i18n.t('opp_has_not_list_items')}
                            </Typography>
                          </Box>
                        ) : (
                          <TableContainer
                            className={classes.rootTableContainer}
                          >
                            <Table className={classes.rootTable}>
                              <TableHeadSorter
                                onRequestSort={handleRequestSort}
                                order={order}
                                orderBy={orderBy}
                                cells={headCells}
                              />
                              <TableBody>
                                {data.map((item: any) => {
                                  return (
                                    <TableRow key={item.id}>
                                      <TableCell
                                        component="th"
                                        scope="row"
                                        onClick={() => {
                                          setShowListDialog(true);
                                          setChildListTree(
                                            convertItemTree(item.item_list, 0),
                                          );
                                        }}
                                        style={{ cursor: 'pointer' }}
                                      >
                                        {item.title}
                                      </TableCell>
                                      <TableCell>
                                        {item.opportunity_playbook.name}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        <Link
                                          component={RouterLink}
                                          color="inherit"
                                          to={`/opportunity/${item.opportunity.id}/playbook-${item.opportunity_playbook.id}?step=${item.opportunity_playbook_item.id}`}
                                        >
                                          {
                                            item?.opportunity_playbook_item
                                              ?.title
                                          }
                                        </Link>
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {item.type.name ?? ''}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {moment(item.last_updated_at).format(
                                          'DD/MM/YYYY HH:mm:ss',
                                        )}
                                      </TableCell>
                                    </TableRow>
                                  );
                                })}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        )}
                      </>
                    );
                  }}
                  innerClass={{
                    button: classes.buttonPagination,
                    resultsInfo: classes.resultsInfo,
                    sortOptions: classes.sortSelect,
                  }}
                />
              </ReactiveBase>
            </Grid>
          </Grid>
          <Box pb={2} pt={2} display="flex" justifyContent="flex-end">
            <Button
              onClick={!!toggleModal ? toggleModal : () => setOpen?.(false)}
              color="primary"
              variant="contained"
            >
              OK
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
      <Dialog
        open={showListDialog}
        onClose={() => {
          setShowListDialog(false);
        }}
        fullWidth
        maxWidth="sm"
      >
        {childListTree?.length > 0 ? (
          <Box p={2}>
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              mt={1}
            >
              <MixTitle title={'List items'} />
            </Box>
            {childListTree.map((child, index) => {
              return (
                <Box key={child.id}>
                  <ListItemTemplate item={child} position={[index + 1]} />
                </Box>
              );
            })}
          </Box>
        ) : (
          <Box p={2}>
            <Typography>{i18n.t('opp_has_not_list_items')}</Typography>
          </Box>
        )}
        <Box pb={2} display="flex" justifyContent="center">
          <Button
            onClick={() => {
              setShowListDialog(false);
            }}
            color="primary"
            variant="contained"
          >
            OK
          </Button>
        </Box>
      </Dialog>
    </>
  );
};
