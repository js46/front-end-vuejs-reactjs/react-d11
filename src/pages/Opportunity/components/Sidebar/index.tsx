import React, { useEffect } from 'react';
import {
  Drawer,
  DrawerProps,
  Grid,
  List,
  ListItem,
  Toolbar,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  PeopleOutline,
  AttachFile,
  FormatListBulleted,
  ListAlt,
  Storage,
  FormatListNumbered,
  Link,
} from '@material-ui/icons';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import NoteOutlinedIcon from '@material-ui/icons/NoteOutlined';
import {
  ExpertSummaryDialog,
  ExpertAttachmentsDialog,
  ExpertDatasetsDialog,
  ExpertTasksDialog,
  ExpertTeamDialog,
  ExpertFindingsDialog,
  NotesDialog,
  ListsDialog,
  LinksDialog,
  IssuesDialog,
} from './component';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 256,
    flexShrink: 0,
  },
  paperAnchorDockedLeft: {
    borderRight: `1px solid ${theme.palette.grey[500]}`,
  },
  paper: {
    width: 256,
    backgroundColor: 'white',
    padding: '10px',
  },
  drawerPaper: {
    width: 256,
    backgroundColor: 'white',
  },
  upperButton: {
    padding: '5px 15px',
    fontWeight: 'bold',
    color: theme.palette.grey[800],
  },
  titleDialog: {
    color: theme.palette.text.primary,
  },
  dialog: {
    padding: '15px 0px',
  },
  item: {
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      background: theme.palette.grey[100],
    },
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
    // color: theme.palette.grey[700],
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
}));
export enum SidebarList {
  Summary = 'Summary',
  Team = 'Team',
  Tasks = 'Tasks',
  Notes = 'Notes',
  Attachments = 'Attachments',
  Datasets = 'Datasets',
  Findings = 'Findings',
  Lists = 'Lists',
  Links = 'Links',
  Issues = 'Issues',
}
const sidebarList = [
  {
    name: SidebarList.Summary,
    icon: <ListAlt />,
  },
  {
    name: SidebarList.Team,
    icon: <PeopleOutline />,
  },
  {
    name: SidebarList.Tasks,
    icon: <FormatListBulleted />,
  },
  {
    name: SidebarList.Notes,
    icon: <NoteOutlinedIcon />,
  },
  {
    name: SidebarList.Attachments,
    icon: <AttachFile />,
  },
  {
    name: SidebarList.Findings,
    icon: <SearchOutlinedIcon />,
  },
  {
    name: SidebarList.Datasets,
    icon: <Storage />,
  },
  {
    name: SidebarList.Lists,
    icon: <FormatListNumbered />,
  },
  {
    name: SidebarList.Links,
    icon: <Link />,
  },
  {
    name: SidebarList.Issues,
    icon: <ReportProblemOutlinedIcon />,
  },
];

type ISideBarProps = {
  openModal: boolean;
  toggleModal: () => void;
  currentType: any;
  setType: React.Dispatch<any>;
} & DrawerProps;

const OpportunitySidebar: React.FC<ISideBarProps> = ({
  openModal,
  toggleModal,
  currentType,
  setType,
  variant,
  ...rest
}) => {
  const classes = useStyles();
  // const [openModal, toggleModal] = useToggleValue(false);
  const modalRef = React.useRef<HTMLElement>(null);
  // const [currentType, setType] = useState<SidebarList | null>(null);

  useEffect(() => {
    if (openModal) {
      const { current: modalElement } = modalRef;
      if (modalElement !== null) modalElement.focus();
    }
  }, [openModal]);

  const onOpenModal = (type: SidebarList) => () => {
    setType(type);
    toggleModal();
  };

  const renderDetailPopup = (type: SidebarList | null) => {
    switch (type) {
      case SidebarList.Summary:
        return (
          <ExpertSummaryDialog
            openModal={openModal}
            toggleModal={toggleModal}
          />
        );
      case SidebarList.Team:
        return (
          <ExpertTeamDialog openModal={openModal} toggleModal={toggleModal} />
        );
      case SidebarList.Tasks:
        return (
          <ExpertTasksDialog openModal={openModal} toggleModal={toggleModal} />
        );
      case SidebarList.Notes:
        return <NotesDialog openModal={openModal} toggleModal={toggleModal} />;
      case SidebarList.Attachments:
        return (
          <ExpertAttachmentsDialog
            openModal={openModal}
            toggleModal={toggleModal}
          />
        );
      case SidebarList.Datasets:
        return (
          <ExpertDatasetsDialog
            openModal={openModal}
            toggleModal={toggleModal}
          />
        );
      case SidebarList.Findings:
        return (
          <ExpertFindingsDialog
            openModal={openModal}
            toggleModal={toggleModal}
          />
        );
      case SidebarList.Lists:
        return <ListsDialog openModal={openModal} toggleModal={toggleModal} />;
      case SidebarList.Links:
        return <LinksDialog openModal={openModal} toggleModal={toggleModal} />;
      case SidebarList.Issues:
        return <IssuesDialog openModal={openModal} toggleModal={toggleModal} />;
      default:
        return;
    }
  };

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{
        paper: classes.paper,
        paperAnchorDockedLeft: classes.paperAnchorDockedLeft,
      }}
      {...rest}
    >
      <Toolbar />
      <Grid container spacing={2}>
        <Grid item container justify="flex-start">
          <List
            component="nav"
            aria-label="opportunity sidebar"
            className={classes.drawerPaper}
          >
            {sidebarList.map((sideBar, index) => (
              <ListItem
                button
                className={classes.item}
                key={index}
                onClick={onOpenModal(sideBar.name)}
              >
                <div className={classes.icon}>{sideBar.icon}</div>
                {sideBar.name}
              </ListItem>
            ))}
          </List>
        </Grid>
      </Grid>
      {renderDetailPopup(currentType)}
    </Drawer>
  );
};

export default OpportunitySidebar;
