export const teamData = [
  {
    name: 'Joe.Bloggs.Proposer@nbn.com.au',
    role: 'Proposer',
    team: 'D&A',
    access_date: '23/3/2020',
    resource_status: 'N/A',
  },
  {
    name: 'Mary.Jane.TeamAdmin@nbn.com.au',
    role: 'Business Analyst',
    team: 'D&A',
    access_date: '3/5/2020',
    resource_status: 'Confirmed',
  },
];

export const attachments = [
  {
    description: 'Conduct Expert Review',
    file_name: 'Mary.Jane.Team.Admin',
    created_by: '27/3/2020',
    date: '22/3/2020',
    phase: 'Shape',
  },
  {
    description: 'Conduct Expert Review',
    file_name: 'Mary.Jane.Team.Admin',
    created_by: '27/3/2020',
    date: '22/3/2020',
    phase: 'Shape',
  },
];

export const datasets = [
  {
    name: 'Mary.Jane.Team.Admin',
    description: 'Conduct Expert Review',
    created_by: '27/3/2020',
    date: '22/3/2020',
    phase: 'Shape',
  },
  {
    name: 'Mary.Jane.Team.Admin',
    description: 'Conduct Expert Review',
    created_by: '27/3/2020',
    date: '22/3/2020',
    phase: 'Shape',
  },
];
