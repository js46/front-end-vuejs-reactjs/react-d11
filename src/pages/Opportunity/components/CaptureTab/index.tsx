import React, { useCallback, useState } from 'react';
import loadable from '@loadable/component';
import { ExpansionPanelDetails, Typography } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';

import {
  IOpportunity,
  IOpportunityParam,
  setOpportunityPrivate,
  updateOpportunity,
} from '../../../../services/opportunity.services';
import {
  getBenefitFocusValues,
  getBusinessUnits,
  IBenefitFocusValue,
} from '../../../../services/references.services';
import {
  IBusinessUnit,
  getCorporateObjectives,
  ICorporateObjective,
  getBusinessProcesses,
  IBusinessProcesses,
  getBenefitFocusCategories,
  IBenefitFocusCategory,
} from '../../../../services/references.services';
import {
  useEffectOnlyOnce,
  useOpportunityRoute,
  useUnmounted,
} from '../../../../hooks';
import {
  ExpansionPanelStyled,
  ExpansionPanelSummaryStyled,
} from '../../../../components/styled';
import { useTranslation } from 'react-i18next';
import { OpportunityPhase } from '../../../../constants/opportunityPhase';

const CaptureDetailPanel = loadable(() => import('./CaptureDetailPanel'));
const BusinessBenefitPanel = loadable(() => import('./BusinessBenefitPanel'));
const ResponsePanel = loadable(() => import('./ResponsePanel'));
const ReviewRequest = loadable(() => import('./ReviewRequest'));

interface OpportunityCaptureTab {
  opportunity: IOpportunity;
  onRefresh: () => void;
}
enum Accordions {
  Detail = 1,
  BusinessBenefit,
  ReviewRequest,
  Reviews,
}
const OpportunityCaptureTab: React.FC<OpportunityCaptureTab> = props => {
  const { opportunity, onRefresh } = props;
  const isUnmounted = useUnmounted();
  const { tab, panel } = useOpportunityRoute();
  const [expanded, setExpanded] = useState<Accordions | undefined>(() => {
    return tab === OpportunityPhase.Capture ? (panel as Accordions) : undefined;
  });
  const [benefitFocuses, setBenefitFocuses] = useState<IBenefitFocusCategory[]>(
    [],
  );
  const [benefitFocusValues, setBenefitFocusValues] = useState<
    IBenefitFocusValue[]
  >([]);
  const [corporateObjectives, setCorporateObjectives] = useState<
    ICorporateObjective[]
  >([]);
  const [businessUnits, setBusinessUnits] = useState<IBusinessUnit[]>([]);
  const [businessProcesses, setBusinessProcesses] = useState<
    IBusinessProcesses[]
  >([]);

  useEffectOnlyOnce(() => {
    Promise.all([
      getDataBusinessUnits(),
      getDataBusinessProcesses(),
      getDataCorporateObjectives(),
      getDataBenefitFocus(),
      getDataBenefitFocusValues(),
    ]);
  });

  const onChangePanel = (name: Accordions) => () => {
    setExpanded(expanded !== name ? name : undefined);
  };

  const renderExpansionIcon = (current: Accordions) => {
    return current === expanded ? <RemoveCircleIcon /> : <AddCircleIcon />;
  };

  const getDataBusinessUnits = async () => {
    const response = await getBusinessUnits();
    if (isUnmounted.current) return;
    if (response.success) {
      setBusinessUnits(response.data.list);
    }
  };

  const getDataBusinessProcesses = async () => {
    const response = await getBusinessProcesses();
    if (isUnmounted.current) return;
    if (response.success) {
      setBusinessProcesses(response.data.list);
    }
  };

  const getDataCorporateObjectives = async () => {
    const response = await getCorporateObjectives();
    if (isUnmounted.current) return;
    if (response.success) {
      setCorporateObjectives(response.data.list);
    }
  };

  const getDataBenefitFocus = async () => {
    const response = await getBenefitFocusCategories();
    if (isUnmounted.current) return;
    if (response.success) {
      setBenefitFocuses(response.data.list);
    }
  };

  const getDataBenefitFocusValues = async () => {
    const json = await getBenefitFocusValues();
    if (isUnmounted.current) return;
    if (json.success) setBenefitFocusValues(json.data.list);
  };

  const onSubmit = useCallback(
    async (opportunityParams: IOpportunityParam) => {
      let data = { ...opportunityParams };
      if (data.corporate_objectives) {
        data.corporate_objectives = data.corporate_objectives.filter(
          item => item !== 0,
        );
      }
      if (data.benefit_focuses) {
        data.benefit_focuses = data.benefit_focuses.filter(
          item => item.benefit_focus_id !== 0,
        );
      }
      if (data.benefit_metrics) {
        data.benefit_metrics = data.benefit_metrics.filter(
          item => item.trim() !== '',
        );
      }
      await updateOpportunity(data, opportunity.id);
      if (opportunityParams.private_flag !== opportunity.private_flag) {
        await setOpportunityPrivate(
          opportunity.id,
          opportunityParams.private_flag,
        );
      }
      onRefresh();
    },
    [onRefresh, opportunity.id, opportunity.private_flag],
  );
  const { t } = useTranslation();

  return (
    <>
      <ExpansionPanelStyled
        expanded={expanded === Accordions.Detail}
        onChange={onChangePanel(Accordions.Detail)}
      >
        <ExpansionPanelSummaryStyled
          aria-controls="opportunity-details-content"
          id="opportunity-details-header"
          expandIcon={renderExpansionIcon(Accordions.Detail)}
        >
          <Typography variant="h5">{t('details')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <CaptureDetailPanel
            businessUnits={businessUnits}
            businessProcesses={businessProcesses}
            corporateObjectives={opportunity.corporate_objectives}
            benefitFocuses={benefitFocuses}
            handleOpportunity={onSubmit}
          />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/* End Capture Panel */}
      {/* Start Business Benefit Panel */}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.BusinessBenefit}
        onChange={onChangePanel(Accordions.BusinessBenefit)}
      >
        <ExpansionPanelSummaryStyled
          aria-controls="opportunity-business-benefits-content"
          id="opportunity-business-benefits-header"
          expandIcon={renderExpansionIcon(Accordions.BusinessBenefit)}
        >
          <Typography variant="h5">{t('business_benefits')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <BusinessBenefitPanel
            opportunity={opportunity}
            benefitFocuses={benefitFocuses}
            benefitFocusValues={benefitFocusValues}
            corporateObjectives={corporateObjectives}
            handleOpportunity={onSubmit}
          />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/* End Business Benefit Panel */}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.ReviewRequest}
        onChange={onChangePanel(Accordions.ReviewRequest)}
      >
        <ExpansionPanelSummaryStyled
          aria-controls="opportunity-review-request-content"
          id="opportunity-review-request-header"
          expandIcon={renderExpansionIcon(Accordions.ReviewRequest)}
        >
          <Typography variant="h5">{t('review_request')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <ReviewRequest />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/* Start Response Panel */}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.Reviews}
        onChange={onChangePanel(Accordions.Reviews)}
      >
        <ExpansionPanelSummaryStyled
          aria-controls="opportunity-response-content"
          id="opportunity-response-header"
          expandIcon={renderExpansionIcon(Accordions.Reviews)}
        >
          <Typography variant="h5">{t('comments')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <ResponsePanel opportunity={opportunity} />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/* End Response Panel */}
    </>
  );
};

export default OpportunityCaptureTab;
