import React, { useCallback, useState } from 'react';
import {
  Avatar,
  Box,
  Grid,
  Link,
  TextField,
  Typography,
} from '@material-ui/core';
import ReplyIcon from '@material-ui/icons/Reply';
import moment from 'moment';
import {
  addTaskComment,
  IComment,
} from '../../../../../services/comment.services';
import { makeStyles } from '@material-ui/core/styles';
import { useAutoHeightInput, useToggleValue } from '../../../../../hooks';
import { ReactiveList } from '@appbaseio/reactivesearch';
import { customChildCommentQuery } from '../../../../../utils';

interface CommentListItemProps {
  data: IComment;
  bordered?: boolean;
  canReply?: boolean;
  showReply?: boolean;
  canEdit?: boolean;
  replyCount: number;
}
export const CommentListItem: React.FC<CommentListItemProps> = React.memo(
  props => {
    const { data, bordered, canReply, canEdit, replyCount } = props;
    const [showReply, toggleReply] = useToggleValue(true);
    const {
      value,
      textRows,
      setValue,
      onChange,
      setTextRows,
    } = useAutoHeightInput(1, 4);

    const [lastRefresh, setLastRefresh] = useState(0);
    const [count, setCount] = useState(replyCount);
    const classes = useStyles();

    function onKeyDown(event: React.KeyboardEvent<HTMLDivElement>) {
      if (event.keyCode === 13) {
        event.preventDefault();
        if (event.ctrlKey || event.shiftKey) {
          setTextRows(prevState => prevState + 1);
          setValue(value + '\n');
        } else {
          onReply().then(() => {
            setTimeout(() => {
              setCount(count + 1);
              setLastRefresh(Date.now());
            }, 900);
          });
        }
      }
    }

    async function onReply() {
      if (!value.trim()) return;
      const newComment = {
        entity_type: data.entity_type,
        entity_id: data?.entity_id,
        comment_type: data.comment_type,
        comment_body: value.trimRight(),
        comment_parent_id: data.id,
      };
      const json = await addTaskComment(newComment);
      if (json.success) {
        setValue('');
        setTextRows(1);
        return json;
      }
      await Promise.reject('Error add reply to comment ' + data.id);
    }

    const defaultQueryReply = useCallback(
      () => customChildCommentQuery(data.id, lastRefresh),
      [data.id, lastRefresh],
    );

    return (
      <Box
        paddingTop={1}
        paddingBottom={1}
        mr={2}
        borderTop={bordered ? '1px solid #eee' : null}
        className={classes.fluid}
        ml={1}
      >
        <Grid container>
          <Box width={33}>
            <Avatar className={classes.avatar}>
              {data.created_by?.name.charAt(0) ?? ''}
            </Avatar>
          </Box>
          <Grid item xs className={classes.emailContent}>
            <Box mr={2}>
              <Typography className={classes.email} component="p">
                {data.created_by?.name ?? ''}
              </Typography>
            </Box>
            <Box mt={1}>
              <Typography component="span" className={classes.content}>
                {data?.comment_body}
              </Typography>
            </Box>
            <Box mt={1} display="flex">
              <Box flex={1}>
                {props.showReply && (
                  <Link
                    component="button"
                    variant="body2"
                    color="inherit"
                    className={classes.btnLink}
                    onClick={toggleReply}
                  >
                    <ReplyIcon />{' '}
                    {count === 1
                      ? '1 Reply'
                      : count > 1
                      ? `${count} Replies`
                      : 'Reply'}
                  </Link>
                )}
                {canEdit && (
                  <Link
                    component="button"
                    variant="body2"
                    color="inherit"
                    className={classes.btnLink}
                  >
                    Edit
                  </Link>
                )}
              </Box>
              <Box display="flex" justifyContent="flex-end" alignItems="center">
                <Typography component="span" className={classes.content}>
                  {moment(data?.created_at).fromNow()}
                </Typography>
              </Box>
            </Box>
            {props.showReply && showReply && (
              <Box mt={1} display="flex">
                <ReactiveList
                  componentId={`Replies of Comment ${data.id}`}
                  dataField="created_at"
                  sortBy="asc"
                  renderResultStats={() => null}
                  renderNoResults={() => null}
                  defaultQuery={defaultQueryReply}
                  showLoader={true}
                  className={classes.fluid}
                >
                  {({ data }) => {
                    return (
                      <>
                        {data.map((item: IComment) => (
                          <CommentListItem
                            key={item.id}
                            data={item}
                            replyCount={0}
                          />
                        ))}
                        {canReply && (
                          <Box
                            flex={1}
                            display="flex"
                            flexDirection="column"
                            margin={1}
                          >
                            <Box display="flex" flexDirection="row" flex={1}>
                              <Box mr={2}>
                                <Avatar className={classes.avatar}>A</Avatar>
                              </Box>
                              <TextField
                                fullWidth
                                placeholder="Write your reply..."
                                name="reply"
                                multiline
                                rows={textRows}
                                variant="outlined"
                                value={value}
                                onKeyDown={onKeyDown}
                                onChange={onChange}
                              />
                            </Box>
                          </Box>
                        )}
                      </>
                    );
                  }}
                </ReactiveList>
              </Box>
            )}
          </Grid>
        </Grid>
      </Box>
    );
  },
);

const useStyles = makeStyles(theme => ({
  emailContent: {
    paddingLeft: '10px',
    '& *': {
      fontSize: 14,
    },
  },
  email: {
    color: theme.palette.grey[900],
    fontWeight: 700,
  },
  content: {
    color: theme.palette.grey[700],
    lineHeight: '16px',
    whiteSpace: 'pre-wrap',
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
  btnLink: {
    padding: 5,
    marginRight: 5,
    '&:hover': {
      textDecoration: 'none',
    },
  },
  fluid: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
  },
}));
