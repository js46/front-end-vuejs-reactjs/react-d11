import React, { useState } from 'react';
import {
  Box,
  Grid,
  FormControl,
  TextField,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { ICaptureReviewsProps } from '../../types';
import { useDisableMultipleClick } from '../../../../hooks';
import { CommentReactiveList } from './components';
import { customQueryComment } from '../../../../utils';
import { addOpportunityComment } from '../../../../services/comment.services';
const ResponsePanel: React.FC<ICaptureReviewsProps> = ({ opportunity }) => {
  const [lastRefresh, setLastRefresh] = useState(0);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const editable = opportunity?.opportunity_phase?.name === 'capture';
  const [content, setContent] = useState('');

  const handleSubmit = async () => {
    if (isSubmitting) return;
    await debounceFn();
    await onSubmit(content);
    endRequest();
  };

  const onSubmit = async (content: string) => {
    if (content && opportunity) {
      try {
        const response = await addOpportunityComment({
          entity_id: opportunity.id,
          entity_type: 'opportunity',
          comment_type: 'peer_review',
          comment_body: content,
          comment_parent_id: 0,
        });
        if (response.success) {
          setTimeout(() => setLastRefresh(new Date().getTime()), 2000);
          setContent('');
        }
      } catch (err) {
        console.log(err);
      }
      return;
    }
  };
  const queryComment = () => {
    if (!opportunity) return;
    return customQueryComment(
      opportunity.id,
      {
        entity_type: 'opportunity',
        comment_type: 'peer_review',
        comment_parent_id: 0,
      },
      lastRefresh,
    );
  };
  const queryCommentCount = () => {
    if (!opportunity) return;
    return customQueryComment(
      opportunity.id,
      {
        entity_type: 'opportunity',
        comment_type: 'peer_review',
      },
      lastRefresh,
    );
  };
  return (
    <Grid container>
      <CommentReactiveList
        defaultQuery={queryComment}
        defaultQueryCount={queryCommentCount}
        showReply
        canReply={editable}
        loader={
          <Box display="flex" justifyContent="center" p={1}>
            <CircularProgress />
          </Box>
        }
      />
      {editable && (
        <Box mt={2} width="100%">
          <Box mt={2} flex={1}>
            <Grid container>
              <Grid container>
                <Grid item xs>
                  <FormControl
                    required
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    disabled={!editable}
                  >
                    <TextField
                      value={content}
                      multiline
                      rows={3}
                      onChange={e => setContent(e.target.value)}
                      variant="outlined"
                      disabled={!editable}
                      id="content"
                      name="content"
                      required
                      autoFocus
                      // helperText={touched.description && errors.description}
                      // error={touched.description && !!errors.description}
                    />
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>
          </Box>
          <Box ml={1}>
            <Button
              type="button"
              size="medium"
              variant="contained"
              color="primary"
              onClick={handleSubmit}
              disabled={isSubmitting}
              startIcon={
                isSubmitting ? <CircularProgress size="small" /> : undefined
              }
            >
              Add
            </Button>
          </Box>
        </Box>
      )}
    </Grid>
  );
};
export default ResponsePanel;
