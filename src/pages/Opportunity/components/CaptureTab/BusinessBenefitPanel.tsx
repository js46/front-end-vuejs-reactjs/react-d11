import React from 'react';
import { Formik, FieldArray } from 'formik';
import clsx from 'clsx';
import {
  TextField,
  Button,
  Grid,
  Box,
  Container,
  Typography,
  FormControl,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { IOpportunityParam } from '../../../../services/opportunity.services';
import { ICaptureBenefitsProps } from '../../types';
import { CustomSelect, MixTitle } from '../../../../components';
import { useToggleValue, useDisableMultipleClick } from '../../../../hooks';
import { PanelActionButton } from '../PanelActionButton';
import { UserProfileState } from '../../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(6),
  },
  inlineBlock: {
    display: 'inline-block',
  },
  padding: {
    padding: theme.spacing(4),
  },
  dflex: {
    display: 'flex',
    alignItems: 'center',
  },
  iteratorNumber: {
    marginRight: '15px',
  },
  form: {
    margin: 0,
  },
  bottomMargin: {
    marginBottom: '20px',
  },
  paddingLeft: {
    paddingLeft: '70px',
  },
  leftMargin: {
    marginLeft: '5px',
  },
  centerElement: {
    textAlign: 'center',
  },
  getBoldText: {
    fontWeight: 700,
  },
  totalMoneyStyle: {
    display: 'inline',
    margin: '0 5px',
  },
  numberInput: {
    marginLeft: '5px',
    marginRight: '5px',
    '& input': {
      textAlign: 'end',
    },
  },
  addButton: {
    padding: '0 20px',
  },
  opporTitle: {
    fontSize: '14px',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  removeBtn: {
    marginLeft: '10px',
    padding: '0 20px',
    textTransform: 'capitalize',
  },
  helpIcon: {
    marginLeft: '30px',
    marginBottom: '-5px',
    fill: '#ddd',
  },
  helpText: {
    marginLeft: '30px',
    fontSize: '13px',
    color: '#6F747A',
  },
  properMargin: {
    '& .MuiFormControl-marginNormal': {
      margin: 0,
    },
  },
  formControl: {
    flexDirection: 'row',
    alignItems: 'center',
  },
}));

export const BusinessBenefitPanel: React.FC<ICaptureBenefitsProps> = React.memo(
  props => {
    const classes = useStyles();
    const {
      opportunity,
      corporateObjectives,
      benefitFocuses,
      benefitFocusValues,
      handleOpportunity,
    } = props;
    const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
    const userProfileState = useSelector(UserProfileState);
    const [editable, toggleEditable] = useToggleValue(false);
    const initialValues: IOpportunityParam = {
      benefit_metrics: opportunity?.benefit_metrics || [''],
      benefit_focuses: opportunity?.benefit_focuses
        ? opportunity.benefit_focuses.map(item => ({
            benefit_focus_id: item.benefit_focus_id,
            benefit_focus_value_id: item.benefit_focus_value.id,
          }))
        : [
            {
              benefit_focus_id: 0,
              benefit_focus_value_id: 0,
            },
          ],
      corporate_objectives:
        opportunity && opportunity.corporate_objectives
          ? opportunity.corporate_objectives.map(item => item.id)
          : [0],
    };

    if (!opportunity) return <></>;

    Yup.addMethod(Yup.array, 'unique', function(
      mapper = (a: any) => a,
      // eslint-disable-next-line
      message: string = '${path} may not have duplicates',
    ) {
      return this.test('unique', message, list => {
        return list.length === new Set(list.map(mapper)).size;
      });
    });

    const toggleForm = (onReset: () => void) => () => {
      if (onReset && editable) onReset();
      return toggleEditable();
    };

    const renderActionButtons = (
      handleSubmit: () => void,
      onReset: () => void,
    ) => (
      <PanelActionButton
        enableEdit={editable}
        onToggleEdit={toggleForm(onReset)}
        onSubmit={handleSubmit}
        disabled={isSubmitting}
      />
    );

    const checkAddBtn = (selections: number[]): boolean => {
      return selections.some(option => option === 0);
    };
    const { t } = useTranslation();

    return (
      <Formik
        initialValues={initialValues}
        validationSchema={Yup.object().shape({
          benefit_metrics: Yup.array()
            .of(Yup.string())
            .unique((s: any) => s, 'This field should be unique'),
        })}
        enableReinitialize
        onSubmit={async values => {
          if (isSubmitting || !handleOpportunity) {
            return;
          }
          await debounceFn();
          await handleOpportunity(values).then(toggleEditable);
          endRequest();
        }}
      >
        {({
          isSubmitting,
          // dirty,
          // isValid,
          values,
          handleChange,
          handleSubmit,
          resetForm,
          // handleBlur,
          touched,
          errors,
        }) => (
          <form onSubmit={handleSubmit} style={{ width: '100%' }}>
            <Box>
              {opportunity?.created_by?.id === userProfileState?.id &&
                renderActionButtons(handleSubmit, resetForm)}
              <Box className={classes.bottomMargin}>
                <Typography component="span">
                  {t('please_enter_benefits')}
                </Typography>
                <Typography component="span" className={classes.opporTitle}>
                  {' '}
                  {opportunity.title ?? ''}{' '}
                </Typography>
                <Typography component="span">
                  {t('in_the_fields_below')}
                </Typography>
              </Box>
              <Box className={classes.bottomMargin}>
                <MixTitle
                  isHelper={true}
                  title="Alignment to corporate objectives"
                  helperText={t(
                    'business_benefits_in_opp.helpText.alignment_to_corporate_objectives',
                  )}
                />
                <FieldArray name="corporate_objectives">
                  {arrayHelpers => {
                    return (
                      <>
                        <Box>
                          {values.corporate_objectives &&
                            values.corporate_objectives.map((item, index) => (
                              <Grid
                                key={index}
                                container
                                className={classes.bottomMargin}
                                spacing={3}
                              >
                                <Grid item xs={10} className={classes.dflex}>
                                  <FormControl
                                    required
                                    fullWidth
                                    error={
                                      (touched.processes &&
                                        !!errors.processes) ||
                                      (!!errors.corporate_objectives &&
                                        !!errors.corporate_objectives[index])
                                    }
                                    margin="none"
                                    variant="outlined"
                                    disabled={!editable}
                                    className={classes.formControl}
                                  >
                                    <Typography
                                      className={classes.iteratorNumber}
                                    >
                                      {index + 1}.
                                    </Typography>
                                    <CustomSelect
                                      fullWidth
                                      value={item ? item : ''}
                                      name={`corporate_objectives.${index}`}
                                      onChange={handleChange}
                                      options={[
                                        {
                                          id: 0,
                                          name: 'None',
                                          archive: false,
                                          style: { fontStyle: 'italic' },
                                        },
                                        ...corporateObjectives,
                                      ].filter(
                                        item =>
                                          item.id ===
                                            values.corporate_objectives![
                                              index
                                            ] ||
                                          (!values.corporate_objectives!.includes(
                                            item.id,
                                          ) &&
                                            !item.archive),
                                      )}
                                    />
                                  </FormControl>
                                </Grid>
                                {editable && index > 0 && (
                                  <Grid item xs={1} className={classes.dflex}>
                                    <Button
                                      className={classes.removeBtn}
                                      variant="outlined"
                                      onClick={() =>
                                        !isSubmitting &&
                                        arrayHelpers.remove(index)
                                      }
                                      disabled={isSubmitting}
                                    >
                                      Remove
                                    </Button>
                                  </Grid>
                                )}
                              </Grid>
                            ))}
                        </Box>
                        <Grid container>
                          <Grid item xs={10}>
                            <Container className={classes.centerElement}>
                              {editable &&
                                values.corporate_objectives &&
                                values.corporate_objectives!.length <
                                  corporateObjectives.length && (
                                  <Button
                                    variant="outlined"
                                    className={classes.addButton}
                                    onClick={() => {
                                      if (isSubmitting) return;
                                      arrayHelpers.push(0);
                                      if (
                                        !values.corporate_objectives?.filter(
                                          item => item,
                                        ).length
                                      ) {
                                        return;
                                      }
                                    }}
                                    disabled={checkAddBtn(
                                      values.corporate_objectives,
                                    )}
                                  >
                                    Add
                                  </Button>
                                )}
                            </Container>
                          </Grid>
                        </Grid>
                      </>
                    );
                  }}
                </FieldArray>
              </Box>
              <Box className={clsx(classes.bottomMargin, classes.root)}>
                <Box>
                  <Grid container>
                    <Grid item xs={5}>
                      <MixTitle
                        isHelper={true}
                        title="Business Focus Category"
                        helperText={t(
                          'business_benefits_in_opp.helpText.benefit_focus_category',
                        )}
                      />
                    </Grid>
                    <Grid item xs={5}>
                      <MixTitle
                        isHelper={true}
                        title="Business Benefit Value p.a."
                        helperText={t(
                          'business_benefits_in_opp.helpText.business_benefit_value',
                        )}
                      />
                    </Grid>
                  </Grid>
                </Box>
                <FieldArray
                  name="benefit_focuses"
                  render={arrayHelpers => {
                    return (
                      <>
                        <Box>
                          {values.benefit_focuses &&
                            values.benefit_focuses.map((item, index) => (
                              <Box key={index} className={classes.bottomMargin}>
                                <Grid container spacing={3}>
                                  <Grid
                                    item
                                    xs
                                    sm={5}
                                    className={classes.dflex}
                                  >
                                    <FormControl
                                      required
                                      fullWidth
                                      margin="none"
                                      variant="outlined"
                                      disabled={!editable}
                                      className={classes.formControl}
                                      error={
                                        touched.benefit_focuses &&
                                        !!errors.benefit_focuses &&
                                        !!errors.benefit_focuses[index]
                                      }
                                    >
                                      <Typography
                                        className={classes.iteratorNumber}
                                      >
                                        {index + 1}.
                                      </Typography>
                                      <CustomSelect
                                        fullWidth
                                        value={item.benefit_focus_id || ''}
                                        name={`benefit_focuses[${index}].benefit_focus_id`}
                                        onChange={handleChange}
                                        options={[
                                          {
                                            id: 0,
                                            name: 'None',
                                            archive: false,
                                            style: { fontStyle: 'italic' },
                                          },
                                          ...benefitFocuses,
                                        ].filter(
                                          item =>
                                            item.id ===
                                              values.benefit_focuses![index]
                                                .benefit_focus_id ||
                                            (!values.benefit_focuses!.filter(
                                              bf =>
                                                bf.benefit_focus_id === item.id,
                                            ).length &&
                                              !item.archive),
                                        )}
                                      />
                                    </FormControl>
                                  </Grid>
                                  <Grid item xs sm={5}>
                                    <FormControl
                                      required
                                      fullWidth
                                      margin="none"
                                      variant="outlined"
                                      disabled={!editable}
                                      className={classes.formControl}
                                      error={
                                        touched.benefit_focuses &&
                                        !!errors.benefit_focuses &&
                                        !!errors.benefit_focuses[index]
                                      }
                                    >
                                      <CustomSelect
                                        fullWidth
                                        value={
                                          item.benefit_focus_value_id || ''
                                        }
                                        name={`benefit_focuses[${index}].benefit_focus_value_id`}
                                        onChange={handleChange}
                                        options={[
                                          {
                                            id: 0,
                                            name: 'None',
                                            style: { fontStyle: 'italic' },
                                          },
                                          ...benefitFocusValues.map(i => ({
                                            id: i.id,
                                            name: i.band,
                                          })),
                                        ]}
                                      />
                                    </FormControl>
                                  </Grid>
                                  {editable && index > 0 && (
                                    <Grid
                                      item
                                      xs
                                      sm={1}
                                      className={classes.dflex}
                                    >
                                      <Button
                                        variant="outlined"
                                        className={classes.removeBtn}
                                        onClick={() => {
                                          if (isSubmitting) {
                                            return;
                                          }
                                          arrayHelpers.remove(index);
                                        }}
                                        // disabled={isSubmitting}
                                      >
                                        Remove
                                      </Button>
                                    </Grid>
                                  )}
                                </Grid>
                              </Box>
                            ))}
                        </Box>
                        <Grid container>
                          <Grid item xs={5}>
                            <Container className={classes.centerElement}>
                              {editable &&
                                values.benefit_focuses!.length <
                                  benefitFocuses.length && (
                                  <Button
                                    // color="primary"
                                    variant="outlined"
                                    className={classes.addButton}
                                    onClick={() => {
                                      if (isSubmitting) return;
                                      arrayHelpers.push({
                                        benefit_focus_id: 0,
                                        benefit_value: 0,
                                      });
                                      if (
                                        !values.benefit_focuses?.filter(
                                          item =>
                                            item.benefit_focus_id &&
                                            item.benefit_focus_value_id,
                                        ).length
                                      ) {
                                        return;
                                      }
                                    }}
                                    disabled={checkAddBtn(
                                      values.benefit_focuses
                                        ? values.benefit_focuses.map(
                                            item => item.benefit_focus_id,
                                          )
                                        : [],
                                    )}
                                  >
                                    Add
                                  </Button>
                                )}
                            </Container>
                          </Grid>
                        </Grid>
                      </>
                    );
                  }}
                />
              </Box>
              <Box className={classes.root}>
                <MixTitle
                  isHelper={true}
                  title="Business Benefit Metrics"
                  moreText="Max 50 words each"
                  helperText={t(
                    'business_benefits_in_opp.helpText.business_benefit_metrics',
                  )}
                />
                <FieldArray
                  name="benefit_metrics"
                  render={arrayHelpers => {
                    return (
                      <>
                        <Box>
                          {values.benefit_metrics &&
                            values.benefit_metrics.map((item, index) => (
                              <Grid
                                key={index}
                                container
                                spacing={3}
                                className={classes.bottomMargin}
                              >
                                <Grid
                                  item
                                  xs={10}
                                  className={clsx(
                                    classes.dflex,
                                    classes.properMargin,
                                  )}
                                >
                                  <Typography
                                    component="span"
                                    className={classes.iteratorNumber}
                                  >
                                    {index + 1}.
                                  </Typography>
                                  <TextField
                                    value={item}
                                    variant="outlined"
                                    fullWidth
                                    className={clsx(classes.inlineBlock)}
                                    name={`benefit_metrics.${index}`}
                                    onChange={handleChange}
                                    disabled={!editable}
                                    InputLabelProps={{
                                      shrink: true,
                                    }}
                                    margin="normal"
                                    error={
                                      touched.benefit_metrics &&
                                      !!errors.benefit_metrics &&
                                      !!errors.benefit_metrics[index]
                                    }
                                  />
                                </Grid>
                                {editable && index > 0 && (
                                  <Grid item xs={2} className={classes.dflex}>
                                    <Button
                                      variant="outlined"
                                      className={classes.removeBtn}
                                      onClick={() => {
                                        if (isSubmitting) {
                                          return;
                                        }
                                        arrayHelpers.remove(index);
                                      }}
                                      disabled={
                                        // isSubmitting ||
                                        typeof errors.benefit_metrics ===
                                        'string'
                                      }
                                      aria-label="close"
                                    >
                                      Remove
                                    </Button>
                                  </Grid>
                                )}
                              </Grid>
                            ))}
                        </Box>
                        <Grid container>
                          <Grid item xs={10}>
                            <Container className={classes.centerElement}>
                              {editable && (
                                <Button
                                  // color="primary"
                                  variant="outlined"
                                  className={classes.addButton}
                                  onClick={() => {
                                    if (isSubmitting) return;
                                    arrayHelpers.push('');
                                  }}
                                  disabled={
                                    // isSubmitting ||
                                    values.benefit_metrics![
                                      values.benefit_metrics!.length - 1
                                    ] === '' ||
                                    typeof errors.benefit_metrics === 'string'
                                  }
                                >
                                  Add
                                </Button>
                              )}
                            </Container>
                          </Grid>
                        </Grid>
                      </>
                    );
                  }}
                />
              </Box>
              {editable && renderActionButtons(handleSubmit, resetForm)}
            </Box>
          </form>
        )}
      </Formik>
    );
  },
);
export default BusinessBenefitPanel;
