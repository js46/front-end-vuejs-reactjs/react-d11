import React from 'react';
import { Formik } from 'formik';
import {
  TextField,
  FormControl,
  Select,
  MenuItem,
  FormHelperText,
  Chip,
  Checkbox,
  ListItemText,
  InputAdornment,
  Box,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { IOpportunityParam } from '../../../../services/opportunity.services';
import { ICaptureDetailProps } from '../../types';
import { MixTitle } from '../../../../components';
import { PanelActionButton } from '../PanelActionButton';
import { useToggleValue, useDisableMultipleClick } from '../../../../hooks';
import { checkRestrictedWord } from '../../../../utils';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../../store/user/selector';
import { OpportunityState } from '../../../../store/opportunity/selector';
const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(3),
  },
  rootRight: {
    paddingRight: 30,
  },
  bottomMargin: {
    marginBottom: '20px',
  },
  rootLeftRight: {
    paddingLeft: 30,
    paddingRight: 50,
  },
  selectItem: {
    color: '#666',
    backgroundColor: '#e0e0e0',
  },
  required: {
    color: 'red',
  },
}));

export const CaptureDetailPanel: React.FC<ICaptureDetailProps> = ({
  businessUnits,
  businessProcesses,
  handleOpportunity,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const userProfileState = useSelector(UserProfileState);
  const [editable, toggleEditable] = useToggleValue(false);
  const opportunity = useSelector(OpportunityState);
  const initialValues: IOpportunityParam = {
    proposer_email: opportunity?.created_by?.email || '',
    proposer_business_unit: opportunity?.created_by?.business_unit || '',
    business_units:
      opportunity && opportunity.business_units
        ? opportunity.business_units.map(item => item.id)
        : [],
    title: opportunity?.title || '',
    description: opportunity?.description || '',
    hypothesis: opportunity?.hypothesis || '',
    processes:
      opportunity && opportunity.processes
        ? opportunity.processes.map(item => item.id)
        : [],
    requested_completion_date: opportunity
      ? new Date(opportunity?.requested_completion_date).toISOString()
      : new Date().toISOString(),
    priority_level: opportunity?.opp_priority_score?.priority_level || 'Medium',
    corporate_objectives: [0],
    benefit_focuses: null,
    benefit_metrics: [],
    sponsor_business_unit_id: opportunity?.sponsor_business_unit.id || 0,
    private_flag: opportunity?.private_flag,
  };

  const validationsSchema = Yup.object().shape({
    // proposer_title: Yup.string().required('Required'),
    // proposer_name: Yup.string().required('Required'),
    proposer_email: Yup.string().required('Required'),
    proposer_business_unit: Yup.string().required('Required'),
    // business_units: Yup.string().required('Required'),
    // processes: Yup.string().required('Required'),
    title: Yup.string().required('Required'),
    description: Yup.string()
      .required('Required')
      .test('len', 'Description should be less than 300 words', function(val) {
        return val === undefined || checkRestrictedWord(val) <= 300;
      }),
    hypothesis: Yup.string().test(
      'len',
      'Hypothesis should be less than 300 words',
      function(val) {
        return val === undefined || checkRestrictedWord(val) <= 300;
      },
    ),
  });

  const toggleForm = (onReset: () => void) => () => {
    if (editable) onReset();
    return toggleEditable();
  };

  const renderActionButtons = (
    handleSubmit: () => void,
    onReset: () => void,
  ) => (
    <PanelActionButton
      enableEdit={editable}
      onToggleEdit={toggleForm(onReset)}
      onSubmit={handleSubmit}
      disabled={isSubmitting}
    />
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationsSchema}
      onSubmit={async values => {
        if (isSubmitting || !handleOpportunity) {
          return;
        }
        await debounceFn();
        await handleOpportunity(values).then(toggleEditable);
        endRequest();
      }}
    >
      {({
        isSubmitting,
        values,
        setFieldValue,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        resetForm,
        // isValid,
      }) => (
        <form onSubmit={handleSubmit}>
          <Grid container>
            <Grid item xs={12} container className={classes.root}>
              {opportunity?.created_by?.id === userProfileState?.id &&
                renderActionButtons(handleSubmit, resetForm)}

              <Grid item container spacing={2}>
                <Grid item xs={12} md={4}>
                  <MixTitle
                    isRequired={true}
                    isHelper={true}
                    title="Proposer email"
                    helperText={t('capture.help_text.proposer_email')}
                  />
                  <TextField
                    id="proposer_email"
                    name="proposer_email"
                    defaultValue={values.proposer_email}
                    onBlur={handleBlur}
                    type="text"
                    InputLabelProps={{ shrink: true }}
                    fullWidth
                    variant="outlined"
                    helperText={touched.proposer_email && errors.proposer_email}
                    error={touched.proposer_email && !!errors.proposer_email}
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <MixTitle
                    isRequired={true}
                    isHelper
                    title="Proposer business unit"
                    helperText={t('capture.help_text.proposer_bu')}
                  />
                  <TextField
                    id="proposer_business_unit"
                    name="proposer_business_unit"
                    defaultValue={values.proposer_business_unit}
                    onBlur={handleBlur}
                    type="text"
                    InputLabelProps={{ shrink: true }}
                    fullWidth
                    variant="outlined"
                    helperText={
                      touched.proposer_business_unit &&
                      errors.proposer_business_unit
                    }
                    error={
                      touched.proposer_business_unit &&
                      !!errors.proposer_business_unit
                    }
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <FormControl
                    required
                    fullWidth
                    error={touched.processes && !!errors.processes}
                    margin="normal"
                    variant="outlined"
                    disabled={!editable}
                  >
                    <MixTitle
                      title="Sponsor business unit"
                      isHeader
                      helperText={t('capture.help_text.sponsor_bu')}
                    />
                    <Select
                      value={values.sponsor_business_unit_id}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      id="sponsor_business_unit"
                      name="sponsor_business_unit_id"
                    >
                      {businessUnits
                        .filter(item => !item.archive)
                        .map(item => (
                          <MenuItem key={item.id} value={item.id}>
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                    <FormHelperText>
                      {touched.processes ? errors.processes : ''}
                    </FormHelperText>
                  </FormControl>
                </Grid>
              </Grid>
              <Grid item container className={classes.root}>
                <Grid item xs={12}>
                  <MixTitle
                    isRequired={true}
                    isHelper={true}
                    title="Title"
                    helperText={t('capture.help_text.title')}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="title"
                    name="title"
                    value={values.title}
                    onChange={handleChange}
                    type="text"
                    InputLabelProps={{ shrink: true }}
                    fullWidth
                    variant="outlined"
                    helperText={touched.title && errors.title}
                    error={touched.title && !!errors.title}
                    disabled={isSubmitting || !editable}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <FormControl fullWidth>
                <MixTitle
                  isRequired={true}
                  isHelper={true}
                  title="Description"
                  moreText="Max 300 words each"
                  helperText={t('capture.help_text.description')}
                />
                <TextField
                  value={values.description}
                  onChange={handleChange}
                  rows={12}
                  // labelWidth={100}
                  variant="outlined"
                  multiline
                  id="description"
                  name="description"
                  helperText={touched.description && errors.description}
                  error={touched.description && !!errors.description}
                  disabled={!editable}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <FormControl fullWidth>
                <MixTitle
                  isHelper={true}
                  title="Hypotheses"
                  moreText="Max 300 words each"
                  helperText={t('capture.help_text.hypotheses')}
                />
                <TextField
                  value={values.hypothesis}
                  onChange={handleChange}
                  rows={12}
                  // labelWidth={100}
                  variant="outlined"
                  multiline
                  id="hypothesis"
                  name="hypothesis"
                  helperText={touched.hypothesis && errors.hypothesis}
                  error={touched.hypothesis && !!errors.hypothesis}
                  disabled={!editable}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <FormControl
                required
                fullWidth
                error={touched.processes && !!errors.processes}
                margin="normal"
                variant="outlined"
                disabled={!editable}
              >
                <MixTitle
                  isHelper={true}
                  title="Business units impacted"
                  moreText="Max 3 items"
                  helperText={t('capture.help_text.business_unit_impacted')}
                />
                <Select
                  multiple
                  value={values.business_units}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  id="business_units"
                  name="business_units"
                  renderValue={selected => {
                    return businessUnits
                      .filter(pb =>
                        (selected as number[]).includes(pb.id as number),
                      )
                      .map(pb => (
                        <Chip
                          label={pb.name}
                          key={pb.id}
                          className={classes.selectItem}
                          size="small"
                        />
                      ));
                  }}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                >
                  {businessUnits
                    .filter(item => !item.archive)
                    .map(item => (
                      <MenuItem key={item.id} value={item.id}>
                        <Checkbox
                          // color="primary"
                          style={{ color: '#2c54e3' }}
                          checked={
                            values.business_units &&
                            values.business_units.includes(item.id)
                          }
                        />
                        <ListItemText primary={item.name} />
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText>
                  {touched.processes ? errors.processes : ''}
                </FormHelperText>
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.root}>
              <FormControl
                required
                fullWidth
                error={touched.processes && !!errors.processes}
                margin="normal"
                variant="outlined"
                disabled={!editable}
              >
                <MixTitle
                  title="Business processes impacted"
                  isHelper={true}
                  helperText={t(
                    'capture.help_text.business_processes_impacted',
                  )}
                />
                <Select
                  multiple
                  value={values.processes}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  id="processes"
                  name="processes"
                  renderValue={selected => {
                    return businessProcesses
                      .filter(pb =>
                        (selected as number[]).includes(pb.id as number),
                      )
                      .map(pb => (
                        <Chip
                          label={pb.name}
                          key={pb.id}
                          className={classes.selectItem}
                          size="small"
                        />
                      ));
                  }}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                >
                  {businessProcesses
                    .filter(item => !item.archive)
                    .map(item => (
                      <MenuItem key={item.id} value={item.id}>
                        <Checkbox
                          // color="primary"
                          style={{ color: '#2c54e3' }}
                          checked={
                            values.processes &&
                            values.processes.includes(item.id)
                          }
                        />
                        <ListItemText primary={item.name} />
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText>
                  {touched.processes ? errors.processes : ''}
                </FormHelperText>
              </FormControl>
            </Grid>
            <Grid container className={classes.root}>
              <Grid item xs={6} className={classes.rootRight}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <MixTitle
                    title="Requested completion date"
                    isHelper={true}
                    helperText={t(
                      'capture.help_text.requested_completion_date',
                    )}
                  />
                  <DatePicker
                    disabled={!editable}
                    disableToolbar
                    allowKeyboardControl
                    inputVariant="outlined"
                    variant="inline"
                    margin="normal"
                    name="requested_completion_date"
                    id="requested_completion_date"
                    format="d MMMM yyyy"
                    onChange={value => {
                      setFieldValue('requested_completion_date', value);
                    }}
                    value={values.requested_completion_date}
                    animateYearScrolling={false}
                    fullWidth
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <ExpandMoreIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              {/* <Grid item xs={6} className={classes.rootLeftRight}>
                <FormControl
                  required
                  fullWidth
                  error={touched.processes && !!errors.processes}
                  margin="normal"
                  variant="outlined"
                  disabled={!editable}
                >
                  <MixTitle title="Priority level" isHelper={true} />
                  <Select
                    fullWidth
                    id="priority_level"
                    name="priority_level"
                    variant="outlined"
                    value={values.priority_level}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    // helperText={
                    //   errors.priority &&
                    //   touched.priority &&
                    //   errors.priority
                    // }
                    // error={
                    //   touched.priority && !!errors.priority
                    // }
                    IconComponent={ExpandMoreIcon}
                  >
                    <MenuItem value="Low">Low</MenuItem>
                    <MenuItem value="Medium">Medium</MenuItem>
                    <MenuItem value="High">High</MenuItem>
                  </Select>
                </FormControl>
              </Grid> */}
            </Grid>
            <Grid container className={classes.root}>
              <Grid item xs={12}>
                <Box display="flex" alignItems="center">
                  <MixTitle
                    title="Private"
                    isHelper
                    noMargin
                    helperText={t('capture.help_text.private')}
                  />
                  <Checkbox
                    disabled={!editable}
                    value={values.private_flag}
                    checked={values.private_flag}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    color="primary"
                    name="private_flag"
                    id="private_flag"
                  />
                </Box>
              </Grid>
            </Grid>
          </Grid>
          {editable && (
            <Box mt={2}>{renderActionButtons(handleSubmit, resetForm)}</Box>
          )}
        </form>
      )}
    </Formik>
  );
};
export default CaptureDetailPanel;
