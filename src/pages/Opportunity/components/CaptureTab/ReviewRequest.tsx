import React, { useState } from 'react';
import {
  Box,
  Grid,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  Table,
  TableBody,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { INotification } from '../../../Capture/types';
import { useParams } from 'react-router-dom';
import { useEffectOnlyOnce } from '../../../../hooks';
import { getOpportunityNotification } from '../../../../services/notification.services';
import moment from 'moment';
import {
  genStringForMailingServiceStatus,
  genNotificationType,
} from '../../../../utils';

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: '#bbb',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(5),
  },
}));

interface Data {
  email: string;
  type: string;
  status: string;
  dispatch_time: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'email',
    label: 'Email',
  },
  {
    id: 'type',
    label: 'Type',
  },
  {
    id: 'status',
    label: 'Status',
  },
  {
    id: 'dispatch_time',
    label: 'Dispatch time',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  // const { classes } = props;
  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            // style={headCell.id === 'action' ? { width: '150px' } : undefined}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface ICaptureReviewRequest {}

const ReviewRequest: React.FC<ICaptureReviewRequest> = () => {
  const classes = useStyles();
  let { id } = useParams();
  const [notification, setNotification] = useState<INotification[]>();

  useEffectOnlyOnce(() => {
    if (!id) return;
    getOpportunityNotification(Number(id)).then(data => {
      const listNotification = data.data.list
        ?.filter(item => item.opportunity.id === Number(id))
        .map(noti => ({
          id: noti.id,
          email: noti.to_email,
          type: genNotificationType(noti?.notification_user_case?.name),
          status: genStringForMailingServiceStatus(noti.status),
          time: new Date(noti.sent_date),
        }));
      setNotification(listNotification);
    });
  });
  return (
    <Grid container>
      <Box mt={2} width="100%">
        <TableContainer>
          <Table>
            <EnhancedTableHead classes={classes} />
            <TableBody>
              {(!notification || notification?.length === 0) && (
                <TableRow>
                  <TableCell colSpan={4}>
                    <Box
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      No data found!
                    </Box>
                  </TableCell>
                </TableRow>
              )}
              {notification?.map((item, index) => (
                <TableRow key={item.email}>
                  <TableCell>
                    <Typography component="p" variant="body2">
                      {item.email}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography component="p" variant="body2">
                      {item.type}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      component="p"
                      variant="body2"
                      style={
                        item.status === 'Sent'
                          ? { fontWeight: 700, color: '#2c54e3' }
                          : undefined
                      }
                    >
                      {item.status}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography component="p" variant="body2">
                      {item.time && moment(item.time).format('DD/MM/YYYY')}
                    </Typography>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Grid>
  );
};
export default ReviewRequest;
