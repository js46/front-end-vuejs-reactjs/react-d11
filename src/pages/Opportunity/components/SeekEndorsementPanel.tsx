import React, { useCallback, useContext, useEffect, useState } from 'react';
import {
  Avatar,
  Box,
  Button,
  Grid,
  Link,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Menu,
  MenuItem,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';
import { useSelector } from 'react-redux';
import OpportunityContext from '../OpportunityContext';
import { OpportunityActionEnum } from '../../../constants';
import {
  getOpportunityActivityLogs,
  IOpportunityActivityLog,
  IOpportunityLogActions,
} from '../../../services/opportunity.services';
import { useEffectOnlyOnce, useUnmounted } from '../../../hooks';
import moment from 'moment';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
// import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../store/user/selector';
import {
  OpportunityState,
  OpportunityPossibleActions,
} from '../../../store/opportunity/selector';
import { OpportunityPhase } from '../../../constants/opportunityPhase';

interface SeekEndorsementPanelProps {
  isCurrentExpert: boolean;
  phases: string[];
  currentTabPhase: OpportunityPhase;
}

const SeekEndorsementPanel: React.FC<SeekEndorsementPanelProps> = ({
  isCurrentExpert,
  phases,
  currentTabPhase,
}) => {
  const [anchorEl, setAnchorEl] = React.useState<any>(null);
  const possibleActions = useSelector(OpportunityPossibleActions);
  const userProfileState = useSelector(UserProfileState);
  const isUnmounted = useUnmounted();
  const { groupState, setGroupState } = useContext(OpportunityContext);
  const opportunity = useSelector(OpportunityState);
  const [data, setData] = useState<IOpportunityActivityLog[]>([]);
  const [actionsEnabled, setActionsEnabled] = useState<OpportunityActionEnum[]>(
    [],
  );

  useEffectOnlyOnce(() => {
    fetchActivityLogs();
  });

  const fetchActivityLogs = useCallback(async () => {
    if (!opportunity) return;
    const json = await getOpportunityActivityLogs(
      opportunity.id,
      IOpportunityLogActions.Endorse,
      currentTabPhase,
    );
    if (json.success && json.data) {
      const data = json.data;
      data.sort(function compare(a, b) {
        let dateA = new Date(a.date).valueOf();
        let dateB = new Date(b.date).valueOf();
        return dateB - dateA;
      });
      if (isUnmounted.current) return;
      setData(data);
    }
  }, [opportunity, currentTabPhase, isUnmounted]);
  useEffect(() => {
    if (opportunity) {
      const options = [
        OpportunityActionEnum.SendEndorsementRequest,
        OpportunityActionEnum.Endorse,
        OpportunityActionEnum.CancelEndorsementRequest,
        OpportunityActionEnum.SendBackForReview,
        OpportunityActionEnum.SendBackForAnalysis,
      ];
      const actions = possibleActions
        ?.filter(item => phases.includes(item.phase))
        ?.map(action => action.display_name as OpportunityActionEnum)
        ?.filter(item => options.includes(item));
      setActionsEnabled(actions || []);
    }
    setTimeout(() => fetchActivityLogs(), 1000);
  }, [fetchActivityLogs, opportunity, phases, possibleActions]);

  const openMenu = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleSelectMenu = (action: OpportunityActionEnum) => () => {
    handleCloseMenu();
    setGroupState({
      ...groupState,
      selectedAction: action,
      isOpenActionDialog: true,
      isClickNext: true,
    });
  };
  const classes = useStyles();
  // const { t } = useTranslation();

  return (
    <Grid container item direction="column" xs={12}>
      {actionsEnabled.length > 0 && (
        <>
          <Box
            flexDirection="column"
            display="flex"
            flex={1}
            mt={3}
            mb={2}
            alignItems="center"
          >
            <Menu
              id="action-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleCloseMenu}
            >
              {actionsEnabled.includes(
                OpportunityActionEnum.SendEndorsementRequest,
              ) && (
                <MenuItem
                  onClick={handleSelectMenu(
                    OpportunityActionEnum.SendEndorsementRequest,
                  )}
                >
                  Send Endorsement Request
                </MenuItem>
              )}
              {actionsEnabled.includes(OpportunityActionEnum.Endorse) && (
                <MenuItem
                  onClick={handleSelectMenu(OpportunityActionEnum.Endorse)}
                >
                  Endorse
                </MenuItem>
              )}
              {actionsEnabled.includes(
                OpportunityActionEnum.SendBackForReview,
              ) && (
                <MenuItem
                  onClick={handleSelectMenu(
                    OpportunityActionEnum.SendBackForReview,
                  )}
                >
                  Send Back For Review
                </MenuItem>
              )}
              {actionsEnabled.includes(
                OpportunityActionEnum.SendBackForAnalysis,
              ) && (
                <MenuItem
                  onClick={handleSelectMenu(
                    OpportunityActionEnum.SendBackForAnalysis,
                  )}
                >
                  Send Back For Analysis
                </MenuItem>
              )}
              {actionsEnabled.includes(
                OpportunityActionEnum.CancelEndorsementRequest,
              ) &&
                isCurrentExpert && (
                  <MenuItem
                    onClick={handleSelectMenu(
                      OpportunityActionEnum.CancelEndorsementRequest,
                    )}
                  >
                    Cancel Endorsement Request
                  </MenuItem>
                )}
            </Menu>
            <Button
              aria-controls="action-menu"
              aria-haspopup="true"
              onClick={openMenu}
              endIcon={<ExpandMoreIcon />}
              variant="outlined"
              color="primary"
            >
              Endorsement Option
            </Button>
          </Box>
        </>
      )}
      {data.length > 0 && (
        <List>
          {data.map((item, index) => (
            <ListItem
              key={item.id}
              alignItems="flex-start"
              divider
              className={classes.listItem}
            >
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  {item.user?.name.charAt(0) ?? ''}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={
                  <>
                    <Link
                      component={RouterLink}
                      color="inherit"
                      style={{ fontWeight: 'bold' }}
                      to={
                        item.user?.id === userProfileState.id
                          ? '/profile'
                          : `/profile/${item.user?.id}`
                      }
                    >
                      {item.user?.name}
                    </Link>
                    <Box pl="5px" display="inline-block">
                      <Typography component="span">
                        {item.activity_definition.title}
                      </Typography>
                    </Box>
                  </>
                }
                secondary={
                  <div>
                    {item.date !== '0001-01-01T00:00:00Z' && (
                      <Typography component="i" display="block">
                        {moment(item.date).fromNow()}
                      </Typography>
                    )}
                    {item.activity_message && (
                      <Box mt={0.5} mb={0.5}>
                        <ChatOutlinedIcon className={classes.icon} />
                        <Typography
                          component="span"
                          variant="body2"
                          color="textPrimary"
                        >
                          {item.activity_message}
                        </Typography>
                      </Box>
                    )}
                  </div>
                }
              />
            </ListItem>
          ))}
        </List>
      )}
    </Grid>
  );
};

export default SeekEndorsementPanel;
const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 13,
    marginRight: 6,
    marginTop: 2,
  },
  listItem: {
    paddingLeft: 0,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    marginLeft: 8,
  },
}));
