import React, { useCallback, useContext, useMemo, useState } from 'react';
import { ExpansionPanelDetails, Typography } from '@material-ui/core';

import {
  addOpportunityReviewSummary,
  IOpportunitySizes,
  IOpportunityTypes,
  updateOpportunityDefinition,
  updateOpportunityProbabilitySuccess,
  updateOpportunityReviewSummary,
  updateOpportunitySize,
  updateOpportunityType,
} from '../../../../services/opportunity.services';
import {
  getOpportunitySizes,
  getOpportunityTypes,
  IOpportunitySuccessProbability,
  getOppSuccessProbability,
} from '../../../../services/references.services';
import { DataSourcePanel } from './DataSourcePanel';
import loadable from '@loadable/component';
import {
  ExpansionPanelStyled,
  ExpansionPanelSummaryStyled,
} from '../../../../components/styled';
import {
  useEffectOnlyOnce,
  useOpportunityRoute,
  useUnmounted,
} from '../../../../hooks';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useSelector } from 'react-redux';
import { OpportunityDefinitionPanel } from './OpportunityDefinitionPanel';
import { OpportunityPhase } from '../../../../constants/opportunityPhase';
import { useTranslation } from 'react-i18next';
import { MixTitle } from '../../../../components';
import { UserProfileState } from '../../../../store/user/selector';

import {
  OpportunityState,
  IsAdminOpportunityState,
} from '../../../../store/opportunity/selector';
import OpportunityContext from '../../OpportunityContext';

enum Accordions {
  OpportunityDefinition = 1,
  DataSource,
  DefineTypeSize,
  Roles,
  Summary,
  PriorityScore,
  Endorsement,
}

const PHASE_SHAPE: number = 2;
const STATE_EXPERT_REVIEW: number = 2;

const OpportunityParameters = loadable(() => import('./OpportunityParameters'));
const ResourceRequirements = loadable(() => import('./ResourceRequirements'));
const SummaryPanel = loadable(() => import('./SummaryPanel'));
const SeekEndorsementPanel = loadable(() => import('../SeekEndorsementPanel'));
const PriorityScorePanel = loadable(() => import('./PriorityScorePanel'));

const ShapeTab: React.FC = React.memo(() => {
  const { skillList, locationList, teamList, roleList, onRefresh } = useContext(
    OpportunityContext,
  );

  const userProfileState = useSelector(UserProfileState);
  const opportunity = useSelector(OpportunityState);
  const isAdminOpportunity = useSelector(IsAdminOpportunityState);
  const { tab, panel } = useOpportunityRoute();
  const isUnmounted = useUnmounted();
  const [loading, setLoading] = useState(true);
  const [expanded, setExpanded] = useState<Accordions | undefined>(() => {
    return tab === OpportunityPhase.Shape ? (panel as Accordions) : undefined;
  });
  const [opportunityTypes, setOpportunityTypes] = useState<
    IOpportunityTypes[]
  >();
  const [opportunitySizes, setOpportunitySizes] = useState<
    IOpportunitySizes[]
  >();
  const [successProbabilities, setSuccessProbabilities] = useState<
    IOpportunitySuccessProbability[]
  >();

  useEffectOnlyOnce(() => {
    Promise.all([
      getOpportunityTypeLists(),
      getOpportunitySizeLists(),
      getSuccessProbabilityList(),
    ])
      .then(() => setLoading(false))
      .catch(() => setLoading(false));
  });

  const onChangePanel = (name: Accordions) => () => {
    setExpanded(expanded !== name ? name : undefined);
  };

  const renderExpansionIcon = (current: Accordions) => {
    return current === expanded ? <RemoveCircleIcon /> : <AddCircleIcon />;
  };

  const getOpportunityTypeLists = async () => {
    const response = await getOpportunityTypes();
    if (isUnmounted.current) return;
    if (response.success) setOpportunityTypes(response.data.list);
  };

  const getOpportunitySizeLists = async () => {
    const response = await getOpportunitySizes();
    if (isUnmounted.current) return;
    if (response.success) setOpportunitySizes(response.data.list);
  };

  const getSuccessProbabilityList = async () => {
    const json = await getOppSuccessProbability();
    if (isUnmounted.current) return;
    if (json.success) setSuccessProbabilities(json.data.list);
  };

  const onSubmitSizeType = useCallback(
    async (size: number, type: number, successProbability: number) => {
      if (!opportunity) return false;
      if (opportunity.opportunity_size?.id !== size) {
        await updateOpportunitySize(opportunity.id, size);
      }
      if (opportunity.opportunity_type?.id !== type) {
        await updateOpportunityType(opportunity.id, type);
      }
      if (opportunity.probability_success.id !== successProbability) {
        await updateOpportunityProbabilitySuccess(
          opportunity.id,
          successProbability,
        );
      }
      onRefresh();
    },
    [onRefresh, opportunity],
  );

  const onSubmitReviewSummary = useCallback(
    async (content: string, update?: boolean) => {
      if (!opportunity) return false;
      if (!update) {
        await addOpportunityReviewSummary(opportunity.id, content);
      } else {
        await updateOpportunityReviewSummary(opportunity.id, content);
      }
      onRefresh();
    },
    [onRefresh, opportunity],
  );

  const onSubmitDefinition = useCallback(
    async (content: string) => {
      if (!opportunity) return;
      await updateOpportunityDefinition(opportunity.id, content);
      onRefresh();
    },
    [onRefresh, opportunity],
  );

  const editable = useMemo(() => {
    if (isAdminOpportunity) {
      return true;
    }
    const editablePhases = ['shape', 'analyse'];
    if (
      opportunity?.opportunity_phase?.name &&
      editablePhases.includes(opportunity.opportunity_phase.name) &&
      opportunity?.current_expert_review?.expert_review?.id ===
        userProfileState.id
    ) {
      return true;
    }
    return !!opportunity?.current_experts?.find(
      expert => expert.user_id === userProfileState?.id,
    );
  }, [isAdminOpportunity, opportunity, userProfileState]);
  const { t } = useTranslation();

  return (
    <>
      {/*Start Opportunity Definition*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.OpportunityDefinition}
        onChange={onChangePanel(Accordions.OpportunityDefinition)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="data-source-content"
          id="data-source-header"
          expandIcon={renderExpansionIcon(Accordions.OpportunityDefinition)}
        >
          <MixTitle
            isHelper
            title={t('frame_the_problem')}
            helperText={t('help_icon.opp_definition')}
            large
          />
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {loading ? (
            <CircularProgress />
          ) : (
            <OpportunityDefinitionPanel
              dataEditable={editable}
              onSubmit={onSubmitDefinition}
              opportunity={opportunity}
            />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Opportunity Definition*/}
      {/*Start Data Sources*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.DataSource}
        onChange={onChangePanel(Accordions.DataSource)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="data-source-content"
          id="data-source-header"
          expandIcon={renderExpansionIcon(Accordions.DataSource)}
        >
          <MixTitle
            isHelper
            title={t('dataset_required')}
            helperText={t('help_icon.datasets_required')}
            large
          />
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {loading ? (
            <CircularProgress />
          ) : (
            <DataSourcePanel dataEditable={editable} />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Data Sources*/}
      {/*Start Size & Type*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.DefineTypeSize}
        onChange={onChangePanel(Accordions.DefineTypeSize)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="size-type-content"
          id="size-type-header"
          expandIcon={renderExpansionIcon(Accordions.DefineTypeSize)}
        >
          <Typography variant="h5">{t('opp_parameters')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {loading ? (
            <CircularProgress />
          ) : (
            <OpportunityParameters
              dataEditable={editable}
              chosenOpportunity={opportunity}
              opportunitySizes={opportunitySizes}
              opportunityTypes={opportunityTypes}
              successProbabilities={successProbabilities}
              onSubmit={onSubmitSizeType}
            />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Size & Type*/}
      {/*Start Roles*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.Roles}
        onChange={onChangePanel(Accordions.Roles)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="role-content"
          id="role-header"
          expandIcon={renderExpansionIcon(Accordions.Roles)}
        >
          <Typography variant="h5">{t('resource_required')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {!loading ? (
            <ResourceRequirements
              dataEditable={editable}
              profileRole={roleList}
              locations={locationList}
              resourceTeam={teamList}
              skillList={skillList}
            />
          ) : (
            <CircularProgress />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Roles*/}
      {/*Start Review Summary*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.Summary}
        onChange={onChangePanel(Accordions.Summary)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="summary-content"
          id="summary-header"
          expandIcon={renderExpansionIcon(Accordions.Summary)}
        >
          <MixTitle
            isHelper
            title={t('review_summary')}
            helperText={t('help_icon.review_summary')}
            large
          />
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {loading ? (
            <CircularProgress />
          ) : (
            <SummaryPanel
              dataEditable={editable}
              opportunity={opportunity}
              onSubmit={onSubmitReviewSummary}
            />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Review Summary*/}
      {/*Start Priority Score*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.PriorityScore}
        onChange={onChangePanel(Accordions.PriorityScore)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="data-source-content"
          id="data-source-header"
          expandIcon={renderExpansionIcon(Accordions.PriorityScore)}
        >
          <MixTitle
            isHelper
            title={t('priority_score')}
            helperText={t('help_icon.priority_or_score')}
            large
          />
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {loading ? (
            <CircularProgress />
          ) : (
            <PriorityScorePanel
              score={opportunity?.opp_priority_score?.priority_score ?? 0}
              note={opportunity?.opp_priority_score?.priority_score_updated_log}
              lastUpdate={
                opportunity?.opp_priority_score?.priority_score_updated_at
              }
            />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Priority Score*/}
      {/*Start Seek Endorsement*/}
      <ExpansionPanelStyled
        expanded={expanded === Accordions.Endorsement}
        onChange={onChangePanel(Accordions.Endorsement)}
        disabled={
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= PHASE_SHAPE &&
          opportunity?.opportunity_state &&
          opportunity?.opportunity_state?.id < STATE_EXPERT_REVIEW
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="endorsement-content"
          id="endorsement-header"
          expandIcon={renderExpansionIcon(Accordions.Endorsement)}
        >
          <MixTitle
            isHelper
            title={t('seek_endorsement')}
            helperText={t('help_icon.seek_endorsement')}
            large
          />
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          {loading ? (
            <CircularProgress />
          ) : (
            <SeekEndorsementPanel
              isCurrentExpert={editable}
              phases={['shape']}
              currentTabPhase={OpportunityPhase.Shape}
            />
          )}
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      {/*End Seek Endorsement*/}
    </>
  );
});
export default ShapeTab;
