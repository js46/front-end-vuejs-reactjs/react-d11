import React, { useState } from 'react';
import { Box, FormControl, Grid, TextField } from '@material-ui/core';
import { useToggleValue, useDisableMultipleClick } from '../../../../hooks';
import { PanelActionButton } from '../PanelActionButton';
import { IOpportunity } from '../../../../services/opportunity.services';

interface OpportunityDefinitionProps {
  dataEditable: boolean;
  onSubmit: (content: string) => void;
  opportunity?: IOpportunity;
}

export const OpportunityDefinitionPanel: React.FC<OpportunityDefinitionProps> = ({
  dataEditable,
  onSubmit,
  opportunity,
}) => {
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [editable, toggleEditable] = useToggleValue(false);
  const [content, setContent] = useState(opportunity?.definition ?? '');

  const handleSubmit = async () => {
    if (isSubmitting) return;
    await debounceFn();
    onSubmit(content);
    toggleEditable();
    endRequest();
  };

  return (
    <Grid container>
      <Box mt={2} width="100%">
        {dataEditable && (
          <PanelActionButton
            enableEdit={editable}
            onToggleEdit={toggleEditable}
            onSubmit={handleSubmit}
            disabled={isSubmitting}
          />
        )}

        <Box mt={2} flex={1}>
          <Grid container>
            <Grid container>
              <Grid item xs>
                <FormControl
                  required
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  disabled={!editable}
                >
                  <TextField
                    value={content}
                    multiline
                    rows={12}
                    onChange={e => setContent(e.target.value)}
                    variant="outlined"
                    disabled={!editable}
                    id="content"
                    name="content"
                    required
                    autoFocus
                    // helperText={touched.description && errors.description}
                    // error={touched.description && !!errors.description}
                  />
                </FormControl>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Grid>
  );
};
