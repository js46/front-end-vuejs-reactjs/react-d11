import React, { useState } from 'react';
import {
  Box,
  FormControl,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';

import { PanelActionButton } from '../PanelActionButton';
import { useToggleValue, useDisableMultipleClick } from '../../../../hooks';
import {
  IOpportunity,
  IOpportunityReviewSummary,
} from '../../../../services/opportunity.services';
import { useTranslation } from 'react-i18next';

interface SummaryPanelProps {
  dataEditable: boolean;
  opportunity?: IOpportunity;
  onSubmit: (content: string, update?: boolean) => void;
}
const SummaryPanel: React.FC<SummaryPanelProps> = props => {
  const { dataEditable, opportunity, onSubmit } = props;
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const [editable, toggleEditable] = useToggleValue(false);
  const [summary, setSummary] = useState(
    opportunity?.review_summary.review_content || '',
  );
  const { t } = useTranslation();

  if (!opportunity) return null;

  const reviewSummary: IOpportunityReviewSummary = opportunity.review_summary;

  const handleSubmit = async () => {
    if (!summary || isSubmitting) return false;
    await debounceFn();
    onSubmit(summary, reviewSummary.id > 0);
    toggleEditable();
    endRequest();
  };

  return (
    <Box mt={2} display="flex" flex={1}>
      <Grid container>
        {dataEditable && (
          <PanelActionButton
            enableEdit={editable}
            onToggleEdit={toggleEditable}
            onSubmit={handleSubmit}
            disabled={isSubmitting}
          />
        )}

        <Grid item xs={12}>
          <Box mt={2}>
            {(dataEditable || reviewSummary.id > 0) && (
              <Box>
                <FormControl fullWidth>
                  <TextField
                    value={summary}
                    onChange={e => setSummary(e.target.value)}
                    rows={12}
                    variant="outlined"
                    multiline
                    disabled={!editable}
                    id="review_summary"
                    name="review_summary"
                    required
                    autoFocus
                    // helperText={touched.description && errors.description}
                    // error={touched.description && !!errors.description}
                  />
                </FormControl>
              </Box>
            )}
            {!reviewSummary.id && !dataEditable && (
              <Box ml={2}>
                <Typography component="span">
                  {t('no_review_summary')}
                </Typography>
              </Box>
            )}
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
export default SummaryPanel;
