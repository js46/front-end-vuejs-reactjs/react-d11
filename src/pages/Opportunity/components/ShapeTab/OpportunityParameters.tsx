import React from 'react';
import {
  IOpportunity,
  IOpportunitySizes,
  IOpportunityTypes,
} from '../../../../services/opportunity.services';
import { Box, FormControl, Grid } from '@material-ui/core';
import { CustomSelect, MixTitle } from '../../../../components';
import { PanelActionButton } from '../PanelActionButton';
import { useToggleValue, useDisableMultipleClick } from '../../../../hooks';
import { Formik } from 'formik';
import { IOpportunitySuccessProbability } from '../../../../services/references.services';
import { useTranslation } from 'react-i18next';

interface SizeTypePanelProps {
  dataEditable: boolean;
  chosenOpportunity?: IOpportunity;
  opportunityTypes?: IOpportunityTypes[];
  opportunitySizes?: IOpportunitySizes[];
  successProbabilities?: IOpportunitySuccessProbability[];
  onSubmit: (size: number, type: number, success: number) => Promise<any>;
}

const SizeTypePanel: React.FC<SizeTypePanelProps> = props => {
  const {
    dataEditable,
    chosenOpportunity,
    opportunitySizes,
    opportunityTypes,
    successProbabilities,
    onSubmit,
  } = props;
  const [editable, toggleEditable] = useToggleValue(false);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const { t } = useTranslation();
  return (
    <Formik
      enableReinitialize
      initialValues={{
        opportunity_type: chosenOpportunity?.opportunity_type?.id || 0,
        opportunity_size: chosenOpportunity?.opportunity_size?.id || 0,
        probability_success: chosenOpportunity?.probability_success.id || 0,
      }}
      onSubmit={async values => {
        if (isSubmitting) return;
        await debounceFn();
        await onSubmit(
          values.opportunity_size,
          values.opportunity_type,
          Number(values.probability_success),
        );
        toggleEditable();
        endRequest();
      }}
    >
      {({ handleSubmit, values, handleChange }) => (
        <Box mt={2} flex={1}>
          <Grid container spacing={1}>
            {dataEditable && (
              <PanelActionButton
                enableEdit={editable}
                onToggleEdit={toggleEditable}
                onSubmit={handleSubmit}
                disabled={isSubmitting}
              />
            )}
            <Grid item xs sm={4}>
              <MixTitle
                isHelper
                title={t('opp_approach')}
                helperText={t('help_icon.opp_type')}
              />
              <FormControl
                required
                fullWidth
                margin="normal"
                variant="outlined"
                disabled={!editable}
              >
                <CustomSelect
                  value={values.opportunity_type || ''}
                  onChange={handleChange}
                  id="opportunity_type"
                  name="opportunity_type"
                  options={
                    opportunityTypes
                      ?.filter(item => !item.archive)
                      .map(item => ({
                        id: item.id!,
                        name: item.name!,
                      })) || []
                  }
                />
              </FormControl>
            </Grid>
            <Grid item sm={4}>
              <MixTitle
                title={t('opp_size')}
                isHelper
                helperText={t('help_icon.opp_size')}
              />
              <FormControl
                required
                fullWidth
                margin="normal"
                variant="outlined"
                disabled={!editable}
              >
                <CustomSelect
                  value={values.opportunity_size || ''}
                  onChange={handleChange}
                  id="opportunity_size"
                  name="opportunity_size"
                  showLabelForOption
                  options={
                    opportunitySizes?.map(item => ({
                      id: item.id!,
                      name: item.t_shirt_size!,
                      duration: item.duration,
                    })) || []
                  }
                />
              </FormControl>
            </Grid>
            <Grid item xs sm={4}>
              <MixTitle
                title={t('confidence_level')}
                isHelper
                helperText={t('help_icon.success_probability')}
              />
              <FormControl
                required
                fullWidth
                margin="normal"
                variant="outlined"
                disabled={!editable}
              >
                <CustomSelect
                  value={values.probability_success || ''}
                  onChange={handleChange}
                  id="probability_success"
                  name="probability_success"
                  options={
                    successProbabilities?.map(item => ({
                      id: item.id,
                      name: item.band,
                    })) || []
                  }
                />
              </FormControl>
            </Grid>
          </Grid>
        </Box>
      )}
    </Formik>
  );
};

export default SizeTypePanel;
