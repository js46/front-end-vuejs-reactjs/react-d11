import React, { useCallback, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Box, Button, FormControl, Grid } from '@material-ui/core';
import { FieldArray, Formik } from 'formik';
import {
  useDisableMultipleClick,
  useEffectOnlyOnce,
  useToggleValue,
} from '../../../../hooks';
import { PanelActionButton } from '../PanelActionButton';
import { updateDatasetInOpportunity } from '../../../../services/opportunity.services';
import {
  getDataSets,
  IDataSet,
} from '../../../../services/references.services';
import { makeStyles } from '@material-ui/core/styles';
import { CustomSelect } from '../../../../components';
import { useSelector, useDispatch } from 'react-redux';
import { OpportunityState } from '../../../../store/opportunity/selector';
import { RequestDetailOpportunity } from '../../../../store/opportunity/actions';

interface DataSourcePanelProps {
  dataEditable: boolean;
}

export const DataSourcePanel: React.FC<DataSourcePanelProps> = ({
  dataEditable,
}) => {
  const [editable, toggleEditable] = useToggleValue(false);
  const opportunity = useSelector(OpportunityState);
  const dispatch = useDispatch();
  const { id } = useParams();
  const { debounceFn, endRequest, isSubmitting } = useDisableMultipleClick();

  const [datasets, setDatasets] = useState<IDataSet[]>([]);
  useEffectOnlyOnce(() => {
    getDatasets();
  });
  const onRefresh = () => {
    dispatch(RequestDetailOpportunity(id));
  };
  const getDatasets = useCallback(async () => {
    const json = await getDataSets();
    if (json.success) setDatasets(json.data.list);
  }, []);

  const renderDatasetDescription = (id: number) => {
    const description = datasets.find(item => item.id === id)?.description;
    if (!description) return null;
    return (
      <Grid container item>
        <Grid item xs>
          <Box component="p" mb={3}>
            {description}
          </Box>
        </Grid>
      </Grid>
    );
  };
  const classes = useStyles();

  // @ts-ignore
  // @ts-ignore
  return (
    <Formik
      enableReinitialize
      initialValues={{
        datasets: opportunity?.data_sets?.map(item => item.id) ?? [0],
      }}
      onSubmit={async values => {
        if (!opportunity || isSubmitting) return;
        await debounceFn();
        await updateDatasetInOpportunity(opportunity.id, {
          data_sets: values.datasets,
        });
        endRequest();
        toggleEditable();
        setTimeout(() => onRefresh(), 500);
      }}
    >
      {({ values, handleChange, handleBlur, handleSubmit }) => (
        <FieldArray name="datasets">
          {({ push, remove }) => (
            <Grid container>
              <Box mt={2} width="100%">
                {dataEditable && (
                  <PanelActionButton
                    enableEdit={editable}
                    onToggleEdit={toggleEditable}
                    onSubmit={handleSubmit}
                  />
                )}

                <Box mt={2} mb={2} flex={1}>
                  {values.datasets.map((selected: number, index: number) => (
                    <Grid container key={index}>
                      <Grid container item spacing={3}>
                        <Grid item xs={10} className={classes.dflex}>
                          <FormControl
                            required
                            fullWidth
                            margin="none"
                            variant="outlined"
                            disabled={!editable}
                          >
                            <CustomSelect
                              options={datasets.filter(
                                item =>
                                  item.id === selected ||
                                  !values.datasets.includes(item.id),
                              )}
                              name={`datasets.${index}`}
                              onBlur={handleBlur}
                              value={selected}
                              onChange={handleChange}
                            />
                          </FormControl>
                        </Grid>
                        {editable && (
                          <Grid item xs={2} md={1} className={classes.dflex}>
                            <Button
                              className={classes.removeBtn}
                              variant="outlined"
                              disabled={isSubmitting}
                              onClick={() => remove(index)}
                            >
                              Remove
                            </Button>
                          </Grid>
                        )}
                      </Grid>
                      {renderDatasetDescription(selected)}
                    </Grid>
                  ))}
                  {dataEditable && (
                    <Grid container>
                      <Grid item xs>
                        <Box mt={2} display="flex" justifyContent="center">
                          <Button
                            disabled={
                              !editable ||
                              isSubmitting ||
                              values.datasets.includes(0)
                            }
                            variant="outlined"
                            onClick={() => push(0)}
                          >
                            Add
                          </Button>
                        </Box>
                      </Grid>
                    </Grid>
                  )}
                </Box>
              </Box>
            </Grid>
          )}
        </FieldArray>
      )}
    </Formik>
  );
};
const useStyles = makeStyles({
  dflex: {
    display: 'flex',
    alignItems: 'center',
  },
  removeBtn: {
    marginLeft: '10px',
    padding: '0 20px',
    textTransform: 'capitalize',
  },
});
