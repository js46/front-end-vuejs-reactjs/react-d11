import React, { useState, useContext } from 'react';
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';
import { SelectInRole } from '../SelectInRole';
import { SelectInSkill } from '../SelectInSkill';
import { FieldArray, Formik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import {
  IListLocations,
  IListRole,
  ISkillsInRole,
} from '../../../../services/profile.services';
import { IResourceTeam } from '../../../../services/references.services';
import { useDisableMultipleClick } from '../../../../hooks';
import { DeleteDialog } from '../../../../components';
import { useTranslation } from 'react-i18next';
import { MixTitle } from '../../../../components';
import { useSelector } from 'react-redux';
import OpportunityContext from '../../OpportunityContext';
import {
  addOpportunityRole,
  deleteOpportunityRole,
  IOpportunityInRole,
  updateOpportunityRoles,
} from '../../../../services/opportunity.services';
import {
  ResourceRequirementState,
  idOpportunityState,
} from '../../../../store/opportunity/selector';

interface RolePanelProps {
  dataEditable: boolean;
  profileRole?: IListRole[];
  locations?: IListLocations[];
  resourceTeam?: IResourceTeam[];
  skillList?: ISkillsInRole[];
}
const useStyles = makeStyles(theme => ({
  subMenuInRole: {
    fontWeight: 'bold',
  },
  divider: {
    backgroundColor: '#bbb',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  textRoleName: {
    fontWeight: 'bold',
  },
}));

const ResourceRequirements: React.FC<RolePanelProps> = React.memo(props => {
  const {
    dataEditable,
    profileRole,
    locations,
    resourceTeam,
    skillList,
  } = props;
  const classes = useStyles();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [editFields, setEditFields] = useState<number[]>([]);
  const [editable, setEditable] = useState<boolean>(false);
  const [openDeleteDialog, setDeleteDialog] = useState<boolean>(false);
  const [resourceRequireId, SetResourceRequireId] = useState<number>(0);
  const { roleList, onRefresh } = useContext(OpportunityContext);

  const resourceRequirements = useSelector(ResourceRequirementState);

  const opportunityID = useSelector(idOpportunityState);

  const onSubmitRole = async (
    action: 'ADD' | 'UPDATE' | 'REMOVE' | 'CANCEL',
    data?: IOpportunityInRole[],
    index?: number,
  ) => {
    switch (action) {
      case 'UPDATE': {
        if (typeof index === 'number' && data) {
          const profileRoleSelected = roleList?.find(role => {
            return role?.id === data[index]?.profile_role?.id;
          });
          var profileRole = {
            id: data[index].id,
            role_name: profileRoleSelected
              ? profileRoleSelected.name
              : data[index].role_name,
            profile_role_id: data[index].profile_role?.id,
            profile_role_level_id: data[index].profile_role_level_id,
            day: data[index].day,
            effort: Number(data[index].effort),
            location_id: data[index]?.location?.id,
            resource_team_id: data[index]?.resource_team?.id,
            opportunity_role_skills:
              data[index].skills?.map(item => ({
                skill_id: item.skill_id,
                proficiency_level_id: item.proficiency_level_id,
              })) ?? [],
          };
          if (profileRole.id) {
            await updateOpportunityRoles(opportunityID, profileRole);
          } else {
            await addOpportunityRole(opportunityID, profileRole);
          }

          onRefresh();
        }
        break;
      }

      case 'REMOVE': {
        if (index) {
          await deleteOpportunityRole(opportunityID, {
            id: index,
          });
          onRefresh();
        }
        break;
      }
    }
  };

  const onSave = (
    action: 'ADD' | 'UPDATE',
    data?: IOpportunityInRole[],
    index?: number,
  ) => async () => {
    if (isSubmitting) return;
    await debounceFn();
    await onSubmitRole(
      action,
      data?.map(item => {
        return { ...item, day: Number(item?.day) };
      }),
      index,
    );
    setEditable(false);
    typeof index === 'number' && toggleEditFields(index)();
    endRequest();
  };
  const actionRole = (isEdit: boolean, index: number) => {
    setEditable(isEdit);

    setTimeout(() => {
      toggleEditFields(index)();
    }, 500);

    //onRefreshLocal(!isEdit);
    return;
  };
  const handleDeleteResourceRequired = async () => {
    if (isSubmitting) return;
    await debounceFn();
    await onSubmitRole('REMOVE', [], resourceRequireId);
    endRequest();
    setDeleteDialog(false);
  };

  const toggleEditFields = (index: number) => () => {
    setEditFields(prevState =>
      prevState.includes(index)
        ? prevState.filter(item => item !== index)
        : [...prevState, index],
    );
  };
  const { t, i18n } = useTranslation();

  return (
    <>
      <Formik
        enableReinitialize
        initialValues={{
          opportunity_roles: resourceRequirements,
        }}
        onSubmit={values => {}}
      >
        {({ values, handleChange, setFieldValue, setValues }) => (
          <FieldArray
            name="opportunity_roles"
            render={arrayHelpers => {
              return (
                <>
                  <Grid item xs={12}>
                    <Box mb={2} mt={2}>
                      {values.opportunity_roles.length > 0 &&
                        values.opportunity_roles.map((role, index) => {
                          const disabled = !editFields.includes(index);
                          return (
                            <Grid container key={role.id}>
                              <Grid container>
                                <Grid item xs={12}>
                                  <Box display="flex" justifyContent="flex-end">
                                    {values.opportunity_roles && dataEditable && (
                                      <Box>
                                        {disabled ? (
                                          <>
                                            <Button
                                              variant="outlined"
                                              color="secondary"
                                              onClick={() => {
                                                SetResourceRequireId(
                                                  role.id ?? 0,
                                                );
                                                setDeleteDialog(true);
                                              }}
                                              disabled={isSubmitting}
                                            >
                                              Remove
                                            </Button>
                                            <Box ml={1} component="span">
                                              <Button
                                                variant="outlined"
                                                color="primary"
                                                onClick={() =>
                                                  actionRole(true, index)
                                                }
                                              >
                                                Edit
                                              </Button>
                                            </Box>
                                          </>
                                        ) : (
                                          <Box mr={1} component="span">
                                            <Button
                                              variant="outlined"
                                              color="default"
                                              onClick={() => {
                                                actionRole(false, index);
                                                if (!role?.id) {
                                                  arrayHelpers.remove(index);
                                                }
                                                setValues({
                                                  opportunity_roles: resourceRequirements,
                                                });
                                              }}
                                              disabled={isSubmitting}
                                            >
                                              Cancel
                                            </Button>
                                          </Box>
                                        )}
                                      </Box>
                                    )}
                                    {!disabled && (
                                      <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={onSave(
                                          'UPDATE',
                                          values.opportunity_roles,
                                          index,
                                        )}
                                        children={'Save'}
                                        disabled={
                                          isSubmitting ||
                                          !role?.day ||
                                          !role.effort ||
                                          !role.profile_role ||
                                          Number(role?.day) <
                                            Number(role?.effort)
                                        }
                                      />
                                    )}
                                  </Box>
                                </Grid>
                              </Grid>
                              <SelectInRole
                                name="Role"
                                field={`opportunity_roles[${index}].profile_role.id`}
                                fieldForProfileRole={`opportunity_roles[${index}].profile_role_level_id`}
                                item={role?.profile_role}
                                profileRoleLevelId={role?.profile_role_level_id}
                                profileRole={profileRole}
                                hasRoleProfiency
                                handleChange={handleChange}
                                //event required in StyledRating
                                handleChangeRating={(event, value) => {
                                  setFieldValue(
                                    `opportunity_roles[${index}].profile_role_level_id`,
                                    value,
                                  );
                                }}
                                disabled={disabled}
                                helperText={t('help_icon.role')}
                              />
                              <Grid container alignItems="center">
                                <Grid item xs={6}>
                                  <Box mt={1} flex={1}>
                                    <Grid container item direction="row">
                                      <Grid
                                        xs={5}
                                        item
                                        container
                                        justify="flex-end"
                                      >
                                        <Box component="span" mt={1} mr={2}>
                                          <MixTitle
                                            title="Duration"
                                            isHelper
                                            helperText={t('help_icon.duration')}
                                          />
                                        </Box>
                                      </Grid>
                                      <Grid item xs={7}>
                                        <FormControl
                                          fullWidth
                                          variant="outlined"
                                          margin="normal"
                                          disabled={disabled}
                                        >
                                          <TextField
                                            type="text"
                                            name={`opportunity_roles[${index}].day`}
                                            value={role.day}
                                            fullWidth
                                            variant="outlined"
                                            onChange={event => {
                                              setFieldValue(
                                                `opportunity_roles[${index}].day`,
                                                event.target.value.replace(
                                                  /[^0-9]/gm,
                                                  '',
                                                ),
                                              );
                                            }}
                                            disabled={disabled}
                                          />
                                        </FormControl>
                                      </Grid>
                                    </Grid>
                                  </Box>
                                </Grid>
                              </Grid>
                              <Grid container alignItems="center">
                                <Grid item xs={6}>
                                  <Box mt={1} flex={1}>
                                    <Grid container item direction="row">
                                      <Grid
                                        xs={5}
                                        item
                                        container
                                        justify="flex-end"
                                      >
                                        <Box component="span" mt={1} mr={2}>
                                          <MixTitle
                                            title="Effort"
                                            isHelper
                                            helperText={t('help_icon.effort')}
                                          />
                                        </Box>
                                      </Grid>
                                      <Grid item xs={7}>
                                        <FormControl
                                          fullWidth
                                          variant="outlined"
                                          margin="normal"
                                          disabled={disabled}
                                        >
                                          <TextField
                                            type="text"
                                            name={`opportunity_roles[${index}].effort`}
                                            value={role.effort}
                                            fullWidth
                                            variant="outlined"
                                            onChange={event => {
                                              setFieldValue(
                                                `opportunity_roles[${index}].effort`,
                                                event.target.value.replace(
                                                  /[^0-9]/gm,
                                                  '',
                                                ),
                                              );
                                            }}
                                            disabled={disabled}
                                          />
                                        </FormControl>
                                      </Grid>
                                    </Grid>
                                  </Box>
                                </Grid>
                                <Grid item xs={6}>
                                  <Box display="flex" pl={3}>
                                    <Typography
                                      variant="caption"
                                      style={{ color: 'red' }}
                                    >
                                      {Number(role.day) < Number(role.effort) &&
                                        t(
                                          'effort_should_less_than_or_qual_duration',
                                        )}
                                    </Typography>
                                  </Box>
                                </Grid>
                              </Grid>
                              <SelectInRole
                                name="Location"
                                field={`opportunity_roles[${index}].location.id`}
                                item={role?.location}
                                locations={locations}
                                optionNone
                                handleChange={e => handleChange(e)}
                                disabled={disabled}
                                helperText={t('help_icon.location')}
                              />
                              <SelectInRole
                                name="Resource Team"
                                field={`opportunity_roles[${index}].resource_team.id`}
                                item={role?.resource_team}
                                optionNone
                                resourceTeam={resourceTeam}
                                handleChange={e => handleChange(e)}
                                disabled={disabled}
                                helperText={t('help_icon.resource_team')}
                              />
                              <FormControl fullWidth>
                                <FieldArray
                                  name={`opportunity_roles.${index}.skills`}
                                  render={skillHelper => (
                                    <Box>
                                      <Box>
                                        {role?.skills?.map(
                                          (skill, indexSkill) => (
                                            <SelectInSkill
                                              key={index + '-' + indexSkill}
                                              name={`Skill ${indexSkill + 1}`}
                                              field={`opportunity_roles[${index}].skills[${indexSkill}].skill_id`}
                                              item={skill}
                                              handleChange={e =>
                                                handleChange(e)
                                              }
                                              //event required in StyledRating
                                              handleChangeRatingInSkill={(
                                                event,
                                                value,
                                              ) => {
                                                setFieldValue(
                                                  `opportunity_roles[${index}].skills[${indexSkill}].proficiency_level_id`,
                                                  value,
                                                );
                                              }}
                                              index={indexSkill}
                                              skillList={skillList}
                                              disabled={disabled}
                                              removable
                                              handleRemove={() =>
                                                skillHelper.remove(indexSkill)
                                              }
                                            />
                                          ),
                                        )}
                                      </Box>
                                      {!disabled && (
                                        <Grid container>
                                          <Grid item xs={6}>
                                            <Grid container>
                                              <Grid xs={5} item />
                                              <Grid item xs={7}>
                                                <Box
                                                  justifyContent="center"
                                                  display="flex"
                                                  mt={1}
                                                >
                                                  <Button
                                                    variant="outlined"
                                                    onClick={() => {
                                                      skillHelper.push({
                                                        id: index + 1,
                                                        proficiency_level_id: 1,
                                                        skill_id: 1,
                                                        skill_name:
                                                          'Problem Solving-1',
                                                      });
                                                    }}
                                                  >
                                                    Add skills
                                                  </Button>
                                                </Box>
                                              </Grid>
                                            </Grid>
                                          </Grid>
                                        </Grid>
                                      )}
                                    </Box>
                                  )}
                                />
                              </FormControl>

                              <Grid item xs={12}>
                                <Divider
                                  variant="fullWidth"
                                  className={classes.divider}
                                />
                              </Grid>
                            </Grid>
                          );
                        })}
                      <Box mb={1} display="flex" justifyContent="center">
                        {dataEditable && (
                          <Button
                            variant="contained"
                            color="primary"
                            className={classes.subMenuInRole}
                            onClick={() => {
                              arrayHelpers.push({
                                role_name: '',
                                id: 0,
                                day: 1,
                                effort: 1,
                                profile_role_level_id: 1,
                                resource_team: {
                                  id: 0,
                                  name: '',
                                },
                                profile_role: {
                                  name: '',
                                  id: 1,
                                },
                                location: {
                                  id: 0,
                                  name: '',
                                },
                              });
                              setEditable(true);
                              toggleEditFields(
                                values.opportunity_roles.length,
                              )();
                            }}
                            disabled={isSubmitting || editable}
                          >
                            Add role
                          </Button>
                        )}
                      </Box>
                    </Box>
                  </Grid>
                </>
              );
            }}
          />
        )}
      </Formik>
      <DeleteDialog
        isOpen={openDeleteDialog}
        header={i18n.t('opportunity.resource_requirement.delete_title')}
        message={
          <Box>
            <Typography component="p" variant="body1">
              {i18n.t('opportunity.resource_requirement.delete_body')}
            </Typography>
          </Box>
        }
        handleClose={() => setDeleteDialog(false)}
        handleDelete={handleDeleteResourceRequired}
      />
    </>
  );
});
export default ResourceRequirements;
