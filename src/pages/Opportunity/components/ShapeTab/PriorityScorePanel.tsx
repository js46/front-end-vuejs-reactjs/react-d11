import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import moment from 'moment';
import { MixTitle } from '../../../../components';

const LOW = 'Low';
const MEDIUM = 'Medium';
const HIGH = 'High';

interface PriorityScoreProps {
  score: number;
  note?: string;
  lastUpdate?: string;
}
const PriorityScorePanel: React.FC<PriorityScoreProps> = ({
  score,
  note,
  lastUpdate,
}) => {
  const updateTime =
    lastUpdate && lastUpdate !== '0001-01-01T00:00:00Z'
      ? moment(lastUpdate).format('DD/MM/YYYY h:ma')
      : '';
  return (
    <Box mt={1.5} width="100%">
      <Grid container>
        <Grid item xs>
          <Box display="flex">
            <MixTitle title={'Priority score:'} />
            <Box ml={1}>
              <Typography variant="body1" children={`${score}/100`} />
            </Box>
            <Box ml={1}>
              <Typography
                variant="body2"
                style={{ fontStyle: 'italic', fontWeight: 'bold' }}
                children={`( ${
                  score < 50 ? LOW : score < 80 ? MEDIUM : HIGH
                } )`}
              />
            </Box>
          </Box>
          <Box display="flex">
            <MixTitle title={'Last Activity:'} />
            <Box ml={1}>
              <Typography variant="body1" children={note} />
            </Box>
          </Box>
          <Box display="flex">
            <MixTitle title={'Last Update:'} />
            <Box ml={1}>
              <Typography variant="body1" children={updateTime} />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default PriorityScorePanel;
