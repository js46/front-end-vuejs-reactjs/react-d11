import React, { useState } from 'react';
import {
  Box,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';

import { useEffectOnlyOnce } from '../../../hooks';
import { IOpportunity } from '../../../services/opportunity.services';
import { ConfigESIndex } from '../../../store/config/selector';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';

const useStyles = makeStyles(theme => ({
  listItem: {
    paddingLeft: 0,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    marginRight: 8,
  },
  icon: {
    fontSize: 12,
    marginRight: 6,
    marginTop: 2,
  },
  email: {
    color: theme.palette.grey[900],
    fontWeight: 700,
  },
  content: {
    color: theme.palette.grey[700],
    lineHeight: '16px',
    whiteSpace: 'pre-wrap',
  },
  container: {
    overflowY: 'scroll',
  },
  itemAvatar: {
    minWidth: 30,
  },
}));

function ActivityIcon() {
  return (
    <svg
      width="16px"
      height="16px"
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      fillRule="evenodd"
      clipRule="evenodd"
      imageRendering="optimizeQuality"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      viewBox="0 0 847 1058.75"
    >
      <path
        d="M423 20c223 0 404 180 404 403S646 827 423 827 20 646 20 423 200 20 423 20zM244 478c-1 6-7 11-14 11H97c-18 0-18-27 0-27h123l17-70c3-13 22-14 27-1l15 48 15-106c1-15 23-17 27-1l25 116 14-155c1-17 26-18 28-1l22 256 22-452c1-17 25-18 28-2l57 447 20-376c1-17 26-18 28-1l34 339 4-25c1-6 6-16 17-16h130c18 0 18 27 0 27H629l-19 126c-3 17-27 15-28-1l-27-270-19 349c0 17-26 17-27 0l-57-441-24 499c-1 17-26 18-28 1l-27-308-9 96c-1 16-24 17-27 2l-27-128-13 88c-2 15-23 16-26 1l-19-57-8 32z"
        className="fil0"
      ></path>
    </svg>
  );
}

export const ChatterTab: React.FC<{
  opportunity: IOpportunity;
}> = React.memo(props => {
  const { opportunity } = props;
  const classes = useStyles();
  const [lastRefresh, setLastRefresh] = useState(0);
  const ES_INDICES = useSelector(ConfigESIndex);

  const customQuery = () => {
    return {
      query: {
        bool: {
          should: [
            {
              bool: {
                must: [
                  {
                    term: { 'response_body.data.id': opportunity?.id },
                  },
                  {
                    term: {
                      object_type: 'opportunity',
                    },
                  },
                  {
                    range: {
                      'response_body.data.opportunity_phase.id': {
                        gte: 2,
                      },
                    },
                  },
                ],
                should: [
                  {
                    term: {
                      dummy: lastRefresh,
                    },
                  },
                ],
              },
            },
            {
              bool: {
                must: [
                  {
                    term: {
                      entity_id: opportunity?.id,
                    },
                  },
                  {
                    terms: {
                      entity_type: ['opportunity', 'task'],
                    },
                  },
                ],
              },
            },
          ],
        },
      },
    };
  };

  useEffectOnlyOnce(() => {
    const refresh = setInterval(() => setLastRefresh(Date.now()), 5000);
    return () => {
      clearInterval(refresh);
    };
  });

  const event_log_index = ES_INDICES.event_log_index_name_prefix + '-*';
  return (
    <ReactiveBase
      app={`${ES_INDICES.comment_index_name},${event_log_index}`}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{ Authorization: window.localStorage.getItem('jwt') }}
    >
      <ReactiveList
        componentId="chativity"
        dataField="time"
        sortBy="desc"
        loader={<></>}
        scrollOnChange={false}
        showResultStats={false}
        renderNoResults={() => null}
        defaultQuery={customQuery}
        pagination={false}
        size={1500}
        render={({ data }) => {
          return (
            <Box
              m={2}
              minHeight={20}
              maxHeight={500}
              className={classes.container}
            >
              <List>
                {data.map((item: any, index: number) => {
                  if (item._index === ES_INDICES.comment_index_name) {
                    return (
                      <EventLogItem
                        key={index}
                        id={item.id}
                        title={item.created_by?.name ?? ''}
                        content={item.comment_body}
                        time={item.time}
                        username={item.created_by?.name ?? ''}
                        isComment
                      />
                    );
                  }
                  return (
                    <EventLogItem
                      key={index}
                      id={item.id}
                      title={item.user_name ?? ''}
                      content={item.action_name}
                      time={item.time}
                      username={item.user_name ?? ''}
                    />
                  );
                })}
              </List>
            </Box>
          );
        }}
      />
    </ReactiveBase>
  );
});
type EventLogItemProps = {
  id: number;
  title: string;
  username: string;
  avatar?: string;
  content?: string;
  time: string;
  isComment?: boolean;
};
const EventLogItem: React.FC<EventLogItemProps> = props => {
  const classes = useStyles();
  const { title, username, content, time, isComment } = props;
  return (
    <ListItem alignItems="flex-start" divider className={classes.listItem}>
      <ListItemAvatar>
        <Avatar className={classes.avatar}>
          {username.charAt(0)?.toUpperCase() ?? ''}
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={
          <Box
            display="inline-block"
            padding="4px 8px"
            borderRadius="8px"
            bgcolor="#F0F2F5"
          >
            <Typography component="p" variant="body1" className={classes.email}>
              {title}
            </Typography>
            <Box display="flex" alignItems="center">
              <Box mr="6px" display="flex" alignItems="center">
                {isComment ? (
                  <ChatOutlinedIcon className={classes.icon} />
                ) : (
                  <ActivityIcon />
                )}
              </Box>
              <Typography component="p" className={classes.content}>
                {content}
              </Typography>
            </Box>
          </Box>
        }
        secondary={
          <div>
            {time !== '0001-01-01T00:00:00Z' && (
              <Box mt="4px">
                <Typography component="i" className={classes.content}>
                  {moment(time).fromNow()}
                </Typography>
              </Box>
            )}
          </div>
        }
      />
    </ListItem>
  );
};
