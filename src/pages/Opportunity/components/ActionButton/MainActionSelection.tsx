import React from 'react';
import {
  DialogContent,
  Box,
  Typography,
  FormControlLabel,
  DialogActions,
  Button,
  Radio,
  RadioGroup,
  FormControl,
} from '@material-ui/core';
import { TGroupStateAction } from '../../index';
import { OpportunityActionEnum } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { OpportunityAction } from '../../../../services/user.services';

interface MainActionSelectionProps {
  groupState: TGroupStateAction;
  setGroupState: React.Dispatch<React.SetStateAction<TGroupStateAction>>;
  handleChangeAction: (event: React.ChangeEvent<HTMLInputElement>) => void;
  listActions: OpportunityAction[];
}

export const MainActionSelection: React.FC<MainActionSelectionProps> = ({
  groupState,
  setGroupState,
  listActions,
  handleChangeAction,
}) => {
  const { t } = useTranslation();

  return (
    <React.Fragment>
      <DialogContent>
        <Box mb={2} mt={2}>
          <Typography variant="h4" component="p">
            {t('select_a_action')}
          </Typography>
        </Box>
        <FormControl>
          <RadioGroup
            aria-label="action"
            name="action"
            value={groupState.selectedAction ?? false}
            onChange={handleChangeAction}
          >
            <FormControlLabel
              key={listActions.length}
              control={<Radio color="primary" />}
              label={OpportunityActionEnum.ChangeStatusWaitingTo}
              value={OpportunityActionEnum.ChangeStatusWaitingTo}
            />
            {listActions.map((action, index) => {
              return (
                <FormControlLabel
                  key={index}
                  control={<Radio color="primary" />}
                  label={action?.display_name}
                  value={action?.display_name}
                />
              );
            })}
          </RadioGroup>
        </FormControl>
      </DialogContent>
      <DialogActions style={{ justifyContent: 'center' }}>
        <Box mt={2}>
          <Button
            color="default"
            variant="outlined"
            onClick={() => {
              setGroupState({
                ...groupState,
                isOpenActionDialog: false,
              });
            }}
          >
            Cancel
          </Button>
          <Box ml={3} display="inline-block">
            <Button
              variant="contained"
              color="primary"
              disabled={!groupState.selectedAction}
              onClick={() =>
                setGroupState({ ...groupState, isClickNext: true })
              }
            >
              Next
            </Button>
          </Box>
        </Box>
      </DialogActions>
    </React.Fragment>
  );
};
