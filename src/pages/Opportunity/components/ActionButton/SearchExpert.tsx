import React, { useState } from 'react';
import { Grid, Box } from '@material-ui/core';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { StyledDataSearch } from '../../../../components/styled';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../store/config/selector';

interface SearchExpertProps {
  onSelectedId: (id: number) => void;
}
export const SearchExpert: React.FC<SearchExpertProps> = ({ onSelectedId }) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const [searchValue, setSearchValue] = useState('');
  const customQuery = {
    query: {
      bool: {
        must: [
          {
            multi_match: {
              query: searchValue,
              fields: ['name', 'profile_role.profile_role_name'],
              type: 'cross_fields',
              operator: 'and',
            },
          },
          {
            term: {
              casbin_role: 'role:expert',
            },
          },
        ],
      },
    },
  };
  return (
    <ReactiveBase
      app={ES_INDICES.user_profile_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{ Authorization: window.localStorage.getItem('jwt') }}
    >
      <StyledDataSearch
        componentId="EmailSearch"
        dataField={['email', 'name']}
        queryFormat="and"
        placeholder="Search for email..."
        showIcon={false}
        showClear={true}
        showDistinctSuggestions
        innerClass={{
          input: 'searchInput',
          title: 'titleSearch',
        }}
        defaultQuery={() => customQuery}
        value={searchValue}
        onChange={value => setSearchValue(value)}
        parseSuggestion={suggestion => ({
          label: (
            <div>
              <Grid container direction={'column'}>
                <Grid item>
                  <Box fontWeight="fontWeightBold">
                    {suggestion.source?.name}
                  </Box>
                </Grid>
                <Grid item>
                  <span style={{ fontStyle: 'italic' }}>Email: </span>
                  {suggestion.source?.email}
                  {' | '}
                  <span style={{ fontStyle: 'italic' }}>Business unit: </span>
                  {suggestion.source?.business_unit?.business_unit_name}
                </Grid>
              </Grid>
            </div>
          ),
          value: suggestion.source.email,
          source: suggestion.source,
        })}
        onValueChange={value => setSearchValue(value)}
        onValueSelected={(value, cause, source) => {
          if (source && source.id) {
            onSelectedId(source.id);
          }
        }}
      />
    </ReactiveBase>
  );
};
