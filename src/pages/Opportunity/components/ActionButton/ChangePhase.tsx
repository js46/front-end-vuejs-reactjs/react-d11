import React, { useState, useContext } from 'react';
import { Grid, Box, Typography, Button, TextField } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { MixTitle } from '../../../../components';
import Autocomplete from '@material-ui/lab/Autocomplete';
import OpportunityContext from '../../OpportunityContext';
import { Status } from '../../../../constants';
import { OpportunityAction } from '../../../../services/user.services';

const useStyles = makeStyles(theme => ({
  autocomplete: {
    '& input': {
      padding: '0!important',
    },
  },
}));

interface ChangePhaseProps {
  handleChangeStatus: (comment: string, selectedExpertId?: number) => void;
  listActions: OpportunityAction[];
  onRefresh: () => Promise<void>;
}
export const ChangePhase: React.FC<ChangePhaseProps> = ({
  handleChangeStatus,
  listActions,
  onRefresh,
}) => {
  const classes = useStyles();
  const { groupState } = useContext(OpportunityContext);
  const [selectedAction, setSelectedAction] = useState<number>();
  const [comment, setComment] = useState('');

  const handleChangeComment = (event: React.ChangeEvent<HTMLInputElement>) => {
    setComment(event.target.value);
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    switch (event.target.name) {
      case 'comment': {
        setComment(event.target.value.trim());
        return;
      }
      default:
        return;
    }
  };
  const { t } = useTranslation();

  const handleChangeAction = (
    event: object,
    value: OpportunityAction | null,
  ) => {
    setSelectedAction(value?.action_id as number);
  };

  return (
    <Grid container style={{ width: '470px' }}>
      <Grid item xs={12} container justify="center">
        <Box mt={2} mb={2}>
          <Typography variant="h4" component="p">
            {t('change_status_waiting_to')}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <MixTitle title="Status" />

        <Autocomplete
          id="action"
          options={listActions}
          getOptionLabel={option => option.display_name}
          className={classes.autocomplete}
          renderInput={params => (
            <TextField {...params} fullWidth variant="outlined" />
          )}
          onChange={handleChangeAction}
        />
      </Grid>
      <Grid item xs={12}>
        <Box mt={5}>
          <MixTitle title="Comment" moreText="Max 300 words" />
          <TextField
            type="text"
            id="comment"
            name="comment"
            variant="outlined"
            fullWidth
            multiline
            rows={8}
            value={comment}
            onChange={handleChangeComment}
            onBlur={handleBlur}
            inputProps={{ maxLength: 300 }}
          />
        </Box>
      </Grid>
      <Grid item xs={12} container justify="center">
        <Box mt={5}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleChangeStatus(comment, selectedAction);
              onRefresh();
            }}
            disabled={
              groupState.isSubmitting === Status.Submitting || !selectedAction
            }
          >
            Change
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
};
