import React, { useState, useEffect, useContext } from 'react';
import {
  Grid,
  Box,
  Typography,
  Select,
  MenuItem,
  Button,
  TextField,
} from '@material-ui/core';
import { MixTitle } from '../../../../components';
import { IOpportunity } from '../../../../services/opportunity.services';
import {
  getResourceTeam,
  IResourceTeam,
} from '../../../../services/references.services';
import OpportunityContext from '../../OpportunityContext';
import { Status } from '../../../../constants';
import { useTranslation } from 'react-i18next';

interface TeamRedirectionProps {
  opportunity?: IOpportunity;
  handleRedirectTeam: (
    commentValue: string,
    selectedTeam?: string,
    resourceTeam?: IResourceTeam[],
  ) => void;
}
export const TeamRedirection: React.FC<TeamRedirectionProps> = ({
  opportunity,
  handleRedirectTeam,
}) => {
  const { groupState } = useContext(OpportunityContext);
  const [selectedTeam, setSelectedTeam] = useState(
    opportunity?.resource_team?.name,
  );
  const [resourceTeam, setResourceTeam] = useState<IResourceTeam[]>();
  const [comment, setComment] = useState('');
  useEffect(() => {
    invokeGetResourceTeam();
  }, []);
  const invokeGetResourceTeam = async () => {
    const response = await getResourceTeam();
    if (response.success) {
      setResourceTeam(response.data.list);
    }
  };
  const handleChangeComment = (event: React.ChangeEvent<HTMLInputElement>) => {
    setComment(event.target.value);
  };
  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    setComment(event.target.value.trim());
  };
  const handleChangeTeam = (
    event: React.ChangeEvent<{ name?: string; value: unknown }>,
  ) => {
    setSelectedTeam(event.target.value as string);
  };
  const { t } = useTranslation();
  return (
    <Grid container>
      <Grid item xs={12} container justify="center">
        <Box mt={2} mb={3}>
          <Typography variant="h4" component="p">
            {t('redirect_to_another_team')}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box>
          <MixTitle title="Select team" />
          <Select
            id="select_team"
            name="select_team"
            value={selectedTeam}
            onChange={handleChangeTeam}
            variant="outlined"
            fullWidth
          >
            {resourceTeam &&
              resourceTeam.length > 0 &&
              resourceTeam.map((item, index) => (
                <MenuItem key={index} value={item.name}>
                  {item.name}
                </MenuItem>
              ))}
          </Select>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box mt={5}>
          <MixTitle title="Comment" moreText="Max 300 words" />
          <TextField
            type="text"
            id="comment"
            name="comment"
            variant="outlined"
            fullWidth
            multiline
            rows={8}
            value={comment}
            onChange={handleChangeComment}
            onBlur={handleBlur}
            inputProps={{ maxLength: 300 }}
          />
        </Box>
      </Grid>
      <Grid item xs={12} container justify="center">
        <Box mt={5}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleRedirectTeam(comment, selectedTeam, resourceTeam);
            }}
            disabled={groupState.isSubmitting === Status.Submitting}
          >
            Re-direct
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
};
