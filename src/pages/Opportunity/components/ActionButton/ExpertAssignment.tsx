import React, { useState, useContext } from 'react';
import { Grid, Box, Typography, Button, TextField } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { MixTitle } from '../../../../components';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { IOpportunity } from '../../../../services/opportunity.services';
import OpportunityContext from '../../OpportunityContext';
import { Status } from '../../../../constants';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { IProfileData } from '../../../../services/profile.services';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../../store/config/selector';

const useStyles = makeStyles(theme => ({
  autocomplete: {
    '& input': {
      padding: '0!important',
    },
  },
}));

interface ExpertAssignmentProps {
  opportunity?: IOpportunity;
  handleAssignExpert: (comment: string, selectedExpertId?: number) => void;
  handleReviewYourOwn: (comment: string, selectedExpertId?: number) => void;
}
export const ExpertAssignment: React.FC<ExpertAssignmentProps> = ({
  opportunity,
  handleAssignExpert,
  handleReviewYourOwn,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const { groupState } = useContext(OpportunityContext);
  const [teamExperts, setTeamExperts] = useState<IProfileData[]>([]);
  const [selectedExpert, setSelectedExpert] = useState<number>();
  const [comment, setComment] = useState('');

  const handleChangeComment = (event: React.ChangeEvent<HTMLInputElement>) => {
    setComment(event.target.value);
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    switch (event.target.name) {
      case 'comment': {
        setComment(event.target.value.trim());
        return;
      }
      default:
        return;
    }
  };
  const { t } = useTranslation();

  const handleChangeExpert = (event: object, value: IProfileData | null) => {
    setSelectedExpert(value?.id as number);
  };

  const customQuery = () => {
    return {
      query: {
        bool: {
          should: [
            {
              bool: {
                must: [
                  {
                    term: {
                      'resource_team.resource_team_id':
                        opportunity?.resource_team?.id,
                    },
                  },
                  {
                    terms: {
                      casbin_role: ['role:expert', 'role:teamlead'],
                    },
                  },
                ],
              },
            },
            {
              terms: {
                casbin_role: ['role:admin'],
              },
            },
          ],
        },
      },
    };
  };

  return (
    <Grid container style={{ width: '470px' }}>
      <ReactiveBase
        app={ES_INDICES.user_profile_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <ReactiveList
          componentId="SearchExpert"
          dataField="search_expert"
          loader={<></>}
          showResultStats={false}
          renderNoResults={() => <></>}
          defaultQuery={customQuery}
          onData={({ data }) => {
            setTeamExperts(data);
          }}
          render={() => <></>}
        />
      </ReactiveBase>
      <Grid item xs={12} container justify="center">
        <Box mt={2} mb={2}>
          <Typography variant="h4" component="p">
            {t('start_review')}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <MixTitle title="Expert" />

        <Autocomplete
          id="expert-reviewer"
          options={teamExperts}
          getOptionLabel={option => option.name}
          className={classes.autocomplete}
          renderInput={params => (
            <TextField {...params} fullWidth variant="outlined" />
          )}
          onChange={handleChangeExpert}
        />
      </Grid>
      <Grid item xs={12}>
        <Box mt={5}>
          <MixTitle title="Comment" moreText="Max 300 words" />
          <TextField
            type="text"
            id="comment"
            name="comment"
            variant="outlined"
            fullWidth
            multiline
            rows={8}
            value={comment}
            onChange={handleChangeComment}
            onBlur={handleBlur}
            inputProps={{ maxLength: 300 }}
          />
        </Box>
      </Grid>
      <Grid item xs={12} container justify="center">
        <Box mt={5}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleAssignExpert(comment, selectedExpert);
            }}
            disabled={
              groupState.isSubmitting === Status.Submitting || !selectedExpert
            }
          >
            Assign
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
};
