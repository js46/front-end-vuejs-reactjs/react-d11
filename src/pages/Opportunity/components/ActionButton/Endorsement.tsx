import React, { useState, useContext } from 'react';
import { Grid, Box, Typography, Button, TextField } from '@material-ui/core';
import { MixTitle } from '../../../../components';
import { IOpportunity } from '../../../../services/opportunity.services';
import OpportunityContext from '../../OpportunityContext';
import { Status } from '../../../../constants';
import { useTranslation } from 'react-i18next';
interface EndorsementProps {
  opportunity?: IOpportunity;
  handleClickButton: (comment: string, emailId?: number) => void;
}
export const Endorsement: React.FC<EndorsementProps> = ({
  opportunity,
  handleClickButton,
}) => {
  const [comment, setComment] = useState('');
  const { groupState } = useContext(OpportunityContext);
  const { t } = useTranslation();
  const handleChangeComment = (event: React.ChangeEvent<HTMLInputElement>) => {
    setComment(event.target.value);
  };
  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    setComment(event.target.value.trim());
  };
  return (
    <Grid container>
      <Grid item xs={12} container justify="center">
        <Box mt={2} mb={3}>
          <Typography variant="h4" component="p">
            {t('endorsement')}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box>
          <MixTitle title="Email" />
          <TextField
            type="email"
            id="email"
            name="email"
            variant="outlined"
            fullWidth
            value={opportunity?.created_by?.email}
            inputProps={{
              readOnly: true,
            }}
            disabled={true}
          />
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box mt={5}>
          <MixTitle title="Comment" moreText="Max 300 words" />
          <TextField
            type="text"
            id="comment"
            name="comment"
            variant="outlined"
            fullWidth
            multiline
            rows={8}
            value={comment}
            onChange={handleChangeComment}
            onBlur={handleBlur}
            inputProps={{ maxLength: 300 }}
          />
        </Box>
      </Grid>
      <Grid item xs={12} container justify="center">
        <Box mt={5}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleClickButton(comment, opportunity?.created_by?.id);
            }}
            disabled={groupState.isSubmitting === Status.Submitting}
          >
            Send Endorsement Request
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
};
