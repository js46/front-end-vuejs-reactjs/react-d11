import React, { useState, useContext } from 'react';
import { Grid, Box, Typography, Button, TextField } from '@material-ui/core';
import { MixTitle } from '../../../../components';
import OpportunityContext from '../../OpportunityContext';
import { Status } from '../../../../constants';

interface CommonSelectedActionProps {
  handleClickButton: (commentValue: string) => void;
  title: string;
  buttonName: string;
}
export const CommonSelectedAction: React.FC<CommonSelectedActionProps> = ({
  handleClickButton,
  title,
  buttonName,
}) => {
  const [comment, setComment] = useState('');
  const { groupState } = useContext(OpportunityContext);
  const handleChangeComment = (event: React.ChangeEvent<HTMLInputElement>) => {
    setComment(event.target.value);
  };
  return (
    <Grid container>
      <Grid item xs={12} container justify="center">
        <Box mt={2} mb={3}>
          <Typography variant="h4" component="p">
            {title}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box>
          <MixTitle title="Comment" moreText="Max 300 words" />
          <TextField
            type="text"
            id="comment"
            name="comment"
            variant="outlined"
            fullWidth
            multiline
            rows={8}
            value={comment}
            onChange={handleChangeComment}
            inputProps={{ maxLength: 300 }}
          />
        </Box>
      </Grid>
      <Grid item xs={12} container justify="center">
        <Box mt={5}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleClickButton(comment);
            }}
            disabled={groupState.isSubmitting === Status.Submitting}
          >
            {buttonName}
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
};
