import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Box, TextField, Button, Divider, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Formik } from 'formik';
import {
  CustomNumberInput,
  DeleteDialog,
  MixTitle,
} from '../../../../../components';
import FindingForOption from './FindingForOption';
import {
  addRecommendationOption,
  deleteRecommendationOption,
  IRecommendationOption,
  updateRecommendationOption,
  addFindingForOption,
} from '../../../../../services/recommendation.services';
import { OpportunityPlaybookT } from '../../../../../services/playbook.services';
import { useSelector } from 'react-redux';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  input: {
    flex: 1,
  },
  divider: {
    backgroundColor: '#bbb',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  subMenuInRole: {
    fontWeight: 'bold',
  },
}));

interface AddOptionProps {
  data: IRecommendationOption[];
  onRefresh?: () => void;
  dataEditable: boolean;
  oppPlaybook: OpportunityPlaybookT[];
}
export default function AddOption({
  data,
  onRefresh,
  dataEditable,
  oppPlaybook,
}: AddOptionProps) {
  const classes = useStyles();
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  const [options, setOptions] = useState<IRecommendationOption[]>([]);
  const [removeID, setRemoveID] = useState<number>();
  const [findingData, setFindingData] = useState<number[]>([]);

  useEffect(() => {
    setOptions(data);
  }, [data]);

  useEffect(() => {
    if (removeID === 0) {
      setOptions(prevState => prevState.filter(item => item.id !== removeID));
      setRemoveID(undefined);
    }
  }, [removeID]);

  const onClickAdd = () => {
    setOptions(prevState => [
      ...prevState,
      {
        id: 0,
        title: '',
        description: '',
        pros: '',
        assumption: '',
        effort: undefined,
        metrics: '',
        implementation: '',
        opportunity_id: opportunity?.id ?? 0,
        findings: [],
        // link: '',
      },
    ]);
  };

  const handleSubmit = useCallback(
    async (values: IRecommendationOption) => {
      try {
        const formData = {
          ...values,
          effort: values.effort ? Number(values.effort) : undefined,
        };
        if (values.id > 0) {
          await updateRecommendationOption(values.id, formData);
        } else {
          if (opportunity) {
            const dataOption = await addRecommendationOption(
              opportunity.id,
              formData,
            );
            if (dataOption.success && findingData.length > 0) {
              await addFindingForOption(dataOption.data.id, {
                finding: findingData,
              });
            }
          }
        }
        onRefresh?.();
      } catch (e) {
        console.log('Error saving option', e);
      }
    },
    [findingData, onRefresh, opportunity],
  );
  const handleRemove = useCallback(async () => {
    if (typeof removeID === 'undefined') return;
    if (removeID > 0) {
      await deleteRecommendationOption(removeID);
      onRefresh?.();
    }
    setRemoveID(undefined);
  }, [onRefresh, removeID]);

  const disabledAdd = useMemo(() => {
    return options.some(item => item.id === 0);
  }, [options]);
  return (
    <Box display="flex" flexDirection="column" flex={1}>
      {options.length > 0 &&
        options.map((item, index) => (
          <OptionFormItem
            data={item}
            key={item.id}
            title={`Option ${index + 1}`}
            onSubmit={handleSubmit}
            onRemove={setRemoveID}
            dataEditable={dataEditable}
            oppPlaybook={oppPlaybook}
            onRefresh={onRefresh}
            setFindingData={setFindingData}
          />
        ))}
      <Box mb={1} display="flex" justifyContent="center">
        {dataEditable && (
          <Button
            variant="contained"
            color="primary"
            className={classes.subMenuInRole}
            onClick={onClickAdd}
            disabled={disabledAdd}
          >
            {t('recommend_option.add')}
          </Button>
        )}
      </Box>
      <DeleteDialog
        isOpen={!!removeID}
        header={t('recommend_option.delete')}
        message={t('recommend_option.delete-warning')}
        handleClose={() => setRemoveID(undefined)}
        handleDelete={handleRemove}
      />
    </Box>
  );
}
interface OptionFormItemProps {
  data: IRecommendationOption;
  title: string;
  dataEditable: boolean;
  onSubmit: (values: IRecommendationOption) => Promise<void>;
  onRemove?: (id: number) => void;
  oppPlaybook: OpportunityPlaybookT[];
  onRefresh?: () => void;
  setFindingData: React.Dispatch<React.SetStateAction<number[]>>;
}
const OptionFormItem: React.FC<OptionFormItemProps> = ({
  data,
  title,
  onSubmit,
  onRemove,
  dataEditable,
  oppPlaybook,
  onRefresh,
  setFindingData,
}) => {
  const [disabled, setDisabled] = useState(data.id !== 0);
  const classes = useStyles();

  return (
    <Formik
      enableReinitialize
      initialValues={{
        ...data,
      }}
      onSubmit={(values, formikHelpers) => {
        if (onSubmit) {
          formikHelpers.setSubmitting(true);
          onSubmit(values)
            .then(() => {
              formikHelpers.setSubmitting(false);
              setDisabled(true);
            })
            .catch(() => {
              formikHelpers.setSubmitting(false);
            });
        }
      }}
      onReset={values => {
        if (values.id === 0 && onRemove) return onRemove(values.id);
      }}
    >
      {({ values, handleChange, handleSubmit, handleReset, isSubmitting }) => (
        <form onSubmit={handleSubmit}>
          <Grid container>
            <Grid item xs={12}>
              <Box display="flex" justifyContent="flex-end">
                {dataEditable && (
                  <>
                    {disabled ? (
                      <>
                        <Button
                          variant="outlined"
                          color="secondary"
                          onClick={() => onRemove?.(values.id)}
                          type="button"
                          disabled={isSubmitting}
                        >
                          Remove
                        </Button>
                        <Box ml={1} component="span">
                          <Button
                            variant="outlined"
                            color="primary"
                            onClick={() => setDisabled(false)}
                            type="button"
                            disabled={isSubmitting}
                          >
                            Edit
                          </Button>
                        </Box>
                      </>
                    ) : (
                      <>
                        <Box>
                          <Box mr={1} component="span">
                            <Button
                              variant="outlined"
                              color="default"
                              onClick={() => {
                                handleReset();
                                setDisabled(true);
                              }}
                              disabled={isSubmitting}
                              type="reset"
                            >
                              Cancel
                            </Button>
                          </Box>
                        </Box>
                        <Button
                          variant="contained"
                          color="primary"
                          children={'Save'}
                          type="submit"
                          disabled={isSubmitting}
                        />
                      </>
                    )}
                  </>
                )}
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box display="flex" alignItems="center" flex={1} mt={1} mb={1}>
                <MixTitle title={title} />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box
                display="flex"
                flexDirection="column"
                flex={1}
                mt={1}
                mb={1}
                ml={10}
                mr={10}
              >
                <MixTitle title="Title" isRequired />
                <Box alignItems="center" display="flex" flex={1}>
                  <Box display="flex" flex={1}>
                    <TextField
                      fullWidth
                      variant="outlined"
                      name="title"
                      value={values.title ?? ''}
                      onChange={handleChange}
                      placeholder="Give this option a title"
                      required
                      disabled={disabled || isSubmitting}
                    />
                  </Box>
                </Box>
                <Box mt={2}>
                  <MixTitle title="Option Description" />
                  <TextField
                    fullWidth
                    variant="outlined"
                    name="description"
                    value={values.description ?? ''}
                    onChange={handleChange}
                    placeholder="Description the option - tell the story"
                    disabled={disabled || isSubmitting}
                  />
                </Box>
                <Box mt={2}>
                  <MixTitle title="Pros & Cons" />
                  <TextField
                    fullWidth
                    variant="outlined"
                    name="pros"
                    value={values.pros ?? ''}
                    onChange={handleChange}
                    placeholder="List the Pros and Cons of this option"
                    disabled={disabled || isSubmitting}
                  />
                </Box>
                <Box mt={2}>
                  <MixTitle title="Assumptions / Limitations / Dependencies" />
                  <TextField
                    fullWidth
                    variant="outlined"
                    name="assumption"
                    value={values.assumption ?? ''}
                    onChange={handleChange}
                    placeholder="What are limitations, assumptions? Dependencies (i.e requires FLS enhancement, etc)"
                    disabled={disabled || isSubmitting}
                  />
                </Box>
                <Box mt={2}>
                  <MixTitle title="Indicative Costs / Timing / Effort" />
                  <TextField
                    fullWidth
                    variant="outlined"
                    name="effort"
                    value={values.effort ?? ''}
                    onChange={handleChange}
                    placeholder="How much will it cost? How long will it take?"
                    disabled={disabled || isSubmitting}
                    InputProps={{
                      inputComponent: CustomNumberInput,
                    }}
                  />
                </Box>
                <Box mt={2}>
                  <MixTitle title="Key Measures / Metrics" />
                  <TextField
                    fullWidth
                    variant="outlined"
                    name="metrics"
                    value={values.metrics ?? ''}
                    onChange={handleChange}
                    placeholder="How to measure the success of this option?"
                    disabled={disabled || isSubmitting}
                  />
                </Box>
                <Box mt={2}>
                  <MixTitle title="Ease of Implementation" />
                  <TextField
                    fullWidth
                    variant="outlined"
                    name="implementation"
                    value={values.implementation ?? ''}
                    onChange={handleChange}
                    placeholder="Probability of success?"
                    disabled={disabled || isSubmitting}
                  />
                </Box>
                <Box>
                  <FindingForOption
                    data={data}
                    item={values}
                    oppPlaybook={oppPlaybook}
                    onRefresh={onRefresh}
                    disabled={disabled}
                    setFindingData={setFindingData}
                  />
                </Box>
              </Box>
            </Grid>
            <Grid container>
              <Grid item xs={12}>
                <Divider variant="fullWidth" className={classes.divider} />
              </Grid>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};
