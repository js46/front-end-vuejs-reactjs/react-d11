import loadable from '@loadable/component';

export const Recommend = loadable(() => import('./Recommend'));
export const AddOption = loadable(() => import('./AddOption'));
