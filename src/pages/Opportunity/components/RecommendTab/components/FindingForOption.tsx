import React, { useState, useMemo, useEffect } from 'react';
import { PlaybookItemFindingT } from '../../../../../services/playbook.services';
import {
  Box,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  MenuItem,
  Menu,
} from '@material-ui/core';
import { AnalysePermission } from '../../../types';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { AnalysePermissionState } from '../../../../../store/opportunity/selector';
import {
  CollapseIconButton,
  DeleteDialog,
  TableAction,
} from '../../../../../components';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useDisableMultipleClick } from '../../../../../hooks';
import {
  IRecommendationOption,
  deleteFindingForOption,
} from '../../../../../services/recommendation.services';
import { OpportunityPlaybookT } from '../../../../../services/playbook.services';

interface Finding {
  id: number;
  finding: PlaybookItemFindingT;
  playbookName?: string;
  stepTitle?: string;
  updateData: string;
  playbook_id?: number;
  current_step?: number;
  opportunity_id?: number;
  public?: boolean;
  del_flg?: boolean;
  titleFinding: string;
}

interface FindingForOptionProps {
  data: IRecommendationOption;
  item: IRecommendationOption;
  onRefresh?: () => void;
  oppPlaybook: OpportunityPlaybookT[];
  disabled: boolean;
  setFindingData: React.Dispatch<React.SetStateAction<number[]>>;
}

const FindingForOption: React.FC<FindingForOptionProps> = ({
  data,
  item,
  onRefresh,
  oppPlaybook,
  disabled,
  setFindingData,
}) => {
  const { t } = useTranslation();
  const analysePermission = useSelector(AnalysePermissionState);
  const [collapse, setCollapse] = useState(true);
  const [anchorElement, setAnchorElement] = useState<any>(null);
  const [deletedItem, setDeletedItem] = useState<Finding>();
  const classes = useStyles();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [findings, setFindings] = useState<Finding[]>();

  const onDeleteFinding = async () => {
    if (!deletedItem) return;

    await debounceFn();
    try {
      await deleteFindingForOption(item.id, deletedItem.id);
    } catch (e) {
      console.log('Error deleting finding', e);
    }
    endRequest();
    setDeletedItem(undefined);
    await handleDialogSuccess();

    let newFinding: Finding[] = [];
    if (findings && deletedItem) {
      newFinding = [...findings];
      newFinding.splice(findings.indexOf(deletedItem), 1);
      setFindings(newFinding);
    }
  };

  const handleDialogSuccess = async () => {
    setAnchorElement(null);
    onRefresh?.();
  };

  const handleAddFindingForOption = async (finding: Finding) => {
    let newFinding: Finding[] = [];
    if (findings) {
      newFinding = [...findings, finding];
    } else {
      newFinding = [finding];
    }

    setFindings(newFinding);
    const findingIds: number[] = newFinding.map(item => item.id);
    setFindingData(findingIds);
  };

  const findingSelectionOptions = useMemo(() => {
    let listFindings: Finding[] = [];
    oppPlaybook?.map(e => {
      e.playbook_items?.map(item => {
        item.findings?.map(finding => {
          if (!finding.del_flg) {
            listFindings.push({
              opportunity_id: e.opportunity.id,
              playbook_id: e.id,
              id: finding.id,
              finding: finding,
              playbookName: e.name,
              stepTitle: item.title,
              current_step: item.id,
              updateData: moment(finding.date).format('DD/MM/YYYY HH:mm:ss'),
              public: finding?.public_flg,
              titleFinding: finding.title,
            });
          }
        });
      });
    });

    const currentFindings = findings?.map(item => item?.titleFinding);

    if (!currentFindings) return listFindings ?? [];
    if (listFindings) {
      return (
        listFindings?.filter(
          item => !currentFindings.includes(item.titleFinding),
        ) ?? []
      );
    }
  }, [findings, oppPlaybook]);

  useEffect(() => {
    let listFindings: Finding[] = [];
    data.findings?.map(finding => {
      listFindings.push({
        opportunity_id: finding.opportunity?.id,
        playbook_id: finding.opportunity_playbook?.id,
        id: finding.id,
        finding: finding,
        playbookName: finding.opportunity_playbook?.name,
        stepTitle: finding.opportunity_playbook_item?.title,
        current_step: finding.opportunity_playbook_item?.id,
        updateData: moment(finding.date).format('DD/MM/YYYY HH:mm:ss'),
        public: finding?.public_flg,
        titleFinding: finding.title,
      });
    });
    setFindings(listFindings);
  }, [data.findings, item.id, item.title]);

  return (
    <>
      <Box display="flex" alignItems="center" mb={2} mt={2}>
        <Typography component="span" variant="caption">
          {t('finding.plural')}
        </Typography>
        <Box ml={2} display="flex">
          <CollapseIconButton
            onToggle={isCollapse1 => setCollapse(isCollapse1)}
          />
        </Box>
        <IconButton
          disabled={disabled || item.title === ''}
          onClick={event => {
            setAnchorElement(event.currentTarget);
          }}
        >
          <AddCircleOutlineIcon />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorElement}
          keepMounted
          open={Boolean(anchorElement)}
          onClose={() => {
            setAnchorElement(null);
          }}
        >
          <MenuItem disabled>
            <Typography>Select findings</Typography>
          </MenuItem>
          {findingSelectionOptions &&
            findingSelectionOptions?.map(finding => {
              return (
                <MenuItem
                  key={finding.id}
                  onClick={event => {
                    handleAddFindingForOption(finding);
                    setAnchorElement(null);
                    setCollapse(false);
                  }}
                >
                  {finding.titleFinding}
                </MenuItem>
              );
            })}
        </Menu>
      </Box>
      {!collapse && findings && findings?.length > 0 && (
        <Box>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell>PLaybook name</TableCell>
                  <TableCell>Step title</TableCell>
                  <TableCell>Last updated</TableCell>
                  <TableCell>Updated By</TableCell>
                  <TableCell align="center" width={50}>
                    Action
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {findings?.map((item, index) => {
                  return (
                    <TableRow key={index} className={classes.tableRow}>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {item?.titleFinding}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {item?.playbookName}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {item?.stepTitle}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {item?.updateData}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <Typography component="span" variant="body1">
                          {/* {item?.created_by_user?.name} */}
                        </Typography>
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        <TableAction
                          hiddenItem={['edit', 'view']}
                          onClickDelete={() => setDeletedItem(item)}
                          disabledDelete={
                            !analysePermission?.includes(
                              AnalysePermission.PlaybookItemNoteDelete,
                            ) || disabled
                          }
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      )}
      <DeleteDialog
        isOpen={!!deletedItem}
        header={t('finding.delete')}
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('finding.delete-warning')}
            </Typography>
          </Box>
        }
        handleClose={() => setDeletedItem(undefined)}
        handleDelete={onDeleteFinding}
        disabled={isSubmitting}
      />
    </>
  );
};
const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  headTable: {
    minWidth: 732,
  },
  tableRow: {
    transition: '0.3s all',
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: '#eee',
    },
  },
}));

export default FindingForOption;
