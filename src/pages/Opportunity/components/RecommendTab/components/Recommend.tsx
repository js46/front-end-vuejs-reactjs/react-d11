import React from 'react';
import {
  Box,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
  TextField,
} from '@material-ui/core';
import { Formik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import {
  IRecommendationOption,
  sendRecommendationSelected,
} from '../../../../../services/recommendation.services';
import { useTranslation } from 'react-i18next';
import { MixTitle } from '../../../../../components';
import { PanelActionButton } from '../../PanelActionButton';
import { useToggleValue, useDisableMultipleClick } from '../../../../../hooks';
import { useSelector } from 'react-redux';
import { OpportunityState } from '../../../../../store/opportunity/selector';
import { UserProfileState } from '../../../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  input: {
    flex: 1,
  },
  form: {
    display: 'flex',
    flex: 1,
  },
}));
interface RecommendProps {
  options: IRecommendationOption[];
  onRefresh?: () => void;
}

export default function Recommend({ options, onRefresh }: RecommendProps) {
  const classes = useStyles();
  const { t } = useTranslation();
  const opportunity = useSelector(OpportunityState);
  const userProfileState = useSelector(UserProfileState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [editable, toggleEditable] = useToggleValue(false);
  const initialValues = {
    summary: opportunity?.recommend_summary?.content ?? '',
    option_id: opportunity?.recommend_option
      ? opportunity?.recommend_option.id.toString()
      : '',
  };

  const isTeamLead =
    userProfileState.team_lead_flg &&
    userProfileState.resource_team.resource_team_id ===
      opportunity?.resource_team?.id;

  const toggleForm = (onReset: () => void) => () => {
    if (editable) onReset();
    return toggleEditable();
  };

  const renderActionButtons = (
    handleSubmit: () => void,
    onReset: () => void,
  ) => (
    <PanelActionButton
      enableEdit={editable}
      onToggleEdit={toggleForm(onReset)}
      onSubmit={handleSubmit}
      disabled={isSubmitting}
    />
  );
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async values => {
        if (isSubmitting) {
          return;
        }
        if (opportunity) {
          await debounceFn();
          await sendRecommendationSelected(
            {
              summary: values.summary,
              opportunity_id: opportunity?.id,
            },
            Number(values.option_id),
          ).then(toggleEditable);
          onRefresh?.();
          endRequest();
        }
      }}
    >
      {({ isSubmitting, values, handleChange, handleSubmit, resetForm }) => (
        <form onSubmit={handleSubmit} className={classes.form}>
          <Box display="flex" flexDirection="column" flex={1}>
            <Box mt={2} display="flex" justifyContent="flex-end">
              {opportunity?.created_by?.id === userProfileState?.id ||
              userProfileState.isAdmin ||
              isTeamLead
                ? renderActionButtons(handleSubmit, resetForm)
                : null}
            </Box>
            <Box display="flex" flexDirection="column" flex={1}>
              <MixTitle title="Select Preferred Options" />
              <Box mt={2} mb={2} ml={6}>
                <RadioGroup
                  name="option_id"
                  value={values.option_id}
                  onChange={handleChange}
                >
                  {options.length > 0 ? (
                    <>
                      {options.map((option, index) => (
                        <Box key={index} display="flex" flex={1} mt={1} mb={1}>
                          <FormControlLabel
                            control={<Radio color="primary" />}
                            label={`Option: ${index + 1}`}
                            value={option.id.toString()}
                            disabled={!editable}
                          />
                          <TextField
                            fullWidth
                            variant="outlined"
                            value={option.title}
                            className={classes.input}
                            disabled
                          />
                        </Box>
                      ))}
                    </>
                  ) : (
                    <Box ml={-6} display="flex" justifyContent="center">
                      <Typography>{t('no_option')}</Typography>
                    </Box>
                  )}
                </RadioGroup>
              </Box>
            </Box>
            <Box>
              <MixTitle
                title="Recommendation Summary"
                moreText="Max 300 words each"
              />
              <Box mt={1} mb={2}>
                <TextField
                  disabled={!editable}
                  fullWidth
                  variant="outlined"
                  name="summary"
                  value={values.summary}
                  onChange={handleChange}
                  rows={4}
                  multiline
                />
              </Box>
            </Box>
          </Box>
        </form>
      )}
    </Formik>
  );
}
