import React, { useCallback, useMemo, useState } from 'react';
import { Box, ExpansionPanelDetails, Typography } from '@material-ui/core';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import {
  ExpansionPanelStyled,
  ExpansionPanelSummaryStyled,
} from '../../../../components/styled';
import { AddOption, Recommend } from './components';
import SeekEndorsementPanel from '../SeekEndorsementPanel';
import { UserProfileState } from '../../../../store/user/selector';
import { useSelector } from 'react-redux';
import {
  IsAdminOpportunityState,
  OpportunityState,
} from '../../../../store/opportunity/selector';
import {
  getRecommendationOptions,
  IRecommendationOption,
} from '../../../../services/recommendation.services';
import {
  getAllPlaybookOfOpportunity,
  OpportunityPlaybookT,
} from '../../../../services/playbook.services';
import { useEffectOnlyOnce } from '../../../../hooks';
import { useTranslation } from 'react-i18next';
import { OpportunityPhase } from '../../../../constants/opportunityPhase';

enum ExpandableAnalyse {
  Recommend = 'Recommend',
  AddOption = 'AddOption',
  SeekEndorsement = 'SeekEndorsement',
}

export enum Status {
  ANALYSE_PHASE = 3,
  ANALYSIS_STATE = 12,
}

export const RecommendTab = () => {
  const { t } = useTranslation();
  const [expanded, setExpanded] = useState<ExpandableAnalyse>();
  const opportunity = useSelector(OpportunityState);
  const userProfileState = useSelector(UserProfileState);
  const isAdminOpportunity = useSelector(IsAdminOpportunityState);
  const [recommendOptions, setRecommendOptions] = useState<
    IRecommendationOption[]
  >([]);
  const [oppPlaybook, setOppPlaybook] = useState<OpportunityPlaybookT[]>([]);
  const getRecommendOptions = useCallback(async () => {
    if (!opportunity) return;
    try {
      const json = await getRecommendationOptions(opportunity.id);
      if (json.success && json.data.list !== null) {
        setRecommendOptions(json.data.list);
      }
    } catch (e) {
      console.log('Error getting options', e);
    }
  }, [opportunity]);

  const getFindingPlayBookItem = useCallback(async () => {
    // let findingPlaybook: ResponsePlayBookT[] = [];
    if (opportunity) {
      const response = await getAllPlaybookOfOpportunity(opportunity?.id);
      if (response.success) {
        // response.data?.map(e => {
        //   e.playbook_items?.map(item => {
        //     // if (!finding.del_flg) {
        //     findingPlaybook.push(item);
        //     // }
        //   });
        // });

        setOppPlaybook(response.data);
      }
    }
  }, [opportunity]);
  useEffectOnlyOnce(() => {
    getFindingPlayBookItem();
    getRecommendOptions();
  });

  const editable = useMemo(() => {
    if (isAdminOpportunity) return true;
    const editablePhases = ['shape', 'analyse'];
    if (
      opportunity?.opportunity_phase?.name &&
      editablePhases.includes(opportunity.opportunity_phase.name) &&
      opportunity?.current_expert_review?.expert_review?.id ===
        userProfileState.id
    ) {
      return true;
    }
    return !!opportunity?.current_experts?.find(
      expert => expert.user_id === userProfileState?.id,
    );
  }, [isAdminOpportunity, opportunity, userProfileState]);

  const onChangePanel = (name: ExpandableAnalyse) => () => {
    setExpanded(expanded !== name ? name : undefined);
  };
  const renderExpansionIcon = (current: ExpandableAnalyse) => {
    return current === expanded ? <RemoveCircleIcon /> : <AddCircleIcon />;
  };
  return (
    <Box>
      <ExpansionPanelStyled
        expanded={expanded === ExpandableAnalyse.AddOption}
        onChange={onChangePanel(ExpandableAnalyse.AddOption)}
        disabled={
          // !editable ||
          opportunity?.opportunity_state?.id !== Status.ANALYSIS_STATE &&
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= Status.ANALYSE_PHASE
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="data-source-content"
          id="data-source-header"
          expandIcon={renderExpansionIcon(ExpandableAnalyse.AddOption)}
        >
          <Typography variant="h5">{t('recommend_option.plural')}</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <AddOption
            data={recommendOptions}
            onRefresh={getRecommendOptions}
            dataEditable={editable}
            oppPlaybook={oppPlaybook}
          />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      <ExpansionPanelStyled
        expanded={expanded === ExpandableAnalyse.Recommend}
        onChange={onChangePanel(ExpandableAnalyse.Recommend)}
        disabled={
          // !editable ||
          opportunity?.opportunity_state?.id !== Status.ANALYSIS_STATE &&
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= Status.ANALYSE_PHASE
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="data-source-content"
          id="data-source-header"
          expandIcon={renderExpansionIcon(ExpandableAnalyse.Recommend)}
        >
          <Typography variant="h5">Recommendation</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <Recommend options={recommendOptions} />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
      <ExpansionPanelStyled
        expanded={expanded === ExpandableAnalyse.SeekEndorsement}
        onChange={onChangePanel(ExpandableAnalyse.SeekEndorsement)}
        disabled={
          // !editable ||
          opportunity?.opportunity_state?.id !== Status.ANALYSIS_STATE &&
          opportunity?.opportunity_phase &&
          opportunity?.opportunity_phase?.id <= Status.ANALYSE_PHASE
        }
      >
        <ExpansionPanelSummaryStyled
          aria-controls="data-source-content"
          id="data-source-header"
          expandIcon={renderExpansionIcon(ExpandableAnalyse.SeekEndorsement)}
        >
          <Typography variant="h5">Seek Endorsement</Typography>
        </ExpansionPanelSummaryStyled>
        <ExpansionPanelDetails>
          <SeekEndorsementPanel
            phases={['recommend', 'analyse']}
            isCurrentExpert={editable}
            currentTabPhase={OpportunityPhase.Recommend}
          />
        </ExpansionPanelDetails>
      </ExpansionPanelStyled>
    </Box>
  );
};

export default RecommendTab;
