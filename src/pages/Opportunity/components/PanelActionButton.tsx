import React from 'react';
import { Box, Button, Grid } from '@material-ui/core';

interface IPanelActionButton {
  enableEdit?: boolean;
  onToggleEdit?: () => void;
  onSubmit?: () => void;
  disabled?: boolean;
}
export const PanelActionButton: React.FC<IPanelActionButton> = props => {
  const { enableEdit, onToggleEdit, onSubmit, disabled } = props;
  return (
    <Grid container item justify={'flex-end'}>
      {enableEdit ? (
        <>
          <Button
            type="button"
            size="medium"
            color="default"
            onClick={onToggleEdit}
            variant="outlined"
          >
            Cancel
          </Button>
          <Box ml={1}>
            <Button
              type="button"
              size="medium"
              variant="contained"
              color="primary"
              onClick={onSubmit}
              disabled={disabled}
            >
              Save
            </Button>
          </Box>
        </>
      ) : (
        <Button
          variant="outlined"
          size="medium"
          color="primary"
          onClick={onToggleEdit}
          type="button"
        >
          Edit
        </Button>
      )}
    </Grid>
  );
};
