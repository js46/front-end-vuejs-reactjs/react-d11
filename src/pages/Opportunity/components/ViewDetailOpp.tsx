/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  Typography,
  TextField,
  FormControl,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableContainer,
} from '@material-ui/core';
import moment from 'moment';
import { SectionBox } from '../../../components/styled';
import { MixTitle } from '../../../components';
import { useSelector } from 'react-redux';
import { OpportunityState } from '../../../store/opportunity/selector';
const useStyles = makeStyles(theme => ({
  section: {
    border: `1px solid ${theme.palette.grey['300']}`,
    borderRadius: 6,
    width: '100%',
    padding: 5,
    marginTop: 8,
    marginBottom: 8,
  },
  sectionBox: {
    marginTop: 8,
    marginBottom: 8,
  },
  table: {
    '& tr td:first-child': {
      width: 200,
    },
  },

  bold: {
    fontWeight: 700,
  },
  title: {
    marginLeft: 10,
    marginRight: 20,
    width: 300,
  },
  disabled: {
    '& .MuiInputBase-input.Mui-disabled': {
      color: theme.palette.grey[800],
    },
  },
  selectItem: {
    color: '#666',
    backgroundColor: '#e0e0e0',
  },
  formControl: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },
  iteratorNumber: {
    marginRight: '15px',
  },
  imgCheckbox: {
    width: 30,
    height: 30,
  },
  imgUnCheckbox: {
    width: 25,
    height: 25,
  },
  textArea: {
    padding: 10,
    border: '1px solid #ccc',
    borderRadius: 6,
    minHeight: 100,
  },
  captureContent: {},
}));
export const ViewDetailOpp: React.FC = () => {
  const opportunity = useSelector(OpportunityState);
  const classes = useStyles();
  const href = window.location.href;

  return (
    <Grid container spacing={2}>
      <Grid item container xs={12}>
        <Box id="capture" width="100%">
          <Box
            id="title-top"
            pt={6}
            pb={3}
            display="flex"
            flexDirection="column"
            alignItems="center"
          >
            <Typography variant="h3">
              <b>{opportunity?.title}</b>
            </Typography>
            <a color="primary" href="http://localhost:3000/opportunity/4">
              {href}
            </a>
          </Box>
          <SectionBox component="section" className={classes.section}>
            <Box m={1}>
              <Box id="title-capture">
                <Typography gutterBottom={true} variant="h4" align="left">
                  Capture
                </Typography>
              </Box>
              <Box pb={2} id="capture-proposer">
                <Grid container spacing={2}>
                  <Grid item xs={12} md={4}>
                    <MixTitle title="Proposer email" bold />
                    <TextField
                      defaultValue={opportunity?.created_by?.email}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <MixTitle title="Proposer business unit" bold />
                    <TextField
                      defaultValue={opportunity?.created_by?.business_unit}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <MixTitle title="Sponsor business unit" bold />
                    <TextField
                      defaultValue={opportunity?.sponsor_business_unit?.name}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                </Grid>
              </Box>
              <Box pb={2} id="capture-description">
                {opportunity?.description && (
                  <Grid container item xs={12}>
                    <Grid item xs={12}>
                      <MixTitle title="Description" bold />
                      <Box className={classes.textArea}>
                        {
                          <Box
                            dangerouslySetInnerHTML={{
                              __html: opportunity?.description.replace(
                                /\n/g,
                                '<br />',
                              ),
                            }}
                          />
                        }
                      </Box>
                    </Grid>
                  </Grid>
                )}
              </Box>
              <Box pb={2} id="capture-hypothesis">
                {opportunity?.hypothesis && (
                  <Grid container item xs={12}>
                    <Grid item xs={12}>
                      <MixTitle title="Hypothesis" bold />
                      <Box className={classes.textArea}>
                        {
                          <Box
                            dangerouslySetInnerHTML={{
                              __html: opportunity?.hypothesis.replace(
                                /\n/g,
                                '<br />',
                              ),
                            }}
                          />
                        }
                      </Box>
                    </Grid>
                  </Grid>
                )}
              </Box>
              <Box pb={2} id="capture-bu">
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <MixTitle title="Business units impacted" bold />
                    <TextField
                      defaultValue={opportunity?.business_units
                        ?.map(item => item.name)
                        .join(', ')}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                </Grid>
              </Box>
              <Box pb={2} id="capture-bp">
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <MixTitle title="Business processes impacted" bold />
                    <TextField
                      defaultValue={opportunity?.processes
                        ?.map(item => item.name)
                        .join(', ')}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                </Grid>
              </Box>
              <Box pb={2} id="capture-completion-date">
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <MixTitle title="Requested completion date" bold />
                    <TextField
                      defaultValue={moment(
                        opportunity?.requested_completion_date!,
                      ).format('DD MMMM YYYY')}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <MixTitle title="Priority level" bold />
                    <TextField
                      defaultValue={
                        opportunity?.opp_priority_score?.priority_level ||
                        'Medium'
                      }
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                </Grid>
              </Box>
              <Box pb={1} id="capture-private">
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Box display="flex" alignItems="center">
                      <MixTitle title="Private" noMargin bold />
                      <Box ml={1} display="flex" alignItems="center">
                        {opportunity?.private_flag ? (
                          <img
                            src={
                              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAQAAAD41aSMAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAACYktHRAD/h4/MvwAAAAlwSFlzAAALEwAACxMBAJqcGAAABRFJREFUeNrt3U9rXFUcxvEnaoqJtXSjLyDSilQNriy6CySF6kZX7VJ8CRLIJp1YC9ai4Etwo92IrlrwT5YKaVw0Ql017aogriaUVkyVuqmYhrk398/5nXvO73yfhVC0Kf1+zMzcmXtvppbFhtwTJAAAAAYAAAwAABgAADAAAGAAAMAAAIABAAADAAAGAAAMAABY+D0V5Ks8q7e1oHnN6YimnRd7oB3d1pbWdUV3+3+5qd4nZh3Xis5opsD/ee/rsi7q5pAPQTP6VDf0XpH5pVm9r990SU8PBXBMm/og0INYrpvWsjb0whAAr+knneBJVNKr+lnzsQGO6Ts9R/tHe17fd/0u6AYwq2/Jv4/gm27PBd0AzvPgM+GB6MNYL0OP60bhT71VRwgvaTvGd8AK+SteEa3EeAg6ojO0rthZHbYHeKvQw64me0an7QEW6ByyTnuAeSqHrNMeYI7KIet0eRJm1TtqDzBN5ZodinMkzIINAAAAYAAAwAAAgAEAAAMAACZJdwAYchs60e0sBwDC5D+lHa3FIACgKr+kGAQAVOePQgBAXf4IBADU5zcnAOCg/MYEAByc35QAgCb5DQkAaJbfjACApvmNCABonp/ngMHzr2kEgKv8ZQNcGz5/yQDXtDR8/nIBEslfKkAy+csESCh/iQBJ5S8PILH8pQEkl78sgATzlwSQZP5yABLNXwpAsvnLAEg4fwkASef3D5B4fu8Ayef3DZBB/ngAd6L/zTZzyB8LYFUv65fI+RdzyB8HYFUXNNZiRIJs8scAWNUFSYpIkFF+e4Bzj/LHI8gqvzXAOX302K/tCTLLbwuwP789QXb5LQFGE/LbEmSY3w5gpPOV/86GIMv8VgB1+W0IMs1vA7B2QP7wBNnmtwBoeg59OIKM81sATDX+L8MQZJ3fAmCktYgEmee3eQ6IR5B9frtXQTEIMnnDeajjAGuCTS1pnHt+2yNhSwIn+W3fC7IjcJPf+t1QGwJH+e0/DwhP4Cp/jE/EwhI4yx/nM+FwBO7yxzorIgyBw/zxzgvqT+Ayf8wz4/oROM0f99TE7gRu88c+N7QbgeP8iv6DaUdSY4SxFvWDHnrOrwF+MnBbAnnOr0F+NHM7AnnOP9T1AW2eC1znH+4CjdAEmeYf8gqZkATZ5h/2EqVQBBnnH/oasRAEWecf/iK9vgSZ50/hKsk+BNnnT+My1a4EDvKncp1wFwIX+dO5ULstgZP8KV0p34bATf60blXQlMBR/tTuFdGEwFX+9G7WcRCBs/wp3i2ljsBd/jRvV1NF4DB/qvcLmkTgMn+6N2zaT+A0f8p3zNpL4DZ/2rcs+4/Acf5hPpRvQ/D/P51uarnt73goVlvUz0NQEQMAAAAYAAAwAABgAADAAACAAQAAAwCAyXtAtJrt2gPsULlmY3uA21Su2S17gC0qh6zTHmCdyiHrtAe4ovt0rtg9XbUHuKvLlK7Yl7oX4zjgIi9FK16CfhLnQOymPqf2hH3W/jVQlxOzJGlGG3qF4o/tuk7qrzjfAdKfekd/0HzPfte7XfJ3fy9oW0sQ7Ml/qusBavc347b0hn6lvaTrfUr0eTd0W6/rUuGviHb1sU72eXvmyTf7/PF/60d9pVm9qENFHnZ9obP6Wv/0+SLdXgXt32Gd1oLmNaej7il2NdYtbWldV9sfdlkBsM7jEzEAAGAAAMAAAIABAAADAAAGAAAMAAAYAAAwAABgAADAAACAGexf9yGQ75J0MaMAAAAASUVORK5CYII='
                            }
                            className={classes.imgCheckbox}
                          />
                        ) : (
                          <img
                            src={
                              'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAABmJLR0QA/wD/AP+gvaeTAAAUfElEQVR4nO3de/BtZV3H8Q/nBnJA5H7wTppw8IKjoZZ5QxxRxzEpNUXBFJ1EC3PSZuqvZvyjzDLLG5GaZEzaGOqMt3AEFesPDVREgYCDFRzOgYMGqBzknF9/PD/0iCDn8qy19v7u12tmDf71+Dxn7Vnv39577fUkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACw+/aaegL8xP5J1if5pSQPXT7WJTkkycHLx6okeyfZd5IZAovkh0m2JrkjyZYkNy7/d2OSa5aPDUm+neTWSWbIzxD0aeyb5LgkT0nyuCSPSQu58wHMm6UkVyf5RpKLklyY5KtpfxAwIgEZx4q0gD8nybOTPD7J6klnBDCcHyf5WpLPJfnM8v/ePumMFoCgD2dlkmckeUmS30j76BxgEd2Q5NwkH0nyxSTbpp1OTYLe3zFJXpPkZUkOm3guALNmU5JzkpyV5DsTz6UUQe9jdZIXJTk9yZMnngvAvLgwyXuS/EvazXfsAUHfM/dN8tokv5/kQRPPBWBe/XeSd6a9a79l4rnMLUHfPWuTvCHJW5IcNPFcAKq4KcnfJvmrJDdPPJe5I+i7ZnXax+p/kuTQiecCUNXmJG9N8r60O+bZCYK+805I8tdJHjn1RAAWxBVJ3pTkU1NPZB4I+r17YNpNG8+feiIAC+rjSV6f5LqpJzLLVk49gRm2V5JT0l5Ij514LgCL7OgkpyX5UdpT6Jamnc5s8g797h2e5ANJnjv1RAD4GZ9P8sok1048j5mzYuoJzKCTklwaMQeYRSck+XqSF0w9kVkj6D+1KsmfJflY2s5mAMymQ9K+Dj0z9sX4CR+5N4ck+eckz5x6IgDski+l7Zlx/dQTmZqgt5+hfSrJQ6aeCAC75dokz0vbwnVhLfpH7s9M8pWIOcA8e0CSLyc5ceqJTGmRg/7StH16D5h6IgDssf2TfCLJi6eeyFQW9XfoL0/yobiZAqCSlWm/VPrfJBdPPJfRLeJ36KcneVdmd+03JLks7ZGHVyS5PG3/4B8sH99b/u/tU00QKG9N2iZUBy7/d22SdUkesXwctXzM6p4WS2lPlnvv1BNhOKcm2ZZ2smfluC7JR9O2YfVdPjBP1iV5UdrPx67K9NfTHY/tSV4z3NKZ0kuT3JHpX2RLaR8F/UGShw65YICRHZm2mcrXM/11dintmr+w36lXdULaR9RTvrCuTfK2JI8aeK0As+DRade8azPttXdrkmcMvFZGckza985TvZiuSnJGkr2HXijADFqdttHVdzLddfj/0v7AYI4dnuS7meYFdHHaRz2L/NNAgDutTHui21Qfx2/I7N7Ex71YleSCjP+i2ZL2jlzIAX7enVtTb8r41+cvpLWBOfOOjPtC2Zbk7LTnwgPwi90vyTsz/s3Kfz7G4ujnpIz7ArkkyeNHWRlALcelbVk91vV6e5Lnj7Iy9tj9k9yY8V4cZ6c9dAGA3bNP2rv1sa7bm5McMcrK2G0rkpyXcV4QNyc5eZxlASyEVyS5JeNcwz+b2X1iKElel3FeCFemPfYQgL6OznhPnfMkuRl1/4zze/NLlv+/ABjGuiQXZfjr+ffTtl5lxnwyw5/8LyS571gLAlhgByQ5P8Nf1z821oLYOc/N8Cf94/G0N4Ax7ZO2x/nQ1/dnjbUgfrHVaduNDnmyz097YQEwrjVJ/i3DXuO/ndYSJnZGhj3RF8XH7ABTOiDtcdpDXutPH2013K21GfbxgVem3ZwBwLQOTXJ5hrveb0yy72ir4ef8cYY7uTfHT9MAZsn6DPs79TePtxR2dN+0jVCGOrEeGgMwe07JcNf9G5LsN95SuNObM9xJPWvEdQCwaz6Y4a7/Z4y4DtLuRhxqn/NvxfcoALNsbYbb0OWa2GJ1VCdnmBN5R+yaBjAPnpC2bfUQLfjtEdex8L6SYU7iu8ZcBAB75MwM04IvjrmIRbY+bT/b3idwU5IDR1wHAHvmoLStUIeI+voR17Gw3pFhTt6pYy4CgC5enWGa8PYxF7GIVmWYB8lcHPviAsyjFWm7YPbuwnVJVo64joXzrAzzl9iLx1wEAF29LMO04ekjrmHhnJX+J+y/4q8wgHm2MskV6d+H9465iEWyIsPc/PA7Yy4CgEGclv59uD6tPXT2xPQ/WdfGlnkAFaxJ+967dyd+ZcxF7Il5+svjOQOM+eEkPx5gXADGdXuScwYY98QBxlx4/5H+f3k9etQVADCkY9O/ExeOuoIFsG/aX189T9JFo64AgDF8I31bsTVzsr/HvHzk/sT0/677HzuPB8D0Ptx5vDWZkz0+5iXovzbAmOcOMCYA0/rXAcZ88gBjdjcvQe99l+GGtC3yAKjlqrTttXuaizvd5yXoj+k83vmdxwNgdlzQeby5uIF6HoK+f5IjO48p6AB19b7GPzzJ2s5jLqQhHijzgFFXAMCYHpT+3Zj5G+Pm4R1673fnm9OeEAdATf+TZEvnMXu3qLtFDPrlnccDYPb0vtYLegcP6TzeFZ3HA2D2CPoMOqLzeN6hA9TX+83bus7jdTcPQT+483jeoQPU1/vN2yGdx+tuHoLe+x9xU+fxAJg9mzuP1/vNZXfzEPQDO493a+fxAJg9N3ce76DO43U3D0Hfu/N4t3QeD4DZ0/vNW+8WdTcPQV/TeTxBB6iv97Ve0DvoHXQfuQPUt3BB32vqCeyEpc7jzcOaAdhzC9WPeXiHDgDcC0EHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKEHQAKEDQAaAAQQeAAgQdAAoQdAAoQNABoABBB4ACBB0AChB0AChA0AGgAEEHgAIEHQAKmIegb+s83prO4wEwe/buPF7vFnU3D0G/vfN4+3UeD4DZs3/n8bZ2Hq+7RQx675MMwOwR9Bl0W+fxBB2gvt7X+t4t6m4egv69zuP5yB2gvt5Bv6nzeN3NQ9Bv7Dze4Z3HA2D2HNZ5vC2dx+tuHoLe+x/xEZ3HA2D2HNV5vN5vLrubh6Bv7Dxe75MMwOzp/ebt+s7jdTcPQf9u5/EEHaC+ozuPt6HzeN3NQ9Cv6Tyej9wB6ut9rRf0Dq7uPN5hSR7YeUwAZseDkxzceUxB7+DSJNs7j/n0zuMBMDuO7zzetiSXdR6zu3kI+g/S/y+jZ3QeD4DZ0fsaf2WSH3Yes7t5CHqSfLPzeCd0Hg+A2fG0zuP1btAg5iXoX+s83oOTHNl5TACm9/AkD+k85kWdxxvEvAT9KwOM+cIBxgRgWicNMOaFA4y5sO6TttPNUsfj4lFXAMAYvpm+rbgtyT6jrmAB/Hv6nqSlJI8edQUADOmx6d+JL426gj0wLx+5J8nnBhjzFQOMCcA0hrimD9GehfeE9P/L69okq8dcBACDWJPkuvTvxOPGXMSiWJFkU/qfrFeNuQgABnFa+vdhY5K9xlzEIjkz/U/YlUlWjrkIALpameSK9O/Du8dcxKI5Pv1P2FKSl4y5CAC6enmGacNTx1zEolmZtidt75P29fhYBWAerUjyrfTvwrWZrxvH52uyaQ/IP2eAcY9NcuoA4wIwrNOSPHKAcT+c/huDcRdHp/0j9/5r7Mb0324PgOEclOSGDPNx+/oR17HQLswwJ/A9Yy4CgD1yVoZpwQUjrmHhvTTDnMRtSY4bcR0A7J4npV2zh2iBG6VHtCrJNRnmRF6aZN/RVgLArlqb5NsZpgFXpzWGEb0pw5zMpSTvH3EdAOyaD2W46/8bRlwHy/ZPu5FtqJPqOe8As+eVGe66vynt3T8T+KMMd2JvSbujHoDZcEySWzPcdf9N4y2Fu1qbYR40c+dxdZIjRlsNAPfk/mnX5KGu99fF/VOT+70Md4KXklyc5IDRVgPAXR2Q5BsZ9lr/u6Othnu0KsklGfZEX5Bkn5HWA8BPrUlyXoa9xn8r7myfGc/OsCd7KcknIuoAY9onyScz/PX9+LEWxM45N8Of9PPj43eAMdwv7dPRoa/rHx1pPeyCI5LclOFP/iVJHjDSmgAW0bq0+5eGvp5/P+1mO2bQazP8C2ApyVXxkzaAIRyTYe9m3/F49UhrYjfsleRzGeeFcEuSU8ZZFsBCeGWG/Z35jsen05rBDDssw/42/a7H2fFkIYA9cZ8kf5fxrtub0z7WZw68IMPsmX5Px6VJnjDKygBqeVKG22jl7o7tSZ47ysro5i8y3gvkzhfJ2UkOHWNxAHPuwCTvTHJHxr1Wv3WMxdHXyoz3ffqOx01JzkiyYvglAsydvdLuP9qc8a/Pn09rA3Po0CQbMv6LZint520vixcPQNKexHZy2lPZprgmX5Xk4MFXyaDWZ5zfp9/TcXXaO3ZPmQMW0eq0d+SXZ7rr8JYkRw29UMZxfJKtme7FtJS2k8/bkxw78FoBZsFjk/xl2rVvymvvbUmeNvBaGdmLM/7NF/d0fDPJHyZ52KArBhjXw5O8JcNvmLWzxx1JfnPQFTOZU5Jsy/Qvsh2P69KeJfzaJA8dbOUA/a1L8qIkZ2a6+5Xu6dieBXsS3CI+Jed1Sd6d2V37lrTvmi5LcsXysSnt6Um3pj17+NYkt081QaC8NUn2S9sgZb/lY12SRywfRy0fs3qT2fYkr0/yvqknMqZZjdrQTk7yD7H/LUA125K8JskHp57I2Bb1J1WXpL0LfkEW998AoJqtaT8XPmfqiUxhUd+h3+nJST6e5JCpJwLAHvlekpPS9lBfSIse9KTdlfnpJL889UQA2C0b0p7PftnUE5mSx5MmVyb51STnTT0RAHbZBWkbvCx0zBPfH9/pR2nfuSylPYDAJxcAs20pydvy0z3UF55w/bznJ3l/7JoGMKs2JXlV2telLBP0u3dYkg8ked7UEwHgZ5yX5NQkG6eeyKzxHfrd25z2Tv3UtAe9ADCt7yd5Y5ITI+Z3yzv0e3dEknel/RwCgPF9NG3Xyuunnsgs8w793m1Me7j/M9M2VQFgHJcleU6Sl0TM75Wg77wvJHl82vOBvbAAhrMxbd+NRyX57MRzmRs+ct89e6d9v/6naRsWALDnbkzy9iR/k/ZzYnaBoO+Z/dK253tjbH0KsLs2JHlH2q+LfjDxXOaWoPexKslvJTk9yVMmngvAPFhK8uUk70nysSR3TDud+Sfo/R2d5LS0HX+OmHguALPmuiT/lOTvk1wx8VxKEfThrEjy1LS7M1+Y5PBppwMwmeuTnJvkI2nvyrdPO52aBH0cK5I8Lu2BCCcmOS7JmklnBDCcrUm+mnaH+meSXJz2ETsDEvRp7JMW9V9PC/2xSR4WPyME5s+2JFelPafjP5NcmORrSW6bclKLSNBnx75J1ic5cofjiCQHLx+HJFm9fOw30RyBxXFrkh8nuT3tEdhb0n5WtjHtrvRrklyd9vCXH04zRQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoJf/B8xz4fvvsPYbAAAAAElFTkSuQmCC'
                            }
                            className={classes.imgUnCheckbox}
                          />
                        )}
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
              <Box pb={2} id="capture-alignment">
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <MixTitle title="Alignment to corporate objectives" bold />
                    {opportunity?.corporate_objectives?.map((item, index) => {
                      return (
                        <FormControl
                          key={index}
                          fullWidth
                          margin="none"
                          variant="outlined"
                          disabled
                          className={classes.formControl}
                        >
                          <Typography className={classes.iteratorNumber}>
                            {index + 1}.
                          </Typography>
                          <TextField
                            defaultValue={item.name}
                            fullWidth
                            variant="outlined"
                            disabled
                            className={classes.disabled}
                          />
                        </FormControl>
                      );
                    })}
                  </Grid>
                </Grid>
              </Box>
              <Box pb={2} id="capture-bf-category">
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <MixTitle title="Business Focus Category" bold />
                    {opportunity?.benefit_focuses?.map((item, index) => {
                      return (
                        <FormControl
                          fullWidth
                          key={index}
                          margin="none"
                          variant="outlined"
                          disabled
                          className={classes.formControl}
                        >
                          <Typography className={classes.iteratorNumber}>
                            {index + 1}.
                          </Typography>
                          <TextField
                            defaultValue={item.benefit_focus_name}
                            fullWidth
                            variant="outlined"
                            disabled
                            className={classes.disabled}
                          />
                        </FormControl>
                      );
                    })}
                  </Grid>
                  <Grid item xs={6}>
                    <MixTitle title="Business Benefit Value" bold />
                    {opportunity?.benefit_focuses?.map((item, index) => {
                      return (
                        <FormControl
                          fullWidth
                          key={index}
                          margin="none"
                          variant="outlined"
                          disabled
                          className={classes.formControl}
                        >
                          <Typography className={classes.iteratorNumber}>
                            {index + 1}.
                          </Typography>
                          <TextField
                            defaultValue={item.benefit_focus_value.band}
                            fullWidth
                            variant="outlined"
                            disabled
                            className={classes.disabled}
                          />
                        </FormControl>
                      );
                    })}
                  </Grid>
                </Grid>
              </Box>
              <Box pb={2} id="capture-metrics">
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <MixTitle title="Business Benefit Metrics" bold />
                    {opportunity?.benefit_metrics?.map((item, index) => {
                      return (
                        <FormControl
                          fullWidth
                          key={index}
                          margin="none"
                          variant="outlined"
                          disabled
                          className={classes.formControl}
                        >
                          <Typography className={classes.iteratorNumber}>
                            {index + 1}.
                          </Typography>
                          <TextField
                            defaultValue={item}
                            fullWidth
                            variant="outlined"
                            disabled
                            className={classes.disabled}
                          />
                        </FormControl>
                      );
                    })}
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </SectionBox>
        </Box>

        <Box id="shape" width="100%">
          <Box
            pt={6}
            pb={3}
            display="flex"
            flexDirection="column"
            alignItems="center"
          >
            <Typography variant="h3">
              <b>{opportunity?.title}</b>
            </Typography>
            <Typography color="primary">{href}</Typography>
          </Box>
          <SectionBox component="section" className={classes.section}>
            <Box m={1}>
              <Box id="title-shape">
                <Typography gutterBottom={true} variant="h3">
                  Shape
                </Typography>
              </Box>
              <Box pb={2} id="opp-definition">
                {opportunity?.definition && (
                  <Grid container item xs={12}>
                    <Grid item xs={12}>
                      <MixTitle title="Opportunity Definition" bold />
                      <Box className={classes.textArea}>
                        {
                          <Box
                            dangerouslySetInnerHTML={{
                              __html: opportunity?.definition.replace(
                                /\n/g,
                                '<br />',
                              ),
                            }}
                          />
                        }
                      </Box>
                    </Grid>
                  </Grid>
                )}
              </Box>
              {opportunity?.opportunity_dataset && (
                <Box pb={2} id="dataset-required">
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <MixTitle title="Dataset Required" bold />
                      {opportunity?.opportunity_dataset?.map((item, index) => {
                        return (
                          <Box mb={2} key={index}>
                            <FormControl
                              fullWidth
                              margin="none"
                              variant="outlined"
                              disabled
                              className={classes.formControl}
                            >
                              <Typography className={classes.iteratorNumber}>
                                {index + 1}.
                              </Typography>
                              <TextField
                                defaultValue={item.name}
                                fullWidth
                                variant="outlined"
                                disabled
                                className={classes.disabled}
                              />
                            </FormControl>
                            {item.description}
                          </Box>
                        );
                      })}
                    </Grid>
                  </Grid>
                </Box>
              )}
              <Box pb={2} id="opp-parameters">
                <Grid container spacing={2}>
                  <Grid item xs={12} md={4}>
                    <MixTitle title="Opportunity Type" bold />
                    <TextField
                      defaultValue={opportunity?.opportunity_type?.name}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <MixTitle title="Opportunity Size" bold />
                    <TextField
                      defaultValue={opportunity?.opportunity_size?.t_shirt_size}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <MixTitle title="Success Probability" bold />
                    <TextField
                      defaultValue={opportunity?.probability_success?.band}
                      fullWidth
                      variant="outlined"
                      disabled
                      className={classes.disabled}
                    />
                  </Grid>
                </Grid>
              </Box>
              {opportunity?.opportunity_roles && (
                <Box mb={2} id="resource-requirements">
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Box id="title-resource">
                        <MixTitle title="Resource Requirements" bold />
                      </Box>
                      {opportunity?.opportunity_roles?.map((item, index) => {
                        return (
                          <Box key={index} pb={2} id={`role-${index}`}>
                            <Box>
                              <Typography className={classes.title}>
                                {item?.role_name}
                              </Typography>
                            </Box>
                            <TableContainer>
                              <Table className={classes.table}>
                                <TableBody
                                  className={classes.sectionBox}
                                  key={item.id}
                                >
                                  <TableRow>
                                    <TableCell>Profile Role</TableCell>
                                    <TableCell>
                                      {item?.profile_role?.name}
                                    </TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell>Role Proficiency</TableCell>
                                    <TableCell>
                                      {item?.profile_role_level_id}
                                    </TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell>Duration</TableCell>
                                    <TableCell>{item?.day}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell>Effort</TableCell>
                                    <TableCell>{item?.effort}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell>Location</TableCell>
                                    <TableCell>
                                      {item?.location?.name}
                                    </TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell>Resource Team</TableCell>
                                    <TableCell>
                                      {item?.resource_team?.name}
                                    </TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell>Skills</TableCell>
                                    <TableCell>
                                      {item?.skills
                                        ?.map(skill => skill.skill_name)
                                        .join(', ')}
                                    </TableCell>
                                  </TableRow>
                                </TableBody>
                              </Table>
                            </TableContainer>
                          </Box>
                        );
                      })}
                    </Grid>
                  </Grid>
                </Box>
              )}
              <Box pb={2} id="review-summary">
                {opportunity?.review_summary.review_content && (
                  <Grid container item xs={12}>
                    <Grid item xs={12}>
                      <MixTitle title="Review Summary" bold />
                      <Box className={classes.textArea}>
                        {
                          <Box
                            dangerouslySetInnerHTML={{
                              __html: opportunity?.review_summary.review_content.replace(
                                /\n/g,
                                '<br />',
                              ),
                            }}
                          />
                        }
                      </Box>
                    </Grid>
                  </Grid>
                )}
              </Box>
              <Box pt={2} id="priority-score">
                <Grid container item xs={12}>
                  <Grid>
                    <Box display="flex">
                      <MixTitle title={'Priority score:'} bold />
                      <Box ml={1}>
                        <Typography
                          variant="body1"
                          children={
                            opportunity?.opp_priority_score?.priority_score
                          }
                        />
                      </Box>
                      <Box ml={1}>
                        <Typography
                          variant="body2"
                          style={{ fontStyle: 'italic', fontWeight: 'bold' }}
                          children={
                            opportunity?.opp_priority_score?.priority_score &&
                            opportunity?.opp_priority_score?.priority_score < 50
                              ? '( Low )'
                              : opportunity?.opp_priority_score
                                  ?.priority_score &&
                                opportunity?.opp_priority_score
                                  ?.priority_score < 80
                              ? '( Medium )'
                              : '( High )'
                          }
                        />
                      </Box>
                    </Box>
                    <Box display="flex">
                      <MixTitle title={'Last Activity:'} bold />
                      <Box ml={1}>
                        <Typography
                          variant="body1"
                          children={
                            opportunity?.opp_priority_score
                              ?.priority_score_updated_log
                          }
                        />
                      </Box>
                    </Box>
                    <Box display="flex">
                      <MixTitle title={'Last Update:'} bold />
                      <Box ml={1}>
                        <Typography
                          variant="body1"
                          children={moment(
                            opportunity?.opp_priority_score
                              ?.priority_score_updated_at,
                          ).format('DD/MM/YYYY h:ma')}
                        />
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>

              {/* <Box ml={2}>
            <Typography gutterBottom={true} variant="h5">
              Playbooks
            </Typography>
            <Box ml={2}>
              <table className={classes.table}>
                {playbookOfOpp?.map(item => {
                  return (
                    <tbody className={classes.sectionBox} key={item.id}>
                      <tr>
                        <td className={classes.bold}>{item.name}</td>
                      </tr>
                      <tr>
                        <td>
                          {item?.playbook_items
                            ?.map(playbook => playbook.title)
                            .join(', ')}
                        </td>
                      </tr>
                    </tbody>
                  );
                })}
              </table>
            </Box>
          </Box> */}
            </Box>
          </SectionBox>
        </Box>
      </Grid>
    </Grid>
  );
};
