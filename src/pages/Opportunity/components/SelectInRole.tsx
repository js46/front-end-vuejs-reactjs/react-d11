import React from 'react';
import { Grid, Box, Select, FormControl, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { StyledRating } from '../../MyProfile/shared';
import { IListRole, IListLocations } from '../../../services/profile.services';
import { IResourceTeam } from '../../../services/references.services';
import { useTranslation } from 'react-i18next';
import { MixTitle } from '../../../components';

const useStyles = makeStyles(theme => ({
  selectItem: {
    color: theme.palette.text.primary,
  },
  subMenuInRole: {
    fontWeight: 'bold',
  },
  iconMinimize: {
    borderRadius: 15,
    backgroundColor: theme.palette.grey[300],
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
  divider: {
    backgroundColor: theme.palette.divider,
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  },
  mb20: {
    marginBottom: 20,
  },
  removeButton: {
    textTransform: 'capitalize',
    marginLeft: 20,
  },
  attachButton: {
    textTransform: 'capitalize',
  },
}));

interface ISelectInRoleProps {
  name: string;
  field: string;
  fieldForProfileRole?: string;
  item?: { id: number; name: string };
  profileRole?: IListRole[];
  profileRoleLevelId?: number;
  locations?: IListLocations[];
  resourceTeam?: IResourceTeam[];
  hasRoleProfiency?: boolean;
  disabled?: boolean;
  optionNone?: boolean;
  // onBlur: (event: React.FocusEvent<HTMLInputElement>) => void;
  handleChange: (e: React.ChangeEvent<any>) => void;
  handleChangeRating?: (e: React.ChangeEvent<{}>, value: number | null) => void;
  helperText?: string;
}
export const SelectInRole: React.FC<ISelectInRoleProps> = ({
  name,
  item,
  field,
  fieldForProfileRole,
  profileRole,
  profileRoleLevelId,
  locations,
  resourceTeam,
  hasRoleProfiency,
  disabled,
  // onBlur,
  handleChange,
  optionNone,
  handleChangeRating,
  helperText,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <Grid container alignItems="center">
      <Grid item xs={6}>
        <Box mt={1}>
          <Grid container justify="flex-end">
            <Grid item xs={5} container justify="flex-end">
              <Box component="span" mt={1} mr={2}>
                <MixTitle title={name} isHelper helperText={helperText} />
              </Box>
            </Grid>
            <Grid item container xs={7}>
              <FormControl
                required
                fullWidth
                margin="normal"
                variant="outlined"
              >
                <Select
                  value={item?.id}
                  fullWidth
                  id={field}
                  name={field}
                  // onBlur={onBlur}
                  onChange={handleChange}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                  disabled={disabled}
                >
                  {optionNone && !profileRole && (
                    <MenuItem key={0} value={0}>
                      <span>
                        <em> None </em>
                      </span>
                    </MenuItem>
                  )}
                  {profileRole &&
                    profileRole.map(role => (
                      <MenuItem key={role.id} value={role.id}>
                        <span>{role.name}</span>
                      </MenuItem>
                    ))}
                  {locations &&
                    locations.map(location => (
                      <MenuItem key={location.id} value={location.id}>
                        <span>{location.name} </span>
                      </MenuItem>
                    ))}
                  {resourceTeam &&
                    resourceTeam.map(resource => (
                      <MenuItem key={resource.id} value={resource.id}>
                        <span>{resource.name} </span>
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>
      </Grid>
      {hasRoleProfiency && (
        <Grid item xs={6}>
          <Box ml={4} display="flex" alignItems="center">
            <Box component="span" mt={1} mr={2}>
              <MixTitle
                title={t('role_proficiency')}
                isHelper
                helperText={t('help_icon.role_proficiency')}
              />
            </Box>
            <StyledRating
              name={fieldForProfileRole}
              onChange={handleChangeRating}
              value={profileRoleLevelId}
              max={4}
              precision={1}
              icon={<div className={classes.iconMinimize} />}
              disabled={disabled}
            />
          </Box>
        </Grid>
      )}
    </Grid>
  );
};
