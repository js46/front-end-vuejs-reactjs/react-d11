import React from 'react';
import {
  Grid,
  Box,
  Select,
  FormControl,
  MenuItem,
  IconButton,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { StyledRating } from '../../MyProfile/shared';
import { ISkillsInRole } from '../../../services/profile.services';
import { IOpportunitySkillsInRole } from '../../../services/opportunity.services';
import { useTranslation } from 'react-i18next';
import { MixTitle } from '../../../components';

const useStyles = makeStyles(theme => ({
  selectItem: {
    color: theme.palette.text.primary,
  },
  subMenuInRole: {
    fontWeight: 'bold',
  },
  iconMinimize: {
    borderRadius: 15,
    backgroundColor: theme.palette.grey[300],
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
  divider: {
    backgroundColor: theme.palette.divider,
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  },
  mb20: {
    marginBottom: 20,
  },
  removeButton: {
    textTransform: 'capitalize',
    marginLeft: 20,
  },
  attachButton: {
    textTransform: 'capitalize',
  },
}));
interface ISelectInSkillProps {
  name: string;
  item: IOpportunitySkillsInRole;
  index: number;
  field: string;
  skillList?: ISkillsInRole[];
  disabled?: boolean;
  // onBlur: (event: React.FocusEvent<HTMLInputElement>) => void;
  handleChange: (e: React.ChangeEvent<any>) => void;
  handleChangeRatingInSkill: (
    e: React.ChangeEvent<{}>,
    value: number | null,
  ) => void;
  removable?: boolean;
  handleRemove?: () => void;
}

export const SelectInSkill: React.FC<ISelectInSkillProps> = ({
  item,
  name,
  index,
  field,
  skillList,
  disabled,
  handleChange,
  handleChangeRatingInSkill,
  handleRemove,
  removable,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <Grid container alignItems="center">
      <Grid item xs={6}>
        <Box mt={1}>
          <Grid container justify="flex-end">
            <Grid item xs={5} container justify="flex-end">
              <Box component="span" mt={1} mr={2}>
                <MixTitle
                  title={name}
                  isHelper
                  helperText={t('help_icon.skill')}
                />
              </Box>
            </Grid>
            <Grid item xs={7}>
              <FormControl
                required
                fullWidth
                margin="normal"
                variant="outlined"
              >
                <Select
                  value={item?.skill_id}
                  id={field}
                  name={field}
                  // onBlur={onBlur}
                  onChange={handleChange}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                  disabled={disabled}
                >
                  {skillList &&
                    skillList.map((skill, iterator) => (
                      <MenuItem key={iterator} value={skill.id}>
                        <span>{skill.name} </span>
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <Box ml={2} display="flex" alignItems="center">
          <Box component="span" mt={1} mr={2}>
            <MixTitle
              title={`Skill ${index + 1} Proficiency`}
              isHelper
              helperText={t('help_icon.skill_proficiency')}
            />
          </Box>
          <StyledRating
            name={name + '-' + Date.now()}
            value={item.proficiency_level_id}
            max={4}
            precision={1}
            onChange={handleChangeRatingInSkill}
            icon={<div className={classes.iconMinimize} />}
            disabled={disabled}
          />
          {removable && (
            <IconButton
              size="medium"
              aria-label="delete"
              color="secondary"
              disabled={disabled}
              onClick={handleRemove}
            >
              <RemoveCircleOutlineIcon />
            </IconButton>
          )}
        </Box>
      </Grid>
    </Grid>
  );
};
