import React, { useState, useReducer, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Box,
  Menu,
  MenuItem,
  Typography,
  Button,
  ExpansionPanelDetails,
} from '@material-ui/core';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import { IOpportunity } from '../../../../services/opportunity.services';
import {
  ExpansionPanelStyled,
  ExpansionPanelSummaryStyled,
} from '../../../../components/styled';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import {
  AnalyseStateContextProvider,
  AnalyseDispatchContextProvider,
  analyseReducer,
  AnalyseAction,
} from './AnalyseContext';
import { useTranslation } from 'react-i18next';
import { ConfigESIndex } from '../../../../store/config/selector';
import { UserProfileState } from '../../../../store/user/selector';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  ProgressUpdate,
  FormulateApproachPanel,
  PlaybookTreePanel,
} from './components';
import { SidebarList } from '../Sidebar';
import { OpportunityPossibleActions } from '../../../../store/opportunity/selector';
import {
  updateOpportunityAllocateResource,
  updateOpportunityReourceAllocationOption,
} from '../../../../services/opportunity.services';
import { RequestDetailOpportunity } from '../../../../store/opportunity/actions';
import { InitAnalyseStateProps } from './types';
import { getAllPlaybookOfOpportunity } from '../../../../services/playbook.services';
import { useEffectOnlyOnce } from '../../../../hooks';

enum ExpandableAnalyse {
  ResourceAllocation = 'Resource Allocation',
  FormulateApproach = ' Formulate Approach',
  PlayBook = 'Playbook',
  ProgressReport = 'Progress Report',
}

enum ResourceAllocationAction {
  AllocateResources = 'Allocate Resources',
  FinishResourceAllocation = 'Finish Resource Allocation',
}

const PHASE_ANALYSE: number = 3;
interface AnalyseTabProps {
  chosenOpportunity: IOpportunity;
  toggleModal: () => void;
  setType: React.Dispatch<any>;
}

export const AnalyseTab: React.FC<AnalyseTabProps> = ({
  chosenOpportunity,
  toggleModal,
  setType,
}) => {
  const initAnalyseState: InitAnalyseStateProps = {
    opportunity: chosenOpportunity,
    playbookDetailList: [],
    isOpenAddingNewPlaybook: false,
    isOpenAddingNewPlaybookItem: false,
    isOpenDeleteDialog: false,
    isOpenPlaybookItemDeleteDialog: false,
    chosenPlaybookItemId: 0,
    chosenPlaybookTemplate: undefined,
    newPlaybookName: '',
    playbookTemplate: [],
    chosenTemplate: [],
    chosenPlaybook: undefined,
    userNotes: undefined,
    currentChosenEmailList: [],
    playbookItemTemplate: {
      playbook_item: {
        titleName: '',
        detail: '',
        playbook_item_parent_id: 0,
        item_order: 0,
      },
      label_item: [0],
    },
  };

  const [analyseState, analyseDispatch] = useReducer(
    analyseReducer,
    initAnalyseState,
  );
  const ES_INDICES = useSelector(ConfigESIndex);
  const possibleActions = useSelector(OpportunityPossibleActions);

  const [expanded, setExpanded] = useState<ExpandableAnalyse | undefined>();
  const dispatch = useDispatch();
  const [analysisState, setAnalysisState] = useState<ExpandableAnalyse>();
  // const [resourceAllocationState, setResourceAllocation] = useState<
  //   ResourceAllocationAction
  // >(ResourceAllocationAction.AllocateResources);
  const [anchorElButton, setAnchorElButton] = React.useState<any>(null);

  const userProfileState = useSelector(UserProfileState);
  const isTeamLead =
    userProfileState.team_lead_flg &&
    userProfileState.resource_team.resource_team_id ===
      chosenOpportunity?.resource_team?.id;

  const onChangePanel = (name: ExpandableAnalyse) => () => {
    setExpanded(expanded !== name ? name : undefined);
  };
  const renderExpansionIcon = (current: ExpandableAnalyse) => {
    return current === expanded ? <RemoveCircleIcon /> : <AddCircleIcon />;
  };
  useEffectOnlyOnce(() => {
    invokePlaybookOfOpportunity();
  });
  const invokePlaybookOfOpportunity = async () => {
    const response = await getAllPlaybookOfOpportunity(
      chosenOpportunity?.id ?? 0,
    );
    if (response.success) {
      // analyseDispatch(updatePlaybookList(data.data));
      analyseDispatch({
        type: AnalyseAction.UPDATE_PLAYBOOK,
        payload: response.data,
      });
    }
  };
  const customQuery = () => {
    return {
      query: {
        bool: {
          filter: [
            {
              term: {
                delete_flg: false,
              },
            },
            {
              term: {
                'created_by.id': userProfileState.id,
              },
            },
          ],
        },
      },
    };
  };

  const openMenu = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setAnchorElButton(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorElButton(null);
  };

  const handleSelectResourceAllocation = (
    action: ResourceAllocationAction,
  ) => async () => {
    handleCloseMenu();
    const actionId =
      possibleActions.find(item => item.display_name === action)?.action_id ??
      0;
    switch (action) {
      case ResourceAllocationAction.AllocateResources:
        setType(SidebarList.Team);
        toggleModal();
        // setResourceAllocation(
        //   ResourceAllocationAction.FinishResourceAllocation,
        // );
        if (actionId) {
          await updateOpportunityAllocateResource(chosenOpportunity.id, {
            action_id: actionId,
          });
        }

        break;
      case ResourceAllocationAction.FinishResourceAllocation:
        setAnalysisState(ExpandableAnalyse.FormulateApproach);
        setExpanded(ExpandableAnalyse.FormulateApproach);
        await updateOpportunityReourceAllocationOption(chosenOpportunity.id, {
          action_id: actionId,
        });
        break;
    }
    if (actionId) {
      dispatch(RequestDetailOpportunity(chosenOpportunity.id));
    }
  };
  const { t } = useTranslation();

  useEffect(() => {
    chosenOpportunity?.opportunity_state &&
    chosenOpportunity?.opportunity_state?.id >=
      AnalyseAction.UPDATE_STATE_TO_ANALYSIS
      ? setAnalysisState(ExpandableAnalyse.FormulateApproach)
      : setAnalysisState(ExpandableAnalyse.ResourceAllocation);
  }, [chosenOpportunity]);

  return (
    <AnalyseStateContextProvider value={analyseState}>
      <AnalyseDispatchContextProvider value={analyseDispatch}>
        <Box>
          <ReactiveBase
            app={ES_INDICES.user_note_index_name}
            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
            headers={{
              Authorization: window.localStorage.getItem('jwt'),
            }}
          >
            <ReactiveList
              componentId="Notes"
              dataField="notes"
              renderResultStats={() => null}
              renderNoResults={() => <></>}
              defaultQuery={customQuery}
              loader={<></>}
              onData={({ data }) => {
                analyseDispatch({
                  type: AnalyseAction.GET_PLAYBOOK_NOTES,
                  payload: data,
                });
              }}
              render={() => {
                return <></>;
              }}
            />
          </ReactiveBase>
          <ExpansionPanelStyled
            expanded={expanded === ExpandableAnalyse.ResourceAllocation}
            onChange={onChangePanel(ExpandableAnalyse.ResourceAllocation)}
            disabled={
              (!userProfileState.isAdmin && !isTeamLead) ||
              analysisState !== ExpandableAnalyse.ResourceAllocation ||
              (chosenOpportunity?.opportunity_phase &&
                chosenOpportunity?.opportunity_phase?.id !== PHASE_ANALYSE)
            }
          >
            <ExpansionPanelSummaryStyled
              aria-controls="data-source-content"
              id="data-source-header"
              expandIcon={renderExpansionIcon(
                ExpandableAnalyse.ResourceAllocation,
              )}
            >
              <Typography variant="h5">{t('resource_allocate')}</Typography>
            </ExpansionPanelSummaryStyled>
            <ExpansionPanelDetails>
              <Box
                flexDirection="column"
                display="flex"
                flex={1}
                mt={3}
                mb={2}
                alignItems="center"
              >
                {chosenOpportunity?.opportunity_phase?.id === PHASE_ANALYSE && (
                  <Menu
                    id="action-menu"
                    anchorEl={anchorElButton}
                    keepMounted
                    open={Boolean(anchorElButton)}
                    onClose={handleCloseMenu}
                  >
                    <MenuItem
                      onClick={handleSelectResourceAllocation(
                        ResourceAllocationAction.AllocateResources,
                      )}
                    >
                      {ResourceAllocationAction.AllocateResources}
                    </MenuItem>
                    <MenuItem
                      disabled={
                        chosenOpportunity?.opportunity_state?.id !==
                        AnalyseAction.UPDATE_STATE_TO_SCHEDULING
                      }
                      onClick={handleSelectResourceAllocation(
                        ResourceAllocationAction.FinishResourceAllocation,
                      )}
                    >
                      {ResourceAllocationAction.FinishResourceAllocation}
                    </MenuItem>
                  </Menu>
                )}
                <Button
                  aria-controls="action-menu"
                  aria-haspopup="true"
                  onClick={openMenu}
                  endIcon={<ExpandMoreIcon />}
                  variant="outlined"
                  color="primary"
                >
                  Resource Allocation Option
                </Button>
              </Box>
            </ExpansionPanelDetails>
          </ExpansionPanelStyled>

          <ExpansionPanelStyled
            expanded={expanded === ExpandableAnalyse.FormulateApproach}
            onChange={onChangePanel(ExpandableAnalyse.FormulateApproach)}
            disabled={
              analysisState !== ExpandableAnalyse.FormulateApproach &&
              chosenOpportunity?.opportunity_phase &&
              chosenOpportunity?.opportunity_phase?.id <= PHASE_ANALYSE
            }
          >
            <ExpansionPanelSummaryStyled
              aria-controls="data-source-content"
              id="data-source-header"
              expandIcon={renderExpansionIcon(
                ExpandableAnalyse.FormulateApproach,
              )}
            >
              <Typography variant="h5">{t('formulate_approach')}</Typography>
            </ExpansionPanelSummaryStyled>
            <ExpansionPanelDetails>
              <Box width="100%">
                <FormulateApproachPanel />
              </Box>
            </ExpansionPanelDetails>
          </ExpansionPanelStyled>
          <ExpansionPanelStyled
            expanded={expanded === ExpandableAnalyse.PlayBook}
            onChange={onChangePanel(ExpandableAnalyse.PlayBook)}
            disabled={
              analysisState !== ExpandableAnalyse.FormulateApproach &&
              chosenOpportunity?.opportunity_phase &&
              chosenOpportunity?.opportunity_phase?.id <= PHASE_ANALYSE
            }
          >
            <ExpansionPanelSummaryStyled
              aria-controls="data-source-content"
              id="data-source-header"
              expandIcon={renderExpansionIcon(ExpandableAnalyse.PlayBook)}
            >
              <Typography variant="h5">{t('playbooks')}</Typography>
            </ExpansionPanelSummaryStyled>
            <ExpansionPanelDetails>
              {/*  {0 ? <PlaybookPanelBeta /> : <PlaybookPanel />} */}
              <PlaybookTreePanel />
            </ExpansionPanelDetails>
          </ExpansionPanelStyled>
          <ExpansionPanelStyled
            expanded={expanded === ExpandableAnalyse.ProgressReport}
            onChange={onChangePanel(ExpandableAnalyse.ProgressReport)}
            disabled={
              analysisState !== ExpandableAnalyse.FormulateApproach &&
              chosenOpportunity?.opportunity_phase &&
              chosenOpportunity?.opportunity_phase?.id <= PHASE_ANALYSE
            }
          >
            <ExpansionPanelSummaryStyled
              aria-controls="data-source-content"
              id="data-source-header"
              expandIcon={renderExpansionIcon(ExpandableAnalyse.ProgressReport)}
            >
              <Typography variant="h5">{t('process_update')}</Typography>
            </ExpansionPanelSummaryStyled>
            <ExpansionPanelDetails>
              <ProgressUpdate />
            </ExpansionPanelDetails>
          </ExpansionPanelStyled>
        </Box>
      </AnalyseDispatchContextProvider>
    </AnalyseStateContextProvider>
  );
};

export default AnalyseTab;
