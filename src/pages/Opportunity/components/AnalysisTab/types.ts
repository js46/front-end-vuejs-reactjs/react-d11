import { IOpportunity } from '../../../../services/opportunity.services';
import {
  OpportunityPlaybookT,
  PlaybookT,
  NoteResponseT,
} from '../../../../services/playbook.services';

export interface IPlaybookItem {
  id?: number;
  titleName: string;
  detail: string;
  item_order: number;
  playbook_item_parent_id: number;
}
export interface InitAnalyseStateProps {
  opportunity: IOpportunity;
  playbookDetailList: OpportunityPlaybookT[];
  isOpenAddingNewPlaybook: boolean;
  isOpenAddingNewPlaybookItem: boolean;
  isOpenDeleteDialog: boolean;
  isOpenPlaybookItemDeleteDialog: boolean;
  chosenPlaybookItemId: number;
  chosenPlaybookTemplate: string | undefined;
  newPlaybookName: string;
  chosenPlaybookId?: number;
  playbookTemplate: PlaybookT[];
  chosenTemplate: IPlaybookItem[];
  chosenPlaybook: number | 'new_playbook' | undefined;
  userNotes: NoteResponseT[] | undefined;
  currentChosenEmailList?: number[];
  playbookItemTemplate: {
    playbook_item: IPlaybookItem;
    label_item: number[];
  };
}
export enum TaskStates {
  todo = 'Todo',
  inProgress = 'InProgress',
  done = 'Done',
}
export enum TaskStateColors {
  blank = '',
  red = '#ff2929',
  green = '#4EE24E',
  amber = '#FFBF00',
}
export interface ICommonObj {
  id: number;
  title: string;
}

export interface ITaskState {
  label: TaskStates;
  color?: TaskStateColors;
  order: number;
}
