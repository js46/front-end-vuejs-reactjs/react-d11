//analyse context

import React from 'react';
import {
  OpportunityPlaybookT,
  PlaybookT,
  NoteResponseT,
} from '../../../../services/playbook.services';

import { IOpportunity } from '../../../../services/opportunity.services';
import { InitAnalyseStateProps, IPlaybookItem } from './types';

export class AnalyseAction {
  static readonly ADD_PLAYBOOK = 'ADD_PLAYBOOK';
  static readonly UPDATE_PLAYBOOK = 'UPDATE_PLAYBOOK';
  static readonly CHANGE_PLAYBOOK = 'CHANGE_PLAYBOOK';
  static readonly UPDATE_PLAYBOOK_LIST = 'UPDATE_PLAYBOOK_LIST';
  static readonly OPEN_DIALOG_ADD_PLAYBOOK = 'OPEN_DIALOG_ADD_PLAYBOOK';
  static readonly GET_PLAYBOOK_TEMPLATE = 'GET_PLAYBOOK_TEMPLATE';
  static readonly CHANGE_TEMPLATE = 'CHANGE_TEMPLATE';
  static readonly CLOSE_NEW_PLAYBOOK_DIALOG = 'CLOSE_NEW_PLAYBOOK_DIALOG';
  static readonly ASK_DELETE_PLAYBOOK = 'ASK_DELETE_PLAYBOOK';
  static readonly CLOSE_DELETE_PLAYBOOK_DIALOG = 'CLOSE_DELETE_PLAYBOOK_DIALOG';
  static readonly DELETE_PLAYBOOK = 'DELETE_PLAYBOOK';
  static readonly ASK_DELETE_PLAYBOOK_ITEM = 'ASK_DELETE_PLAYBOOK_ITEM';
  static readonly CLOSE_PLAYBOOK_ITEM_DELETE_DIALOG =
    'CLOSE_PLAYBOOK_ITEM_DELETE_DIALOG';
  static readonly OPEN_ADD_NEW_PLAYBOOK = 'OPEN_ADD_NEW_PLAYBOOK';
  static readonly CLOSE_ADD_NEW_PLAYBOOK = 'CLOSE_ADD_NEW_PLAYBOOK';
  static readonly OPEN_ADD_NEW_PLAYBOOK_ITEM = 'OPEN_ADD_NEW_PLAYBOOK_ITEM';
  static readonly CLOSE_ADD_NEW_PLAYBOOK_ITEM = 'CLOSE_ADD_NEW_PLAYBOOK_ITEM';
  static readonly GET_PLAYBOOK_NOTES = 'GET_PLAYBOOK_NOTES';
  static readonly UPDATE_CURRENT_CHOSEN_EMAIL_LIST =
    'UPDATE_CURRENT_CHOSEN_EMAIL_LIST';
  static readonly UPDATE_STATE_TO_SCHEDULING = 11;
  static readonly UPDATE_STATE_TO_ANALYSIS = 12;
  static readonly PHASE_ANALYSE = 3;
}

const initOpportunity: IOpportunity = {
  id: 0,
  proposer_email: '',
  proposer_business_unit: '',
  title: '',
  detail: '',
  requested_completion_date: '',
  estimated_completion_date: '',
  screen_id: 0,
  completed_step: 0,
  business_units: [{ id: 0, name: '' }],
  sponsor_business_unit: { id: 0, name: '' },
  processes: [{ id: 0, name: '' }],
  probability_success: { id: 0, band: '', description: '', points: 0 },
  corporate_objectives: [{ id: 0, name: '', archive: false }],
  benefit_focuses: [
    {
      id: 0,
      benefit_focus_id: 0,
      benefit_focus_name: '',
      benefit_focus_value: { id: 0, band: '', nominal_value: 0, points: 0 },
    },
  ],
  benefit_metrics: [''],
  review_summary: {
    id: 0,
    review_content: '',
    created_by_user: {},
    updated_by_user: {},
  },
  last_updated_by: { id: 0, name: '' },
};

const initContext: InitAnalyseStateProps = {
  opportunity: initOpportunity,
  playbookDetailList: [],
  isOpenAddingNewPlaybook: false,
  isOpenAddingNewPlaybookItem: false,
  isOpenDeleteDialog: false,
  isOpenPlaybookItemDeleteDialog: false,
  chosenPlaybookItemId: 0,
  chosenPlaybookTemplate: undefined,
  newPlaybookName: '',
  playbookTemplate: [],
  chosenTemplate: [],
  chosenPlaybook: undefined,
  userNotes: undefined,
  currentChosenEmailList: [],
  playbookItemTemplate: {
    playbook_item: {
      titleName: '',
      detail: '',
      playbook_item_parent_id: 0,
      item_order: 0,
    },
    label_item: [0],
  },
};

export const AnalyseStateContext = React.createContext(initContext);
export const AnalyseDispatchContext = React.createContext<any>(initContext);

export const AnalyseStateContextProvider = AnalyseStateContext.Provider;
export const AnalyseDispatchContextProvider = AnalyseDispatchContext.Provider;

type UpdatePlaybookT = {
  type: typeof AnalyseAction.UPDATE_PLAYBOOK;
  payload: OpportunityPlaybookT[];
};

export const updatePlaybookList = (data: OpportunityPlaybookT[]) => {
  return {
    type: AnalyseAction.UPDATE_PLAYBOOK,
    payload: data,
  };
};

type AddPlaybookT = {
  type: typeof AnalyseAction.ADD_PLAYBOOK;
  payload: OpportunityPlaybookT[];
};

type ChangePlaybookT = {
  type: typeof AnalyseAction.CHANGE_PLAYBOOK;
  payload: number | 'new_playbook' | undefined;
};

type UpdatePlaybookListT = {
  type: typeof AnalyseAction.UPDATE_PLAYBOOK;
  payload: OpportunityPlaybookT[];
};

type OpenAddPlaybookDialogT = {
  type: typeof AnalyseAction.OPEN_DIALOG_ADD_PLAYBOOK;
};

type GetPlaybookTemplateT = {
  type: typeof AnalyseAction.GET_PLAYBOOK_TEMPLATE;
  payload: PlaybookT[];
};

type ChangeTemplateT = {
  type: typeof AnalyseAction.CHANGE_TEMPLATE;
  payload: {
    pbTemplate: string;
    pbId?: number;
    template: IPlaybookItem[];
  };
};

type CloseNewPlaybookDialogT = {
  type: typeof AnalyseAction.CLOSE_NEW_PLAYBOOK_DIALOG;
};
type AskDeletePlaybookT = {
  type: typeof AnalyseAction.ASK_DELETE_PLAYBOOK;
};
type CloseDeletePlaybookDialogT = {
  type: typeof AnalyseAction.CLOSE_DELETE_PLAYBOOK_DIALOG;
};
type DeletePlaybookT = {
  type: typeof AnalyseAction.DELETE_PLAYBOOK;
};
type AskDeletePlaybookItemT = {
  type: typeof AnalyseAction.ASK_DELETE_PLAYBOOK_ITEM;
  payload: number;
};
type ClosePlaybookItemDeleteDialogT = {
  type: typeof AnalyseAction.CLOSE_PLAYBOOK_ITEM_DELETE_DIALOG;
};
type OpenAddNewPlaybookT = {
  type: typeof AnalyseAction.OPEN_ADD_NEW_PLAYBOOK;
};
type CloseAddNewPlaybookT = {
  type: typeof AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK;
};
type OpenAddNewPlaybookItemT = {
  type: typeof AnalyseAction.OPEN_ADD_NEW_PLAYBOOK_ITEM;
  payload: {
    playbook_item: IPlaybookItem;
    label_item: number[];
  };
};
type CloseAddNewPlaybookItemT = {
  type: typeof AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK_ITEM;
};
type GetPlaybookNotesT = {
  type: typeof AnalyseAction.GET_PLAYBOOK_NOTES;
  payload: NoteResponseT[];
};
type UpdateChosenEmailListT = {
  type: typeof AnalyseAction.UPDATE_CURRENT_CHOSEN_EMAIL_LIST;
  payload: number[] | undefined;
};

type AnalyseActionT =
  | AddPlaybookT
  | ChangePlaybookT
  | UpdatePlaybookT
  | UpdatePlaybookListT
  | OpenAddPlaybookDialogT
  | GetPlaybookTemplateT
  | ChangeTemplateT
  | CloseNewPlaybookDialogT
  | AskDeletePlaybookT
  | CloseDeletePlaybookDialogT
  | DeletePlaybookT
  | AskDeletePlaybookItemT
  | ClosePlaybookItemDeleteDialogT
  | OpenAddNewPlaybookT
  | OpenAddNewPlaybookItemT
  | CloseAddNewPlaybookT
  | CloseAddNewPlaybookItemT
  | GetPlaybookNotesT
  | UpdateChosenEmailListT;

export const analyseReducer = (
  state: InitAnalyseStateProps,
  // action: { type: AnalyseAction; payload?: any },
  action: AnalyseActionT,
): InitAnalyseStateProps => {
  switch (action.type) {
    case AnalyseAction.ADD_PLAYBOOK: {
      return {
        ...state,
        playbookDetailList: action.payload,
        chosenPlaybook: action.payload
          ? action.payload[action.payload.length - 1].id
          : undefined,
      };
    }
    case AnalyseAction.UPDATE_PLAYBOOK: {
      return {
        ...state,
        playbookDetailList: action.payload,
      };
    }
    case AnalyseAction.CHANGE_PLAYBOOK: {
      return { ...state, chosenPlaybook: action.payload };
    }
    // case AnalyseAction.UPDATE_PLAYBOOK_LIST: {
    //   return {
    //     ...state,
    //     isOpenAddingNewPlaybook: false,
    //     chosenPlaybookTemplate: undefined,
    //     chosenPlaybook: action.payload,
    //   };
    // }
    case AnalyseAction.OPEN_DIALOG_ADD_PLAYBOOK: {
      return {
        ...state,
        isOpenAddingNewPlaybook: true,
        chosenPlaybook: undefined,
      };
    }
    case AnalyseAction.GET_PLAYBOOK_TEMPLATE: {
      return {
        ...state,
        playbookTemplate: action.payload,
      };
    }
    case AnalyseAction.CHANGE_TEMPLATE: {
      return {
        ...state,
        chosenPlaybookTemplate: action.payload?.pbTemplate,
        chosenTemplate: action.payload?.template,
        chosenPlaybookId: action.payload?.pbId,
      };
    }
    case AnalyseAction.CLOSE_NEW_PLAYBOOK_DIALOG: {
      return {
        ...state,
        isOpenAddingNewPlaybook: false,
        chosenPlaybookTemplate: undefined,
      };
    }
    case AnalyseAction.ASK_DELETE_PLAYBOOK: {
      return {
        ...state,
        isOpenDeleteDialog: true,
      };
    }
    case AnalyseAction.CLOSE_DELETE_PLAYBOOK_DIALOG: {
      return {
        ...state,
        isOpenDeleteDialog: false,
      };
    }
    case AnalyseAction.DELETE_PLAYBOOK: {
      return {
        ...state,
        isOpenDeleteDialog: false,
        chosenPlaybook: undefined,
      };
    }
    case AnalyseAction.ASK_DELETE_PLAYBOOK_ITEM: {
      return {
        ...state,
        isOpenPlaybookItemDeleteDialog: true,
        chosenPlaybookItemId: action.payload,
      };
    }
    case AnalyseAction.CLOSE_PLAYBOOK_ITEM_DELETE_DIALOG: {
      return {
        ...state,
        isOpenPlaybookItemDeleteDialog: false,
      };
    }
    case AnalyseAction.OPEN_ADD_NEW_PLAYBOOK: {
      return {
        ...state,
        isOpenAddingNewPlaybook: true,
      };
    }
    case AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK: {
      return {
        ...state,
        isOpenAddingNewPlaybook: false,
        playbookItemTemplate: {
          playbook_item: {
            playbook_item_parent_id: 0,
            item_order: 0,
            titleName: '',
            detail: '',
          },
          label_item: [],
        },
      };
    }
    case AnalyseAction.OPEN_ADD_NEW_PLAYBOOK_ITEM: {
      return {
        ...state,
        isOpenAddingNewPlaybookItem: true,
        playbookItemTemplate: action.payload,
      };
    }
    case AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK_ITEM: {
      return {
        ...state,
        isOpenAddingNewPlaybookItem: false,
      };
    }
    case AnalyseAction.GET_PLAYBOOK_NOTES: {
      return {
        ...state,
        userNotes: action.payload,
      };
    }
    case AnalyseAction.UPDATE_CURRENT_CHOSEN_EMAIL_LIST: {
      return {
        ...state,
        currentChosenEmailList: action.payload,
      };
    }
    default:
      return state;
  }
};
