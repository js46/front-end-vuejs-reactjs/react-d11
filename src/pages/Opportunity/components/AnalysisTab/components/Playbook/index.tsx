import React, { useContext, useEffect, useState } from 'react';
import {
  Box,
  Button,
  Dialog,
  Grid,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';
import { AnalysePermission } from '../../../../types';
import {
  AnalyseAction,
  AnalyseDispatchContext,
  AnalyseStateContext,
} from '../../AnalyseContext';
import {
  getPlaybookList,
  selectPlaybook,
  PlaybookT,
} from '../../../../../../services/playbook.services';
import { useTranslation } from 'react-i18next';
import AddPlaybookItemDialog from '../AddPlaybookItemDialog';
import PlaybookTemplate from './components/PlaybookTemplate';
import {
  AnalysePermissionState,
  OpportunityState,
} from '../../../../../../store/opportunity/selector';
import { useSelector } from 'react-redux';
import {
  IOpportunityTypes,
  getOpportunityTypes,
} from '../../../../../../services/references.services';
import { PlaybookTable } from './components';
import { AddCircleOutline } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { useEffectOnlyOnce } from '../../../../../../hooks';

const useStyles = makeStyles(theme => ({
  icon: {
    color: 'white',
  },
  button: {
    padding: '6px 18px',
  },
}));
const playbookActive = 2;

export default function PlaybookPanel() {
  const classes = useStyles();
  const opportunity = useSelector(OpportunityState);
  const analysePermission = useSelector(AnalysePermissionState);
  const analyseState = useContext(AnalyseStateContext);
  const analyseDispatch = useContext(AnalyseDispatchContext);
  const [opportunityTypeSelectID, setOpportunityTypeSelectID] = useState<
    number
  >(opportunity?.opportunity_type?.id ?? 0);
  const [opportunityTypes, setOpportunityTypes] = useState<IOpportunityTypes[]>(
    [],
  );
  const [playbookFilters, setPlaybookFilters] = useState<PlaybookT[]>([]);

  const getOpportunityTypeLists = async () => {
    const response = await getOpportunityTypes();
    if (response.success) setOpportunityTypes(response.data.list ?? []);
  };

  useEffectOnlyOnce(() => {
    Promise.all([invokePlaybookTemplateList(), getOpportunityTypeLists()]);
  });
  useEffect(() => {
    let playbookTemp = analyseState.playbookTemplate;
    if (opportunityTypeSelectID !== 0) {
      playbookTemp = playbookTemp.filter(
        item => item.opportunity_type.id === opportunityTypeSelectID,
      );
    }
    setPlaybookFilters(playbookTemp ?? []);
  }, [analyseState.playbookTemplate, opportunityTypeSelectID]);
  useEffect(() => {
    if (analyseState.chosenPlaybook === 'new_playbook') {
      analyseDispatch({ type: AnalyseAction.OPEN_DIALOG_ADD_PLAYBOOK });
    }
    handleChoosePlaybookForOpportunity();
    // eslint-disable-next-line
  }, [analyseState.chosenPlaybook]);

  useEffect(() => {
    if (analyseState.playbookDetailList) {
      analyseState.playbookDetailList.forEach(pb => {
        if (pb.selected_flg) {
          analyseDispatch({
            type: AnalyseAction.CHANGE_PLAYBOOK,
            payload: pb.id,
          });
        }
      });
    }
  }, [analyseDispatch, analyseState.playbookDetailList]);

  const invokePlaybookTemplateList = async () => {
    const data = await getPlaybookList([{ key: 'limit', value: 1000 }]);
    if (data.success) {
      analyseDispatch({
        type: AnalyseAction.GET_PLAYBOOK_TEMPLATE,
        payload: data.data.list,
      });
    }
  };

  const handleChoosePlaybookForOpportunity = async () => {
    if (
      analyseState.chosenPlaybook &&
      analyseState.chosenPlaybook !== 'new_playbook'
    ) {
      const response = await selectPlaybook(analyseState.chosenPlaybook);
      if (response.success) {
        //do something
      }
    }
  };

  const handleChangeTemplate = (ev: React.ChangeEvent<any>) => {
    const pbTemplate = analyseState.playbookTemplate.find(
      item => item.name === ev.target.value,
    );
    if (ev.target.value === 'blank') {
      analyseDispatch({
        type: AnalyseAction.CHANGE_TEMPLATE,
        payload: {
          pbTemplate: ev.target.value,
          template: [],
        },
      });
    } else if (pbTemplate) {
      const updatedChosenTemplate =
        pbTemplate.playbook_items?.map(pbItem => ({
          id: pbItem.id,
          titleName: pbItem?.name,
          detail: pbItem?.description,
          playbook_item_parent_id: pbItem.playbook_item_parent_id,
          item_order: pbItem.item_order,
        })) ?? [];
      analyseDispatch({
        type: AnalyseAction.CHANGE_TEMPLATE,
        payload: {
          pbTemplate: ev.target.value,
          pbId: pbTemplate.id,
          template: updatedChosenTemplate,
        },
      });
    }
  };

  const handleCancel = () => {
    analyseDispatch({
      type: AnalyseAction.CLOSE_NEW_PLAYBOOK_DIALOG,
    });
  };

  const { t } = useTranslation();

  return (
    <>
      <Box padding={1.5} width="100%">
        <Box mb={2} display="flex" justifyContent="space-between">
          <Grid container alignItems="center">
            <Grid item container justify="flex-end" alignItems="center" xs={12}>
              <Box>
                {analysePermission?.includes(
                  AnalysePermission.PlaybookCreate,
                ) && (
                  <Button
                    color="primary"
                    variant="contained"
                    className={classes.button}
                    onClick={() =>
                      analyseDispatch({
                        type: AnalyseAction.OPEN_ADD_NEW_PLAYBOOK,
                      })
                    }
                    startIcon={<AddCircleOutline className={classes.icon} />}
                  >
                    <Typography className={classes.icon}>Add</Typography>
                  </Button>
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
        <PlaybookTable playbookLists={analyseState.playbookDetailList} />
      </Box>
      <Dialog
        open={analyseState.isOpenAddingNewPlaybook}
        onClose={() => {
          analyseDispatch({
            type: AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK,
          });
        }}
        maxWidth="md"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Box padding="24px 50px" minWidth="700px">
          <Box textAlign="center" mb={5}>
            <Typography component="p" variant="h3">
              {t('new_playbook')}
            </Typography>
          </Box>
          <Box display="flex" flexDirection="column" mb={3}>
            <Box mb={2}>
              <Box mb={2}>
                <Typography component="p" variant="caption">
                  {t('choose_opportunity_type')}
                </Typography>
              </Box>
              <Select
                variant="outlined"
                fullWidth
                value={opportunityTypeSelectID}
                onChange={event =>
                  setOpportunityTypeSelectID(Number(event.target.value))
                }
              >
                <MenuItem value={0}>All</MenuItem>
                {opportunityTypes?.map((item, index) => (
                  <MenuItem key={index} value={item.id}>
                    {item.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={2}>
              <Typography component="p" variant="caption">
                {t('choose_playbook_template')}
              </Typography>
            </Box>
            <Select
              variant="outlined"
              fullWidth
              onChange={handleChangeTemplate}
              value={analyseState.chosenPlaybookTemplate ?? ''}
            >
              <MenuItem value="blank">Blank template</MenuItem>
              {playbookFilters
                ?.filter(item => item.archive === playbookActive)
                ?.map((playbookItem, index) => (
                  <MenuItem key={index} value={playbookItem.name}>
                    {playbookItem.name}
                  </MenuItem>
                ))}
            </Select>
          </Box>
          <Box>
            {analyseState.chosenPlaybookTemplate && (
              <PlaybookTemplate onCancel={handleCancel} />
            )}
          </Box>
        </Box>
      </Dialog>
      {analyseState.isOpenAddingNewPlaybookItem && (
        <Dialog
          open={analyseState.isOpenAddingNewPlaybookItem}
          onClose={() => {
            analyseDispatch({
              type: AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK_ITEM,
            });
          }}
          maxWidth="md"
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <AddPlaybookItemDialog
            onCancel={() => {
              analyseDispatch({
                type: AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK_ITEM,
              });
            }}
          />
        </Dialog>
      )}
    </>
  );
}
