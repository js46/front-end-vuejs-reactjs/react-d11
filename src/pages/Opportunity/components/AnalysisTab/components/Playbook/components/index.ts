import loadable from '@loadable/component';

export const PlaybookTable = loadable(() => import('./PlaybookTable'));
