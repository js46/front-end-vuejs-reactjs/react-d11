import React, { useState, useContext } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  TableSortLabel,
  Typography,
  Box,
  Link,
} from '@material-ui/core';
import {
  deletePlaybook,
  OpportunityPlaybookT,
  getAllPlaybookOfOpportunity,
} from '../../../../../../../services/playbook.services';
import { makeStyles } from '@material-ui/core/styles';
import { TableAction } from '../../../../../../../components';
import {
  OpportunityState,
  AnalysePermissionState,
} from '../../../../../../../store/opportunity/selector';
import { AnalysePermission } from '../../../../../types';
import { useSelector } from 'react-redux';
import { DeleteDialog } from '../../../../../../../components';
import { useDisableMultipleClick } from '../../../../../../../hooks';
import { AnalyseAction, AnalyseDispatchContext } from '../../../AnalyseContext';
import { ProfileLink } from '../../../../../../Expert/ProfileLink';
import { useTranslation } from 'react-i18next';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  table: {
    '& tr td:last-child': {
      width: 100,
    },
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
}));

interface Data {
  id: string;
  name: string;
  task_count: string;
  issue_count?: number;
  created_by: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
  disableSorting?: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'name',
    label: 'Playbook name',
    disablePadding: true,
  },
  {
    id: 'task_count',
    label: 'Tasks',
    disablePadding: true,
  },
  {
    id: 'issue_count',
    label: 'Issues',
    disablePadding: true,
  },
  {
    id: 'created_by',
    label: 'Created by',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    //onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              disabled={headCell.disableSorting}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>
  );
}

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}

interface PlaybookTableProps {
  playbookLists: OpportunityPlaybookT[];
}

const PlaybookTable: React.FC<PlaybookTableProps> = ({ playbookLists }) => {
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('desc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const opportunity = useSelector(OpportunityState);
  const { t } = useTranslation();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const analyseDispatch = useContext(AnalyseDispatchContext);
  const analysePermission = useSelector(AnalysePermissionState);
  const [isOpenDeleteDialog, setIsOpenDeleteDialog] = useState<boolean>(false);
  const [playbookToDeleteId, setPlaybookToDeleteId] = useState<
    OpportunityPlaybookT
  >();

  const invokePlaybookOfOpportunity = async () => {
    const response = await getAllPlaybookOfOpportunity(opportunity?.id ?? 0);
    if (response.success) {
      // analyseDispatch(updatePlaybookList(data.data));
      analyseDispatch({
        type: AnalyseAction.UPDATE_PLAYBOOK,
        payload: response.data,
      });
    }
  };

  const handleDeletePlaybook = async () => {
    if (!playbookToDeleteId || isSubmitting) return;
    await debounceFn();
    const data = await deletePlaybook(playbookToDeleteId.id as number);
    if (data.success) {
      analyseDispatch({
        type: AnalyseAction.DELETE_PLAYBOOK,
      });
      invokePlaybookOfOpportunity?.();
    }
    setIsOpenDeleteDialog(false);
    endRequest();
  };

  return (
    <>
      {playbookLists && playbookLists.length > 0 ? (
        <TableContainer>
          <Table className={classes.table}>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {playbookLists?.map((item: any, index: number) => (
                <TableRow key={index} hover classes={{ root: classes.rowLink }}>
                  <TableCell component="td" scope="row">
                    <Link
                      component={RouterLink}
                      color="inherit"
                      to={`/opportunity/${opportunity?.id}/playbook-${item.id}`}
                    >
                      {item?.name}
                    </Link>
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item.task_count}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    {item.issue_count}
                  </TableCell>
                  <TableCell component="td" scope="row">
                    <ProfileLink
                      id={
                        item.updated_by_user?.name !== ''
                          ? item.updated_by_user?.id
                          : item.created_by_user?.id
                      }
                      title={
                        item.updated_by_user?.name !== ''
                          ? item.updated_by_user?.name
                          : item.created_by_user?.name
                      }
                    />
                  </TableCell>

                  <TableCell component="td" scope="row" padding="none">
                    <TableAction
                      hiddenItem={['edit', 'view']}
                      onClickDelete={() => {
                        setIsOpenDeleteDialog(true);
                        setPlaybookToDeleteId(item);
                      }}
                      disabledDelete={
                        !analysePermission?.includes(
                          AnalysePermission.PlaybookDelete,
                        )
                      }
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      ) : (
        <Box display="flex" flex={1} justifyContent="center">
          <Typography>{t('no_playbook')}</Typography>
        </Box>
      )}

      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header="Delete Playbook"
        message={
          <Typography component="p" variant="body1">
            Are you sure you want to delete{' '}
            <Typography component="span" variant="caption">
              {playbookToDeleteId?.name ?? ''}
            </Typography>
          </Typography>
        }
        handleClose={() => {
          setIsOpenDeleteDialog(false);
        }}
        handleDelete={handleDeletePlaybook}
        disabled={isSubmitting}
      />
    </>
  );
};

export default PlaybookTable;
