import React, { useContext, useEffect } from 'react';
import {
  Box,
  Typography,
  Button,
  TextField,
  DialogActions,
} from '@material-ui/core';
import { Formik, Form, FieldArray, FieldArrayRenderProps } from 'formik';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles } from '@material-ui/styles';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import {
  AnalyseStateContext,
  AnalyseDispatchContext,
  AnalyseAction,
} from '../../../AnalyseContext';
import {
  getAllPlaybookOfOpportunity,
  addNewPlaybook,
  INewPlaybookItemTree,
} from '../../../../../../../services/playbook.services';
import { useDisableMultipleClick } from '../../../../../../../hooks';
import { useTranslation } from 'react-i18next';
import { IPlaybookItem } from '../../../types';
import { CustomContextMenu } from '../../../../../../../components';

const useStyles = makeStyles(theme => ({
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

interface InitValueT {
  template: INewPlaybookItemTree[];
  isOpenCollapse: string[];
  playbookName: string;
}

interface PlaybookTemplateProps {
  onCancel: () => void;
}

export const PlaybookTemplate: React.FC<PlaybookTemplateProps> = ({
  onCancel,
}) => {
  const classes = useStyles();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const analyseState = useContext(AnalyseStateContext);
  const analyseDispatch = useContext(AnalyseDispatchContext);
  const [playbookItemTree, setPlaybookItemTree] = React.useState<
    INewPlaybookItemTree[]
  >([]);
  useEffect(() => {
    let parentItems = analyseState.chosenTemplate?.filter(
      item => item.playbook_item_parent_id === 0,
    );
    let childItems = analyseState.chosenTemplate?.filter(
      item => item.playbook_item_parent_id !== 0,
    );
    if (parentItems) {
      const treeItems = convertItemTree(parentItems);
      if (treeItems) {
        setPlaybookItemTree(treeItems);
      }
    }

    function convertItemTree(items: IPlaybookItem[]) {
      let list: INewPlaybookItemTree[] = [];
      for (let i = 0; i < items.length; i++) {
        let itemTree: INewPlaybookItemTree = {
          title: items[i]?.titleName,
          description: items[i]?.detail,
          item_order: items[i]?.item_order,
        };
        //find child
        let childFound = childItems?.filter(
          item => item.playbook_item_parent_id === items[i]?.id,
        );
        if (childFound) {
          itemTree.children = convertItemTree(childFound);
        }
        list.push(itemTree);
      }
      return list;
    }
  }, [analyseDispatch, analyseState, analyseState.chosenTemplate]);
  const initialValues: InitValueT = {
    template: playbookItemTree,
    isOpenCollapse: [],
    playbookName:
      analyseState.chosenPlaybookTemplate !== 'blank' &&
      analyseState.chosenPlaybookTemplate
        ? analyseState.chosenPlaybookTemplate
        : '',
  };
  const invokePlaybookOfOpportunity = async () => {
    const data = await getAllPlaybookOfOpportunity(analyseState.opportunity.id);
    if (data.success) {
      console.log('data', data);
      analyseDispatch({
        type: AnalyseAction.ADD_PLAYBOOK,
        payload: data.data,
      });
    }
  };

  const handleAddNewPlayBook = async (
    template: INewPlaybookItemTree[],
    playbookName: string,
  ) => {
    if (isSubmitting) return;
    await debounceFn();
    const playbookPayload = {
      name: playbookName,
      origin_playbook_id: analyseState.chosenPlaybookId,
      playbook_items: template,
    };
    const response = await addNewPlaybook(
      playbookPayload,
      analyseState.opportunity.id,
    );
    if (response.success) {
      invokePlaybookOfOpportunity();
      endRequest();
      analyseDispatch({
        type: AnalyseAction.CLOSE_NEW_PLAYBOOK_DIALOG,
      });
    } else {
      endRequest();
    }
  };
  const { t } = useTranslation();
  const renderPlaybookItem = (
    nameTemplate: string,
    item: INewPlaybookItemTree,
    index: number,
    values: InitValueT,
    setFieldValue: (
      field: string,
      value: any,
      shouldValidate?: boolean | undefined,
    ) => void,
    arrayHelpers: FieldArrayRenderProps,
    handleChange: (e: string | React.ChangeEvent<any>) => void,
    position: number[],
  ) => {
    const label = position.join('.');
    const isOpenCollapse = values.isOpenCollapse.indexOf(label) > -1;
    const animatedStyle = {
      marginRight: '5px',
      transition: '0.3s all',
      transform: isOpenCollapse ? 'rotate(90deg)' : 'rotate(0deg)',
      '&:hover': {
        cursor: 'poiter',
      },
    };
    const countChildren = item?.children?.length ?? 0;
    let options = [
      {
        name: 'Delete',
        disabled: countChildren > 0,
        onClick: () => {
          arrayHelpers.remove(index);
        },
      },
    ];
    if (position.length < 3) {
      options.push({
        name: 'Add Sub Item',
        disabled: false,
        onClick: () => {
          setFieldValue(`${nameTemplate}.${index}.children.${countChildren}`, {
            title: '',
            description: '',
            item_order: countChildren,
          });
        },
      });
    }

    return (
      <>
        <Box mb={3} key={`${nameTemplate}.${index}`}>
          <Box mt={2} ml={position.length * 2}>
            <Box display="flex" mb={2} alignItems="center">
              <Box
                display="flex"
                minWidth={110}
                component="span"
                alignItems="center"
              >
                <Typography component="span" variant="caption">
                  Step {label}
                </Typography>
                <Box ml={1} display="flex" alignItems="center">
                  <ArrowForwardIosIcon
                    style={animatedStyle}
                    className={classes.icon}
                    fontSize="small"
                    key={label}
                    onClick={() => {
                      if (isOpenCollapse) {
                        setFieldValue(
                          'isOpenCollapse',
                          values.isOpenCollapse.filter(child => {
                            return child !== label;
                          }) ?? [],
                        );
                      } else {
                        setFieldValue('isOpenCollapse', [
                          ...values.isOpenCollapse,
                          label,
                        ]);
                      }
                    }}
                  />
                </Box>
              </Box>

              <Box mr={1} display="flex">
                <Typography component="span" variant="caption">
                  Name:
                </Typography>
              </Box>

              <TextField
                variant="outlined"
                fullWidth
                name={`${nameTemplate}.${index}.title`}
                key={`${nameTemplate}.${index}.title`}
                value={item?.title ?? ''}
                onChange={handleChange}
              />
              <Box ml={2}>
                <CustomContextMenu
                  key={`${nameTemplate}.${index}`}
                  options={options}
                  size={'small'}
                />
              </Box>
            </Box>
            {isOpenCollapse && (
              <Box pl={9}>
                <Box>
                  <Box mb={1}>
                    <Typography component="p" variant="caption">
                      Detail:
                    </Typography>
                  </Box>
                  <TextField
                    fullWidth
                    multiline
                    rows={5}
                    name={`${nameTemplate}.${index}.description`}
                    key={`${nameTemplate}.${index}.description`}
                    onChange={handleChange}
                    variant="outlined"
                    value={item?.description ?? ''}
                  />
                </Box>
              </Box>
            )}
          </Box>
        </Box>
        <FieldArray
          key={`${label}.${index}.children`}
          name={`${nameTemplate}.${index}.children`}
        >
          {arrayHelpers => (
            <Box>
              {item?.children &&
                item?.children?.length > 0 &&
                item.children.map((itemChild, indexChild) =>
                  renderPlaybookItem(
                    `${nameTemplate}.${index}.children`,
                    itemChild,
                    indexChild,
                    values,
                    setFieldValue,
                    arrayHelpers,
                    handleChange,
                    [...position, indexChild + 1],
                  ),
                )}
            </Box>
          )}
        </FieldArray>
      </>
    );
  };
  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={values => {
        handleAddNewPlayBook(values.template, values.playbookName);
      }}
    >
      {({ values, handleChange, setFieldValue }) => {
        return (
          <Form>
            <Box mb={4} display="flex">
              <Box display="flex" alignItems="center" minWidth="125px">
                <Typography component="span" variant="caption">
                  {t('playbook_name')}
                </Typography>
              </Box>

              <TextField
                fullWidth
                variant="outlined"
                name="playbookName"
                value={values.playbookName}
                onChange={handleChange}
              />
            </Box>
            <FieldArray name="template" key={`${values.playbookName}.template`}>
              {arrayHelpers => (
                <Box>
                  {values.template &&
                    values.template.length > 0 &&
                    values.template.map((item, index) =>
                      renderPlaybookItem(
                        'template',
                        item,
                        index,
                        values,
                        setFieldValue,
                        arrayHelpers,
                        handleChange,
                        [index + 1],
                      ),
                    )}
                  <Button
                    variant="outlined"
                    color="primary"
                    key={`${values.playbookName}.template`}
                    startIcon={<ControlPointIcon />}
                    onClick={() => {
                      arrayHelpers.push({
                        title: '',
                        description: '',
                        item_order: values.template.length ?? 1,
                      });
                    }}
                  >
                    Add Step
                  </Button>
                </Box>
              )}
            </FieldArray>
            <DialogActions>
              <Button color="default" onClick={onCancel} variant="outlined">
                Cancel
              </Button>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                autoFocus
                disabled={!values.playbookName || isSubmitting}
              >
                Save
              </Button>
            </DialogActions>
          </Form>
        );
      }}
    </Formik>
  );
};

export default PlaybookTemplate;
