import React, { useState, useEffect, useContext, useMemo } from 'react';
import moment from 'moment';
import {
  Box,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Button,
  TableHead,
  Dialog,
  DialogActions,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import InfoIcon from '@material-ui/icons/Info';

import {
  ResponsePlayBookT,
  NoteT,
  addNoteToItem,
  editNoteToItem,
  NoteResponseT,
  getAllPlaybookOfOpportunity,
  removeNoteOnPlaybook,
} from '../../../../../services/playbook.services';
import {
  addOpportunityDetailNote,
  updateOpportunityDetailNote,
  deleteOpportunityDetailNote,
} from '../../../../../services/opportunity.services';
import { useSelector, useDispatch } from 'react-redux';
import { NoteDialog } from '../../../../../components/NoteDialog';
import {
  AnalyseStateContext,
  AnalyseDispatchContext,
  updatePlaybookList,
} from '../AnalyseContext';
import { DeleteDialog, TableAction } from '../../../../../components';
import { useDisableMultipleClick } from '../../../../../hooks';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../../../store/user/selector';
import { UpdatedOpportunityDetail } from '../../../../../store/opportunity/actions';
import {
  AnalysePermissionState,
  idOpportunityState,
} from '../../../../../store/opportunity/selector';
import { AnalysePermission } from '../../../types';

const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  headTable: {
    minWidth: 732,
  },
  tableRow: {
    transition: '0.3s all',
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: '#eee',
    },
  },
}));

interface Data {
  name: string;
  author: string;
  date: string;
  action: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'author',
    label: 'Author',
  },
  {
    id: 'date',
    label: 'Date',
  },
  {
    id: 'action',
    label: 'Actions',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.id === 'action' ? 'center' : undefined}
            style={headCell.id === 'action' ? { width: '120px' } : undefined}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface NoteWrapperProps {
  data: ResponsePlayBookT;
  values: {
    note: NoteT[] | undefined;
    title: string;
    detail: string;
  };
  entity_type?: string;
}

export const NoteWrapper: React.FC<NoteWrapperProps> = ({
  data,
  values,
  entity_type,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userProfileState = useSelector(UserProfileState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const analysePermission = useSelector(AnalysePermissionState);
  const opportunityID = useSelector(idOpportunityState);
  const analyseState = useContext(AnalyseStateContext);
  const analyseDispatch = useContext(AnalyseDispatchContext);

  const emptyNoteContent = {
    name: '',
    author: userProfileState.full_name,
    content: '',
    date: new Date(),
  };
  const initNoteContent = {
    name:
      data?.notes?.find(note => note?.id === currentEditNoteId)?.title ?? '',
    author: userProfileState.full_name,
    content:
      data?.notes?.find(note => note?.id === currentEditNoteId)?.content ?? '',
    date: new Date(),
  };

  const [anchorElement, setAnchorElement] = useState<any>(null);
  const [isOpenAddNote, setOpenAddNote] = useState(false);
  // const [isOpenEditNote, setOpenEditNote] = useState(false);
  const [currentEditNoteId, setCurrentEditNoteId] = useState<number>();
  const [noteContent, setNoteContent] = useState(initNoteContent);
  const [collapse, setCollapse] = useState(false);
  // const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [noteToDeleteId, setNoteToDeleteId] = useState<number>();
  const [informationDialog, setInformationDialog] = useState<NoteT>();

  const { t } = useTranslation();
  useEffect(() => {
    const dataChoose = entity_type ? values.note : data?.notes;
    if (currentEditNoteId && dataChoose) {
      let item = dataChoose?.find(note => note?.id === currentEditNoteId);
      const updatedNoteContent = {
        name: item?.title ?? '',
        author: userProfileState.full_name,
        content: item?.content ?? '',
        date: new Date(),
      };
      setNoteContent(updatedNoteContent);
    }
    // eslint-disable-next-line
  }, [currentEditNoteId]);

  const noteSelectionOptions = useMemo(() => {
    const currentNotes = values.note?.map(item => item?.title);
    if (!currentNotes) return analyseState.userNotes ?? [];
    return (
      analyseState.userNotes?.filter(
        item => !currentNotes.includes(item.title),
      ) ?? []
    );
  }, [analyseState.userNotes, values.note]);

  const handleUpdatePlaybookOfOpportunity = async () => {
    const response = await getAllPlaybookOfOpportunity(
      analyseState?.opportunity?.id,
    );

    if (response.success) {
      analyseDispatch(updatePlaybookList(response.data));
    }
  };

  const handleAddNote = async (content: any) => {
    if (isSubmitting) return;
    await debounceFn();
    if (entity_type) {
      const response = await addOpportunityDetailNote(
        {
          title: noteContent.name,
          content: content,
        },
        opportunityID,
      );
      if (response.success) {
        setNoteContent(emptyNoteContent);
        dispatch(UpdatedOpportunityDetail(response.data));
        setOpenAddNote(false);
      }
    } else {
      const playbookItemId = data.id;
      const payload = {
        notes: [
          {
            title: noteContent.name,
            content: content,
            created_by_user_id: userProfileState.id,
          },
        ],
      };
      const response = await addNoteToItem(payload, playbookItemId);
      if (response.success) {
        setNoteContent(emptyNoteContent);
        await handleUpdatePlaybookOfOpportunity();
        setOpenAddNote(false);
      }
    }

    endRequest();
  };

  const handleEditNote = async (content: any) => {
    //const noteId = data?.notes?.find(item => item.id === currentEditNoteId)?.id;
    if (!currentEditNoteId || isSubmitting) return;
    await debounceFn();
    const payload = {
      title: noteContent.name,
      content: content,
      created_by_user_id: userProfileState.id,
    };
    if (entity_type) {
      const response = await updateOpportunityDetailNote(
        payload,
        currentEditNoteId ?? 0,
      );
      if (response.success) {
        setNoteContent(emptyNoteContent);
        setCurrentEditNoteId(undefined);
        dispatch(UpdatedOpportunityDetail(response.data));
      }
    } else {
      const response = await editNoteToItem(payload, currentEditNoteId ?? 0);
      if (response.success) {
        setNoteContent(emptyNoteContent);
        setCurrentEditNoteId(undefined);
        await handleUpdatePlaybookOfOpportunity();
      }
    }

    endRequest();
  };

  const handleAddNoteFromExistingOne = (note: NoteResponseT) => async () => {
    if (isSubmitting) return;
    await debounceFn();
    const item = {
      title: note.title,
      content: note.content,
      user_note_id: note.created_by.id,
      created_by_user_id: userProfileState.id,
    };
    const payload = {
      notes: [item],
    };
    if (entity_type) {
      const response = await addOpportunityDetailNote(item, opportunityID);
      if (response.success) {
        setNoteContent(emptyNoteContent);
        dispatch(UpdatedOpportunityDetail(response.data));
        setOpenAddNote(false);
      }
    } else {
      const response = await addNoteToItem(payload, data.id);
      if (response.success) {
        await handleUpdatePlaybookOfOpportunity();
        setAnchorElement(null);
      }
    }

    endRequest();
  };

  const handleRemoveNote = async (idNote?: number) => {
    if (isSubmitting || !idNote) return;
    await debounceFn();
    if (entity_type) {
      const response = await deleteOpportunityDetailNote(idNote);
      if (response.success) {
        dispatch(UpdatedOpportunityDetail(response.data));
      }
    } else {
      const response = await removeNoteOnPlaybook(idNote);
      if (response.success) {
        await handleUpdatePlaybookOfOpportunity();
      }
    }
    setNoteToDeleteId(undefined);
    endRequest();
  };

  const handleChangeNoteContent = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    event.persist();
    switch (event.target.name) {
      case 'note_name': {
        setNoteContent({ ...noteContent, name: event.target.value });
        break;
      }
      case 'note_detail_content': {
        setNoteContent({ ...noteContent, content: event.target.value });
        break;
      }
    }
  };

  const visualizeNoteDialog = (
    type: 'show_info' | 'edit_note' | 'delete_note',
    event:
      | React.MouseEvent<any, MouseEvent>
      | React.MouseEvent<HTMLButtonElement, MouseEvent>,
    param?: NoteT | number,
  ) => {
    switch (type) {
      case 'show_info': {
        setInformationDialog(param as NoteT);
        break;
      }
      case 'edit_note': {
        event.stopPropagation();

        setCurrentEditNoteId(param as number);
        break;
      }
      case 'delete_note': {
        event.stopPropagation();

        setNoteToDeleteId(param as number);
        break;
      }
      default:
        return;
    }
  };

  const renderNoteTitle = (noteTitle: string, index: number) => {
    const arrNoteTitle = analyseState.userNotes?.filter(
      (note: NoteResponseT) => note.title === noteTitle,
    );
    if (arrNoteTitle && arrNoteTitle.length > 1) {
      return `${noteTitle} (${index + 1})`;
    } else {
      return noteTitle;
    }
  };

  const animatedStyle = {
    transition: '0.3s all',
    transform: collapse ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'poiter',
    },
  };
  const verifyDisable = (param: string) => {
    switch (param) {
      case 'add_note': {
        return !analysePermission?.includes(
          AnalysePermission.PlaybookItemNoteCreate,
        );
      }
      default:
        return false;
    }
  };
  // console.log('analysePermission', analysePermission);
  return (
    <React.Fragment>
      <Box display="flex" alignItems="center" mb={2} mt={2}>
        <Typography component="span" variant="caption">
          {t('notes')}
        </Typography>
        <Box ml={2} display="flex">
          <IconButton
            size="small"
            onClick={() => {
              setCollapse(!collapse);
            }}
            disabled={
              !analysePermission?.includes(
                AnalysePermission.PlaybookItemNoteRead,
              )
            }
          >
            <ArrowForwardIosIcon
              style={animatedStyle}
              className={classes.icon}
            />
          </IconButton>
        </Box>

        <Box display="flex">
          <IconButton
            onClick={event => {
              setAnchorElement(event.currentTarget);
            }}
            disabled={verifyDisable('add_note')}
          >
            <AddCircleOutlineIcon />
          </IconButton>

          <Menu
            id="simple-menu"
            anchorEl={anchorElement}
            keepMounted
            open={Boolean(anchorElement)}
            onClose={() => setAnchorElement(null)}
          >
            {noteSelectionOptions.map((note: NoteResponseT, index: number) => (
              <MenuItem
                key={note.id}
                onClick={handleAddNoteFromExistingOne(note)}
              >
                {renderNoteTitle(note.title, index)}
              </MenuItem>
            ))}
            {analysePermission?.includes(
              AnalysePermission.PlaybookItemNoteCreate,
            ) && (
              <MenuItem
                onClick={() => {
                  setOpenAddNote(true);
                  setAnchorElement(null);
                }}
              >
                <Button startIcon={<AddCircleOutlineIcon />}>
                  Create new note
                </Button>
              </MenuItem>
            )}
          </Menu>
          <NoteDialog
            type="add"
            isOpen={isOpenAddNote}
            noteContent={noteContent}
            handleClose={() => {
              setNoteContent(emptyNoteContent);
              setOpenAddNote(false);
            }}
            handleChange={handleChangeNoteContent}
            handleSubmit={handleAddNote}
            disabled={isSubmitting}
          />
        </Box>
      </Box>
      {collapse && (
        <Box>
          {values && values.note && values?.note?.length > 0 && (
            <TableContainer>
              <Table>
                <EnhancedTableHead classes={classes} />
                <TableBody>
                  {values?.note?.map((item, noteIndex) => {
                    return (
                      <TableRow
                        key={noteIndex}
                        className={classes.tableRow}
                        onClick={event =>
                          visualizeNoteDialog('show_info', event, item)
                        }
                      >
                        <TableCell component="th" scope="row">
                          <Typography component="span" variant="body1">
                            {item?.title}
                          </Typography>
                        </TableCell>
                        <TableCell component="th" scope="row">
                          <Typography component="span" variant="body1">
                            {item?.created_by_user?.name}
                          </Typography>
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {item.date !== '0001-01-01T00:00:00Z' && (
                            <Typography component="span" variant="body1">
                              {moment(item.date).format('DD/MM/YYYY hh:mma')}
                            </Typography>
                          )}
                        </TableCell>
                        <TableCell component="th" scope="row" padding="none">
                          <TableAction
                            hiddenItem={['view']}
                            onClickEdit={event => {
                              if (!event) return;
                              visualizeNoteDialog('edit_note', event, item?.id);
                            }}
                            disabledEdit={
                              !analysePermission?.includes(
                                AnalysePermission.PlaybookItemNoteUpdate,
                              )
                            }
                            onClickDelete={event => {
                              if (!event) return;
                              visualizeNoteDialog(
                                'delete_note',
                                event,
                                item.id,
                              );
                            }}
                            disabledDelete={
                              !analysePermission?.includes(
                                AnalysePermission.PlaybookItemNoteDelete,
                              ) ||
                              item?.created_by_user?.id !== userProfileState.id
                            }
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          )}
          <Dialog
            open={!!informationDialog}
            onClose={() => setInformationDialog(undefined)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            maxWidth="md"
          >
            <Box padding={3} minWidth="500px">
              <Box mb={3} display="flex" alignItems="center">
                <Box mr={1} display="flex" alignItems="center">
                  <InfoIcon color="primary" />
                </Box>
                <Typography component="span" variant="h4">
                  {t('note_info')}
                </Typography>
              </Box>
              <Box mb={1}>
                <Typography component="span" variant="caption">
                  {t('title')}
                </Typography>
                <Typography component="span" variant="body1">
                  {informationDialog?.title}
                </Typography>
              </Box>

              <Box>
                <Typography component="p" variant="caption">
                  {t('content')}
                </Typography>
                <Box
                  mb={2}
                  dangerouslySetInnerHTML={{
                    __html: informationDialog?.content ?? '',
                  }}
                ></Box>
              </Box>
              <DialogActions>
                <Button
                  onClick={() => setInformationDialog(undefined)}
                  color="default"
                  variant="outlined"
                >
                  {t('close')}
                </Button>
              </DialogActions>
            </Box>
          </Dialog>

          <DeleteDialog
            isOpen={!!noteToDeleteId}
            header="Delete Note"
            message={
              <Box>
                <Typography component="p" variant="body1">
                  {t('delete_this_note')}
                </Typography>
              </Box>
            }
            handleClose={() => setNoteToDeleteId(undefined)}
            handleDelete={() => handleRemoveNote(noteToDeleteId)}
            disabled={isSubmitting}
          />
          <NoteDialog
            type="edit"
            isOpen={!!currentEditNoteId}
            noteContent={{
              ...noteContent,
            }}
            handleClose={() => {
              setNoteContent(emptyNoteContent);
              setCurrentEditNoteId(undefined);
            }}
            handleChange={handleChangeNoteContent}
            handleSubmit={handleEditNote}
            disabled={isSubmitting}
          />
          {(!values.note || values?.note?.length === 0) && (
            <Box textAlign="center" padding="5px">
              No note found
            </Box>
          )}
        </Box>
      )}
    </React.Fragment>
  );
};
