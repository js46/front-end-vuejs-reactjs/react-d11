import React from 'react';

import { Box, Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { PlaybookProcess } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '10px 0px',
  },
}));

const ProgressUpdate: React.FC = () => {
  const classes = useStyles();
  const { i18n } = useTranslation();
  return (
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        <Box textAlign="center" mt={1}>
          <Box textAlign="left" mb={1}>
            <Typography component="p" variant="h4">
              {i18n.t('opportunity.playbook_progress')}
            </Typography>
          </Box>
          <Box textAlign="center">
            <PlaybookProcess />
          </Box>
        </Box>
        {/* <Box textAlign="center" mt={2}>
          <Box textAlign="left" mb={1}>
            <Typography component="p" variant="h4">
              {i18n.t('opportunity.finding_public')}
            </Typography>
          </Box>
          <Box textAlign="center">
            <FindingTable />
          </Box>
        </Box> */}
      </Grid>
    </Grid>
  );
};
export default ProgressUpdate;
