import { ReactiveBase } from '@appbaseio/reactivesearch';
import {
  Box,
  Link,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import React, { useCallback } from 'react';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import {
  ESLoader,
  ESNoResult,
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from '../../../../../../../components';
import { StyledReactiveList } from '../../../../../../../components/styled';
import { useTableHeadSorter } from '../../../../../../../hooks';
import { PlaybookItemFindingT } from '../../../../../../../services/playbook.services';
import { ConfigESIndex } from '../../../../../../../store/config/selector';
import { idOpportunityState } from '../../../../../../../store/opportunity/selector';
import { FindingDetail } from '../../../../Sidebar/component/FindingDetail';

const tableFields = [
  'id',
  'findingTitle',
  'playbookName',
  'stepTitle',
  'updateData',
  'public',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'findingTitle',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'playbookName',
    label: 'Playbook name',
    disablePadding: true,
  },
  {
    id: 'stepTitle',
    label: 'Step title',
    disablePadding: true,
  },
  {
    id: 'updateData',
    label: 'Last updated',
    disablePadding: true,
  },
];
interface IFindingTableProps {
  customQuery?: () => void;
}
export const FindingTable: React.FC<IFindingTableProps> = props => {
  const { customQuery } = props;
  const ES_INDICES = useSelector(ConfigESIndex);
  const opportunityID = useSelector(idOpportunityState);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'time',
    SortOrder.DESC,
  );
  const [isOpenModal, setOpenModal] = useState<boolean>(false);
  const [findingSelected, setFindingSelected] = useState<
    PlaybookItemFindingT
  >();

  const classes = useStyles();
  const defaultQuery = useCallback(() => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'opportunity.id': opportunityID,
              },
            },
            {
              term: {
                entity_type: 'opportunity_playbook_item',
              },
            },
            {
              term: {
                public_flg: true,
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  }, [opportunityID]);
  const onSeeDetailFinding = (item: PlaybookItemFindingT) => {
    setOpenModal(true);
    setFindingSelected(item);
  };
  return (
    <>
      <ReactiveBase
        app={ES_INDICES.opportunity_index_finding}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <StyledReactiveList
          componentId="SearchResult"
          dataField={orderBy}
          sortBy={order}
          size={10}
          paginationAt="bottom"
          pages={5}
          pagination={true}
          showLoader
          loader={<ESLoader />}
          renderResultStats={() => null}
          renderNoResults={() => <ESNoResult msgKey="no_data" />}
          render={({ data, loading }) => {
            if (loading) return null;
            if (!data?.length) return <></>;

            return (
              <TableContainer classes={{ root: classes.rootTableContainer }}>
                <Table classes={{ root: classes.rootTable }}>
                  <TableHeadSorter
                    onRequestSort={handleRequestSort}
                    order={order}
                    orderBy={orderBy}
                    cells={headCells}
                  />
                  <TableBody>
                    {data.map((item: any, index: number) => (
                      <TableRow key={index}>
                        <TableCell component="th" scope="row">
                          <Box
                            onClick={() => onSeeDetailFinding(item)}
                            className={classes.title}
                          >
                            {item?.title}
                          </Box>
                        </TableCell>

                        <TableCell component="th" scope="row">
                          {item?.opportunity_playbook?.name}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          <Link
                            component={RouterLink}
                            color="inherit"
                            to={`/opportunity/${item?.opportunity?.id}/playbook-${item?.opportunity_playbook?.id}?step=${item.entity_id}`}
                          >
                            {item?.opportunity_playbook_item?.title}
                          </Link>
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {moment(item.last_updated_at).format(
                            'DD/MM/YYYY HH:mm:ss',
                          )}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            );
          }}
          innerClass={{
            button: classes.buttonPagination,
            resultsInfo: classes.resultsInfo,
            sortOptions: classes.sortSelect,
          }}
          defaultQuery={customQuery ? customQuery : defaultQuery}
        />
      </ReactiveBase>
      {isOpenModal && (
        <FindingDetail
          openModal={isOpenModal}
          toggleModal={setOpenModal}
          findingSelected={findingSelected}
        />
      )}
    </>
  );
};
const useStyles = makeStyles(theme => ({
  title: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  sortByLabel: {
    paddingRight: '30px',
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  gridRight: {
    minWidth: 217,
  },
  gridSort: {
    minWidth: 229,
  },
  gridFilter: {
    minWidth: 121,
  },
  rootTableContainer: {
    marginTop: 15,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));
