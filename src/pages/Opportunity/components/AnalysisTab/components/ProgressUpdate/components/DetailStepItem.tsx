import React from 'react';
import { Box, Chip, TableCell, TableRow, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ResponsePlayBookT } from '../../../../../../../services/playbook.services';
import moment from 'moment';
import { ICommonObj, ITaskState } from '../../../types';
import { shallowEqual, useSelector } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { idOpportunityState } from '../../../../../../../store/opportunity/selector';

interface DetailStepItemProps {
  item: ResponsePlayBookT;
  completionDate?: string;
  playbook: ICommonObj;
  listStates: Map<number, ITaskState>;
}

export const DetailStepItem: React.FC<DetailStepItemProps> = ({
  item,
  completionDate,
  playbook,
  listStates,
}) => {
  const classes = useStyles();
  const opportunityID = useSelector(idOpportunityState, shallowEqual);

  const renderStateTask = (stepItem: ResponsePlayBookT) => {
    let taskState = listStates.get(stepItem.id);

    return (
      <Box display="flex" flex={1} component="span" justifyContent="flex-end">
        {item.due_date?.toString() !== '0001-01-01T00:00:00Z' && (
          <Box component="span">
            <Chip
              label={`Due: ${moment(item.due_date).format('DD/MM/YYYY')}`}
              variant="outlined"
              size="small"
            />
          </Box>
        )}
        <Box component="span">
          <Chip
            label={taskState?.label}
            size="small"
            style={{
              backgroundColor: taskState?.color,
            }}

            //style={{ color: taskState.color }}
          />
        </Box>
      </Box>
    );
  };

  return (
    <>
      <TableRow
        hover
        classes={{
          selected: classes.focus,
        }}
      >
        <TableCell>
          <Box ml={2} display="flex">
            <Link
              component={RouterLink}
              color="inherit"
              to={`/opportunity/${opportunityID}/playbook-${playbook.id}?step=${item.id}`}
            >
              {item?.title}
            </Link>
            {renderStateTask(item)}
          </Box>
        </TableCell>
        {/*  <TableCell>
        {item?.due_date !== '0001-01-01T00:00:00Z' &&
          moment(item?.due_date).format('DD/MM/YYYY')}
      </TableCell>
      <TableCell></TableCell> */}
      </TableRow>
    </>
  );
};
const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  detailRow: {
    display: 'none',
  },
  collRow: {
    display: 'table-row',
  },
  focus: {
    backgroundColor: `${theme.palette.grey[300]}!important`,
  },
}));
