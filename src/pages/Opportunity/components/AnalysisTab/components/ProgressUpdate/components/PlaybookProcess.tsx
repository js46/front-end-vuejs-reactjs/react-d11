import React, { useContext } from 'react';
import { Box, Grid, Table, TableBody, TableContainer } from '@material-ui/core';

import { completionDateState } from '../../../../../../../store/opportunity/selector';
import { useSelector } from 'react-redux';
import { DetailPlaybook } from './DetailPlaybook';
import { AnalyseStateContext } from '../../../AnalyseContext';

export const PlaybookProcess: React.FC = () => {
  const completionDate = useSelector(completionDateState);
  const analyseState = useContext(AnalyseStateContext);

  return (
    <Grid container>
      <Grid item xs={12}>
        <Box mt={1}>
          <TableContainer>
            <Table>
              <TableBody>
                {analyseState.playbookDetailList.map(item => (
                  <DetailPlaybook
                    key={item.id}
                    item={item}
                    completionDate={completionDate}
                  />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Grid>
    </Grid>
  );
};
