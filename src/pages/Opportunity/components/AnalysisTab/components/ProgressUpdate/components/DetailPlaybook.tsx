import React from 'react';
import {
  Box,
  Chip,
  IconButton,
  Link,
  TableCell,
  TableRow,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Link as RouterLink } from 'react-router-dom';
import { useToggleValue } from '../../../../../../../hooks';
import {
  OpportunityPlaybookT,
  ResponsePlayBookT,
} from '../../../../../../../services/playbook.services';
import { DetailStepItem } from './DetailStepItem';
import { ITaskState, TaskStateColors, TaskStates } from '../../../types';
import moment from 'moment';
import { IOpportunityTask } from '../../../../../../../services/task.services';
import { maxBy } from 'lodash';

interface DetailPlaybookProps {
  item: OpportunityPlaybookT;
  completionDate?: string;
}

export const DetailPlaybook: React.FC<DetailPlaybookProps> = ({
  item,
  completionDate,
}) => {
  const [isExpanded, toggleExpanded] = useToggleValue(false);
  const classes = useStyles();
  const animatedStyle = {
    transition: '0.3s all',
    transform: isExpanded ? 'rotate(90deg)' : 'rotate(0deg)',
  };

  const mergeTasks = (data: ResponsePlayBookT) => {
    let tasks = data.tasks ?? [];
    let itemChild = item?.playbook_items?.filter(
      itemChild => itemChild.playbook_item_parent_id === data.id,
    );
    if (itemChild && itemChild?.length) {
      for (let item of itemChild) {
        let tasksChild = mergeTasks(item);
        tasks.push(...tasksChild);
      }
    }
    return tasks;
  };
  const stepTopLevels =
    item?.playbook_items
      ?.filter(child => child.playbook_item_parent_id === 0)
      ?.map(item => {
        item.tasks = mergeTasks(item);
        return item;
      }) ?? [];
  const listStates = new Map<number, ITaskState>();
  const getTaskState = (stepItem: ResponsePlayBookT) => {
    let taskState = {
      label: TaskStates.todo,
      color: TaskStateColors.blank,
      order: -5,
    };
    if (stepItem.due_date?.toString() === '0001-01-01T00:00:00Z') {
      taskState = {
        label: TaskStates.todo,
        color: TaskStateColors.blank,
        order: -5,
      };
    } else if (
      moment(completionDate).isBefore(moment(stepItem.due_date), 'day')
    ) {
      taskState = {
        label: TaskStates.inProgress,
        color: TaskStateColors.red,
        order: 3,
      };
    } else if (!stepItem.tasks || stepItem.tasks.length === 0) {
      taskState = {
        label: TaskStates.inProgress,
        color: TaskStateColors.green,
        order: 1,
      };
    } else {
      let maxDueDate = maxBy(stepItem.tasks, (item: IOpportunityTask) =>
        new Date(item?.requested_completion_date ?? '').getTime(),
      )?.requested_completion_date;

      if (
        maxDueDate &&
        moment(maxDueDate).isAfter(moment(stepItem.due_date), 'day') &&
        moment(maxDueDate).isAfter(moment(completionDate), 'day')
      ) {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.red,
          order: 3,
        };
      } else if (
        maxDueDate &&
        moment(maxDueDate).isAfter(moment(stepItem.due_date), 'day') &&
        moment(maxDueDate).isSameOrBefore(moment(completionDate), 'day')
      ) {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.amber,
          order: 2,
        };
      } else {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.green,
          order: 1,
        };
      }
    }
    if (stepItem.playbook_item_state?.name === 'DONE') {
      taskState.label = TaskStates.done;
    }
    return taskState;
  };
  for (let itemStep of stepTopLevels) {
    listStates.set(itemStep.id, getTaskState(itemStep));
  }
  const taskStateArr = Array.from(listStates).map(([name, value]) => value);
  let taskState = maxBy(taskStateArr, 'order');
  if (taskStateArr.length) {
    if (
      taskState?.label === TaskStates.done &&
      taskStateArr?.filter(item => item.label !== TaskStates.done)?.length
    ) {
      taskState = {
        ...taskState,
        label: TaskStates.inProgress,
      };
    }
  }

  return (
    <>
      <TableRow
        hover
        classes={{
          selected: classes.focus,
        }}
      >
        <TableCell>
          <Box onClick={toggleExpanded} display="flex" alignItems="center">
            <IconButton size="small">
              <ArrowForwardIosIcon
                style={animatedStyle}
                className={classes.icon}
              />
            </IconButton>
            <Link
              component={RouterLink}
              color="inherit"
              to={`/opportunity/${item?.opportunity?.id}/playbook-${item?.id}`}
            >
              {item?.name}
            </Link>

            {!!taskState && (
              <Box display="flex" flex={1} justifyContent="flex-end">
                <Chip
                  label={taskState?.label}
                  size="small"
                  style={{
                    backgroundColor: taskState?.color,
                  }}
                />
              </Box>
            )}
          </Box>
        </TableCell>
      </TableRow>
      {isExpanded &&
        stepTopLevels?.map(child => (
          <DetailStepItem
            item={child}
            completionDate={completionDate}
            playbook={{ id: item.id, title: item.name }}
            listStates={listStates}
          />
        ))}
    </>
  );
};
const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  detailRow: {
    display: 'none',
  },
  collRow: {
    display: 'table-row',
  },
  focus: {
    backgroundColor: `${theme.palette.grey[300]}!important`,
  },
}));
