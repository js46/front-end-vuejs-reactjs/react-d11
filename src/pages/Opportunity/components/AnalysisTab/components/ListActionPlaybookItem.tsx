import React, { useContext, useState } from 'react';
import {
  ResponsePlayBookT,
  updatePlaybookItem,
  getAllPlaybookOfOpportunity,
} from '../../../../../services/playbook.services';
import { Box, MenuItem, TextField } from '@material-ui/core';
import Fade from '@material-ui/core/Fade';
import { useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { maxBy } from 'lodash';
import { StyledMenu } from '../../../../../components/styled';
import {
  AnalysePermissionState,
  idOpportunityState,
} from '../../../../../store/opportunity/selector';
import {
  AnalyseAction,
  AnalyseDispatchContext,
  updatePlaybookList,
} from '../AnalyseContext';
import { MixTitle, FormDialog } from '../../../../../components';
import { useDisableMultipleClick } from '../../../../../hooks';
import FindingsWrapper from '../../FindingsWrapper';
import TasksWrapper from './TasksWrapper';
import { AnalysePermission } from '../../../types';

interface ListActionPlaybookItemProps {
  data: IPlayItemTree;
  position: number[];
  anchorEl: any;
  setAnchorEl: React.Dispatch<null>;
}

export type IPlayItemTree = {
  children?: Array<IPlayItemTree>;
} & ResponsePlayBookT;

export const ListActionPlaybookItem: React.FC<ListActionPlaybookItemProps> = ({
  data,
  position,
  setAnchorEl,
  anchorEl,
}) => {
  const open = Boolean(anchorEl);
  const analyseDispatch = useContext(AnalyseDispatchContext);
  const analysePermission = useSelector(AnalysePermissionState);
  const [openDialog, setDialog] = useState<boolean>(false);

  const opportunityID = useSelector(idOpportunityState);

  const handleClose = () => {
    setAnchorEl(null);
  };
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const level = position.length;
  const handleAskDeletePlaybookItem = (pbItemId: number) => {
    analyseDispatch({
      type: AnalyseAction.ASK_DELETE_PLAYBOOK_ITEM,
      payload: pbItemId,
    });
  };
  let orderItem = 0;
  if (data && data.children && data.children.length > 0) {
    orderItem =
      maxBy(data.children, item => item.item_order)?.item_order ??
      data.children.length;
  }
  const handleUpdatePlaybookItem = async (payload: {
    title: string;
    description: string;
  }) => {
    if (isSubmitting) return;
    await debounceFn();
    const response = await updatePlaybookItem(
      { playbook_items: [payload] },
      data.id,
    );
    if (response.success) {
      handleUpdatePlaybookOfOpportunity();
    }
    endRequest();
    setDialog(false);
  };
  const handleUpdatePlaybookOfOpportunity = async () => {
    const response = await getAllPlaybookOfOpportunity(opportunityID);
    if (response.success) {
      analyseDispatch(updatePlaybookList(response.data));
    }
  };

  const initialDataForm = {
    note: data?.notes,
    title: data?.title,
    detail: data?.description,
  };
  const playbookItemSchema = Yup.object({
    title: Yup.string().required('This field is required'),
  });
  return (
    <>
      <StyledMenu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {level < 3 && (
          <MenuItem
            key={1}
            disabled={
              !analysePermission?.includes(AnalysePermission.PlaybookItemCreate)
            }
            onClick={() => {
              setAnchorEl(null);
              analyseDispatch({
                type: AnalyseAction.OPEN_ADD_NEW_PLAYBOOK_ITEM,
                payload: {
                  playbook_item: {
                    title: '',
                    description: '',
                    playbook_item_parent_id: data.id,
                    item_order: orderItem,
                  },
                  label_item: [...position, data.children?.length ?? 1],
                },
              });
            }}
          >
            Add sub item
          </MenuItem>
        )}
        <MenuItem
          key={2}
          onClick={() => {
            setAnchorEl(null);
            setDialog(true);
          }}
          disabled={
            !analysePermission?.includes(AnalysePermission.PlaybookItemUpdate)
          }
        >
          Edit item
        </MenuItem>
        <MenuItem
          key={3}
          onClick={() => {
            setAnchorEl(null);
            handleAskDeletePlaybookItem(data.id ?? 0);
          }}
          disabled={
            !analysePermission?.includes(AnalysePermission.PlaybookItemDelete)
          }
        >
          Delete item
        </MenuItem>
      </StyledMenu>

      {openDialog && (
        <Formik
          key={position.join('.')}
          initialValues={initialDataForm}
          validationSchema={playbookItemSchema}
          enableReinitialize
          onSubmit={values => {
            handleUpdatePlaybookItem({
              title: values.title,
              description: values.detail,
            });
          }}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isValid,
            errors,
            touched,
          }) => (
            <FormDialog
              title={'Playbook item ' + position.join('.')}
              submit={handleSubmit}
              maxWidth="md"
              open={openDialog}
              setOpen={setDialog}
              isAddingForm={false}
              disable={!isValid}
              formContent={
                <Box>
                  <Box mb={3} mt={3}>
                    <MixTitle title="Name" isRequired />
                    <TextField
                      fullWidth
                      variant="outlined"
                      name="title"
                      id="title"
                      autoComplete="title"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.title}
                      error={touched.title && !!errors.title}
                      helperText={touched.title ? errors.title : ''}
                    />
                  </Box>
                  <Box mb={3} mt={3}>
                    <MixTitle title="Detail" />
                    <TextField
                      fullWidth
                      variant="outlined"
                      rows={3}
                      name="detail"
                      multiline
                      id="detail"
                      autoComplete="detail"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.detail}
                      error={touched.detail && !!errors.detail}
                      helperText={touched.detail ? errors.detail : ''}
                    />
                  </Box>
                  <Box mt={2} mb={2}>
                    {/* <NoteWrapper data={data} values={values} />
                    <AttachmentWrapper
                      data={data}
                      index={position[level - 1] ?? 0}
                    /> */}
                    <TasksWrapper
                      data={data.tasks ?? []}
                      itemID={data.id}
                      onRefresh={handleUpdatePlaybookOfOpportunity}
                    />
                    <FindingsWrapper
                      data={data.findings ?? []}
                      key={position.join('.')}
                      itemID={data.id}
                      editable
                      onRefresh={handleUpdatePlaybookOfOpportunity}
                    />
                  </Box>
                </Box>
              }
            />
          )}
        </Formik>
      )}
    </>
  );
};
