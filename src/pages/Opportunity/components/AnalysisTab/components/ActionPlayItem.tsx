import React, { useState } from 'react';
import { ResponsePlayBookT } from '../../../../../services/playbook.services';
import { Box, IconButton, Tooltip } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { ListActionPlaybookItem } from './ListActionPlaybookItem';

interface ActionPlayItemProps {
  data: IPlayItemTree;
  position: number[];
}

export type IPlayItemTree = {
  children?: Array<IPlayItemTree>;
} & ResponsePlayBookT;

export const ActionPlayItem: React.FC<ActionPlayItemProps> = ({
  data,
  position,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <Box ml={3} display="flex" alignItems="right" justifyContent="flex-end">
      <Box ml={2}>
        <Tooltip title="Action" placement="top">
          <IconButton
            aria-controls="fade-menu"
            aria-haspopup="true"
            onClick={handleClick}
            aria-label="Action"
          >
            <MoreVertIcon />
          </IconButton>
        </Tooltip>
        {setAnchorEl && (
          <ListActionPlaybookItem
            key={position.join('.')}
            data={data}
            position={position}
            anchorEl={anchorEl}
            setAnchorEl={setAnchorEl}
          />
        )}
      </Box>
    </Box>
  );
};
