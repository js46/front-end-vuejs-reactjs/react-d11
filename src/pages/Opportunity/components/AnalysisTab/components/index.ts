import loadable from '@loadable/component';

export const FormulateApproachPanel = loadable(() =>
  import('./FormualateApproach/FormulateApproachPanel'),
);
export const ProgressUpdate = loadable(() => import('./ProgressUpdate'));
export const PlaybookTreePanel = loadable(() => import('./Playbook'));
