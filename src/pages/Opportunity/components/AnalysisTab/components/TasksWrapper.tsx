import React, { useState } from 'react';
import {
  addTaskNew,
  IOpportunityTask,
} from '../../../../../services/task.services';
import { Box, IconButton, Typography } from '@material-ui/core';
import {
  CollapseIconButton,
  SortOrder,
  TaskFormDialog,
  TaskTable,
} from '../../../../../components';
import { AnalysePermission } from '../../../types';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useSelector } from 'react-redux';
import { AnalysePermissionState } from '../../../../../store/opportunity/selector';
import { useTranslation } from 'react-i18next';
import { useTableHeadSorter } from '../../../../../hooks';
import {
  TaskState,
  TaskStateID,
  TaskSubTypeID,
  TaskTypeID,
} from '../../../../../constants';

const tableFields = [
  'id',
  'title.keyword',
  'task_type.keyword',
  'task_state.keyword',
  'created_at',
  'created_by_user.name.keyword',
  'assignee_user.name.keyword',
  'last_updated_at',
  'requested_completion_date',
] as const;

interface TasksWrapperProps {
  data: IOpportunityTask[];
  itemID: number;
  onRefresh?: () => void;
}

const TasksWrapper: React.FC<TasksWrapperProps> = ({
  data,
  itemID,
  onRefresh,
}) => {
  const { t } = useTranslation();
  const analysePermission = useSelector(AnalysePermissionState);
  const [collapse, setCollapse] = useState(true);
  const [showFormDialog, setFormDialog] = useState(false);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'last_updated_at',
    SortOrder.DESC,
  );

  const handleSubmit = async (
    title: string,
    assignee: number,
    description?: string,
    dueDate?: Date,
  ) => {
    const json = await addTaskNew({
      entity_id: itemID,
      entity_type: 'opportunity_playbook_item',
      title,
      description: description || '',
      assignee_user_id: assignee,
      task_state_id: TaskStateID[TaskState.Assigned],
      task_sub_type_id: TaskSubTypeID.Review,
      task_type_id: TaskTypeID.User,
      requested_completion_date: dueDate,
    });
    if (json.success) onRefresh?.();
    setFormDialog(false);
  };
  // console.log('Tasks', data);
  return (
    <>
      <Box display="flex" alignItems="center" mb={2} mt={2}>
        <Typography component="span" variant="caption">
          {t('task.plural')}
        </Typography>
        <Box ml={2} display="flex">
          <CollapseIconButton
            onToggle={collapse => setCollapse(collapse)}
            disabled={
              !analysePermission?.includes(
                AnalysePermission.PlaybookItemNoteRead,
              )
            }
          />
        </Box>
        <IconButton onClick={() => setFormDialog(true)}>
          <AddCircleOutlineIcon />
        </IconButton>
      </Box>
      {!collapse && data.length > 0 && (
        <Box>
          <TaskTable
            data={data}
            onRequestSort={handleRequestSort}
            order={order}
            orderBy={orderBy}
            clickDisabled
          />
        </Box>
      )}
      <TaskFormDialog
        open={showFormDialog}
        toggle={() => setFormDialog(prev => !prev)}
        onSubmit={handleSubmit}
      />
    </>
  );
};

export default TasksWrapper;
