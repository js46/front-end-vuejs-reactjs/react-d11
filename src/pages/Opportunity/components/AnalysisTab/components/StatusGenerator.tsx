import React from 'react';
import { PlaybookItemStatusT } from '../../../../../services/playbook.services';
import {
  Box,
  Typography,
  MenuItem,
  IconButton,
  Tooltip,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Fade from '@material-ui/core/Fade';
import { useSelector } from 'react-redux';
import { capitalizeFirstLetter } from '../../../../../utils';
import { AnalysePermission } from '../../../types';
// import { AnalyseStateContext } from './AnalyseContext';
import { StyledMenu } from '../../../../../components/styled';
import { AnalysePermissionState } from '../../../../../store/opportunity/selector';

interface AssignStatusProps {
  status?: PlaybookItemStatusT;
  hasEmail: boolean;
  onUpdatePlaybookItemStatus: (playbookItemStatus: PlaybookItemStatusT) => void;
}

const getAccurateDisplayText = (text: PlaybookItemStatusT): any => {
  if (text === 'ASSIGNED') {
    return 'ASSIGN';
  } else if (text === 'UNASSIGNED') {
    return 'UNASSIGN';
  }
  return text;
};

const getNextPossiblePlaybookItemStatus = (
  currentPbItemStatus: PlaybookItemStatusT,
  hasEmail: boolean,
  analysePermission: string[] | undefined,
): PlaybookItemStatusT[] => {
  switch (currentPbItemStatus) {
    case 'UNASSIGNED': {
      let action: PlaybookItemStatusT[] = ['ASSIGNED', 'DONE', 'DELETE'];
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemAssign)) {
        action = action.filter(item => item !== 'ASSIGNED');
      }
      if (!hasEmail) {
        action = action.filter(item => item !== 'ASSIGNED');
      }
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDone)) {
        action = action.filter(item => item !== 'DONE');
      }
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDelete)) {
        action = action.filter(item => item !== 'DELETE');
      }
      return action;
    }
    case 'ASSIGNED': {
      let action: PlaybookItemStatusT[] = ['UNASSIGNED', 'DONE', 'DELETE'];
      if (
        !analysePermission?.includes(AnalysePermission.PlaybookItemUnassign)
      ) {
        action = action.filter(item => item !== 'UNASSIGNED');
      }
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDone)) {
        action = action.filter(item => item !== 'DONE');
      }
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDelete)) {
        action = action.filter(item => item !== 'DELETE');
      }
      return action;
    }
    case 'DONE': {
      let action: PlaybookItemStatusT[] = ['UNDONE', 'DELETE'];
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemUndone)) {
        action = action.filter(item => item !== 'UNDONE');
      }
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDelete)) {
        action = action.filter(item => item !== 'DELETE');
      }
      return action;
    }
    case 'UNDONE': {
      let action: PlaybookItemStatusT[] = ['DONE', 'DELETE'];
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDone)) {
        action = action.filter(item => item !== 'DONE');
      }
      if (!analysePermission?.includes(AnalysePermission.PlaybookItemDelete)) {
        action = action.filter(item => item !== 'DELETE');
      }
      return action;
    }
    default:
      return [];
  }
};

export const GroupButtonStatus: React.FC<AssignStatusProps> = ({
  status = 'UNASSIGNED',
  hasEmail,
  onUpdatePlaybookItemStatus,
}) => {
  const analysePermission = useSelector(AnalysePermissionState);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleChangeStatus = (playbookItemStatus: PlaybookItemStatusT) => {
    onUpdatePlaybookItemStatus(playbookItemStatus);
    setAnchorEl(null);
  };

  return (
    <Box
      ml={3}
      display="flex"
      alignItems="center"
      minWidth="175px"
      justifyContent="space-between"
    >
      <Box minWidth="130px">
        <Typography component="span" variant="body2">
          status:{' '}
          <Typography component="span" variant="caption">
            {capitalizeFirstLetter(status)}
          </Typography>
        </Typography>
      </Box>

      <Box ml={2}>
        {getNextPossiblePlaybookItemStatus(status, hasEmail, analysePermission)
          .length > 0 && (
          <Tooltip title="Action" placement="top">
            <IconButton
              aria-controls="fade-menu"
              aria-haspopup="true"
              onClick={handleClick}
              aria-label="Action"
            >
              <MoreVertIcon />
            </IconButton>
          </Tooltip>
        )}

        <StyledMenu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          {getNextPossiblePlaybookItemStatus(
            status,
            hasEmail,
            analysePermission,
          ).map((playbookStatus: PlaybookItemStatusT, index: number) => (
            <MenuItem
              key={index}
              onClick={() => handleChangeStatus(playbookStatus)}
            >
              {capitalizeFirstLetter(getAccurateDisplayText(playbookStatus))}
            </MenuItem>
          ))}
        </StyledMenu>
      </Box>
    </Box>
  );
};
