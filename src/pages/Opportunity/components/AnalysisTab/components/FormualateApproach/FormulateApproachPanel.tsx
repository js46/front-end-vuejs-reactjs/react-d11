import React, { useState } from 'react';
import { Box, Button } from '@material-ui/core';
import { Formik, Form, FieldArray } from 'formik';

import { useDisableMultipleClick } from '../../../../../../hooks';
import { updateOpportunityDetail } from '../../../../../../services/opportunity.services';
import { useSelector, useDispatch } from 'react-redux';
import { AnalysePermission } from '../../../../types';
import { UpdatedOpportunityDetail as updateOpportunityAction } from '../../../../../../store/opportunity/actions';
import {
  OpportunityState,
  AnalysePermissionState,
} from '../../../../../../store/opportunity/selector';
import { MixTitle } from '../../../../../../components';
import { TinyMCEForm } from '../../../../../../components/form/TinyMCEForm';

const FormulateApproachPanel: React.FC = () => {
  const chosenOpportunity = useSelector(OpportunityState);

  const analysePermission = useSelector(AnalysePermissionState);
  const dispatch = useDispatch();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [isEditable, setEditable] = useState(false);
  const initialValues = {
    note: '',
    title: '',
    detail: chosenOpportunity?.detail,
  };
  const [loadingEditor, setLoadingEditor] = useState(true);
  const handleUpdateDetailFormulateApproach = async (
    payload: {
      detail: any;
    },
    oppID: number,
  ) => {
    if (isSubmitting) return;
    await debounceFn();
    const response = await updateOpportunityDetail(payload, oppID);
    if (response.success) {
      setEditable(false);
      dispatch(updateOpportunityAction(response.data));
    }
    endRequest();
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={values => {
          // console.log(values);
        }}
      >
        {({ values, setFieldValue }) => (
          <Form>
            <Box>
              <Box pl={1} mt={2}>
                <FieldArray name="note">
                  {arrayHelpers => {
                    return (
                      <>
                        <Box display="flex" justifyContent="flex-end" mb={1}>
                          {!isEditable && (
                            <Button
                              variant="outlined"
                              onClick={() => setEditable(true)}
                              disabled={
                                !analysePermission?.includes(
                                  AnalysePermission.PlaybookItemUpdate,
                                )
                              }
                            >
                              Edit
                            </Button>
                          )}
                          {isEditable && (
                            <Box display="flex">
                              <Button
                                color="default"
                                variant="outlined"
                                onClick={() => setEditable(false)}
                              >
                                Cancel
                              </Button>
                              <Box ml={1}>
                                <Button
                                  variant="contained"
                                  color="primary"
                                  onClick={() => {
                                    const payload = {
                                      detail: values.detail,
                                    };

                                    handleUpdateDetailFormulateApproach(
                                      payload,
                                      chosenOpportunity?.id ?? 0,
                                    );
                                  }}
                                  disabled={isSubmitting}
                                >
                                  Save
                                </Button>
                              </Box>
                            </Box>
                          )}
                        </Box>
                        <Box
                          style={
                            loadingEditor ? { display: 'none' } : undefined
                          }
                        >
                          <Box>
                            <MixTitle title="Description" />
                            <Box minHeight="400px" width="100%">
                              <TinyMCEForm
                                value={values.detail ?? ''}
                                disabled={isSubmitting || !isEditable}
                                setLoadingEditor={setLoadingEditor}
                                setValue={(content: string) => {
                                  setFieldValue('detail', content);
                                }}
                              />
                            </Box>
                          </Box>
                        </Box>
                      </>
                    );
                  }}
                </FieldArray>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default FormulateApproachPanel;
