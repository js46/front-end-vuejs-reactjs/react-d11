import React, { useState, useContext, useCallback } from 'react';
import moment from 'moment';
import {
  Box,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableHead,
  Button,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CloudDownloadOutlined from '@material-ui/icons/CloudDownloadOutlined';
import {
  ResponsePlayBookT,
  getAllPlaybookOfOpportunity,
} from '../../../../../../services/playbook.services';
import { AttachmentDialog } from './AttachmentDialog';
// import { AttachmentDialog } from '../../../../../../components/AttachmentDialog';
import {
  deleteAttachments,
  AttachmentResponse,
} from '../../../../../../services/opportunity.services';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import {
  AnalyseStateContext,
  AnalyseDispatchContext,
  updatePlaybookList,
} from '../../AnalyseContext';

import { useSelector } from 'react-redux';
import { DeleteDialog, TableAction } from '../../../../../../components';
import { useDisableMultipleClick } from '../../../../../../hooks';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../../../../store/user/selector';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { ConfigESIndex } from '../../../../../../store/config/selector';
import { AnalysePermissionState } from '../../../../../../store/opportunity/selector';
import { AnalysePermission } from '../../../../types';
const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 15,
  },
  headTable: {
    minWidth: 732,
  },
  fileName: {
    maxWidth: '240px',
    display: 'inline-block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    marginBottom: -6,
  },
  attachmentDescription: {
    '& input': {
      height: '100%',
    },
  },
}));

interface Data {
  name: string;
  description: string;
  date: string;
  action: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'name',
    label: 'File name',
  },
  {
    id: 'description',
    label: 'Description',
  },
  {
    id: 'date',
    label: 'Date',
  },
  {
    id: 'action',
    label: 'Actions',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.id === 'action' ? 'center' : undefined}
            style={headCell.id === 'action' ? { width: '120px' } : undefined}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface AttachmentWrapperProps {
  data: ResponsePlayBookT;
  index: number;
  entity_type?: string;
}

export const AttachmentWrapper: React.FC<AttachmentWrapperProps> = ({
  data,
  index,
  entity_type,
}) => {
  const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const userProfileState = useSelector(UserProfileState);
  const analysePermission = useSelector(AnalysePermissionState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const [isOpenAddAttachment, setOpenAddAttachment] = useState(false);
  const [isOpenEditAttachment, setOpenEditAttachment] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState<number>();
  const [isOpenDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [attachmentToDeleteId, setAttachmentToDeleteId] = useState<number>();
  const [collapse, setCollapse] = useState(false);

  const analyseState = useContext(AnalyseStateContext);
  const analyseDispatch = useContext(AnalyseDispatchContext);

  const [anchorElement, setAnchorElement] = useState<any>(null);
  const [attachmentSelector, setAttachmentSelector] = useState<
    AttachmentResponse
  >();

  const handleUpdatePlaybookOfOpportunity = async () => {
    const data = await getAllPlaybookOfOpportunity(
      analyseState?.opportunity?.id,
    );
    if (data.success) {
      analyseDispatch(updatePlaybookList(data.data));
    }
  };
  const onClickDownload = (item: AttachmentResponse) => {
    const urlDownload = `${item.storage_prefix}/${item.attachment_name}`;
    window.open(urlDownload, '_blank');
  };
  const handleRemoveAttachment = async (id: number) => {
    if (isSubmitting) return;
    await debounceFn();
    const response = await deleteAttachments(id);
    if (response.success) {
      setCollapse(false);
      handleUpdatePlaybookOfOpportunity();
      setOpenDeleteDialog(false);
    }
    endRequest();
  };

  const animatedStyle = {
    transition: '0.3s all',
    transform: collapse ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'pointer',
    },
  };
  const { t } = useTranslation();

  const customQuery = useCallback(() => {
    console.log('collapse', collapse);
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                entity_id: analyseState?.opportunity?.id,
              },
            },
            {
              term: {
                entity_type: 'opportunity_formulate_approach',
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  }, [analyseState, collapse]);
  return (
    <React.Fragment>
      <Box display="flex" alignItems="center" mb={2} mt={2}>
        <Typography component="span" variant="caption">
          {t('attachment')}
        </Typography>
        <Box ml={2} display="flex">
          <IconButton
            size="small"
            onClick={() => {
              setCollapse(!collapse);
            }}
            disabled={
              !analysePermission?.includes(
                AnalysePermission.PlaybookItemAttachmentRead,
              )
            }
          >
            <ArrowForwardIosIcon
              style={animatedStyle}
              className={classes.icon}
            />
          </IconButton>
        </Box>
        <Box display="flex">
          <IconButton
            onClick={event => {
              setAnchorElement(event.currentTarget);
            }}
            disabled={
              !analysePermission?.includes(
                AnalysePermission.PlaybookItemAttachmentCreate,
              )
            }
          >
            <AddCircleOutlineIcon />
          </IconButton>

          <Menu
            id="simple-menu"
            anchorEl={anchorElement}
            keepMounted
            open={Boolean(anchorElement)}
            onClose={() => setAnchorElement(null)}
          >
            {analysePermission?.includes(
              AnalysePermission.PlaybookItemAttachmentCreate,
            ) && (
              <MenuItem
                onClick={() => {
                  setOpenAddAttachment(true);
                  setAnchorElement(null);
                }}
              >
                <Button startIcon={<AddCircleOutlineIcon />}>
                  Create new Attachment
                </Button>
              </MenuItem>
            )}
          </Menu>
          <AttachmentDialog
            isAddingDialog={true}
            isOpen={isOpenAddAttachment}
            data={data}
            attachmentSelector={attachmentSelector}
            handleClose={() => {
              handleUpdatePlaybookOfOpportunity();
              setOpenAddAttachment(false);
              setCollapse(false);
            }}
            entity_type={
              entity_type ? entity_type : `opportunity_playbook_item`
            }
          />
        </Box>
      </Box>
      {collapse && (
        <Box>
          <ReactiveBase
            app={ES_INDICES.attachment_index_name}
            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
            headers={{
              Authorization: window.localStorage.getItem('jwt'),
            }}
          >
            <ReactiveList
              componentId="SearchAttachmentFormulateApproach"
              dataField="attachment"
              renderResultStats={() => null}
              renderNoResults={() => <></>}
              defaultQuery={customQuery}
            >
              {({ data }) => {
                return (
                  <React.Fragment>
                    {data && data?.length > 0 && (
                      <TableContainer>
                        <Table>
                          <EnhancedTableHead classes={classes} />
                          <TableBody>
                            {data?.map((item: any, atmIndex: any) => {
                              return (
                                <TableRow key={atmIndex}>
                                  <TableCell component="th" scope="row">
                                    <Typography
                                      component="span"
                                      variant="body1"
                                    >
                                      {item?.client_file_name}
                                    </Typography>
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    <Typography
                                      component="span"
                                      variant="body1"
                                    >
                                      {item?.description}
                                    </Typography>
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    <Typography
                                      component="span"
                                      variant="body1"
                                    >
                                      {moment(new Date()).format(
                                        'DD/MM/YYYY hh:mma',
                                      )}
                                    </Typography>
                                  </TableCell>
                                  <TableCell
                                    component="th"
                                    scope="row"
                                    padding="none"
                                  >
                                    <TableAction
                                      hiddenItem={['view']}
                                      onClickEdit={() => {
                                        setOpenEditAttachment(true);
                                        setCurrentEditIndex(atmIndex);
                                        setAttachmentSelector(item);
                                      }}
                                      onClickDelete={() => {
                                        setOpenDeleteDialog(true);
                                        setAttachmentToDeleteId(item.id);
                                      }}
                                      disabledDelete={
                                        !analysePermission?.includes(
                                          AnalysePermission.PlaybookItemAttachmentDelete,
                                        ) ||
                                        item.created_by_user.id !==
                                          userProfileState.id
                                          ? true
                                          : false
                                      }
                                    >
                                      <IconButton
                                        onClick={() => onClickDownload(item)}
                                        disabled={item?.storage_type !== 's3'}
                                      >
                                        <CloudDownloadOutlined
                                          fill={
                                            item?.storage_type !== 's3'
                                              ? 'gray'
                                              : undefined
                                          }
                                        />
                                      </IconButton>
                                    </TableAction>
                                  </TableCell>
                                </TableRow>
                              );
                            })}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    )}
                    {(!data || data?.length === 0) && (
                      <Box textAlign="center" padding="5px">
                        No attachment found
                      </Box>
                    )}
                  </React.Fragment>
                );
              }}
            </ReactiveList>
          </ReactiveBase>
        </Box>
      )}
      {isOpenEditAttachment && (
        <AttachmentDialog
          isAddingDialog={false}
          isOpen={isOpenEditAttachment}
          data={data}
          attachmentSelector={attachmentSelector}
          index={currentEditIndex}
          handleClose={() => {
            handleUpdatePlaybookOfOpportunity();
            setOpenEditAttachment(false);
          }}
          entity_type={entity_type ? entity_type : `opportunity_playbook_item`}
        />
      )}
      <DeleteDialog
        isOpen={isOpenDeleteDialog}
        header={t('delete_attachment')}
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_this_attachment')}
            </Typography>
          </Box>
        }
        handleClose={() => setOpenDeleteDialog(false)}
        handleDelete={() => {
          if (attachmentToDeleteId) {
            handleRemoveAttachment(attachmentToDeleteId);
          }
        }}
        disabled={isSubmitting}
      />
    </React.Fragment>
  );
};
