import React, { useContext } from 'react';
import {
  Box,
  Typography,
  Button,
  TextField,
  DialogActions,
} from '@material-ui/core';
import { Formik, Form, FieldArray } from 'formik';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles } from '@material-ui/styles';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  AnalyseStateContext,
  AnalyseDispatchContext,
  AnalyseAction,
  updatePlaybookList,
} from '../AnalyseContext';
import {
  addPlaybookItem,
  getAllPlaybookOfOpportunity,
} from '../../../../../services/playbook.services';
import { useDisableMultipleClick } from '../../../../../hooks';
import { useTranslation } from 'react-i18next';
import { IPlaybookItem } from '../types';
const useStyles = makeStyles(theme => ({
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

interface InitValueT {
  template: IPlaybookItem[];
  isOpenCollapse: number | undefined;
}

interface AddPlaybookItemDialogProps {
  onCancel: () => void;
}

export const AddPlaybookItemDialog: React.FC<AddPlaybookItemDialogProps> = ({
  onCancel,
}) => {
  const classes = useStyles();

  const analyseState = useContext(AnalyseStateContext);
  const analyseDispatch = useContext(AnalyseDispatchContext);
  const initialValues: InitValueT = {
    template: [analyseState.playbookItemTemplate.playbook_item],
    isOpenCollapse: undefined,
  };
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const level = analyseState.playbookItemTemplate.label_item.length ?? 1;
  const indexItem = analyseState.playbookItemTemplate.label_item[level - 1];
  const playbookItem = analyseState.playbookItemTemplate.playbook_item;
  const position = [...analyseState.playbookItemTemplate.label_item].slice(
    0,
    -1,
  );
  const renderLabelItem = (index: number) => {
    if (position.length) {
      return [...position, indexItem + index + 1].join('.');
    }
    return indexItem + index + 1;
  };

  const updatePlaybookOfOpportunity = async () => {
    const data = await getAllPlaybookOfOpportunity(
      analyseState?.opportunity?.id,
    );
    if (data.success) {
      analyseDispatch(updatePlaybookList(data.data));
    }
  };

  const handleAddPlaybookItem = async (values: IPlaybookItem[]) => {
    if (
      !analyseState.chosenPlaybook ||
      typeof analyseState.chosenPlaybook !== 'number' ||
      isSubmitting
    ) {
      return;
    }
    const payload = {
      playbook_items: values.map(item => ({
        title: item.titleName,
        description: item.detail,
        playbook_item_parent_id: item.playbook_item_parent_id,
        item_order: item.item_order,
      })),
    };
    await debounceFn();
    const response = await addPlaybookItem(
      payload,
      analyseState.chosenPlaybook,
    );
    if (response.success) {
      analyseDispatch({
        type: AnalyseAction.CLOSE_ADD_NEW_PLAYBOOK_ITEM,
      });
      analyseDispatch({
        type: AnalyseAction.UPDATE_CURRENT_CHOSEN_EMAIL_LIST,
        payload:
          analyseState.currentChosenEmailList &&
          [...analyseState.currentChosenEmailList].concat([0]),
      });
      updatePlaybookOfOpportunity();
    }
    endRequest();
  };
  const { t } = useTranslation();
  return (
    <Box padding="24px 25px" minWidth="650px">
      <Box textAlign="center" mb={5}>
        <Typography component="p" variant="h3">
          {playbookItem.playbook_item_parent_id
            ? t('add_sub_playbook_item')
            : t('add_playbook_item')}
        </Typography>
      </Box>
      <Box>
        <Formik
          initialValues={initialValues}
          enableReinitialize
          onSubmit={values => {
            handleAddPlaybookItem(values.template);
          }}
        >
          {({ values, handleChange, setFieldValue }) => {
            return (
              <Form>
                <FieldArray name="template">
                  {arrayHelpers => (
                    <Box>
                      {values.template.map((item, index) => {
                        const animatedStyle = {
                          marginRight: '5px',
                          transition: '0.3s all',
                          transform:
                            values.isOpenCollapse === index
                              ? 'rotate(90deg)'
                              : 'rotate(0deg)',
                          '&:hover': {
                            cursor: 'poiter',
                          },
                        };
                        return (
                          <Box mb={3} key={index}>
                            <Box mt={2}>
                              <Box display="flex" mb={2} alignItems="center">
                                <Box
                                  display="flex"
                                  minWidth={110}
                                  alignItems="center"
                                >
                                  <Typography
                                    component="span"
                                    variant="caption"
                                  >
                                    Item {renderLabelItem(index)}
                                  </Typography>

                                  <Box
                                    ml={1}
                                    display="flex"
                                    alignItems="center"
                                  >
                                    <ArrowForwardIosIcon
                                      style={animatedStyle}
                                      className={classes.icon}
                                      fontSize="small"
                                      onClick={() => {
                                        if (values.isOpenCollapse === index) {
                                          setFieldValue(
                                            'isOpenCollapse',
                                            undefined,
                                          );
                                        } else {
                                          setFieldValue(
                                            'isOpenCollapse',
                                            index,
                                          );
                                        }
                                      }}
                                    />
                                  </Box>
                                </Box>
                                <Box mr={1} display="flex">
                                  <Typography
                                    component="span"
                                    variant="caption"
                                  >
                                    Name:
                                  </Typography>
                                </Box>

                                <TextField
                                  variant="outlined"
                                  fullWidth
                                  name={`template.${index}.titleName`}
                                  value={item?.titleName ?? ''}
                                  onChange={handleChange}
                                />
                                <Box ml={2}>
                                  <DeleteIcon
                                    className={classes.icon}
                                    onClick={() => {
                                      arrayHelpers.remove(index);
                                    }}
                                  />
                                </Box>
                              </Box>
                              {values.isOpenCollapse === index && (
                                <Box pl={9}>
                                  <Box>
                                    <Box mb={1}>
                                      <Typography
                                        component="p"
                                        variant="caption"
                                      >
                                        Detail:
                                      </Typography>
                                    </Box>
                                    <TextField
                                      fullWidth
                                      multiline
                                      rows={5}
                                      name={`template.${index}.detail`}
                                      onChange={handleChange}
                                      variant="outlined"
                                      value={item?.detail ?? ''}
                                    />
                                  </Box>
                                </Box>
                              )}
                            </Box>
                          </Box>
                        );
                      })}
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<ControlPointIcon />}
                        onClick={() => {
                          arrayHelpers.push({
                            titleName: '',
                            detail: '',
                            playbook_item_parent_id:
                              playbookItem.playbook_item_parent_id,
                            item_order:
                              playbookItem.item_order +
                              values.template.length +
                              1,
                          });
                        }}
                      >
                        Add Item
                      </Button>
                    </Box>
                  )}
                </FieldArray>
                <DialogActions>
                  <Button color="default" variant="outlined" onClick={onCancel}>
                    Cancel
                  </Button>
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    autoFocus
                    disabled={
                      values.template.every(tp => {
                        if (tp.titleName === '' && tp.detail === '') {
                          return true;
                        }
                        return false;
                      }) || isSubmitting
                    }
                  >
                    Add
                  </Button>
                </DialogActions>
              </Form>
            );
          }}
        </Formik>
      </Box>
    </Box>
  );
};

export default AddPlaybookItemDialog;
