import loadable from '@loadable/component';
export * from './ChatterTab';
export * from './ActivityTab';
export * from './ViewDetailOpp';
export const OpportunityCaptureTab = loadable(() => import('./CaptureTab'));
export const ShapeTab = loadable(() => import('./ShapeTab'));
export const OpportunitySidebar = loadable(() => import('./Sidebar'));
export const AnalyseTab = loadable(() => import('./AnalysisTab'));
export const RecommendTab = loadable(() => import('./RecommendTab'));
