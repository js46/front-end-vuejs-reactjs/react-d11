import React, { useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import {
  Box,
  Grid,
  Tab,
  Tabs,
  Typography,
  Button,
  Dialog,
  TextField,
} from '@material-ui/core';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import { ShareOutlined } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import axios from '../../commons/axios';
import { TabPanel, ExportOpportunity, FormDialog } from '../../components';
import { a11yProps } from '../../utils';
import {
  useEffectOnlyOnce,
  useOpportunityRoute,
  useQueryOpportunity,
} from '../../hooks';
import { SectionBox, TabFluid, TStringBoolean } from '../../components/styled';
import {
  getAnOpportunity,
  addOpportunityCurrentExperts,
  updateResourceTeam,
  deleteOpportunity,
  updatePhaseForOpportunity,
  updateOpportunityTag,
} from '../../services/opportunity.services';
import {
  ChatterTab,
  OpportunityCaptureTab,
  OpportunitySidebar,
  ShapeTab,
  AnalyseTab,
  RecommendTab,
} from './components';
import {
  MainActionSelection,
  ExpertAssignment,
  TeamRedirection,
  CommonSelectedAction,
  Endorsement,
  ChangePhase,
} from './components/ActionButton';
import { OpportunityActionEnum, Status, Message } from '../../constants';
import {
  IListLocations,
  IListRole,
  ISkillsInRole,
  getLocations,
  getRoleProfile,
  getAllSkills,
} from '../../services/profile.services';
import {
  IResourceTeam,
  getResourceTeam,
  ITag,
  getListTags,
} from '../../services/references.services';
import { Breadcrumb } from '../../layouts/main/components';
import { addTaskComment } from '../../services/comment.services';
import OpportunityContext from './OpportunityContext';
import { OpportunityPhase } from '../../constants/opportunityPhase';
import { useTranslation } from 'react-i18next';
import { DeleteDialog } from '../../components';
import {
  addFavoriteItem,
  deleteFavoriteItem,
  EntityType,
} from '../../services/favorite.services';
import { UserProfileState } from '../../store/user/selector';
import {
  RequestDetailOpportunity,
  UpdatedOpportunityDetail as updatedOpportunityDetailAction,
} from '../../store/opportunity/actions';
import {
  OpportunityState,
  OpportunityPossibleActions,
  OpportunityMapActionState,
  OpportunityPhaseState,
} from '../../store/opportunity/selector';
import { notifyError, notifySuccess } from '../../store/common/actions';
import { useToggleValue } from '../../hooks';
import { SidebarList } from './components/Sidebar';
import { OpportunityAction } from '../../services/user.services';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import { Autocomplete } from '@material-ui/lab';

const OPPORTUNITY = 'opportunity';
const TASK = 'task';
const useStyles = makeStyles(theme => ({
  actionDialog: {
    '& .MuiDialog-paper': {
      padding: '50px 64px',
    },
  },
  successTitle: {
    lineHeight: '25px',
    padding: '0 40px',
    textAlign: 'center',
    fontWeight: 700,
    color: theme.palette.text.primary,
    '& h2': {
      fontSize: '20px',
    },
  },
  mrRight10: {
    marginRight: 10,
  },
}));

export interface TGroupStateAction {
  isOpenActionDialog: boolean;
  isShareActionDialog: boolean;
  selectedAction: string;
  isClickNext: boolean;
  isSubmitting: Status;
}

const initState: TGroupStateAction = {
  isOpenActionDialog: false,
  isShareActionDialog: false,
  selectedAction: '',
  isClickNext: false,
  isSubmitting: Status.Idle,
};
export const Opportunity: React.FC = () => {
  const { id } = useParams();
  const classes = useStyles();
  const history = useHistory();
  if (!id) {
    history.push('/search');
  }
  const { canViewOpportunity } = useQueryOpportunity(false);
  const possibleActions = useSelector(OpportunityPossibleActions);
  const userProfileState = useSelector(UserProfileState);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { tab } = useOpportunityRoute();
  const [groupState, setGroupState] = useState(initState);
  const [activeTab, setActiveTab] = useState(tab);
  const [loading, setLoading] = useState(true);
  const [activeSidebarTab, setSidebarActiveTab] = useState(0);

  const [profileRole, setProfileRole] = useState<IListRole[]>();
  const [locations, setLocations] = useState<IListLocations[]>();
  const [resourceTeam, setResourceTeam] = useState<IResourceTeam[]>();
  const [skillList, setSkillList] = useState<ISkillsInRole[]>();
  const opportunity = useSelector(OpportunityState);
  const [openTagDialog, setOpenTagDialog] = useState<boolean>(false);
  const [tagReferences, setTagReferences] = useState<ITag[]>([]);
  const [tagForm, setTagForm] = useState<number[]>([]);
  const isTeamLead =
    userProfileState.team_lead_flg &&
    userProfileState.resource_team.resource_team_id ===
      opportunity?.resource_team?.id;
  const isUserReview =
    opportunity?.current_expert_review?.expert_review?.id ===
    userProfileState.id;
  const listButtonAction =
    possibleActions.filter(action => {
      return (
        [
          OpportunityActionEnum.Endorse,
          OpportunityActionEnum.SendEndorsementRequest,
          OpportunityActionEnum.CancelEndorsementRequest,
          OpportunityActionEnum.SendBackForReview,
          OpportunityActionEnum.SendBackForAnalysis,
          OpportunityActionEnum.OpportunityAnalysis,
          OpportunityActionEnum.AllocateResources,
          OpportunityActionEnum.FinishResourceAllocation,
        ].indexOf(action.display_name as OpportunityActionEnum) === -1
      );
    }) ?? [];
  const opportunityMapAction = useSelector(OpportunityMapActionState);
  const opportunityPhase = useSelector(OpportunityPhaseState);

  const keyAllStateAction = `${opportunityPhase}_all_state`;
  if (opportunityMapAction.has(keyAllStateAction)) {
    let actionDelete = opportunityMapAction
      .get(keyAllStateAction)
      ?.find(item => item.casbin_name === `role:${opportunityPhase}_delete`);

    let backPhaseActions:
      | OpportunityAction[]
      | undefined = opportunityMapAction
      .get(keyAllStateAction)
      ?.filter(
        item => item.casbin_name === `role:${opportunityPhase}_back_phase`,
      );

    if (backPhaseActions) {
      backPhaseActions.map(action => listButtonAction.push(action));
    }
    const keyAfterBacklog = `${opportunityPhase}_after_backlog`;
    const oppStateReviewBacklog = opportunityPhase === 'shape' ? 1 : 5;
    if (
      opportunityMapAction.has(keyAfterBacklog) &&
      opportunity?.opportunity_state?.id !== oppStateReviewBacklog
    ) {
      let afterBacklogAction = opportunityMapAction
        .get(keyAfterBacklog)
        ?.find(
          item => item.casbin_name === `role:${opportunityPhase}_back_phase`,
        );
      if (afterBacklogAction) {
        listButtonAction.push(afterBacklogAction);
      }
    }

    if (actionDelete) {
      listButtonAction.push(actionDelete as OpportunityAction);
    }
  }

  const [openModal, toggleModal] = useToggleValue(false);
  const [currentType, setType] = useState<SidebarList | null>(null);

  useEffectOnlyOnce(() => {
    onRefresh();
    Promise.all([
      //getDataOpportunity(),
      getRoleProfileLists(),
      getLocationLists(),
      getResourceTeamLists(),
      getSkillLists(),
      getListTagsReferences(),
      //fetchOpportunityPermissions(),
    ]).then(() => {
      setLoading(false);
    });
  });
  const onRefresh = () => {
    dispatch(RequestDetailOpportunity(id));
  };
  const onChangeTab = (event: React.ChangeEvent<{}>, newValue: number) => {
    setActiveTab(newValue);
  };

  const getListTagsReferences = async () => {
    const response = await getListTags([{ key: 'limit', value: 1000 }]);
    if (response.success) {
      setTagReferences(response.data.list ?? []);
    }
  };

  const getRoleProfileLists = async () => {
    const response = await getRoleProfile();
    if (response.success) {
      setProfileRole(response.data.list);
    }
  };

  const getResourceTeamLists = async () => {
    const response = await getResourceTeam();
    if (response.success) {
      setResourceTeam(response.data.list);
    }
  };

  const getLocationLists = async () => {
    const response = await getLocations();
    if (response.success) {
      setLocations(response.data.list);
    }
  };

  const getSkillLists = async () => {
    const response = await getAllSkills();
    if (response.success) {
      setSkillList(response.data.list);
    }
  };

  const onChangeSidebarTab = (
    event: React.ChangeEvent<{}>,
    newValue: number,
  ) => {
    setSidebarActiveTab(newValue);
  };

  const getDataOpportunity = useCallback(async () => {
    const response = await getAnOpportunity(Number(id));
    if (response.data) {
      dispatch(updatedOpportunityDetailAction(response.data));
    }
  }, [dispatch, id]);

  const handleChangeAction = (event: React.ChangeEvent<HTMLInputElement>) => {
    setGroupState({ ...groupState, selectedAction: event.target.value });
  };

  const handleAddComment = useCallback(
    (entityType: string, commentType: string, commentValue: string) => {
      if (id && commentValue !== '') {
        const bodyRequest = {
          entity_id: parseInt(id),
          entity_type: entityType,
          comment_type: commentType,
          comment_body: commentValue,
          comment_parent_id: 0,
        };
        addTaskComment(bodyRequest);
      }
    },
    [id],
  );

  const handleCloseDialog = useCallback(() => {
    setGroupState({
      ...groupState,
      isOpenActionDialog: false,
      isShareActionDialog: false,
      isClickNext: false,
      selectedAction: '',
      isSubmitting: Status.Idle,
    });
  }, [groupState]);

  const handleAssignExpert = useCallback(
    async (
      commentValue: string,
      emailId?: number,
      // teamRoleName: string,
      // duration?: number,
    ) => {
      const actionId = possibleActions.find(
        item => item.display_name === groupState.selectedAction,
      )?.action_id;
      if (emailId && opportunity?.id) {
        setGroupState({
          ...groupState,
          isSubmitting: Status.Submitting,
        });
        handleAddComment(OPPORTUNITY, 'ASSIGN_EXPERT', commentValue);
        addOpportunityCurrentExperts(opportunity.id, {
          action_id: actionId!,
          expert: {
            user_id: emailId,
            assignment_date: new Date(),
          },
          activity_message: commentValue,
        })
          .then(({ data }) => {
            dispatch(
              notifySuccess({
                message: t('opportunity.successful_expert_assignment'),
              }),
            );
            handleCloseDialog();
            dispatch(updatedOpportunityDetailAction(data));
          })
          .catch(err => {
            dispatch(
              notifyError({
                message: Message.Error,
              }),
            );
          });
      }
    },
    [
      possibleActions,
      opportunity,
      groupState,
      handleAddComment,
      dispatch,
      t,
      handleCloseDialog,
    ],
  );

  const handleChangeStatus = useCallback(
    async (commentValue: string, actionID?: number) => {
      if (actionID && opportunity?.id) {
        setGroupState({
          ...groupState,
          isSubmitting: Status.Submitting,
        });
        handleAddComment(OPPORTUNITY, 'CHANGE_STATUS', commentValue);
        updatePhaseForOpportunity(opportunity.id, {
          action_id: actionID,
        })
          .then(({ data }) => {
            dispatch(
              notifySuccess({
                message: t('opportunity.successful_change_status'),
              }),
            );
            handleCloseDialog();
            dispatch(updatedOpportunityDetailAction(data));
          })
          .catch(err => {
            dispatch(
              notifyError({
                message: Message.Error,
              }),
            );
          });
      }
    },
    [dispatch, groupState, handleAddComment, handleCloseDialog, opportunity, t],
  );

  const handleReviewYourOwn = useCallback(
    (
      commentValue: string,
      emailId?: number,
      // teamRoleName: string,
      // duration?: number,
    ) => {
      const actionId = possibleActions?.find(
        item => item.display_name === groupState.selectedAction,
      )?.action_id;
      if (emailId && opportunity?.id) {
        setGroupState({
          ...groupState,
          isSubmitting: Status.Submitting,
        });

        handleAddComment(OPPORTUNITY, 'ASSIGN_EXPERT_YOURSELF', commentValue);
        addOpportunityCurrentExperts(opportunity.id, {
          action_id: actionId!,
          expert: {
            user_id: emailId,
            assignment_date: new Date(),
            // team_role_name: teamRoleName,
            // day: duration || 0,
          },
          activity_message: commentValue,
        })
          .then(({ data }) => {
            handleCloseDialog();
            dispatch(updatedOpportunityDetailAction(data));
          })
          .catch(err => console.log(err));
      }
    },
    [
      dispatch,
      groupState,
      handleAddComment,
      handleCloseDialog,
      opportunity,
      possibleActions,
    ],
  );

  const handleRedirectTeam = useCallback(
    async (
      commentValue: string,
      selectedTeam?: string,
      resourceTeam?: IResourceTeam[],
    ) => {
      const idSelectedTeam = resourceTeam?.find(
        team => team.name === selectedTeam,
      );
      if (opportunity?.id && idSelectedTeam) {
        setGroupState({
          ...groupState,
          isSubmitting: Status.Submitting,
        });
        handleAddComment(OPPORTUNITY, 'REDIRECT_ANOTHER_TEAM', commentValue);
        const response = await updateResourceTeam(opportunity?.id, {
          resource_team_id: idSelectedTeam?.id,
        });
        if (response.success) {
          dispatch(
            notifySuccess({
              message: t('opportunity.successful_team_redirection'),
            }),
          );
          handleCloseDialog();
          dispatch(updatedOpportunityDetailAction(response.data));
        } else {
          dispatch(
            notifyError({
              message: Message.Error,
            }),
          );
        }
      }
    },
    [opportunity, groupState, handleAddComment, dispatch, t, handleCloseDialog],
  );

  const handleDynamicAction = useCallback(
    async (message?: string) => {
      setGroupState({ ...groupState, isSubmitting: Status.Submitting });
      const expectedAction = possibleActions.find(
        item => item.display_name === groupState.selectedAction,
      );
      return await axios.post(
        `/api/v1${expectedAction?.path}/${opportunity?.id}`,
        {
          action_id: expectedAction?.action_id,
          opportunity_phase_id: opportunity?.opportunity_phase?.id,
          activity_message: message,
        },
      );
    },
    [groupState, opportunity, possibleActions],
  );

  const handleExpertReviewDone = useCallback(
    async (commentValue: string) => {
      const response = await handleDynamicAction();

      handleAddComment(OPPORTUNITY, 'EXPERT_REVIEW_DONE', commentValue);
      if (response.data.success) {
        handleCloseDialog();
        dispatch(updatedOpportunityDetailAction(response.data.data));
      }
    },
    [dispatch, handleAddComment, handleCloseDialog, handleDynamicAction],
  );

  const handleSeekEndorsement = useCallback(
    async (commentValue: string, emailId?: number) => {
      if (emailId && opportunity?.id) {
        const response = await handleDynamicAction(commentValue);
        if (response.data.success) {
          handleAddComment(TASK, 'ENDORSE', commentValue);
          dispatch(
            notifySuccess({
              message: t('opportunity.successful_endorsement_seeking'),
            }),
          );
          handleCloseDialog();
          dispatch(RequestDetailOpportunity(opportunity.id));
          //dispatch(updatedOpportunityDetailAction(response.data.data));
        } else {
          dispatch(
            notifyError({
              message: Message.Error,
            }),
          );
        }
      }
    },
    [
      opportunity,
      handleDynamicAction,
      handleAddComment,
      dispatch,
      t,
      handleCloseDialog,
    ],
  );

  const handleSeekEndorsementDone = useCallback(
    async (commentValue: string) => {
      setGroupState({ ...groupState, isSubmitting: Status.Submitting });
      const response = await handleDynamicAction(commentValue);
      if (response.data.success) {
        handleAddComment(TASK, 'ENDORSE_DONE', commentValue);
        dispatch(RequestDetailOpportunity(Number(id)));
        dispatch(
          notifySuccess({
            message: t('opportunity.successful_endorsement_seeking_done'),
          }),
        );
        setGroupState({
          ...groupState,
          isOpenActionDialog: false,
          isClickNext: false,
          selectedAction: '',
        });
        dispatch(updatedOpportunityDetailAction(response.data.data));
      }
    },
    [groupState, handleDynamicAction, handleAddComment, dispatch, id, t],
  );

  const handlePause = useCallback(
    async (commentValue: string) => {
      const response = await handleDynamicAction(commentValue);
      if (response.data.success) {
        handleAddComment(OPPORTUNITY, 'PAUSE', commentValue);
        dispatch(
          notifySuccess({
            message: t('opportunity.successful_pause'),
          }),
        );
        handleCloseDialog();
        dispatch(updatedOpportunityDetailAction(response.data.data));
      } else {
        dispatch(
          notifyError({
            message: Message.Error,
          }),
        );
      }
    },
    [handleDynamicAction, handleAddComment, dispatch, t, handleCloseDialog],
  );

  const handleCancelEndorsementRequest = useCallback(
    async (commentValue: string, type = 'SEND_BACK_FOR_REVIEW') => {
      const response = await handleDynamicAction(commentValue);
      if (response.data.success) {
        handleAddComment(TASK, type, commentValue);
        dispatch(
          notifySuccess({
            message:
              type === OpportunityActionEnum.SendBackForReview
                ? t('opportunity.successful_send_back_for_review')
                : t('opportunity.successful_endorsement_cancellation'),
          }),
        );
        handleCloseDialog();
        dispatch(updatedOpportunityDetailAction(response.data.data));
      } else {
        dispatch(
          notifyError({
            message: Message.Error,
          }),
        );
      }
    },
    [handleDynamicAction, handleAddComment, dispatch, t, handleCloseDialog],
  );

  const handleUnpause = useCallback(
    async (commentValue: string) => {
      const response = await handleDynamicAction(commentValue);
      if (response.data.success) {
        handleAddComment(OPPORTUNITY, 'UNPAUSE', commentValue);
        dispatch(
          notifySuccess({
            message: t('opportunity.successful_unpause'),
          }),
        );
        handleCloseDialog();
        dispatch(updatedOpportunityDetailAction(response.data.data));
      } else {
        dispatch(
          notifyError({
            message: Message.Error,
          }),
        );
      }
    },
    [handleDynamicAction, handleAddComment, dispatch, t, handleCloseDialog],
  );

  const handleDeleteOpportunity = async () => {
    if (!id) return;
    const response = await deleteOpportunity(parseInt(id));
    if (response.success) {
      dispatch(
        notifySuccess({
          message: t('opportunity.successful_delete'),
        }),
      );
      history.push('/');
    } else {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
  };
  const getSelectedActionInActionDialog = useCallback(
    (selectedAction: string) => {
      switch (selectedAction) {
        case OpportunityActionEnum.StartReview:
          return (
            <ExpertAssignment
              opportunity={opportunity}
              handleAssignExpert={handleAssignExpert}
              handleReviewYourOwn={handleReviewYourOwn}
            />
          );
        case OpportunityActionEnum.AssignExpertForReview:
          return (
            <ExpertAssignment
              opportunity={opportunity}
              handleAssignExpert={handleAssignExpert}
              handleReviewYourOwn={handleReviewYourOwn}
            />
          );
        case OpportunityActionEnum.RedirectAnotherTeam:
          return (
            <TeamRedirection
              opportunity={opportunity}
              handleRedirectTeam={handleRedirectTeam}
            />
          );
        case OpportunityActionEnum.Endorse:
          return (
            <CommonSelectedAction
              handleClickButton={handleSeekEndorsementDone}
              title="Endorse Done"
              buttonName="Endorse"
            />
          );
        case OpportunityActionEnum.ExpertReviewDone:
          return (
            <CommonSelectedAction
              handleClickButton={handleExpertReviewDone}
              title="Expert Review Done"
              buttonName="Expert Review Done"
            />
          );
        case OpportunityActionEnum.Pause:
          return (
            <CommonSelectedAction
              handleClickButton={handlePause}
              title="Pause"
              buttonName="Pause Process"
            />
          );
        case OpportunityActionEnum.SendEndorsementRequest:
          return (
            <Endorsement
              opportunity={opportunity}
              handleClickButton={handleSeekEndorsement}
            />
          );
        case OpportunityActionEnum.CancelEndorsementRequest:
          return (
            <CommonSelectedAction
              handleClickButton={handleCancelEndorsementRequest}
              title="Cancel Endorsement Request"
              buttonName="Cancel Endorsement"
            />
          );
        case OpportunityActionEnum.SendBackForReview:
          return (
            <CommonSelectedAction
              handleClickButton={value =>
                handleCancelEndorsementRequest(
                  value,
                  OpportunityActionEnum.SendBackForReview,
                )
              }
              title="Send Back For Review"
              buttonName="Send Back For Review"
            />
          );
        case OpportunityActionEnum.SendBackForAnalysis:
          return (
            <CommonSelectedAction
              handleClickButton={value =>
                handleCancelEndorsementRequest(
                  value,
                  OpportunityActionEnum.SendBackForAnalysis,
                )
              }
              title={OpportunityActionEnum.SendBackForAnalysis}
              buttonName={OpportunityActionEnum.SendBackForAnalysis}
            />
          );
        case OpportunityActionEnum.Unpause:
          return (
            <CommonSelectedAction
              handleClickButton={handleUnpause}
              title="Unpause"
              buttonName="Unpause"
            />
          );
        case OpportunityActionEnum.ChangeStatusWaitingTo:
          return (
            <ChangePhase
              handleChangeStatus={handleChangeStatus}
              onRefresh={getDataOpportunity}
              listActions={listButtonAction.filter(
                action =>
                  action.casbin_name === `role:${opportunityPhase}_back_phase`,
              )}
            />
          );
        default:
          return <></>;
      }
    },
    [
      getDataOpportunity,
      handleAssignExpert,
      handleCancelEndorsementRequest,
      handleChangeStatus,
      handleExpertReviewDone,
      handlePause,
      handleRedirectTeam,
      handleReviewYourOwn,
      handleSeekEndorsement,
      handleSeekEndorsementDone,
      handleUnpause,
      listButtonAction,
      opportunity,
      opportunityPhase,
    ],
  );

  const toggleFavorite = async () => {
    if (!opportunity) return;
    try {
      const favorite = opportunity.favorites?.find(
        item => item.user_id === userProfileState.id,
      );
      if (favorite) {
        await deleteFavoriteItem(favorite.id);
      } else {
        await addFavoriteItem(EntityType.opportunity, opportunity.id);
      }
      await getDataOpportunity();
    } catch (e) {
      console.log('Error marking favorite', e);
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
  };

  const handleSubmitTags = async (
    event?: React.FormEvent<HTMLFormElement> | undefined,
  ) => {
    event?.preventDefault();
    const payload = tagForm.map(item => {
      return { tag_id: item };
    });
    if (opportunity) {
      const response = await updateOpportunityTag(opportunity.id, {
        tags: payload,
      });
      if (response.success) {
        setOpenTagDialog(false);
        onRefresh?.();
      }
    }
  };

  if (loading) return <></>;

  if (!id || !opportunity) {
    return <Box mt={2}>{t('opp_not_found')}</Box>;
  }

  if (!canViewOpportunity(opportunity)) {
    return <Box mt={2}>{t('opp_unauthorized')}</Box>;
  }
  return (
    <OpportunityContext.Provider
      value={{
        setGroupState,
        groupState,
        onRefresh,
        roleList: profileRole,
        locationList: locations,
        teamList: resourceTeam,
        skillList: skillList,
      }}
    >
      <OpportunitySidebar
        openModal={openModal}
        toggleModal={toggleModal}
        currentType={currentType}
        setType={setType}
      />
      <SectionBox
        data-has-sidebar={TStringBoolean.TRUE}
        component="section"
        style={{ paddingTop: 0 }}
      >
        <Breadcrumb />
        <Box mt={2}>
          <Box mb={3}>
            <Box mb={3}>
              <Grid container item lg={9} spacing={1}>
                <Grid xs={11} item alignItems="center" container>
                  <Box mr={'10px'}>
                    <Typography variant="h1" component="div">
                      {opportunity.title}
                      <Box ml={2} display="inline">
                        <Button
                          startIcon={
                            opportunity.favorites?.some(
                              item => item.user_id === userProfileState.id,
                            ) ? (
                              <ThumbUpIcon />
                            ) : (
                              <ThumbUpAltOutlinedIcon />
                            )
                          }
                          onClick={toggleFavorite}
                        >
                          <b>{opportunity.favorites?.length ?? 0}</b>
                        </Button>
                        <Button
                          variant="text"
                          onClick={() =>
                            setGroupState({
                              ...groupState,
                              isShareActionDialog: true,
                            })
                          }
                          color="inherit"
                        >
                          <ShareOutlined className={classes.mrRight10} />
                        </Button>

                        <Button
                          onClick={() => setOpenTagDialog(true)}
                          disabled={
                            !userProfileState.isAdmin &&
                            !isTeamLead &&
                            !isUserReview
                          }
                        >
                          <LocalOfferIcon className={classes.mrRight10} />
                        </Button>
                      </Box>
                    </Typography>
                  </Box>
                </Grid>
                <Grid
                  container
                  xs={1}
                  item
                  justify={'flex-end'}
                  alignItems={'flex-start'}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() =>
                      setGroupState({
                        ...groupState,
                        isOpenActionDialog: true,
                      })
                    }
                    disabled={
                      listButtonAction.length === 0 ||
                      (!userProfileState.isAdmin &&
                        !isTeamLead &&
                        !isUserReview)
                    }
                  >
                    Action
                  </Button>
                </Grid>
              </Grid>
            </Box>
            <Grid container spacing={2}>
              {/* Start Opportunity detail tabs */}
              <Grid item xs lg={9}>
                <Tabs
                  value={activeTab}
                  onChange={onChangeTab}
                  aria-label="opportunity tabs"
                >
                  <Tab
                    label="Capture"
                    {...a11yProps('capture-tab', OpportunityPhase.Capture)}
                  />
                  <Tab
                    label="Shape"
                    {...a11yProps('shape-tab', OpportunityPhase.Shape)}
                  />
                  <Tab
                    label="Analyse"
                    {...a11yProps('analysis-tab', OpportunityPhase.Analyse)}
                  />
                  <Tab
                    label="Recommend"
                    {...a11yProps('recommend-tab', OpportunityPhase.Recommend)}
                  />
                  <Tab
                    label="Implement"
                    {...a11yProps('implement-tab', OpportunityPhase.Implement)}
                  />
                  <Tab
                    label="Track"
                    {...a11yProps('track-tab', OpportunityPhase.Track)}
                  />
                </Tabs>
                <Box mt={2}>
                  <TabPanel value={activeTab} index={OpportunityPhase.Capture}>
                    <OpportunityCaptureTab
                      opportunity={opportunity}
                      onRefresh={getDataOpportunity}
                    />
                  </TabPanel>
                  <TabPanel value={activeTab} index={OpportunityPhase.Shape}>
                    <ShapeTab />
                  </TabPanel>
                  <TabPanel value={activeTab} index={OpportunityPhase.Analyse}>
                    <AnalyseTab
                      chosenOpportunity={opportunity}
                      toggleModal={toggleModal}
                      setType={setType}
                    />
                  </TabPanel>
                  <TabPanel
                    value={activeTab}
                    index={OpportunityPhase.Recommend}
                  >
                    <RecommendTab />
                  </TabPanel>
                  <TabPanel
                    value={activeTab}
                    index={OpportunityPhase.Implement}
                  >
                    <Box m={2}>{t('under_construction')}</Box>
                  </TabPanel>
                  <TabPanel value={activeTab} index={OpportunityPhase.Track}>
                    <Box m={2}>{t('under_construction')}</Box>
                  </TabPanel>
                </Box>
              </Grid>
              {/* End Opportunity detail tabs */}
              {/* Start Right sidebar tabs */}
              <Grid item xs lg={3}>
                <Tabs
                  value={activeSidebarTab}
                  onChange={onChangeSidebarTab}
                  aria-label="sidebar tabs"
                >
                  <TabFluid
                    label="Chativity"
                    {...a11yProps('sidebar-tab', 0)}
                  />
                  <TabFluid label="Wizard" {...a11yProps('sidebar-tab', 1)} />
                  {/* <TabFluid label="Activity" {...a11yProps('sidebar-tab', 2)} /> */}
                </Tabs>
                <TabPanel value={activeSidebarTab} index={0}>
                  <ChatterTab opportunity={opportunity} />
                </TabPanel>
                <TabPanel value={activeSidebarTab} index={1}>
                  <Box m={2}>{t('under_construction')}</Box>
                </TabPanel>
                {/* <TabPanel value={activeSidebarTab} index={2}>
                  <ActivityTab opportunity={opportunity} />
                </TabPanel> */}
              </Grid>
              {/* End Right sidebar tabs */}
            </Grid>
          </Box>
          {/*  dialog share */}

          <ExportOpportunity
            open={groupState.isShareActionDialog}
            close={() => {
              setGroupState({
                ...groupState,
                isShareActionDialog: false,
              });
            }}
            opportunity={opportunity}
          />
          {/*  dialog action */}
          {groupState.isOpenActionDialog && (
            <Dialog
              open={groupState.isOpenActionDialog}
              onClose={() => {
                setGroupState({
                  ...groupState,
                  isOpenActionDialog: false,
                  isClickNext: false,
                });
              }}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              className={classes.actionDialog}
            >
              {!groupState.isClickNext && groupState.isOpenActionDialog && (
                <MainActionSelection
                  groupState={groupState}
                  listActions={listButtonAction.filter(
                    action =>
                      action.casbin_name !==
                      `role:${opportunityPhase}_back_phase`,
                  )}
                  setGroupState={setGroupState}
                  handleChangeAction={handleChangeAction}
                />
              )}
              {groupState.isClickNext &&
                groupState.isOpenActionDialog &&
                getSelectedActionInActionDialog(groupState.selectedAction)}
            </Dialog>
          )}
          {groupState.isClickNext && groupState.selectedAction === 'Delete' && (
            <DeleteDialog
              isOpen={groupState.selectedAction === 'Delete'}
              header="Delete opportunity"
              message="Are you sure you want to delete this Opportunity?"
              handleClose={() => {
                setGroupState({
                  ...groupState,
                  isClickNext: false,
                });
              }}
              handleDelete={handleDeleteOpportunity}
            />
          )}
          <FormDialog
            title="Tags"
            submit={handleSubmitTags}
            open={openTagDialog}
            setOpen={setOpenTagDialog}
            isAddingForm={false}
            configLabel={'OK'}
            disable={false}
            formContent={
              <Box mt={2} mb={2}>
                <Autocomplete
                  multiple
                  id="multiple-limit-tags"
                  onChange={(e, value) => {
                    setTagForm(value?.map(item => item.id ?? 0) ?? []);
                  }}
                  defaultValue={opportunity?.tags}
                  options={tagReferences ?? []}
                  getOptionLabel={option => option?.name ?? ''}
                  renderInput={params => (
                    <TextField
                      {...params}
                      variant="outlined"
                      placeholder="Add tag"
                    />
                  )}
                />
              </Box>
            }
          />
        </Box>
      </SectionBox>
    </OpportunityContext.Provider>
  );
};

export default Opportunity;
