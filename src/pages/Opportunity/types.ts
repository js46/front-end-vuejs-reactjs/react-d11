import {
  IOpportunity,
  IOpportunityParam,
  IAttachments,
} from '../../services/opportunity.services';
import {
  IBusinessUnit,
  ICorporateObjective,
  IBusinessProcesses,
  IBenefitFocusCategory,
  IBenefitFocusValue,
} from '../../services/references.services';
import { IComment, ICommentParam } from '../../services/comment.services';

interface ICaptureAction {
  handleNext?: () => void;
  handleBack?: () => void;
  isSubmitAll?: boolean;
  handleOpportunity?: (
    values: IOpportunityParam,
    type?: string,
  ) => Promise<void>;
  handleComment?: (values: ICommentParam, type?: string) => Promise<void>;
  deleteOpportunity?: () => Promise<void>;
}

export interface ICaptureBenefitsProps extends ICaptureAction {
  opportunity?: IOpportunity;
  corporateObjectives: ICorporateObjective[];
  benefitFocuses: IBenefitFocusCategory[];
  benefitFocusValues: IBenefitFocusValue[];
}

export interface ICaptureDetailProps extends ICaptureAction {
  businessUnits: IBusinessUnit[];
  businessProcesses: IBusinessProcesses[];
  corporateObjectives: ICorporateObjective[];
  benefitFocuses: IBenefitFocusCategory[];
}

export interface ICaptureDuplicatesProps extends ICaptureAction {
  opportunity?: IOpportunity;
  comment?: IComment;
}

export interface ICaptureAttachmentsProps extends ICaptureAction {
  opportunity?: IOpportunity;
  attachments: Array<IAttachments>;
  handleChangeFile: (index: number, value: string) => void;
  handleRemoveFile?: (index: number) => void;
  handleRemove: (index: number) => void;
  handleAddAttachment: () => void;
  handleChangeDescription: (index: number, value: string) => void;
}

export interface ICaptureReviewsProps extends ICaptureAction {
  opportunity?: IOpportunity;
}
export interface ICaptureResponsesProps extends ICaptureAction {
  opportunity?: IOpportunity;
  comment?: IComment;
}
export interface ICaptureSubmitProps extends ICaptureAction {
  opportunity?: IOpportunity;
  handleFinalSubmit: () => void;
  benefitFocuses: IBenefitFocusCategory[];
}

export interface IStepButtonDetailProps {
  handleSubmit: (e?: React.FormEvent<HTMLFormElement> | undefined) => void;
  handleBack: () => void;
  isDisableNext: boolean;
  deleteOpportunity: () => Promise<void>;
  isHideBack?: boolean;
  step?: string;
  opportunity?: IOpportunity;
}
export enum AnalysePermission {
  PlaybookRead = 'role:analyse_playbook_read',
  PlaybookCreate = 'role:analyse_playbook_create',
  PlaybookUpdate = 'role:analyse_playbook_update',
  PlaybookDelete = 'role:analyse_playbook_delete',
  PlaybookItemRead = 'role:analyse_playbook_item_read',
  PlaybookItemCreate = 'role:analyse_playbook_item_create',
  PlaybookItemUpdate = 'role:analyse_playbook_item_update',
  PlaybookItemDelete = 'role:analyse_playbook_item_delete',
  PlaybookItemAssign = 'role:analyse_playbook_item_assign',
  PlaybookItemUnassign = 'role:analyse_playbook_item_unassign',
  PlaybookItemDone = 'role:analyse_playbook_item_done',
  PlaybookItemUndone = 'role:analyse_playbook_item_undone',
  PlaybookItemNoteRead = 'role:analyse_playbook_item_note_read',
  PlaybookItemNoteCreate = 'role:analyse_playbook_item_note_create',
  PlaybookItemNoteUpdate = 'role:analyse_playbook_item_note_update',
  PlaybookItemNoteDelete = 'role:analyse_playbook_item_note_delete',
  PlaybookItemAttachmentRead = 'role:analyse_playbook_item_attachment_read',
  PlaybookItemAttachmentCreate = 'role:analyse_playbook_item_attachment_create',
  PlaybookItemAttachmentUpdate = 'role:analyse_playbook_item_attachment_update',
  PlaybookItemAttachmentDelete = 'role:analyse_playbook_item_attachment_delete',
}
