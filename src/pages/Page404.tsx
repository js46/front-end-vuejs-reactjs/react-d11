import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    minHeight: '75vh',
  },
}));

export const Page404: React.FC = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      className={classes.root}
    >
      {' '}
      <Typography component="h2" variant="h3" color="inherit" noWrap>
        {t('page_not_found')}
      </Typography>
    </Grid>
  );
};
