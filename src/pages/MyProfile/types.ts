export interface ISkills {
  skill_name: string;
  proficiency_level_id: number;
  favorites?: Array<{ id: number; user_id: number }> | null;
  endorsements: number;
  readOnly?: boolean;
}

export interface IContent {
  name: string;
  favorites: number;
  date?: any;
}
export interface IProficiency {
  title: string;
}

export interface IRecommendations {
  name: string;
  content: string;
  date: any;
}

export const ProficiencyTitle: Array<IProficiency> = [
  {
    title: 'Key Accomplishments',
  },
  {
    title: 'Awards',
  },
  {
    title: 'Publications',
  },
  {
    title: 'Professional Certifications',
  },
  {
    title: 'Knowledgebase Assets Created',
  },
];
