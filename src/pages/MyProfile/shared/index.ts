import Rating from '@material-ui/lab/Rating';
import { withStyles } from '@material-ui/core/styles';

export const StyledRating = withStyles({
  iconFilled: {
    '& div': {
      // borderColor: '#333333',
      backgroundColor: '#333333',
    },
  },
  iconHover: {
    '& div': {
      // borderColor: '#333333',
      backgroundColor: '#333333',
    },
  },
})(Rating);
export const StyledRatingView = withStyles({
  iconFilled: {
    '& div': {
      // borderColor: '#2C54E3',
      backgroundColor: '#2C54E3',
    },
  },
  iconHover: {
    '& div': {
      // borderColor: '#2C54E3',
      backgroundColor: '#2C54E3',
    },
  },
})(Rating);
