import React, { useState, useEffect, SetStateAction, useCallback } from 'react';
import { Grid, Typography, Box, Avatar, Button, Chip } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Admin, User100 } from '../../assets';
import { useParams } from 'react-router-dom';

import {
  getAllSkills,
  getUserProfile,
  IProfileData,
  ISkillsInRole,
} from '../../services/profile.services';
import { ViewProfile, EditProfile } from './components';
import { Status } from '../../constants';
import { delay, getRoleLabels } from '../../utils';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../store/user/selector';

const useStyles = makeStyles(theme => ({
  avatar: {
    color: theme.palette.getContrastText(theme.palette.grey[500]),
    backgroundColor: theme.palette.grey[500],
    width: 105,
    height: 105,
    fontSize: 24,
    margin: 0,
  },
}));

type ProfileContextType = {
  submittingStatus: Status;
  setSubmittingStatus: React.Dispatch<SetStateAction<Status>>;
};

export const ProfileContext = React.createContext<ProfileContextType>({
  submittingStatus: Status.Idle,
  setSubmittingStatus: () => {},
});

export const MyProfile: React.FC = () => {
  const classes = useStyles();
  let { id } = useParams();

  const userProfileState = useSelector(UserProfileState);
  const [roleProficiencyRatingLevel, setRoleProficiencyRatingLevel] = useState(
    0,
  );
  const [profileData, setProfileData] = useState<IProfileData>();
  const [skillList, setSkillList] = useState<ISkillsInRole[]>();
  const [edit, setEdit] = useState(false);
  const [isSaveProfile, setSaveProfile] = useState(false);
  const [ownProfile, setOwnProfile] = useState(true);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<Boolean>();
  const [submittingStatus, setSubmittingStatus] = useState(Status.Idle);
  const { t } = useTranslation();
  const getUserData = async () => {
    if (id && userProfileState.id !== parseInt(id)) {
      const response = await getUserProfile(parseInt(id));
      if (response.success) {
        setProfileData(response.data);
        setRoleProficiencyRatingLevel(
          response.data.profile_role.proficiency_level_id ?? 0,
        );
        if (error) {
          setError(false);
        }
      }
      if (
        !response.success &&
        (response.message === 'record not found' ||
          response.message === 'Invalid param request')
      ) {
        setError(true);
      }
    }
  };

  const invokeUserProfile = async () => {
    if (userProfileState.id) {
      const response = await getUserProfile(userProfileState.id);
      if (response && response.success) {
        setProfileData(response.data);
        setRoleProficiencyRatingLevel(
          response.data.profile_role.proficiency_level_id ?? 0,
        );
        if (error) {
          setError(false);
        }
      }
    }
  };

  useEffect(() => {
    (async () => {
      if (id) {
        await getUserData();
        setOwnProfile(userProfileState.id === parseInt(id));
      } else {
        await Promise.all([invokeUserProfile()]);
      }
      await getSkillList();
      setLoading(false);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  const currentProfileId = userProfileState.id;

  const getSkillList = useCallback(async () => {
    const response = await getAllSkills();
    if (response.success) {
      setSkillList(response.data.list);
    }
  }, []);

  const editProfile = () => {
    setEdit(true);
  };
  const cancelEdit = () => {
    setEdit(false);
  };
  const handleSaveProfile = async () => {
    setSubmittingStatus(Status.Submitting);
    await delay(300);
    setSaveProfile(true);
  };
  const handleRefresh = () => {
    setSaveProfile(false);
    setEdit(false);
    invokeUserProfile();
  };
  const handleRefreshViewProfile = () => {
    if (id) {
      getUserData();
    } else {
      invokeUserProfile();
    }
  };
  const ProfileAvatar = () => {
    if (id) {
      if (parseInt(id) === 2) {
        return (
          <Avatar className={classes.avatar} src={Admin}>
            {/* {profileData?.name?.substring(0, 1).toUpperCase()} */}
          </Avatar>
        );
      } else {
        return (
          <Avatar className={classes.avatar} src={User100}>
            {profileData?.name?.substring(0, 1).toUpperCase()}
          </Avatar>
        );
      }
    } else {
      if (userProfileState.id === 2) {
        return (
          <Avatar className={classes.avatar} src={Admin}>
            {profileData?.name?.substring(0, 1).toUpperCase()}
          </Avatar>
        );
      } else {
        return (
          <Avatar className={classes.avatar} src={User100}>
            {profileData?.name?.substring(0, 1).toUpperCase()}
          </Avatar>
        );
      }
    }
  };
  const RenderActionButton = () => {
    if (edit) {
      return (
        <Box>
          <Box mr={2} display="inline-block">
            <Button variant="outlined" onClick={cancelEdit}>
              Cancel
            </Button>
          </Box>
          <Button
            variant="contained"
            color="primary"
            onClick={handleSaveProfile}
            disabled={submittingStatus === Status.Submitting}
          >
            Save Profile
          </Button>
        </Box>
      );
    } else if (ownProfile) {
      return (
        <Button variant="outlined" onClick={editProfile}>
          Edit Profile
        </Button>
      );
    }
    return <></>;
  };

  if (error) {
    throw new Error("User doesn't exist");
    // return <Box>User doesn't exist</Box>;
  }
  if (!loading) {
    return (
      <ProfileContext.Provider
        value={{ submittingStatus, setSubmittingStatus }}
      >
        <Box mt={2}>
          <Box mb={3}>
            <Typography variant="h1">{t('profile')}</Typography>
          </Box>
          <Grid item container spacing={2}>
            <Grid item container justify="space-between" alignItems="flex-end">
              <Grid item>
                <Grid container>
                  <Box marginRight={4}>
                    <ProfileAvatar />
                  </Box>
                  <Box>
                    <Typography variant="h2" gutterBottom>
                      {profileData?.name}
                    </Typography>
                    <Typography variant="body2" component="p">
                      {'Business Unit: '}
                      {profileData?.business_unit?.business_unit_name || 'None'}
                    </Typography>
                    <Typography variant="body2" component="p">
                      {'Resource Team: '}
                      {profileData?.resource_team?.resource_team_name || 'None'}
                    </Typography>
                    <Typography variant="body2" component="div">
                      {getRoleLabels(profileData?.casbin_role ?? []).map(
                        (item, index) => (
                          <Chip key={index} label={item} size="small" />
                        ),
                      )}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
              <Grid item>
                <RenderActionButton />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              {edit ? (
                <EditProfile
                  dataFromProfile={profileData}
                  userProfileState={userProfileState}
                  skillList={skillList}
                  cancelEdit={cancelEdit}
                  isSaveProfile={isSaveProfile}
                  handleRefresh={handleRefresh}
                  handleSaveProfile={handleSaveProfile}
                />
              ) : (
                <ViewProfile
                  profileData={profileData}
                  roleProficiencyRatingLevel={roleProficiencyRatingLevel}
                  currentProfileId={currentProfileId}
                  ownProfile={ownProfile}
                  skillList={skillList}
                  editProfile={editProfile}
                  handleRefreshViewProfile={handleRefreshViewProfile}
                  paramId={id}
                />
              )}
            </Grid>
            <Grid item container justify="flex-end">
              <RenderActionButton />
            </Grid>
          </Grid>
        </Box>
      </ProfileContext.Provider>
    );
  }
  return <></>;
};

export default MyProfile;
