import React, { useState, useEffect, useContext } from 'react';
import { useDispatch } from 'react-redux';
import {
  Grid,
  Divider,
  Select,
  MenuItem,
  FormControl,
  TextField,
  Box,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { makeStyles } from '@material-ui/core/styles';
import { MixTitle } from '../../../components';
import { Proficiencies, Skills, ProficiencyLevelDescription } from './index';
import {
  ISkillsInRole,
  IListRole,
  getLocations,
  UserLocation,
  UpdateProfileType,
  IProfileData,
  IProficiencyProps,
  getRoleProfile,
  updateUserProfile,
  ISkills,
} from '../../../services/profile.services';
import { RequestNewUserProfile } from '../../../store/user/actions';
import { StyledRating } from '../shared';
import { ProfileContext } from '..';
import { Status } from '../../../constants';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: `${theme.palette.grey[500]}`,
    width: `calc(100% + ${theme.spacing(3) * 2}px)`,
    marginLeft: -theme.spacing(3),
  },
  iconMinimize: {
    borderRadius: 15,
    backgroundColor: '#E1E1E1',
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
}));

interface UserProfile {
  full_name: string;
  profile_name: string;
  email: string;
  id: number;
  user_name: string;
}
interface IEditProfileProps {
  dataFromProfile?: IProfileData;
  userProfileState: UserProfile;
  skillList?: ISkillsInRole[];
  cancelEdit: () => void;
  isSaveProfile: boolean;
  handleRefresh: () => void;
  handleSaveProfile: () => void;
}

interface ProfileStateType {
  chosenLocation: string;
  userLocation: UserLocation[];
}

export const EditProfile: React.FC<IEditProfileProps> = ({
  dataFromProfile,
  userProfileState,
  skillList,
  cancelEdit,
  isSaveProfile,
  handleRefresh,
  handleSaveProfile,
}) => {
  const initProfileState: ProfileStateType = {
    chosenLocation: dataFromProfile?.location.name ?? '',
    userLocation: [],
  };
  const classes = useStyles();
  const { t } = useTranslation();
  const { setSubmittingStatus } = useContext(ProfileContext);
  const dispatch = useDispatch();
  const [state, setState] = useState(initProfileState);

  const [skills, setSkills] = useState<ISkills[] | undefined>(
    dataFromProfile?.skills,
  );
  const [profileRole, setProfileRole] = useState<IListRole[]>();
  const [roleProficiencyRatingLevel, setRoleProficiencyRatingLevel] = useState(
    dataFromProfile?.profile_role?.proficiency_level_id
      ? dataFromProfile?.profile_role?.proficiency_level_id
      : 1,
  );
  const [careerMission, setCareerMission] = useState(
    dataFromProfile?.career_mission,
  );
  const [chosenRole, setChosenRole] = useState<string>(
    dataFromProfile?.profile_role?.profile_role_name ?? '',
  );

  useEffect(() => {
    getInitData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (skillList && skillList.length > 0) {
      const updateSkills: typeof skills = skillList.map(item => {
        const includedInSkillList =
          skills &&
          skills.filter(skillUser => skillUser.skill_name === item.name);
        if (includedInSkillList && includedInSkillList.length) {
          return {
            skill_id: item.id,
            skill_name: item.name,
            proficiency_level_id: includedInSkillList[0].proficiency_level_id,
            favorites: includedInSkillList[0].favorites,
            isChecked: true,
            type: item.group_skill?.title,
          };
        }
        return {
          skill_id: item.id,
          skill_name: item.name,
          proficiency_level_id: 0,
          favorites: null,
          isChecked: false,
          type: item.group_skill?.title,
        };
      });
      setSkills(updateSkills);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [skillList]);

  const getInitData = async () => {
    try {
      await Promise.all([invokeGetRole(), invokeGetLocation()]);
    } catch (err) {
      console.log(err);
    }
  };
  const invokeGetRole = async () => {
    const response = await getRoleProfile();
    if (response.success) {
      setProfileRole(response.data.list);
    }
  };

  const invokeGetLocation = async () => {
    const response = await getLocations();
    if (response.success) {
      setState({ ...state, userLocation: response.data.list });
    }
  };
  const handleUpdateProfileUser = async (awards: IProficiencyProps) => {
    const profileData: UpdateProfileType = {
      resource_team_id: dataFromProfile?.resource_team.resource_team_id,
      location_id: state.userLocation.find(
        location => location.name === state.chosenLocation,
      )?.id,
      profile_role: {
        proficiency_level_id: roleProficiencyRatingLevel,
        profile_role_id: profileRole?.find(item => item?.name === chosenRole)
          ?.id,
      },
      career_mission: careerMission,
      skills: skills
        ?.filter(item => item.isChecked)
        .map(item => ({
          skill_id: item.skill_id,
          proficiency_level_id: item.proficiency_level_id,
        })),
      accomplishments: awards.key_accomplishments
        ?.map(item => ({
          id: item?.id,
          name: item?.name,
        }))
        ?.filter(item => item?.name),
      rewards: awards.awards
        ?.map(item => ({
          id: item?.id,
          name: item?.name,
        }))
        ?.filter(item => item?.name),
      publications: awards.publications
        ?.map(item => ({
          id: item?.id,
          name: item?.name,
        }))
        ?.filter(item => item?.name),
      certifications: awards.professional_certifications
        ?.map(item => ({
          id: item?.id,
          name: item?.name,
          date: item?.date,
        }))
        ?.filter(item => item?.name),
      knowledgebase_assets: awards.knowledge_assets
        ?.map(item => ({
          id: item?.id,
          name: item?.name,
          date: item?.date,
        }))
        ?.filter(item => item?.name),
    };

    const response = await updateUserProfile(userProfileState.id, profileData);
    if (response.success) {
      setSubmittingStatus(Status.Idle);
      dispatch(RequestNewUserProfile());
      handleRefresh();
    }
  };
  const handleGetAwardsData = (awards: IProficiencyProps) => {
    handleUpdateProfileUser(awards);
  };
  const handleChange = (
    event: React.ChangeEvent<{ name?: string; value: any }>,
  ) => {
    switch (event.target.name) {
      case 'role_profile': {
        setChosenRole(event.target.value);
        break;
      }
      case 'location': {
        setState({ ...state, chosenLocation: event.target.value });
        break;
      }
      default:
        return;
    }
  };
  const changeDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCareerMission(event.target.value);
  };

  //rating event handler
  const handleChangeRating = (
    event: React.ChangeEvent<{}>,
    value: number | null,
  ) => {
    setRoleProficiencyRatingLevel(value ?? 0);
  };
  const handleChangeSkillsRating = (
    event: React.ChangeEvent<any>,
    value: number | null,
  ) => {
    const id = parseInt(event.currentTarget?.name.split('_')[1]);
    if (skills) {
      const updatedSkills = skills.map(item => {
        if (item.skill_id !== id) return item;
        return { ...item, proficiency_level_id: value ?? 0 };
      });
      setSkills(updatedSkills);
    }
  };

  const handleModifyCheckBoxSkill = (skill_id: number) => {
    const modifySkills = skills?.map(item => {
      if (item.skill_id === skill_id) {
        item.isChecked = !item.isChecked;
        if (!item.proficiency_level_id) {
          item.proficiency_level_id = 1;
        }
        return item;
      }
      return item;
    });
    setSkills(modifySkills);
  };
  return (
    <Grid>
      <Box mt={2} mb={4}>
        <Divider variant="fullWidth" className={classes.divider} />
      </Box>
      <Grid container>
        <MixTitle isHelper={true} isHeader={true} title="About" />
      </Grid>
      <Box mt={3}>
        <Grid container>
          <MixTitle isHelper={true} title="Location" isRequired={true} />
          <Grid container>
            <Grid item xs={3}>
              <Select
                id="location"
                name="location"
                value={
                  state.chosenLocation && state.userLocation.length > 0
                    ? state.chosenLocation
                    : ''
                }
                fullWidth
                onChange={handleChange}
                variant="outlined"
                IconComponent={ExpandMoreIcon}
              >
                {state.userLocation &&
                  state.userLocation.length > 0 &&
                  state.userLocation?.map((item, index) => (
                    <MenuItem key={index} value={item?.name}>
                      {item?.name}
                    </MenuItem>
                  ))}
              </Select>
            </Grid>
          </Grid>
        </Grid>
      </Box>
      <Box mt={5}>
        <Grid container justify="space-between">
          <Grid item xs={3}>
            <MixTitle
              isHelper
              title="Current role"
              isRequired
              helperText={t('myStuff.helpText.current_role')}
            />
          </Grid>
          <Grid item xs={9}>
            <Grid container justify="space-between">
              <Grid item xs={2} />
              <Grid item xs={10}>
                <MixTitle
                  title="Role Proficiency Level"
                  isHelper={true}
                  helperText={t('myStuff.helpText.proficiency')}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
      <Grid container justify="space-between">
        <Grid item xs={3}>
          <Box width="100%" paddingTop={1}>
            <Select
              id="role_profile"
              name="role_profile"
              value={chosenRole && profileRole ? chosenRole : ''}
              fullWidth
              onChange={handleChange}
              variant="outlined"
              IconComponent={ExpandMoreIcon}
            >
              {profileRole &&
                profileRole.length > 0 &&
                profileRole?.map((item, index) => (
                  <MenuItem key={index} value={item?.name}>
                    {item?.name}
                  </MenuItem>
                ))}
            </Select>
          </Box>
        </Grid>
        <Grid item xs={9}>
          <Box width="100%" height="100%" display="flex" alignItems="center">
            <Grid container alignItems="center">
              <Grid item xs={2} />
              <Grid item xs={10}>
                <StyledRating
                  name="roleProficiencyLevel"
                  value={roleProficiencyRatingLevel}
                  max={4}
                  precision={1}
                  onChange={handleChangeRating}
                  icon={<div className={classes.iconMinimize} />}
                />
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
      <Grid container>
        <Box width="100%" mt={3}>
          <MixTitle
            isHelper
            title="Career Mission / Objectives"
            isRequired
            helperText={t('myStuff.helpText.career_mission')}
          />
        </Box>
      </Grid>
      <Grid container>
        <Grid item xs={5}>
          <FormControl fullWidth>
            <TextField
              rows={8}
              variant="outlined"
              multiline
              inputProps={{
                maxLength: 300,
              }}
              onChange={changeDescription}
              value={careerMission}
            />
          </FormControl>
        </Grid>
      </Grid>
      <Grid container>
        <Box width="100%" mt={6}>
          <MixTitle
            isHeader={true}
            isHelper={true}
            title="Skills & Proficiency"
            helperText={t('myStuff.helpText.skills_and_proficiency')}
          />
          <Grid container justify="space-between">
            <Box width="100%" mt={3}>
              <Grid item xs={6}>
                <Grid container justify="space-between">
                  <Grid item xs={6}>
                    <MixTitle
                      isHelper={true}
                      title="Skills"
                      helperText={t('myStuff.helpText.skill')}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <MixTitle
                      isHelper={true}
                      title="Proficiency Level"
                      helperText={<ProficiencyLevelDescription />}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Skills
            skills={skills}
            edit={true}
            handleChangeSkillsRating={handleChangeSkillsRating}
            handleCheckBox={handleModifyCheckBoxSkill}
          />
        </Box>
      </Grid>
      <Grid container direction="column">
        <Box width="100%" mt={6}>
          <Proficiencies
            edit={true}
            data={dataFromProfile}
            isSaveProfile={isSaveProfile}
            handleSubmitProficiency={handleGetAwardsData}
          />
        </Box>
      </Grid>
      <Box mt={7} mb={2}>
        <Divider variant="fullWidth" className={classes.divider} />
      </Box>
    </Grid>
  );
};
