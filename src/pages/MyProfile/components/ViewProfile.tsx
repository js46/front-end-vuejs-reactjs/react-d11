import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Grid,
  Typography,
  Divider,
  FormControl,
  TextField,
  Button,
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { MixTitle } from '../../../components';
import { Proficiencies, Recommendations, Skills } from './index';
import {
  addFavorite,
  IProfileData,
  removeFavorite,
  Favorite,
  addRecomendationComment,
  ISkillsInRole,
  ISkills,
} from '../../../services/profile.services';

import { StyledRatingView } from '../shared';
import { useDisableMultipleClick } from '../../../hooks';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(5),
  },
  divider: {
    backgroundColor: `${theme.palette.grey[500]}`,
    width: `calc(100% + ${theme.spacing(3) * 2}px)`,
    marginLeft: -theme.spacing(3),
  },
  iconMinimizeBlue: {
    borderRadius: 15,
    backgroundColor: '#E1E1E1',
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
  caption: {
    fontWeight: 700,
    fontSize: 13,
  },
  description: {
    fontSize: 13,
  },
}));

export const ProficiencyLevelDescription: React.FC = () => {
  const classes = useStyles();
  return (
    <Box>
      <Box mb={1}>
        <Typography component="span" className={classes.caption}>
          L1 BASIC:
        </Typography>
        <Typography component="span" className={classes.description}>
          {'  '}
          1-2 years experience. Can perform basic elements of a given skill
        </Typography>
      </Box>
      <Box mb={1}>
        <Typography component="span" className={classes.caption}>
          L2 SENIOR:
        </Typography>
        <Typography component="span" className={classes.description}>
          {'  '}
          3-4 years experience. Can perform elements of the skill at senior
          level and manage a small team
        </Typography>
      </Box>
      <Box mb={1}>
        <Typography component="span" className={classes.caption}>
          L3 EXPERT:
        </Typography>
        <Typography component="span" className={classes.description}>
          {'  '}
          5-6 years experience
        </Typography>
      </Box>
      <Box mb={1}>
        <Typography component="span" className={classes.caption}>
          L4 MASTER:
        </Typography>
        <Typography component="span" className={classes.description}>
          {'  '}
          or Coach 7+ years experience. Can deliver master classes and/or coach
          others in a particular skill
        </Typography>
      </Box>
    </Box>
  );
};

interface IViewProfileProps {
  profileData?: IProfileData;
  roleProficiencyRatingLevel: number;
  currentProfileId?: number;
  ownProfile?: boolean;
  skillList?: ISkillsInRole[];
  editProfile: () => void;
  handleRefreshViewProfile: () => void;
  paramId?: string;
}

export const ViewProfile: React.FC<IViewProfileProps> = ({
  profileData,
  roleProficiencyRatingLevel,
  ownProfile,
  skillList,
  handleRefreshViewProfile,
  paramId,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const userProfileState = useSelector(UserProfileState);
  const [recommendationComment, setRecommendationComment] = useState('');
  const [reloadRecommendations, setReloadRecommendations] = useState(1);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const handleChangeSkillEndorsements = async (
    skill_id?: number,
    favorite?: Favorite[],
  ) => {
    //figure out whether to Like or Inlike action here
    let isLikable: boolean = true;
    let idUnlikeSkill: number = 0;
    if (
      favorite &&
      favorite?.some(item => item.user_id === userProfileState.id)
    ) {
      isLikable = false;
      idUnlikeSkill = favorite?.filter(
        item => item.user_id === userProfileState.id,
      )[0]?.id;
    }

    if (isLikable) {
      handleLikeEndorsement(skill_id);
    } else {
      handleUnlikeEndorsement(idUnlikeSkill);
    }
  };

  const handleLikeEndorsement = async (skill_id?: number) => {
    const favoriteData = {
      entity_id: skill_id ?? 0,
      entity_type: 'skill',
    };
    const response = await addFavorite(favoriteData);
    if (response.success) {
      handleRefreshViewProfile();
    }
  };

  const handleUnlikeEndorsement = async (idUnlikeSkill: number) => {
    const response = await removeFavorite(idUnlikeSkill);
    if (response.success) {
      handleRefreshViewProfile();
    }
  };

  const handleLikeAward = (
    id: number,
    title: string,
    favorite?: Favorite[],
  ) => {
    switch (title) {
      case 'Key Accomplishments': {
        handleRequestFavoriteAward(id, 'accomplishment', favorite);
        break;
      }
      case 'Awards': {
        handleRequestFavoriteAward(id, 'reward', favorite);
        break;
      }
      case 'Publications': {
        handleRequestFavoriteAward(id, 'publication', favorite);
        break;
      }
      case 'Professional Certifications': {
        handleRequestFavoriteAward(id, 'certification', favorite);
        break;
      }
      case 'Knowledgebase Assets Created': {
        handleRequestFavoriteAward(id, 'knowledgebase_asset', favorite);
        break;
      }
    }
  };
  const handleRequestFavoriteAward = async (
    id: number,
    typeOfAward: string,
    favorite?: Favorite[],
  ) => {
    //figure out whether to Like or Unlike action here
    let isLikable = true;
    let idUnlikeSkill = 0;
    if (
      favorite &&
      favorite?.some(item => item.user_id === userProfileState.id)
    ) {
      isLikable = false;
      idUnlikeSkill = favorite?.filter(
        item => item.user_id === userProfileState.id,
      )[0]?.id;
    }

    if (isLikable) {
      const favoriteData = {
        entity_id: id,
        entity_type: typeOfAward,
      };
      const response = await addFavorite(favoriteData);
      if (response.success) {
        handleRefreshViewProfile();
      }
    } else {
      const response = await removeFavorite(idUnlikeSkill);
      if (response.success) {
        handleRefreshViewProfile();
      }
    }
  };

  const handleChange = (event: React.ChangeEvent<any>) => {
    setRecommendationComment(event.target.value);
  };
  const handleSubmitComment = async () => {
    if (isSubmitting) return;
    await debounceFn(300);
    const bodyRequest = {
      entity_id: paramId ? parseInt(paramId) : 0,
      entity_type: 'recommendation',
      comment_type: 'default',
      comment_body: recommendationComment,
      comment_parent_id: 0,
    };
    const response = await addRecomendationComment(bodyRequest);
    if (response.success) {
      setRecommendationComment('');
      setTimeout(() => {
        setReloadRecommendations(reloadRecommendations + 1);
      }, 800);
    }
    endRequest();
  };

  const mySkills: ISkills[] =
    profileData?.skills?.map(item => {
      let skillData = skillList?.find(
        listItem => listItem.id === item.skill_id,
      );
      return {
        ...item,
        type: skillData?.group_skill?.title,
      };
    }) ?? [];

  return (
    <Grid>
      <Box mt={2} mb={4}>
        <Divider variant="fullWidth" className={classes.divider} />
      </Box>
      <Grid container>
        <MixTitle isHeader={true} title="About" />
      </Grid>
      <Grid container>
        <Box mt={3} width="100%">
          <MixTitle
            isHelper={true}
            title="Location"
            helperText={t('myStuff.helpText.location')}
          />
          <Typography variant="body2">{profileData?.location?.name}</Typography>
        </Box>
      </Grid>
      <Grid container justify="space-between">
        <Grid item xs={9}>
          <Box mt={3.5}>
            <Grid container justify="space-between">
              <MixTitle
                isHelper={true}
                title="Current role"
                helperText={t('myStuff.helpText.current_role')}
              />
              <Grid item xs={7}>
                <MixTitle
                  isHelper={true}
                  title="Role Proficiency Level"
                  helperText={<ProficiencyLevelDescription />}
                />
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
      <Grid container justify="space-between">
        <Box pt={1} width="100%">
          <Grid item xs={9}>
            <Grid container justify="space-between" alignItems="center">
              <Grid item xs={5} sm={5}>
                <Typography variant="body2">
                  {profileData?.profile_role?.profile_role_name}
                </Typography>
              </Grid>
              <Grid item xs={7} sm={7}>
                <Grid container alignItems="center">
                  {roleProficiencyRatingLevel > 0 && (
                    <StyledRatingView
                      name="roleProficiencyLevel"
                      value={roleProficiencyRatingLevel}
                      precision={1}
                      max={4}
                      icon={<div className={classes.iconMinimizeBlue} />}
                      readOnly
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Grid>
      <Box pt={3}>
        <Grid container>
          <MixTitle
            isHelper={true}
            title="Career Mission / Objectives"
            helperText={t('myStuff.helpText.career_mission')}
          />
        </Grid>
        <Typography variant="body2">{profileData?.career_mission}</Typography>
      </Box>

      <Box mt={6}>
        <Grid container>
          <MixTitle
            isHeader={true}
            isHelper={true}
            title="Skills & Proficiency"
            helperText={t('myStuff.helpText.skills_and_proficiency')}
          />
          <Box mt={3} width="100%">
            <Grid container justify="space-between">
              <Grid item xs={6}>
                <Grid container justify="space-between">
                  <Grid item xs={6}>
                    <MixTitle
                      isHelper={true}
                      title="Skills"
                      helperText={t('myStuff.helpText.skill')}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <MixTitle
                      isHelper={true}
                      helperText={<ProficiencyLevelDescription />}
                      title="Proficiency Level"
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={6}>
                <Box pl={2}>
                  <MixTitle
                    isHelper={true}
                    title="Endorsements"
                    helperText={t('myStuff.helpText.endorsements')}
                  />
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Skills
            skills={mySkills}
            ownProfile={ownProfile}
            handleChangeSkillEndorsements={handleChangeSkillEndorsements}
          />
        </Grid>
      </Box>

      <Box mt={6}>
        <Grid container direction="column">
          <Proficiencies
            data={profileData}
            ownProfile={ownProfile}
            handleLike={handleLikeAward}
          />
        </Grid>
      </Box>

      <Box mb={8} mt={6}>
        <Grid container>
          <MixTitle
            isHeader={true}
            isHelper={true}
            title="Recommendations"
            helperText={t('myStuff.helpText.recommendations')}
          />
          <Recommendations
            ownProfile={ownProfile}
            paramId={paramId}
            recall={reloadRecommendations}
          />
          {!ownProfile && (
            <React.Fragment>
              <Grid item xs={12}>
                <Grid container className={classes.root}>
                  <Grid item xs={8}>
                    <FormControl fullWidth>
                      <MixTitle
                        title="Provide A Recommendation"
                        isHelper={true}
                      />
                      <TextField
                        value={recommendationComment}
                        onChange={handleChange}
                        rows={8}
                        // labelWidth={100}
                        variant="outlined"
                        multiline
                        id="recommendation_comment"
                        name="recommendation_comment"
                      />
                    </FormControl>
                  </Grid>
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleSubmitComment}
                  disabled={isSubmitting || recommendationComment === ''}
                >
                  Submit
                </Button>
              </Box>
            </React.Fragment>
          )}
        </Grid>
      </Box>
      <Box mt={7} mb={2}>
        <Divider variant="fullWidth" className={classes.divider} />
      </Box>
    </Grid>
  );
};
