import React, { useRef, useEffect } from 'react';
import clsx from 'clsx';
import moment from 'moment';
import { useSelector } from 'react-redux';
import {
  Grid,
  Typography,
  Box,
  TextField,
  Button,
  InputAdornment,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MixTitle } from '../../../components';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import { Formik, FieldArray } from 'formik';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  IProfileData,
  Favorite,
  IAccomplishments,
  IPublication,
  ICertification,
  IKnowledgeBase,
  IAward,
  IProficiencyProps,
} from '../../../services/profile.services';
import { ProficiencyTitle } from '../types';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  likeNumber: {
    marginLeft: 10,
    color: `${theme.palette.grey[500]}`,
  },
  like: {
    color: `${theme.palette.grey[500]}`,
  },
  hoverable: {
    color: `${theme.palette.grey[500]}`,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  mt16: {
    marginTop: theme.spacing(2),
  },
  removeBtn: {
    marginLeft: 10,
    padding: '0 20px',
    textTransform: 'capitalize',
  },
  addButton: {
    padding: '0 20px',
  },
  pt5: {
    paddingTop: 5,
  },
}));

interface IRewardProps {
  id: number;
  name: string;
  date: Date;
  favorites: Favorite[] | null;
}

interface IProficiency {
  data?: IProfileData;
  edit?: boolean;
  ownProfile?: boolean;
  isSaveProfile?: boolean;
  handleLike?: (id: number, title: string, favorite?: Favorite[]) => void;
  handleSubmitProficiency?: (values: IProficiencyProps) => void;
}

const formatFavorites = (fav: Favorite[] | null) => {
  if (!fav) {
    return 0;
  } else if (fav && fav.length > 0) {
    return fav.length;
  }
};

export const Proficiencies: React.FC<IProficiency> = ({
  edit = false,
  handleLike,
  ownProfile,
  isSaveProfile,
  data,
  handleSubmitProficiency,
}) => {
  const initValues = {
    key_accomplishments: data?.accomplishments ?? [],
    awards: data?.rewards ?? [],
    publications: data?.publications ?? [],
    professional_certifications: data?.certifications ?? [],
    knowledge_assets: data?.knowledgebase_assets ?? [],
  };
  const classes = useStyles();
  const { t } = useTranslation();
  const userProfileState = useSelector(UserProfileState);
  const submitButtonRef = useRef<any>();
  useEffect(() => {
    if (isSaveProfile && submitButtonRef && submitButtonRef.current) {
      submitButtonRef.current.click();
    }
  }, [isSaveProfile]);
  const _renderSingleField = (
    value: IAccomplishments[] | IAward[] | IPublication[],
    name: string,
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
  ) => {
    return (
      <FieldArray
        name={name}
        render={arrayHelpers => {
          return (
            <>
              <Box>
                {value &&
                  value.map((item, index) => (
                    <Box mb={2} key={index}>
                      <Grid container>
                        <Grid item xs={8}>
                          <TextField
                            value={item.name}
                            variant="outlined"
                            fullWidth
                            name={`${name}.${index}.name`}
                            onChange={handleChange}
                            InputLabelProps={{
                              shrink: true,
                            }}
                            margin="normal"
                          />
                        </Grid>
                        {index > 0 && (
                          <Grid item xs={1} className={classes.pt5}>
                            <Button
                              variant="outlined"
                              className={classes.removeBtn}
                              aria-label="close"
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              Remove
                            </Button>
                          </Grid>
                        )}
                      </Grid>
                    </Box>
                  ))}
              </Box>
              <Grid container>
                <Grid item xs={8}>
                  <Box textAlign="center">
                    <Button
                      variant="outlined"
                      className={classes.addButton}
                      onClick={() => {
                        arrayHelpers.push({
                          name: '',
                          date: new Date(),
                        });
                      }}
                      disabled={value?.some(item => item.name === '')}
                    >
                      Add
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </>
          );
        }}
      />
    );
  };

  const _renderMultipleField = (
    value: IKnowledgeBase[] | ICertification[],
    name: string,
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    setFieldValue: (field: string, value: Date | null) => void,
  ) => {
    return (
      <FieldArray
        name={name}
        render={arrayHelpers => {
          return (
            <>
              <Box>
                {value &&
                  value.map((item, index) => (
                    <Box mb={2} key={index}>
                      <Grid container>
                        <Grid item xs={6}>
                          <TextField
                            value={item.name}
                            variant="outlined"
                            fullWidth
                            name={`${name}.${index}.name`}
                            onChange={handleChange}
                            InputLabelProps={{
                              shrink: true,
                            }}
                            margin="normal"
                          />
                        </Grid>

                        <Grid item xs={2}>
                          <Box pl="30px">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                              <DatePicker
                                disableToolbar
                                allowKeyboardControl
                                inputVariant="outlined"
                                variant="inline"
                                margin="normal"
                                name={`${name}.${index}.date`}
                                // id="professional_certification_date"
                                format="d MMMM yyyy"
                                onChange={dateValue => {
                                  // handleChangeDate(dateValue, name, index);
                                  setFieldValue(
                                    `${name}.${index}.date`,
                                    dateValue,
                                  );
                                }}
                                value={
                                  item.date ? new Date(item.date) : new Date()
                                }
                                animateYearScrolling={false}
                                fullWidth
                                InputProps={{
                                  endAdornment: (
                                    <InputAdornment position="end">
                                      <ExpandMoreIcon />
                                    </InputAdornment>
                                  ),
                                }}
                              />
                            </MuiPickersUtilsProvider>
                          </Box>
                        </Grid>

                        {index > 0 && (
                          <Grid item xs={1} className={classes.pt5}>
                            <Button
                              variant="outlined"
                              className={classes.removeBtn}
                              aria-label="close"
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              Remove
                            </Button>
                          </Grid>
                        )}
                      </Grid>
                    </Box>
                  ))}
              </Box>
              <Grid container>
                <Grid item xs={8}>
                  <Box textAlign="center">
                    <Button
                      variant="outlined"
                      className={classes.addButton}
                      onClick={() => {
                        arrayHelpers.push({
                          name: '',
                          date: new Date(),
                        });
                      }}
                      disabled={value?.some(item => item.name === '')}
                    >
                      Add
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </>
          );
        }}
      />
    );
  };

  if (edit) {
    return (
      <Formik
        initialValues={initValues}
        // validationSchema={}
        onSubmit={values => {
          if (values && handleSubmitProficiency) {
            handleSubmitProficiency(values);
          }
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          errors,
          touched,
          isSubmitting,
          isValid,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box>
              <Box mb={2.5}>
                <Grid container>
                  <Grid item xs={12}>
                    <MixTitle
                      isHelper={true}
                      title="Key Accomplishments"
                      helperText={t('myStuff.helpText.key_accomplishments')}
                    />
                    {values.key_accomplishments &&
                      _renderSingleField(
                        values.key_accomplishments,
                        'key_accomplishments',
                        handleChange,
                      )}
                  </Grid>
                </Grid>
              </Box>
              <Box mb={2.5}>
                <Grid container>
                  <Grid item xs={12}>
                    <MixTitle
                      isHelper={true}
                      title="Awards"
                      helperText={t('myStuff.helpText.awards')}
                    />
                    {values.awards &&
                      _renderSingleField(values.awards, 'awards', handleChange)}
                  </Grid>
                </Grid>
              </Box>
              <Box mb={2.5}>
                <Grid container>
                  <Grid item xs={12}>
                    <MixTitle
                      isHelper={true}
                      title="Publications"
                      helperText={t('myStuff.helpText.publications')}
                    />
                    {values.publications &&
                      _renderSingleField(
                        values.publications,
                        'publications',
                        handleChange,
                      )}
                  </Grid>
                </Grid>
              </Box>
              <Box mb={2.5}>
                <Grid container>
                  <Grid item xs={12}>
                    <MixTitle
                      isHelper={true}
                      title="Professional Certifications"
                      helperText={t(
                        'myStuff.helpText.professional_certifications',
                      )}
                    />
                    {values.professional_certifications &&
                      _renderMultipleField(
                        values.professional_certifications,
                        'professional_certifications',
                        handleChange,
                        setFieldValue,
                      )}
                  </Grid>
                </Grid>
              </Box>

              <Grid container>
                <Grid item xs={12}>
                  <MixTitle
                    isHelper={true}
                    title="Knowledgebase Assets Created"
                    helperText={t(
                      'myStuff.helpText.knowledge_base_asset_created',
                    )}
                  />
                  {values.knowledge_assets &&
                    _renderMultipleField(
                      values.knowledge_assets,
                      'knowledge_assets',
                      handleChange,
                      setFieldValue,
                    )}
                </Grid>
              </Grid>
              <Box display="none">
                <button type="submit" ref={submitButtonRef}></button>
              </Box>
            </Box>
          </form>
        )}
      </Formik>
    );
  }

  //for view profile
  const _renderRewards = (
    dataItem:
      | IAccomplishments
      | IRewardProps
      | IPublication
      | ICertification
      | IKnowledgeBase,
    index: number,
    i: number,
    title: string,
  ) => {
    return (
      <Grid
        container
        justify="space-between"
        alignItems="center"
        className={clsx({
          [classes.mt16]: i > 0,
        })}
      >
        <Grid item xs={6}>
          <Typography variant="body2">
            {title === 'Professional Certifications' ||
            title === 'Knowledgebase Assets Created' ? (
              <Grid container justify="space-between">
                <Grid item xs={7} sm={7} lg={6} xl={6}>
                  {dataItem?.name}
                </Grid>

                <Grid item xs={5} sm={5} lg={6} xl={6}>
                  {`Date Created: ${moment(dataItem?.date).format(
                    'MM/DD/YYYY',
                  )}`}
                </Grid>
              </Grid>
            ) : (
              dataItem?.name
            )}
          </Typography>
        </Grid>
        <Grid item xs={6} style={{ paddingLeft: '20px' }}>
          <Grid container alignItems="center">
            {!ownProfile &&
            dataItem &&
            dataItem?.favorites?.some(
              item => item.user_id === userProfileState.id,
            ) ? (
              <ThumbUpIcon
                onClick={() => {
                  if (handleLike && !ownProfile && dataItem?.favorites) {
                    handleLike(dataItem?.id, title, dataItem?.favorites);
                  }
                }}
                className={!ownProfile ? classes.hoverable : classes.like}
              />
            ) : (
              <ThumbUpAltOutlinedIcon
                onClick={() => {
                  if (handleLike && !ownProfile) {
                    handleLike(dataItem?.id, title);
                  }
                }}
                className={!ownProfile ? classes.hoverable : classes.like}
              />
            )}
            <span className={classes.likeNumber}>
              {formatFavorites(dataItem?.favorites)}
            </span>
          </Grid>
        </Grid>
      </Grid>
    );
  };

  return (
    <React.Fragment>
      {ProficiencyTitle.map((item, index) => (
        <Grid key={index}>
          <MixTitle isHelper={true} title={item.title} />
          <Box mb={2}>
            <Grid container justify="space-between">
              <Grid item xs={12}>
                {item.title === 'Key Accomplishments' &&
                  data &&
                  data.accomplishments &&
                  data.accomplishments?.map((dataItem, i) => (
                    <React.Fragment key={i}>
                      {_renderRewards(dataItem, index, i, item.title)}
                    </React.Fragment>
                  ))}
                {item.title === 'Awards' &&
                  data &&
                  data.rewards &&
                  data.rewards?.map((dataItem, i) => (
                    <React.Fragment key={i}>
                      {_renderRewards(dataItem, index, i, item.title)}
                    </React.Fragment>
                  ))}
                {item.title === 'Publications' &&
                  data &&
                  data.publications &&
                  data.publications?.map((dataItem, i) => (
                    <React.Fragment key={i}>
                      {_renderRewards(dataItem, index, i, item.title)}
                    </React.Fragment>
                  ))}
                {item.title === 'Professional Certifications' &&
                  data &&
                  data.certifications &&
                  data.certifications?.map((dataItem, i) => (
                    <React.Fragment key={i}>
                      {_renderRewards(dataItem, index, i, item.title)}
                    </React.Fragment>
                  ))}
                {item.title === 'Knowledgebase Assets Created' &&
                  data &&
                  data.knowledgebase_assets &&
                  data.knowledgebase_assets?.map((dataItem, i) => (
                    <React.Fragment key={i}>
                      {_renderRewards(dataItem, index, i, item.title)}
                    </React.Fragment>
                  ))}
              </Grid>
            </Grid>
          </Box>
        </Grid>
      ))}
    </React.Fragment>
  );
};
