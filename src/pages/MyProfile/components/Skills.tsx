import React from 'react';
import clsx from 'clsx';
import { useSelector } from 'react-redux';
import { Box, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
// import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Checkbox from '@material-ui/core/Checkbox';
import { StyledRating, StyledRatingView } from '../shared';
import {
  IProfileData,
  Favorite,
  ISkills,
} from '../../../services/profile.services';
import { groupBy } from 'lodash';
import { UserProfileState } from '../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  subContentSkill: {
    paddingTop: 20,
  },
  iconMinimize: {
    borderRadius: 15,
    backgroundColor: '#E1E1E1',
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
  iconMinimizeBlue: {
    borderRadius: 15,
    backgroundColor: '#E1E1E1',
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
  round: {
    border: `1.5px solid ${theme.palette.grey[500]}`,
    borderRadius: 12,
    color: `${theme.palette.grey[500]}`,
  },
  hoverable: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  number: {
    marginLeft: 7,
    color: `${theme.palette.grey[500]}`,
  },
  endorsementWrapper: {
    paddingLeft: 20,
  },
}));

interface ISkillProps {
  skills?: ISkills[];
  edit?: boolean;
  profileData?: IProfileData;
  ownProfile?: boolean;
  handleChangeSkillsRating?: (
    event: React.ChangeEvent<{}>,
    value: number | null,
  ) => void;
  handleChangeSkillEndorsements?: (
    skill_id?: number,
    favorite?: Favorite[],
  ) => void;
  handleCheckBox?: (skill_id: number) => void;
}
interface IFavorite {
  id: number;
  user_id: number;
}

const formatFavorites = (fav: null | IFavorite[]) => {
  if (!fav) {
    return 0;
  } else if (fav && fav.length > 0) {
    return fav.length;
  }
};

export const Skills: React.FC<ISkillProps> = ({
  skills,
  edit = false,
  ownProfile,
  profileData,
  handleChangeSkillsRating,
  handleChangeSkillEndorsements,
  handleCheckBox,
}) => {
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState);

  const handleClickEndorsement = (
    skill_id?: number,
    favorite?: Favorite[],
  ) => () => {
    if (handleChangeSkillEndorsements && !edit && !ownProfile) {
      handleChangeSkillEndorsements(skill_id, favorite);
    }
  };

  const handleModifyCheckboxSkill = (skill_id: number) => () => {
    if (handleCheckBox) {
      handleCheckBox(skill_id);
    }
  };

  if (!skills && !profileData) {
    return <></>;
  }

  // console.log('Skill list', skills);
  const skillGroups = groupBy(skills, item => item.type);
  // console.log('skillGroups', skillGroups);

  if (edit) {
    return (
      <React.Fragment>
        {Object.keys(skillGroups).map((group, i) => (
          <Box key={i} mt={2}>
            <Typography variant="h6">
              <i>{group}</i>
            </Typography>
            {skillGroups[group].map((skill, index) => (
              <Grid
                key={index}
                container
                justify="space-between"
                alignItems="center"
                className={classes.subContentSkill}
              >
                <Grid item xs={6}>
                  <Grid container justify="space-between" alignItems="center">
                    <Grid item xs={6}>
                      <Checkbox
                        onChange={handleModifyCheckboxSkill(skill?.skill_id)}
                        checked={skill?.isChecked ?? false}
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                        style={{ padding: '0', marginRight: '20px' }}
                      />
                      <Typography variant="body1">{`${index +
                        1}. ${skill?.skill_name ?? ''}`}</Typography>
                    </Grid>
                    <Grid item xs={6} sm={6}>
                      <Grid container alignItems="center">
                        {skill?.isChecked && (
                          <StyledRating
                            id={`skill__${skill.skill_id}`}
                            name={`${skill?.skill_name}_${skill.skill_id}`}
                            value={skill.proficiency_level_id}
                            onChange={handleChangeSkillsRating}
                            precision={1}
                            max={4}
                            icon={<div className={classes.iconMinimize} />}
                          />
                        )}
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            ))}
          </Box>
        ))}
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      {Object.keys(skillGroups).map((group, index) => (
        <Grid key={index} container>
          <Box mt={2}>
            <Typography variant="h6">
              <i>{group}</i>
            </Typography>
          </Box>
          {skillGroups[group].map((skill, index) => (
            <Grid
              key={index}
              container
              justify="space-between"
              alignItems="center"
              className={classes.subContentSkill}
            >
              <Grid item xs={6}>
                <Grid container justify="space-between" alignItems="center">
                  <Grid item xs={6}>
                    <Typography variant="body1">{skill.skill_name}</Typography>
                  </Grid>
                  <Grid item xs={6} sm={6}>
                    <Grid container alignItems="center">
                      <StyledRatingView
                        name={`${skill.skill_name}_${index}`}
                        value={skill.proficiency_level_id}
                        precision={1}
                        max={4}
                        icon={<div className={classes.iconMinimizeBlue} />}
                        readOnly
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={6} className={classes.endorsementWrapper}>
                <Grid container alignItems="center">
                  {!ownProfile &&
                  skill &&
                  skill.favorites?.some(
                    item => item.user_id === userProfileState.id,
                  ) ? (
                    <StarIcon
                      onClick={handleClickEndorsement(
                        skill?.id,
                        skill?.favorites,
                      )}
                      className={
                        !ownProfile
                          ? clsx(classes.round, classes.hoverable)
                          : classes.round
                      }
                    />
                  ) : (
                    <StarBorderIcon
                      onClick={handleClickEndorsement(skill?.id)}
                      className={
                        !ownProfile
                          ? clsx(classes.round, classes.hoverable)
                          : classes.round
                      }
                    />
                  )}
                  <span className={classes.number}>
                    {formatFavorites(skill?.favorites)}
                  </span>
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      ))}
    </React.Fragment>
  );
};
