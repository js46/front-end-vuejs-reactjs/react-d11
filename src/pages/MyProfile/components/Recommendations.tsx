import React from 'react';
import moment from 'moment';
import { Grid, Typography, Avatar, Box } from '@material-ui/core';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../store/config/selector';
import { IRecommendations } from '../types';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  subRecommendations: {
    paddingTop: 27,
  },
  contentComment: {
    paddingLeft: 17,
  },
  description: {
    paddingTop: 17,
  },
}));

interface RecommendationsProfile {
  ownProfile?: boolean;
  recommendations?: IRecommendations[];
  paramId?: string;
  recall?: number;
}

export const Recommendations: React.FC<RecommendationsProfile> = ({
  ownProfile,
  recommendations,
  paramId,
  recall,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState);
  const { t } = useTranslation();
  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                entity_id: paramId ? paramId : userProfileState?.id,
              },
            },
            {
              term: {
                entity_type: 'recommendation',
              },
            },
          ],
          should: [
            {
              term: {
                stupid: recall,
              },
            },
          ],
        },
      },
      sort: [
        {
          created_at: {
            order: 'ASC',
          },
        },
      ],
    };
  };

  const getRecommendation = () => {
    return (
      <ReactiveBase
        app={ES_INDICES.comment_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <ReactiveList
          componentId="recommendation_comment"
          dataField="comment"
          loader={<></>}
          showResultStats={false}
          renderNoResults={() => (
            <Typography component="p">{t('no_comments_were_found')}</Typography>
          )}
          defaultQuery={customQuery}
        >
          {({ data }) => {
            return (
              <Box>
                {data &&
                  data.length > 0 &&
                  data.map((item: any, index: number) => (
                    <Grid
                      key={index}
                      container
                      justify="space-between"
                      className={classes.subRecommendations}
                    >
                      <Grid item lg={6} xl={6} xs={8} sm={8}>
                        <Grid container>
                          <Grid item lg={1} xl={1} sm={1} xs={1}>
                            <Avatar src="/broken-image.jpg" />
                          </Grid>
                          <Grid
                            item
                            lg={11}
                            xl={11}
                            sm={11}
                            xs={11}
                            className={classes.contentComment}
                          >
                            <Box display="flex" justifyContent="space-between">
                              <Typography variant="h5">
                                {item?.created_by?.name}
                              </Typography>
                              <Typography variant="body2">
                                {item?.last_updated_at
                                  ? moment(item?.last_updated_at).format(
                                      'MM/DD/YYYY h:mma',
                                    )
                                  : ''}
                              </Typography>
                            </Box>
                            <Grid
                              className={classes.description}
                              item
                              lg={12}
                              xl={12}
                            >
                              <Typography variant="body2">
                                {item?.comment_body}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}
              </Box>
            );
          }}
        </ReactiveList>
      </ReactiveBase>
    );
  };
  return <React.Fragment>{getRecommendation()}</React.Fragment>;
};
