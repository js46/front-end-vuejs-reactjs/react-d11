import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

export const Dataset: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Box mt={2}>
      <Typography component="p" variant="body1">
        {t('page_under_construction')}
      </Typography>
    </Box>
  );
};

export default Dataset;
