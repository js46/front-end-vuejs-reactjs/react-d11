import {
  ResponsePlayBookT,
  IChildrenListItem,
} from '../../services/playbook.services';

export type IPlayItemTree = {
  children?: Array<IPlayItemTree>;
} & ResponsePlayBookT;
export type IChildListTree = {
  children?: Array<IChildListTree>;
} & IChildrenListItem;
export enum PlaybookPermission {
  PlaybookRead = 'role:analyse_playbook_read',
  PlaybookCreate = 'role:analyse_playbook_create',
  PlaybookUpdate = 'role:analyse_playbook_update',
  PlaybookDelete = 'role:analyse_playbook_delete',
  PlaybookItemRead = 'role:analyse_playbook_item_read',
  PlaybookItemCreate = 'role:analyse_playbook_item_create',
  PlaybookItemUpdate = 'role:analyse_playbook_item_update',
  PlaybookItemWork = 'role:analyse_playbook_item_work',
  PlaybookItemDelete = 'role:analyse_playbook_item_delete',
  PlaybookItemAssign = 'role:analyse_playbook_item_assign',
  PlaybookItemUnassign = 'role:analyse_playbook_item_unassign',
  PlaybookItemDone = 'role:analyse_playbook_item_done',
  PlaybookItemUndone = 'role:analyse_playbook_item_undone',
  PlaybookItemNoteRead = 'role:analyse_playbook_item_note_read',
  PlaybookItemNoteCreate = 'role:analyse_playbook_item_note_create',
  PlaybookItemNoteUpdate = 'role:analyse_playbook_item_note_update',
  PlaybookItemNoteDelete = 'role:analyse_playbook_item_note_delete',
  PlaybookItemAttachmentRead = 'role:analyse_playbook_item_attachment_read',
  PlaybookItemAttachmentCreate = 'role:analyse_playbook_item_attachment_create',
  PlaybookItemAttachmentUpdate = 'role:analyse_playbook_item_attachment_update',
  PlaybookItemAttachmentDelete = 'role:analyse_playbook_item_attachment_delete',
  PlaybookItemListRead = 'role:analyse_playbook_item_list_read',
  PlaybookItemListCreate = 'role:analyse_playbook_item_list_create',
  PlaybookItemListUpdate = 'role:analyse_playbook_item_list_update',
  PlaybookItemListDelete = 'role:analyse_playbook_item_list_delete',
  PlaybookItemLinkRead = 'role:analyse_playbook_item_link_read',
  PlaybookItemLinkCreate = 'role:analyse_playbook_item_link_create',
  PlaybookItemLinkUpdate = 'role:analyse_playbook_item_link_update',
  PlaybookItemLinkDelete = 'role:analyse_playbook_item_link_delete',
  PlaybookItemTaskRead = 'role:analyse_playbook_item_task_read',
  PlaybookItemTaskCreate = 'role:analyse_playbook_item_task_create',
  PlaybookItemTaskUpdate = 'role:analyse_playbook_item_task_update',
  PlaybookItemTaskDelete = 'role:analyse_playbook_item_task_delete',
  PlaybookItemFindingRead = 'role:analyse_playbook_item_finding_read',
  PlaybookItemFindingCreate = 'role:analyse_playbook_item_finding_create',
  PlaybookItemFindingUpdate = 'role:analyse_playbook_item_finding_update',
  PlaybookItemFindingDelete = 'role:analyse_playbook_item_finding_delete',
  PlaybookItemIssueRead = 'role:analyse_playbook_item_issue_read',
  PlaybookItemIssueCreate = 'role:analyse_playbook_item_issue_create',
  PlaybookItemIssueUpdate = 'role:analyse_playbook_item_issue_update',
  PlaybookItemIssueDelete = 'role:analyse_playbook_item_issue_delete',
}
export enum ActionEditable {
  PlaybookItem = 1,
  Finding = 2,
  Task = 3,
  List = 4,
  Link = 5,
  Issues = 6,
}
