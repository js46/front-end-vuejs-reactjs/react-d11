import loadable from '@loadable/component';

export const Chativity = loadable(() => import('./Chativity'));
export const Wizard = loadable(() => import('./Wizard'));
export const StepMenu = loadable(() => import('./StepMenu'));
export const StepDetails = loadable(() => import('./StepDetails'));
