import React, { useState, useEffect } from 'react';
import {
  Box,
  Grid,
  Drawer,
  DrawerProps,
  Toolbar,
  Typography,
  Tooltip,
  IconButton,
} from '@material-ui/core';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { sortBy, maxBy } from 'lodash';
import { TaskTable, SortOrder, ESLoader } from '../../../components';
import { DeleteDialog } from '../../../components';
import { StepItem } from './StepDetails/StepItem';
import { IPlayItemTree, PlaybookPermission } from '../types';
import { ResponsePlayBookT } from '../../../services/playbook.services';
import { IOpportunityTask } from '../../../services/task.services';
import { AnalysePermissionState } from '../../../store/opportunity/selector';
import { StyledReactiveList } from '../../../components/styled';
import { FormDialog } from '../../../components/FormDialog';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import LinkOutlinedIcon from '@material-ui/icons/LinkOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import FormatListNumberedOutlinedIcon from '@material-ui/icons/FormatListNumberedOutlined';
import BallotIcon from '@material-ui/icons/Ballot';
import AddPlaybookItemDialog from './StepDetails/components/AddPlaybookItemDialog';
import { ConfigESIndex } from '../../../store/config/selector';
import { useTableHeadSorter } from '../../../hooks';
import {
  PlaybookDetailState,
  IsOpenAddingDialogPbItemState,
  IsOpenDeleteDialogPbItemState,
  PbItemDeleteIDState,
} from '../../../store/playbook/selector';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import {
  RequestDeletePbItem,
  CloseDeleteDialogPb,
  AddPlaybookItem,
} from '../../../store/playbook/actions';
import { ListsDialog } from '../../Opportunity/components/Sidebar/component/ListsDialog';
import { LinksDialog } from '../../Opportunity/components/Sidebar/component/LinksDialog';
import { ExpertFindingsDialog } from '../../Opportunity/components/Sidebar/component/FindingsDialog';
import { IssuesDialog } from '../../Opportunity/components/Sidebar/component/IssueDialog';
const useStyles = makeStyles(theme => ({
  drawer: {
    width: 256,
    flexShrink: 0,
  },
  paperAnchorDockedLeft: {
    borderRight: `1px solid ${theme.palette.grey[500]}`,
  },
  paper: {
    width: 256,
    backgroundColor: 'white',
    padding: '10px',
  },
  drawerPaper: {
    width: 256,
    backgroundColor: 'white',
  },
  upperButton: {
    padding: '5px 15px',
    fontWeight: 'bold',
    color: theme.palette.grey[800],
  },
  titleDialog: {
    color: theme.palette.text.primary,
  },
  dialog: {
    padding: '15px 0px',
  },
  item: {
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      background: theme.palette.grey[100],
    },
  },
  icon: {
    fontSize: 15,
  },
  overText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    maxWidth: 180,
    width: 180,
  },
  parentItem: {
    padding: '15px 0px 15px 8px',
    borderRadius: 5,
    '&:hover': {
      background: theme.palette.grey[100],
      cursor: 'pointer',
    },
    '& > .MuiGrid-spacing-xs-2 > .MuiGrid-item': {
      padding: 0,
    },
  },
  parentItemSelected: {
    padding: '8px 0px 8px 8px',
    borderRadius: 5,
    background: theme.palette.primary.light,
    '&:hover': {
      cursor: 'pointer',
    },
    '& > .MuiGrid-spacing-xs-2 > .MuiGrid-item': {
      padding: 0,
    },
  },
  tabInLhs: {
    cursor: 'pointer',
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));

type ISideBarProps = {} & DrawerProps;
const tableFields = [
  'title.keyword',
  'task_type.keyword',
  'entity_type.keyword',
  'task_state.keyword',
  'created_at',
  'created_by_user.name.keyword',
  'assignee_user.name.keyword',
  'last_updated_at',
  'requested_completion_date',
] as const;
const StepMenu: React.FC<ISideBarProps> = ({ variant, ...rest }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const playbookDetail = useSelector(PlaybookDetailState);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'last_updated_at',
    SortOrder.DESC,
  );
  const ES_INDICES = useSelector(ConfigESIndex);
  // const opportunity = useSelector(OpportunityState);
  const isOpenAddingDialog = useSelector(IsOpenAddingDialogPbItemState);
  const isOpenDeleteDialog = useSelector(IsOpenDeleteDialogPbItemState);
  const itemIdDelete = useSelector(PbItemDeleteIDState);
  const [openTaskDialog, setOpenTaskDialog] = useState<boolean>(false);
  const [currentOpenCollapse, setOpenCollapse] = useState<number | undefined>(
    1,
  );
  const [itemTrees, setItemFree] = useState<IPlayItemTree[]>();
  const [openListDialog, setOpenListDialog] = useState<boolean>(false);
  const [openLinkDialog, setOpenLinkDialog] = useState<boolean>(false);
  const [openFindingDialog, setOpenFindingDialog] = useState<boolean>(false);
  const [openIssuesDialog, setOpenIssuesDialog] = useState<boolean>(false);
  const [data, setData] = useState<IOpportunityTask[]>([]);
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);
  const dispatch = useDispatch();
  const animatedStyle = (index: number) => ({
    transition: '0.3s all',
    transform: currentOpenCollapse === index ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'poiter',
    },
  });
  useEffect(() => {
    if (playbookDetail) {
      const playbookItems = sortBy(
        playbookDetail?.playbook_items,
        'item_order',
      );
      setItemFree(convertTree(playbookItems ?? [], 0));
    }

    function convertTree(items: ResponsePlayBookT[], parent: number) {
      const itemFilter = items.filter(
        item => item.playbook_item_parent_id === parent,
      );
      let itemTrees: IPlayItemTree[] = [];
      if (!itemFilter) {
        return [];
      }
      for (let item of itemFilter) {
        let itemTree = {
          ...item,
          children: convertTree(items, item.id),
        };

        itemTrees.push(itemTree);
      }
      return itemTrees;
    }
  }, [playbookDetail]);
  const handleDeletePlaybookItem = async () => {
    dispatch(RequestDeletePbItem(itemIdDelete));
  };
  const orderItem =
    maxBy(itemTrees, item => item.item_order)?.item_order ??
    itemTrees?.length ??
    1;
  const defaultQueryTaskList = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                entity_type: 'opportunity_playbook_item',
              },
            },
            {
              term: {
                'opportunity_playbook.id': playbookDetail?.id,
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  };
  const customQueryList = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'opportunity_playbook.id': playbookDetail?.id,
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  };
  const customQueryLink = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'opportunity_playbook.id': playbookDetail?.id,
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  };
  const customQueryFinding = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                'opportunity_playbook.id': playbookDetail?.id,
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  };
  const customQueryIssue = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                entity_type: 'opportunity_playbook_item',
              },
            },
            {
              term: {
                'opportunity_playbook.id': playbookDetail?.id,
              },
            },
          ],
          filter: {
            term: {
              delete_flg: false,
            },
          },
        },
      },
    };
  };
  return (
    <>
      <Drawer
        variant="permanent"
        className={classes.drawer}
        classes={{
          paper: classes.paper,
          paperAnchorDockedLeft: classes.paperAnchorDockedLeft,
        }}
        {...rest}
      >
        <Toolbar />
        <Grid container>
          <Box
            mt={2}
            display="flex"
            style={{ width: '100%' }}
            alignItems="center"
            className={classes.parentItem}
            onClick={() => {
              if (currentOpenCollapse === 1) {
                setOpenCollapse(undefined);
              } else {
                setOpenCollapse(1);
              }
            }}
          >
            <Box display="flex" alignItems="center">
              <BallotIcon />
            </Box>
            <Box
              display="flex"
              ml={1}
              flex={1}
              alignItems="center"
              justifyContent="space-around"
            >
              <Typography variant="caption">Playbook Steps</Typography>
              {analysePermission?.includes(
                PlaybookPermission.PlaybookItemCreate,
              ) && (
                <Tooltip title="Add Playbook Step" placement="top">
                  <IconButton
                    size="small"
                    aria-controls="fade-menu"
                    aria-haspopup="true"
                    onClick={() => {
                      dispatch(
                        AddPlaybookItem({
                          parent: 0,
                          position: [itemTrees?.length ?? 1],
                          item_order: orderItem + 1,
                        }),
                      );
                    }}
                    aria-label="Action"
                  >
                    <AddCircleOutlineIcon color="primary" />
                  </IconButton>
                </Tooltip>
              )}
            </Box>

            <Box display="flex" justifyContent="flex-end" mr={2}>
              <ArrowForwardIosIcon
                style={animatedStyle(1)}
                className={classes.icon}
              />
            </Box>
          </Box>
          {currentOpenCollapse === 1 && (
            <Box>
              {itemTrees?.map((item, index) => {
                return (
                  <StepItem
                    playbook={playbookDetail}
                    data={item}
                    key={index}
                    itemIndex={index}
                  />
                );
              })}
            </Box>
          )}
          <Box
            display="flex"
            style={{ width: '100%' }}
            alignItems="center"
            className={classes.parentItem}
            onClick={() => {
              if (currentOpenCollapse === 2) {
                setOpenCollapse(undefined);
              } else {
                setOpenCollapse(2);
              }
            }}
          >
            <Box display="flex" alignItems="center">
              <LibraryBooksIcon />
            </Box>
            <Box display="flex" ml={1} flex={1}>
              <Typography variant="caption">Playbook Content</Typography>
            </Box>

            <Box display="flex" justifyContent="flex-end" mr={2}>
              <ArrowForwardIosIcon
                style={animatedStyle(2)}
                className={classes.icon}
              />
            </Box>
          </Box>
          {currentOpenCollapse === 2 && (
            <Box display="flex" flexDirection="column">
              <Box
                display="flex"
                alignItems="center"
                ml={2}
                flex={1}
                mt={2}
                mb={2}
                className={classes.tabInLhs}
                onClick={() => {
                  setOpenTaskDialog(true);
                }}
              >
                <Box mr={1} display="flex" alignItems="center">
                  <FormatListBulletedIcon />
                </Box>
                <Typography>{t('task.title_in_LHS')}</Typography>
              </Box>
              {/* Issues */}
              <Box
                display="flex"
                alignItems="center"
                ml={2}
                flex={1}
                mt={2}
                mb={2}
                className={classes.tabInLhs}
                onClick={() => {
                  setOpenIssuesDialog(true);
                }}
              >
                <Box mr={1} display="flex" alignItems="center">
                  <ReportProblemOutlinedIcon />
                </Box>
                <Typography>{t('issue.title_in_LHS')}</Typography>
              </Box>
              {/* Item list */}
              <Box
                display="flex"
                alignItems="center"
                ml={2}
                flex={1}
                mt={2}
                mb={2}
                className={classes.tabInLhs}
                onClick={() => {
                  setOpenListDialog(true);
                }}
              >
                <Box mr={1} display="flex" alignItems="center">
                  <FormatListNumberedOutlinedIcon />
                </Box>
                <Typography>{t('item_list.title_in_LHS')}</Typography>
              </Box>
              {/* link */}
              <Box
                display="flex"
                alignItems="center"
                ml={2}
                flex={1}
                mt={2}
                mb={2}
                className={classes.tabInLhs}
                onClick={() => {
                  setOpenLinkDialog(true);
                }}
              >
                <Box mr={1} display="flex" alignItems="center">
                  <LinkOutlinedIcon />
                </Box>
                <Typography>{t('link.title_in_LHS')}</Typography>
              </Box>
              {/* finding */}
              <Box
                display="flex"
                alignItems="center"
                ml={2}
                flex={1}
                mt={2}
                mb={2}
                className={classes.tabInLhs}
                onClick={() => {
                  setOpenFindingDialog(true);
                }}
              >
                <Box mr={1} display="flex" alignItems="center">
                  <SearchOutlinedIcon />
                </Box>
                <Typography>{t('finding.title_in_LHS')}</Typography>
              </Box>
            </Box>
          )}

          {openTaskDialog && (
            <FormDialog
              title={t('task.title_in_LHS')}
              open={openTaskDialog}
              setOpen={setOpenTaskDialog}
              labelBtn="OK"
              maxWidth={data.length === 0 ? 'sm' : 'lg'}
              isAddingForm={false}
              submit={() => {
                setOpenTaskDialog(false);
              }}
              disable={false}
              formContent={
                <ReactiveBase
                  app={ES_INDICES.opportunity_task_index_name}
                  url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                  headers={{
                    Authorization: window.localStorage.getItem('jwt'),
                  }}
                >
                  <StyledReactiveList
                    componentId="opportunity-task"
                    dataField={orderBy}
                    sortBy={order}
                    renderResultStats={() => null}
                    renderNoResults={() => null}
                    defaultQuery={defaultQueryTaskList}
                    onData={({ data }) => setData(data)}
                    size={5}
                    paginationAt="bottom"
                    pages={5}
                    pagination={true}
                    showLoader
                    loader={<ESLoader pageSize={5} />}
                    render={({ data, loading }) => {
                      if (loading) return null;
                      if (!data?.length)
                        return (
                          <Box mt={3}>
                            <Typography>
                              {t('task.no_task_found_in_playground')}
                            </Typography>
                          </Box>
                        );
                      return (
                        <Box mt={2}>
                          <TaskTable
                            data={data}
                            onRequestSort={handleRequestSort}
                            order={order}
                            orderBy={orderBy}
                            clickDisabled={true}
                          />
                        </Box>
                      );
                    }}
                    innerClass={{
                      button: classes.buttonPagination,
                      resultsInfo: classes.resultsInfo,
                      sortOptions: classes.sortSelect,
                    }}
                  />
                </ReactiveBase>
              }
            />
          )}
          {isOpenAddingDialog && <AddPlaybookItemDialog />}
          <DeleteDialog
            isOpen={isOpenDeleteDialog}
            header="Delete Playbook Item"
            message={
              <Typography component="p" variant="body1">
                Are you sure you want to delete{' '}
                <Typography component="span" variant="caption">
                  {
                    playbookDetail?.playbook_items?.find(
                      item => item.id === itemIdDelete,
                    )?.title
                  }
                </Typography>{' '}
                playbook item?
              </Typography>
            }
            handleClose={() => {
              dispatch(CloseDeleteDialogPb());
            }}
            handleDelete={handleDeletePlaybookItem}
          />
        </Grid>
      </Drawer>
      <ListsDialog
        openModal={openListDialog}
        setOpen={setOpenListDialog}
        customQueryES={customQueryList}
      />
      <LinksDialog
        openModal={openLinkDialog}
        setOpen={setOpenLinkDialog}
        customQueryES={customQueryLink}
      />
      <ExpertFindingsDialog
        openModal={openFindingDialog}
        setOpen={setOpenFindingDialog}
        customQueryES={customQueryFinding}
      />
      <IssuesDialog
        openModal={openIssuesDialog}
        setOpen={setOpenIssuesDialog}
        customQueryES={customQueryIssue}
      />
    </>
  );
};

export default StepMenu;
