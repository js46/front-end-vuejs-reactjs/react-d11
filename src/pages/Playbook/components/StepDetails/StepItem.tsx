import React, { useState, useContext } from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Box, Grid, Typography, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { OpportunityPlaybookT } from '../../../../services/playbook.services';
import { IPlayItemTree } from '../../types';
import { StepItemAction } from './StepItemAction';
import PlaybookContext from '../../PlaybookContext';
const useStyles = makeStyles(theme => ({
  drawer: {
    width: 300,
    flexShrink: 0,
  },
  paperAnchorDockedLeft: {
    borderRight: `1px solid ${theme.palette.grey[500]}`,
  },
  paper: {
    width: 300,
    backgroundColor: 'white',
    padding: theme.spacing(0, 3),
  },
  drawerPaper: {
    width: 300,
    backgroundColor: 'white',
  },
  upperButton: {
    padding: '5px 15px',
    fontWeight: 'bold',
    color: theme.palette.grey[800],
  },
  titleDialog: {
    color: theme.palette.text.primary,
  },
  dialog: {
    padding: '15px 0px',
  },
  item: {
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      background: theme.palette.grey[100],
    },
  },
  icon: {
    fontSize: 15,
  },
  iconSelected: {
    fontSize: 15,
    color: theme.palette.primary.main,
  },
  overText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    maxWidth: 180,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  parentItem: {
    padding: '8px 0px 8px 8px',
    borderRadius: 5,
    '&:hover': {
      background: theme.palette.grey[100],
      cursor: 'pointer',
    },
    '& > .MuiGrid-spacing-xs-2 > .MuiGrid-item': {
      padding: 0,
    },
  },
  parentItemSelected: {
    padding: '8px 0px 8px 8px',
    borderRadius: 5,
    background: theme.palette.primary.light,
    '&:hover': {
      cursor: 'pointer',
    },
    '& > .MuiGrid-spacing-xs-2 > .MuiGrid-item': {
      padding: 0,
    },
  },
  titleSelected: {
    color: theme.palette.primary.main,
    fontWeight: 'bold',
  },
  title: {},
}));

interface StepItemProps {
  playbook?: OpportunityPlaybookT;
  data: IPlayItemTree;
  itemIndex: number;
}

export const StepItem: React.FC<StepItemProps> = ({
  playbook,
  data,
  itemIndex,
}) => {
  const classes = useStyles();

  const [openCollapse, setCollapse] = useState<string[]>([]);
  const { setSelectedItemID, setLabel, selectedItemID } = useContext(
    PlaybookContext,
  );
  const selectPlaybookItem = (id: number, label: string) => {
    setSelectedItemID?.(id);
    setLabel?.(label);
  };
  const renderPlaybookItem = (item: IPlayItemTree, position: number[]) => {
    const label = position.join('.');
    const isOpenCollapse = openCollapse.indexOf(label) > -1;
    const hasChildren = item.children && item.children.length > 0;
    const animatedStyle = {
      transition: '0.3s all',
      transform: isOpenCollapse ? 'rotate(90deg)' : 'rotate(0deg)',
      '&:hover': {
        cursor: 'pointer',
      },
    };
    const width = 190 - position.length * 20 - (hasChildren ? 12 : 0);
    return (
      <>
        <Box
          display="flex"
          pl={position.length}
          alignItems="center"
          className={
            selectedItemID === item.id
              ? classes.parentItemSelected
              : classes.parentItem
          }
        >
          <Box display="flex" pl={position.length - 1} alignItems="center">
            <Box pr={hasChildren ? '2px' : '10px'}>
              <Typography
                component="span"
                variant="caption"
                className={
                  selectedItemID === item.id
                    ? classes.titleSelected
                    : classes.title
                }
              >
                {label}
              </Typography>
            </Box>

            {hasChildren && (
              <IconButton
                size="small"
                onClick={() => {
                  if (isOpenCollapse) {
                    setCollapse(
                      openCollapse.filter(item => item !== label) ?? [],
                    );
                  } else {
                    setCollapse([...openCollapse, label]);
                  }
                }}
              >
                <ArrowForwardIosIcon
                  style={animatedStyle}
                  className={
                    selectedItemID === item.id
                      ? classes.iconSelected
                      : classes.icon
                  }
                />
              </IconButton>
            )}

            <Box
              onClick={() => selectPlaybookItem(item.id, label)}
              flex={1}
              key={label}
              width={width}
              display="flex"
            >
              <Typography
                key={label}
                component="span"
                variant="body1"
                className={
                  selectedItemID === item.id
                    ? `${classes.titleSelected} ${classes.overText}`
                    : `${classes.title} ${classes.overText}`
                }
              >
                {item?.title ?? ''}
              </Typography>
            </Box>

            <Box>
              <StepItemAction data={item} position={position} />
            </Box>
          </Box>
        </Box>

        {isOpenCollapse &&
          hasChildren &&
          item.children?.map((itemChild, indexChild) => (
            <Grid
              item
              container
              xs={12}
              key={[...position, indexChild + 1].join('.')}
            >
              <Box alignItems="center" flex={1}>
                {renderPlaybookItem(itemChild, [...position, indexChild + 1])}
              </Box>
            </Grid>
          ))}
      </>
    );
  };
  return (
    <Grid item container xs={12}>
      <Box alignItems="center" flex={1} key={itemIndex}>
        {renderPlaybookItem(data, [itemIndex + 1])}
      </Box>
    </Grid>
  );
};
