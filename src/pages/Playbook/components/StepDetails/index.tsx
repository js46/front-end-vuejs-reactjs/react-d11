import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Button,
  Grid,
  TextField,
  Typography,
  InputAdornment,
  Tooltip,
  IconButton,
  Dialog,
  DialogActions,
  Chip,
} from '@material-ui/core';
import PlaybookContext from '../../PlaybookContext';
import { Formik } from 'formik';
import { CustomContextMenuOption, MixTitle } from '../../../../components';
import { PanelGroup } from './components/PanelGroup';
import {
  PlaybookItemFindingT,
  updatePlaybookItem,
  IListItem,
  ResponsePlayBookT,
} from '../../../../services/playbook.services';
import { PlaybookDetailState } from '../../../../store/playbook/selector';

import { IResponseLink } from '../../../../services/link';
import { IIssueItems } from '../../../../services/issue';
import { ActionEditable, PlaybookPermission } from '../../types';
import { useParams } from 'react-router-dom';
import { TaskMenu } from './components/Task/TaskMenu';
import { IssueMenu } from './components/IssueMenu';
import { IOpportunityTask } from '../../../../services/task.services';
import { TaskState, TaskSubType, TaskType } from '../../../../constants';
import { AnalysePermissionState } from '../../../../store/opportunity/selector';
import { notifySuccess } from '../../../../store/common/actions';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import {
  RequestChangeStatePbItem,
  ChangeStatePbItem,
} from '../../../../store/playbook/actions';
import { useSelector } from 'react-redux';
import { useDisableMultipleClick } from '../../../../hooks';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  PlayCircleOutline,
  RotateLeft,
  CheckCircleOutline,
} from '@material-ui/icons';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import { CustomAddMenu } from './components/CustomAddMenu';
import {
  ITaskState,
  TaskStates,
  TaskStateColors,
} from '../../../Opportunity/components/AnalysisTab/types';
import { maxBy } from 'lodash';

const useStyles = makeStyles(theme => ({
  icon: {
    fontSize: 18,
  },
  button: {
    '&:hover': {
      textDecorationLine: 'underline',
    },
  },
}));

const listState = [
  {
    id: 1,
    name: 'To do',
  },
  {
    id: 2,
    name: 'In Progress',
  },
  {
    id: 3,
    name: 'Done',
  },
];

const dateDefault = new Date('0001-01-01T00:00:00Z');

const StepDetails = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const classes = useStyles();
  const playbookDetail = useSelector(PlaybookDetailState);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const { selectedItem, onRefresh, label } = useContext(PlaybookContext);
  const [currentOpenCollapse, setOpenCollapse] = useState<number>();
  const [findings, setFindings] = useState<PlaybookItemFindingT[]>([]);
  const [links, setLinks] = useState<IResponseLink[]>([]);
  const [editable, setEditable] = useState<number>(0);
  const [isOpenEditDueDatePopup, setOpenEditDueDatePopup] = useState<boolean>(
    false,
  );
  const [task, setTasks] = useState<IOpportunityTask[]>([]);
  const [listItems, setListItems] = useState<IListItem[]>([]);
  const [issue, setIssue] = useState<IIssueItems[]>([]);
  const [indexSidebarTab, setIndexSidebarTab] = useState<number>(0);
  const analysePermission = useSelector(AnalysePermissionState);
  const { opportunityID } = useParams();
  const [actionsEnabled, setActionsEnabled] = useState<number | undefined>(
    selectedItem?.playbook_item_state?.id,
  );
  // const opportunity = useSelector(OpportunityState);
  const [openIssueDialog, setOpenIssueDialog] = useState<boolean>(false);
  const [openTaskDialog, setOpenTaskDialog] = useState<boolean>(false);

  const animatedStyle = (index: number) => ({
    fontSize: 20,
    transition: '0.3s all',
    transform: currentOpenCollapse === index ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'pointer',
    },
  });
  const [dueDate, setDueDate] = useState<Date | null | undefined>(
    playbookDetail?.opportunity.completion_date !== '0001-01-01T00:00:00Z'
      ? moment(playbookDetail?.opportunity.completion_date).toDate()
      : moment().toDate(),
  );

  const handleChangeStatus = async (actionId: number) => {
    setActionsEnabled(actionId);

    const payload = {
      playbook_item_state_id: actionId,
    };

    if (isSubmitting) {
      return;
    }
    await debounceFn();
    dispatch(ChangeStatePbItem(selectedItem?.id));

    dispatch(RequestChangeStatePbItem(payload));
    endRequest();

    dispatch(
      notifySuccess({
        message: t('playbook_step_item.change_status_successfully'),
      }),
    );
  };
  useEffect(() => {
    setFindings(selectedItem?.findings ?? []);
    setIssue(selectedItem?.issues ?? []);
    setTasks(
      selectedItem?.tasks?.filter(e => {
        return (
          e.entity_type === 'opportunity_playbook_item' &&
          e.task_type === 'USER'
        );
      }) ?? [],
    );
    setLinks(selectedItem?.links ?? []);
    setListItems(selectedItem?.lists ?? []);
    setEditable(0);
    setActionsEnabled(selectedItem?.playbook_item_state?.id);
    setOpenCollapse(selectedItem?.id ?? 0);
  }, [selectedItem]);
  const onClickAdd = useCallback(
    (type: 'finding' | 'task' | 'list' | 'link' | 'issue') => {
      if (type === 'finding') {
        setFindings(prev => [
          ...prev,
          {
            id: 0,
            title: '',
            description: '',
            date: '',
            created_by_user: {
              id: 0,
              name: '',
            },
            public_flg: false,
          },
        ]);
        setEditable(ActionEditable.Finding);
      } else if (type === 'task') {
        setTasks(prev => [
          ...prev,
          {
            id: 0,
            opportunity: {
              id: opportunityID,
              phase: '',
              title: '',
              state: '',
            },
            title: '',
            description: '',
            task_type: TaskType.User,
            task_state: TaskState.Assigned,
            task_sub_type: TaskSubType.Review,
            assignee_user: { id: 0, name: '' },
            archive_flg: false,
            requested_completion_date: null,
          },
        ]);
        setEditable(ActionEditable.Task);
      } else if (type === 'link') {
        setLinks(prev => [
          ...prev,
          {
            id: 0,
            title: '',
            opportunity_playbook_item_id: selectedItem?.id ?? 0,
            links: [
              {
                id: 0,
                name: '',
                url: '',
                parent_id: 0,
                description: '',
                entity_type: 'opportunity_playbook_item',
              },
            ],
          },
        ]);
        setEditable(ActionEditable.Link);
      } else if (type === 'list') {
        setListItems(prev => [
          ...prev,
          {
            id: 0,
            type: {
              id: 1,
              name: 'Dependency',
              description: '',
            },
            title: '',
            item_order: 0,
            parent_id: 0,
            is_deleted: false,
            item_list: [],
          },
        ]);
        setEditable(ActionEditable.List);
      } else {
        setIssue(prev => [
          ...prev,
          {
            id: 0,
            title: '',
            description: '',
            state: '',
            is_deleted: false,
          },
        ]);
        setEditable(ActionEditable.Issues);
      }
    },
    [opportunityID, selectedItem],
  );
  const actions: CustomContextMenuOption[] = [
    {
      name: 'Add Finding',
      disabled:
        !!editable ||
        !analysePermission?.includes(
          PlaybookPermission.PlaybookItemFindingCreate,
        ),
      onClick: () => onClickAdd('finding'),
    },
    {
      name: 'Add List',
      disabled:
        !!editable ||
        !analysePermission?.includes(PlaybookPermission.PlaybookItemListCreate),
      onClick: () => onClickAdd('list'),
    },
    {
      name: 'Add Links',
      disabled:
        !!editable ||
        !analysePermission?.includes(PlaybookPermission.PlaybookItemLinkCreate),
      onClick: () => onClickAdd('link'),
    },
    {
      name: 'Add Task',
      disabled:
        !!editable ||
        !analysePermission?.includes(PlaybookPermission.PlaybookItemTaskCreate),
      onClick: () => onClickAdd('task'),
    },
    {
      name: 'Add Issue',
      disabled:
        !!editable ||
        !analysePermission?.includes(
          PlaybookPermission.PlaybookItemIssueCreate,
        ),
      onClick: () => onClickAdd('issue'),
    },
  ];

  const handleSubmit = async () => {
    if (selectedItem) {
      await updatePlaybookItem(
        {
          playbook_items: [
            {
              due_date: dueDate,
            },
          ],
        },
        selectedItem.id,
      );
      onRefresh?.();
    }
  };

  const optionContents = actions.filter(
    action => action.name !== 'Add Task' && action.name !== 'Add Issue',
  );

  const getTaskState = (stepItem: ResponsePlayBookT) => {
    const completionDate = playbookDetail?.opportunity.completion_date;
    let taskState = {
      label: TaskStates.todo,
      color: TaskStateColors.blank,
      order: 0,
    };
    if (stepItem.due_date?.toString() === '0001-01-01T00:00:00Z') {
      taskState = {
        label: TaskStates.todo,
        color: TaskStateColors.blank,
        order: 0,
      };
    } else if (
      moment(completionDate).isBefore(
        moment(stepItem.due_date?.toString()),
        'day',
      )
    ) {
      if (stepItem.playbook_item_state?.name === 'DONE') {
        taskState = {
          label: TaskStates.done,
          color: TaskStateColors.red,
          order: 3,
        };
      } else {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.red,
          order: 3,
        };
      }
    } else if (!stepItem.tasks || stepItem.tasks.length === 0) {
      if (stepItem.playbook_item_state?.name === 'DONE') {
        taskState = {
          label: TaskStates.done,
          color: TaskStateColors.green,
          order: 3,
        };
      } else {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.green,
          order: 1,
        };
      }
    } else {
      let maxDueDate = maxBy(stepItem.tasks, (item: IOpportunityTask) =>
        new Date(item?.requested_completion_date ?? '').getTime(),
      )?.requested_completion_date;

      if (
        maxDueDate &&
        moment(maxDueDate).isAfter(moment(stepItem.due_date), 'day') &&
        moment(maxDueDate).isAfter(moment(completionDate), 'day')
      ) {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.red,
          order: 3,
        };
      } else if (
        maxDueDate &&
        moment(maxDueDate).isAfter(moment(stepItem.due_date), 'day') &&
        moment(maxDueDate).isSameOrBefore(moment(completionDate), 'day')
      ) {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.amber,
          order: 2,
        };
      } else {
        taskState = {
          label: TaskStates.inProgress,
          color: TaskStateColors.green,
          order: 1,
        };
      }
    }
    if (stepItem.playbook_item_state?.name === 'DONE') {
      taskState.label = TaskStates.done;
    }
    return taskState;
  };

  if (!selectedItem) return null;

  const mergeTasks = (data: ResponsePlayBookT) => {
    let tasks = data.tasks ?? [];
    let itemChild = playbookDetail?.playbook_items?.filter(
      itemChild => itemChild.playbook_item_parent_id === data.id,
    );
    if (!!itemChild && itemChild?.length) {
      for (let item of itemChild) {
        let tasksChild = mergeTasks(item);
        if (tasksChild.length) {
          tasks = [...tasks, ...tasksChild];
        }
      }
    }
    return tasks;
  };

  let taskState = { ...selectedItem };
  taskState.tasks = mergeTasks(taskState);

  let stateStep: ITaskState = getTaskState(taskState);
  function findParentStepItem(
    stepItem?: ResponsePlayBookT,
  ): ResponsePlayBookT | undefined {
    if (!stepItem || !stepItem.playbook_item_parent_id) {
      return stepItem;
    }
    let itemParent = playbookDetail?.playbook_items?.find(
      child => child.id === stepItem.playbook_item_parent_id,
    );
    return findParentStepItem(itemParent);
  }
  const parentStepItem = findParentStepItem(selectedItem);
  return (
    <Grid container spacing={1} direction="column">
      <Grid item container direction="row" alignItems="center">
        <IconButton
          size="small"
          onClick={() => {
            if (currentOpenCollapse === selectedItem?.id) {
              setOpenCollapse(undefined);
            } else {
              setOpenCollapse(selectedItem.id);
            }
          }}
        >
          <ArrowForwardIosIcon
            style={animatedStyle(selectedItem?.id ?? 0)}
            className={classes.icon}
          />
        </IconButton>
        <Typography variant="h4">
          Step #{label}: {selectedItem.title}
        </Typography>

        <Box>
          {analysePermission?.includes(PlaybookPermission.PlaybookItemWork) &&
          selectedItem.playbook_item_parent_id === 0 ? (
            <Box display="flex" flex={1} ml={1}>
              {actionsEnabled === listState[0].id ? (
                <Tooltip title={t('playbook_step_item.start_work')}>
                  <Button
                    onClick={() => {
                      setOpenEditDueDatePopup(true);
                    }}
                    startIcon={<PlayCircleOutline color="primary" />}
                    className={classes.button}
                  >
                    Start Work
                  </Button>
                </Tooltip>
              ) : actionsEnabled === listState[1].id ? (
                <Tooltip title={t('playbook_step_item.end_work')}>
                  <Button
                    onClick={() => {
                      handleChangeStatus(listState[2].id);
                    }}
                    startIcon={<CheckCircleOutline color="primary" />}
                    className={classes.button}
                  >
                    End Work
                  </Button>
                </Tooltip>
              ) : actionsEnabled === listState[2].id ? (
                <Tooltip title={t('playbook_step_item.back_to_in_progress')}>
                  <Button
                    onClick={() => {
                      setOpenEditDueDatePopup(true);
                      handleChangeStatus(listState[1].id);
                    }}
                    startIcon={<RotateLeft color="primary" />}
                    className={classes.button}
                  >
                    Back to In progress
                  </Button>
                </Tooltip>
              ) : null}
            </Box>
          ) : (
            <></>
          )}
        </Box>
        {selectedItem.playbook_item_parent_id === 0 && (
          <Box>
            <Chip
              label={stateStep?.label}
              size="small"
              style={{
                background: stateStep?.color,
              }}
            />
          </Box>
        )}
        {parentStepItem?.playbook_item_state?.name !== 'TODO' && (
          <Box display="flex" flex={1} justifyContent="flex-end">
            <Box>
              <TaskMenu
                openTaskDialog={openTaskDialog}
                setOpenTaskDialog={setOpenTaskDialog}
                tasks={task}
                onClickAdd={onClickAdd}
                setTasks={setTasks}
                disabled={
                  !!editable ||
                  !analysePermission?.includes(
                    PlaybookPermission.PlaybookItemTaskRead,
                  )
                }
              />
            </Box>
            <Box>
              <IssueMenu
                setOpenIssueDialog={setOpenIssueDialog}
                issues={issue}
                onClickAdd={onClickAdd}
                setIssue={setIssue}
                disabled={
                  !!editable ||
                  !analysePermission?.includes(
                    PlaybookPermission.PlaybookItemIssueRead,
                  )
                }
              />
            </Box>
            <Box>
              <CustomAddMenu options={optionContents} />
            </Box>
          </Box>
        )}
      </Grid>
      <Grid item lg={12}>
        {/* Start Detail form */}
        <Formik
          enableReinitialize
          initialValues={{
            title: selectedItem.title,
            description: selectedItem.description,
            due_date:
              moment(selectedItem.due_date).year() ===
              moment(dateDefault).year()
                ? null
                : selectedItem.due_date,
          }}
          onSubmit={async (values, formikHelpers) => {
            if (!selectedItem) return;
            formikHelpers.setSubmitting(true);
            try {
              await updatePlaybookItem(
                {
                  playbook_items: [values],
                },
                selectedItem.id,
              );
              onRefresh?.();
              setEditable(0);
            } catch (e) {
              console.log('Error saving playbook item', e);
            }
            formikHelpers.setSubmitting(false);
          }}
        >
          {({
            values,
            handleSubmit,
            handleChange,
            handleReset,
            isSubmitting,
            setFieldValue,
          }) => (
            <form onSubmit={handleSubmit}>
              {currentOpenCollapse === selectedItem?.id && (
                <>
                  <Box mt={1} display="flex" justifyContent="flex-end">
                    {actionsEnabled === listState[0].id ? (
                      <></>
                    ) : (
                      <>
                        {editable === ActionEditable.PlaybookItem ? (
                          <>
                            <Box mr={1}>
                              <Button
                                variant="outlined"
                                color="default"
                                type="button"
                                onClick={() => {
                                  handleReset();
                                  setEditable(0);
                                }}
                                disabled={isSubmitting}
                              >
                                Cancel
                              </Button>
                            </Box>
                            <Button
                              variant="contained"
                              color="primary"
                              type="button"
                              disabled={isSubmitting}
                              onClick={() => handleSubmit()}
                            >
                              Save
                            </Button>
                          </>
                        ) : (
                          <>
                            <Button
                              color="primary"
                              variant="outlined"
                              disabled={
                                !!editable ||
                                !analysePermission?.includes(
                                  PlaybookPermission.PlaybookItemUpdate,
                                )
                              }
                              onClick={() =>
                                setEditable(ActionEditable.PlaybookItem)
                              }
                              type="button"
                            >
                              Edit
                            </Button>
                          </>
                        )}
                      </>
                    )}
                  </Box>
                  <Box>
                    <Grid container spacing={3}>
                      <Grid item xs={8}>
                        <MixTitle title="Title" isRequired />
                        <TextField
                          value={values.title}
                          onChange={handleChange}
                          variant="outlined"
                          fullWidth
                          name="title"
                          required
                          disabled={
                            editable !== ActionEditable.PlaybookItem ||
                            isSubmitting
                          }
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box>
                          <MixTitle title="Due date" />
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              disabled={
                                editable !== ActionEditable.PlaybookItem ||
                                isSubmitting
                              }
                              disableToolbar
                              allowKeyboardControl
                              inputVariant="outlined"
                              variant="inline"
                              margin="normal"
                              id="due_date"
                              name="due_date"
                              format="d MMMM yyyy"
                              autoOk={true}
                              onChange={(date: MaterialUiPickersDate) =>
                                setFieldValue('due_date', date)
                              }
                              value={values.due_date}
                              animateYearScrolling={false}
                              fullWidth
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <ExpandMoreIcon />
                                  </InputAdornment>
                                ),
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </Box>
                      </Grid>
                    </Grid>
                  </Box>
                  <Box mt={2}>
                    <MixTitle title="Description" />
                    <TextField
                      rows={3}
                      multiline
                      value={values.description}
                      onChange={handleChange}
                      variant="outlined"
                      fullWidth
                      name="description"
                      disabled={
                        editable !== ActionEditable.PlaybookItem || isSubmitting
                      }
                    />
                  </Box>
                </>
              )}
            </form>
          )}
        </Formik>
        {/* End Detail form */}
      </Grid>

      <Grid item>
        {/* Start Accordion group */}
        <PanelGroup
          findings={findings}
          setFinding={setFindings}
          setEditable={setEditable}
          editable={editable}
          tasks={task}
          setTasks={setTasks}
          listItems={listItems}
          setListItems={setListItems}
          indexSidebarTab={indexSidebarTab}
          setIndexSidebarTab={setIndexSidebarTab}
          links={links}
          setLinks={setLinks}
          issueItem={issue}
          setIssue={setIssue}
          onClickAdd={onClickAdd}
          disableHandle={actionsEnabled === listState[0].id}
          actions={actions}
          openIssueDialog={openIssueDialog}
          setOpenIssueDialog={setOpenIssueDialog}
          openTaskDialog={openTaskDialog}
          setOpenTaskDialog={setOpenTaskDialog}
        />
      </Grid>

      {isOpenEditDueDatePopup && (
        <Dialog
          open={isOpenEditDueDatePopup}
          onClose={() => setOpenEditDueDatePopup(false)}
          fullWidth
          maxWidth="sm"
          aria-labelledby="alert-dialog-title"
        >
          <Box display="flex" flex={1} justifyContent="center" mt={4} mb={2}>
            <Typography variant="h4">Update Due Date</Typography>
          </Box>
          <Box pt={2} pb={2} pl={4} pr={4} mt={1}>
            <MixTitle title="Due date" />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker
                disableToolbar
                allowKeyboardControl
                inputVariant="outlined"
                variant="inline"
                margin="normal"
                id="due_date"
                name="due_date"
                format="d MMMM yyyy"
                autoOk={true}
                onChange={(date: MaterialUiPickersDate) => setDueDate(date)}
                value={dueDate}
                animateYearScrolling={false}
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <ExpandMoreIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </MuiPickersUtilsProvider>
          </Box>
          <DialogActions>
            <Box display="flex" justifyContent="center" flex={1} mb={2}>
              <Button
                onClick={() => {
                  setOpenEditDueDatePopup(false);
                  handleSubmit();
                  handleChangeStatus(listState[1].id);
                }}
                variant="contained"
                color="primary"
                disabled={!dueDate}
              >
                OK
              </Button>
            </Box>
          </DialogActions>
        </Dialog>
      )}
    </Grid>
  );
};
export default StepDetails;
