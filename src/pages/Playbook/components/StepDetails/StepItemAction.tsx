import React, { useState } from 'react';

import { Box, MenuItem, Tooltip, IconButton } from '@material-ui/core';
import Fade from '@material-ui/core/Fade';
import { useSelector, useDispatch } from 'react-redux';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import { StyledMenu } from '../../../../components/styled';
import { AnalysePermissionState } from '../../../../store/opportunity/selector';

import { PlaybookPermission, IPlayItemTree } from '../../types';
import {
  OpenDeleteDialogPb,
  AddPlaybookItem,
} from '../../../../store/playbook/actions';
import { maxBy } from 'lodash';

interface StepItemActionProps {
  data: IPlayItemTree;
  position: number[];
}

export const StepItemAction: React.FC<StepItemActionProps> = ({
  data,
  position,
}) => {
  const analysePermission = useSelector(AnalysePermissionState);

  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const level = position.length;

  let orderItem = 0;
  if (data && data.children && data.children.length > 0) {
    orderItem =
      maxBy(data.children, item => item.item_order)?.item_order ??
      data.children.length;
  }
  console.log();
  // const initialDataForm = {
  //   note: data?.notes,
  //   title: data?.title,
  //   detail: data?.description,
  // };
  // const playbookItemSchema = Yup.object({
  //   title: Yup.string().required('This field is required'),
  // });
  return (
    <Box>
      <Tooltip title="Action" placement="top">
        <IconButton
          size="small"
          aria-controls="fade-menu"
          aria-haspopup="true"
          onClick={handleClick}
          aria-label="Action"
        >
          <MoreVertIcon />
        </IconButton>
      </Tooltip>
      {setAnchorEl && (
        <StyledMenu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          {level < 3 && (
            <MenuItem
              key={1}
              disabled={
                !analysePermission?.includes(
                  PlaybookPermission.PlaybookItemCreate,
                )
              }
              onClick={() => {
                setAnchorEl(null);
                dispatch(
                  AddPlaybookItem({
                    parent: data.id,
                    position: [...position, data.children?.length ?? 1],
                    item_order: orderItem + 1,
                  }),
                );
              }}
            >
              Add Sub Step
            </MenuItem>
          )}

          <MenuItem
            key={3}
            onClick={() => {
              setAnchorEl(null);
              dispatch(OpenDeleteDialogPb(data.id));
            }}
            disabled={
              !analysePermission?.includes(
                PlaybookPermission.PlaybookItemDelete,
              ) ||
              (data.children && data.children?.length > 0)
            }
          >
            Delete Step
          </MenuItem>
        </StyledMenu>
      )}
    </Box>
  );
};
