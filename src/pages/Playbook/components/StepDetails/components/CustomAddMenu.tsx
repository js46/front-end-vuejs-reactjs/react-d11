import React from 'react';
import { Box, Button, MenuItem } from '@material-ui/core';
import { StyledMenu } from '../../../../../components/styled';
import Fade from '@material-ui/core/Fade';
import { AddCircleOutline } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  button: {
    minWidth: 40,
    '&:hover': {
      textDecorationLine: 'underline',
    },
  },
}));

export type CustomContextMenuOption = {
  name: string;
  onClick?: () => any;
  disabled?: boolean;
};
type CustomContextMenuProps = {
  options: CustomContextMenuOption[];
  size?: 'small' | 'medium';
};
export const CustomAddMenu: React.FC<CustomContextMenuProps> = ({
  options,
  size,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  return (
    <Box>
      <Button
        variant="text"
        onClick={(event: any) => setAnchorEl(event.target)}
        className={classes.button}
        startIcon={<AddCircleOutline color="primary" />}
      >
        Add More
      </Button>
      <StyledMenu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
        TransitionComponent={Fade}
      >
        {options.map((option, index) => (
          <MenuItem
            key={index}
            onClick={() => {
              setAnchorEl(null);
              option.onClick?.();
            }}
            disabled={option.disabled}
          >
            {option.name}
          </MenuItem>
        ))}
      </StyledMenu>
    </Box>
  );
};
