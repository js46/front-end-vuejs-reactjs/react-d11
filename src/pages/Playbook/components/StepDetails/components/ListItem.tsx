import React, { useState, useContext, useEffect } from 'react';

import {
  Typography,
  Grid,
  TextField,
  Box,
  Button,
  Select,
  MenuItem,
  IconButton,
} from '@material-ui/core';
import { object, string } from 'yup';
import {
  ActionEditable,
  IChildListTree,
  PlaybookPermission,
} from '../../../types';
import {
  Form,
  Formik,
  Field,
  FieldProps,
  FieldArray,
  FieldArrayRenderProps,
} from 'formik';

import { useDisableMultipleClick } from '../../../../../hooks';
import {
  IListItem,
  addPlayBookItemList,
  updatePlayBookItemList,
  IChildrenListItem,
} from '../../../../../services/playbook.services';
import {
  MixTitle,
  MessageDialog,
  CustomContextMenu,
  DeleteDialog,
} from '../../../../../components';
import PlaybookContext from '../../../PlaybookContext';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useSelector, shallowEqual } from 'react-redux';
import { AnalysePermissionState } from '../../../../../store/opportunity/selector';
import { IListType } from '../../../../../services/references.services';

const useStyles = makeStyles(theme => ({
  time: {
    color: theme.palette.grey[600],
    fontSize: 13,
  },
  titleInput: {
    width: '75%',
  },
  icon: {
    fontSize: 18,
  },
  typeInput: {
    width: '20%',
  },
  heightSelect: {
    height: 37,
  },
  overText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    Width: 500,
    maxWidth: 500,
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

interface ListItemProps {
  index: number;
  id: number;
  data: IListItem;
  //   description?: string;
  //   assignee: number;
  //   assigneeName: string;
  //   dueDate?: Date | null;
  expanded?: string;
  setEditable: (value: number) => void;
  editable: number;
  onDelete?: (id: number) => void;
  onChangeCollapse: (current: string) => void;
  listTypes?: IListType[];
}
interface RenderListItemProps {
  // indexFirstLv: number;
  // indexPrev: number;
  index: number;
  setFieldValue: (
    field: string,
    value: any,
    shouldValidate?: boolean | undefined,
  ) => void;
  expanded?: string;
  editMode: boolean;
  handleChange: {
    (e: React.ChangeEvent<any>): void;
    <T = string | React.ChangeEvent<any>>(
      field: T,
    ): T extends React.ChangeEvent<any>
      ? void
      : (e: string | React.ChangeEvent<any>) => void;
  };
  item: IChildListTree;
  setEditable: (value: number) => void;
  editable: number;
  onChangeCollapse: (current: string) => void;
  values: {
    id: number;
    items: IChildListTree[];
  };
  arrayHelpers: FieldArrayRenderProps;
  isSubmitting: boolean;
  onDelete?: (id: number) => void;
  style?: any;
  position: number[];
  nameTemplate: string;
}
const RenderListItem: React.FC<RenderListItemProps> = ({
  setFieldValue,
  expanded,
  editMode,
  handleChange,
  item,
  setEditable,
  editable,
  onChangeCollapse,
  values,
  isSubmitting,
  onDelete,
  position,
  style,
  index,
  arrayHelpers,

  nameTemplate,
}) => {
  const label = position.join('.');
  const { i18n } = useTranslation();
  const [success, setSuccess] = useState(false);

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  return (
    <Box style={style} ml={position.length === 1 ? 0 : position.length}>
      {item.is_deleted === false && (
        <Box
          mt={2}
          display="flex"
          flex="1"
          flexDirection="row"
          justifyContent="center"
          alignItems="center"
          flexWrap="wrap"
        >
          <Box mt={1} mr={1}>
            <MixTitle title={label} />
          </Box>
          <Box flex="1" mt={1}>
            <TextField
              name={`${nameTemplate}.${index}.title`}
              variant="outlined"
              required
              fullWidth
              margin="normal"
              disabled={
                isSubmitting || !editMode || editable !== ActionEditable.List
              }
              value={item.title}
              onChange={handleChange}
            />
          </Box>
          <Box
            ml={1}
            flex="0.05"
            style={
              isSubmitting || !editMode || editable !== ActionEditable.List
                ? { pointerEvents: 'none' }
                : {}
            }
          >
            {position.length < 3 ? (
              <CustomContextMenu
                options={[
                  {
                    name: 'Delete',
                    onClick: () => {
                      let unDeletedItem = item?.children?.filter(e => {
                        return e.is_deleted === false;
                      });
                      if (unDeletedItem && unDeletedItem?.length > 0) {
                        setSuccess(true);
                      } else {
                        setShowDeleteDialog(true);
                      }
                    },
                  },
                  {
                    name: 'Add Sub Item',
                    onClick: () => {
                      setFieldValue(
                        `${nameTemplate}.${index}.children.${item?.children
                          ?.length ?? 0}`,
                        {
                          id: 0,
                          title: '',
                          item_order: position.length === 1 ? 1 : 2,
                          parent_id: item?.id ?? 0,
                          is_deleted: false,
                          children: [],
                        },
                      );
                    },
                  },
                ]}
                size={'small'}
              />
            ) : (
              <CustomContextMenu
                options={[
                  {
                    name: 'Delete',
                    onClick: () => {
                      if (item.id === 0) {
                        arrayHelpers.remove(index);
                      } else {
                        setShowDeleteDialog(true);
                      }
                    },
                  },
                ]}
                size={'small'}
              />
            )}
          </Box>
        </Box>
      )}

      {item.children && item.children?.length > 0 && (
        <Box>
          <FieldArray name={`${nameTemplate}.${index}.children`}>
            {arrayHelpers => (
              <Box>
                {item.children &&
                  item.children?.map((child, indexChild) => (
                    <Box key={indexChild}>
                      <RenderListItem
                        // indexFirstLv={position === 0 ? index : indexFirstLv}
                        // indexPrev={index}
                        index={indexChild}
                        nameTemplate={`${nameTemplate}.${index}.children`}
                        onChangeCollapse={onChangeCollapse}
                        setFieldValue={setFieldValue}
                        handleChange={handleChange}
                        values={values}
                        arrayHelpers={arrayHelpers}
                        expanded={expanded}
                        setEditable={setEditable}
                        editable={editable}
                        isSubmitting={isSubmitting}
                        editMode={editMode}
                        item={child ?? []}
                        position={[...position, indexChild + 1]}
                        // onDelete={handleDeleteListItems}
                      />
                    </Box>
                  ))}
              </Box>
            )}
          </FieldArray>
        </Box>
      )}
      <MessageDialog
        mode="warning"
        message={i18n.t('item_list.delete_list_with_sub')}
        open={success}
        onClose={() => setSuccess(false)}
      />
      <DeleteDialog
        isOpen={showDeleteDialog}
        header={i18n.t('item_list.delete')}
        message={i18n.t('item_list.delete-item-warning')}
        handleClose={() => setShowDeleteDialog(false)}
        handleDelete={() => {
          if (item.id === 0) {
            arrayHelpers.remove(index);
          } else {
            item.is_deleted = true;
            setFieldValue('items', values.items);
          }
          setShowDeleteDialog(false);
        }}
      />
    </Box>
  );
};
export const ListItem: React.FC<ListItemProps> = ({
  index,
  expanded,
  id,
  onChangeCollapse,
  data,
  editable,
  setEditable,
  onDelete,
  listTypes,
}) => {
  const classes = useStyles();
  const { selectedItem, onRefresh } = useContext(PlaybookContext);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [editMode, setEditMode] = useState<boolean>(id === 0);
  const [childListTree, setChildListTree] = useState<IChildListTree[]>([]);
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);

  useEffect(() => {
    if (data.item_list?.length === 1 && !data.item_list[0].id) {
      setChildListTree(data.item_list);
    } else {
      setChildListTree(convertItemTree(data.item_list, 0));
    }
    function convertItemTree(items: IChildrenListItem[], parentId: number) {
      let itemFilter = items?.filter(child => child.parent_id === parentId);
      if (!itemFilter) return [];
      let list: IChildListTree[] = [];

      for (let i = 0; i < itemFilter?.length; i++) {
        list.push({
          ...itemFilter[i],
          children: convertItemTree(items, itemFilter[i]?.id),
        });
      }
      return list;
    }
  }, [data]);
  const validationSchema = () =>
    object().shape({
      title: string().required(),
    });

  const filterListType = (id: number) => {
    const listTypeData = listTypes?.find(item => item.id === id);

    if (listTypeData) {
      return listTypeData.name;
    }
  };
  const animatedStyle = (index: number) => ({
    fontSize: 20,
    transition: '0.3s all',
    transform: `l${index}` === expanded ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'poiter',
    },
  });
  return (
    <>
      <Grid container spacing={1} direction="column">
        <Box ml={2} mb={2}>
          <Grid item container direction="row" alignItems="center">
            <IconButton
              size="small"
              onClick={() => onChangeCollapse(`l${index}`)}
              disabled={
                !analysePermission?.includes(
                  PlaybookPermission.PlaybookItemLinkRead,
                )
              }
            >
              <ArrowForwardIosIcon
                style={animatedStyle(index)}
                className={classes.icon}
              />
            </IconButton>
            <Box display="flex" alignItems="center" mr={2}>
              <Typography variant="h5">
                {filterListType(data?.type?.id ?? 1)}
              </Typography>
              <Box ml={1}>
                <Typography variant="h5" className={classes.overText}>
                  {data.id === 0 ? `List ${index + 1}` : `List - ${data.title}`}
                </Typography>
              </Box>
            </Box>
            <Box display="flex" flex={1} justifyContent="flex-end">
              {data.last_updated_at && data.created_by_user && (
                <Typography className={classes.time}>
                  {`Last updated by ${
                    data.updated_by_user?.name !== ''
                      ? data.updated_by_user?.name
                      : data.created_by_user.name
                  } at ${moment(data.last_updated_at).format(
                    'DD/MM/YYYY HH:mm:ss',
                  )}`}
                </Typography>
              )}
            </Box>
          </Grid>
          <Formik
            initialValues={{
              id: id,
              items: childListTree,
              title: data.title,
              type: data.type?.id,
            }}
            enableReinitialize
            validationSchema={validationSchema}
            onSubmit={async (values, formikHelpers) => {
              if (isSubmitting) {
                return;
              }
              await debounceFn();
              formikHelpers.setSubmitting(true);
              try {
                if (values.id !== 0) {
                  values.items.map(e => {
                    if (!e.id) delete e.id;
                    return e;
                  });
                  // items.name = values.title;
                  let payload = {
                    title: values.title ?? '',
                    list_type_id: values.type ?? 1,
                    item_list: values.items,
                  };
                  let response = await updatePlayBookItemList(
                    payload,
                    data?.id,
                  );
                  if (response.success) {
                    setEditable(0);
                    onRefresh?.();
                  }
                } else {
                  let payload = {
                    title: values.title ?? '',
                    list_type_id: values.type ?? 1,
                    item_list: values.items,
                  };
                  let response = await addPlayBookItemList(
                    payload,
                    selectedItem?.id,
                  );
                  if (response.success) {
                    formikHelpers.setFieldValue('id', -1);
                    setEditable(0);
                    setEditMode(false);
                    onRefresh?.();
                  }
                }
              } catch (e) {
                console.log('Error submitting list', e);
              }
              formikHelpers.setSubmitting(false);
              endRequest();
            }}
            // onReset={(values, formikHelpers) => {
            //   if (values.id === 0 && onDelete) onDelete(0);
            // }}
          >
            {({
              errors,
              touched,
              values,
              setFieldValue,
              handleChange,
              isSubmitting,
              handleReset,
              handleSubmit,
              resetForm,
            }) => (
              <>
                {expanded === `l${index}` && (
                  <Form style={{ minWidth: '100%' }}>
                    <Box width={'100%'} pt={2}>
                      <Grid container direction="column" spacing={3}>
                        <Box display="flex" mt={2} justifyContent="flex-end">
                          {editMode && editable === ActionEditable.List ? (
                            <Box display="flex">
                              <Box mr={2} component="span" display="flex">
                                <Button
                                  variant="outlined"
                                  color="default"
                                  type="button"
                                  onClick={() => {
                                    setEditMode(false);
                                    setEditable(0);
                                    onDelete?.(0);
                                    resetForm();
                                  }}
                                  disabled={isSubmitting}
                                >
                                  Cancel
                                </Button>
                              </Box>
                              <Button
                                variant="contained"
                                color="primary"
                                type="button"
                                disabled={isSubmitting}
                                onClick={() => handleSubmit()}
                              >
                                Save
                              </Button>
                            </Box>
                          ) : (
                            <Box display="flex">
                              <Box mr={2} component="span" display="flex">
                                <Button
                                  variant="outlined"
                                  color="secondary"
                                  onClick={() => onDelete?.(values.id)}
                                  disabled={
                                    !analysePermission?.includes(
                                      PlaybookPermission.PlaybookItemListDelete,
                                    )
                                  }
                                >
                                  Remove
                                </Button>
                              </Box>
                              <Button
                                color="primary"
                                variant="outlined"
                                disabled={
                                  !analysePermission?.includes(
                                    PlaybookPermission.PlaybookItemListUpdate,
                                  )
                                }
                                onClick={() => {
                                  setEditable(ActionEditable.List);
                                  setEditMode(true);
                                }}
                                type="button"
                              >
                                Edit
                              </Button>
                            </Box>
                          )}
                        </Box>
                        <Grid item>
                          <Box
                            display="flex"
                            flex={1}
                            justifyContent="space-between"
                          >
                            <Box className={classes.titleInput}>
                              <MixTitle title={'Title'} isRequired />
                              <Box display="flex" flex={1}>
                                <Field name="title">
                                  {({ field }: { field: FieldProps }) => (
                                    <TextField
                                      fullWidth
                                      name={'title'}
                                      variant="outlined"
                                      required
                                      margin="normal"
                                      {...field}
                                      disabled={
                                        isSubmitting ||
                                        !editMode ||
                                        editable !== ActionEditable.List
                                      }
                                      value={values.title}
                                      error={touched.title && !!errors?.title}
                                      // helperText={!!errors.title ? errors.title : ''}
                                      onChange={handleChange}
                                    />
                                  )}
                                </Field>
                              </Box>
                            </Box>
                            <Box className={classes.typeInput}>
                              <MixTitle title={'Category'} isRequired />
                              <Box display="flex" flex={1}>
                                <Field name="type">
                                  {({ field }: { field: FieldProps }) => (
                                    <Select
                                      fullWidth
                                      name="type"
                                      id="type"
                                      variant="outlined"
                                      {...field}
                                      disabled={
                                        isSubmitting ||
                                        !editMode ||
                                        editable !== ActionEditable.List
                                      }
                                      value={values.type}
                                      IconComponent={ExpandMoreIcon}
                                      MenuProps={{
                                        anchorOrigin: {
                                          vertical: 'bottom',
                                          horizontal: 'left',
                                        },
                                        transformOrigin: {
                                          vertical: 'top',
                                          horizontal: 'left',
                                        },
                                        getContentAnchorEl: null,
                                      }}
                                    >
                                      {listTypes?.map(listType => (
                                        <MenuItem
                                          key={listType.id}
                                          value={listType.id}
                                        >
                                          {listType.name}
                                        </MenuItem>
                                      ))}
                                    </Select>
                                  )}
                                </Field>
                              </Box>
                            </Box>
                          </Box>
                          <FieldArray name={'items'}>
                            {arrayHelpers => (
                              <Box>
                                <Box mt={2}>
                                  <MixTitle title={'Items list'} />
                                  {values.items.map((item, indexItem) => (
                                    <Box key={indexItem}>
                                      <RenderListItem
                                        // indexFirstLv={0}
                                        // indexPrev={0}
                                        index={indexItem}
                                        nameTemplate={'items'}
                                        onChangeCollapse={onChangeCollapse}
                                        setFieldValue={setFieldValue}
                                        handleChange={handleChange}
                                        values={values}
                                        expanded={expanded}
                                        arrayHelpers={arrayHelpers}
                                        isSubmitting={isSubmitting}
                                        setEditable={setEditable}
                                        editMode={editMode}
                                        editable={editable}
                                        item={item}
                                        position={[indexItem + 1]}
                                        // onDelete={handleDeleteListItems}
                                      />
                                    </Box>
                                  ))}
                                </Box>
                                <Box
                                  pb={2}
                                  display="flex"
                                  justifyContent="flex-start"
                                  mt={2}
                                >
                                  <Button
                                    disabled={
                                      isSubmitting ||
                                      !editMode ||
                                      editable !== ActionEditable.List
                                    }
                                    onClick={() => {
                                      arrayHelpers.push({
                                        id: 0,
                                        name: '',
                                        item_order: 0,
                                        parent_id: values.id,
                                        is_deleted: false,
                                        children: [],
                                      });
                                      // setFieldValue('items', values.items);
                                    }}
                                    color="primary"
                                    variant="contained"
                                  >
                                    Add Item
                                  </Button>
                                </Box>
                              </Box>
                            )}
                          </FieldArray>
                        </Grid>
                      </Grid>
                    </Box>
                  </Form>
                )}
              </>
            )}
          </Formik>
        </Box>
      </Grid>
    </>
  );
};
