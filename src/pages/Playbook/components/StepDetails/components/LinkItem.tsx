import React, {
  // useMemo,
  useState,
  useContext,
  useMemo,
} from 'react';
import * as Yup from 'yup';
import {
  Formik,
  FieldArray,
  FieldArrayRenderProps,
  FormikErrors,
  FormikTouched,
} from 'formik';

import {
  ILink,
  updateLink,
  addLink,
  IResponseLink,
} from '../../../../../services/link';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

import PlaybookContext from '../../../PlaybookContext';
import { ActionEditable, PlaybookPermission } from '../../../types';
import {
  MixTitle,
  CustomContextMenu,
  DeleteDialog,
} from '../../../../../components';
import {
  Typography,
  Box,
  Button,
  TextField,
  Grid,
  IconButton,
} from '@material-ui/core';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import InputAdornment from '@material-ui/core/InputAdornment';
import LinkIcon from '@material-ui/icons/Link';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { useSelector, shallowEqual } from 'react-redux';
import { AnalysePermissionState } from '../../../../../store/opportunity/selector';

interface LinkItemProps {
  index: number;
  data: IResponseLink;

  onDelete?: (id: number) => void;
  expanded?: string;
  setEditable: (value: number) => void;
  editable: number;
  onChangeCollapse: (current: string) => void;
}
const useStyles = makeStyles(theme => ({
  overText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    Width: 500,
    maxWidth: 500,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  divider: {
    backgroundColor: '#bbb',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  shadowBox: {
    border: '1px solid #ccc',
    boxShadow: '5px 8px #eee',
    borderRadius: 5,
    // maxWidth: '10%',
  },
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  mr43: {
    marginRight: 43,
  },
  time: {
    color: theme.palette.grey[600],
    fontSize: 13,
  },
}));

export const LinkItem: React.FC<LinkItemProps> = ({
  index,
  data,
  expanded,
  setEditable,
  editable,
  onChangeCollapse,
  onDelete,
}) => {
  const { selectedItem, onRefresh } = useContext(PlaybookContext);
  const [editMode, setEditMode] = useState<boolean>(data.id === 0);
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);
  const { i18n } = useTranslation();
  const classes = useStyles();

  const validationScheme = useMemo(
    () =>
      Yup.object().shape({
        title: Yup.string()
          .trim()
          .required(),
        template: Yup.array().of(
          Yup.object()
            .shape({
              name: Yup.string()
                .trim()
                .required(),
              url: Yup.string()
                .trim()
                .url()
                .required(),
              // Rest of your amenities object properties
            })
            .required('Must have friends'),
        ),
      }),
    [],
  );
  type ILinkForm = ILink;
  interface IForm {
    title: string;
    id: number;
    template: ILinkForm[];
    isOpenCollapse: number | undefined;
  }
  const initDataForm: IForm = {
    template: data.item_link ?? [],
    title: data.title,
    id: data.id,
    isOpenCollapse: undefined,
  };
  const renderForm = (
    item: ILinkForm,
    setFieldValue: (
      field: string,
      value: any,
      shouldValidate?: boolean | undefined,
    ) => void,
    handleChange: (e: string | React.ChangeEvent<any>) => void,
    disabled: boolean,
    index: number,
    arrayHelpers: FieldArrayRenderProps,
    touched: FormikTouched<ILink>[],
    errors: FormikErrors<ILink>[],
    isOpenCollapse?: number,
  ) => {
    const animatedStyle = {
      marginRight: '5px',
      transition: '0.3s all',
      transform: isOpenCollapse === index ? 'rotate(90deg)' : 'rotate(0deg)',
      '&:hover': {
        cursor: 'poiter',
      },
    };
    if (item.is_deleted) {
      return <></>;
    }
    return (
      <Box mt={2} key={index}>
        <Box display="flex" flexDirection="row" alignItems="center">
          <Box minWidth={85} display="flex">
            <MixTitle title={`Link ${index + 1}`} />
            <Box ml={1} display="flex" alignItems="center" mb={0.7}>
              <ArrowForwardIosIcon
                style={animatedStyle}
                className={classes.icon}
                fontSize="small"
                onClick={() => {
                  if (isOpenCollapse === index) {
                    setFieldValue('isOpenCollapse', undefined);
                  } else {
                    setFieldValue('isOpenCollapse', index);
                  }
                }}
              />
            </Box>
          </Box>

          <Box
            ml={1}
            className={classes.mr43}
            display="flex"
            justifyContent="center"
          >
            <MixTitle title={`Name`} isRequired />
          </Box>
          <TextField
            variant="outlined"
            fullWidth
            name={`template[${index}].name`}
            value={item.name}
            onChange={handleChange}
            disabled={disabled}
            error={touched?.[index]?.name && !!errors?.[index]?.name}
            // helperText={!!errors?.[index]?.name ? errors?.[index]?.name : ''}
          />

          <Box
            display="flex"
            flex={1}
            style={disabled ? { pointerEvents: 'none' } : {}}
          >
            <CustomContextMenu
              options={[
                {
                  name: 'Delete',

                  onClick: () => {
                    setShowDeleteDialog(true);
                  },
                },
              ]}
              size={'small'}
            />
          </Box>
        </Box>
        <Box ml={10}>
          {((!!errors?.[index] && touched?.[index]) ||
            isOpenCollapse === index) && (
            <Box display="flex" flex={1} flexDirection="column">
              <Box display="flex" flexDirection="row" mt={2}>
                <Box ml={1} mr={5} display="flex">
                  <MixTitle title={`URL`} isRequired />
                </Box>
                <TextField
                  variant="outlined"
                  fullWidth
                  name={`template.${index}.url`}
                  value={item.url}
                  onChange={handleChange}
                  style={{ flex: 0.94 }}
                  disabled={disabled}
                  error={touched?.[index]?.url && !!errors?.[index]?.url}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <a
                          href={item.url}
                          rel="noopener noreferrer"
                          target="_blank"
                          style={disabled ? {} : { pointerEvents: 'none' }}
                        >
                          <LinkIcon />
                        </a>
                      </InputAdornment>
                    ),
                  }}
                  // helperText={
                  //   !!errors?.[index]?.url ? errors?.[index]?.url : ''
                  // }
                />
              </Box>
              <Box display="flex" flexDirection="row" mt={2}>
                <Box ml={1} mr={2} display="flex">
                  <MixTitle title={`Description`} />
                </Box>
                <TextField
                  variant="outlined"
                  fullWidth
                  rows={2}
                  multiline
                  name={`template.${index}.description`}
                  value={item.description}
                  onChange={handleChange}
                  style={{ flex: 0.94 }}
                  disabled={disabled}
                />
              </Box>
            </Box>
          )}
        </Box>
        <DeleteDialog
          isOpen={showDeleteDialog}
          header={i18n.t('link.delete')}
          message={i18n.t('link.delete-warning')}
          handleClose={() => setShowDeleteDialog(false)}
          handleDelete={() => {
            if (item.id === 0) {
              arrayHelpers.remove(index);
            } else {
              setFieldValue(`template[${index}].is_deleted`, true);
            }
            setShowDeleteDialog(false);
          }}
        />
      </Box>
    );
  };
  const animatedStyle = (index: number) => ({
    fontSize: 20,
    transition: '0.3s all',
    transform: `link${index}` === expanded ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'poiter',
    },
  });
  return (
    <Grid container spacing={1} direction="column">
      <Box ml={2} mb={2}>
        <Grid item container direction="row" alignItems="center">
          <IconButton
            size="small"
            onClick={() => onChangeCollapse(`link${index}`)}
            disabled={
              !analysePermission?.includes(
                PlaybookPermission.PlaybookItemLinkRead,
              )
            }
          >
            <ArrowForwardIosIcon
              style={animatedStyle(index)}
              className={classes.icon}
            />
          </IconButton>
          <Box display="flex" alignItems="center" mr={2}>
            <Typography variant="h5" className={classes.overText}>
              {data.id === 0 ? `Links ${index + 1}` : `Links - ${data.title}`}
            </Typography>
          </Box>
          <Box display="flex" flex={1} justifyContent="flex-end">
            {data.last_updated_at && data.created_by_user && (
              <Typography className={classes.time}>
                {`Last updated by ${
                  data.updated_by_user?.name !== ''
                    ? data.updated_by_user?.name
                    : data.created_by_user.name
                } at ${moment(data.last_updated_at).format(
                  'DD/MM/YYYY HH:mm:ss',
                )}`}
              </Typography>
            )}
          </Box>
        </Grid>
        <Formik
          initialValues={initDataForm}
          validationSchema={validationScheme}
          enableReinitialize
          onSubmit={async (values, formikHelpers) => {
            if (!selectedItem) return;
            formikHelpers.setSubmitting(true);
            try {
              if (values.id > 0) {
                let response = await updateLink(data.id, {
                  title: values.title,
                  opportunity_playbook_item_id: selectedItem.id,
                  links: values.template,
                });
                if (response.success) {
                  formikHelpers.setFieldValue('disabled', true);
                }
              } else {
                await addLink({
                  title: values.title,
                  // parent_id: 0,
                  // entity_type: data.entity_type,
                  // entity_id: data.entity_id,
                  opportunity_playbook_item_id: selectedItem.id,
                  links: values.template,
                });
              }
              setEditMode(false);
              setEditable(0);
              onRefresh?.();
            } catch (e) {
              console.log('Error submitting finding', e);
            }
            formikHelpers.setSubmitting(false);
          }}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            errors,
            touched,
            isSubmitting,
            setFieldValue,
            isValid,
            dirty,
            resetForm,
          }) => (
            <>
              {expanded === `link${index}` && (
                <Box width={'100%'} pt={2}>
                  <Box display="flex" justifyContent="flex-end">
                    {editMode && editable === ActionEditable.Link ? (
                      <Box display="flex">
                        <Box mr={2} component="span" display="flex">
                          <Button
                            variant="outlined"
                            color="default"
                            type="button"
                            onClick={() => {
                              setEditMode(false);
                              setEditable(0);
                              onDelete?.(0);
                              resetForm();
                            }}
                            disabled={isSubmitting}
                          >
                            Cancel
                          </Button>
                        </Box>
                        <Button
                          variant="contained"
                          color="primary"
                          type="button"
                          disabled={isSubmitting}
                          onClick={() => handleSubmit()}
                        >
                          Save
                        </Button>
                      </Box>
                    ) : (
                      <Box display="flex">
                        <Box mr={2} component="span" display="flex">
                          <Button
                            variant="outlined"
                            color="secondary"
                            onClick={() => onDelete?.(values.id)}
                            disabled={
                              !analysePermission?.includes(
                                PlaybookPermission.PlaybookItemLinkDelete,
                              )
                            }
                          >
                            Remove
                          </Button>
                        </Box>
                        <Button
                          color="primary"
                          variant="outlined"
                          disabled={
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemLinkUpdate,
                            )
                          }
                          onClick={() => {
                            setEditable(ActionEditable.Link);
                            setEditMode(true);
                          }}
                          type="button"
                        >
                          Edit
                        </Button>
                      </Box>
                    )}
                  </Box>

                  <form onSubmit={handleSubmit} key={data.id}>
                    <Box mb={2}>
                      <MixTitle title="Title" isRequired />
                      <TextField
                        value={values.title}
                        name="title"
                        fullWidth
                        onChange={handleChange}
                        variant="outlined"
                        required
                        disabled={
                          isSubmitting ||
                          !editMode ||
                          editable !== ActionEditable.Link
                        }
                        error={touched.title && !!errors.title}
                        // helperText={
                        //   touched.title && errors.title ? errors.title : ''
                        // }
                      />
                    </Box>
                    <FieldArray name="template" key={data.id}>
                      {arrayHelpers => (
                        <Box>
                          {values.template &&
                            values.template.length > 0 &&
                            values.template.map((item, index) =>
                              renderForm(
                                item,
                                setFieldValue,
                                handleChange,
                                isSubmitting ||
                                  !editMode ||
                                  editable !== ActionEditable.Link,
                                index,
                                arrayHelpers,
                                touched.template as FormikTouched<ILink>[],
                                errors.template as FormikErrors<ILink>[],
                                values.isOpenCollapse,
                              ),
                            )}
                          <Box
                            display="flex"
                            justifyContent="flex-start"
                            mt={4}
                            mb={2}
                          >
                            <Button
                              variant="contained"
                              color="primary"
                              startIcon={<ControlPointIcon />}
                              onClick={() => {
                                arrayHelpers.push({
                                  name: '',
                                  url: '',
                                  id: 0,
                                  item_order: values.template.length ?? 1,
                                });
                              }}
                              disabled={
                                isSubmitting ||
                                !editMode ||
                                editable !== ActionEditable.Link
                              }
                            >
                              Add Link
                            </Button>
                          </Box>
                        </Box>
                      )}
                    </FieldArray>
                  </form>
                </Box>
              )}
            </>
          )}
        </Formik>
      </Box>
    </Grid>
  );
};
