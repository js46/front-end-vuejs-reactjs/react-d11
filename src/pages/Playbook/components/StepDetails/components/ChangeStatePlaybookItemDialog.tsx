import React, { useContext } from 'react';
import { Box } from '@material-ui/core';
import { Formik, FormikHelpers } from 'formik';
// import { makeStyles } from '@material-ui/styles';
import PlaybookContext from '../../../../../pages/Playbook/PlaybookContext';
import { useDisableMultipleClick } from '../../../../../hooks';
import { useDispatch } from 'react-redux';
import { CustomSelect } from '../../../../../components/CustomSelect';
import {
  RequestChangeStatePbItem,
  CloseChangeStateDialogPb,
} from '../../../../../store/playbook/actions';
import { FormDialog } from '../../../../../components';

// const useStyles = makeStyles(theme => ({}));
const listState = [
  {
    id: 1,
    name: 'To do',
  },
  {
    id: 2,
    name: 'In Progress',
  },
  {
    id: 3,
    name: 'Done',
  },
];

interface InitValueT {
  playbook_item_state_id: number;
}
const ChangeStatePlaybookItemDialog: React.FC = () => {
  //   const classes = useStyles();
  const dispatch = useDispatch();
  const { selectedItem } = useContext(PlaybookContext);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const selectedItemState = listState.find(
    e => e.id === selectedItem?.playbook_item_state?.id,
  );
  const initialValues: InitValueT = {
    playbook_item_state_id: selectedItemState?.id ?? 1,
  };
  const handleChangeStatePbItem = async (
    idState: number,
    formilHelper: FormikHelpers<InitValueT>,
  ) => {
    if (isSubmitting) {
      return;
    }
    const payload = {
      playbook_item_state_id: idState,
    };
    await debounceFn();
    dispatch(RequestChangeStatePbItem(payload));
    endRequest();
  };
  const onCancel = () => {
    dispatch(CloseChangeStateDialogPb());
  };
  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={(values, formilHelper) => {
        handleChangeStatePbItem(values.playbook_item_state_id, formilHelper);
      }}
    >
      {({ values, handleChange, setFieldValue, handleSubmit }) => (
        <FormDialog
          title="Change state"
          open={true}
          setOpen={onCancel}
          submit={handleSubmit}
          isAddingForm={false}
          disable={false}
          formContent={
            <Box mb={2} mt={2}>
              <CustomSelect
                fullWidth
                id="playbook_item_state_id"
                name="playbook_item_state_id"
                value={values.playbook_item_state_id}
                onChange={handleChange}
                options={
                  listState?.map(item => ({
                    id: item.id!,
                    name: item.name!,
                  })) || []
                }
              />
            </Box>
          }
        />
      )}
    </Formik>
  );
};
export default ChangeStatePlaybookItemDialog;
