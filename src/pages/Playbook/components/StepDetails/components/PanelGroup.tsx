import React, { useCallback, useContext, useEffect, useState } from 'react';
import { FindingItem } from './FindingItem';
import { ListItem } from './ListItem';
import {
  deletePlaybookItemFinding,
  deletePlayBookItemList,
  PlaybookItemFindingT,
  IListItem,
} from '../../../../../services/playbook.services';
import { useSelector } from 'react-redux';
import { deleteLink, IResponseLink } from '../../../../../services/link';
import {
  IOpportunityTask,
  deleteTask,
} from '../../../../../services/task.services';
import {
  getListTypes,
  IListType,
} from '../../../../../services/references.services';
import {
  DeleteDialog,
  CustomContextMenuOption,
} from '../../../../../components';
import PlaybookContext from '../../../PlaybookContext';
import { TaskDialog } from './Task/TaskDialog';
import { useTranslation } from 'react-i18next';
import { UserID } from '../../../../../store/user/selector';
import {
  PlaybookItemTaskId,
  PlaybookItemIssueId,
} from '../../../../../store/playbook/selector';
import { LinkItem } from './LinkItem';
import { IIssueItems } from '../../../../../services/issue';
import { useEffectOnlyOnce } from '../../../../../hooks';
import { IssueDialog } from './IssueMenu/IssueDialog';
import { Box } from '@material-ui/core';

interface PanelGroupProps {
  findings: PlaybookItemFindingT[];
  links: IResponseLink[];
  disableHandle: boolean;
  setFinding: (findings: PlaybookItemFindingT[]) => void;
  tasks: IOpportunityTask[];
  setTasks: (tasks: IOpportunityTask[]) => void;
  setLinks: (items: IResponseLink[]) => void;
  onClickAdd: (type: 'list' | 'link' | 'task' | 'finding' | 'issue') => void;
  editable: number;
  setEditable: (editable: number) => void;
  listItems: IListItem[];
  setListItems: (listItems: IListItem[]) => void;
  issueItem: IIssueItems[];
  setIssue: (issue: IIssueItems[]) => void;
  indexSidebarTab: number;
  setIndexSidebarTab: React.Dispatch<React.SetStateAction<number>>;
  actions: CustomContextMenuOption[];
  openTaskDialog: boolean;
  setOpenTaskDialog: React.Dispatch<React.SetStateAction<boolean>>;
  openIssueDialog: boolean;
  setOpenIssueDialog: React.Dispatch<React.SetStateAction<boolean>>;
}

export const PanelGroup: React.FC<PanelGroupProps> = ({
  findings,
  tasks,
  onClickAdd,
  setTasks,
  setFinding,
  setEditable,
  links,
  editable,
  listItems,
  setLinks,
  setListItems,
  issueItem,
  setIssue,
  indexSidebarTab,
  setIndexSidebarTab,
  disableHandle,
  actions,
  openIssueDialog,
  setOpenIssueDialog,
  openTaskDialog,
  setOpenTaskDialog,
}) => {
  const { t } = useTranslation();
  const { onRefresh } = useContext(PlaybookContext);
  const [expanded, setExpanded] = useState<string | undefined>();
  const [deleteFinding, setDeleteFinding] = useState<number | undefined>();
  const [deleteLinkID, setDeleteLinkID] = useState<number | undefined>();
  const [deleteTaskId, setDeleteTaskId] = useState<number | undefined>();
  const currentUserID = useSelector(UserID);
  const selectedTaskID = useSelector(PlaybookItemTaskId);
  const selectedIssueID = useSelector(PlaybookItemIssueId);
  const [deleteListItemsId, setDeleteListItemsId] = useState<
    number | undefined
  >();
  const [listTypes, setListTypes] = useState<IListType[]>([]);

  const selectedTask = tasks.find(e => {
    return e.id === selectedTaskID;
  });
  const selectedIssue = issueItem.find(e => {
    return e.id === selectedIssueID;
  });

  useEffect(() => {
    const latest = findings.findIndex(item => item.id === 0);
    const latestListItem = listItems.findIndex(item => item.id === 0);
    const latestLinkItem = links.findIndex(item => item.id === 0);
    if (latest >= 0) setExpanded(`f${latest}`);
    else if (latestListItem >= 0) setExpanded(`l${latestListItem}`);
    else if (latestLinkItem >= 0) setExpanded(`link${latestLinkItem}`);
  }, [
    findings,
    indexSidebarTab,
    issueItem,
    links,
    listItems,

    setOpenIssueDialog,
    setOpenTaskDialog,
    tasks,
  ]);

  useEffect(() => {
    if (!openTaskDialog) {
      setEditable(0);
    }
    if (!openIssueDialog) {
      setEditable(0);
    }
  }, [onRefresh, openIssueDialog, openTaskDialog, setEditable]);
  const onChangeCollapse = (name: string) => {
    if (editable) return;
    setExpanded(prevState => (name === prevState ? undefined : name));
  };

  const handleDeleteTask = useCallback(
    (id: number) => {
      if (id === undefined) return;
      if (id === 0) {
        return setTasks(tasks.filter(item => item.id !== 0));
      }
      setDeleteTaskId(id);
    },
    [setTasks, tasks],
  );
  const onDeleteTask = useCallback(async () => {
    if (deleteTaskId === undefined) return;
    try {
      await deleteTask(deleteTaskId);
      onRefresh?.();
    } catch (e) {
      console.log('Error deleting finding', e);
    }
    setDeleteTaskId(undefined);
  }, [deleteTaskId, onRefresh]);
  const onDeleteLinkItems = useCallback(async () => {
    if (deleteLinkID === undefined) return;
    try {
      await deleteLink(deleteLinkID);
      onRefresh?.();
    } catch (e) {
      console.log('Error deleting finding', e);
    }
    setDeleteLinkID(undefined);
  }, [deleteLinkID, onRefresh]);
  const handleDeleteFinding = useCallback(
    (id: number) => {
      if (id === undefined) return;
      if (id === 0) {
        return setFinding(findings.filter(item => item.id !== 0));
      }
      setDeleteFinding(id);
    },
    [findings, setFinding],
  );

  const onDeleteFinding = useCallback(async () => {
    if (deleteFinding === undefined) return;
    try {
      await deletePlaybookItemFinding(deleteFinding);
      onRefresh?.();
    } catch (e) {
      console.log('Error deleting finding', e);
    }
    setDeleteFinding(undefined);
  }, [deleteFinding, onRefresh]);
  const handleDeleteListItems = useCallback(
    (id: number | undefined) => {
      if (id === undefined) return;
      if (id === 0) {
        return setListItems(listItems.filter(item => item.id !== 0));
      }
      setDeleteListItemsId(id);
    },
    [listItems, setListItems],
  );

  const onDeleteListItems = useCallback(async () => {
    if (deleteListItemsId === undefined) return;
    try {
      await deletePlayBookItemList(deleteListItemsId);
      onRefresh?.();
    } catch (e) {
      console.log('Error deleting finding', e);
    }
    setDeleteListItemsId(undefined);
  }, [deleteListItemsId, onRefresh]);

  const handleDeleteLink = useCallback(
    (id: number) => {
      if (id === undefined) return;
      if (id === 0) {
        return setLinks(links.filter(item => item.id !== 0));
      }
      setDeleteLinkID(id);
    },
    [links, setLinks],
  );

  const getListTypeData = async () => {
    const response = await getListTypes();

    if (response.success) {
      setListTypes(response.data.list);
    }
  };

  useEffectOnlyOnce(() => {
    getListTypeData();
  });

  return (
    <>
      {disableHandle ? (
        <></>
      ) : (
        <Box mt={2}>
          {findings.map((finding, index) => (
            <React.Fragment key={finding.id}>
              <FindingItem
                index={index}
                expanded={expanded}
                setEditable={setEditable}
                editable={editable}
                onChangeCollapse={onChangeCollapse}
                item={finding}
                userID={currentUserID}
                onDelete={handleDeleteFinding}
              />
            </React.Fragment>
          ))}
          {listItems.map((listItem, index) => (
            <React.Fragment key={index}>
              <ListItem
                index={index}
                id={listItem.id}
                onChangeCollapse={onChangeCollapse}
                expanded={expanded}
                setEditable={setEditable}
                editable={editable}
                data={listItem}
                onDelete={handleDeleteListItems}
                listTypes={listTypes}
              />
            </React.Fragment>
          ))}
          {links.map((item, index) => (
            <React.Fragment key={index}>
              <LinkItem
                index={index}
                data={item}
                children={item.item_link ?? []}
                onChangeCollapse={onChangeCollapse}
                expanded={expanded}
                setEditable={setEditable}
                editable={editable}
                onDelete={handleDeleteLink}
              />
            </React.Fragment>
          ))}
        </Box>
      )}
      {/* Start task */}
      {openTaskDialog && !!tasks && (
        <TaskDialog
          open={openTaskDialog}
          task={selectedTask ?? tasks[tasks.length - 1]}
          setOpen={setOpenTaskDialog}
          editable={editable}
          setEditable={setEditable}
          onDelete={handleDeleteTask}
        />
      )}
      {openIssueDialog && !!issueItem && (
        <IssueDialog
          open={openIssueDialog}
          issue={selectedIssue ?? issueItem[issueItem.length - 1]}
          setOpen={setOpenIssueDialog}
          editable={editable}
          setEditable={setEditable}
          // onDelete={handleDeleteTask}
        />
      )}

      {/* Start Delete Finding dialog */}
      <DeleteDialog
        isOpen={!!deleteFinding}
        header={t('finding.delete')}
        message={t('finding.delete-warning')}
        handleClose={() => setDeleteFinding(undefined)}
        handleDelete={onDeleteFinding}
      />
      {/* End Delete Finding dialog */}
      <DeleteDialog
        isOpen={!!deleteTaskId}
        header={t('task.delete')}
        message={t('task.delete-warning')}
        handleClose={() => setDeleteTaskId(undefined)}
        handleDelete={onDeleteTask}
      />
      <DeleteDialog
        isOpen={!!deleteListItemsId}
        header={t('item_list.delete')}
        message={t('item_list.delete-warning')}
        handleClose={() => setDeleteListItemsId(undefined)}
        handleDelete={onDeleteListItems}
      />
      <DeleteDialog
        isOpen={!!deleteLinkID}
        header={t('link.delete')}
        message={t('link.delete-warning')}
        handleClose={() => setDeleteLinkID(undefined)}
        handleDelete={onDeleteLinkItems}
      />
    </>
  );
};
