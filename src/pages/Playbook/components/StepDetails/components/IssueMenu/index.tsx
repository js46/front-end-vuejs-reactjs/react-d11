import React from 'react';
import { Box, Button, MenuItem, Tooltip, Chip } from '@material-ui/core';
import { StyledMenu } from '../../../../../../components/styled';
import { useTranslation } from 'react-i18next';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import { IIssueItems } from '../../../../../../services/issue';
import Fade from '@material-ui/core/Fade';
import { GetPlaybookItemIssueIndex } from '../../../../../../store/playbook/actions';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  chipOpen: {
    background: '#4EE24E',
    marginLeft: 8,
  },
  chipClose: {
    background: '#E1E1E1',
    marginLeft: 8,
  },
  button: {
    minWidth: 40,
    '&:hover': {
      textDecorationLine: 'underline',
    },
  },
}));

type IssueMenuProps = {
  issues: IIssueItems[];
  setIssue: React.Dispatch<React.SetStateAction<IIssueItems[]>>;
  size?: 'small' | 'large';
  onClickAdd: (type: 'list' | 'link' | 'task' | 'finding' | 'issue') => void;
  disabled: boolean;
  setOpenIssueDialog: React.Dispatch<React.SetStateAction<boolean>>;
};

export const IssueMenu: React.FC<IssueMenuProps> = props => {
  const classes = useStyles();
  const { issues, size, onClickAdd, disabled, setOpenIssueDialog } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  return (
    <Box>
      <Tooltip title={t('playbook_step_item.issue')}>
        <Button
          aria-controls="fade-menu"
          aria-haspopup="true"
          onClick={(event: any) => setAnchorEl(event.target)}
          disabled={disabled}
          className={classes.button}
          startIcon={
            <ReportProblemOutlinedIcon color="primary" fontSize={size} />
          }
        >
          Issues
        </Button>
      </Tooltip>

      <StyledMenu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => {
          setAnchorEl(null);
        }}
        TransitionComponent={Fade}
      >
        {issues
          .filter(is => is.id !== 0)
          .map((issue, index) => (
            <MenuItem
              key={index}
              onClick={() => {
                setAnchorEl(null);
                setOpenIssueDialog(true);
                dispatch(GetPlaybookItemIssueIndex(issue.id));
              }}
              disabled={false}
            >
              <Box
                display="flex"
                flex={1}
                justifyContent="space-between"
                alignItems="center"
              >
                {issue.title.length > 40
                  ? `${issue.title.slice(0, 40)}...`
                  : issue.title}
                <Chip
                  label={issue.state}
                  size="small"
                  className={
                    issue.state === 'open'
                      ? classes.chipOpen
                      : classes.chipClose
                  }
                />
              </Box>
            </MenuItem>
          ))}
        <MenuItem value="new_playbook">
          <Button
            startIcon={<AddCircleOutlineIcon />}
            onClick={() => {
              setAnchorEl(null);
              onClickAdd('issue');
              setOpenIssueDialog(true);
              dispatch(GetPlaybookItemIssueIndex(0));
            }}
          >
            New Issue
          </Button>
        </MenuItem>
      </StyledMenu>
    </Box>
  );
};
