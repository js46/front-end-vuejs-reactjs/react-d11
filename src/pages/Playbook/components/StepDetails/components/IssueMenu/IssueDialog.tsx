import React, { useContext, useState } from 'react';
import {
  Grid,
  TextField,
  Box,
  Typography,
  Select,
  MenuItem,
} from '@material-ui/core';
import { GetPlaybookItemTaskIndex } from '../../../../../../store/playbook/actions';
import { MixTitle, FormDialog } from '../../../../../../components';
import { useDispatch } from 'react-redux';
import { useDisableMultipleClick } from '../../../../../../hooks';
import { Field, FieldProps, Formik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import { IIssueItems } from '../../../../../../services/issue';
import {
  addIssue,
  updateIssue,
} from '../../../../../../services/issue/issue.service';
import PlaybookContext from '../../../../PlaybookContext';
import { useSelector, shallowEqual } from 'react-redux';
import { AnalysePermissionState } from '../../../../../../store/opportunity/selector';
import { PlaybookPermission } from '../../../../../../pages/Playbook/types';
import { addOpportunityComment } from '../../../../../../services/comment.services';
import IssueComment from './IssueComment';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const STATES = [
  {
    key: 'open',
    name: 'Open',
  },
  {
    key: 'close',
    name: 'Close',
  },
];

interface IIssueDialogProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  issue: IIssueItems;
  editable: number;
  setEditable: (value: number) => void;
  onDelete?: (id: number) => void;
}

export const IssueDialog: React.FC<IIssueDialogProps> = props => {
  const dispatch = useDispatch();
  const { setOpen, issue, setEditable } = props;
  // const [comment, setComment] = useState('');
  // const [editMode, setEditMode] = useState<boolean>(issue.id === 0);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const { selectedItem, onRefresh } = useContext(PlaybookContext);
  const [comment, setComment] = useState('');
  const [lastRefresh, setLastRefresh] = useState<number>(0);

  const handleSubmitComment = async () => {
    if (isSubmitting) return;
    await debounceFn();
    await onSubmit(comment);
    endRequest();
  };
  const onSubmit = async (comment: string) => {
    if (comment) {
      try {
        const response = await addOpportunityComment({
          entity_id: issue?.id,
          entity_type: 'playbook_issue',
          comment_type: 'ASSIGNED',
          comment_body: comment,
          comment_parent_id: 0,
        });
        if (response.success) {
          setTimeout(() => setLastRefresh(new Date().getTime()), 1000);
          setComment('');
        }
      } catch (err) {
        console.log(err);
      }
      return;
    }
  };
  const useStyles = makeStyles(theme => ({
    time: {
      color: theme.palette.grey[600],
      fontSize: 13,
    },
    gridContainer: {
      flexWrap: 'unset',
    },
    tab: {
      maxWidth: '10%',
    },
    issueStatus: {
      fontWeight: 'bold',
    },
  }));
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);
  const classes = useStyles();

  return (
    <Formik
      initialValues={{
        id: issue.id,
        title: issue.title,
        description: issue.description,
        state: issue.state,
      }}
      enableReinitialize
      onSubmit={async (values, formikHelpers) => {
        if (isSubmitting) {
          return;
        }
        await debounceFn();
        formikHelpers.setSubmitting(true);
        try {
          if (values.id > 0) {
            let response = await updateIssue(values.id, {
              title: values.title,
              description: values.description || '',
              state: values.state,
            });
            if (response.success) {
              setEditable(0);
              dispatch(GetPlaybookItemTaskIndex(0));
              setOpen(false);
              onRefresh?.();
            }
          } else {
            let response = await addIssue({
              entity_id: selectedItem?.id ?? 0,
              entity_type: 'opportunity_playbook_item',
              title: values.title,
              description: values.description || '',
              state: 'open',
            });
            if (response.success) {
              setEditable(0);
              setOpen(false);
              onRefresh?.();
            }
          }
        } catch (e) {
          console.log('Error submitting finding', e);
        }
        formikHelpers.setSubmitting(false);
        endRequest();
        props.setOpen(false);
      }}
    >
      {({ errors, touched, values, handleChange, handleSubmit }) => (
        <FormDialog
          maxWidth="md"
          title={values.id === 0 ? 'Add Issue' : 'Issue Detail'}
          submit={handleSubmit}
          open={props.open}
          setOpen={() => props.setOpen(false)}
          isAddingForm={values.id === 0 ? true : false}
          disable={
            values.title === '' ||
            (values.id !== 0 &&
              !analysePermission?.includes(
                PlaybookPermission.PlaybookItemIssueUpdate,
              )) ||
            !analysePermission?.includes(
              PlaybookPermission.PlaybookItemIssueCreate,
            )
          }
          formContent={
            <>
              <Box pt={2} pb={2}>
                <Box textAlign="center">
                  <Typography component="p" variant="h4"></Typography>
                </Box>
                <Grid
                  container
                  direction="column"
                  spacing={3}
                  className={classes.gridContainer}
                >
                  <Grid item>
                    <MixTitle title="Title" isRequired />
                    <Field name="title">
                      {({ field }: { field: FieldProps }) => (
                        <TextField
                          variant="outlined"
                          required
                          fullWidth
                          margin="normal"
                          {...field}
                          error={touched.title && !!errors?.title}
                          disabled={
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemIssueUpdate,
                            ) ||
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemIssueCreate,
                            )
                          }
                        />
                      )}
                    </Field>
                  </Grid>
                  <Grid item>
                    <MixTitle title="Description" />
                    <Field name="description">
                      {({ field }: { field: FieldProps }) => (
                        <TextField
                          {...field}
                          variant="outlined"
                          fullWidth
                          margin="normal"
                          multiline
                          rows={5}
                          disabled={
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemIssueUpdate,
                            ) ||
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemIssueCreate,
                            )
                          }
                        />
                      )}
                    </Field>
                  </Grid>
                  {values.id !== 0 && (
                    <Grid item>
                      <Box width="120px">
                        <MixTitle title="State" />
                        <Select
                          disabled={
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemIssueUpdate,
                            ) ||
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemIssueCreate,
                            )
                          }
                          fullWidth
                          name="state"
                          id="state"
                          variant="outlined"
                          onChange={handleChange}
                          value={values.state}
                          IconComponent={ExpandMoreIcon}
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            getContentAnchorEl: null,
                          }}
                        >
                          {STATES?.map(state => (
                            <MenuItem key={state.key} value={state.key}>
                              {state.name}
                            </MenuItem>
                          ))}
                        </Select>
                      </Box>
                    </Grid>
                  )}
                  {values.id !== 0 && (
                    <IssueComment
                      issue={issue}
                      comment={comment}
                      lastRefresh={lastRefresh}
                      setComment={setComment}
                      handleSubmitComment={handleSubmitComment}
                    />
                  )}
                </Grid>
              </Box>
            </>
          }
        />
      )}
    </Formik>
  );
};
