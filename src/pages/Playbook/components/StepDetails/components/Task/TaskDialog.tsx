import React, { useState, useContext } from 'react';
import {
  Grid,
  TextField,
  Box,
  InputAdornment,
  Tabs,
  Dialog,
  Typography,
  DialogActions,
  Button,
} from '@material-ui/core';
import {
  ActionEditable,
  PlaybookPermission,
} from '../../../../../../pages/Playbook/types';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { a11yProps } from '../../../../../../utils';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DateFnsUtils from '@date-io/date-fns';
import {
  StyledDataSearch,
  TabFluid,
} from '../../../../../../components/styled';
import { useParams } from 'react-router-dom';
import PlaybookContext from '../../../../PlaybookContext';
import { GetPlaybookItemTaskIndex } from '../../../../../../store/playbook/actions';
import { addOpportunityComment } from '../../../../../../services/comment.services';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { AnalysePermissionState } from '../../../../../../store/opportunity/selector';
import { CustomSelect } from '../../../../../../components/CustomSelect';
import { useSelector, shallowEqual } from 'react-redux';
import { MixTitle, TabPanel } from '../../../../../../components';
import { useDispatch } from 'react-redux';
import TaskDetail from './TaskDetail';
import TaskComment from './TaskComment';
import {
  TaskState,
  StateLabels,
  TaskSubTypeID,
  TaskTypeID,
  listTaskState,
} from '../../../../../../constants/task';
import { ConfigESIndex } from '../../../../../../store/config/selector';
import { useDisableMultipleClick } from '../../../../../../hooks';
import { Field, FieldProps, Formik, FormikBag } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import {
  addTaskNew,
  updateTask,
  IOpportunityTask,
} from '../../../../../../services/task.services';
import { UserProfileState } from '../../../../../../store/user/selector';

interface ITaskDialogProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  task: IOpportunityTask;
  editable: number;
  setEditable: (value: number) => void;
  onDelete?: (id: number) => void;
}
export const TaskDialog: React.FC<ITaskDialogProps> = props => {
  const dispatch = useDispatch();
  const { setOpen, task, editable, setEditable, onDelete, open } = props;
  const [activeSidebarTab, setSidebarActiveTab] = useState<number>(0);
  const { opportunityID } = useParams();
  const [comment, setComment] = useState('');
  const [editMode, setEditMode] = useState<boolean>(task.id === 0);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [lastRefresh, setLastRefresh] = useState<number>(0);
  const { selectedItem, onRefresh } = useContext(PlaybookContext);
  const onChangeSidebarTab = (
    event: React.ChangeEvent<{}>,
    newValue: number,
  ) => {
    setSidebarActiveTab(newValue);
  };

  const handleSubmitComment = async () => {
    if (isSubmitting) return;
    await debounceFn();
    await onSubmit(comment);
    endRequest();
  };
  const onSubmit = async (comment: string) => {
    if (comment) {
      try {
        const response = await addOpportunityComment({
          entity_id: task?.id,
          entity_type: 'playbook_task',
          comment_type: task?.task_state,
          comment_body: comment,
          comment_parent_id: 0,
        });
        if (response.success) {
          setTimeout(() => setLastRefresh(new Date().getTime()), 1000);
          setComment('');
        }
      } catch (err) {
        console.log(err);
      }
      return;
    }
  };
  const useStyles = makeStyles(theme => ({
    time: {
      color: theme.palette.grey[600],
      fontSize: 13,
    },
    gridContainer: {
      flexWrap: 'unset',
    },
    tab: {
      maxWidth: '10%',
    },
    taskStatus: {
      fontWeight: 'bold',
    },
    selectAssignee: {
      pointerEvents: isSubmitting || !editMode ? 'none' : 'auto',
    },
  }));
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const taskStateInfo = listTaskState.find(e => e.name === task.task_state);
  const userProfileState = useSelector(UserProfileState);

  return (
    <Formik
      initialValues={{
        id: task.id,
        title: task.title,
        description: task.description,
        assignee: task.assignee_user.id,
        assigneeName: task.assignee_user.name,
        dueDate: task.requested_completion_date,
        taskStateId: taskStateInfo?.id ?? 0,
      }}
      enableReinitialize
      onSubmit={async (values, formikHelpers) => {
        if (isSubmitting) {
          return;
        }
        await debounceFn();
        formikHelpers.setSubmitting(true);
        try {
          if (values.id > 0) {
            let response = await updateTask(values.id, {
              entity_id: parseInt(opportunityID),
              entity_type: 'opportunity_playbook_item',
              title: values.title,
              description: values.description || '',
              assignee_user_id: values.assignee,
              task_state_id: values.taskStateId,
              task_sub_type_id: TaskSubTypeID.Review,
              task_type_id: TaskTypeID.User,
              requested_completion_date: values.dueDate,
            });
            if (response.success) {
              setEditable(0);
              dispatch(GetPlaybookItemTaskIndex(0));
              setOpen(false);
              onRefresh?.();
            }
          } else {
            let response = await addTaskNew({
              entity_id: selectedItem?.id ?? 0,
              entity_type: 'opportunity_playbook_item',
              title: values.title,
              description: values.description || '',
              assignee_user_id: values.assignee,
              task_state_id: values.taskStateId,
              task_sub_type_id: TaskSubTypeID.Review,
              task_type_id: TaskTypeID.User,
              requested_completion_date: values.dueDate,
            });
            if (response.success) {
              setEditable(0);
              // setEditMode(false);
              setOpen(false);
              onRefresh?.();
            }
          }
        } catch (e) {
          console.log('Error submitting finding', e);
        }
        formikHelpers.setSubmitting(false);
        endRequest();
      }}
    >
      {({
        errors,
        touched,
        values,
        setFieldValue,
        handleChange,
        isSubmitting,
        handleReset,
        handleSubmit,
        resetForm,
      }) => (
        <Dialog open={open} aria-labelledby="form-dialog" maxWidth={'md'}>
          <Box width={800}>
            <Box padding={4}>
              <Box textAlign="center">
                <Typography component="p" variant="h4">
                  {values.id === 0 ? 'Add task' : 'Task detail'}
                </Typography>
              </Box>
              <Grid
                container
                direction="column"
                spacing={3}
                className={classes.gridContainer}
              >
                <Grid item>
                  <MixTitle title="Title" isRequired />
                  <Field name="title">
                    {({ field }: { field: FieldProps }) => (
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        margin="normal"
                        {...field}
                        disabled={
                          isSubmitting ||
                          !editMode ||
                          !analysePermission?.includes(
                            PlaybookPermission.PlaybookItemTaskUpdate,
                          )
                        }
                        error={touched.title && !!errors?.title}
                      />
                    )}
                  </Field>
                </Grid>
                <Grid item>
                  <MixTitle title="Description" />
                  <Field name="description">
                    {({ field }: { field: FieldProps }) => (
                      <TextField
                        {...field}
                        variant="outlined"
                        fullWidth
                        margin="normal"
                        multiline
                        disabled={
                          isSubmitting ||
                          !editMode ||
                          !analysePermission?.includes(
                            PlaybookPermission.PlaybookItemTaskUpdate,
                          )
                        }
                        rows={5}
                      />
                    )}
                  </Field>
                </Grid>
                <Grid item>
                  <Grid container spacing={2}>
                    <Grid item xs={5}>
                      <MixTitle title="Assignee" isRequired />
                      <Field name="assignee">
                        {({
                          field,
                          form,
                        }: {
                          field: FieldProps;
                          form: FormikBag<any, any>;
                        }) => (
                          <ReactiveBase
                            app={ES_INDICES.user_profile_index_name}
                            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                            headers={{
                              Authorization: window.localStorage.getItem('jwt'),
                            }}
                          >
                            <StyledDataSearch
                              defaultValue={values.assigneeName}
                              componentId="UserSelect"
                              dataField={['email', 'name']}
                              queryFormat="or"
                              placeholder="Search for user..."
                              showIcon={false}
                              showClear={true}
                              showDistinctSuggestions
                              innerClass={{
                                input: 'searchInput',
                                title: 'titleSearch',
                              }}
                              parseSuggestion={suggestion => ({
                                label: (
                                  <div>
                                    <Grid container direction={'column'}>
                                      <Grid item>
                                        <Box fontWeight="fontWeightBold">
                                          {suggestion.source?.name}
                                        </Box>
                                      </Grid>
                                      <Grid item>
                                        <span style={{ fontStyle: 'italic' }}>
                                          Email:{' '}
                                        </span>
                                        {suggestion.source?.email}
                                        {' | '}
                                        <span style={{ fontStyle: 'italic' }}>
                                          Business unit:{' '}
                                        </span>
                                        {
                                          suggestion.source?.business_unit
                                            ?.business_unit_name
                                        }
                                      </Grid>
                                    </Grid>
                                  </div>
                                ),
                                value: suggestion.source.email,
                                source: suggestion.source,
                              })}
                              onValueSelected={(value, cause, source) => {
                                form.setFieldValue(
                                  'assignee',
                                  source?.id ?? '',
                                  true,
                                );
                              }}
                              className={classes.selectAssignee}
                            />
                          </ReactiveBase>
                        )}
                      </Field>
                    </Grid>
                    <Grid item xs={3}>
                      <MixTitle title="Current state" isRequired />
                      <Field name="taskStateId">
                        {({
                          field,
                          form,
                        }: {
                          field: FieldProps;
                          form: FormikBag<any, any>;
                        }) => (
                          <CustomSelect
                            value={values.taskStateId}
                            onChange={handleChange}
                            id="taskStateId"
                            name="taskStateId"
                            options={
                              listTaskState?.map(item => ({
                                id: item.id!,
                                name: StateLabels[item.name as TaskState]!,
                              })) || []
                            }
                            disabled={
                              values.id === 0 ||
                              isSubmitting ||
                              !editMode ||
                              (values.assignee !== userProfileState.id &&
                                !analysePermission?.includes(
                                  PlaybookPermission.PlaybookItemAssign,
                                ))
                            }
                            fullWidth
                            style={{}}
                          />
                        )}
                      </Field>
                    </Grid>
                    <Grid item xs={4}>
                      <MixTitle title="Due date" />
                      <Field name="dueDate">
                        {({
                          field,
                          form,
                        }: {
                          field: any;
                          form: FormikBag<any, any>;
                        }) => (
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              disableToolbar
                              allowKeyboardControl
                              minDate={
                                editable === ActionEditable.Task &&
                                !values.dueDate
                                  ? new Date()
                                  : ''
                              }
                              inputVariant="outlined"
                              variant="inline"
                              margin="normal"
                              // name="requested_completion_date"
                              id="due-date"
                              format="d MMMM yyyy"
                              autoOk={true}
                              onChange={(date: MaterialUiPickersDate) => {
                                form.setFieldValue(field.name, date);
                              }}
                              value={
                                field.value === '0001-01-01T00:00:00Z'
                                  ? null
                                  : field.value
                              }
                              animateYearScrolling={false}
                              fullWidth
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <ExpandMoreIcon />
                                  </InputAdornment>
                                ),
                              }}
                              disabled={isSubmitting || !editMode}
                            />
                          </MuiPickersUtilsProvider>
                        )}
                      </Field>
                    </Grid>
                  </Grid>
                </Grid>
                {values.id !== 0 && (
                  <Grid item>
                    <Tabs
                      value={activeSidebarTab}
                      onChange={onChangeSidebarTab}
                      aria-label="sidebar tabs"
                    >
                      <TabFluid
                        label="Detail"
                        className={classes.tab}
                        {...a11yProps('sidebar-tab', 0)}
                      />
                      <TabFluid
                        label="Comment"
                        className={classes.tab}
                        {...a11yProps('sidebar-tab', 1)}
                      />
                    </Tabs>
                    <TabPanel value={activeSidebarTab} index={0}>
                      <TaskDetail task={task ?? []} />
                    </TabPanel>
                    <TabPanel
                      value={activeSidebarTab}
                      index={1}
                      noOutlined={true}
                    >
                      <TaskComment
                        task={task}
                        comment={comment}
                        lastRefresh={lastRefresh}
                        setComment={setComment}
                        handleSubmitComment={handleSubmitComment}
                      />
                    </TabPanel>
                  </Grid>
                )}
              </Grid>
              <DialogActions style={{ padding: 0, marginTop: 15 }}>
                <Button
                  onClick={() => {
                    dispatch(GetPlaybookItemTaskIndex(0));
                    setEditMode(false);
                    setEditable(0);
                    setOpen(false);
                    onDelete?.(0);
                    resetForm();
                  }}
                  color="default"
                  variant="outlined"
                >
                  Cancel
                </Button>
                <Button
                  type="submit"
                  color="primary"
                  variant="contained"
                  onClick={
                    editMode
                      ? () => handleSubmit()
                      : () => {
                          setEditable(ActionEditable.Task);
                          setEditMode(true);
                        }
                  }
                  disabled={
                    (values.title && values.assignee) ||
                    (values.id !== 0 &&
                      !analysePermission?.includes(
                        PlaybookPermission.PlaybookItemTaskUpdate,
                      ))
                      ? false
                      : true
                  }
                >
                  {values.id === 0 ? 'Add' : !editMode ? 'Edit' : 'Update'}
                </Button>
              </DialogActions>
            </Box>
          </Box>
        </Dialog>
      )}
    </Formik>
  );
};
