import React, { useCallback, useState, useContext } from 'react';
import {
  ExpansionPanelStyled,
  ExpansionPanelSummaryStyled,
  TabFluid,
} from '../../../../../../components/styled';
import { useSelector, shallowEqual } from 'react-redux';
// import { useTranslation } from 'react-i18next';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  ExpansionPanelDetails,
  Typography,
  Grid,
  TextField,
  Box,
  InputAdornment,
  Tabs,
} from '@material-ui/core';
import TaskDetail from './TaskDetail';
import TaskComment from './TaskComment';
import { ActionEditable, PlaybookPermission } from '../../../../types';
import { Field, FieldProps, Form, Formik, FormikBag } from 'formik';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { a11yProps } from '../../../../../../utils';
import { date, number, object, string } from 'yup';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useDisableMultipleClick } from '../../../../../../hooks';
import { StyledDataSearch } from '../../../../../../components/styled';
import { ConfigESIndex } from '../../../../../../store/config/selector';
import DateFnsUtils from '@date-io/date-fns';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import {
  CancelIconButton,
  DeleteIconButton,
  EditIconButton,
  MixTitle,
  SaveIconButton,
  TabPanel,
} from '../../../../../../components';
import { useParams } from 'react-router-dom';
import {
  addTaskNew,
  updateTask,
  IOpportunityTask,
} from '../../../../../../services/task.services';
import { addOpportunityComment } from '../../../../../../services/comment.services';
import {
  TaskState,
  StateLabels,
  TaskSubTypeID,
  TaskTypeID,
  listTaskState,
} from '../../../../../../constants/task';
import PlaybookContext from '../../../../PlaybookContext';
import { AnalysePermissionState } from '../../../../../../store/opportunity/selector';
import TaskStateChip from '../../../../../../components/TaskStateChip';
import { CustomSelect } from '../../../../../../components/CustomSelect';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
const useStyles = makeStyles(theme => ({
  time: {
    color: theme.palette.grey[600],
    fontSize: 13,
  },
  gridContainer: {
    flexWrap: 'unset',
  },
  tab: {
    maxWidth: '10%',
  },
  taskStatus: {
    fontWeight: 'bold',
  },
  panelTtile: {},
}));

interface TaskItemProps {
  index: number;
  task: IOpportunityTask;
  userLoginId: number;
  expanded?: string;
  setEditable: (value: number) => void;
  setActiveTab: React.Dispatch<React.SetStateAction<number>>;
  editable: number;
  onDelete?: (id: number) => void;
  onChangePanel: (current: string) => void;
}

export const TaskItem: React.FC<TaskItemProps> = ({
  index,
  task,
  userLoginId,
  expanded,
  onChangePanel,
  setActiveTab,
  editable,
  setEditable,
  onDelete,
}) => {
  const classes = useStyles();
  // const { t } = useTranslation();
  const ES_INDICES = useSelector(ConfigESIndex);
  const { selectedItem, onRefresh } = useContext(PlaybookContext);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const [comment, setComment] = useState('');
  const [editMode, setEditMode] = useState<boolean>(task.id === 0);
  const { opportunityID } = useParams();
  const [activeSidebarTab, setSidebarActiveTab] = useState<number>(0);
  const [lastRefresh, setLastRefresh] = useState<number>(0);
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);

  const onChangeSidebarTab = (
    event: React.ChangeEvent<{}>,
    newValue: number,
  ) => {
    setSidebarActiveTab(newValue);
  };
  const renderExpansionIcon = useCallback(
    (current: string) => {
      return current === expanded ? <RemoveCircleIcon /> : <AddCircleIcon />;
    },
    [expanded],
  );

  const handleSubmitComment = async () => {
    if (isSubmitting) return;
    await debounceFn();
    await onSubmit(comment);
    endRequest();
  };
  const onSubmit = async (comment: string) => {
    if (comment) {
      try {
        const response = await addOpportunityComment({
          entity_id: task.id,
          entity_type: 'playbook_task',
          comment_type: task.task_state,
          comment_body: comment,
          comment_parent_id: 0,
        });
        if (response.success) {
          setTimeout(() => setLastRefresh(new Date().getTime()), 1000);
          setComment('');
        }
      } catch (err) {
        console.log(err);
      }
      return;
    }
  };

  const validationSchema = () =>
    object().shape({
      title: string().required(),
      description: string(),
      assignee: number().required(),
      dueDate: date().nullable(),
    });
  const taskStateInfo = listTaskState.find(e => e.name === task.task_state);
  return (
    <Formik
      initialValues={{
        id: task.id,
        title: task.title,
        description: task.description,
        assignee: task.assignee_user.id,
        assigneeName: task.assignee_user.name,
        dueDate: task.requested_completion_date,
        taskStateId: taskStateInfo?.id ?? 0,
      }}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={async (values, formikHelpers) => {
        if (isSubmitting) {
          return;
        }
        await debounceFn();
        formikHelpers.setSubmitting(true);
        try {
          if (values.id > 0) {
            let response = await updateTask(values.id, {
              entity_id: parseInt(opportunityID),
              entity_type: 'opportunity_playbook_item',
              title: values.title,
              description: values.description || '',
              assignee_user_id: values.assignee,
              task_state_id: values.taskStateId,
              task_sub_type_id: TaskSubTypeID.Review,
              task_type_id: TaskTypeID.User,
              requested_completion_date: values.dueDate,
            });
            if (response.success) {
              setEditable(0);
              onRefresh?.();
              setActiveTab(1);
            }
          } else {
            let response = await addTaskNew({
              entity_id: selectedItem?.id ?? 0,
              entity_type: 'opportunity_playbook_item',
              title: values.title,
              description: values.description || '',
              assignee_user_id: values.assignee,
              task_state_id: values.taskStateId,
              task_sub_type_id: TaskSubTypeID.Review,
              task_type_id: TaskTypeID.User,
              requested_completion_date: values.dueDate,
            });
            if (response.success) {
              setEditable(0);
              setEditMode(false);
              onRefresh?.();
              setActiveTab(1);
            }
          }
        } catch (e) {
          console.log('Error submitting finding', e);
        }
        formikHelpers.setSubmitting(false);
        endRequest();
      }}
      // onReset={(values, formikHelpers) => {
      //   if (values.id === 0 && onDelete) onDelete(0);
      //   formikHelpers.setFieldValue('disabled', true);
      // }}
    >
      {({
        errors,
        touched,
        values,
        setFieldValue,
        handleChange,
        isSubmitting,
        handleReset,
        handleSubmit,
        resetForm,
      }) => (
        <ExpansionPanelStyled
          key={index}
          expanded={expanded === `t${index}`}
          onChange={() => onChangePanel(`t${index}`)}
        >
          <ExpansionPanelSummaryStyled
            aria-controls="step-task"
            expandIcon={renderExpansionIcon(`t${index}`)}
            disabled={
              !analysePermission?.includes(
                PlaybookPermission.PlaybookItemTaskRead,
              )
            }
          >
            <Box
              display="flex"
              justifyContent="space-between"
              flex={1}
              alignItems="center"
            >
              <Box display="flex" flex={1} alignItems="center">
                <Box
                  mr={2}
                  display="flex"
                  flexDirection="row"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Typography variant="h5" style={{ marginRight: 5 }}>
                    {task.id === 0 ? `Task ${index + 1}` : task.title}
                  </Typography>
                  <TaskStateChip taskState={task.task_state} />
                </Box>
              </Box>
              <Box display="flex" flex={1} justifyContent="flex-end">
                {task.last_updated_at && task.created_by_user && (
                  <Typography className={classes.time}>
                    {`Last updated by ${
                      task.updated_user?.name !== ''
                        ? task.updated_user?.name
                        : task.created_by_user?.name
                    } at ${moment(task.last_updated_at).format(
                      'DD/MM/YYYY HH:mm:ss',
                    )}`}
                  </Typography>
                )}
              </Box>
            </Box>
          </ExpansionPanelSummaryStyled>
          <ExpansionPanelDetails>
            <Form style={{ minWidth: '100%' }}>
              <Grid
                container
                direction="column"
                spacing={3}
                className={classes.gridContainer}
              >
                <Box display="flex" justifyContent="flex-end">
                  {editMode && editable === ActionEditable.Task ? (
                    <Box display="flex" mt={2} mr={1.5}>
                      <Box mr={2} component="span" display="flex">
                        <CancelIconButton
                          onClick={() => {
                            setEditMode(false);
                            setEditable(0);
                            onDelete?.(0);
                            resetForm();
                            setActiveTab(1);
                          }}
                          disabled={isSubmitting}
                        />
                      </Box>
                      <SaveIconButton
                        disabled={isSubmitting || !values.assignee}
                        onClick={() => {
                          handleSubmit();
                          setActiveTab(1);
                        }}
                      />
                    </Box>
                  ) : (
                    <Box display="flex" mt={2} mr={1.5}>
                      <Box mr={2} component="span" display="flex">
                        <DeleteIconButton
                          onClick={() => {
                            onDelete?.(values.id);
                            setActiveTab(1);
                          }}
                          disabled={
                            !analysePermission?.includes(
                              PlaybookPermission.PlaybookItemTaskDelete,
                            )
                          }
                        />
                      </Box>

                      <EditIconButton
                        key={index}
                        onClick={() => {
                          setEditable(ActionEditable.Task);
                          setEditMode(true);
                        }}
                        disabled={
                          !analysePermission?.includes(
                            PlaybookPermission.PlaybookItemTaskUpdate,
                          )
                        }
                      />
                    </Box>
                  )}
                </Box>
                <Grid item>
                  <MixTitle title="Title" isRequired />
                  <Field name="title">
                    {({ field }: { field: FieldProps }) => (
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        margin="normal"
                        {...field}
                        disabled={
                          isSubmitting ||
                          !editMode ||
                          editable !== ActionEditable.Task
                        }
                        error={touched.title && !!errors?.title}
                      />
                    )}
                  </Field>
                </Grid>
                <Grid item>
                  <MixTitle title="Description" />
                  <Field name="description">
                    {({ field }: { field: FieldProps }) => (
                      <TextField
                        {...field}
                        variant="outlined"
                        fullWidth
                        margin="normal"
                        multiline
                        rows={5}
                        disabled={
                          isSubmitting ||
                          !editMode ||
                          editable !== ActionEditable.Task
                        }
                      />
                    )}
                  </Field>
                </Grid>
                <Grid item>
                  <Grid container spacing={5}>
                    <Grid item xs={4}>
                      <MixTitle title="Assignee" isRequired />
                      <Field name="assignee">
                        {({
                          field,
                          form,
                        }: {
                          field: FieldProps;
                          form: FormikBag<any, any>;
                        }) => (
                          <ReactiveBase
                            app={ES_INDICES.user_profile_index_name}
                            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                            headers={{
                              Authorization: window.localStorage.getItem('jwt'),
                            }}
                          >
                            <StyledDataSearch
                              defaultValue={values.assigneeName}
                              componentId="UserSelect"
                              dataField={['email', 'name']}
                              queryFormat="or"
                              placeholder="Search for user..."
                              showIcon={false}
                              showClear={true}
                              showDistinctSuggestions
                              innerClass={{
                                input: 'searchInput',
                                title: 'titleSearch',
                              }}
                              parseSuggestion={suggestion => ({
                                label: (
                                  <div>
                                    <Grid container direction={'column'}>
                                      <Grid item>
                                        <Box fontWeight="fontWeightBold">
                                          {suggestion.source?.name}
                                        </Box>
                                      </Grid>
                                      <Grid item>
                                        <span style={{ fontStyle: 'italic' }}>
                                          Email:{' '}
                                        </span>
                                        {suggestion.source?.email}
                                        {' | '}
                                        <span style={{ fontStyle: 'italic' }}>
                                          Business unit:{' '}
                                        </span>
                                        {
                                          suggestion.source?.business_unit
                                            ?.business_unit_name
                                        }
                                      </Grid>
                                    </Grid>
                                  </div>
                                ),
                                value: suggestion.source.email,
                                source: suggestion.source,
                              })}
                              onValueSelected={(value, cause, source) => {
                                form.setFieldValue(
                                  'assignee',
                                  source?.id ?? '',
                                  true,
                                );
                              }}
                              style={
                                isSubmitting ||
                                !editMode ||
                                editable !== ActionEditable.Task
                                  ? { pointerEvents: 'none' }
                                  : {}
                              }
                            />
                          </ReactiveBase>
                        )}
                      </Field>
                    </Grid>
                    <Grid item xs={4}>
                      <MixTitle title="Current state" isRequired />
                      <Field name="taskStateId">
                        {({
                          field,
                          form,
                        }: {
                          field: FieldProps;
                          form: FormikBag<any, any>;
                        }) => (
                          <CustomSelect
                            value={values.taskStateId}
                            onChange={handleChange}
                            id="taskStateId"
                            name="taskStateId"
                            options={
                              listTaskState?.map(item => ({
                                id: item.id!,
                                name: StateLabels[item.name as TaskState]!,
                              })) || []
                            }
                            disabled={
                              task.id === 0 ||
                              isSubmitting ||
                              !editMode ||
                              editable !== ActionEditable.Task ||
                              !analysePermission?.includes(
                                PlaybookPermission.PlaybookItemAssign,
                              )
                            }
                            fullWidth
                          />
                        )}
                      </Field>
                    </Grid>
                    <Grid item xs={4}>
                      <MixTitle title="Due date" />
                      <Field name="dueDate">
                        {({
                          field,
                          form,
                        }: {
                          field: any;
                          form: FormikBag<any, any>;
                        }) => (
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              disableToolbar
                              disabled={
                                isSubmitting ||
                                !editMode ||
                                editable !== ActionEditable.Task
                              }
                              allowKeyboardControl
                              minDate={new Date()}
                              inputVariant="outlined"
                              variant="inline"
                              margin="normal"
                              // name="requested_completion_date"
                              id="due-date"
                              format="d MMMM yyyy"
                              autoOk={true}
                              onChange={(date: MaterialUiPickersDate) => {
                                form.setFieldValue(field.name, date);
                              }}
                              value={
                                field.value === '0001-01-01T00:00:00Z'
                                  ? null
                                  : field.value
                              }
                              animateYearScrolling={false}
                              fullWidth
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <ExpandMoreIcon />
                                  </InputAdornment>
                                ),
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        )}
                      </Field>
                    </Grid>
                  </Grid>
                </Grid>
                {values.id !== 0 && (
                  <Grid item>
                    <Tabs
                      value={activeSidebarTab}
                      onChange={onChangeSidebarTab}
                      aria-label="sidebar tabs"
                    >
                      <TabFluid
                        label="Detail"
                        className={classes.tab}
                        {...a11yProps('sidebar-tab', 0)}
                      />
                      <TabFluid
                        label="Comment"
                        className={classes.tab}
                        {...a11yProps('sidebar-tab', 1)}
                      />
                    </Tabs>
                    <TabPanel value={activeSidebarTab} index={0}>
                      <TaskDetail task={task} />
                    </TabPanel>
                    <TabPanel
                      value={activeSidebarTab}
                      index={1}
                      noOutlined={true}
                    >
                      <TaskComment
                        task={task}
                        comment={comment}
                        lastRefresh={lastRefresh}
                        setComment={setComment}
                        handleSubmitComment={handleSubmitComment}
                      />
                    </TabPanel>
                  </Grid>
                )}
              </Grid>
            </Form>
          </ExpansionPanelDetails>
        </ExpansionPanelStyled>
      )}
    </Formik>
  );
};
