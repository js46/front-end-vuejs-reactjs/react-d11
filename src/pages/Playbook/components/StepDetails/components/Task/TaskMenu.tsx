import React from 'react';
import { Box, Button, MenuItem, Tooltip } from '@material-ui/core';
import { StyledMenu } from '../../../../../../components/styled';
import { useTranslation } from 'react-i18next';
import { PlaybookPermission } from '../../../../../../pages/Playbook/types';
import FormatListBulletedOutlinedIcon from '@material-ui/icons/FormatListBulletedOutlined';
import { AnalysePermissionState } from '../../../../../../store/opportunity/selector';
import { IOpportunityTask } from '../../../../../../services/task.services';
import Fade from '@material-ui/core/Fade';
import { GetPlaybookItemTaskIndex } from '../../../../../../store/playbook/actions';
import TaskStateChip from '../../../../../../components/TaskStateChip';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  button: {
    minWidth: 40,
    '&:hover': {
      textDecorationLine: 'underline',
    },
  },
}));

type TaskMenuProps = {
  tasks: IOpportunityTask[];
  setTasks: React.Dispatch<React.SetStateAction<IOpportunityTask[]>>;
  size?: 'small' | 'large';
  onClickAdd: (type: 'list' | 'link' | 'task' | 'finding' | 'issue') => void;
  disabled: boolean;
  openTaskDialog: boolean;
  setOpenTaskDialog: React.Dispatch<React.SetStateAction<boolean>>;
};
export const TaskMenu: React.FC<TaskMenuProps> = props => {
  const {
    tasks,
    size,
    onClickAdd,
    disabled,
    // openTaskDialog,
    setOpenTaskDialog,
  } = props;
  const classes = useStyles();
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  return (
    <Box>
      <Tooltip title={t('playbook_step_item.task')}>
        <Button
          aria-controls="fade-menu"
          aria-haspopup="true"
          onClick={(event: any) => setAnchorEl(event.target)}
          disabled={disabled}
          className={classes.button}
          startIcon={
            <FormatListBulletedOutlinedIcon color="primary" fontSize={size} />
          }
        >
          Tasks
        </Button>
      </Tooltip>

      <StyledMenu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => {
          setAnchorEl(null);
        }}
        TransitionComponent={Fade}
      >
        {tasks.map((task, index) => (
          <MenuItem
            key={index}
            onClick={() => {
              setAnchorEl(null);
              setOpenTaskDialog(true);
              dispatch(GetPlaybookItemTaskIndex(task.id));
            }}
            disabled={false}
          >
            <Tooltip title={task.title}>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                width="100%"
              >
                <Box mr={1}>
                  {task.title.length > 45
                    ? `${task.title.slice(0, 45)}...`
                    : task.title}
                </Box>
                <TaskStateChip taskState={task.task_state} />
              </Box>
            </Tooltip>
          </MenuItem>
        ))}
        <MenuItem value="new_task">
          <Button
            startIcon={<AddCircleOutlineIcon />}
            onClick={() => {
              setAnchorEl(null);
              onClickAdd('task');
              dispatch(GetPlaybookItemTaskIndex(0));
              setOpenTaskDialog(true);
            }}
            disabled={
              !analysePermission?.includes(
                PlaybookPermission.PlaybookItemTaskCreate,
              )
            }
          >
            New Task
          </Button>
        </MenuItem>
      </StyledMenu>
    </Box>
  );
};
