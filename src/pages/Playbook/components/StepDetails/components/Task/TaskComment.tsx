import React, { useState, useRef } from 'react';
import { TextField, Box, Button, CircularProgress } from '@material-ui/core';
import { customQueryComment } from '../../../../../../utils';
import { IOpportunityTask } from '../../../../../../services/task.services';
import { CommentReactiveList } from '../../../../../../pages/Opportunity/components/CaptureTab/components';

interface ITaskComment {
  task: IOpportunityTask;
  comment: string;
  setComment: React.Dispatch<React.SetStateAction<string>>;
  lastRefresh: number;
  handleSubmitComment: () => Promise<void>;
}
const TaskComment: React.FC<ITaskComment> = props => {
  const { task, comment, setComment, handleSubmitComment, lastRefresh } = props;
  const [showCommentBtn, setCommentBtn] = useState(false);
  let messagesEndRef = useRef<HTMLDivElement>(null);
  const toggleCommentBtn = (show: boolean) => () => {
    setCommentBtn(show);
    if (show) {
      setTimeout(() => {
        messagesEndRef?.current?.scrollIntoView({ behavior: 'smooth' });
      }, 400);
    }
  };

  const onBlurComment = () => {
    if (!comment.trim()) toggleCommentBtn(false)();
  };
  const queryComment = () => {
    return customQueryComment(
      task.id,
      {
        entity_type: 'playbook_task',
        comment_parent_id: 0,
      },
      lastRefresh,
    );
  };
  const queryCommentCount = () => {
    return customQueryComment(
      task.id,
      {
        entity_type: 'playbook_task',
      },
      lastRefresh,
    );
  };
  return (
    <Box>
      <CommentReactiveList
        defaultQuery={queryComment}
        defaultQueryCount={queryCommentCount}
        showReply
        canReply={true}
        loader={
          <Box display="flex" justifyContent="center" p={1}>
            <CircularProgress />
          </Box>
        }
      />
      <TextField
        multiline
        placeholder="Add a comment..."
        rows={3}
        fullWidth
        id="comment"
        name="comment"
        variant="outlined"
        onFocus={toggleCommentBtn(true)}
        onBlur={onBlurComment}
        onChange={event => setComment(event.target.value)}
        value={comment}
      />
      {showCommentBtn && (
        <Box pb={2} pt={2} display="flex" flexDirection="row">
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={handleSubmitComment}
          >
            Save
          </Button>
          <Box ml={1}>
            <Button variant="outlined" color="default" size="small">
              Cancel
            </Button>
          </Box>
        </Box>
      )}
    </Box>
  );
};
export default TaskComment;
