import React from 'react';
import { Typography, Grid, Box } from '@material-ui/core';
import { TaskState, StateLabels } from '../../../../../../constants/task';
import { IOpportunityTask } from '../../../../../../services/task.services';
import TaskStateChip from '../../../../../../components/TaskStateChip';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import { MixTitle } from '../../../../../../components';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { ConfigESIndex } from '../../../../../../store/config/selector';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import moment from 'moment';
const useStyles = makeStyles(theme => ({
  taskStatus: {
    fontWeight: 'bold',
  },
}));
interface ITaskDetailProps {
  task: IOpportunityTask;
}
const TaskDetail: React.FC<ITaskDetailProps> = props => {
  const { task } = props;
  const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const { t } = useTranslation();
  const defaultQuery = () => {
    return {
      query: {
        bool: {
          must: [
            { term: { action_name: 'task' } },
            { term: { 'response_body.data.id': task.id } },
          ],
        },
      },
    };
  };
  return (
    <Grid container spacing={5}>
      <Grid item xs={8}>
        <Box
          m={2}
          style={{
            overflowY: 'scroll',
            overflowX: 'hidden',
            maxHeight: 300,
          }}
        >
          {/* <MixTitle title="Activities" /> */}
          <ReactiveBase
            app={ES_INDICES.event_log_index_name_prefix + '-*'}
            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
            headers={{
              Authorization: window.localStorage.getItem('jwt'),
            }}
          >
            <ReactiveList
              componentId="change state activities list"
              dataField="time"
              sortBy="desc"
              renderResultStats={() => null}
              renderNoResults={() => <></>}
              defaultQuery={defaultQuery}
            >
              {({ data }) => {
                return (
                  <>
                    {data?.length === 0 ? (
                      <Box mt={2}>
                        <Typography>
                          {t('no_history_change_status_task')}
                        </Typography>
                      </Box>
                    ) : (
                      <>
                        {data?.map((item: any, index: number) => {
                          if (item.action_name === 'Update status task') {
                            return (
                              <>
                                <Box key={item.id}>
                                  <Grid container spacing={1}>
                                    <Grid item>
                                      <MixTitle
                                        title={
                                          item.response_body.data?.updated_user
                                            ?.name?.length > 20
                                            ? `${item.response_body.data.updated_user.name.slice(
                                                0,
                                                20,
                                              )}...`
                                            : item.response_body.data
                                                .updated_user.name
                                        }
                                      />
                                    </Grid>
                                    <Grid item>
                                      <Typography>has changed the </Typography>
                                      <Typography
                                        className={classes.taskStatus}
                                      >
                                        Status
                                      </Typography>
                                    </Grid>
                                    <Grid item>
                                      <Typography>
                                        {moment(
                                          item.response_body.data
                                            .last_updated_at,
                                        ).format('DD/MM/YYYY, h:mma')}
                                      </Typography>
                                    </Grid>
                                  </Grid>
                                  <Box
                                    display="flex"
                                    flexDirection="row"
                                    // justifyContent="center"
                                    alignItems="center"
                                  >
                                    <TaskStateChip
                                      taskState={
                                        item.response_body.data.task_state_prev
                                      }
                                    />
                                    <ArrowRightAltIcon />
                                    <TaskStateChip
                                      taskState={
                                        item.response_body.data.task_state
                                      }
                                    />
                                  </Box>
                                </Box>
                              </>
                            );
                          }
                        })}
                      </>
                    )}
                  </>
                );
              }}
            </ReactiveList>
          </ReactiveBase>
        </Box>
      </Grid>
      <Grid item xs={4}>
        <Box m={2}>
          <Typography variant="h6">Task type</Typography>
          <Typography paragraph>{task.task_type}</Typography>
          <Typography variant="h6">Task state</Typography>
          <Typography paragraph>
            {StateLabels[task.task_state as TaskState]}
          </Typography>
          <Typography variant="h6">Created by</Typography>
          <Typography paragraph>{task.created_by_user?.name}</Typography>
          <Typography variant="h6">Assigned at</Typography>
          <Typography paragraph>
            {moment(task.created_at).format('DD/MM/YYYY, h:mma')}
          </Typography>
          <Typography variant="h6">Updated at</Typography>
          <Typography paragraph>
            {moment(task.last_updated_at).format('DD/MM/YYYY, h:mma')}
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );
};

export default TaskDetail;
