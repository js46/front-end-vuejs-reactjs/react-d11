import React, { useContext, useMemo, useState } from 'react';
import { object, string } from 'yup';
import {
  Box,
  Grid,
  TextField,
  Typography,
  Button,
  Checkbox,
  IconButton,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { MixTitle, ExportFinding, FormDialog } from '../../../../../components';
import { Formik } from 'formik';
import {
  addPlaybookItemFinding,
  updatePlaybookItemFinding,
  PlaybookItemFindingT,
} from '../../../../../services/playbook.services';

import PlaybookContext from '../../../PlaybookContext';
import { ShareOutlined } from '@material-ui/icons';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { FindingType } from '../../../../../constants/findingType';
import { ActionEditable, PlaybookPermission } from '../../../types';
import { TinyMCEForm } from '../../../../../components/form/TinyMCEForm';
import {
  deleteFavoriteItem,
  addFavoriteItem,
  EntityType,
} from '../../../../../services/favorite.services';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { notifyError } from '../../../../../store/common/actions';
import { Message } from '../../../../../constants';
import moment from 'moment';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import { Autocomplete } from '@material-ui/lab';
import { updateFindingTag } from '../../../../../services/finding';
import { ITag } from '../../../../../services/references.services';
import _ from 'lodash';
import { AnalysePermissionState } from '../../../../../store/opportunity/selector';
interface FindingItemProps {
  index: number;
  item: PlaybookItemFindingT;
  onDelete?: (id: number) => void;
  expanded?: string;
  setEditable: (value: number) => void;
  editable: number;
  userID: number;
  onChangeCollapse: (current: string) => void;
}
const useStyles = makeStyles(theme => ({
  overText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    Width: 260,
    maxWidth: 260,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  icon: {
    fontSize: 18,
  },
  time: {
    color: theme.palette.grey[600],
    fontSize: 13,
  },
}));
export const FindingItem: React.FC<FindingItemProps> = ({
  item,
  onDelete,
  index,
  expanded,
  setEditable,
  editable,
  userID,
  onChangeCollapse,
}) => {
  const { selectedItem, onRefresh, tagReferences } = useContext(
    PlaybookContext,
  );
  const [loadingEditor, setLoadingEditor] = useState(true);
  const [editMode, setEditMode] = useState<boolean>(item.id === 0);
  const [isShare, setIsShare] = useState<boolean>(false);
  const [showTagDialog, setShowTagDialog] = useState<boolean>(false);
  const [tagForm, setTagForm] = useState<ITag[]>([]);
  const analysePermission = useSelector(AnalysePermissionState, shallowEqual);
  const classes = useStyles();

  const dispatch = useDispatch();

  const handleLikeAction = async (event?: React.ChangeEvent<{}>) => {
    event?.stopPropagation();

    if (!item.id) return;
    try {
      const favorite = item.favorites?.find(item => item.user_id === userID);
      if (favorite) {
        await deleteFavoriteItem(favorite.id);
      } else {
        await addFavoriteItem(EntityType.findings, item.id);
      }
      await onRefresh?.();
    } catch (e) {
      console.log('Error marking favorite', e);
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
  };
  const renderTitleIcons = (item: PlaybookItemFindingT) => (
    <>
      <Button
        startIcon={
          item?.favorites?.some(item => item.user_id === userID) ? (
            <ThumbUpIcon />
          ) : (
            <ThumbUpAltOutlinedIcon />
          )
        }
        onClick={handleLikeAction}
      >
        <b>{item?.favorites?.length ?? 0}</b>
      </Button>

      <Box component="span">
        <Button
          startIcon={<ShareOutlined />}
          onClick={() => setIsShare(true)}
        />
      </Box>
    </>
  );

  const validationScheme = useMemo(
    () =>
      object().shape({
        title: string()
          .trim()
          .required(),
      }),
    [],
  );
  const animatedStyle = (index: number) => ({
    fontSize: 20,
    transition: '0.3s all',
    transform: `f${index}` === expanded ? 'rotate(90deg)' : 'rotate(0deg)',
    '&:hover': {
      cursor: 'poiter',
    },
  });
  return (
    <>
      <Grid container spacing={1} direction="column">
        <Box ml={2} mb={2}>
          <Grid item container direction="row" alignItems="center">
            <IconButton
              size="small"
              onClick={() => onChangeCollapse(`f${index}`)}
              disabled={
                !analysePermission?.includes(
                  PlaybookPermission.PlaybookItemFindingRead,
                )
              }
            >
              <ArrowForwardIosIcon
                style={animatedStyle(index)}
                className={classes.icon}
              />
            </IconButton>
            <Box display="flex" alignItems="center">
              <Typography variant="h5" className={classes.overText}>
                {item.id === 0
                  ? `Finding ${index + 1}`
                  : `Finding - ${item.title}`}
              </Typography>
            </Box>
            <Box display="flex" alignItems="center" ml={2}>
              {renderTitleIcons(item)}
            </Box>
            <Box display="flex" flex={1} justifyContent="flex-end">
              {item?.last_updated_at && (
                <Typography className={classes.time}>
                  {`Last updated by ${
                    item?.updated_by_user?.name &&
                    item?.updated_by_user?.name.length > 0
                      ? item?.updated_by_user?.name?.length > 20
                        ? `${item?.updated_by_user?.name.slice(0, 20)}...`
                        : item?.updated_by_user?.name
                      : item?.created_by_user?.name.length > 20
                      ? `${item?.created_by_user?.name.slice(0, 20)}...`
                      : item?.created_by_user?.name
                  } at ${moment(item?.last_updated_at).format(
                    'DD/MM/YYYY HH:mm:ss',
                  )}`}
                </Typography>
              )}
            </Box>
          </Grid>
          <Formik
            initialValues={item}
            validationSchema={validationScheme}
            enableReinitialize
            onSubmit={async (values, formikHelpers) => {
              if (!selectedItem) return;
              formikHelpers.setSubmitting(true);
              try {
                if (values.id > 0) {
                  let UpdateResponse = await updatePlaybookItemFinding(
                    values.id,
                    values.title,
                    values.description ?? '',
                    values.public_flg,
                  );
                  if (UpdateResponse.success) {
                    const payload = tagForm.map(item => {
                      return { tag_id: item.id };
                    });
                    const response = await updateFindingTag(values.id, {
                      tags: payload,
                    });
                    if (response.success) {
                      onRefresh?.();
                    }
                    formikHelpers.setFieldValue('disabled', true);
                  }
                } else {
                  let findingResponse = await addPlaybookItemFinding(
                    selectedItem.id,
                    FindingType.PlaybookItem,
                    values.title,
                    values.description ?? '',
                    values.public_flg,
                  );
                  if (findingResponse.success) {
                    const payload = tagForm.map(item => {
                      return { tag_id: item.id };
                    });
                    const response = await updateFindingTag(
                      findingResponse.data.id,
                      {
                        tags: payload,
                      },
                    );
                    if (response.success) {
                      onRefresh?.();
                    }
                  }
                }
                setEditMode(false);
                setEditable(0);
                onRefresh?.();
              } catch (e) {
                console.log('Error submitting finding', e);
              }
              formikHelpers.setSubmitting(false);
            }}
          >
            {({
              values,
              handleChange,
              setFieldValue,
              handleSubmit,
              handleBlur,
              errors,
              touched,
              isSubmitting,
            }) => {
              const handleSubmitTags = async (
                event?: React.FormEvent<HTMLFormElement> | undefined,
              ) => {
                event?.preventDefault();
                handleSubmit();
                setShowTagDialog(false);
              };
              return (
                <>
                  {expanded === `f${index}` && (
                    <Box width={'100%'} pt={2}>
                      <Box display="flex" justifyContent="flex-end">
                        {editMode && editable === ActionEditable.Finding ? (
                          <Box display="flex">
                            <Box mr={2} component="span" display="flex">
                              <Button
                                variant="outlined"
                                color="default"
                                type="button"
                                onClick={() => {
                                  setEditMode(false);
                                  setEditable(0);
                                  onDelete?.(0);
                                }}
                                disabled={isSubmitting}
                              >
                                Cancel
                              </Button>
                            </Box>
                            <Button
                              variant="contained"
                              color="primary"
                              type="button"
                              disabled={isSubmitting}
                              onClick={() => handleSubmit()}
                            >
                              Save
                            </Button>
                          </Box>
                        ) : (
                          <Box display="flex">
                            <Box mr={2} component="span" display="flex">
                              <Button
                                variant="outlined"
                                color="secondary"
                                onClick={() => onDelete?.(values.id)}
                                disabled={
                                  !analysePermission?.includes(
                                    PlaybookPermission.PlaybookItemFindingDelete,
                                  )
                                }
                              >
                                Remove
                              </Button>
                            </Box>
                            <Button
                              color="primary"
                              variant="outlined"
                              disabled={
                                !analysePermission?.includes(
                                  PlaybookPermission.PlaybookItemFindingUpdate,
                                )
                              }
                              onClick={() => {
                                setEditable(ActionEditable.Finding);
                                setEditMode(true);
                              }}
                              type="button"
                            >
                              Edit
                            </Button>
                          </Box>
                        )}
                      </Box>
                      <form onSubmit={handleSubmit}>
                        <Box
                          width="100%"
                          height="550px"
                          style={
                            loadingEditor
                              ? { display: 'flex' }
                              : { display: 'none' }
                          }
                          display="flex"
                          justifyContent="center"
                          alignItems="flex-end"
                        >
                          <CircularProgress />
                        </Box>
                        <Box
                          style={
                            loadingEditor ? { display: 'none' } : undefined
                          }
                        >
                          <Box mt={2} mb={2} display="flex">
                            <Grid container>
                              <Grid item xs={8}>
                                <Box
                                  mt={0.5}
                                  display="flex"
                                  alignItems="center"
                                >
                                  <Box mr={2}>
                                    <MixTitle
                                      title="Title"
                                      isRequired
                                      noMargin
                                    />
                                  </Box>
                                  {/* <Box> */}
                                  <TextField
                                    value={values.title}
                                    name="title"
                                    fullWidth
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    variant="outlined"
                                    required
                                    disabled={
                                      isSubmitting ||
                                      !editMode ||
                                      editable !== ActionEditable.Finding
                                    }
                                    error={touched && errors && !!errors.title}
                                  />
                                  {/* </Box> */}
                                </Box>
                              </Grid>
                              <Grid item xs={4}>
                                <Box display="flex" flexDirection="row">
                                  <Box
                                    ml={4}
                                    display="flex"
                                    alignItems="center"
                                  >
                                    <MixTitle title="Public:" noMargin />
                                    <Checkbox
                                      value={values.public_flg}
                                      checked={values.public_flg}
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      disabled={
                                        isSubmitting ||
                                        !editMode ||
                                        editable !== ActionEditable.Finding
                                      }
                                      color="primary"
                                      name="public_flg"
                                      id="public_flg"
                                    />
                                  </Box>
                                  {/* {values.id !== 0 && ( */}
                                  <Box
                                    ml={4}
                                    display="flex"
                                    alignItems="center"
                                  >
                                    <MixTitle title="Tags:" noMargin />
                                    <LocalOfferIcon
                                      style={{ cursor: 'pointer' }}
                                      onClick={() => {
                                        setShowTagDialog(true);
                                      }}
                                    />
                                  </Box>
                                  {/* )} */}
                                </Box>
                              </Grid>
                            </Grid>
                          </Box>
                          <Box>
                            <MixTitle title="Description" />
                            <Box minHeight="400px" width="100%">
                              <TinyMCEForm
                                value={values.description ?? ''}
                                disabled={
                                  isSubmitting ||
                                  !editMode ||
                                  editable !== ActionEditable.Finding
                                }
                                setLoadingEditor={setLoadingEditor}
                                setValue={(content: string) => {
                                  setFieldValue('description', content);
                                }}
                              />
                            </Box>
                          </Box>
                        </Box>
                      </form>
                    </Box>
                  )}
                  <FormDialog
                    title="Tags"
                    submit={handleSubmitTags}
                    open={showTagDialog}
                    setOpen={setShowTagDialog}
                    isAddingForm={false}
                    labelBtn={'OK'}
                    disable={tagForm.length === 0 || !editMode}
                    formContent={
                      <Box mt={2} mb={2}>
                        <Autocomplete
                          multiple
                          id="multiple-limit-tags"
                          onChange={(e, value) => {
                            setTagForm(
                              _.uniqBy(
                                value?.map(item => item),
                                'id',
                              ) ?? [],
                            );
                          }}
                          defaultValue={
                            item.id === 0
                              ? tagForm
                              : item.tags?.map(item => item.tag)
                          }
                          options={tagReferences ?? []}
                          getOptionLabel={option => option?.name ?? ''}
                          renderInput={params => (
                            <TextField
                              {...params}
                              variant="outlined"
                              placeholder="Add tag"
                            />
                          )}
                          disabled={
                            !editMode || editable !== ActionEditable.Finding
                          }
                        />
                      </Box>
                    }
                  />
                </>
              );
            }}
          </Formik>

          <ExportFinding
            open={isShare}
            close={() => {
              setIsShare(false);
            }}
            finding={item}
          />
        </Box>
      </Grid>
    </>
  );
};
