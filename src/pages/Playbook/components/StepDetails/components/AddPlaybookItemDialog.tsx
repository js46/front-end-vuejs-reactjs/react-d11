import React from 'react';
import { Box, Typography, Button, TextField } from '@material-ui/core';
import { Formik, FieldArray } from 'formik';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles } from '@material-ui/styles';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import { DeleteIcon } from '../../../../../assets/icon';

import { INewPlaybookItem } from '../../../../../services/playbook.services';
import { useDisableMultipleClick } from '../../../../../hooks';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { AddPbItemFormState } from '../../../../../store/playbook/selector';
import {
  RequestAddingPbItem,
  CloseAddingDialogPb,
} from '../../../../../store/playbook/actions';
import { FormDialog } from '../../../../../components';

const useStyles = makeStyles(theme => ({
  icon: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

interface InitValueT {
  template: INewPlaybookItem[];
  isOpenCollapse: number | undefined;
}

export const AddPlaybookItemDialog: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const formAdding = useSelector(AddPbItemFormState);
  const initialValues: InitValueT = {
    template: [
      {
        title: '',
        description: '',
        item_order: formAdding?.item_order ?? 0,
        playbook_item_parent_id: formAdding?.parent ?? 0,
      },
    ],
    isOpenCollapse: undefined,
  };
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const level = formAdding?.position.length ?? 1;
  const indexItem = formAdding?.position[level - 1] ?? 0;
  const position = formAdding?.position?.slice(0, -1);
  const orderItem = formAdding?.item_order ?? 1;
  const renderLabelItem = (index: number) => {
    if (position && position.length) {
      return [...position, indexItem + index + 1].join('.');
    }
    return indexItem + index + 1;
  };

  const handleAddPlaybookItem = async (values: INewPlaybookItem[]) => {
    if (isSubmitting) {
      return;
    }
    const payload = {
      playbook_items: values,
    };
    await debounceFn();
    dispatch(RequestAddingPbItem(payload));
    endRequest();
  };
  const onCancel = () => {
    dispatch(CloseAddingDialogPb());
  };
  const { t } = useTranslation();
  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={values => {
        handleAddPlaybookItem(values.template);
      }}
    >
      {({ values, handleChange, setFieldValue, handleSubmit }) => (
        <FormDialog
          title={
            formAdding?.parent
              ? t('add_sub_playbook_item')
              : t('add_playbook_item')
          }
          submit={handleSubmit}
          open={true}
          setOpen={onCancel}
          isAddingForm={true}
          disable={
            values.template.every(tp => {
              if (tp.title === '' && tp.description === '') {
                return true;
              }
              return false;
            }) || isSubmitting
          }
          formContent={
            <Box padding="24px 25px">
              <FieldArray name="template">
                {arrayHelpers => (
                  <Box>
                    {values.template.map((item, index) => {
                      const animatedStyle = {
                        marginRight: '5px',
                        transition: '0.3s all',
                        transform:
                          values.isOpenCollapse === index
                            ? 'rotate(90deg)'
                            : 'rotate(0deg)',
                        '&:hover': {
                          cursor: 'poiter',
                        },
                      };
                      return (
                        <Box mb={3} key={index}>
                          <Box mt={2}>
                            <Box display="flex" mb={2} alignItems="center">
                              <Box
                                display="flex"
                                minWidth={110}
                                alignItems="center"
                              >
                                <Typography component="span" variant="caption">
                                  Step {renderLabelItem(index)}
                                </Typography>

                                <Box ml={1} display="flex" alignItems="center">
                                  <ArrowForwardIosIcon
                                    style={animatedStyle}
                                    className={classes.icon}
                                    fontSize="small"
                                    onClick={() => {
                                      if (values.isOpenCollapse === index) {
                                        setFieldValue(
                                          'isOpenCollapse',
                                          undefined,
                                        );
                                      } else {
                                        setFieldValue('isOpenCollapse', index);
                                      }
                                    }}
                                  />
                                </Box>
                              </Box>
                              <Box mr={1} display="flex">
                                <Typography component="span" variant="caption">
                                  Name:
                                </Typography>
                              </Box>

                              <TextField
                                variant="outlined"
                                fullWidth
                                name={`template.${index}.title`}
                                value={item?.title ?? ''}
                                onChange={handleChange}
                              />
                              <Box ml={2}>
                                <DeleteIcon
                                  className={classes.icon}
                                  onClick={() => {
                                    arrayHelpers.remove(index);
                                  }}
                                />
                              </Box>
                            </Box>
                            {values.isOpenCollapse === index && (
                              <Box pl={9}>
                                <Box>
                                  <Box mb={1}>
                                    <Typography component="p" variant="caption">
                                      Detail:
                                    </Typography>
                                  </Box>
                                  <TextField
                                    fullWidth
                                    multiline
                                    rows={5}
                                    name={`template.${index}.description`}
                                    onChange={handleChange}
                                    variant="outlined"
                                    value={item?.description ?? ''}
                                  />
                                </Box>
                              </Box>
                            )}
                          </Box>
                        </Box>
                      );
                    })}
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<ControlPointIcon />}
                      onClick={() => {
                        arrayHelpers.push({
                          title: '',
                          description: '',
                          playbook_item_parent_id: formAdding?.parent ?? 0,
                          item_order: orderItem + values.template.length,
                        });
                      }}
                    >
                      Add Step
                    </Button>
                  </Box>
                )}
              </FieldArray>
            </Box>
          }
        />
      )}
    </Formik>
  );
};

export default AddPlaybookItemDialog;
