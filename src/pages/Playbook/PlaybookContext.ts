import { createContext } from 'react';
import { ResponsePlayBookT } from '../../services/playbook.services';
import { ITag } from '../../services/references.services';

export type PlaybookContextT = {
  onRefresh?: () => void;
  selectedItem?: ResponsePlayBookT;
  setLabel?: (label: string) => void;
  label?: string;
  selectedItemID?: number;
  tagReferences?: ITag[];
  setSelectedItemID?: (value: number) => void;
};

const PlaybookContext = createContext<PlaybookContextT>({});

export default PlaybookContext;
