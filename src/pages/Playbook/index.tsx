import React, { useState, useEffect } from 'react';
import { Chativity, StepDetails, StepMenu, Wizard } from './components';
import { SectionBox, TabFluid, TStringBoolean } from '../../components/styled';
import { Breadcrumb } from '../../layouts/main/components';
import { Box, Grid, Tabs, Typography, Button } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { a11yProps, generateOpportunityLink } from '../../utils';
import { TabPanel } from '../../components';
import PlaybookContext from './PlaybookContext';
import { ResponsePlayBookT } from '../../services/playbook.services';
import { useParams, useHistory, useLocation } from 'react-router-dom';
import { useEffectOnlyOnce } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { RequestOpportunityPermission } from '../../store/opportunity/actions';
import { RequestOppPlaybookDetail } from '../../store/playbook/actions';
import { PlaybookDetailState } from '../../store/playbook/selector';
import ArrowBackOutlined from '@material-ui/icons/ArrowBackOutlined';
import { ITag, getListTags } from '../../services/references.services';

const Playbook = () => {
  const query = new URLSearchParams(useLocation().search);
  const stepID = Number(query.get('step') ?? 0);
  const [activeSidebarTab, setSidebarActiveTab] = useState(0);
  const playbookDetail = useSelector(PlaybookDetailState);
  const [selectedItem, setSelectedItem] = useState<ResponsePlayBookT>();
  const [selectedItemID, setSelectedItemID] = useState<number>(stepID);
  const [label, setLabel] = useState<string>('1');
  const [tagReferences, setTagReferences] = useState<ITag[]>([]);
  const { opportunityID, playbookID } = useParams();
  const [loading, setLoading] = useState<boolean>(true);
  const dispatch = useDispatch();
  const history = useHistory();
  const { i18n } = useTranslation();

  useEffect(() => {
    if (playbookDetail) {
      if (!selectedItemID) {
        setSelectedItemID(playbookDetail?.playbook_items?.[0]?.id ?? 0);
      } else {
        setSelectedItem(
          playbookDetail?.playbook_items?.find(
            item => item.id === selectedItemID,
          ),
        );
        if (loading) setLoading(false);
      }
    }
  }, [loading, playbookDetail, selectedItemID]);
  useEffectOnlyOnce(() => {
    if (!opportunityID || !playbookID) return;
    dispatch(RequestOppPlaybookDetail(playbookID));
    dispatch(RequestOpportunityPermission(opportunityID));
    getListTagsReferences();
  });
  const getListTagsReferences = async () => {
    const response = await getListTags([{ key: 'limit', value: 1000 }]);
    if (response.success) {
      setTagReferences(response.data.list ?? []);
    }
  };
  const onRefresh = () => {
    dispatch(RequestOppPlaybookDetail(playbookID));
  };
  const onChangeSidebarTab = (
    event: React.ChangeEvent<{}>,
    newValue: number,
  ) => {
    setSidebarActiveTab(newValue);
  };
  const goBackOpp = () => {
    const pathname = generateOpportunityLink(
      opportunityID,
      'analyse',
      'analysis',
    );
    history.push(pathname);
  };
  return (
    <PlaybookContext.Provider
      value={{
        setLabel,
        label,
        onRefresh,
        selectedItem,
        selectedItemID,
        setSelectedItemID,
        tagReferences,
      }}
    >
      {/* Start Left Menu */}
      <StepMenu />
      {/* End Left Menu */}
      <SectionBox
        data-has-sidebar={TStringBoolean.TRUE}
        component="section"
        style={{ paddingTop: 0 }}
      >
        <Breadcrumb />

        <Box mt={2}>
          <Box mb={3}>
            <Grid container item lg={12} spacing={1}>
              <Grid container item lg={9} xs={9}>
                <Box display="flex" flex={1} alignItems="center">
                  <Typography variant="h4" component="div">
                    {playbookDetail?.name}
                  </Typography>
                  <Box display="flex" flex={1} justifyContent="flex-end">
                    <Button
                      onClick={goBackOpp}
                      variant="text"
                      startIcon={<ArrowBackOutlined />}
                    >
                      {i18n.t('back_to_opp')}
                    </Button>
                  </Box>
                </Box>
              </Grid>
            </Grid>
            <Grid container spacing={4}>
              {/* Start Main content */}
              <Grid item xs={12} lg={9}>
                {!loading && (
                  <Box mt={1}>
                    {selectedItem ? (
                      <StepDetails />
                    ) : (
                      <Typography>{i18n.t('playbook_has_no_item')}</Typography>
                    )}
                  </Box>
                )}
              </Grid>
              {/* End Main content */}
              {/* Start Right sidebar tabs */}
              <Grid item xs={12} lg={3}>
                <Tabs
                  value={activeSidebarTab}
                  onChange={onChangeSidebarTab}
                  aria-label="sidebar tabs"
                >
                  <TabFluid
                    label="Chativity"
                    {...a11yProps('sidebar-tab', 0)}
                  />
                  <TabFluid label="Wizard" {...a11yProps('sidebar-tab', 1)} />
                </Tabs>
                <TabPanel value={activeSidebarTab} index={0}>
                  <Chativity opportunityID={opportunityID} />
                </TabPanel>
                <TabPanel value={activeSidebarTab} index={1}>
                  <Wizard />
                </TabPanel>
              </Grid>
              {/* End Right sidebar tabs */}
            </Grid>
          </Box>
        </Box>
      </SectionBox>
    </PlaybookContext.Provider>
  );
};
export default Playbook;
