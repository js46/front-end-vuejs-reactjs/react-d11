import React from 'react';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { useSelector } from 'react-redux';
import { Box, Button, Grid, Typography } from '@material-ui/core';
import {
  StyledDataSearch,
  StyledReactiveComponent,
} from '../../components/styled';
import { SelectedFiltersCustom } from '../../components';
import { makeStyles } from '@material-ui/core/styles';
import { useCustomSearch } from '../../hooks';
import { ConfigESIndex } from '../../store/config/selector';
import { AuditTable, SidebarFilter } from './components';
import { SearchSuggestion } from './components/SearchSuggestion';

export const Audit = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const mustQuery = [
    {
      exists: {
        field: 'action_name',
      },
    },
    {
      exists: {
        field: 'object_type',
      },
    },
  ];
  const {
    searchBtnRef,
    value,
    onClearSearch,
    onValueSelected,
    onChange,
    onValueChange,
    onClickSearch,
    searchQuery,
  } = useCustomSearch(
    ['action_name', 'user_name', 'client_ip', 'object_type'],
    'or',
    mustQuery,
  );

  const defaultQuery = () => ({
    query: {
      bool: {
        must: mustQuery,
      },
    },
  });

  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        <Typography variant="h4">Audit</Typography>
        <br />
      </Grid>
      <ReactiveBase
        app={ES_INDICES.event_log_index_name_prefix + '*'}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <Grid container item>
          <Grid item lg={2} xl={2} sm={12} xs={12}>
            <SidebarFilter defaultQuery={defaultQuery} />
          </Grid>
          <Grid item lg={10} xl={10} xs={12} sm={12}>
            {/* Start Search keyword */}
            <Grid container>
              <Grid item lg={10} xl={10} xs={12} sm={12}>
                <StyledDataSearch
                  componentId="SearchInput"
                  dataField={[
                    'action_name',
                    'user_name',
                    'client_ip',
                    'object_type',
                  ]}
                  showDistinctSuggestions
                  queryFormat="or"
                  placeholder="Search for activity..."
                  iconPosition="left"
                  showClear={true}
                  showFilter={false}
                  innerClass={{
                    input: 'searchInput',
                    title: 'titleSearch',
                  }}
                  defaultQuery={() => searchQuery}
                  parseSuggestion={suggestion => {
                    return {
                      label: <SearchSuggestion data={suggestion.source} />,
                      value: suggestion.source.object_type,
                      source: suggestion.source,
                    };
                  }}
                  onChange={onChange}
                  value={value}
                  onValueChange={onValueChange}
                  onValueSelected={onValueSelected}
                />
              </Grid>
              <Grid item lg={2} xl={2} xs={12} sm={12}>
                <Box
                  display="flex"
                  justifyContent={{
                    xs: 'flex-start',
                    sm: 'flex-start',
                    lg: 'flex-end',
                    xl: 'flex-end',
                  }}
                >
                  <StyledReactiveComponent
                    componentId="Search"
                    filterLabel="Keyword"
                    showFilter={true}
                    render={({ setQuery }) => (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => onClickSearch(setQuery)}
                        ref={searchBtnRef}
                      >
                        Search
                      </Button>
                    )}
                  />
                </Box>
              </Grid>
            </Grid>
            {/* End Search keyword */}
            {/* Start Search filter selections */}
            <Grid container className={classes.root}>
              <SelectedFiltersCustom onClearSearch={onClearSearch} />
            </Grid>
            {/* End Search filter selections */}
            {/* Start Result list */}
            <Grid container>
              <Grid item xs={12}>
                <AuditTable defaultQuery={defaultQuery} />
              </Grid>
            </Grid>
            {/* End Result list */}
          </Grid>
        </Grid>
      </ReactiveBase>
    </Grid>
  );
};
const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  heading: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
  },
  icon: {
    textAlign: 'center',
  },
  expandIcon: {
    margin: '0px!important',
  },
  dataSearch: {
    fontSize: '14px!important',
  },
  searchBtn: {
    height: '42px',
    background: '#2C54E3',
    textAlign: 'center',
    borderRadius: '6px',
    border: '1px solid #2C54E3',
  },
  textBtn: {
    width: '68.54px',
    height: '23px',
    fontSize: '14px',
    lineHeight: '20px',
    textAlign: 'center',
  },
}));

export default Audit;
