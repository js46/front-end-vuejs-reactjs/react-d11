import React from 'react';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Box, Typography } from '@material-ui/core';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { StyledDatePicker, StyledMultiList } from '../../../components/styled';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import { makeStyles } from '@material-ui/core/styles';
import { datePickerInputProps } from '../../../utils';

interface SidebarFilterProps {
  defaultQuery?: () => object;
}
export const SidebarFilter: React.FC<SidebarFilterProps> = ({
  defaultQuery,
}) => {
  const classes = useStyles();

  return (
    <>
      {/* Start Filter by Date */}
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">Date</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledDatePicker
              componentId="Date"
              dataField="time"
              queryFormat="date"
              dayPickerInputProps={datePickerInputProps}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {/* End Filter by Date */}
      {/* Start Filter by Object type */}
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">Object Types</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Object types"
              dataField="object_type.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: ['Keyword', 'Date', 'Actions', 'Users'],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {/* End Filter by Object type */}
      {/* Start Filter by Action */}
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">Actions</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Actions"
              dataField="action_name.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: ['Keyword', 'Date', 'Object types', 'Users'],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {/* End Filter by Action */}
      {/* Start Filter by User */}
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">Users</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Users"
              dataField="user_name.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: ['Keyword', 'Date', 'Object types', 'Actions'],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {/* End Filter by User */}
    </>
  );
};
const useStyles = makeStyles(theme => ({
  filterSummary: {
    padding: '0px 5px 0px 0px',
    margin: '0px',
  },
  filterSummaryContent: {
    margin: 0,
    padding: 0,
    // '&$expanded': {
    //   margin: 'auto',
    //   padding: '0px',
    // },
  },
  filterDetail: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
  },
  lineDivider: {
    border: 1,
    borderColor: '#f4f4f4',
    borderStyle: 'solid',
  },
  panel: {
    margin: '0px!important',
    boxShadow: 'none',
    '&::before': {
      background: 'none!important',
    },
  },
  icon: {
    textAlign: 'center',
  },
  expandIcon: {
    margin: '0px!important',
  },
  inputDate: {
    display: 'block !important',
    '& > div': {
      border: 'none !important',
      '& input': {
        borderRadius: '10px !important',
        border: '1px solid #ccc !important',
        marginBottom: '10px',
      },
      '&:nth-child(2)': {
        display: 'none',
      },
    },
    border: 'none !important',
  },
}));
