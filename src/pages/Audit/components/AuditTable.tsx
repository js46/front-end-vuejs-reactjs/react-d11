import React from 'react';
import {
  ESLoader,
  ESNoResult,
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from '../../../components';
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@material-ui/core';
import { ILogEvent } from '../../../services/log.services';
import moment from 'moment';
import { StyledReactiveList } from '../../../components/styled';
import { useTableHeadSorter } from '../../../hooks';
import { makeStyles } from '@material-ui/core/styles';

const tableFields = [
  'time',
  'object_type.keyword',
  'response_body.data.title.keyword',
  'action_name.keyword',
  'user_name.keyword',
  'status',
  'client_ip',
] as const;

const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'time',
    label: 'Time',
    disablePadding: true,
  },
  {
    id: 'object_type.keyword',
    label: 'Type',
    disablePadding: true,
  },
  {
    id: 'response_body.data.title.keyword',
    label: 'Description',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'action_name.keyword',
    label: 'Action',
    disablePadding: true,
  },
  {
    id: 'user_name.keyword',
    label: 'Performed by',
    disablePadding: true,
  },
  {
    id: 'client_ip',
    label: 'IP address',
    disablePadding: true,
  },
  {
    id: 'status',
    label: 'Status',
    disablePadding: true,
  },
];

interface AuditTableProps {
  defaultQuery?: () => object;
}
export const AuditTable: React.FC<AuditTableProps> = ({ defaultQuery }) => {
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'time',
    SortOrder.DESC,
  );
  const classes = useStyles();

  const getAuditTitle = (data: ILogEvent) => {
    return (
      data.user_name +
      ' ' +
      data.action_name.toLowerCase() +
      ' ' +
      data.object_type.toLowerCase()
    );
  };

  return (
    <StyledReactiveList
      componentId="SearchResult"
      react={{
        and: ['Date', 'Search', 'Actions', 'Object types', 'Users'],
      }}
      dataField={orderBy}
      sortBy={order}
      size={10}
      paginationAt="bottom"
      pages={5}
      pagination={true}
      showLoader
      loader={<ESLoader />}
      renderNoResults={() => <ESNoResult msgKey="no_data" />}
      renderResultStats={function(stats) {
        return (
          <Grid container>
            <Grid item xs>
              <Typography component="h1" align="left">
                {stats.numberOfResults.toLocaleString()} Results
              </Typography>
            </Grid>
          </Grid>
        );
      }}
      defaultQuery={defaultQuery}
      render={({ data, loading }) => {
        if (loading) return null;
        return (
          <TableContainer classes={{ root: classes.rootTableContainer }}>
            <Table classes={{ root: classes.rootTable }}>
              <TableHeadSorter
                onRequestSort={handleRequestSort}
                order={order}
                orderBy={orderBy}
                cells={headCells}
              />
              <TableBody>
                {data.map((item: ILogEvent, index: number) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {item.time !== '0001-01-01T00:00:00Z' &&
                        moment(item.time).format('DD/MM/YYYY hh:mm A')}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.object_type}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {getAuditTitle(item)}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.action_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.user_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.client_ip}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.status}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        );
      }}
      innerClass={{
        button: classes.buttonPagination,
        resultsInfo: classes.resultsInfo,
        sortOptions: classes.sortSelect,
      }}
    />
  );
};
const useStyles = makeStyles(theme => ({
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  rootTableContainer: {
    marginTop: 15,
  },
}));
