import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { ILogEvent } from '../../../services/log.services';

interface SearchSuggestionProps {
  data: ILogEvent;
}

export const SearchSuggestion: React.FC<SearchSuggestionProps> = ({ data }) => {
  // console.log('Suggest', data);
  return (
    <div>
      <Grid container>
        <Grid item xs={12}>
          <Box fontWeight="fontWeightBold">{data.action_name}</Box>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <span style={{ fontStyle: 'italic' }}>Type: </span>
          {data.object_type}
          {' | '}
          <span style={{ fontStyle: 'italic' }}>By: </span>
          {data.user_name}
        </Grid>
      </Grid>
    </div>
  );
};
