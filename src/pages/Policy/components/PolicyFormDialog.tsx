import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  FormControl,
  Select,
  MenuItem,
  DialogActions,
  Button,
  Typography,
  Box,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { MixTitle } from '../../../components';
import { useDisableMultipleClick } from '../../../hooks';
import { useTranslation } from 'react-i18next';

const METHODS = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'];
export const PolicyFormDialog: React.FC<{
  open: boolean;
  closeDialog: () => void;
  refreshData: () => void;
  policy?: any;
}> = ({ open, closeDialog, refreshData, policy }) => {
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const initialValues = {
    name: policy?.name ?? '',
    path: policy?.path ?? '',
    method: policy?.method ?? 'GET',
  };

  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object({
      name: Yup.string().required('This field is required'),
      path: Yup.string().required('This field is required'),
      method: Yup.string().required('This field is required'),
    }),
    onSubmit: async values => {
      await debounceFn();
      endRequest();
      refreshData();
      closeDialog();
    },
  });
  const { t } = useTranslation();

  return (
    <Dialog open={open} onClose={closeDialog} maxWidth="sm" fullWidth>
      <form onSubmit={formik.handleSubmit}>
        <Box p={4} pl={2} pr={2}>
          <DialogTitle>
            <Box textAlign="center">
              <Typography component="p" variant="h4">
                {policy ? t('update') : t('add')} Policy
              </Typography>
            </Box>
          </DialogTitle>
          <DialogContent>
            <Box mb={2}>
              <MixTitle title="Name" isRequired />
              <TextField
                margin="normal"
                required
                fullWidth
                variant="outlined"
                id="name"
                name="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && !!formik.errors.name}
                helperText={formik.touched.name ? formik.errors.name : ''}
              />
            </Box>

            <Box mb={2}>
              <MixTitle title="Path" isRequired />
              <TextField
                margin="normal"
                required
                variant="outlined"
                fullWidth
                id="path"
                name="path"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.path}
                error={formik.touched.path && !!formik.errors.path}
                helperText={formik.touched.path ? formik.errors.path : ''}
              />
            </Box>

            <Box mb={2}>
              <MixTitle title="Method" isRequired />
              <FormControl fullWidth margin="normal" variant="outlined">
                <Select
                  value={formik.values.method}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  id="method"
                  name="method"
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                  IconComponent={ExpandMoreIcon}
                  error={formik.touched.method && !!formik.errors.method}
                >
                  {METHODS.map(item => {
                    return (
                      <MenuItem key={item} value={item}>
                        {item}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={closeDialog} variant="outlined">
              Cancel
            </Button>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              disabled={isSubmitting}
            >
              Save
            </Button>
          </DialogActions>
        </Box>
      </form>
    </Dialog>
  );
};
