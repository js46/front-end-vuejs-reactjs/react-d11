import React, { useState } from 'react';
import {
  Box,
  Button,
  Grid,
  IconButton,
  InputAdornment,
  OutlinedInput,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import PolicyIcon from '@material-ui/icons/Policy';
import { DeleteIconButton, EditIconButton } from '../../components';
import { Pagination } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import { PolicyFormDialog } from './components';
import { useTranslation } from 'react-i18next';

export const Policy = () => {
  const [editItem, setEditItem] = useState();
  const { t } = useTranslation();
  const [formDialogOpened, setFormDialogOpened] = useState(false);
  const handleOpenFormDialog = (item?: any) => {
    setEditItem(item);
    setFormDialogOpened(true);
  };
  const data = [
    {
      id: 1,
      name: 'Get user list',
      method: 'GET',
      path: '/users',
    },
  ];
  const classes = useStyles();
  // const numberOfPages = Math.ceil(total / PAGE_SIZE);

  return (
    <Grid>
      <Grid item xs={12}>
        <Box mt={2} mb={2}>
          <Typography variant="h4">{t('policies')}</Typography>
        </Box>
      </Grid>
      <Grid item container>
        <Grid item xs>
          <OutlinedInput
            name="keyword"
            placeholder="Search a keyword..."
            // value={keyword}
            // onChange={event => setKeyword(event.target.value)}
            // onKeyDown={event => (event.keyCode === 13 ? onSearch() : null)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton type="submit">
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            }
          />
        </Grid>
        <Grid item xs container justify="flex-end">
          <Button
            startIcon={<PolicyIcon />}
            variant="contained"
            color="primary"
            onClick={() => handleOpenFormDialog()}
          >
            Add
          </Button>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <TableContainer className={classes.rootTableContainer}>
          <Table className={classes.rootTable}>
            <TableHead>
              <TableRow>
                <TableCell style={{ width: 20 }}>ID</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Method</TableCell>
                <TableCell>Path</TableCell>
                <TableCell align="center" style={{ width: 95 }}>
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map(item => (
                <TableRow key={item.id}>
                  <TableCell>{item.id}</TableCell>
                  <TableCell>{item.name}</TableCell>
                  <TableCell>{item.method}</TableCell>
                  <TableCell>{item.path}</TableCell>
                  <TableCell align="center">
                    <EditIconButton
                      onClick={() => handleOpenFormDialog(item)}
                    />
                    <DeleteIconButton />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Box display="flex" justifyContent="center" p={3}>
          <Pagination
            page={1}
            count={1}
            variant="outlined"
            shape="rounded"
            // onChange={onChangePage}
          />
        </Box>
      </Grid>
      {formDialogOpened && (
        <PolicyFormDialog
          open={formDialogOpened}
          policy={editItem}
          closeDialog={() => setFormDialogOpened(false)}
          refreshData={() => console.log('Policy refreshed')}
        />
      )}
    </Grid>
  );
};
const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
}));

export default Policy;
