import React from 'react';
import { Formik, FieldArray } from 'formik';
import clsx from 'clsx';
import {
  TextField,
  Button,
  MenuItem,
  Grid,
  Box,
  Typography,
  Select,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import { StepButtonDetail } from './StepButton';
import { IOpportunityParam } from '../../../services/opportunity.services';
import { ICaptureBenefitsProps } from '../types';
import { MixTitle } from '../../../components';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(8),
  },
  inlineBlock: {
    display: 'inline-block',
  },
  padding: {
    padding: theme.spacing(4),
  },
  dflex: {
    display: 'flex',
    alignItems: 'center',
  },
  form: {
    margin: 0,
  },
  getBoldText: {
    fontWeight: 700,
  },
  totalMoneyStyle: {
    display: 'inline',
    margin: '0 5px',
  },
  numberInput: {
    marginLeft: '5px',
    marginRight: '5px',
    '& input': {
      textAlign: 'end',
    },
  },
  addButton: {
    padding: '0 20px',
  },
  opporTitle: {
    fontSize: '14px',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  removeBtn: {
    marginLeft: '10px',
    padding: '0 20px',
    textTransform: 'capitalize',
  },
  properMargin: {
    '& .MuiFormControl-marginNormal': {
      margin: 0,
    },
  },
}));

export const CaptureBenefits: React.FC<ICaptureBenefitsProps> = ({
  opportunity,
  corporateObjectives,
  benefitFocuses,
  benefitFocusValues,
  handleNext,
  handleBack,
  handleOpportunity,
  deleteOpportunity,
}) => {
  let fieldSubmit = '';
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const initialValues: IOpportunityParam = {
    benefit_metrics: opportunity?.benefit_metrics || [''],
    benefit_focuses: opportunity?.benefit_focuses
      ? opportunity.benefit_focuses.map(item => ({
          benefit_focus_id: item.benefit_focus_id,
          benefit_focus_value_id: item.benefit_focus_value.id,
        }))
      : [
          {
            benefit_focus_id: 0,
            benefit_focus_value_id: 0,
          },
        ],
    corporate_objectives:
      opportunity && opportunity.corporate_objectives
        ? opportunity.corporate_objectives.map(item => item.id)
        : [0],
  };

  if (!opportunity) return <></>;

  Yup.addMethod(Yup.array, 'unique', function(
    mapper = (a: any) => a,
    // eslint-disable-next-line
    message: string = '${path} may not have duplicates',
  ) {
    return this.test('unique', message, list => {
      return list.length === new Set(list.map(mapper)).size;
    });
  });

  const checkAddDisabled = (selections?: number[]) => {
    return selections && selections.some(item => item === 0);
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={Yup.object().shape({
        benefit_metrics: Yup.array()
          .of(Yup.string())
          .unique((s: any) => s, 'This field should be unique'),
      })}
      onSubmit={async values => {
        await handleOpportunity(values, fieldSubmit);
      }}
    >
      {({
        isSubmitting,
        // dirty,
        // isValid,
        values,
        handleChange,
        handleSubmit,
        // handleBlur,
        touched,
        errors,
        setFieldValue,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box>
            <Box mb="20px">
              <Typography variant="h4">Business Benefits</Typography>
            </Box>
            <Box mb="20px">
              <Typography component="span">
                {t('please_enter_benefits')}
              </Typography>
              <Typography component="span" className={classes.opporTitle}>
                {' '}
                {opportunity.title ?? ''}{' '}
              </Typography>
              <Typography component="span">
                {t('in_the_fields_below')}
              </Typography>
            </Box>
            <Box mb="20px">
              <MixTitle
                isHelper={true}
                title={t('alignment_to_corporate_objectives')}
                helperText={i18n.t('capture.help_text.corporate_objectives')}
              />
              <FieldArray name="corporate_objectives">
                {arrayHelpers => {
                  return (
                    <>
                      <Box>
                        {values.corporate_objectives &&
                          values.corporate_objectives.map((item, index) => (
                            <Box key={index} mb="20px">
                              <Grid container>
                                <Grid item xs={5} className={classes.dflex}>
                                  <Box mr="15px">
                                    <Typography>{index + 1}.</Typography>
                                  </Box>

                                  <Select
                                    value={item ? item : ''}
                                    variant="outlined"
                                    fullWidth
                                    name={`corporate_objectives.${index}`}
                                    onChange={e => {
                                      handleChange(e);
                                      if (
                                        !values.corporate_objectives?.filter(
                                          item => item,
                                        ).length
                                      ) {
                                        fieldSubmit =
                                          'add_corporate_objectives';
                                      } else {
                                        fieldSubmit = 'corporate_objectives';
                                      }
                                      if (
                                        opportunity?.corporate_objectives
                                          .length > 0 &&
                                        e.target.value === 0
                                      ) {
                                        fieldSubmit =
                                          'remove_corporate_objectives';
                                      }

                                      handleSubmit();
                                    }}
                                    IconComponent={ExpandMoreIcon}
                                  >
                                    <MenuItem value={0}>
                                      <Typography component="i">
                                        None
                                      </Typography>
                                    </MenuItem>
                                    {corporateObjectives
                                      .filter(
                                        item =>
                                          item.id ===
                                            values.corporate_objectives![
                                              index
                                            ] ||
                                          (!values.corporate_objectives!.includes(
                                            item.id,
                                          ) &&
                                            !item.archive),
                                      )

                                      .map(item => (
                                        <MenuItem key={item.id} value={item.id}>
                                          {item.name}
                                        </MenuItem>
                                      ))}
                                  </Select>
                                </Grid>
                                {index > 0 && (
                                  <Grid
                                    item
                                    xs={1}
                                    container
                                    alignItems="center"
                                  >
                                    <Button
                                      className={classes.removeBtn}
                                      variant="outlined"
                                      onClick={() => {
                                        if (isSubmitting) {
                                          return;
                                        }
                                        fieldSubmit =
                                          'remove_corporate_objectives';
                                        handleSubmit();
                                        arrayHelpers.remove(index);
                                      }}
                                      // aria-label="close"
                                      // disabled={isSubmitting}
                                    >
                                      {t('remove')}
                                    </Button>
                                  </Grid>
                                )}
                              </Grid>
                            </Box>
                          ))}
                      </Box>
                      <Grid container>
                        <Grid item xs={5}>
                          <Box textAlign="center">
                            {values.corporate_objectives &&
                              values.corporate_objectives!.length <
                                corporateObjectives.length && (
                                <Button
                                  variant="outlined"
                                  className={classes.addButton}
                                  onClick={() => {
                                    if (isSubmitting) {
                                      return;
                                    }
                                    arrayHelpers.push(0);
                                    if (
                                      !values.corporate_objectives?.filter(
                                        item => item,
                                      ).length
                                    ) {
                                      return;
                                    }
                                    fieldSubmit = 'corporate_objectives';
                                    handleSubmit();
                                  }}
                                  disabled={checkAddDisabled(
                                    values.corporate_objectives,
                                  )}
                                >
                                  {t('add')}
                                </Button>
                              )}
                          </Box>
                        </Grid>
                      </Grid>
                    </>
                  );
                }}
              </FieldArray>
            </Box>
            <Box mb="20px" mt={8}>
              <Box>
                <Grid container>
                  <Grid item xs={5}>
                    <MixTitle
                      isHelper={true}
                      title="Business Focus Category"
                      helperText={i18n.t('capture.help_text.benefit_focuses')}
                    />
                  </Grid>
                  <Grid item xs={5}>
                    <Box ml="70px">
                      <MixTitle
                        isHelper={true}
                        title="Business Benefit Value p.a."
                        helperText={i18n.t('capture.help_text.benefit_value')}
                      />
                    </Box>
                  </Grid>
                </Grid>
              </Box>
              <FieldArray
                name="benefit_focuses"
                render={arrayHelpers => {
                  return (
                    <>
                      <Box>
                        {values.benefit_focuses &&
                          values.benefit_focuses.map((item, index) => (
                            <Box key={index} mb="20px">
                              <Grid container>
                                <Grid item xs={5} className={classes.dflex}>
                                  <Box mr="15px">
                                    <Typography>{index + 1}.</Typography>
                                  </Box>

                                  <Select
                                    variant="outlined"
                                    value={
                                      item.benefit_focus_id
                                        ? item.benefit_focus_id
                                        : ''
                                    }
                                    fullWidth
                                    name={`benefit_focuses[${index}].benefit_focus_id`}
                                    onChange={e => {
                                      handleChange(e);
                                      fieldSubmit = 'benefit_focuses';
                                      if (
                                        !values.benefit_focuses?.some(
                                          item =>
                                            item.benefit_focus_id ||
                                            item.benefit_focus_value_id,
                                        )
                                      ) {
                                        fieldSubmit = 'add_benefit_focuses';
                                      }
                                      if (
                                        opportunity?.benefit_focuses.length >
                                          0 &&
                                        e.target.value === 0
                                      ) {
                                        setFieldValue(
                                          `benefit_focuses[${index}].benefit_focus_value_id`,
                                          '',
                                        );
                                        fieldSubmit = 'remove_benefit_focuses';
                                      }
                                      handleSubmit();
                                    }}
                                    IconComponent={ExpandMoreIcon}
                                  >
                                    <MenuItem value={0}>
                                      <Typography component="i">
                                        None
                                      </Typography>
                                    </MenuItem>
                                    {benefitFocuses
                                      .filter(
                                        item =>
                                          item.id ===
                                            values.benefit_focuses![index]
                                              .benefit_focus_id ||
                                          (!values.benefit_focuses!.filter(
                                            bf =>
                                              bf.benefit_focus_id === item.id,
                                          ).length &&
                                            !item.archive),
                                      )
                                      .map(item => (
                                        <MenuItem key={item.id} value={item.id}>
                                          {item.name}
                                        </MenuItem>
                                      ))}
                                  </Select>
                                </Grid>
                                <Grid item xs={2}>
                                  <Box
                                    display="flex"
                                    alignItems="center"
                                    width="100%"
                                    ml="70px"
                                  >
                                    <Select
                                      variant="outlined"
                                      value={item.benefit_focus_value_id ?? ''}
                                      fullWidth
                                      name={`benefit_focuses[${index}].benefit_focus_value_id`}
                                      onChange={e => {
                                        handleChange(e);
                                        fieldSubmit = 'benefit_focuses';
                                        if (
                                          !values.benefit_focuses?.some(
                                            item =>
                                              item.benefit_focus_id ||
                                              item.benefit_focus_value_id,
                                          )
                                        ) {
                                          fieldSubmit = 'add_benefit_focuses';
                                        }
                                        handleSubmit();
                                      }}
                                      IconComponent={ExpandMoreIcon}
                                    >
                                      {benefitFocusValues.map(item => (
                                        <MenuItem key={item.id} value={item.id}>
                                          {item.band}
                                        </MenuItem>
                                      ))}
                                    </Select>
                                  </Box>
                                </Grid>
                                {index > 0 && (
                                  <Grid
                                    xs
                                    item
                                    container
                                    alignItems="center"
                                    style={{ marginLeft: '70px' }}
                                  >
                                    <Button
                                      variant="outlined"
                                      className={classes.removeBtn}
                                      onClick={() => {
                                        if (isSubmitting) {
                                          return;
                                        }
                                        fieldSubmit = 'remove_benefit_focuses';
                                        handleSubmit();
                                        arrayHelpers.remove(index);
                                      }}
                                      // disabled={isSubmitting}
                                    >
                                      {t('remove')}
                                    </Button>
                                  </Grid>
                                )}
                              </Grid>
                            </Box>
                          ))}
                      </Box>
                      <Grid container>
                        <Grid item xs={5}>
                          <Box textAlign="center">
                            {values.benefit_focuses!.length <
                              benefitFocuses.length && (
                              <Button
                                // color="primary"
                                variant="outlined"
                                className={classes.addButton}
                                onClick={() => {
                                  if (isSubmitting) {
                                    return;
                                  }
                                  arrayHelpers.push({
                                    benefit_focus_id: 0,
                                    benefit_focus_value_id: 0,
                                  });
                                  if (
                                    !values.benefit_focuses?.filter(
                                      item =>
                                        item.benefit_focus_id &&
                                        item.benefit_focus_value_id,
                                    ).length
                                  ) {
                                    return;
                                  }
                                  fieldSubmit = 'benefit_focuses';
                                  handleSubmit();
                                }}
                                disabled={checkAddDisabled(
                                  values.benefit_focuses?.map(
                                    item => item.benefit_focus_id,
                                  ),
                                )}
                              >
                                {t('add')}
                              </Button>
                            )}
                          </Box>
                        </Grid>
                      </Grid>
                    </>
                  );
                }}
              />
            </Box>
            <Box mt={8}>
              <MixTitle
                isHelper={true}
                title={t('business_benefit_metrics')}
                moreText={t('max_50_words')}
                helperText={i18n.t(
                  'capture.help_text.business_benefit_metrics',
                )}
              />
              <FieldArray
                name="benefit_metrics"
                render={arrayHelpers => {
                  return (
                    <>
                      <Box>
                        {values.benefit_metrics &&
                          values.benefit_metrics.map((item, index) => (
                            <Box mb="20px" key={index}>
                              <Grid container>
                                <Grid
                                  item
                                  xs={5}
                                  className={clsx(
                                    classes.dflex,
                                    classes.properMargin,
                                  )}
                                >
                                  <Box mr="15px">
                                    <Typography>{index + 1}.</Typography>
                                  </Box>
                                  <TextField
                                    value={item}
                                    variant="outlined"
                                    fullWidth
                                    className={clsx(classes.inlineBlock)}
                                    name={`benefit_metrics.${index}`}
                                    onChange={handleChange}
                                    onBlur={() => {
                                      fieldSubmit = 'benefit_metrics';
                                      handleSubmit();
                                    }}
                                    InputLabelProps={{
                                      shrink: true,
                                    }}
                                    margin="normal"
                                    helperText={
                                      touched.benefit_metrics &&
                                      errors.benefit_metrics
                                    }
                                    error={
                                      touched.benefit_metrics &&
                                      typeof errors.benefit_metrics === 'string'
                                    }
                                  />
                                </Grid>
                                {index > 0 && (
                                  <Grid
                                    item
                                    xs={1}
                                    container
                                    alignItems="center"
                                  >
                                    <Button
                                      variant="outlined"
                                      className={classes.removeBtn}
                                      onClick={() => {
                                        if (isSubmitting) {
                                          return;
                                        }
                                        fieldSubmit = 'remove_benefit_metrics';
                                        handleSubmit();
                                        arrayHelpers.remove(index);
                                      }}
                                      disabled={
                                        // isSubmitting ||
                                        typeof errors.benefit_metrics ===
                                        'string'
                                      }
                                      aria-label="close"
                                    >
                                      {t('remove')}
                                    </Button>
                                  </Grid>
                                )}
                              </Grid>
                            </Box>
                          ))}
                      </Box>
                      <Grid container>
                        <Grid item xs={5}>
                          <Box textAlign="center">
                            <Button
                              // color="primary"
                              variant="outlined"
                              className={classes.addButton}
                              onClick={() => {
                                if (isSubmitting) {
                                  return;
                                }
                                arrayHelpers.push('');
                                fieldSubmit = 'add_benefit_metrics';
                                handleSubmit();
                              }}
                              disabled={
                                // isSubmitting ||
                                values.benefit_metrics![
                                  values.benefit_metrics!.length - 1
                                ] === '' ||
                                typeof errors.benefit_metrics === 'string'
                              }
                            >
                              {t('add')}
                            </Button>
                          </Box>
                        </Grid>
                      </Grid>
                    </>
                  );
                }}
              />
            </Box>
            <StepButtonDetail
              opportunity={opportunity}
              handleSubmit={() => {
                handleNext();
              }}
              isDisableNext={false}
              handleBack={handleBack}
              deleteOpportunity={deleteOpportunity}
            />
          </Box>
        </form>
      )}
    </Formik>
  );
};
