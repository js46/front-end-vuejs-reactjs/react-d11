import React, { useState } from 'react';
import moment from 'moment';
import { Box, Grid, Typography, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { StepButtonDetail } from './StepButton';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { ICaptureSubmitProps } from '../types';
import { OppDetailTable } from '../../../components';
import { WrapperBox } from '../../../components/styled';
import { AttachmentResponse } from '../../../services/opportunity.services';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../store/config/selector';
import { useTranslation } from 'react-i18next';
const useStyles = makeStyles(theme => ({
  opporTitle: {
    fontSize: '14px',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  profileIcon: {
    color: '#E1e1e1',
    fontSize: 40,
  },
  actionButton: {
    color: '#333',
    fontWeight: 'bold',
    marginLeft: 5,
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));
interface ITruncateText {
  text: string;
}

const TruncateText: React.FC<ITruncateText> = ({ text }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [collapse, setCollapse] = useState(false);
  if (text.length < 350) {
    return <Typography component="span">{text}</Typography>;
  }
  return collapse ? (
    <>
      <Typography component="span">{text}</Typography>
      <Typography
        className={classes.actionButton}
        onClick={() => setCollapse(false)}
      >
        {t('see_less')}
      </Typography>
    </>
  ) : (
    <>
      <Typography component="span">{`${text.slice(0, 350)}...`}</Typography>
      <Typography
        className={classes.actionButton}
        onClick={() => setCollapse(true)}
      >
        {t('see_more')}
      </Typography>
    </>
  );
};
export const CaptureSubmit: React.FC<ICaptureSubmitProps> = ({
  opportunity,
  handleNext,
  handleBack,
  handleFinalSubmit,
  deleteOpportunity,
  benefitFocuses,
  isSubmitAll,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const { t } = useTranslation();
  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              match: { entity_id: opportunity?.id },
            },
            {
              match: {
                entity_type: 'opportunity',
              },
            },
          ],
        },
      },
    };
  };

  const DetailRow: React.FC<{
    title: string;
    content: string | number | undefined;
  }> = ({ title, content }) => {
    return (
      <Box mb="30px">
        <Grid container>
          <Grid item xs={2}>
            <Typography component="span" variant="caption">
              {title}
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <Box pl="20px">
              <Typography component="p">{content}</Typography>
            </Box>
          </Grid>
        </Grid>
      </Box>
    );
  };

  return (
    <Box mb={4}>
      <Box mb="30px">
        <Typography component="h2" variant="h4">
          {t('submit')}
        </Typography>
      </Box>
      <Box mb="30px">
        <Typography component="span">{t('please_review_for')}</Typography>
        <Typography
          component="span"
          className={classes.opporTitle}
        >{` ${opportunity?.title ?? ''}`}</Typography>
      </Box>
      <Box>
        <OppDetailTable isDisableLink isSubmitAll opportunity={opportunity} />
      </Box>
      <Box>
        <WrapperBox>
          <Box mb="30px">
            <Typography component="h3" variant="h5">
              {t('details')}
            </Typography>
          </Box>
          <DetailRow
            title="Proposer email"
            content={opportunity?.created_by?.email}
          />
          <DetailRow
            title="Proposer business unit"
            content={opportunity?.created_by?.business_unit}
          />
          <DetailRow
            title="Sponsor business unit"
            content={opportunity?.sponsor_business_unit?.name}
          />
          <DetailRow title="Title" content={opportunity?.title} />
          <DetailRow title="Description" content={opportunity?.description} />
          <DetailRow title="Hypotheses" content={opportunity?.hypothesis} />

          <Box mb="30px">
            <Grid container>
              <Grid item xs={2}>
                <Typography component="span" variant="caption">
                  {t('business_unit_impacted')}
                </Typography>
              </Grid>
              <Grid item xs={10}>
                <Box pl="20px">
                  {opportunity?.business_units?.map((item, index) => (
                    <Typography component="p" key={index}>
                      {item?.name}
                    </Typography>
                  ))}
                </Box>
              </Grid>
            </Grid>
          </Box>
          <Box mb="30px">
            <Grid container>
              <Grid item xs={2}>
                <Typography component="span" variant="caption">
                  {t('business_processes_impacted')}
                </Typography>
              </Grid>
              <Grid item xs={10}>
                <Box pl="20px">
                  {opportunity?.processes?.map((item, index) => (
                    <Typography component="p" key={index}>
                      {item?.name}
                    </Typography>
                  ))}
                </Box>
              </Grid>
            </Grid>
          </Box>
          <DetailRow
            title={t('requested_completion_date')}
            content={
              opportunity?.completion_date
                ? moment(opportunity?.requested_completion_date).format(
                    'DD MMM YYYY',
                  )
                : ''
            }
          />
          <DetailRow
            title="Priority level"
            content={opportunity?.opp_priority_score?.priority_level}
          />
          <DetailRow
            title="Priority score"
            content={opportunity?.opp_priority_score?.priority_score + '/100'}
          />
        </WrapperBox>

        <WrapperBox>
          <Box mb="30px">
            <Typography component="h3" variant="h5">
              {t('business_benefits')}
            </Typography>
          </Box>
          <Box mb="30px">
            {opportunity?.corporate_objectives[0].id !== 0 && (
              <Grid container>
                <Grid item xs={2}>
                  <Typography component="span" variant="caption">
                    {t('alignment_to_corporate_objectives')}
                  </Typography>
                </Grid>
                <Grid item xs={10}>
                  <Box pl="20px">
                    {opportunity?.corporate_objectives.map((item, index) => (
                      <Typography component="p" key={index}>
                        {index + 1}. {item?.name}
                      </Typography>
                    ))}
                  </Box>
                </Grid>
              </Grid>
            )}
          </Box>
          <Box mb="30px">
            {opportunity?.benefit_focuses[0].benefit_focus_id !== 0 && (
              <Grid container>
                <Grid item xs={2}>
                  <Typography component="span" variant="caption">
                    {t('benefit_focus_category')}
                  </Typography>
                </Grid>
                <Grid item xs={10}>
                  <Box pl="20px">
                    {opportunity?.benefit_focuses?.map((item, index) => (
                      <Typography component="p" key={index}>
                        {index + 1}. {item?.benefit_focus_name}
                      </Typography>
                    ))}
                  </Box>
                </Grid>
              </Grid>
            )}
          </Box>
          <Box mb="30px">
            {opportunity?.benefit_metrics && (
              <Grid container>
                <Grid item xs={2}>
                  <Typography component="span" variant="caption">
                    {t('business_benefit_metrics')}
                  </Typography>
                </Grid>
                <Grid item xs={10}>
                  <Box pl="20px">
                    {opportunity?.benefit_metrics?.map((item, index) => (
                      <Typography component="p" key={index}>
                        {index + 1}. {item}
                      </Typography>
                    ))}
                  </Box>
                </Grid>
              </Grid>
            )}
          </Box>
        </WrapperBox>
        <WrapperBox>
          <Box mb="30px">
            <Typography component="h3" variant="h5">
              {t('attachment')}
            </Typography>
          </Box>
          <ReactiveBase
            app={ES_INDICES.attachment_index_name}
            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
            headers={{
              Authorization: window.localStorage.getItem('jwt'),
            }}
          >
            <ReactiveList
              componentId="oppAttachment"
              dataField="attachment"
              renderResultStats={() => null}
              renderNoResults={() => <></>}
              defaultQuery={customQuery}
            >
              {({ data }) => {
                return data?.map((item: AttachmentResponse, index: number) => (
                  <Box key={index} mb="30px">
                    <Grid container>
                      <Grid item xs={2}>
                        <Typography component="span" variant="body2">
                          {item?.description}
                        </Typography>
                      </Grid>
                      <Grid item xs={10}>
                        <Box pl="20px">
                          <Typography component="p">
                            {item?.client_file_name}
                          </Typography>
                        </Box>
                      </Grid>
                    </Grid>
                  </Box>
                ));
              }}
            </ReactiveList>
          </ReactiveBase>
        </WrapperBox>
        <WrapperBox>
          <Box mb="30px">
            <Typography component="h3" variant="h5">
              {t('comments')}
            </Typography>
          </Box>
          <ReactiveBase
            app={ES_INDICES.comment_index_name}
            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
            headers={{
              Authorization: window.localStorage.getItem('jwt'),
            }}
          >
            <ReactiveList
              componentId="oppComment"
              dataField="comment"
              loader="Loading comments..."
              showResultStats={false}
              // renderResultStats={() => null}
              renderNoResults={() => <></>}
              defaultQuery={customQuery}
            >
              {({ data }) => {
                console.log(data);
                return data?.map((item: any, index: number) => (
                  <Grid item xs={6} key={index}>
                    <Box mb="20px">
                      {index > 0 && (
                        <Box mb="20px">
                          <Divider variant="fullWidth" />
                        </Box>
                      )}
                      <Grid container>
                        <Grid item xs={1}>
                          <AccountCircleIcon className={classes.profileIcon} />
                        </Grid>
                        <Grid item xs={11}>
                          <Box
                            display="flex"
                            justifyContent="space-between"
                            mb="20px"
                            ml="10px"
                          >
                            <Box>
                              <Typography variant="caption" component="p">
                                {item.created_by?.name ?? ''}
                              </Typography>
                            </Box>
                            <Box>
                              <Typography color="textSecondary">
                                {item.time
                                  ? moment(item.time).format('DD/MM/YYYY h:mma')
                                  : ''}
                              </Typography>
                            </Box>
                          </Box>
                          <Box ml="10px">
                            <Typography color="textPrimary">
                              <TruncateText text={item.comment_body} />
                            </Typography>
                          </Box>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                ));
              }}
            </ReactiveList>
          </ReactiveBase>
        </WrapperBox>
      </Box>
      <Box>
        <StepButtonDetail
          handleSubmit={() => {
            handleFinalSubmit();
          }}
          isDisableNext={false}
          handleBack={handleBack}
          deleteOpportunity={deleteOpportunity}
          step="submit"
          opportunity={opportunity}
        />
      </Box>
    </Box>
  );
};
