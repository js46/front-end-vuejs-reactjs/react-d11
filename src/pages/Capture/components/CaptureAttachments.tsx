import React, { useRef, useState } from 'react';
import {
  Box,
  Grid,
  Typography,
  Button,
  Input,
  TextField,
} from '@material-ui/core';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import DeleteOutlineRoundedIcon from '@material-ui/icons/DeleteOutlineRounded';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { StepButtonDetail } from './StepButton';
import { ICaptureAttachmentsProps } from '../types';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import {
  deleteAttachments,
  AttachmentResponse,
} from '../../../services/opportunity.services';
import { ConfigESIndex } from '../../../store/config/selector';

const useStyles = makeStyles(theme => ({
  root: {
    paddingBottom: theme.spacing(4),
  },
  fileName: {
    maxWidth: '240px',
    display: 'inline-block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    marginBottom: -6,
  },
  attachmentDescription: {
    '& input': {
      height: '100%',
    },
  },
}));

export const CaptureAttachments: React.FC<ICaptureAttachmentsProps> = ({
  opportunity,
  opportunityQueryId,
  attachments,
  handleChangeFile,
  // handleRemoveFile,
  handleRemove,
  handleAddAttachment,
  handleChangeDescription,
  handleUpdateAttachmentDescription,
  handleNext,
  handleBack,
  deleteOpportunity,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const attachFileRef = useRef<HTMLInputElement[]>([]);
  const [attachmentFromES, setAttachmentFromES] = useState<
    AttachmentResponse[]
  >();
  const { t } = useTranslation();
  const handleClickButtonFile = (index: number) => () => {
    if (attachFileRef && attachFileRef.current) {
      attachFileRef.current[index].click();
    }
  };
  const onHandleChangeFile = (index: number) => (e: any) => {
    handleChangeFile(index, e.target.files[0]);
  };
  const onHandleRemove = (index: number) => () => {
    handleRemove(index);
  };
  const onHandleAddAttachment = () => {
    handleAddAttachment();
  };
  const onHandleChangeDescription = (index: number) => (e: any) => {
    handleChangeDescription(index, e.target.value);
  };

  const handleDeleteAttachmentFromES = async (fileId: number) => {
    const response = await deleteAttachments(fileId);
    if (response.success && attachmentFromES) {
      setAttachmentFromES(
        attachmentFromES?.filter(item => item?.id !== fileId),
      );
    }
  };
  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                entity_id: opportunity?.id,
              },
            },
            {
              term: {
                entity_type: 'opportunity',
              },
            },
          ],
          filter: {
            term: {
              del_flg: false,
            },
          },
        },
      },
    };
  };

  const getAttachmentContent = (data?: AttachmentResponse[]) => {
    return (
      <Box mb={3}>
        <Grid container>
          {attachments &&
            attachments.length > 0 &&
            attachments.map((item, index) => (
              <Box mb={3} key={index} width="100%">
                <Grid container>
                  <Grid item xs={4}>
                    {item.attachments && (
                      <Box display="flex">
                        <Typography
                          component="span"
                          style={{ marginRight: '20px' }}
                        >
                          {data && data.length + index + 1}.
                        </Typography>
                        {item && item.attachments && item.attachments.name && (
                          <Typography
                            component="span"
                            className={classes.fileName}
                          >
                            {item.attachments.name}
                          </Typography>
                        )}
                      </Box>
                    )}

                    <Input
                      type="file"
                      inputRef={el => (attachFileRef.current[index] = el)}
                      onChange={onHandleChangeFile(index)}
                      style={{ display: 'none' }}
                    />
                    {!item.attachments && (
                      <Box display="flex">
                        <Typography
                          component="span"
                          style={{ marginRight: '20px' }}
                        >
                          {data && data.length + index + 1}.
                        </Typography>
                        <Button
                          variant="contained"
                          startIcon={<AttachFileIcon />}
                          onClick={handleClickButtonFile(index)}
                        >
                          {t('attack_file')}
                        </Button>
                      </Box>
                    )}
                  </Grid>
                  <Grid item xs={8}>
                    {item && item.attachments && (
                      <Grid container>
                        <Grid item xs={8}>
                          <TextField
                            fullWidth
                            variant="outlined"
                            name={`attachment_${item.id}`}
                            value={item.description}
                            onChange={onHandleChangeDescription(index)}
                            className={classes.attachmentDescription}
                            onBlur={e =>
                              handleUpdateAttachmentDescription(
                                e.target.value,
                                index,
                              )
                            }
                          />
                        </Grid>
                        <Box ml={5}>
                          <Button
                            variant="contained"
                            startIcon={<DeleteOutlineRoundedIcon />}
                            onClick={onHandleRemove(index)}
                          >
                            {t('remove')}
                          </Button>
                        </Box>
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              </Box>
            ))}
        </Grid>
      </Box>
    );
  };

  return (
    <Box>
      <Box mb={3}>
        <Typography component="h2" variant="h4">
          {t('add_attachment')}
        </Typography>
      </Box>
      <Box mb={3}>
        <Typography component="p">{t('click_attack_file')}</Typography>
      </Box>
      <Box mb={3}>
        <Grid container>
          <Grid item xs={4}>
            <Box ml="35px">
              <Typography component="p" variant="caption">
                {t('file')}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={8}>
            <Typography component="p" variant="caption">
              {t('description')}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      {opportunityQueryId && (
        <Box mb={3}>
          <ReactiveBase
            app={ES_INDICES.attachment_index_name}
            url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
            headers={{
              Authorization: window.localStorage.getItem('jwt'),
            }}
          >
            <ReactiveList
              componentId="SearchAttachment"
              dataField="attachment"
              renderResultStats={() => null}
              renderNoResults={() => <></>}
              defaultQuery={customQuery}
              onData={({ data }) => {
                const processedAttachment = data?.filter(
                  (item: any) =>
                    !attachments.map(atm => atm.id).includes(item?.id),
                );
                setAttachmentFromES(processedAttachment);
              }}
            >
              {({ data }) => {
                return (
                  <React.Fragment>
                    {attachmentFromES?.map(
                      (item: any, indexAttachment: number) => (
                        <Box mb={4} key={indexAttachment}>
                          <Grid container>
                            <Grid item xs={4}>
                              <Box display="flex">
                                <Typography component="span">
                                  {indexAttachment + 1}.
                                </Typography>
                                <Box pl={3} display="flex">
                                  <Typography
                                    component="span"
                                    className={classes.fileName}
                                  >
                                    {item?.client_file_name}
                                  </Typography>
                                  {!item?.description && (
                                    <Box pl={2}>
                                      <Button
                                        variant="contained"
                                        startIcon={<DeleteOutlineRoundedIcon />}
                                        onClick={() =>
                                          handleDeleteAttachmentFromES(item.id)
                                        }
                                      >
                                        {t('remove')}
                                      </Button>
                                    </Box>
                                  )}
                                </Box>
                              </Box>
                            </Grid>
                            <Grid item xs={8}>
                              {item?.description && (
                                <Grid container>
                                  <Grid item xs={8}>
                                    <Typography variant="body2">
                                      {item?.description}
                                    </Typography>
                                  </Grid>
                                  <Box ml={5}>
                                    <Button
                                      variant="contained"
                                      startIcon={<DeleteOutlineRoundedIcon />}
                                      onClick={() =>
                                        handleDeleteAttachmentFromES(item.id)
                                      }
                                    >
                                      {t('remove')}
                                    </Button>
                                  </Box>
                                </Grid>
                              )}
                            </Grid>
                          </Grid>
                        </Box>
                      ),
                    )}
                    {getAttachmentContent(attachmentFromES)}
                  </React.Fragment>
                );
              }}
            </ReactiveList>
          </ReactiveBase>
        </Box>
      )}
      {!opportunityQueryId && getAttachmentContent([])}

      <Box display="flex" justifyContent="center">
        <Button
          variant="contained"
          onClick={onHandleAddAttachment}
          disabled={attachments.some(attachment => !attachment.attachments)}
        >
          Add
        </Button>
      </Box>
      <StepButtonDetail
        opportunity={opportunity}
        handleSubmit={() => {
          handleNext();
        }}
        isDisableNext={false}
        handleBack={handleBack}
        deleteOpportunity={deleteOpportunity}
        // isHideBack
      />
    </Box>
  );
};
