import React from 'react';
import { Formik } from 'formik';
import {
  TextField,
  FormControl,
  Select,
  MenuItem,
  FormHelperText,
  Chip,
  Checkbox,
  ListItemText,
  Box,
  Typography,
  InputAdornment,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { useTranslation } from 'react-i18next';

import {
  IOpportunityParam,
  removeOpportunityBusinessUnit,
  removeOpportunityBPI,
} from '../../../services/opportunity.services';
import { ICaptureDetailProps } from '../types';
import { MixTitle } from '../../../components';
import { checkRestrictedWord } from '../../../utils';
import { UserProfileState } from '../../../store/user/selector';
import { StepButtonDetail } from './StepButton';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(6),
  },
  selectItem: {
    color: '#666',
    backgroundColor: '#e0e0e0',
  },
}));

export const CaptureDetail: React.FC<ICaptureDetailProps> = ({
  opportunity,
  businessUnits,
  businessProcessess,
  handleNext,
  handleBack,
  handleOpportunity,
  setOpportunity,
  deleteOpportunity,
}) => {
  const classes = useStyles();
  const { i18n } = useTranslation();
  const userProfileState = useSelector(UserProfileState);
  let fieldSubmit = '';
  const initialValues: IOpportunityParam = {
    proposer_email: opportunity?.created_by?.email || userProfileState.email,
    proposer_business_unit:
      opportunity?.created_by?.business_unit ||
      userProfileState.business_unit?.business_unit_name,
    sponsor_business_unit_id:
      opportunity?.sponsor_business_unit.id ||
      userProfileState.business_unit?.business_unit_id,
    business_units:
      opportunity && opportunity.business_units
        ? opportunity.business_units.map(item => item.id)
        : [],
    title: opportunity?.title || '',
    description: opportunity?.description || '',
    hypothesis: opportunity?.hypothesis || '',
    processes:
      opportunity && opportunity.processes
        ? opportunity.processes.map(item => item.id)
        : [],
    requested_completion_date: opportunity
      ? new Date(opportunity.requested_completion_date!).toISOString()
      : new Date().toISOString(),
    corporate_objectives: [0],
    benefit_focuses: null,
    benefit_metrics: [],
    private_flag: opportunity?.private_flag,
  };
  const validationsSchema = Yup.object().shape({
    // proposer_title: Yup.string().required('Required'),
    // proposer_name: Yup.string().required('Required'),
    proposer_email: Yup.string().required('Required'),
    proposer_business_unit: Yup.string().required('Required'),
    business_units: Yup.array()
      .of(Yup.number())
      .max(3),
    // processes: Yup.string().required('Required'),
    title: Yup.string()
      .trim()
      .required('Required'),
    description: Yup.string()
      .trim()
      .required('Required')
      .test('len', 'Description should be less than 300 words', function(val) {
        return val === undefined || checkRestrictedWord(val) <= 300;
      }),
    hypothesis: Yup.string()
      .trim()
      .test('len', 'Hypothesis should be less than 300 words', function(val) {
        return val === undefined || checkRestrictedWord(val) <= 300;
      }),
  });
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationsSchema}
      onSubmit={async values => {
        await handleOpportunity(values, fieldSubmit);
      }}
    >
      {({
        isSubmitting,
        values,
        setFieldValue,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isValid,
      }) => (
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Box>
                <Box mb={2.5}>
                  <Typography variant="h4">{i18n.t('details')}</Typography>
                </Box>

                <Typography>
                  {i18n.t('please_enter_new_details')}
                  <Typography component="span" color="error">
                    *{' '}
                  </Typography>
                  {i18n.t('are_mandatory')}
                </Typography>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Grid container className={classes.root}>
                <Grid item xs={9}>
                  <Grid container>
                    <Grid item xs={4}>
                      <Box pr="20px">
                        <MixTitle
                          isRequired={true}
                          isHelper={true}
                          title={i18n.t('proposer_email')}
                          helperText={i18n.t(
                            'capture.help_text.proposer_email',
                          )}
                        />
                        <TextField
                          id="proposer_email"
                          name="proposer_email"
                          value={values.proposer_email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type="text"
                          InputLabelProps={{ shrink: true }}
                          fullWidth
                          variant="outlined"
                          helperText={
                            touched.proposer_email && errors.proposer_email
                          }
                          error={
                            touched.proposer_email && !!errors.proposer_email
                          }
                          disabled={true}
                        />
                      </Box>
                    </Grid>
                    <Grid item xs={4}>
                      <Box pr="20px">
                        <MixTitle
                          isRequired={true}
                          isHelper={true}
                          title={i18n.t('proposer_business_unit')}
                          helperText={i18n.t('capture.help_text.proposer_bu')}
                        />
                        <TextField
                          id="proposer_business_unit"
                          name="proposer_business_unit"
                          value={values.proposer_business_unit}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type="text"
                          InputLabelProps={{ shrink: true }}
                          fullWidth
                          variant="outlined"
                          helperText={
                            touched.proposer_business_unit &&
                            errors.proposer_business_unit
                          }
                          error={
                            touched.proposer_business_unit &&
                            !!errors.proposer_business_unit
                          }
                          disabled={true}
                        />
                      </Box>
                    </Grid>
                    <Grid item xs={4}>
                      <Box>
                        <MixTitle
                          title={i18n.t('sponsor_business_unit')}
                          isRequired={true}
                          isHelper={true}
                          helperText={i18n.t('capture.help_text.sponsor_bu')}
                        />
                        <FormControl
                          required
                          fullWidth
                          error={touched.processes && !!errors.processes}
                          margin="normal"
                          variant="outlined"
                        >
                          <Select
                            value={values.sponsor_business_unit_id}
                            onChange={e => {
                              handleChange(e);
                              fieldSubmit = 'sponsor_business_unit_id';
                              handleSubmit();
                            }}
                            onBlur={handleBlur}
                            id="sponsor_business_unit"
                            name="sponsor_business_unit_id"
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              getContentAnchorEl: null,
                            }}
                            IconComponent={ExpandMoreIcon}
                          >
                            {businessUnits
                              .filter(item => !item.archive)
                              .map(item => {
                                return (
                                  <MenuItem key={item.id} value={item.id}>
                                    {item.name}
                                  </MenuItem>
                                );
                              })}
                          </Select>
                          <FormHelperText>
                            {touched.processes ? errors.processes : ''}
                          </FormHelperText>
                        </FormControl>
                      </Box>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={9}>
                  <Grid container className={classes.root}>
                    <Grid item xs={12}>
                      <MixTitle
                        isRequired={true}
                        isHelper={true}
                        title="Title"
                        helperText={i18n.t('capture.help_text.title')}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        id="title"
                        name="title"
                        value={values.title}
                        onChange={handleChange}
                        onBlur={e => {
                          fieldSubmit = e.target.name;
                          handleSubmit();
                        }}
                        type="text"
                        InputLabelProps={{ shrink: true }}
                        fullWidth
                        variant="outlined"
                        helperText={touched.title && errors.title}
                        error={touched.title && !!errors.title}
                        disabled={isSubmitting}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container className={classes.root}>
                <Grid item xs={9}>
                  <FormControl fullWidth>
                    <MixTitle
                      isRequired={true}
                      isHelper={true}
                      title={i18n.t('description')}
                      moreText={i18n.t('max_300_words')}
                      helperText={i18n.t('capture.help_text.description')}
                    />
                    <TextField
                      value={values.description}
                      // onChange={handleChange}
                      // onBlur={e => {
                      //   fieldSubmit = e.target.name;
                      //   handleSubmit();
                      // }}
                      onChange={handleChange}
                      onBlur={e => {
                        fieldSubmit = e.target.name;
                        handleSubmit();
                      }}
                      rows={12}
                      // labelWidth={100}
                      variant="outlined"
                      multiline
                      id="description"
                      name="description"
                      helperText={touched.description && errors.description}
                      error={touched.description && !!errors.description}
                    />
                  </FormControl>
                </Grid>
              </Grid>
              <Grid container className={classes.root}>
                <Grid item xs={9}>
                  <FormControl fullWidth>
                    <MixTitle
                      isHelper={true}
                      title={i18n.t('hypotheses')}
                      moreText={i18n.t('max_300_words')}
                      helperText={i18n.t('capture.help_text.hypotheses')}
                    />
                    <TextField
                      value={values.hypothesis}
                      onChange={handleChange}
                      onBlur={e => {
                        fieldSubmit = e.target.name;
                        handleSubmit();
                      }}
                      rows={12}
                      // labelWidth={100}
                      variant="outlined"
                      multiline
                      id="hypothesis"
                      name="hypothesis"
                      helperText={touched.hypothesis && errors.hypothesis}
                      error={touched.hypothesis && !!errors.hypothesis}
                    />
                  </FormControl>
                </Grid>
              </Grid>
              <Grid container className={classes.root}>
                <Grid item xs={9}>
                  <FormControl
                    required
                    fullWidth
                    error={touched.business_units && !!errors.business_units}
                    margin="normal"
                    variant="outlined"
                  >
                    <MixTitle
                      isHelper={true}
                      title={i18n.t('business_unit_impacted')}
                      moreText={i18n.t('max_3_items')}
                      helperText={i18n.t(
                        'capture.help_text.business_unit_impacted',
                      )}
                    />
                    <Select
                      multiple
                      value={values.business_units}
                      onChange={(e: any) => {
                        if (
                          values.business_units &&
                          e.target.value.length < values.business_units?.length
                        ) {
                          if (opportunity?.id) {
                            const removedItem = values.business_units?.filter(
                              item => !e.target.value?.includes(item),
                            );
                            const removeBusinessUnit = async () => {
                              const response = await removeOpportunityBusinessUnit(
                                removedItem,
                                opportunity?.id,
                              );
                              setOpportunity(response.data);
                            };
                            removeBusinessUnit();
                          }
                          handleChange(e);
                        } else {
                          if (
                            !values.business_units ||
                            values.business_units.length < 3
                          ) {
                            fieldSubmit = 'businessUnits';
                            handleChange(e);
                            handleSubmit();
                          }
                        }
                      }}
                      onBlur={handleBlur}
                      id="business_units"
                      name="business_units"
                      renderValue={selected => {
                        return businessUnits
                          .filter(pb =>
                            (selected as number[]).includes(pb.id as number),
                          )
                          .map(pb => (
                            <Chip
                              label={pb.name}
                              key={pb.id}
                              className={classes.selectItem}
                              size="small"
                            />
                          ));
                      }}
                      MenuProps={{
                        anchorOrigin: {
                          vertical: 'bottom',
                          horizontal: 'left',
                        },
                        transformOrigin: {
                          vertical: 'top',
                          horizontal: 'left',
                        },
                        getContentAnchorEl: null,
                      }}
                      IconComponent={ExpandMoreIcon}
                    >
                      {businessUnits
                        .filter(item => !item.archive)
                        .map(item => {
                          return (
                            <MenuItem key={item.id} value={item.id}>
                              <Checkbox
                                // color="primary"
                                style={{ color: '#2c54e3' }}
                                checked={
                                  values.business_units &&
                                  values.business_units.includes(item.id)
                                }
                              />
                              <ListItemText primary={item.name} />
                            </MenuItem>
                          );
                        })}
                    </Select>
                    <FormHelperText>
                      {touched.processes ? errors.processes : ''}
                    </FormHelperText>
                  </FormControl>
                </Grid>
              </Grid>
              <Grid container className={classes.root}>
                <Grid item xs={9}>
                  <FormControl
                    required
                    fullWidth
                    error={touched.processes && !!errors.processes}
                    margin="normal"
                    variant="outlined"
                  >
                    <MixTitle
                      title={i18n.t('business_processes_impacted')}
                      isHelper={true}
                      helperText={i18n.t(
                        'capture.help_text.business_processes_impacted',
                      )}
                    />
                    <Select
                      multiple
                      value={values.processes}
                      onChange={(e: any) => {
                        if (
                          values.processes &&
                          e.target.value.length < values.processes?.length
                        ) {
                          if (opportunity?.id) {
                            const removedItem = values.processes?.filter(
                              item => !e.target.value?.includes(item),
                            );
                            const removeProcess = async () => {
                              const response = await removeOpportunityBPI(
                                removedItem,
                                opportunity?.id,
                              );
                              setOpportunity(response.data);
                            };
                            removeProcess();
                          }
                          handleChange(e);
                        } else {
                          fieldSubmit = 'processes';
                          handleChange(e);
                          handleSubmit();
                        }
                      }}
                      onBlur={handleBlur}
                      id="processes"
                      name="processes"
                      renderValue={selected => {
                        return businessProcessess
                          .filter(pb =>
                            (selected as number[]).includes(pb.id as number),
                          )
                          .map(pb => (
                            <Chip
                              label={pb.name}
                              key={pb.id}
                              className={classes.selectItem}
                              size="small"
                            />
                          ));
                      }}
                      MenuProps={{
                        anchorOrigin: {
                          vertical: 'bottom',
                          horizontal: 'left',
                        },
                        transformOrigin: {
                          vertical: 'top',
                          horizontal: 'left',
                        },
                        getContentAnchorEl: null,
                      }}
                      IconComponent={ExpandMoreIcon}
                    >
                      {businessProcessess
                        .filter(item => !item.archive)
                        .map(item => {
                          return (
                            <MenuItem key={item.id} value={item.id}>
                              <Checkbox
                                // color="primary"
                                style={{ color: '#2c54e3' }}
                                checked={
                                  values.processes &&
                                  values.processes.includes(item.id)
                                }
                              />
                              <ListItemText primary={item.name} />
                            </MenuItem>
                          );
                        })}
                    </Select>
                    <FormHelperText>
                      {touched.processes ? errors.processes : ''}
                    </FormHelperText>
                  </FormControl>
                </Grid>
              </Grid>
              <Grid container className={classes.root}>
                <Grid item xs={4}>
                  <Box pr="30px">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <MixTitle
                        title={i18n.t('requested_completion_date')}
                        isHelper={true}
                        helperText={i18n.t(
                          'capture.help_text.requested_completion_date',
                        )}
                      />
                      <DatePicker
                        disableToolbar
                        allowKeyboardControl
                        minDate={new Date()}
                        inputVariant="outlined"
                        variant="inline"
                        margin="normal"
                        name="requested_completion_date"
                        id="requested_completion_date"
                        format="d MMMM yyyy"
                        autoOk={true}
                        onChange={value => {
                          setFieldValue('requested_completion_date', value);
                          fieldSubmit = 'requested_completion_date';
                          handleSubmit();
                        }}
                        value={values.requested_completion_date}
                        animateYearScrolling={false}
                        fullWidth
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <ExpandMoreIcon />
                            </InputAdornment>
                          ),
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>
                </Grid>
              </Grid>
              <Grid container className={classes.root}>
                <Grid item xs={12}>
                  <Box display="flex" alignItems="center">
                    <MixTitle
                      title="Private"
                      isHelper={true}
                      noMargin
                      helperText={i18n.t('capture.help_text.private')}
                    />
                    <Checkbox
                      value={values.private_flag}
                      checked={values.private_flag}
                      onChange={(e: any) => {
                        fieldSubmit = 'private_flag';
                        handleChange(e);
                        handleSubmit();
                      }}
                      onBlur={handleBlur}
                      color="primary"
                      name="private_flag"
                      id="private_flag"
                    />
                  </Box>
                </Grid>
              </Grid>
            </Grid>
            <StepButtonDetail
              opportunity={opportunity}
              handleSubmit={() => {
                handleNext();
              }}
              isDisableNext={isSubmitting || !isValid || !opportunity}
              handleBack={handleBack}
              deleteOpportunity={deleteOpportunity}
              isHideBack
            />
          </Grid>
        </form>
      )}
    </Formik>
  );
};
