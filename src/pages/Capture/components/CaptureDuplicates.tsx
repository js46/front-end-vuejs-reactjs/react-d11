import React, { useState } from 'react';
import { Formik } from 'formik';

import {
  Grid,
  Box,
  Typography,
  FormControl,
  Table,
  TableCell,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { StepButtonDetail } from './StepButton';
import { ICommentParam } from '../../../services/comment.services';
import { ICaptureDuplicatesProps } from '../types';
import { getComparator, stableSort } from '../../../commons/util';
import { ReactiveBase, SelectedFilters } from '@appbaseio/reactivesearch';
import { StyledReactiveList } from '../../../components/styled';
import moment from 'moment';
import { MixTitle } from '../../../components';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../store/config/selector';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(5),
  },
  notFoundIcon: {
    fontSize: '200px',
    margin: '10px 0',
  },
  opporTitle: {
    fontSize: '14px',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '0px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
        fontSize: '14px!important',
        fontFamily: 'Helvetica Neue,Arial',
      },
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
  },
}));

interface Data {
  id: string;
  title: string;
  business_unit: string;
  current_experts: string;
  phase: string;
  last_update_at: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
}
const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'Opp',
    disablePadding: true,
  },
  {
    id: 'title',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'business_unit',
    label: 'Business unit',
    disablePadding: true,
  },
  {
    id: 'current_experts',
    label: 'Experts',
    disablePadding: true,
  },
  {
    id: 'phase',
    label: 'Phase',
    disablePadding: true,
  },
  {
    id: 'last_update_at',
    label: 'Last updated',
    disablePadding: true,
  },
];

type Order = 'asc' | 'desc';

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    onRequestSort(event, property);
  };
  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

export const CaptureDuplicates: React.FC<ICaptureDuplicatesProps> = ({
  opportunity,
  comment,
  handleComment,
  handleNext,
  handleBack,
  handleOpportunity,
  deleteOpportunity,
}) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const { t } = useTranslation();
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  // const [isComment] = useState(false);
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  let fieldSubmit = '';
  const initialValues: ICommentParam = {
    comment_body: comment?.comment_body,
  };
  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                del_flag: false,
              },
            },
            { match: { title: opportunity?.title } },
          ],
        },
      },
    };
  };

  const NoResponse: React.FC = () => {
    return (
      <Box>
        <Typography component="h1">{t('no_similar_or_duplicate')}</Typography>
      </Box>
    );
  };
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async values => {
        await handleComment(values, fieldSubmit);
      }}
    >
      {({
        isSubmitting,
        values,
        setFieldValue,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isValid,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box>
            <Box mb="20px">
              <Typography component="h2" variant="h4">
                {t('similar_or_duplicate')}
              </Typography>
            </Box>
            <Box>
              <ReactiveBase
                app={ES_INDICES.opportunity_index_name}
                url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                headers={{ Authorization: window.localStorage.getItem('jwt') }}
              >
                <Grid container>
                  <Grid item lg={12} xl={12} xs={12} sm={12}>
                    <SelectedFilters />
                    <StyledReactiveList
                      componentId="SearchResult"
                      dataField="title"
                      loader="Loading comments..."
                      showResultStats={false}
                      renderNoResults={() => <></>}
                      pagination={true}
                      paginationAt="bottom"
                      pages={5}
                      size={5}
                      render={({ data, loading }) => {
                        if (data && data.length === 1) {
                          return <NoResponse />;
                        }
                        return (
                          opportunity &&
                          opportunity.id &&
                          data &&
                          data.length > 1 && (
                            <Grid container>
                              <Grid item xs={12}>
                                <Box mb={5}>
                                  <Typography>
                                    <Typography component="span">
                                      {t('no_similar_or_duplicate_for')}
                                    </Typography>
                                    <Typography
                                      component="span"
                                      className={classes.opporTitle}
                                    >{` ${opportunity?.title ??
                                      ''}`}</Typography>
                                  </Typography>
                                  <Typography component="h1">
                                    {t('click_opp_view_details')}
                                  </Typography>
                                </Box>
                              </Grid>
                              <Grid item lg={12} xl={12} xs={12} sm={12}>
                                <TableContainer>
                                  <Table>
                                    <EnhancedTableHead
                                      classes={classes}
                                      order={order}
                                      orderBy={orderBy}
                                      onRequestSort={handleRequestSort}
                                    />
                                    <TableBody>
                                      {stableSort(
                                        data.filter(
                                          (item: any) =>
                                            item.id !== opportunity.id,
                                        ),
                                        getComparator(order, orderBy),
                                      ).map((item: any, index: number) => (
                                        <TableRow key={index}>
                                          <TableCell component="th" scope="row">
                                            {item.id}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {item.title}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {item.business_units
                                              ? item.business_units.map(
                                                  (bu: any, eindex: number) => (
                                                    <div key={eindex}>
                                                      {bu.name}
                                                    </div>
                                                  ),
                                                )
                                              : ''}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {item.current_experts
                                              ? item.current_experts.map(
                                                  (
                                                    expert: any,
                                                    eindex: number,
                                                  ) => (
                                                    <div key={eindex}>
                                                      {expert.name}
                                                    </div>
                                                  ),
                                                )
                                              : ''}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {item.opportunity_phase.name}
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {moment(
                                              item.last_updated_at,
                                            ).format('DD/MM/YYYY HH:mm:ss')}
                                          </TableCell>
                                        </TableRow>
                                      ))}
                                    </TableBody>
                                  </Table>
                                </TableContainer>
                              </Grid>
                            </Grid>
                          )
                        );
                      }}
                      innerClass={{
                        button: classes.buttonPagination,
                        resultsInfo: classes.resultsInfo,
                        sortOptions: classes.sortSelect,
                        select: classes.sortSelect,
                      }}
                      defaultQuery={customQuery}
                    />
                    {/* {({ data }) => {
                        
                      }}
                    </StyledReactiveList> */}
                  </Grid>
                </Grid>
              </ReactiveBase>
              <Grid item xs={12}>
                <Grid container className={classes.root}>
                  <Grid item xs={8}>
                    <FormControl fullWidth>
                      <MixTitle
                        title={t('comment')}
                        isHelper={true}
                        helperText={t('capture.help_text.comment')}
                      />
                      <TextField
                        value={values.comment_body}
                        onChange={handleChange}
                        onBlur={e => {
                          fieldSubmit = e.target.name;
                          handleSubmit();
                        }}
                        rows={12}
                        // labelWidth={100}
                        variant="outlined"
                        multiline
                        id="comment_body"
                        name="comment_body"
                      />
                    </FormControl>
                  </Grid>
                </Grid>
              </Grid>
            </Box>

            <StepButtonDetail
              opportunity={opportunity}
              handleSubmit={() => {
                handleNext();
              }}
              isDisableNext={false}
              handleBack={handleBack}
              deleteOpportunity={deleteOpportunity}
            />
          </Box>
        </form>
      )}
    </Formik>
  );
};
