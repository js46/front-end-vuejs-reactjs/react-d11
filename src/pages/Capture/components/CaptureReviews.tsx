import React, { useState } from 'react';
import {
  Box,
  Grid,
  Typography,
  TextField,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  Table,
  TableBody,
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
  IconButton,
  Tooltip,
  Divider,
} from '@material-ui/core';
import { useFormik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import * as Yup from 'yup';
import moment from 'moment';
import { ReactiveBase, ReactiveList } from '@appbaseio/reactivesearch';
import { useTranslation } from 'react-i18next';

import { StepButtonDetail } from './StepButton';
import { ICaptureReviewsProps } from '../types';
import {
  addNotification,
  resendNotification,
  getOpportunityNotification,
} from '../../../services/notification.services';
import { Message } from '../../../constants';
import { useDisableMultipleClick } from '../../../hooks';
import {
  genStringForMailingServiceStatus,
  genNotificationType,
} from '../../../utils';
import { ResendIcon } from '../../../assets/icon';

import { TruncateText } from '../../../components';
import { useDispatch, useSelector } from 'react-redux';
import { ConfigESIndex } from '../../../store/config/selector';
import { IComment } from '../../../services/comment.services';
import { notifyError, notifySuccess } from '../../../store/common/actions';
const useStyles = makeStyles(theme => ({
  notFoundIcon: {
    fontSize: '200px',
    margin: '10px 0',
  },
  opporTitle: {
    fontSize: '14px',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  profileIcon: {
    color: '#E1e1e1',
    fontSize: 44,
  },
  properMargin: {
    '& .MuiFormControl-marginNormal': {
      margin: 0,
    },
  },
}));

interface Data {
  email: string;
  type: string;
  status: string;
  dispatch_time: string;
  action: string;
}

interface HeadCell {
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: 'email',
    label: 'Email',
  },
  {
    id: 'type',
    label: 'Type',
  },
  {
    id: 'status',
    label: 'Status',
  },
  {
    id: 'dispatch_time',
    label: 'Dispatch time',
  },
  {
    id: 'action',
    label: 'Actions',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  // const { classes } = props;
  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            style={headCell.id === 'action' ? { width: '80px' } : undefined}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

enum EmailReceiver {
  SponsorReview = 'sponsor_review',
  PeerReview = 'peer_review',
}

export const CaptureReviews: React.FC<ICaptureReviewsProps> = ({
  opportunity,
  handleNext,
  handleBack,
  // handleOpportunity,
  deleteOpportunity,
  emailRequestReview,
  onUpdateEmailRequestReview,
  notification,
  setNotification,
}) => {
  const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const [isAddReviewerDialogOpen, setReviewerDialogOpen] = useState(false);
  const [receiverType, setReceiverType] = useState(EmailReceiver.SponsorReview);
  const dispatch = useDispatch();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const { t, i18n } = useTranslation();

  const email = useFormik({
    initialValues: {
      email: '',
    },
    onSubmit: values => {
      if (notification?.find(item => item.email === values.email)) {
        dispatch(
          notifyError({
            message: i18n.t('notification.sent_before', {
              email: values.email,
            }),
          }),
        );
        return;
      }
      handleInteractEmail(values.email);
      setReviewerDialogOpen(false);
      email.resetForm();
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .required()
        .email(i18n.t('validate.field_email_invalid')),
    }),
  });

  const handleChangeReceiver = (event: React.ChangeEvent<HTMLInputElement>) => {
    setReceiverType(event.target.value as EmailReceiver);
  };

  const handleInteractEmail = async (email: string) => {
    if (!opportunity?.id || isSubmitting) return;
    await debounceFn();
    const body = {
      notification_user_case_id:
        receiverType === EmailReceiver.SponsorReview ? 1 : 2,
      opportunity_id: opportunity?.id,
      to_email: email,
    };
    const response = await addNotification(body);
    if (response.success) {
      refetchNotification();
      dispatch(
        notifySuccess({
          message: i18n.t('notification.send_email', {
            email: email,
          }),
        }),
      );
      setTimeout(() => {
        refetchNotification();
      }, 3000);
    } else {
      dispatch(
        notifyError({
          message: response.message ?? '',
        }),
      );
    }
    endRequest();
  };

  const handleResendNotification = async (idParam: number, email: string) => {
    const response = await resendNotification(idParam);
    if (response.success) {
      dispatch(
        notifySuccess({
          message: i18n.t('notification.resend_email', {
            email: email,
          }),
        }),
      );
      refetchNotification();
    } else {
      dispatch(
        notifyError({
          message: response.message ?? '',
        }),
      );
    }
  };

  const refetchNotification = () => {
    if (!opportunity?.id) return;
    getOpportunityNotification(opportunity?.id)
      .then(data => {
        const listNotification = data.data.list
          ?.filter(item => item.opportunity.id === opportunity?.id)
          .map(noti => ({
            id: noti.id,
            email: noti.to_email,
            type: genNotificationType(noti?.notification_user_case?.name),
            status: genStringForMailingServiceStatus(noti.status),
            time: new Date(noti.sent_date),
          }));
        setNotification(listNotification);
      })
      .catch(err =>
        dispatch(
          notifyError({
            message: Message.Error,
          }),
        ),
      );
  };

  const customQuery = () => {
    return {
      query: {
        bool: {
          must: [
            {
              match: { entity_id: opportunity?.id },
            },
            {
              match: {
                entity_type: 'opportunity',
              },
            },
            {
              match: {
                comment_type: 'peer_review',
              },
            },
          ],
        },
      },
    };
  };

  const NoResponse = () => {
    return (
      <Box>
        <Typography component="h1">{t('no_review_or_response')}</Typography>
        <SentimentVeryDissatisfiedIcon className={classes.notFoundIcon} />
        <Typography component="h1">{t('wait_to_hear')}</Typography>
      </Box>
    );
  };

  return (
    <Box mb={5}>
      <Box>
        <Box mb="30px">
          <Typography component="h2" variant="h4">
            {t('request_review')}
          </Typography>
        </Box>
        <Box mb="20px">
          <Box mb="40px">
            <Typography component="p">{t('send_opp_to_someone')}</Typography>
            <Box mb="10px">
              <Typography component="p">{t('enter_their_email')}</Typography>
            </Box>
          </Box>
        </Box>
        <Grid container>
          <Grid item xs md={6}>
            <Box display="flex" justifyContent="flex-end">
              <Button
                variant="contained"
                color="primary"
                startIcon={<ControlPointIcon />}
                onClick={() => setReviewerDialogOpen(true)}
              >
                {t('add_reviewer')}
              </Button>
            </Box>
          </Grid>
        </Grid>

        <Box mb={3} mt={3}>
          <Grid container>
            <Grid item xs md={6}>
              <TableContainer>
                <Table>
                  <EnhancedTableHead classes={classes} />
                  <TableBody>
                    {(!notification || notification?.length === 0) && (
                      <TableRow>
                        <TableCell colSpan={5}>
                          <Box
                            display="flex"
                            justifyContent="center"
                            alignItems="center"
                          >
                            {t('no_data')}
                          </Box>
                        </TableCell>
                      </TableRow>
                    )}
                    {notification?.map((item, index) => (
                      <TableRow key={item.email}>
                        <TableCell>
                          <Typography component="p" variant="body2">
                            {item.email}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography component="p" variant="body2">
                            {item.type}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography
                            component="p"
                            variant="body2"
                            style={
                              item.status === 'Sent'
                                ? { fontWeight: 700, color: '#2c54e3' }
                                : undefined
                            }
                          >
                            {item.status}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography component="p" variant="body2">
                            {item.time &&
                              moment(item.time).format('DD/MM/YYYY')}
                          </Typography>
                        </TableCell>
                        <TableCell scope="row" component="th" padding="none">
                          <Box display="flex" justifyContent="center">
                            <Tooltip title="Resend">
                              <Box>
                                <IconButton
                                  onClick={() =>
                                    handleResendNotification(
                                      item.id,
                                      item.email,
                                    )
                                  }
                                  disabled={item.status === 'Submitted'}
                                >
                                  <ResendIcon />
                                </IconButton>
                              </Box>
                            </Tooltip>
                          </Box>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Box mb={4}>
        <Typography component="h2" variant="h4">
          {t('comments')}
        </Typography>
      </Box>
      <Box mb={5}>
        <Grid container>
          <Grid item xs={5}>
            <ReactiveBase
              app={ES_INDICES.comment_index_name}
              url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
              headers={{ Authorization: window.localStorage.getItem('jwt') }}
            >
              <ReactiveList
                componentId="commentResult"
                dataField="comment"
                loader="Loading comments..."
                showResultStats={false}
                renderNoResults={NoResponse}
                defaultQuery={customQuery}
              >
                {({ data }) => {
                  return (
                    <>
                      <Grid item xs={12}>
                        <Box mb={5}>
                          <Typography component="span">
                            {t('following_are_reviews_and_response')}
                          </Typography>
                          <Typography
                            component="span"
                            className={classes.opporTitle}
                          >{` ${opportunity?.title ?? ''}`}</Typography>
                        </Box>
                      </Grid>
                      {data &&
                        data.length > 0 &&
                        data.map((item: IComment, index: number) => (
                          <Grid container key={index}>
                            <Grid item xs={12}>
                              <Box mb={3}>
                                {index > 0 && (
                                  <Box mb="20px" bgcolor="#bbb">
                                    <Divider variant="fullWidth" />
                                  </Box>
                                )}
                                <Grid container>
                                  <Grid item xs={1}>
                                    <AccountCircleIcon
                                      className={classes.profileIcon}
                                    />
                                  </Grid>
                                  <Grid item xs={11}>
                                    <Box
                                      display="flex"
                                      justifyContent="space-between"
                                      mb={3}
                                      ml="10px"
                                    >
                                      <Box>
                                        <Typography
                                          component="p"
                                          variant="caption"
                                        >
                                          {item.created_by?.name ?? ''}
                                        </Typography>
                                      </Box>
                                      <Box>
                                        <Typography color="textSecondary">
                                          {item.time
                                            ? moment(item.time).format(
                                                'DD/MM/YYYY h:mma',
                                              )
                                            : ''}
                                        </Typography>
                                      </Box>
                                    </Box>
                                    <Box ml="10px">
                                      <Typography>
                                        <TruncateText
                                          text={item.comment_body}
                                        />
                                      </Typography>
                                    </Box>
                                  </Grid>
                                </Grid>
                              </Box>
                            </Grid>
                          </Grid>
                        ))}
                    </>
                  );
                }}
              </ReactiveList>
            </ReactiveBase>
          </Grid>
        </Grid>
      </Box>
      <Box>
        <StepButtonDetail
          opportunity={opportunity}
          handleSubmit={() => {
            handleNext();
          }}
          isDisableNext={false}
          handleBack={handleBack}
          deleteOpportunity={deleteOpportunity}
        />
      </Box>
      <Dialog
        open={isAddReviewerDialogOpen}
        onClose={() => setReviewerDialogOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <form onSubmit={email.handleSubmit}>
          <Box padding={3} width="450px">
            <DialogTitle>
              <Box display="flex" justifyContent="center">
                <Typography component="p" variant="caption">
                  Add Reviewer
                </Typography>
              </Box>
            </DialogTitle>
            <DialogContent>
              <Box mb={2} mt={2}>
                <Typography variant="caption" component="p">
                  Select an option below:
                </Typography>
              </Box>
              <FormControl>
                <RadioGroup
                  aria-label="action"
                  name="action"
                  value={receiverType}
                  onChange={handleChangeReceiver}
                >
                  <FormControlLabel
                    control={<Radio color="primary" />}
                    label="Sponsor Review"
                    value={EmailReceiver.SponsorReview}
                  />
                  <FormControlLabel
                    control={<Radio color="primary" />}
                    label="Peer Review"
                    value={EmailReceiver.PeerReview}
                  />
                </RadioGroup>
              </FormControl>
              <Box mt={2}>
                <TextField
                  id="email"
                  name="email"
                  type="email"
                  variant="outlined"
                  fullWidth
                  value={email.values.email}
                  onChange={email.handleChange}
                  onBlur={email.handleBlur}
                  error={email.touched.email && !!email.errors.email}
                  helperText={email.touched.email ? email.errors.email : ''}
                />
              </Box>
              <Box display="flex" justifyContent="center" mt={2}>
                <Button type="submit" variant="contained" color="primary">
                  Send Notification
                </Button>
              </Box>
            </DialogContent>
          </Box>
        </form>
      </Dialog>
    </Box>
  );
};
