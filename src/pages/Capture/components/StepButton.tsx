import React from 'react';
import Button from '@material-ui/core/Button';
import { Grid, Box, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IStepButtonDetailProps } from '../types';
import { UserProfileState } from '../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: '#bbb',
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(2),
  },
}));

export const StepButtonDetail: React.FC<IStepButtonDetailProps> = ({
  handleSubmit,
  handleBack,
  isDisableNext,
  deleteOpportunity,
  isHideBack = false,
  step,
  opportunity,
}) => {
  const classes = useStyles();
  let { id } = useParams();
  const userProfileState = useSelector(UserProfileState);
  const getSubmitButtonTitle = () => {
    if (step !== 'submit') {
      return 'Next';
    } else if (
      step === 'submit' &&
      opportunity &&
      opportunity?.opportunity_phase?.id !== 1
    ) {
      return 'Update';
    } else {
      return 'Submit';
    }
  };
  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <Divider variant="fullWidth" className={classes.divider} />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={6}>
          <Box
            display="flex"
            flexDirection="row"
            p={1}
            m={1}
            bgcolor="background.paper"
          >
            <Box p={1}>
              <Button
                onClick={deleteOpportunity}
                variant="outlined"
                disabled={
                  userProfileState.id !== opportunity?.created_by?.id &&
                  opportunity?.opportunity_phase?.name === 'capture'
                }
              >
                {id ? 'Delete' : 'Cancel'}
              </Button>
            </Box>
            <Box p={1}>
              <Button
                variant="outlined"
                component={RouterLink}
                to="/my-stuff"
                disabled={isDisableNext}
              >
                Save For Later
              </Button>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Box
            display="flex"
            flexDirection="row-reverse"
            p={1}
            m={1}
            bgcolor="background.paper"
          >
            <Box p={1}>
              <Button
                variant="contained"
                onClick={() => handleSubmit()}
                disabled={isDisableNext}
                color="primary"
              >
                {getSubmitButtonTitle()}
              </Button>
            </Box>
            <Box p={1}>
              {!isHideBack && (
                <Button onClick={handleBack} variant="outlined">
                  Back
                </Button>
              )}
            </Box>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};
