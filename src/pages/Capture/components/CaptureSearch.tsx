import React from 'react';
// import { SearchBase } from '../../../commons';
import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { Typography, Link, Breadcrumbs } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  capitalize: {
    textTransform: 'capitalize',
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
  },
}));

export const CaptureSearch: React.FC<{ handleNext: () => void }> = ({
  handleNext,
}) => {
  const classes = useStyles();
  let location = useLocation();
  const { t } = useTranslation();
  const pathArray = location.pathname.split('/').slice(1);
  return (
    <>
      <Breadcrumbs
        // separator="->"
        aria-label="breadcrumb"
      >
        {location.pathname === '/dashboard' && (
          <Typography>{t('home')}</Typography>
        )}
        {location.pathname !== '/dashboard' && (
          <Link color="inherit" component={RouterLink} to="/dashboard">
            {t('home')}
          </Link>
        )}
        {location.pathname !== '/dashboard' &&
          pathArray.length > 1 &&
          pathArray.map((path, index) => {
            return (
              <Link
                key={index}
                color="inherit"
                href={`/${path}`}
                className={classes.capitalize}
              >
                {path}
              </Link>
            );
          })}
        {location.pathname !== '/dashboard' && (
          <Typography className={classes.capitalize}>
            {pathArray[pathArray.length - 1]}
          </Typography>
        )}
      </Breadcrumbs>
      <Typography variant="h5">Search</Typography>
      {/* <SearchBase /> */}
      <Grid container className={classes.root} justify="center" spacing={3}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleNext}
          type="submit"
        >
          {t('capture_new_opp')}
        </Button>
      </Grid>
    </>
  );
};
