import {
  IOpportunity,
  IOpportunityParam,
  IAttachments,
} from '../../services/opportunity.services';
import {
  ICorporateObjective,
  IBusinessProcesses,
  IBenefitFocusCategory,
  IBenefitFocusValue,
  IBusinessUnit,
} from '../../services/references.services';
import { IComment, ICommentParam } from '../../services/comment.services';

interface ICaptureAction {
  handleNext: () => void;
  handleBack: () => void;
  isSubmitAll?: boolean;
  handleOpportunity: (
    values: IOpportunityParam,
    type?: string,
  ) => Promise<void>;
  handleComment: (values: ICommentParam, type?: string) => Promise<void>;
  deleteOpportunity: () => Promise<void>;
}

export interface ICaptureBenefitsProps extends ICaptureAction {
  opportunity?: IOpportunity;
  corporateObjectives: ICorporateObjective[];
  benefitFocuses: IBenefitFocusCategory[];
  benefitFocusValues: IBenefitFocusValue[];
}

export interface ICaptureDetailProps extends ICaptureAction {
  opportunity?: IOpportunity;
  businessUnits: IBusinessUnit[];
  businessProcessess: IBusinessProcesses[];
  corporateObjectives: ICorporateObjective[];
  benefitFocuses: IBenefitFocusCategory[];
  setOpportunity: React.Dispatch<
    React.SetStateAction<IOpportunity | undefined>
  >;
}

export interface ICaptureDuplicatesProps extends ICaptureAction {
  opportunity?: IOpportunity;
  comment?: IComment;
}

export interface ICaptureAttachmentsProps extends ICaptureAction {
  opportunity?: IOpportunity;
  opportunityQueryId?: string;
  attachments: Array<IAttachments>;
  handleChangeFile: (index: number, value: string) => void;
  handleRemoveFile?: (index: number) => void;
  handleRemove: (index: number) => void;
  handleAddAttachment: () => void;
  handleChangeDescription: (index: number, value: string) => void;
  handleUpdateAttachmentDescription: (value: string, index: number) => void;
}

export interface IEmailRequestReview {
  executiveReviews: string[];
  peerReviews: string[];
}
export interface ICaptureReviewsProps extends ICaptureAction {
  opportunity?: IOpportunity;
  emailRequestReview: IEmailRequestReview;
  onUpdateEmailRequestReview: (values: IEmailRequestReview) => void;
  notification: INotification[] | undefined;
  setNotification: React.Dispatch<React.SetStateAction<any>>;
}

export interface INotification {
  id: number;
  email: string;
  status: string;
  time: Date | string;
  type: string;
}
export interface ICaptureResponsesProps extends ICaptureAction {
  opportunity?: IOpportunity;
  comment?: IComment;
}
export interface ICaptureSubmitProps extends ICaptureAction {
  opportunity?: IOpportunity;
  handleFinalSubmit: () => void;
  benefitFocuses: IBenefitFocusCategory[];
}

export interface IStepButtonDetailProps {
  handleSubmit: (e?: React.FormEvent<HTMLFormElement> | undefined) => void;
  handleBack: () => void;
  isDisableNext: boolean;
  deleteOpportunity: () => Promise<void>;
  isHideBack?: boolean;
  step?: string;
  opportunity?: IOpportunity;
}
