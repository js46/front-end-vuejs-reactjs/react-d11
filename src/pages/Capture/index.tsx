import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Grid,
  Typography,
  Divider,
  Button,
  Box,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  DialogContentText,
  Step,
  Stepper,
  StepLabel,
  StepConnector,
} from '@material-ui/core';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { StepIconProps } from '@material-ui/core/StepIcon';
import clsx from 'clsx';
import {
  CaptureDetail,
  CaptureBenefits,
  CaptureDuplicates,
  CaptureReviews,
  CaptureSubmit,
  CaptureAttachments,
} from './components';
import {
  IOpportunity,
  OpportunityTask,
  IAttachments,
  IOpportunityParam,
  ICustomParam,
  createOpportunity,
  submitOpportunity,
  updateOpportunityTitle,
  deleteOpportunity,
  updateScreenId,
  updateOpportunityBusinessUnit,
  updateOpportunityDescription,
  updateOpportunityHypothesis,
  updateOpportunityBPI,
  updateOpportunityReqCompleteDate,
  updateOpportunityCorpObj,
  updateOpportunityBenefitMetric,
  updateOpportunityBenefitFocus,
  addOpportunityCorpObj,
  removeOpportunityCorpObj,
  addOpportunityBenefitFocus,
  removeOpportunityBenefitFocus,
  removeOpportunityBenefitMetrics,
  addOpportunityBenefitMetric,
  getAnOpportunity,
  getOpportunityTask,
  updateOpportunityTask,
  addAttachments,
  deleteAttachments,
  updateAttachment,
  updateOpportunitySponsorBusinessUnit,
  setOpportunityPrivate,
} from '../../services/opportunity.services';
import {
  getCorporateObjectives,
  ICorporateObjective,
  getBusinessProcesses,
  getBusinessUnits,
  IBusinessProcesses,
  IBenefitFocusCategory,
  getBenefitFocusCategories,
  getBenefitFocusValues,
  IBenefitFocusValue,
  IBusinessUnit,
} from '../../services/references.services';
import {
  IComment,
  ICommentParam,
  addOpportunityComment,
  updateOpportunityComment,
} from '../../services/comment.services';
import { IResponse } from '../../services/common.services';
import { useHistory, useParams, useLocation } from 'react-router-dom';
import { difference, differenceBy } from 'lodash';
import { TaskState, Message } from '../../constants';
import { useEffectOnlyOnce, useToggleValue } from '../../hooks';
import { isEmpty } from 'lodash';
import { IEmailRequestReview, INotification } from './types';
import { OpportunityPhase } from '../../constants/opportunityPhase';
import { DeleteDialog } from '../../components';
import { getOpportunityNotification } from '../../services/notification.services';
import {
  genNotificationType,
  genStringForMailingServiceStatus,
} from '../../utils';
import { useTranslation } from 'react-i18next';
import { UserProfileState } from '../../store/user/selector';
import { notifyError, notifySuccess } from '../../store/common/actions';

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: '#bbb',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(5),
  },
  successTitle: {
    lineHeight: '25px',
    fontWeight: 700,
    color: '#333',
    maxWidth: '350px',
    '& h2': {
      fontSize: '20px',
    },
  },
  successDialog: {
    '& .MuiDialog-paper': {
      padding: '50px 20px',
    },
  },
  stepper: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  backHomeButton: {
    border: '2px solid #ccc',
    padding: '5px 15px',
  },
}));

const useStepIconStyles = makeStyles({
  root: {
    color: '#eaeaf0',
    display: 'flex',
    height: 22,
    alignItems: 'center',
  },
  active: {
    color: '#2C54E3',
    '& > div': {
      backgroundColor: '#2C54E3',
      border: '2px solid #2C54E3',
    },
  },
  circle: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    backgroundColor: '#fff',
    border: '2px solid #ccc',
  },
  completed: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    border: '2px solid #ccc',
    backgroundColor: '#ccc',
    zIndex: 1,
    fontSize: 18,
  },
});

function StepIcon(props: StepIconProps) {
  const classes = useStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? (
        <div className={classes.completed} />
      ) : (
        <div className={classes.circle} />
      )}
    </div>
  );
}

const CustomConnector = withStyles({
  root: {
    left: 'calc(-50% + 10px)',
    right: 'calc(50% + 10px)',
    top: 10,
  },
  active: {
    '& $line': {
      borderTopWidth: '3px',
    },
  },
  completed: {
    '& $line': {
      borderTopWidth: '3px',
    },
  },
})(StepConnector);

interface IEmailRequest {
  executiveReviews: string[];
  peerReviews: string[];
}

const arraySteps = [
  'Detail',
  'Business Benefits',
  'Similarities/ Duplicates',
  'Add Attachments',
  'Request Reviews',
  'Submit',
];

export const Capture: React.FC = () => {
  const history = useHistory();
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState);
  const dispatch = useDispatch();

  const [businessUnits, setBusinessUnits] = useState<IBusinessUnit[]>([]);
  const [businessProcessess, setBusinessProcessess] = useState<
    IBusinessProcesses[]
  >([]);
  const [opportunity, setOpportunity] = useState<IOpportunity>();
  const [opportunityTask, setOpportunityTask] = useState<OpportunityTask>();

  const [comment, setComment] = useState<IComment>();
  const [attachments, setAttachments] = useState<Array<IAttachments>>([
    { id: 0, attachments: '', description: '' },
  ]);
  const [activeStep, setActiveStep] = useState(0);
  const [corporateObjectives, setCorporateObjectives] = useState<
    ICorporateObjective[]
  >([]);
  const [benefitFocuses, setBenefitFocuses] = useState<IBenefitFocusCategory[]>(
    [],
  );
  const [benefitFocusValues, setBenefitFocusValues] = useState<
    IBenefitFocusValue[]
  >([]);
  const [isLoading, setLoading] = useState(true);
  const [completed, setCompleted] = React.useState(new Set<number>());
  const [maxCompleted, setMaxCompleted] = React.useState(0);
  const [isSubmitAll, setSubmitStatus] = useState(false);
  const [deleteConfirm, toggleDeleteConfirm] = useToggleValue(false);

  const [emailRequestReview, setEmailRequestReview] = useState<IEmailRequest>({
    executiveReviews: [''],
    peerReviews: [''],
  });
  const [notification, setNotification] = useState<INotification[]>();
  const { t } = useTranslation();
  let { id } = useParams();
  const query = new URLSearchParams(useLocation().search);
  let task_id = query.get('task');

  useEffectOnlyOnce(() => {
    const getData = async () => {
      await Promise.all([
        getDataCorporateObjectives(),
        getDataBenefitFocus(),
        getDataBusinessUnits(),
        getDataBusinessProcesses(),
        getDataOpportunity(),
        getDataTask(),
        getDataBenefitFocusValues(),
      ]);
      setLoading(false);
    };
    getData();
  });

  useEffectOnlyOnce(() => {
    if (!id) return;
    getOpportunityNotification(Number(id)).then(data => {
      const listNotification = data.data.list
        ?.filter(item => item.opportunity.id === Number(id))
        .map(noti => ({
          id: noti.id,
          email: noti.to_email,
          type: genNotificationType(noti?.notification_user_case?.name),
          status: genStringForMailingServiceStatus(noti.status),
          time: new Date(noti.sent_date),
        }));
      setNotification(listNotification);
    });
  });

  const getDataCorporateObjectives = async () => {
    const response = await getCorporateObjectives();
    setCorporateObjectives(response.data.list);
  };

  const getDataBenefitFocus = async () => {
    const response = await getBenefitFocusCategories();
    setBenefitFocuses(response.data.list);
  };

  const getDataBenefitFocusValues = async () => {
    const json = await getBenefitFocusValues();
    if (json.success) setBenefitFocusValues(json.data.list);
  };

  const getDataBusinessUnits = async () => {
    const response = await getBusinessUnits();
    setBusinessUnits(response.data.list);
  };

  const getDataBusinessProcesses = async () => {
    const response = await getBusinessProcesses();
    setBusinessProcessess(response.data.list);
  };

  const getDataOpportunity = async () => {
    if (!id) return;
    const response = await getAnOpportunity(parseInt(id));
    if (response.data) {
      setOpportunity(response.data);
      setActiveStep(response.data.screen_id);
      // let completed_step = response.data.completed_step || 0;
      setMaxCompleted(response.data.completed_step);
      for (var i = 0; i <= response.data.completed_step; i++) {
        setCompleted(completed.add(i));
      }
    }
  };
  const getDataTask = async () => {
    if (task_id) {
      const response = await getOpportunityTask(parseInt(task_id));
      if (response.success) {
        setOpportunityTask(response.data);
      }
    }
  };

  const isFirstStep = (step: number) => {
    return step === 0;
  };

  function isStepComplete(step: number) {
    return completed.has(step);
  }

  const handleStep = (step: number) => () => {
    if (opportunity && completed.has(step)) {
      setActiveStep(step);
    }
  };

  const handleBack = async () => {
    if (!opportunity || !opportunity.id) return;

    if (activeStep === 1) setActiveStep(activeStep - 1);
    const response = await updateScreenId(
      { screen_id: activeStep - 1 },
      opportunity.id,
    );
    if (response.success) {
      setActiveStep(activeStep - 1);
    }
  };

  const handleNext = async () => {
    setCompleted(completed.add(activeStep));
    if (opportunity && opportunity.id) {
      try {
        let customParam = {} as ICustomParam;
        customParam.screen_id = activeStep + 1;

        if (maxCompleted < activeStep + 1) {
          setMaxCompleted(maxCompleted + 1);
          customParam.completed_step = maxCompleted;
        }
        const response = await submitOpportunity(customParam, opportunity.id);
        if (response.success) {
          setActiveStep(activeStep + 1);
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  const handleComment = async (values: ICommentParam, type?: string) => {
    // if (type === 'next_step') {
    //   handleNext();
    //   return;
    // }
    if (!opportunity || !opportunity.id) return;

    const bodyRequest = {
      entity_id: opportunity.id,
      entity_type: 'opportunity',
      comment_type: 'default',
      comment_body: values.comment_body,
      comment_parent_id: 0,
    };
    if (!comment) {
      const response = await addOpportunityComment(bodyRequest);
      if (response.success) {
        setComment(response.data);
      }
      return;
    }
    if (comment && values.comment_body !== comment?.comment_body) {
      const response = await updateOpportunityComment(
        bodyRequest,
        comment ? comment.id : 0,
      );
      if (response.success) {
        setComment(response.data);
      }
    }
  };

  const handleFinalSubmit = async () => {
    const updatedState = {
      task_state: TaskState.Completed,
    };
    const submittedData = {
      screen_id: activeStep,
      completed_step: maxCompleted,
      opportunity_state_id: 1,
      opportunity_phase_id: OpportunityPhase.Shape,
      opportunity_prev_phase_id: OpportunityPhase.Capture,
      capture_submit_date: new Date().toISOString(),
    };
    if (opportunity && opportunity.id) {
      const response = await submitOpportunity(submittedData, opportunity.id);
      if (response.success) {
        setSubmitStatus(true);
      }
    }
    if (opportunityTask && opportunityTask.id)
      updateOpportunityTask(opportunityTask.id, updatedState);
  };

  const handleOpportunity = async (
    values: IOpportunityParam,
    type?: string,
  ) => {
    let response: IResponse<IOpportunity>;
    // let commentResponse: IResponse<IComment>;
    if (!opportunity) {
      response = await createOpportunity({
        ...values,
        resource_team_id: userProfileState?.resource_team?.resource_team_id,
        screen_id: activeStep,
      });
    } else {
      let body = { ...values };
      body.benefit_metrics = body.benefit_metrics?.filter(
        item => !isEmpty(item.trim()),
      );
      // switch by type to update special field
      switch (type) {
        case 'title':
          if (values.title === opportunity.title) return;
          response = await updateOpportunityTitle(values, opportunity.id);
          break;
        case 'businessUnits':
          response = await updateOpportunityBusinessUnit(
            values,
            opportunity.id,
          );
          break;
        case 'sponsor_business_unit_id':
          response = await updateOpportunitySponsorBusinessUnit(
            values.sponsor_business_unit_id as number,
            opportunity.id,
          );
          break;
        case 'description':
          if (values.description === opportunity.description) return;
          response = await updateOpportunityDescription(values, opportunity.id);
          break;
        case 'hypothesis':
          if (values.hypothesis === opportunity.hypothesis) return;
          response = await updateOpportunityHypothesis(values, opportunity.id);
          break;
        case 'processes':
          response = await updateOpportunityBPI(values, opportunity.id);
          break;
        case 'requested_completion_date':
          response = await updateOpportunityReqCompleteDate(
            values,
            opportunity.id,
          );
          break;
        case 'private_flag':
          response = await setOpportunityPrivate(
            opportunity.id,
            values.private_flag,
          );
          break;
        case 'corporate_objectives':
          response = await updateOpportunityCorpObj(values, opportunity.id);
          break;
        case 'add_corporate_objectives':
          response = await addOpportunityCorpObj(values, opportunity.id);
          break;
        case 'remove_corporate_objectives':
          if (
            values.corporate_objectives &&
            opportunity.corporate_objectives &&
            values.corporate_objectives.length > 0 &&
            difference(
              opportunity.corporate_objectives.map(item => item.id),
              values.corporate_objectives,
            ).length > 0
          ) {
            response = await removeOpportunityCorpObj(
              difference(
                opportunity.corporate_objectives.map(item => item.id),
                values.corporate_objectives,
              ),
              opportunity.id,
            );
          } else {
            return;
          }
          break;
        case 'benefit_focuses':
          response = await updateOpportunityBenefitFocus(
            values,
            opportunity.id,
          );
          break;
        case 'add_benefit_focuses':
          response = await addOpportunityBenefitFocus(values, opportunity.id);
          break;
        case 'remove_benefit_focuses':
          if (
            values.benefit_focuses &&
            opportunity.benefit_focuses &&
            differenceBy(
              opportunity.benefit_focuses,
              values.benefit_focuses,
              'benefit_focus_id',
            ).length > 0
          ) {
            response = await removeOpportunityBenefitFocus(
              differenceBy(
                opportunity.benefit_focuses,
                values.benefit_focuses,
                'benefit_focus_id',
              ).map(item => ({ id: item.id })),
              opportunity.id,
            );
          } else {
            return;
          }
          break;
        case 'benefit_metrics':
          if (
            JSON.stringify(values.benefit_metrics) ===
            JSON.stringify(opportunity.benefit_metrics)
          )
            return;
          response = await updateOpportunityBenefitMetric(body, opportunity.id);
          break;
        case 'add_benefit_metrics':
          response = await addOpportunityBenefitMetric(body, opportunity.id);
          break;
        case 'remove_benefit_metrics':
          if (
            values.benefit_metrics &&
            opportunity.benefit_metrics &&
            difference(
              opportunity.benefit_metrics,
              values.benefit_metrics,
            )[0] !== ''
          ) {
            response = await removeOpportunityBenefitMetrics(
              difference(opportunity.benefit_metrics, values.benefit_metrics),
              opportunity.id,
            );
          } else {
            return;
          }
          break;
        default:
          response = await submitOpportunity(
            { ...values, screen_id: activeStep },
            opportunity.id,
          );
      }
    }
    if (response.success) {
      setOpportunity(response.data);
    }
    return;
  };

  const confirmDelete = async () => {
    if (opportunity) return toggleDeleteConfirm();
    history.push('/');
  };

  const handleDeleteOpportunity = async () => {
    if (opportunity) {
      const data = await deleteOpportunity(opportunity.id);
      if (data.success) {
        dispatch(
          notifySuccess({
            message: t('delete_opp_success'),
          }),
        );
        setOpportunity(undefined);
        history.push('/');
      } else {
        dispatch(
          notifyError({
            message: Message.Error,
          }),
        );
      }
    }
  };
  const handleChangeFile = async (index: number, value: string) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].attachments = value;
    setAttachments(updatedAttachments);
    if (opportunity?.id) {
      const formData = new FormData();
      formData.append('attachment', value);
      formData.append('entity_id', opportunity?.id.toString());
      formData.append('description', attachments[index].description);
      formData.append('entity_type', 'opportunity');
      const response = await addAttachments(formData);
      if (response.success) {
        setAttachments(
          attachments.map((attachment, atmIndex) => {
            if (atmIndex === index) {
              return { ...attachment, id: response.data.id };
            } else {
              return attachment;
            }
          }),
        );
      }
    }
  };
  const handleRemoveFile = (index: number) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].attachments = '';
    setAttachments(updatedAttachments);
  };

  const handleRemove = (index: number) => {
    let updatedAttachments = [...attachments];
    updatedAttachments = updatedAttachments
      .slice(0, index)
      .concat(updatedAttachments.slice(index + 1));
    setAttachments(updatedAttachments);
    deleteAttachment(index);
  };
  const deleteAttachment = async (index: number) => {
    const response = await deleteAttachments(attachments[index].id);
    if (response.success) {
      setAttachments(
        attachments.filter((attachment, atmIndex) => atmIndex !== index),
      );
    }
  };
  const handleAddAttachment = () => {
    setAttachments(
      attachments.concat([
        { id: attachments.length + 1, attachments: '', description: '' },
      ]),
    );
  };
  const handleChangeDescription = (index: number, value: string) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].description = value;
    setAttachments(updatedAttachments);
  };

  const handleUpdateAttachmentDescription = async (
    value: string,
    index: number,
  ) => {
    if (opportunity?.id) {
      updateAttachment(attachments[index].id, { description: value });
    }
  };

  const handleUpdateEmailRequestReview = (values: IEmailRequestReview) => {
    setEmailRequestReview(values);
  };

  const renderStep = (step: number) => {
    switch (step) {
      case 1:
        return (
          <CaptureBenefits
            handleNext={handleNext}
            handleComment={handleComment}
            handleBack={handleBack}
            benefitFocuses={benefitFocuses}
            benefitFocusValues={benefitFocusValues}
            handleOpportunity={handleOpportunity}
            opportunity={opportunity}
            isSubmitAll={false}
            corporateObjectives={corporateObjectives}
            deleteOpportunity={confirmDelete}
          />
        );
      case 2:
        return (
          <CaptureDuplicates
            comment={comment}
            opportunity={opportunity}
            isSubmitAll={false}
            handleComment={handleComment}
            handleNext={handleNext}
            handleBack={handleBack}
            handleOpportunity={handleOpportunity}
            deleteOpportunity={confirmDelete}
          />
        );
      case 3:
        return (
          <CaptureAttachments
            opportunity={opportunity}
            opportunityQueryId={id}
            attachments={attachments}
            isSubmitAll={false}
            handleChangeFile={handleChangeFile}
            handleRemove={handleRemove}
            handleRemoveFile={handleRemoveFile}
            handleAddAttachment={handleAddAttachment}
            handleChangeDescription={handleChangeDescription}
            handleUpdateAttachmentDescription={
              handleUpdateAttachmentDescription
            }
            handleNext={handleNext}
            handleBack={handleBack}
            handleOpportunity={handleOpportunity}
            handleComment={handleComment}
            deleteOpportunity={confirmDelete}
          />
        );
      case 4:
        return (
          <CaptureReviews
            handleComment={handleComment}
            handleNext={handleNext}
            handleBack={handleBack}
            handleOpportunity={handleOpportunity}
            opportunity={opportunity}
            isSubmitAll={false}
            deleteOpportunity={confirmDelete}
            emailRequestReview={emailRequestReview}
            onUpdateEmailRequestReview={handleUpdateEmailRequestReview}
            notification={notification}
            setNotification={setNotification}
          />
        );
      case 5:
        return (
          <CaptureSubmit
            handleComment={handleComment}
            handleNext={handleNext}
            handleBack={handleBack}
            handleOpportunity={handleOpportunity}
            opportunity={opportunity}
            deleteOpportunity={confirmDelete}
            handleFinalSubmit={handleFinalSubmit}
            isSubmitAll={true}
            benefitFocuses={benefitFocuses}
          />
        );
      case 0:
      default:
        return (
          <CaptureDetail
            handleComment={handleComment}
            handleNext={handleNext}
            handleBack={handleBack}
            businessUnits={businessUnits}
            handleOpportunity={handleOpportunity}
            setOpportunity={setOpportunity}
            opportunity={opportunity}
            businessProcessess={businessProcessess}
            deleteOpportunity={confirmDelete}
            benefitFocuses={benefitFocuses}
            corporateObjectives={corporateObjectives}
          />
        );
    }
  };
  const handleRedirect = () => {
    if (opportunity && opportunity?.opportunity_phase?.id !== 1) {
      history.push('/my-stuff/todo-list');
      return;
    }
    history.push('/');
  };
  const getArrayForStepper = () => {
    if (opportunity && opportunity?.opportunity_phase?.id !== 1) {
      const newArraySteps = [...arraySteps];
      newArraySteps[newArraySteps.length - 1] = 'Update';
      return newArraySteps;
    } else {
      return arraySteps;
    }
  };
  return (
    <Box mt={2}>
      {opportunity && opportunity?.opportunity_phase?.id !== 1 ? (
        <Typography variant="h4">{t('opp_backlog')}</Typography>
      ) : (
        <Typography variant="h4">{t('capture_opp')}</Typography>
      )}
      <Grid container>
        <Grid container>
          <Grid item xs={6}>
            <Stepper
              // nonLinear
              alternativeLabel
              activeStep={activeStep}
              connector={<CustomConnector />}
            >
              {getArrayForStepper().map((label, index) => {
                const stepProps: { completed?: boolean } = {};
                // const buttonProps: { optional?: React.ReactNode } = {};
                return (
                  <Step key={label} {...stepProps}>
                    {/* <StepButton
                      
                      {...buttonProps}
                    >
                      
                    </StepButton> */}
                    <StepLabel
                      onClick={handleStep(index)}
                      completed={isStepComplete(index)}
                      StepIconComponent={StepIcon}
                      className={isStepComplete(index) ? classes.stepper : ''}
                    >
                      {label}
                    </StepLabel>
                  </Step>
                );
              })}
            </Stepper>
          </Grid>
          <Grid item xs={3} />
          <Grid item xs={3}>
            <Box
              display="flex"
              flexDirection="row-reverse"
              p={1}
              m={1}
              bgcolor="background.paper"
            >
              <Box p={1}>
                {activeStep < arraySteps.length - 1 && (
                  <Button
                    variant="contained"
                    // className={clsx(classes.commonButton, classes.nextButton)}
                    color="primary"
                    onClick={handleNext}
                    disabled={!opportunity}
                  >
                    {t('next')}
                  </Button>
                )}
              </Box>

              <Box p={1}>
                {!isFirstStep(activeStep) && (
                  <Button onClick={handleBack} variant="outlined">
                    {t('back')}
                  </Button>
                )}
              </Box>
            </Box>
          </Grid>
        </Grid>

        <Dialog
          open={isSubmitAll}
          onClose={() => setSubmitStatus(false)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          className={classes.successDialog}
        >
          <DialogTitle id="alert-dialog-title" className={classes.successTitle}>
            <Box textAlign="center">{t('opp_success')}</Box>
          </DialogTitle>
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            height="150px"
          >
            <CheckCircleOutlineIcon style={{ transform: 'scale(6)' }} />
          </Box>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {t('opp_created_on_shape')}
            </DialogContentText>
            <DialogContentText id="alert-dialog-description">
              Opportunity backlog for the {opportunity?.resource_team?.name}{' '}
              Team
            </DialogContentText>
            <DialogContentText id="alert-dialog-description">
              Notification sent to {opportunity?.resource_team?.name} Team
              Leader.
            </DialogContentText>
          </DialogContent>
          <DialogActions style={{ justifyContent: 'center' }}>
            <Button
              onClick={handleRedirect}
              className={classes.backHomeButton}
              autoFocus
            >
              {opportunity?.opportunity_phase?.id !== 1
                ? t('back_to_task')
                : t('back_to_home')}
            </Button>
          </DialogActions>
        </Dialog>

        <Grid item xs={12}>
          <Divider variant="fullWidth" className={classes.divider} />
          {!isLoading && renderStep(activeStep)}
        </Grid>
      </Grid>

      <DeleteDialog
        isOpen={deleteConfirm}
        header={t('delete_opp_title')}
        message={
          <Box>
            <Typography component="p" variant="body1">
              {t('delete_opp')}
            </Typography>
          </Box>
        }
        handleClose={toggleDeleteConfirm}
        handleDelete={handleDeleteOpportunity}
      />
    </Box>
  );
};

export default Capture;
