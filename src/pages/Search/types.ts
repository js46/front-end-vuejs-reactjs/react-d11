export enum ESearchType {
  opp = 1,
  expert = 2,
  finding = 3,
}
export interface ItemCountSearch {
  order: number;
  index: string;
  label: string;
  doc_count: number;
}
export interface IESIndex {
  opp: string;
  expert: string;
  finding: string;
}
