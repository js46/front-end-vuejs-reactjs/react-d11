import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../store/config/selector';
import { ItemCountSearch } from './types';
import { useQueryOpportunity } from '../../hooks';
import { SearchBase } from './components/SearchBase';
import { SearchCountByList } from './components/SearchCountByList';
import { OpportunityCountByExpert } from '../../components/opportunity/OpportunityCountByExpert';

const Search: React.FC = () => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const [opportunityCount, setOpportunityCount] = useState<
    Map<number, number>
  >();
  const [listCountDefault, setListCountDefaultSearch] = useState<
    ItemCountSearch[]
  >([
    {
      index: ES_INDICES.opportunity_index_name,
      label: 'Opportunities',
      order: 1,
      doc_count: 0,
    },
    {
      index: ES_INDICES.user_profile_index_name,
      label: 'Experts',
      order: 2,
      doc_count: 0,
    },
    {
      index: ES_INDICES.opportunity_index_finding,
      label: 'Finding',
      order: 3,
      doc_count: 0,
    },
  ]);

  const extraQueryOpp = {
    filter: {
      range: {
        'opportunity_phase.id': {
          gte: 1,
        },
      },
    },
  };
  const { defaultQuery } = useQueryOpportunity(true, extraQueryOpp);

  const classes = useStyles();
  let defQueryOpp = defaultQuery();
  defQueryOpp.query.bool.must.push({
    term: {
      _index: ES_INDICES.opportunity_index_name,
    },
  });
  const defaultQueryCount = {
    query: {
      bool: {
        should: [
          defQueryOpp.query,
          {
            bool: {
              must: [
                {
                  term: {
                    _index: ES_INDICES.user_profile_index_name,
                  },
                },
                { term: { casbin_role: 'role:expert' } },
              ],
            },
          },
          {
            bool: {
              must: [
                {
                  term: {
                    _index: ES_INDICES.opportunity_index_finding,
                  },
                },
              ],
            },
          },
        ],
      },
    },
  };

  return (
    <>
      <OpportunityCountByExpert
        setOpportunityCount={setOpportunityCount}
        opportunityCount={opportunityCount}
        isExpertMenu
      />
      <SearchCountByList
        defaultQuery={() => defaultQueryCount}
        setList={setListCountDefaultSearch}
        lists={listCountDefault}
        indexList={{
          opp: ES_INDICES.opportunity_index_name,
          finding: ES_INDICES.opportunity_index_finding,
          expert: ES_INDICES.user_profile_index_name,
        }}
      />

      <Grid container className={classes.root}>
        {!!opportunityCount && (
          <SearchBase
            listCountDefault={listCountDefault}
            defaultQueryOpp={defaultQuery}
            opportunityCount={opportunityCount}
            indexList={{
              opp: ES_INDICES.opportunity_index_name,
              finding: ES_INDICES.opportunity_index_finding,
              expert: ES_INDICES.user_profile_index_name,
            }}
          />
        )}
      </Grid>
    </>
  );
};
export default Search;

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  grid: {
    flexBasis: '0px!important',
  },
}));
