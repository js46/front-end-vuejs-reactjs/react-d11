import React, { useState, useEffect, useCallback } from 'react';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { ItemCountSearch, ESearchType, IESIndex } from '../types';
import { Grid, Typography, Box, Button, Chip } from '@material-ui/core';
import {
  StyledDataSearch,
  StyledReactiveComponent,
} from '../../../components/styled';
import { OppSidebar, ExpertSidebar, FindingSidebar } from './Sidebar';

import { useTranslation } from 'react-i18next';
import { SelectedFiltersCustom } from '../../../components';
import { FindingTable, OppTable, ExpertTable } from './Table';
import { useSearchCombine } from '../../../hooks';
import ReactDOM from 'react-dom';

import { SearchCountByList } from './SearchCountByList';
import {
  customQuerySearchExpert,
  customCombineQueryDefault,
  customQuerySearchOpportunity,
  customQuerySearchFinding,
  customCombineSearchQuery,
} from '../../../utils';

interface SearchBaseProps {
  listCountDefault: ItemCountSearch[];
  defaultQueryOpp: () => { query: { bool: { must: object[] } } };

  indexList: IESIndex;
  opportunityCount?: Map<number, number>;
}
export const SearchBase: React.FC<SearchBaseProps> = ({
  listCountDefault,
  defaultQueryOpp,
  opportunityCount,
  indexList,
}) => {
  const [type, setType] = useState<number>(1);
  let [listCount, setListCount] = useState<ItemCountSearch[]>([]);
  const [currentCount, setCurrentCount] = useState<number>();
  const [taskStates, setTaskState] = useState<string[]>([]);
  const [queryCountAll, setQueryCountAll] = useState<object>();
  const { i18n } = useTranslation();

  const handleChooseSearchType = (value: number) => {
    Promise.resolve().then(() => {
      ReactDOM.unstable_batchedUpdates(() => {
        setCurrentCount(undefined);
        setType(value);
        if (keyword) {
          onValueSelected(keyword ?? '');
        }
      });
    });
  };

  useEffect(() => {
    if (currentCount !== undefined) {
      let checkChange = false;
      let countTemp = listCount.map(item => {
        if (item.order === type) {
          if (item.doc_count !== currentCount) {
            checkChange = true;
          }
        }

        return {
          ...item,
          doc_count:
            item.order === type && item.doc_count !== currentCount
              ? currentCount
              : item.doc_count,
        };
      });
      if (checkChange) {
        setListCount(countTemp);
      }
    } else {
      setListCount(listCountDefault);
    }
  }, [currentCount, listCount, listCountDefault, type]);

  const defaultQuery = customCombineQueryDefault(type, defaultQueryOpp().query);
  const {
    searchBtnRef,
    value,
    onClearSearch,
    onValueSelected,
    onChange,
    keyword,
    onValueChange,
  } = useSearchCombine();
  const searchQuery = customCombineSearchQuery(
    type,
    defaultQueryOpp().query.bool.must,
    keyword,
  );

  const onClickSearch = useCallback(
    (setQuery: Function) => {
      if (!keyword) return setQuery({});
      setQuery({
        value: keyword,
        query: searchQuery,
      });
    },
    [keyword, searchQuery],
  );
  const onKeyPress = (e: KeyboardEvent, triggerQuery: Function) => {
    if (e.key === 'Enter') {
      if (keyword) {
        onValueSelected(keyword ?? '');
        const querySearchExpert = customQuerySearchExpert(
          keyword,
          indexList.expert,
        );
        const querySearchOpp = customQuerySearchOpportunity(
          defaultQueryOpp().query.bool.must,
          keyword,
          indexList.opp,
        );
        const querySearchFinding = customQuerySearchFinding(
          keyword,
          indexList.finding,
        );
        setQueryCountAll({
          query: {
            bool: {
              should: [
                querySearchExpert.query,
                querySearchOpp.query,
                querySearchFinding.query,
              ],
            },
          },
        });
      }
    }
  };
  useEffect(() => {
    if (!queryCountAll) {
      setListCount(listCountDefault);
    }
  }, [listCountDefault, queryCountAll]);

  useEffect(() => {
    if (!keyword) {
      setQueryCountAll(undefined);
    }
  }, [keyword]);
  return (
    <>
      <ReactiveBase
        app={
          listCountDefault.find(item => item.order === type)?.index ??
          indexList.opp
        }
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <Grid container>
          <Grid item lg={2} xl={2} sm={12} xs={12}>
            <Typography variant="h4">{i18n.t('search')}</Typography>

            {!!queryCountAll && (
              <SearchCountByList
                setList={setListCount}
                type={type}
                lists={listCount}
                indexList={indexList}
                defaultQuery={() => queryCountAll}
              />
            )}
            <Box mt={2}>
              {type === ESearchType.opp && (
                <OppSidebar
                  defaultQuery={() => defaultQuery}
                  setTaskState={setTaskState}
                  taskStates={taskStates}
                />
              )}
              {type === ESearchType.expert && (
                <ExpertSidebar defaultQuery={() => defaultQuery} />
              )}
              {type === ESearchType.finding && (
                <FindingSidebar defaultQuery={() => defaultQuery} />
              )}
            </Box>
          </Grid>
          <Grid item lg={10} xl={10} xs={12} sm={12}>
            <Grid container>
              <Grid item lg={10} xl={10} xs={12} sm={12}>
                <StyledDataSearch
                  placeholder="Search "
                  componentId="SearchInput"
                  dataField={[
                    'title',
                    'description',
                    'opportunity_type.name',
                    'created_by.name',
                  ]}
                  showClear={true}
                  showFilter={false}
                  autosuggest={false}
                  innerClass={{
                    input: 'searchInput',
                    title: 'titleSearch',
                  }}
                  onKeyPress={onKeyPress}
                  defaultQuery={() => searchQuery}
                  /* parseSuggestion={suggestion => {
                    return {
                      label: (
                        <div>
                          <Grid container>
                            <Grid item xs={12}>
                              <Box fontWeight="fontWeightBold">
                                {removeHtmlContent(suggestion.label)}
                              </Box>
                            </Grid>
                          </Grid>
                          <Grid container>
                            {suggestion.source._index === indexList.opp && (
                              <Grid item xs={12}>
                                <span style={{ fontStyle: 'italic' }}>
                                  Completion date at:
                                </span>
                                {suggestion.source.completion_date !==
                                  '0001-01-01T00:00:00Z' &&
                                  moment(
                                    suggestion.source.completion_date,
                                  ).format('DD/MM/YYYY')}
                                |
                                <span style={{ fontStyle: 'italic' }}>
                                  Opportunity type:
                                </span>
                                {suggestion.source.opportunity_type.name}
                              </Grid>
                            )}
                            {suggestion.source._index === indexList.expert && (
                              <Grid item xs={12}>
                                <span style={{ fontStyle: 'italic' }}>
                                  Role:
                                </span>
                                {
                                  suggestion.source.profile_role
                                    ?.profile_role_name
                                }
                                |
                                <span style={{ fontStyle: 'italic' }}>
                                  Location:
                                </span>
                                {suggestion.source.location?.name || 'N/A'}
                              </Grid>
                            )}
                          </Grid>
                        </div>
                      ),
                      value:
                        suggestion.source?.title ?? suggestion.source?.name,
                      source: suggestion.source,
                    };
                  }} */
                  onChange={onChange}
                  value={value}
                  onValueChange={onValueChange}
                  onValueSelected={onValueSelected}
                />
                <Box mt={2}>
                  {listCount?.map((item: any, index: number) => (
                    <Chip
                      key={index}
                      label={`${item.label} (${item.doc_count}) `}
                      variant="outlined"
                      color={type === item.order ? 'primary' : 'default'}
                      onClick={() => handleChooseSearchType(item.order)}
                    />
                  ))}
                </Box>
              </Grid>
              <Grid item lg={2} xl={2} xs={12} sm={12}>
                <Box
                  display="flex"
                  justifyContent={{
                    xs: 'flex-start',
                    sm: 'flex-start',
                    lg: 'flex-end',
                    xl: 'flex-end',
                  }}
                >
                  <StyledReactiveComponent
                    componentId="Search"
                    filterLabel="Keyword"
                    showFilter={true}
                    render={({ setQuery }) => (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => onClickSearch(setQuery)}
                        ref={searchBtnRef}
                      >
                        Search
                      </Button>
                    )}
                  />
                </Box>
              </Grid>
            </Grid>
            <Grid container>
              <Box mt={1} mb={1}>
                <SelectedFiltersCustom onClearSearch={onClearSearch} />
              </Box>
            </Grid>
            <Grid item sm={12} xs={12}>
              <Box>
                {type === ESearchType.opp && (
                  <OppTable
                    defaultQuery={() => defaultQuery}
                    setCount={setCurrentCount}
                    taskStates={taskStates}
                  />
                )}
                {type === ESearchType.expert && (
                  <ExpertTable
                    defaultQuery={() => defaultQuery}
                    opportunityCount={opportunityCount}
                    setCount={setCurrentCount}
                  />
                )}
                {type === ESearchType.finding && (
                  <FindingTable
                    defaultQuery={() => defaultQuery}
                    setCount={setCurrentCount}
                  />
                )}
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </ReactiveBase>
    </>
  );
};
