import React from 'react';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { StyledSingleList } from '../../../components/styled';
import { IESIndex, ItemCountSearch } from '../types';
import { isEqual } from 'lodash';
interface SearchCountByListProps {
  defaultQuery: () => object;
  indexList: IESIndex;
  type?: number;
  lists: ItemCountSearch[];
  setList: React.Dispatch<React.SetStateAction<ItemCountSearch[]>>;
}
export const SearchCountByList: React.FC<SearchCountByListProps> = ({
  defaultQuery,
  indexList,
  setList,
  type,
  lists,
}) => {
  return (
    <>
      <ReactiveBase
        app={`${indexList.opp},${indexList.expert},${indexList.finding}`}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <StyledSingleList
          componentId="Search Count Type"
          showSearch={false}
          dataField="_index"
          showFilter
          defaultQuery={defaultQuery}
          transformData={(data: any[]) => {
            if (data) {
              let arr = lists?.map(item => ({
                ...item,
                doc_count:
                  !type || type !== item.order
                    ? data?.find(itemChild => itemChild.key === item.index)
                        ?.doc_count ?? 0
                    : item.doc_count,
              }));

              if (!isEqual(arr, lists)) setList(arr);
            }
            return data;
          }}
          render={() => null}
        />
      </ReactiveBase>
    </>
  );
};
