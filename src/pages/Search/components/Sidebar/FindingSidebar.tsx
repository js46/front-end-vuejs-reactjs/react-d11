import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Box, Typography } from '@material-ui/core';
import {
  ExpansionPanelDetailFilter,
  ExpansionPanelFilter,
  ExpansionPanelSummaryFilter,
  StyledMultiList,
  StyledDateRange,
} from '../../../../components/styled';
import { useTranslation } from 'react-i18next';
import { datePickerInputProps } from '../../../../utils';
import { useSelector, shallowEqual } from 'react-redux';
import { UserProfileState } from '../../../../store/user/selector';
interface FindingSidebarProps {
  defaultQuery?: () => object;
}
export const FindingSidebar: React.FC<FindingSidebarProps> = ({
  defaultQuery,
}) => {
  const { t } = useTranslation();
  const userProfileState = useSelector(UserProfileState, shallowEqual);
  return (
    <>
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="role-content"
          id="role-header"
        >
          <Typography variant="h6">{t('last_updated_at')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledDateRange
              componentId="Date"
              innerClass={{
                'input-container': 'input-date',
              }}
              placeholder={{
                start: 'From',
                end: 'To',
              }}
              queryFormat="date"
              dataField="last_updated_at"
              dayPickerInputProps={datePickerInputProps}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
      {/*Filter by Opportunity Type*/}
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="role-content"
          id="role-header"
        >
          <Typography variant="h6">{t('opportunity_type')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledMultiList
              componentId="Opportunity"
              dataField="opportunity.opportunity_type.keyword"
              filterLabel="opportunity_type"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: ['Public flag', 'Search', 'Date', 'Tags'],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
      {/*End Filter by Opportunity Type*/}

      {/*Filter by Privacy*/}
      {(userProfileState.isAdmin || userProfileState.internal) && (
        <ExpansionPanelFilter>
          <ExpansionPanelSummaryFilter
            expandIcon={<ExpandMoreIcon />}
            aria-controls="resource-team-content"
            id="resource-team-header"
          >
            <Typography variant="h6">{t('privacy')}</Typography>
          </ExpansionPanelSummaryFilter>
          <ExpansionPanelDetailFilter>
            <Box width="100%">
              <StyledMultiList
                componentId="Public flag"
                filterLabel="Privacy"
                dataField="public_flg"
                showCheckbox={true}
                showSearch={false}
                innerClass={{
                  label: 'multiList',
                }}
                customQuery={function(values?: string[]) {
                  if (!values || values.length >= 2) return undefined;
                  return {
                    query: {
                      bool: {
                        must: [
                          {
                            term: {
                              public_flg: values[0] === 'Public',
                            },
                          },
                        ],
                      },
                    },
                  };
                }}
                transformData={(data: any[]) => {
                  if (data) {
                    return data.map(item => ({
                      key: item.key === 1 ? 'Public' : 'Private',
                      doc_count: item.doc_count,
                    }));
                  }
                  return data;
                }}
                react={{
                  and: ['Opportunity', 'Search', 'Date', 'Tags'],
                }}
                defaultQuery={defaultQuery}
              />
            </Box>
          </ExpansionPanelDetailFilter>
        </ExpansionPanelFilter>
      )}
      {/*End Filter by Privacy*/}
      <ExpansionPanelFilter>
        <ExpansionPanelSummaryFilter
          expandIcon={<ExpandMoreIcon />}
          aria-controls="role-content"
          id="role-header"
        >
          <Typography variant="h6">{t('tags')}</Typography>
        </ExpansionPanelSummaryFilter>
        <ExpansionPanelDetailFilter>
          <Box width="100%">
            <StyledMultiList
              componentId="Tags"
              dataField="tags.name.keyword"
              filterLabel="Tags"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: ['Opportunity', 'Public flag', 'Search', 'Date'],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetailFilter>
      </ExpansionPanelFilter>
    </>
  );
};
