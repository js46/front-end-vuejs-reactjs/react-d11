import React from 'react';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useSelector, shallowEqual } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { datePickerInputProps } from '../../../../utils';
import {
  StyledDateRange,
  StyledMultiList,
  StyledSingleList,
  StyledRangSlider,
} from '../../../../components/styled';
import { upperCase, isEqual } from 'lodash';
import { UserProfileState } from '../../../../store/user/selector';
interface OppSidebarProps {
  defaultQuery?: () => object;
  setTaskState: (value: string[]) => void;
  taskStates: string[];
}
const TaskStateMap = new Map([
  ['ASSIGNED', 'Assigned'],
  ['INPROGRESS', 'In Progress'],
  ['COMPLETED', 'Completed'],
  ['CANCELLED', 'Cancelled'],
  ['TIMEOUT', 'Timeout'],
]);

export const OppSidebar: React.FC<OppSidebarProps> = ({
  defaultQuery,
  taskStates,
  setTaskState,
}) => {
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState, shallowEqual);
  const { i18n } = useTranslation();

  return (
    <>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('date_submitted')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledDateRange
              componentId="Date"
              innerClass={{
                'input-container': 'input-date',
              }}
              placeholder={{
                start: 'From',
                end: 'To',
              }}
              queryFormat="date"
              dataField="capture_submit_date"
              dayPickerInputProps={datePickerInputProps}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">State List</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="State list"
              dataField="opportunity_state.display_name.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Date',
                  'Phase list',
                  'Priority score',
                  'Priority list',
                  'Search',
                  'Role list',
                  'Privacy flag',
                  'Tags',
                  'Task State',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('phase_list')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Phase list"
              dataField="opportunity_phase.display_name.keyword"
              showCheckbox={true}
              showSearch={false}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Date',
                  'State list',
                  'Priority score',
                  'Priority list',
                  'Search',
                  'Role list',
                  'Privacy flag',
                  'Tags',
                  'Task State',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={
            <ExpandMoreIcon style={{ alignItems: 'center!important' }} />
          }
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('priority_score')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetailSlider}>
          <Box width="400px">
            <StyledRangSlider
              dataField="opp_priority_score.priority_score"
              componentId="Priority score"
              rangeLabels={{
                start: '0',
                end: '100',
              }}
              range={{
                start: 0,
                end: 100,
              }}
              showHistogram={true}
              tooltipTrigger="hover"
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('priority_level')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Priority list"
              filterLabel="Priority level"
              dataField="opp_priority_score.priority_level.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Date',
                  'State list',
                  'Phase list',
                  'Priority score',
                  'Search',
                  'Role list',
                  'Privacy flag',
                  'Tags',
                  'Task State',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('role')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Role list"
              filterLabel="Role"
              dataField="opportunity_roles.profile_role.name.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Date',
                  'State list',
                  'Phase list',
                  'Priority score',
                  'Priority list',
                  'Search',
                  'Privacy flag',
                  'Tags',
                  'Task State',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      {(userProfileState.isAdmin || userProfileState.internal) && (
        <ExpansionPanel classes={{ root: classes.panel }}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            classes={{
              root: classes.filterSummary,
              content: classes.filterSummaryContent,
              expandIcon: classes.expandIcon,
            }}
          >
            <Typography variant="h6">{i18n.t('privacy')}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.filterDetail}>
            <Box width="100%">
              <StyledSingleList
                componentId="Privacy flag"
                filterLabel="Privacy"
                showSearch={false}
                dataField="private_flag"
                showFilter
                defaultQuery={defaultQuery}
                customQuery={function(value?: string) {
                  if (!value) return undefined;
                  return {
                    query: {
                      bool: {
                        must: {
                          term: {
                            private_flag: value === 'Private',
                          },
                        },
                      },
                    },
                  };
                }}
                transformData={(data: any[]) => {
                  if (data) {
                    return data.map(item => ({
                      key: item.key === 1 ? 'Private' : 'Public',
                      doc_count: item.doc_count,
                    }));
                  }
                  return data;
                }}
                react={{
                  and: [
                    'Date',
                    'State list',
                    'Phase list',
                    'Priority score',
                    'Priority list',
                    'Search',
                    'Role list',
                    'Tags',
                    'Task State',
                  ],
                }}
              />
            </Box>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      )}
      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('task_state')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Task State"
              filterLabel="task"
              dataField="tasks.task_state.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              customQuery={function(values: string[]) {
                if (!values) values = [];
                if (!isEqual(values, taskStates)) {
                  setTaskState(values ?? []);
                }
                if (!values?.length) return undefined;
                return {
                  query: {
                    bool: {
                      must: [
                        {
                          terms: {
                            'tasks.task_state.keyword': values?.map(item =>
                              upperCase(item),
                            ),
                          },
                        },
                      ],
                    },
                  },
                };
              }}
              transformData={(data: any[]) => {
                if (data) {
                  return data.map(item => ({
                    key: TaskStateMap.get(item.key) ?? item.key,
                    doc_count: item.doc_count,
                  }));
                }
                return data;
              }}
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Date',
                  'State list',
                  'Phase list',
                  'Priority score',
                  'Priority list',
                  'Search',
                  'Privacy flag',
                  'Tags',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>

      <ExpansionPanel classes={{ root: classes.panel }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.filterSummary,
            content: classes.filterSummaryContent,
            expandIcon: classes.expandIcon,
          }}
        >
          <Typography variant="h6">{i18n.t('tags')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.filterDetail}>
          <Box width="100%">
            <StyledMultiList
              componentId="Tags"
              filterLabel="Tags"
              dataField="tags.name.keyword"
              showCheckbox={true}
              showSearch={false}
              queryFormat="or"
              innerClass={{
                label: 'multiList',
              }}
              react={{
                and: [
                  'Date',
                  'State list',
                  'Phase list',
                  'Priority score',
                  'Priority list',
                  'Search',
                  'Privacy flag',
                  'Task State',
                ],
              }}
              defaultQuery={defaultQuery}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </>
  );
};
const useStyles = makeStyles(theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
  },
  panel: {
    margin: '0px!important',
    boxShadow: 'none',
    '&::before': {
      background: 'none!important',
    },
  },
  icon: {
    textAlign: 'center',
  },
  expandIcon: {
    margin: '0px!important',
  },
  filterText: {
    paddingBottom: 10,
  },
  filterSummary: {
    padding: '0px 5px 0px 0px',
    margin: '0px',
  },
  filterSummaryContent: {
    margin: 0,
    padding: 0,
    // '&$expanded': {
    //   margin: 'auto',
    //   padding: '0px',
    // },
  },
  filterDetail: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
  },
  lineDivider: {
    border: 1,
    borderColor: '#f4f4f4',
    borderStyle: 'solid',
  },
  filterDetailSlider: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
  /* grid: {
    flexBasis: '0px!important',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  heading: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
  },
  filterText: {
    paddingBottom: 10,
  },
  filterSummary: {
    padding: '0px 5px 0px 0px',
    margin: '0px',
  },
  filterSummaryContent: {
    margin: 0,
    padding: 0,
    // '&$expanded': {
    //   margin: 'auto',
    //   padding: '0px',
    // },
  },
  filterDetail: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
  },
  lineDivider: {
    border: 1,
    borderColor: '#f4f4f4',
    borderStyle: 'solid',
  },
  panel: {
    margin: '0px!important',
    boxShadow: 'none',
    '&::before': {
      background: 'none!important',
    },
  },
  icon: {
    textAlign: 'center',
  },
  expandIcon: {
    margin: '0px!important',
  },
  dataSearch: {
    fontSize: '14px!important',
  },
  searchBtn: {
    height: '42px',
    background: '#2C54E3',
    textAlign: 'center',
    borderRadius: '6px',
    border: '1px solid #2C54E3',
  },
  textBtn: {
    width: '68.54px',
    height: '23px',
    fontSize: '14px',
    lineHeight: '20px',
    textAlign: 'center',
  },
  checkBoxInput: {
    borderRadius: '10px!important',
  },
  inputDate: {
    display: 'block !important',
    '& > div': {
      border: 'none !important',
      '& input': {
        borderRadius: '10px !important',
        border: '1px solid #ccc !important',
        marginBottom: '10px',
      },
      '&:nth-child(2)': {
        display: 'none',
      },
    },
    border: 'none !important',
  },
  sortByDropdownList: {
    marginBottom: '20px',
    height: '42px',
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
  sortByDropdownListContent: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
  filterDetailSlider: {
    margin: '0px',
    padding: '0px 30px 0px 0px',
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
  sortByLabel: {
    paddingRight: '30px',
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  gridRight: {
    minWidth: 217,
  },
  gridSort: {
    minWidth: 229,
  },
  gridFilter: {
    minWidth: 121,
  },
  rootTableContainer: {
    marginTop: 15,
  }, */
}));
