import React from 'react';
import {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { Link } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import {
  TableHeadSorter,
  HeadCellProps,
  ESLoader,
  ESNoResult,
} from '../../../../components';
import { UserProfileState } from '../../../../store/user/selector';
import moment from 'moment';
import { IOpportunity } from '../../../../services/opportunity.services';
import { useSelector } from 'react-redux';
import { useTableHeadSorter } from '../../../../hooks';
import { StyledReactiveList } from '../../../../components/styled';

const tableFields = [
  'id',
  'title.keyword',
  'created_by.name.keyword',
  'opp_priority_score.priority_level.keyword',
  'opp_priority_score.priority_score',
  'created_at',
  'completion_date',
  'opportunity_type.name.keyword',
  'tasks.raw',
];
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: 'Opp#',
    disablePadding: true,
  },
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Created by',
    disablePadding: true,
  },
  {
    id: 'opp_priority_score.priority_level.keyword',
    label: 'Priority',
    disablePadding: true,
  },

  {
    id: 'created_at',
    label: 'Created date',
    disablePadding: true,
  },
  {
    id: 'completion_date',
    label: 'Completion date',
    disablePadding: true,
  },
  {
    id: 'opportunity_type.name.keyword',
    label: 'Opportunity type',
    disablePadding: true,
  },
  {
    id: 'tasks.raw',
    label: 'Task count',
    disablePadding: true,
  },
];
interface OppTableProps {
  defaultQuery?: () => object;
  setCount: (value: number) => void;
  taskStates: string[];
}
export const OppTable: React.FC<OppTableProps> = ({
  defaultQuery,
  setCount,
  taskStates,
}) => {
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'title.keyword',
  );
  const calculateCountTask = (values?: any) => {
    if (!taskStates.length) return values?.length ?? 0;
    return (
      values?.filter((item: any) => {
        return taskStates.indexOf(item?.task_state) !== -1;
      })?.length ?? 0
    );
  };
  return (
    <StyledReactiveList
      componentId="SearchResult"
      react={{
        and: [
          'Date',
          'State list',
          'Phase list',
          'Priority score',
          'Priority list',
          'Search',
          'Role list',
          'Privacy flag',
          'Tags',
          'Task State',
        ],
      }}
      dataField={orderBy}
      sortBy={order}
      size={10}
      paginationAt="bottom"
      pages={5}
      pagination={true}
      showLoader
      loader={<ESLoader />}
      renderNoResults={() => <ESNoResult msgKey="no_data" />}
      onData={({ rawData }) => {
        setCount(rawData?.hits?.total?.value as number);
      }}
      renderResultStats={() => null}
      render={({ data, loading }) => {
        if (loading) return null;
        if (!data?.length) return <></>;
        return (
          <TableContainer classes={{ root: classes.rootTableContainer }}>
            <Table classes={{ root: classes.rootTable }}>
              <TableHeadSorter
                onRequestSort={handleRequestSort}
                order={order}
                orderBy={orderBy}
                cells={headCells}
              />
              <TableBody>
                {data?.map((item: IOpportunity, index: number) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {item.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={`/opportunity/${item.id}`}
                      >
                        {item.title}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={
                          item.created_by?.id === userProfileState.id
                            ? `/profile`
                            : `/profile/${item.created_by?.id}`
                        }
                      >
                        {item.created_by?.name}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.opp_priority_score?.priority_level}
                    </TableCell>

                    <TableCell component="th" scope="row">
                      {item.created_at !== '0001-01-01T00:00:00Z' &&
                        moment(item.created_at).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.completion_date !== '0001-01-01T00:00:00Z'
                        ? moment(item.completion_date).format('DD/MM/YYYY')
                        : ''}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.opportunity_type?.name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {calculateCountTask(item.tasks)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        );
      }}
      innerClass={{
        button: classes.buttonPagination,
        resultsInfo: classes.resultsInfo,
        sortOptions: classes.sortSelect,
      }}
      defaultQuery={defaultQuery}
    />
  );
};

const useStyles = makeStyles(theme => ({
  sortByLabel: {
    paddingRight: '30px',
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  gridRight: {
    minWidth: 217,
  },
  gridSort: {
    minWidth: 229,
  },
  gridFilter: {
    minWidth: 121,
  },
  rootTableContainer: {
    marginTop: 15,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));
