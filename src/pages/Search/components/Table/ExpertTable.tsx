import React from 'react';
import {
  TableHeadSorter,
  HeadCellProps,
  ESLoader,
  ESNoResult,
} from '../../../../components';
import {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import BeatLoader from 'react-spinners/BeatLoader';
import { ProfileLink } from '../../../Expert/ProfileLink';
import { StyledReactiveList } from '../../../../components/styled';
import { useTableHeadSorter } from '../../../../hooks';

interface ExpertTableProps {
  defaultQuery?: () => object;
  opportunityCount?: Map<number, number>;
  setCount: (value: number) => void;
}
const tableFields = [
  'id',
  'name.keyword',
  'profile_role.profile_role_name.keyword',
  'location.name.keyword',
  'resource_team.resource_team_name.keyword',
  'created_at',
  'current_opportunity',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: '#',
    disablePadding: true,
  },
  {
    id: 'name.keyword',
    label: 'Name',
    disablePadding: true,
  },
  {
    id: 'profile_role.profile_role_name.keyword',
    label: 'Role',
    disablePadding: true,
  },
  {
    id: 'location.name.keyword',
    label: 'Location',
    disablePadding: true,
  },
  {
    id: 'resource_team.resource_team_name.keyword',
    label: 'Resource team',
    disablePadding: true,
  },
  {
    id: 'current_opportunity',
    label: 'Opportunity count #',
    disablePadding: true,
    disableSorting: true,
  },
];
export const ExpertTable: React.FC<ExpertTableProps> = ({
  defaultQuery,
  opportunityCount,
  setCount,
}) => {
  const classes = useStyles();
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'name.keyword',
  );
  return (
    <StyledReactiveList
      componentId="SearchResult"
      react={{
        and: [
          'Search',
          'Role list',
          'Skill list',
          'Location list',
          'Resource team list',
          'Tags',
        ],
      }}
      dataField={orderBy}
      sortBy={order}
      size={10}
      paginationAt="bottom"
      pages={5}
      pagination={true}
      showLoader
      loader={<ESLoader />}
      renderNoResults={() => <ESNoResult msgKey="no_data" />}
      onData={({ rawData }) => {
        if (rawData) {
          setCount(rawData?.hits?.total?.value as number);
        }
      }}
      renderResultStats={() => null}
      render={({ data, loading }) => {
        if (loading) return null;
        if (!data?.length) return <></>;
        return (
          <TableContainer classes={{ root: classes.rootTableContainer }}>
            <Table classes={{ root: classes.rootTable }}>
              <TableHeadSorter
                onRequestSort={handleRequestSort}
                order={order}
                orderBy={orderBy}
                cells={headCells}
              />

              <TableBody>
                {data.map((item: any, index: number) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {item.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <ProfileLink id={item.id} title={item.name} />
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.profile_role?.profile_role_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item.location?.name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {item?.resource_team?.resource_team_name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {opportunityCount ? (
                        <ProfileLink
                          id={item.id}
                          title={opportunityCount.get(item.id) ?? 0}
                        />
                      ) : (
                        <BeatLoader size={5} color={'grey'} loading />
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        );
      }}
      innerClass={{
        button: classes.buttonPagination,
        resultsInfo: classes.resultsInfo,
        sortOptions: classes.sortSelect,
      }}
      defaultQuery={defaultQuery}
    />
  );
};

const useStyles = makeStyles(theme => ({
  sortByLabel: {
    paddingRight: '30px',
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  gridRight: {
    minWidth: 217,
  },
  gridSort: {
    minWidth: 229,
  },
  gridFilter: {
    minWidth: 121,
  },
  rootTableContainer: {
    marginTop: 15,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));
