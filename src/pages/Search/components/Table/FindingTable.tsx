import React from 'react';

import {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Link,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import {
  HeadCellProps,
  ESLoader,
  ESNoResult,
  TableHeadSorter,
} from '../../../../components';
import { UserProfileState } from '../../../../store/user/selector';
import { useTableHeadSorter } from '../../../../hooks';
import { StyledReactiveList } from '../../../../components/styled';
import { useSelector } from 'react-redux';
import moment from 'moment';

interface FindingTableProps {
  defaultQuery?: () => object;
  setCount: (value: number) => void;
}
const tableFields = [
  'id',
  'title.keyword',
  'opportunity.title.keyword',
  'opportunity_playbook_book.name.keyword',
  'opportunity_playbook_item.title.keyword',
  'created_by.name.keyword',
  'last_updated_at',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: '#',
    disablePadding: true,
  },
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'opportunity.title.keyword',
    label: 'Opportunity title',
    disablePadding: true,
  },

  {
    id: 'opportunity_playbook_book.name.keyword',
    label: 'Playbook name',
    disablePadding: true,
  },
  {
    id: 'opportunity_playbook_item.title.keyword',
    label: 'Step title',
    disablePadding: true,
  },
  {
    id: 'created_by.name.keyword',
    label: 'Created by',
    disablePadding: true,
  },

  {
    id: 'last_updated_at',
    label: 'Last updated',
    disablePadding: true,
  },
];

export const FindingTable: React.FC<FindingTableProps> = ({
  defaultQuery,
  setCount,
}) => {
  const classes = useStyles();
  const userProfileState = useSelector(UserProfileState);
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    'title.keyword',
  );
  return (
    <StyledReactiveList
      componentId="SearchResult"
      react={{
        and: ['Opportunity', 'Public flag', 'Search', 'Date', 'Tags'],
      }}
      dataField={orderBy}
      sortBy={order}
      size={10}
      paginationAt="bottom"
      pages={5}
      pagination={true}
      showLoader
      loader={<ESLoader />}
      onData={({ rawData }) => {
        if (rawData) {
          setCount(rawData?.hits?.total?.value as number);
        }
      }}
      renderResultStats={() => null}
      renderNoResults={() => <ESNoResult msgKey="no_data" />}
      render={({ data, loading }) => {
        if (loading) return null;
        if (!data?.length) return <></>;
        return (
          <TableContainer classes={{ root: classes.rootTableContainer }}>
            <Table classes={{ root: classes.rootTable }}>
              <TableHeadSorter
                onRequestSort={handleRequestSort}
                order={order}
                orderBy={orderBy}
                cells={headCells}
              />
              <TableBody>
                {data.map((item: any, index: number) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {item.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={`/opportunity/${item?.opportunity?.id}/playbook-${item?.opportunity_playbook?.id}?step=${item?.opportunity_playbook_item?.id}`}
                      >
                        {item.title}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={`/opportunity/${item?.opportunity?.id}`}
                      >
                        {item?.opportunity?.title}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={`/opportunity/${item?.opportunity?.id}/playbook-${item?.opportunity_playbook?.id}`}
                      >
                        {item?.opportunity_playbook?.name}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={`/opportunity/${item?.opportunity?.id}/playbook-${item?.opportunity_playbook?.id}?step=${item?.opportunity_playbook_item?.id}`}
                      >
                        {item?.opportunity_playbook_item?.title}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link
                        component={RouterLink}
                        color="inherit"
                        to={
                          item.created_by?.id === userProfileState.id
                            ? `/profile`
                            : `/profile/${item.created_by?.id}`
                        }
                      >
                        {item.created_by?.name}
                      </Link>
                    </TableCell>

                    <TableCell component="th" scope="row">
                      {item.last_updated_at !== '0001-01-01T00:00:00Z'
                        ? moment(item.last_updated_at).format('DD/MM/YYYY')
                        : ''}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        );
      }}
      innerClass={{
        button: classes.buttonPagination,
        resultsInfo: classes.resultsInfo,
        sortOptions: classes.sortSelect,
      }}
      defaultQuery={defaultQuery}
    />
  );
};

const useStyles = makeStyles(theme => ({
  sortByLabel: {
    paddingRight: '30px',
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  gridRight: {
    minWidth: 217,
  },
  gridSort: {
    minWidth: 229,
  },
  gridFilter: {
    minWidth: 121,
  },
  rootTableContainer: {
    marginTop: 15,
  },
  buttonPagination: {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
    '&:active': {
      backgroundColor: '#0054d7!important',
      color: '#fff!important',
    },
  },
  sortSelect: {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial!important',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },
  resultsInfo: {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '-10px',
        marginBottom: '20px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
}));
