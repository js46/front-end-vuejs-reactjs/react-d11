import React, { useCallback, useEffect, useState } from 'react';
import {
  Box,
  Button,
  Grid,
  IconButton,
  InputAdornment,
  OutlinedInput,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@material-ui/core';
import {
  deleteUser,
  getRoles,
  getUsers,
  IUser,
} from '../../services/user.services';
import { getComparator, stableSort } from '../../commons/util';
import { UserFormDialog } from './components';
import { makeStyles } from '@material-ui/core/styles';
import { getRoleLabels } from '../../utils';
import { PersonAdd } from '@material-ui/icons';
import { Pagination } from '@material-ui/lab';
import SearchIcon from '@material-ui/icons/Search';
import {
  CustomSelect,
  DeleteDialog,
  DeleteIconButton,
  EditIconButton,
  ESLoader,
  HeadCellProps,
  StatusCheckbox,
  TableHeadSorter,
} from '../../components';
import { useTranslation } from 'react-i18next';
import {
  getBusinessUnits,
  getResourceTeam,
  IBusinessUnit,
  IResourceTeam,
} from '../../services/references.services';
import { useEffectOnlyOnce, useTableHeadSorter } from '../../hooks';
import { useSelector } from 'react-redux';
import { IsSuperAdmin } from '../../store/user/selector';

type FilterProps = {
  businessUnit: number | 'all';
  role: string | 'all';
};
const PAGE_SIZE = 10;
const tableFields = [
  'id',
  'full_name',
  'email',
  'auth_source',
  'resource_team.name',
  'business_unit.name',
  'status',
  'role',
  'action',
] as const;
const headCells: HeadCellProps<typeof tableFields[number]>[] = [
  {
    id: 'id',
    label: 'ID',
    disablePadding: true,
  },
  {
    id: 'full_name',
    label: 'Full name',
    disablePadding: true,
  },
  {
    id: 'email',
    label: 'Email',
    disablePadding: true,
  },
  {
    id: 'auth_source',
    label: 'Auth source',
    disablePadding: true,
  },
  {
    id: 'resource_team.name',
    label: 'Resource team',
    disableSorting: false,
    disablePadding: true,
  },
  {
    id: 'business_unit.name',
    label: 'Business unit',
    disableSorting: false,
    disablePadding: true,
  },
  {
    id: 'status',
    label: 'Active',
    disablePadding: true,
    disableSorting: true,
    center: true,
    width: 60,
  },
  {
    id: 'role',
    label: 'Roles',
    disablePadding: true,
    disableSorting: true,
  },
  {
    id: 'action',
    label: 'Action',
    disablePadding: true,
    disableSorting: true,
    center: true,
    width: 95,
  },
];

export const User: React.FC = () => {
  const defaultFilter: FilterProps = {
    businessUnit: 'all',
    role: 'all',
  };
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFields,
    '',
  );
  const isSuperAdmin = useSelector(IsSuperAdmin);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [raw, setRaw] = useState<IUser[]>([]);
  const [data, setData] = useState<IUser[]>([]);
  const [roleList, setRoleList] = useState<string[]>([]);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState<FilterProps>(defaultFilter);
  const [resourceTeams, setResourceTeams] = useState<IResourceTeam[]>([]);
  const [businessUnits, setBusinessUnits] = useState<IBusinessUnit[]>([]);

  const [deleteUserID, setDeleteUserID] = useState<number>();
  const [editUser, setEditUser] = useState<IUser>();
  const [formDialogOpened, setFormDialogOpened] = useState(false);

  useEffectOnlyOnce(() => {
    Promise.all([
      fetchRoles(),
      fetchResourceTeams(),
      fetchBusinessUnits(),
      fetchRaw(),
    ]).then(() => {
      // console.log('Mounted', raw);
      setLoading(false);
    });
  });

  useEffect(() => {
    handleRawData(keyword);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, filters, raw, order, orderBy]);

  const handleClose = () => {
    setFormDialogOpened(false);
  };

  const handleOpenFormDialog = (userEdit?: IUser) => {
    setEditUser(userEdit);
    setFormDialogOpened(true);
  };

  const fetchRaw = useCallback(async () => {
    const json = await getUsers(1, 1000);
    if (json.success) {
      await setRaw(json.data.list);
      await setTotal(json.data.total);
    }
  }, []);

  const handleRawData = useCallback(
    (keyword = '') => {
      keyword = keyword.trim();
      let list = raw;
      if (filters.role !== 'all') {
        list = list.filter(item => item.roles.includes(filters.role));
      }
      if (keyword !== '') {
        list = list.filter(
          item =>
            item.full_name.includes(keyword) || item.email.includes(keyword),
        );
      }
      if (orderBy !== '') {
        list = stableSort(
          list as any[],
          getComparator(order, orderBy),
        ) as any[];
      }
      setTotal(list.length);
      if (list.length > PAGE_SIZE) {
        list = list.slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE);
      }
      setData(list);
    },
    [filters.role, order, orderBy, page, raw],
  );

  const fetchRoles = useCallback(async () => {
    const json = await getRoles();
    const roles = [
      'role:admin',
      'role:teamlead',
      'role:expert',
      'role:proposer',
    ];
    if (isSuperAdmin) roles.push('role:super-admin');
    if (json.success) {
      const data = json.data.filter(item => roles.includes(item));
      setRoleList(['role:user', ...data]);
    }
  }, [isSuperAdmin]);

  const fetchResourceTeams = useCallback(async () => {
    const json = await getResourceTeam();
    if (json.success) setResourceTeams(json.data.list);
  }, []);

  const fetchBusinessUnits = useCallback(async () => {
    const json = await getBusinessUnits();
    if (json.success)
      setBusinessUnits(json.data.list.filter(item => !item.archive));
  }, []);

  const onRefreshData = useCallback(() => {
    setKeyword('');
    setFilters(defaultFilter);
    fetchRaw();
    //setPage(1);
  }, [defaultFilter, fetchRaw]);

  const onDeleteUser = useCallback(
    async (id: number) => {
      if (!isSuperAdmin) return;
      try {
        await deleteUser(id);
        setDeleteUserID(undefined);
        await onRefreshData();
      } catch (e) {
        console.log(e);
        alert('Error occurs on deleting the user!');
      }
    },
    [isSuperAdmin, onRefreshData],
  );

  const onChangePage = useCallback((e: object, newPage: number) => {
    setPage(newPage);
  }, []);

  function onFiltersChange(
    event: React.ChangeEvent<{ name?: string; value: any }>,
  ) {
    const { value, name } = event.target;
    if (!name) return;

    setFilters(prevState => ({
      ...prevState,
      [name]: value,
    }));
  }

  const onSearch = async () => {
    setPage(1);
    handleRawData(keyword);
  };

  const classes = useStyles();
  const { t } = useTranslation();
  const numberOfPages = Math.ceil(total / PAGE_SIZE);

  return (
    <>
      <Grid>
        <Grid item xs={12}>
          <Box mt={2} mb={2}>
            <Typography variant="h4">{t('users')}</Typography>
          </Box>
        </Grid>
        <Grid item container>
          <Grid item xs={12} sm={10} direction="row" container>
            <Box pr={2} display="flex" alignItems="center">
              <Typography variant="h6">{t('search')}</Typography>
            </Box>
            <OutlinedInput
              name="keyword"
              placeholder="Search a keyword..."
              value={keyword}
              onChange={event => setKeyword(event.target.value)}
              onKeyDown={event => (event.keyCode === 13 ? onSearch() : null)}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton type="submit" onClick={onSearch}>
                    <SearchIcon />
                  </IconButton>
                </InputAdornment>
              }
            />
            <Box display="flex" alignItems="center" pr={2} ml={3}>
              <Typography variant="h6">{t('role')}</Typography>
            </Box>
            <Box width={'20%'}>
              <CustomSelect
                options={roleList
                  .map(item => ({
                    id: item,
                    name: getRoleLabels(item)[0],
                  }))
                  .filter(item => item.name !== 'User')}
                fullWidth
                enabledAllOption
                value={filters.role}
                name="role"
                onChange={onFiltersChange}
              />
            </Box>
          </Grid>
          <Grid item xs={12} sm container justify="flex-end">
            <Button
              startIcon={<PersonAdd />}
              variant="contained"
              color="primary"
              onClick={() => handleOpenFormDialog()}
            >
              Add
            </Button>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          {loading ? (
            <ESLoader />
          ) : (
            <TableContainer className={classes.rootTableContainer}>
              <Table className={classes.rootTable}>
                <TableHeadSorter
                  onRequestSort={handleRequestSort}
                  order={order}
                  orderBy={orderBy}
                  cells={headCells}
                />
                <TableBody>
                  {data.length > 0 ? (
                    <>
                      {data.map(user => (
                        <TableRow key={user.id}>
                          <TableCell style={{ width: 20 }}>{user.id}</TableCell>
                          <TableCell>{user.full_name}</TableCell>
                          <TableCell>{user.email}</TableCell>
                          <TableCell>{user.auth_source}</TableCell>
                          <TableCell>{user.resource_team?.name}</TableCell>
                          <TableCell>{user.business_unit?.name}</TableCell>
                          <TableCell align="center">
                            <StatusCheckbox checked={user.active} />
                          </TableCell>
                          <TableCell>
                            {getRoleLabels(user.roles).join(', ')}
                          </TableCell>
                          <TableCell align="center">
                            <EditIconButton
                              onClick={() => handleOpenFormDialog(user)}
                            />
                            {isSuperAdmin && (
                              <DeleteIconButton
                                onClick={() => setDeleteUserID(user.id)}
                              />
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                    </>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={9}>
                        <Typography align="center" component="p">
                          {t('no_data')}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
          )}
          {numberOfPages > 1 && (
            <Box display="flex" justifyContent="center" p={3}>
              <Pagination
                page={page}
                count={numberOfPages}
                variant="outlined"
                shape="rounded"
                onChange={onChangePage}
              />
            </Box>
          )}
        </Grid>
      </Grid>
      {formDialogOpened && (
        <UserFormDialog
          open={formDialogOpened}
          closeDialog={handleClose}
          rolesData={roleList}
          refreshData={onRefreshData}
          user={editUser}
          resourceTeams={resourceTeams}
          businessUnits={businessUnits}
        />
      )}
      {isSuperAdmin && deleteUserID && (
        <DeleteDialog
          isOpen={Boolean(deleteUserID)}
          header={'Delete user'}
          message={t('delete_user_alert')}
          handleClose={() => setDeleteUserID(undefined)}
          handleDelete={() => onDeleteUser(deleteUserID)}
        />
      )}
    </>
  );
};
const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
}));

export default User;
