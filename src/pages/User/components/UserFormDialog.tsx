import React from 'react';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  TextField,
  FormControl,
  Select,
  Chip,
  MenuItem,
  Checkbox,
  ListItemText,
  FormHelperText,
  Box,
  Typography,
} from '@material-ui/core';
import * as Yup from 'yup';

import { addUser, IUser, updateUser } from '../../../services/user.services';
import {
  IBusinessUnit,
  IResourceTeam,
} from '../../../services/references.services';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { MixTitle, FormDialog } from '../../../components';
import { makeStyles } from '@material-ui/core/styles';
import { useDisableMultipleClick } from '../../../hooks';
import { getRoleLabels } from '../../../utils';
import { notifyError, notifySuccess } from '../../../store/common/actions';

export const UserFormDialog: React.FC<{
  open: boolean;
  closeDialog: () => void;
  rolesData: string[];
  refreshData: () => void;
  user?: IUser;
  businessUnits: IBusinessUnit[];
  resourceTeams: IResourceTeam[];
}> = ({
  open,
  closeDialog,
  rolesData,
  refreshData,
  user,
  businessUnits,
  resourceTeams,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const initialValues = {
    new_email: user?.email ?? '',
    full_name: user?.full_name ?? '',
    team_lead_flg: user?.team_lead_flg,
    business_unit_id: user?.business_unit?.id || 0,
    resource_team_id: user?.resource_team?.id || 0,
    new_password: '',
    roles: user?.roles ?? ['role:user'],
    active: user?.active ?? true,
  };
  const { i18n } = useTranslation();
  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object({
      new_email: Yup.string()
        .email(i18n.t('validate.field_email_invalid'))
        .required(i18n.t('validate.field_required')),
      full_name: Yup.string().required(i18n.t('validate.field_required')),
      business_unit_id: Yup.number().notOneOf(
        [0],
        i18n.t('validate.field_required'),
      ),

      resource_team_id: Yup.number().notOneOf(
        [0],
        i18n.t('validate.field_required'),
      ),
      new_password: user
        ? Yup.string()
        : Yup.string().required(i18n.t('validate.field_required')),
      roles: Yup.array().min(1, i18n.t('validate.field_required')),
    }),
    onSubmit: async values => {
      await debounceFn();
      const isTeamLead = values.roles.includes('role:teamlead');
      const userForm = {
        email: values.new_email,
        password: values.new_password,
        full_name: values.full_name,
        business_unit_id: values.business_unit_id,
        resource_team_id: values.resource_team_id,
        roles: values.roles,
        team_lead_flg: isTeamLead,
        active: values.active,
      };

      if (user) {
        await updateUser({
          id: user.id,
          casbin_user: user.casbin_user,
          ...userForm,
        });
      } else {
        let response = await addUser(userForm);
        if (response.success) {
          dispatch(
            notifySuccess({
              message: t('create_user_success'),
            }),
          );
        } else {
          dispatch(
            notifyError({
              message: t('email_existed'),
            }),
          );
        }
      }
      endRequest();
      refreshData();
      closeDialog();
    },
  });

  const classes = useStyles();
  const changeBusinessUnit = (
    event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>,
  ) => {
    //find business unit
    const buFound = businessUnits.find(
      item => item.id === Number(event.target.value),
    );
    if (buFound) {
      formik.setFieldValue('business_unit_id', Number(event.target.value));
      formik.setFieldValue('resource_team_id', buFound.resource_team?.id);
    }
  };
  return (
    <FormDialog
      title={user ? 'Update User' : 'Add User'}
      submit={formik.handleSubmit}
      open={open}
      setOpen={closeDialog}
      isAddingForm={!user}
      formContent={
        <Box>
          <Box mb={2}>
            <MixTitle title="Full Name" isRequired />
            <TextField
              margin="normal"
              required
              variant="outlined"
              fullWidth
              id="full_name"
              name="full_name"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.full_name}
              error={formik.touched.full_name && !!formik.errors.full_name}
              helperText={
                formik.touched.full_name ? formik.errors.full_name : ''
              }
            />
          </Box>
          <Box mb={2}>
            <MixTitle title="Email Address" isRequired />
            <TextField
              margin="normal"
              required
              fullWidth
              variant="outlined"
              id="new_email"
              name="new_email"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.new_email}
              error={formik.touched.new_email && !!formik.errors.new_email}
              helperText={
                formik.touched.new_email ? formik.errors.new_email : ''
              }
              disabled={!!user}
            />
          </Box>

          {!user && (
            <Box mb={2}>
              <MixTitle title="Password" isRequired />
              <TextField
                margin="normal"
                required
                variant="outlined"
                fullWidth
                id="new_password"
                name="new_password"
                type="password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.new_password}
                error={
                  formik.touched.new_password && !!formik.errors.new_password
                }
                helperText={
                  formik.touched.new_password ? formik.errors.new_password : ''
                }
              />
            </Box>
          )}
          <Box mb={2}>
            <MixTitle title="Business Unit" isRequired />
            <FormControl fullWidth margin="normal" variant="outlined">
              <Select
                value={formik.values.business_unit_id}
                onChange={changeBusinessUnit}
                onBlur={formik.handleBlur}
                id="business_unit_id"
                name="business_unit_id"
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem key={0} value={0} disabled>
                  {i18n.t('validate.label_help_select_bu')}
                </MenuItem>
                {businessUnits.map(item => {
                  return (
                    <MenuItem key={item.id} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
              <FormHelperText>
                {formik.touched.business_unit_id
                  ? formik.errors.business_unit_id
                  : ''}
              </FormHelperText>
            </FormControl>
          </Box>
          <Box mb={2}>
            <MixTitle title="Resource Team" isRequired />
            <FormControl fullWidth margin="normal" variant="outlined">
              <Select
                labelId="resource-team-label"
                value={formik.values.resource_team_id}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                id="resource_team_id"
                name="resource_team_id"
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
                IconComponent={ExpandMoreIcon}
              >
                <MenuItem key={0} value={0} disabled>
                  {i18n.t('validate.label_help_select_rt')}
                </MenuItem>
                {resourceTeams.map(item => {
                  return (
                    <MenuItem key={item.id} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
              </Select>
              <FormHelperText>
                {formik.touched.resource_team_id
                  ? formik.errors.resource_team_id
                  : ''}
              </FormHelperText>
            </FormControl>
          </Box>
          <Box mb={2}>
            <MixTitle title="Roles" />
            <FormControl
              required
              fullWidth
              error={formik.touched.roles && !!formik.errors.roles}
              margin="normal"
              variant="outlined"
            >
              <Select
                multiple
                value={formik.values.roles}
                onChange={formik.handleChange}
                id="roles"
                name="roles"
                IconComponent={ExpandMoreIcon}
                renderValue={selected =>
                  (selected as string[]).map(item => (
                    <Chip
                      label={getRoleLabels(item)[0]}
                      key={item}
                      className={classes.selectItem}
                      size="small"
                    />
                  ))
                }
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {rolesData.map(r => (
                  <MenuItem key={r} value={r} disabled={r === 'role:user'}>
                    <Checkbox
                      style={{ color: '#2c54e3' }}
                      checked={formik.values.roles.includes(r)}
                    />
                    <ListItemText primary={getRoleLabels(r)[0]} />
                  </MenuItem>
                ))}
              </Select>
              <FormHelperText>
                {formik.touched.roles ? formik.errors.roles : ''}
              </FormHelperText>
            </FormControl>
          </Box>

          {user && (
            <Box mb={2} display="flex" alignItems="center">
              <Typography children="Active" className={classes.fieldTitle} />
              <Checkbox
                value={formik.values.active}
                checked={formik.values.active}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                color="primary"
                name="active"
              />
            </Box>
          )}
        </Box>
      }
      disable={!(formik.isValid && formik.dirty) || isSubmitting}
    ></FormDialog>
  );
};
const useStyles = makeStyles(theme => ({
  selectItem: {
    color: '#666',
    backgroundColor: '#e0e0e0',
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
}));
