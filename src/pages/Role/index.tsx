import React, { useState, useEffect } from 'react';
import {
  Typography,
  Button,
  Card,
  CardContent,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Checkbox,
  TableBody,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  IconButton,
} from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles, lighten } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import clsx from 'clsx';
import { getRoles } from '../../services/user.services';
import MaterialTable from 'material-table';
import { AddCircle } from '@material-ui/icons';
import { tableIcons } from '../../commons';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  card: {
    marginTop: '16px',
  },
  cardContent: {
    padding: 0,
  },
}));
const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));
const policies = [
  ['role:user', '/api/v1/users/dashboard', 'GET'],
  ['role:user', '/api/v1/users/:id', 'POST'],
  ['role:user', '/dashboard', 'GET'],
  ['role:user', '/api/v1/users/permissions', 'GET'],
];

export const Role: React.FC = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);
  const [role, setRole] = useState([{ name: '', description: '' }]);
  const handleClose = () => {
    setOpen(false);
  };
  const changeCheckBox = (event: React.MouseEvent<unknown>, path: string) => {
    const selectedIndex = selected.indexOf(path);
    let newSelected: string[] = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, path);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };
  const onCheckBoxAll = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newSelected: string[] = [];
    if (event.target.checked) {
      policies.forEach(item => {
        newSelected = newSelected.concat(selected, item[1]);
        setSelected(newSelected);
      });
    } else {
      setSelected([]);
    }
  };
  interface EnhancedTableToolbarProps {
    numSelected: number;
  }

  const isSelected = (path: string) => selected.indexOf(path) !== -1;
  const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles();
    const { numSelected } = props;

    return (
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        {numSelected > 0 ? (
          <Typography
            className={classes.title}
            color="inherit"
            variant="subtitle1"
          >
            {numSelected} selected
          </Typography>
        ) : (
          <></>
        )}
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <></>
        )}
      </Toolbar>
    );
  };
  const formik = useFormik({
    initialValues: {
      name: '',
      policy: false,
      role: false,
    },
    onSubmit: values => {},
  });
  useEffect(() => {
    getRole();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const getRole = async () => {
    const dataRole = await getRoles();
    if (dataRole && dataRole.data) {
      setRole(
        dataRole.data.map(item => ({
          name: item,
          description: `Description for ${item}`,
        })),
      );
    }
  };

  return (
    <>
      <Box pl={2}>
        <MaterialTable
          title="Roles"
          columns={[
            { title: 'Name', field: 'name' },
            { title: 'Description', field: 'description' },
          ]}
          data={role}
          icons={tableIcons}
          actions={[
            {
              icon: () => <AddCircle color="primary" />,
              tooltip: 'Add',
              isFreeAction: true,
              onClick: () => {
                setOpen(true);
              },
              position: 'toolbar',
            },
          ]}
        />
        <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth>
          <form noValidate onSubmit={formik.handleSubmit}>
            <DialogTitle id="form-dialog-title">Add a role</DialogTitle>
            <DialogContent>
              <TextField
                margin="normal"
                label="Name"
                fullWidth
                name="name"
                id="name"
                onChange={formik.handleChange}
                value={formik.values.name}
              />
              <TextField
                margin="normal"
                label=""
                fullWidth
                name="name"
                id="name"
                onChange={formik.handleChange}
                value={formik.values.name}
              />
              <Card className={classes.card}>
                <CardContent className={classes.cardContent}>
                  <EnhancedTableToolbar numSelected={selected.length} />
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell padding="checkbox">
                          <Checkbox
                            color="primary"
                            onChange={onCheckBoxAll}
                            checked={
                              policies.length > 0 &&
                              policies.length === selected.length
                            }
                            name="policy"
                            id="policy"
                          />
                        </TableCell>
                        <TableCell>Policy</TableCell>
                        <TableCell>Path</TableCell>
                        <TableCell>Method</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {policies.map((item, index) => {
                        const isItemSelected = isSelected(item[1]);
                        return (
                          <TableRow hover key={index}>
                            <TableCell padding="checkbox" key={index}>
                              <Checkbox
                                color="primary"
                                value={item[1]}
                                onClick={event =>
                                  changeCheckBox(event, item[1])
                                }
                                checked={isItemSelected}
                                name="role"
                                id="role"
                              />
                            </TableCell>

                            <TableCell>{item[0].slice(5)}</TableCell>
                            <TableCell>{item[1]}</TableCell>
                            <TableCell>{item[2]}</TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary" variant="outlined">
                Cancel
              </Button>
              <Button onClick={handleClose} type="submit" color="primary">
                Save
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </Box>
    </>
  );
};

export default Role;
