import React from 'react';

import {
  MainLayout,
  LoginLayout,
  OpportunityLayout,
  ReferenceLayout,
} from '../layouts';
import { Page404, Page401 } from '../pages';

const Login = React.lazy(() => import('../pages/Auth/Login'));
const Register = React.lazy(() => import('../pages/Auth/Register'));
const Home = React.lazy(() => import('../pages/Home'));
const Policy = React.lazy(() => import('../pages/Policy'));
const Role = React.lazy(() => import('../pages/Role'));
const Capture = React.lazy(() => import('../pages/Capture'));
const Shape = React.lazy(() => import('../pages/Backlog'));
const MyStuff = React.lazy(() => import('../pages/MyStuff'));
const Dataset = React.lazy(() => import('../pages/Dataset'));
const Setting = React.lazy(() => import('../pages/Setting'));
const Audit = React.lazy(() => import('../pages/Audit'));
const MyTeam = React.lazy(() => import('../pages/MyTeam'));
const Playbook = React.lazy(() => import('../pages/Playbook'));
const ViewOtherStuff = React.lazy(() =>
  import('../pages/MyStuff/component/ViewOtherStuff'),
);
/* const SearchOpportunity = React.lazy(() =>
  import('../pages/Search/components/SearchOpportunity'),
); */
const Search = React.lazy(() => import('../pages/Search'));
const Opportunity = React.lazy(() => import('../pages/Opportunity'));
const User = React.lazy(() => import('../pages/User'));
const Expert = React.lazy(() => import('../pages/Expert'));
const Dashboard = React.lazy(() => import('../pages/Dashboard'));
const ReferenceData = React.lazy(() => import('../pages/Reference'));

export interface IRoute {
  Component: ((props: any) => JSX.Element) | React.FC<any>;
  Layout: ((props: any) => JSX.Element) | React.FC<any>;
  Protected: boolean;
  path?: string | string[];
  routePath?: string;
  from?: string;
  to?: string;
  exact?: boolean;
  hasSidebar?: boolean;
}

export const routes: IRoute[] = [
  {
    Component: Login,
    Layout: LoginLayout,
    exact: true,
    path: '/login',
    routePath: '/login',
    Protected: false,
  },
  {
    Component: Register,
    Layout: LoginLayout,
    exact: true,
    path: '/register',
    routePath: '/register',
    Protected: false,
  },
  {
    Component: Home,
    Layout: MainLayout,
    exact: true,
    path: '/',
    routePath: '/',
    Protected: true,
  },
  {
    Component: MyTeam,
    Layout: MainLayout,
    exact: true,
    Protected: true,
    path: '/my-team',
    routePath: '/my-team',
  },
  {
    Component: User,
    Layout: MainLayout,
    exact: true,
    path: '/users',
    routePath: '/users',
    Protected: true,
  },
  {
    Component: Policy,
    Layout: MainLayout,
    exact: true,
    path: '/policies',
    routePath: '/policies',
    Protected: true,
  },
  {
    Component: Role,
    Layout: MainLayout,
    exact: true,
    path: '/roles',
    routePath: '/roles',
    Protected: true,
  },
  {
    Component: Search,
    Layout: MainLayout,
    exact: true,
    Protected: true,
    path: '/search',
    routePath: '/search',
  },
  {
    Component: Dashboard,
    Layout: MainLayout,
    exact: true,
    path: '/dashboard',
    routePath: '/dashboard',
    Protected: true,
  },
  {
    Component: Dataset,
    Layout: MainLayout,
    hasSidebar: false,
    exact: true,
    path: '/dataset',
    routePath: '/dataset',
    Protected: true,
  },
  {
    Component: Setting,
    Layout: MainLayout,
    hasSidebar: false,
    exact: true,
    path: '/setting',
    routePath: '/setting',
    Protected: true,
  },
  {
    Component: Opportunity,
    Layout: OpportunityLayout,
    exact: true,
    Protected: true,
    path: ['/opportunity', '/opportunity/:id'],
    routePath: '/opportunity',
  },
  {
    Component: Capture,
    Layout: MainLayout,
    exact: true,
    Protected: true,
    path: '/capture-opportunity',
    routePath: '/capture-opportunity',
  },
  {
    Component: Capture,
    Layout: MainLayout,
    exact: true,
    Protected: true,
    path: '/capture-opportunity/:id',
    routePath: '/capture-opportunity',
  },
  {
    Component: Shape,
    Layout: MainLayout,
    exact: true,
    Protected: true,
    path: ['/opportunity-pipeline', '/opportunity-pipeline/:id'],
    routePath: '/opportunity-pipeline',
  },
  {
    Component: ViewOtherStuff,
    Layout: MainLayout,
    Protected: true,
    hasSidebar: true,
    exact: true,
    path: [
      '/profile/:id',
      '/experts/:id',
      '/profile/:id/opportunity',
      '/experts/:id/opportunity',
    ],
    routePath: '/my-stuff',
  },
  {
    Component: MyStuff,
    Layout: MainLayout,
    Protected: true,
    hasSidebar: true,
    path: ['/profile', '/my-stuff'],
    routePath: '/my-stuff',
  },
  {
    Component: Expert,
    Layout: MainLayout,
    exact: true,
    path: '/experts',
    routePath: '/experts',
    Protected: true,
  },
  {
    Component: ReferenceData,
    Layout: ReferenceLayout,
    hasSidebar: true,
    path: ['/reference-data'],
    routePath: '/reference-data',
    Protected: true,
  },
  {
    Component: Audit,
    Layout: MainLayout,
    exact: true,
    path: '/audit',
    routePath: '/audit',
    Protected: true,
  },
  {
    Component: Playbook,
    Layout: OpportunityLayout,
    Protected: true,
    exact: true,
    path: '/opportunity/:opportunityID/playbook-:playbookID',
    routePath: '/opportunity/playbook',
  },
  {
    Component: Page401,
    Layout: MainLayout,
    path: '/401',
    Protected: false,
  },
  {
    Component: Page404,
    Layout: MainLayout,
    from: '*',
    to: '/404',
    Protected: false,
  },
];
