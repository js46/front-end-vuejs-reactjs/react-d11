export type ES_INDEX_KEYS = {
  opportunity_index_name: string;
  event_log_index_name_prefix: string;
  comment_index_name: string;
  attachment_index_name: string;
  favorite_index_name: string;
  user_profile_index_name: string;
  opportunity_task_index_name: string;
  opportunity_expert_index_name: string;
  user_note_index_name: string;
  opportunity_index_finding: string;
  link_index_name: string;
  list_index_name: string;
  tag_index_name: string;
  issue_index_name: string;
};

export type GOOGLE_CONFIG = {
  active: boolean;
  google_client_id: string;
  google_client_secret: string;
};

export type PUBLIC_CONFIG = {
  default_login: 'account' | 'google' | 'okta';
  google_client_id: string;
  okta_login_url: string;
  okta_client_id: string;
  okta_redirect_uri: string;
};
