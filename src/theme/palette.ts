import * as colors from '@material-ui/core/colors';
import { PaletteOptions } from '@material-ui/core/styles/createPalette';

const white = '#FFFFFF';
const black = '#000000';

const palette: PaletteOptions = {
  common: {
    black,
    white,
  },
  primary: {
    main: '#2C54E3',
    light: '#E1E5F2',
  },
  secondary: {
    main: '#CE162D',
  },
  text: {
    primary: '#333333',
    secondary: '#666666',
  },
  background: {
    default: '#F4F6F8',
    paper: white,
  },
  divider: colors.grey[200],
  grey: {
    100: '#F4F4F4',
    300: '#E1E1E1',
    400: '#C2C2C2',
    500: '#999999',
    700: '#666666',
    800: '#454545',
    900: '#333333',
    A100: '#424242',
    A200: '#595959',
  },
};

export default palette;
