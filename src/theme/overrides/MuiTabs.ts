import palette from '../palette';
import { StyleRules } from '@material-ui/core/styles/withStyles';

const style: StyleRules = {
  root: {
    borderBottom: `1px solid ${palette.grey?.['400']}`,
    borderBottomLeftRadius: 6,
  },
  indicator: {
    display: 'none',
  },
};
export default style;
