import { StyleRules } from '@material-ui/core/styles/withStyles';
import palette from '../palette';

const style: StyleRules = {
  root: {
    border: `1px solid ${palette.grey![400]}`,
    borderRadius: 6,
    borderCollapse: 'separate',
  },
};
export default style;
