import { StyleRules } from '@material-ui/core/styles/withStyles';

const style: StyleRules = {
  root: {
    '&:last-child .MuiTableCell-root.MuiTableCell-body': {
      borderBottom: 'none',
    },
  },
};
export default style;
