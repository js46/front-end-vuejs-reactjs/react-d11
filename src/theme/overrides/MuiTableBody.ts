import { StyleRules } from '@material-ui/core/styles/withStyles';
import palette from '../palette';

const style: StyleRules = {
  root: {
    color: palette.grey![900],
  },
};

export default style;
