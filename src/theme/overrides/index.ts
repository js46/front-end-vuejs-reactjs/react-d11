import MuiButton from './MuiButton';
import MuiIconButton from './MuiIconButton';
import MuiPaper from './MuiPaper';
import MuiTableCell from './MuiTableCell';
import MuiTableHead from './MuiTableHead';
import MuiTypography from './MuiTypography';
import MuiChip from './MuiChip';
import MuiTableContainer from './MuiTableContainer';
import MuiTableRow from './MuiTableRow';
import MuiTableBody from './MuiTableBody';
import MuiInputBase from './MuiInputBase';
import MuiOutlinedInput from './MuiOutlinedInput';
import MuiFormControl from './MuiFormControl';
import MuiFormHelperText from './MuiFormHelperText';
import MuiInputLabel from './MuiInputLabel';
import MuiTab from './MuiTab';
import MuiTabs from './MuiTabs';

export default {
  MuiButton,
  MuiIconButton,
  MuiPaper,
  MuiTableCell,
  MuiTableHead,
  MuiTypography,
  MuiChip,
  MuiTableContainer,
  MuiTableRow,
  MuiTableBody,
  MuiInputBase,
  MuiOutlinedInput,
  MuiFormControl,
  MuiFormHelperText,
  MuiInputLabel,
  MuiTab,
  MuiTabs,
};
