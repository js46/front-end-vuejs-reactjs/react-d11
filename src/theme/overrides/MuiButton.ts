import { StyleRules } from '@material-ui/core/styles/withStyles';

const style: StyleRules = {
  root: {
    textTransform: 'capitalize',
  },
  contained: {
    padding: '6px 24px',
  },
  outlined: {
    padding: '5px 24px',
  },
};
export default style;
