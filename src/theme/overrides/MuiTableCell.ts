import { StyleRules } from '@material-ui/core/styles/withStyles';
import typography from '../typography';
import palette from '../palette';

const style: StyleRules = {
  root: {
    ...typography.body1,
    borderBottom: `1px solid ${palette.grey![400]}`,
    borderRight: `1px solid ${palette.grey![400]}`,
    '&:last-child': {
      borderRight: 'none',
    },
    paddingTop: '10px',
    paddingBottom: '10px',
  },
};

export default style;
