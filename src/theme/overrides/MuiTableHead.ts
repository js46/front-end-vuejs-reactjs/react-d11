import palette from '../palette';

export default {
  root: {
    backgroundColor: palette.grey![100],
    color: palette.grey![900],
  },
};
