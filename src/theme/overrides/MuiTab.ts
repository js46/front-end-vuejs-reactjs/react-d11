import palette from '../palette';
import { StyleRules } from '@material-ui/core/styles/withStyles';

const style: StyleRules = {
  root: {
    color: '#111',
    fontWeight: 'bold',
    textTransform: 'initial',
    backgroundColor: palette.grey?.['300'],
    border: `1px solid ${palette.grey?.['400']}`,
    fontSize: 18,
    '&$selected': {
      backgroundColor: palette.common?.white,
    },
    '@media (min-width:600px)': {
      minWidth: 100,
    },
    // '&:first-child': {
    //   borderTopLeftRadius: 6,
    //   borderBottomLeftRadius: 6,
    // },
    // '&:last-child': {
    //   borderTopRightRadius: 6,
    //   borderBottomRightRadius: 6,
    // },
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
};
export default style;
