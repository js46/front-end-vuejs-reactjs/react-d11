import { createMuiTheme } from '@material-ui/core/styles';
import palette from './palette';
import typography from './typography';
import overrides from './overrides';

const theme = createMuiTheme({
  palette,
  overrides,
  typography,
  // typography: {
  //   fontFamily: 'Helvetica Neue, Arial',
  // },
  shape: {
    borderRadius: 6,
  },
  props: {
    MuiTypography: {
      variantMapping: {
        h1: 'h1',
        h2: 'h2',
        h3: 'h3',
        h4: 'h4',
        h5: 'h5',
        h6: 'h6',
        subtitle1: 'h3',
        subtitle2: 'h4',
        body1: 'span',
        body2: 'span',
        button: 'button',
      },
    },
  },
});
// @ts-ignore: Unreachable code error
window.theme = theme;

export default theme;
