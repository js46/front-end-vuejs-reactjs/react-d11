import palette from './palette';
import { TypographyOptions } from '@material-ui/core/styles/createTypography';
import * as colors from '@material-ui/core/colors';

const typography: TypographyOptions = {
  fontFamily: ['Helvetica Neue', 'Arial'].join(','),
  h1: {
    color: palette.text!.primary,
    fontWeight: 'bold',
    fontSize: '32px',
    letterSpacing: '-0.24px',
    lineHeight: '40px',
  },
  h2: {
    color: palette.text!.primary,
    fontWeight: 'bold',
    fontSize: '26px',
    letterSpacing: '-0.24px',
    lineHeight: '32px',
  },
  h3: {
    color: palette.text!.primary,
    fontWeight: 'bold',
    fontSize: '24px',
    letterSpacing: '-0.06px',
    lineHeight: '28px',
  },
  h4: {
    color: palette.text!.primary,
    fontWeight: 'bold',
    fontSize: '20px',
    letterSpacing: '-0.06px',
    lineHeight: '24px',
  },
  h5: {
    color: palette.text!.primary,
    fontWeight: 'bold',
    fontSize: '18px',
    letterSpacing: '-0.05px',
    lineHeight: '20px',
  },
  h6: {
    color: palette.text!.primary,
    fontWeight: 'bold',
    fontSize: '16px',
    letterSpacing: '-0.05px',
    lineHeight: '20px',
  },
  subtitle1: {
    color: colors.blueGrey[900],
    fontSize: '16px',
    letterSpacing: '-0.05px',
    lineHeight: '20px',
  },
  subtitle2: {
    color: colors.blueGrey[600],
    fontWeight: 400,
    fontSize: '14px',
    letterSpacing: '-0.05px',
    lineHeight: '20 px',
  },
  body1: {
    color: palette.text!.primary,
    fontSize: '14px',
    fontWeight: 'normal',
    letterSpacing: '-0.05px',
    lineHeight: '21px',
  },
  body2: {
    color: palette.text!.primary,
    fontSize: '14px',
    letterSpacing: '-0.04px',
    lineHeight: '18px',
  },
  caption: {
    color: palette.text!.primary,
    fontWeight: 700,
    fontSize: '14px',
    letterSpacing: '0.33px',
    lineHeight: '13px',
  },
  overline: {
    color: palette.text!.secondary,
    fontSize: '11px',
    fontWeight: 500,
    letterSpacing: '0.33px',
    lineHeight: '13px',
    textTransform: 'uppercase',
  },
};

export default typography;
