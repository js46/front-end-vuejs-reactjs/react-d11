import React from 'react';
import ReactDOM from 'react-dom';
import { css } from '@emotion/core';
import { PersistGate } from 'redux-persist/integration/react';
import FadeLoader from 'react-spinners/FadeLoader';

import App from './App';
import * as serviceWorker from './serviceWorker';
import { store, persistor } from './store/configureStore';
import { Provider } from 'react-redux';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

ReactDOM.render(
  <Provider store={store}>
    <PersistGate
      loading={<FadeLoader css={override} color={'#2C54E3'} loading={true} />}
      persistor={persistor}
    >
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('amperfii'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
