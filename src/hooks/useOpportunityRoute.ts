import { useLocation } from 'react-router-dom';
import { OpportunityPhase } from '../constants/opportunityPhase';
import { capitalize, startCase } from 'lodash';
import { OpportunityState } from '../constants';

export const useOpportunityRoute = () => {
  const query = new URLSearchParams(useLocation().search);
  const phase = capitalize(query.get('phase') || 'capture') as unknown;
  const state = startCase(query.get('state') || '') as OpportunityState;
  const tab = OpportunityPhase[phase as OpportunityPhase];
  let panel: number | undefined;

  switch (state) {
    case OpportunityState.ReviewBacklog:
      panel = 4;
      break;
    case OpportunityState.ExpertReview:
    case OpportunityState.ExpertReviewDone:
      panel = 5;
      break;
    case OpportunityState.Endorse:
    case OpportunityState.EndorseDone:
      panel = 7;
      break;
    case OpportunityState.Analysis:
      panel = 2; //todo fix list panel string
      break;
    default:
      panel = undefined;
  }
  // console.log('[Opportunity]: ', { phase, state });

  return {
    tab: (tab ? parseInt(tab) : undefined) as OpportunityPhase,
    panel,
  };
};
