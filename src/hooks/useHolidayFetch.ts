import { useState } from 'react';
import { getAllHolidays, IHoliday } from '../services/holiday.services';
import moment from 'moment';
import { useEffectOnlyOnce } from './useEffectOnlyOnce';

export const useHolidayFetch = () => {
  const [holidays, setHolidays] = useState<Map<string, string>>();
  const [rawData, setRawData] = useState<IHoliday[]>([]);
  const fetchHolidays = async () => {
    const json = await getAllHolidays();
    const data = new Map<string, string>();
    if (json.success) {
      for (let item of json.data.list) {
        data.set(moment(item.date).format('DD/MM'), item.name);
      }
      setRawData(json.data.list);
    }
    setHolidays(data);
  };
  useEffectOnlyOnce(() => {
    fetchHolidays();
  });
  return { holidays, rawData };
};
