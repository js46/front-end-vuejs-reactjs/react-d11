import React, { useCallback, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

export const useSearchCombine = () => {
  let searchBtnRef = React.createRef<HTMLButtonElement>();
  const [keyword, setKeyword] = useState<undefined | string>();
  const [value, setValue] = useState<undefined | string>();

  const [searchInputSelected, setSearchInputSelected] = useState<
    undefined | string
  >();

  useEffect(() => {
    if (!keyword) searchBtnRef.current?.click();
  }, [keyword, searchBtnRef]);

  useEffect(() => {
    searchBtnRef.current?.click();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchInputSelected]);

  useEffect(() => {
    if (!keyword) searchBtnRef.current?.click();
  }, [keyword, searchBtnRef]);
  const onClearSearch = useCallback(() => {
    Promise.resolve().then(() => {
      ReactDOM.unstable_batchedUpdates(() => {
        setKeyword('');
        setValue('');
        setSearchInputSelected(undefined);
      });
    });
  }, []);

  const onValueSelected = useCallback((value: string) => {
    setKeyword(value);
    setSearchInputSelected(undefined);
    setValue(value);
    setTimeout(() => {
      setSearchInputSelected(value);
    }, 200);
  }, []);

  const onChange = useCallback((value: string) => setValue(value), []);
  const onValueChange = useCallback((value: string) => setKeyword(value), []);

  return {
    searchBtnRef,
    value,
    onValueSelected,
    onClearSearch,
    onChange,
    onValueChange,
    keyword,
  };
};
