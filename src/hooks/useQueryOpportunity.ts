import { useSelector, shallowEqual } from 'react-redux';

import { UserProfileState } from '../store/user/selector';

import { IOpportunity } from '../services/opportunity.services';

export const useQueryOpportunity = (
  checkPrivacy?: boolean,
  extraQuery = {},
) => {
  const userProfile = useSelector(UserProfileState, shallowEqual);

  let privateQuery: object[] = [];
  if (checkPrivacy && !userProfile.isAdmin && !userProfile.internal) {
    let queryTeam = [];
    if (userProfile.team_lead_flg) {
      queryTeam.push({
        term: {
          'resource_team.id': userProfile.resource_team.resource_team_id,
        },
      });
    }
    privateQuery = [
      {
        bool: {
          should: [
            {
              term: {
                private_flag: false,
              },
            },
            {
              term: {
                'created_by.id': userProfile.id,
              },
            },
            {
              term: {
                'current_experts.user_id': userProfile.id,
              },
            },
            {
              term: {
                'current_expert_review.expert_review.id': userProfile.id,
              },
            },
            ...queryTeam,
          ],
        },
      },
    ];
  }
  let defaultQuery = {
    query: {
      bool: {
        must: [
          {
            term: {
              del_flag: false,
            },
          },
          ...privateQuery,
        ],
        ...extraQuery,
      },
    },
  };

  const canViewOpportunity = (opportunity: IOpportunity): boolean => {
    // Check user role
    if (userProfile.isAdmin || userProfile.internal) return true;
    //check team lead
    if (
      userProfile.team_lead_flg &&
      opportunity.resource_team?.id ===
        userProfile.resource_team.resource_team_id
    )
      return true;
    // Check current experts
    const isExpert = !!opportunity.current_experts?.some(
      item => item.user_id === userProfile.id,
    );
    // Check opportunity
    return (
      !opportunity.private_flag ||
      opportunity.created_by?.id === userProfile.id ||
      opportunity.current_expert_review?.expert_review?.id === userProfile.id ||
      isExpert
    );
  };

  return { defaultQuery: () => defaultQuery, privateQuery, canViewOpportunity };
};
