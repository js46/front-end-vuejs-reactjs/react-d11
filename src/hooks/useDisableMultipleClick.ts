import { useState } from 'react';
import { Status } from '../constants';
import { delay } from '../utils';

export const useDisableMultipleClick = () => {
  const [submittingStatus, setSubmittingStatus] = useState(Status.Idle);
  const debounceFn = async (ms: number = 500) => {
    setSubmittingStatus(Status.Submitting);
    await delay(ms);
  };
  const endRequest = () => {
    setSubmittingStatus(Status.Idle);
  };
  return {
    isSubmitting: submittingStatus === Status.Submitting,
    debounceFn,
    endRequest,
  };
};
