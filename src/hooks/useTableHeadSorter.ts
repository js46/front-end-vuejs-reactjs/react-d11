import React, { useCallback, useState } from 'react';
import { SortOrder } from '../components';

export const useTableHeadSorter = (
  fields: readonly string[],
  defaultSortField: typeof fields[number],
  defaultOrder = SortOrder.ASC,
) => {
  const [order, setOrder] = useState<SortOrder>(defaultOrder);
  const [orderBy, setOrderBy] = useState<typeof fields[number]>(
    defaultSortField,
  );
  const handleRequestSort = useCallback(
    (event: React.MouseEvent<unknown>, field: typeof fields[number]) => {
      const isAsc = orderBy === field && order === SortOrder.ASC;
      setOrder(isAsc ? SortOrder.DESC : SortOrder.ASC);
      setOrderBy(field);
    },
    [fields, order, orderBy],
  );
  return {
    order,
    orderBy,
    handleRequestSort,
  };
};
