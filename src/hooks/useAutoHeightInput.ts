import React, { useState } from 'react';

export const useAutoHeightInput = (minRow = 1, maxRow = 5) => {
  const [textRows, setTextRows] = useState(1);
  const [value, setValue] = useState('');
  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const text = event.target.value;
    const lines = text.split('\n').length;
    if (lines >= minRow && lines <= maxRow) {
      setTextRows(lines);
    }
    setValue(text);
  };
  return { textRows, onChange, value, setValue, setTextRows };
};
