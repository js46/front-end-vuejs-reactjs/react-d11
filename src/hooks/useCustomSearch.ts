import React, { useCallback, useEffect, useMemo, useState } from 'react';

export const useCustomSearch = (
  fields: string[],
  queryFormat = 'or',
  must: any[] = [],
  extraQuery = {},
) => {
  let searchBtnRef = React.createRef<HTMLButtonElement>();
  const [keyword, setKeyword] = useState<undefined | string>();
  const [value, setValue] = useState<undefined | string>();
  const [searchInputSelected, setSearchInputSelected] = useState<
    undefined | string
  >();

  useEffect(() => {
    if (!keyword) searchBtnRef.current?.click();
  }, [keyword, searchBtnRef]);

  useEffect(() => {
    searchBtnRef.current?.click();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchInputSelected]);

  const searchQuery = useMemo(() => {
    return {
      query: {
        bool: {
          must: [
            {
              multi_match: {
                query: keyword,
                fields,
                type: 'cross_fields',
                operator: queryFormat,
              },
            },
            ...must,
          ],
          ...extraQuery,
        },
      },
    };
  }, [extraQuery, fields, keyword, must, queryFormat]);

  const onClearSearch = useCallback(() => {
    setKeyword('');
    setValue('');
    setSearchInputSelected('');
  }, []);

  const onValueSelected = useCallback((value: string) => {
    setKeyword(value);
    setSearchInputSelected(value);
  }, []);

  const onChange = useCallback((value: string) => setValue(value), []);
  const onValueChange = useCallback((value: string) => setKeyword(value), []);

  const onClickSearch = useCallback(
    (setQuery: any) => {
      if (!keyword) return setQuery({});
      setQuery({
        value: keyword,
        query: searchQuery,
      });
    },
    [keyword, searchQuery],
  );

  return {
    searchBtnRef,
    searchQuery,
    value,
    onValueSelected,
    onClearSearch,
    onChange,
    onValueChange,
    onClickSearch,
  };
};
