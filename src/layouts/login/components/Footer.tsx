import React from 'react';
import { Typography, Link } from '@material-ui/core';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    position: 'fixed',
    bottom: '0',
    width: '100vw',
  },
}));

export const Footer: React.FC<{ className?: string }> = ({ className }) => {
  const classes = useStyles();

  return (
    <footer className={clsx(classes.root, className)}>
      <Typography variant="body1" align="center">
        <Link component="a" href="/" target="_blank">
          Amperfii
        </Link>{' '}
        &copy; {new Date().getFullYear()}
      </Typography>
    </footer>
  );
};
