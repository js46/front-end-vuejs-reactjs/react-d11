import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import * as colors from '@material-ui/core/colors';

import { Footer } from './components/Footer';
import { Grid, Paper, Container } from '@material-ui/core';
import mainLogo from '../../assets/img/small.png';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    background: colors.grey[200],
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(0, 3, 3, 3),
  },
  container: {
    maxWidth: '496px',
  },
}));

export const LoginLayout: React.FC = ({ children }) => {
  const classes = useStyles();

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <Container
        component="section"
        maxWidth="xs"
        className={classes.container}
      >
        <Paper elevation={3} className={classes.paper}>
          <img src={mainLogo} alt="" />
          {children}
        </Paper>
      </Container>
      <Footer />
    </Grid>
  );
};
