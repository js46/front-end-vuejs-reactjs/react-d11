import React from 'react';
import { Toolbar } from '@material-ui/core';
import { Topbar, Breadcrumb, Sidebar } from './components';
import { MainBox, SectionBox, TStringBoolean } from '../../components/styled';

export const MainLayout: React.FC<{
  hasSidebar?: boolean;
}> = ({ children, hasSidebar }) => {
  return (
    <MainBox component="main">
      <Topbar />
      {hasSidebar && <Sidebar />}
      <SectionBox
        data-has-sidebar={
          hasSidebar ? TStringBoolean.TRUE : TStringBoolean.FALSE
        }
        component="section"
      >
        <Toolbar />
        <Breadcrumb />
        {children}
      </SectionBox>
    </MainBox>
  );
};
