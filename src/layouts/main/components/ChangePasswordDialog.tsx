import React from 'react';
import { Box, TextField } from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { FormDialog, MixTitle } from '../../../components';
import { ChangePasswordUser } from '../../../services/profile.services';
import { useDispatch } from 'react-redux';
import { notifySuccess } from '../../../store/common/actions';
import { useTranslation } from 'react-i18next';
interface ChangePasswordProps {
  open: boolean;
  onClose: () => void;
}

export const ChangePasswordDialog: React.FC<ChangePasswordProps> = React.memo(
  ({ open, onClose }) => {
    const initialDataForm = {
      old_password: '',
      new_password: '',
      new_confirm_password: '',
    };
    const dispatch = useDispatch();
    const { i18n } = useTranslation();
    const handleSubmit = async (value: any) => {
      try {
        const json = await ChangePasswordUser({
          password: value.old_password,
          new_password: value.new_password,
        });
        if (json.success) {
          onClose();
          formik.resetForm({});
          dispatch(
            notifySuccess({
              message: i18n.t('auth.change_password_success'),
            }),
          );
          return;
        }
        formik.setFieldError('old_password', json.message);
      } catch (e) {
        formik.setFieldError('old_password', e.message);
      }
    };

    const formik = useFormik({
      initialValues: initialDataForm,
      validationSchema: Yup.object({
        old_password: Yup.string().required('Current password is required'),
        new_password: Yup.string()
          .required('New password is required')
          .min(6, 'New password must be at least 6 characters'),
        new_confirm_password: Yup.string()
          .required('Confirm password is required')
          .oneOf(
            [Yup.ref('new_password'), null],
            'Confirm Password must match',
          ),
      }),
      onSubmit: values => {
        handleSubmit(values);
      },
    });
    return (
      <FormDialog
        title="Change Password"
        labelBtn="Change Password"
        submit={formik.handleSubmit}
        open={open}
        setOpen={onClose}
        isAddingForm={false}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Current Password" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="old_password"
                type="password"
                id="old_password"
                autoComplete="old_password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.old_password}
                error={
                  formik.touched.old_password && !!formik.errors.old_password
                }
                helperText={
                  formik.touched.old_password ? formik.errors.old_password : ''
                }
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="New Password" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="new_password"
                id="new_password"
                type="password"
                autoComplete="new_password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.new_password}
                error={
                  formik.touched.new_password && !!formik.errors.new_password
                }
                helperText={
                  formik.touched.new_password ? formik.errors.new_password : ''
                }
              />
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Confirm New Password" isRequired />
              <TextField
                fullWidth
                variant="outlined"
                name="new_confirm_password"
                id="new_confirm_password"
                type="password"
                autoComplete="new_confirm_password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.new_confirm_password}
                error={
                  formik.touched.new_confirm_password &&
                  !!formik.errors.new_confirm_password
                }
                helperText={
                  formik.touched.new_confirm_password
                    ? formik.errors.new_confirm_password
                    : ''
                }
              />
            </Box>
          </Box>
        }
      />
    );
  },
);
