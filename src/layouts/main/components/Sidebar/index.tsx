import React, { useEffect, useState } from 'react';
import { Drawer, DrawerProps, Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ListAltIcon from '@material-ui/icons/ListAlt';
import NoteIcon from '@material-ui/icons/Note';
import BuildIcon from '@material-ui/icons/Build';
import PersonIcon from '@material-ui/icons/Person';
import NotificationsIcon from '@material-ui/icons/Notifications';
import SettingsIcon from '@material-ui/icons/Settings';
import { AttachFile } from '@material-ui/icons';
import { useParams } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { SidebarNav } from './components';
import { UserMenuState } from '../../../../store/user/selector';
import { UserProfileState } from '../../../../store/user/selector';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 256,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 256,
    backgroundColor: 'white',
    padding: '10px',
  },
  paperAnchorLeft: {
    borderRight: `1px solid ${theme.palette.grey[500]}`,
  },
}));

export const Sidebar: React.FC<DrawerProps> = ({ variant, ...rest }) => {
  const classes = useStyles();
  const userMenuState = useSelector(UserMenuState);
  const { id } = useParams();

  let location = useLocation();
  const pathArray = location.pathname.split('/').slice(1);
  const userNavigation = pathArray[pathArray.length - 2];

  const myPages = [
    {
      title: 'Opportunities',
      href: '/my-stuff',
      icon: <BuildIcon />,
      path: '/my-stuff',
      hasId: true,
    },
    {
      title: 'Profile',
      href: '/profile',
      icon: <PersonIcon />,
      path: '/profile',
      hasId: true,
    },
    {
      title: 'To Do List',
      href: '/my-stuff/todo-list',
      icon: <ListAltIcon />,
      path: '/my-stuff/todo-list',
      hasId: false,
    },
    {
      title: 'Notes',
      href: '/my-stuff/notes',
      icon: <NoteIcon />,
      path: '/my-stuff/notes',
      hasId: true,
    },
    {
      title: 'Attachments',
      href: '/my-stuff/attachments',
      icon: <AttachFile />,
      path: '/my-stuff/attachments',
      hasId: true,
    },
    // {
    //   title: 'Dashboard',
    //   href: '/dashboard',
    //   icon: <InsertChartIcon />,
    //   path: '/dashboard',
    //   hasId: false,
    // },
    {
      title: 'Notification',
      href: '/',
      icon: <NotificationsIcon />,
      path: '/',
      hasId: false,
    },
    // {
    //   title: 'Search',
    //   href: '/search',
    //   icon: <SearchIcon />,
    //   path: '/search',
    //   hasId: false,
    // },
    {
      title: 'Setting',
      href: '/',
      icon: <SettingsIcon />,
      path: '/',
      hasId: false,
    },
  ];

  const userPages = [
    {
      title: 'Opportunities',
      href: `/profile/${id}/opportunity`,
      icon: <BuildIcon />,
      path: '/my-stuff',
      hasId: true,
    },
    {
      title: 'Profile',
      href: `/profile/${id}`,
      icon: <PersonIcon />,
      path: '/profile',
      hasId: true,
    },
  ];

  const expertPages = [
    {
      title: 'Opportunities',
      href: `/experts/${id}/opportunity`,
      icon: <BuildIcon />,
      path: '/my-stuff',
      hasId: true,
    },
    {
      title: 'Profile',
      href: `/experts/${id}`,
      icon: <PersonIcon />,
      path: '/experts',
      hasId: true,
    },
  ];

  const [pageState, setPageState] = useState<typeof myPages>([]);
  const userProfileState = useSelector(UserProfileState);
  // const [profile,setProfile,] = useState({ username: '' });

  useEffect(() => {
    const pathMap = userMenuState.map(item => item[1]);
    if (id && Number(id) !== userProfileState.id) {
      if (userNavigation && userNavigation === 'experts') {
        const listSidebarItem = expertPages.filter(
          p => pathMap.indexOf(p.path) > -1,
        );
        setPageState(listSidebarItem);
      } else {
        const listSidebarItem = userPages.filter(
          p => pathMap.indexOf(p.path) > -1,
        );
        setPageState(listSidebarItem);
      }
    } else {
      const listSidebarItem = myPages.filter(p => pathMap.indexOf(p.path) > -1);
      setPageState(listSidebarItem);
    }

    // const getToken = localStorage.getItem('jwt');
    // if (getToken) {
    //   setProfile(jwt_decode(getToken));
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userMenuState]);

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
        paperAnchorLeft: classes.paperAnchorLeft,
      }}
      {...rest}
    >
      <Toolbar />
      {/* <img src={mainLogo} alt="" style={{ maxHeight: 48, width: '100%' }} /> */}
      {/* <Profile profile={profile} /> */}
      <SidebarNav pages={pageState} />
    </Drawer>
  );
};
export default Sidebar;
