import React from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import { List, ListItem, Button, ListProps, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: {
    padding: '10px 8px',
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
    fontWeight: theme.typography.fontWeightMedium,
    borderRadius: 6,
    color: theme.palette.grey[700],
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
    color: theme.palette.grey[700],
  },
  active: {
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightBold,
    background: theme.palette.primary.light,
    '& $icon': {
      color: theme.palette.primary.main,
    },
    // '&:hover, &:focus': {
    //   background: theme.palette.primary.main,
    // },
  },
}));

export const SidebarNav: React.FC<{
  pages: any[];
} & ListProps> = ({ className, pages, ...rest }) => {
  const classes = useStyles();

  return (
    <div>
      <List {...rest} className={className}>
        {pages &&
          pages.map(page => (
            <Box key={page.title}>
              <ListItem className={classes.item} disableGutters>
                <Button
                  component={RouterLink}
                  to={page.href}
                  className={classes.button}
                  activeClassName={classes.active}
                  exact
                >
                  <div className={classes.icon}>{page.icon}</div>
                  {page.title}
                </Button>
              </ListItem>
            </Box>
          ))}
      </List>
    </div>
  );
};
