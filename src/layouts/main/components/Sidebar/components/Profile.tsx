import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AvatarProps, Avatar, Typography, Grid } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  avatar: {
    width: 60,
    height: 60,
    fontSize: 24,
  },
  name: {
    marginTop: theme.spacing(1),
    fontSize: 14,
  },
}));
export const Profile: React.FC<{ profile: any } & AvatarProps> = ({
  className,
  profile,
  ...rest
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <div>
      <Grid container className={classes.root}>
        <Grid item xs={4}>
          <Avatar className={classes.avatar}>
            {profile.username.substring(0, 1).toUpperCase()}
          </Avatar>
        </Grid>
        <Grid item xs={8}>
          <Typography variant="h4" className={classes.name}>
            {profile.username}
          </Typography>
          <Typography variant="body2">{t('role_name_desc')}</Typography>
        </Grid>
      </Grid>
    </div>
  );
};
