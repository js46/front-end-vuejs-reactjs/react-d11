import React, { useEffect, useState } from 'react';
import { AvatarProps, Avatar } from '@material-ui/core';
import jwtDecode from 'jwt-decode';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

export const UserAvatar: React.FC<AvatarProps> = ({ ...rest }) => {
  const [username, setUsername] = useState('');
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const [isAuthen, setIsAuthen] = useState(
    !!window.localStorage.getItem('jwt'),
  );
  let history = useHistory();
  const logout = () => {
    isAuthen && window.localStorage.removeItem('jwt');
    setIsAuthen(false);
    history.push('/login');
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const useStyles = makeStyles(theme => ({
    menu: {
      left: '57px!important',
      // top: ' 787px!important',
      bottom: '10px!important',
      width: 200,
      borderRadius: 5,
    },
  }));
  const classes = useStyles();
  useEffect(() => {
    const getToken = localStorage.getItem('jwt');
    if (getToken) {
      const userInfo = jwtDecode<{ username: string; [key: string]: any }>(
        getToken,
      );
      setUsername(userInfo.username);
    }
  }, []);
  return (
    <div>
      <Avatar onClick={handleClick} {...rest}>
        {username.substring(0, 1).toUpperCase()}
      </Avatar>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
        PopoverClasses={{
          paper: classes.menu,
        }}
        // className={{ paper: classes.paper }}
      >
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>My account</MenuItem>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </Menu>
    </div>
  );
};
