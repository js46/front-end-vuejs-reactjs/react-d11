import React, { useState } from 'react';
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Box,
  Grid,
  IconButton,
  Paper,
  Badge,
  Avatar,
  IconButtonTypeMap,
} from '@material-ui/core';
import {
  Apps,
  MailOutline,
  Delete,
  NotificationsNone,
  Input,
  PersonAdd,
  Search,
  Help,
  EventAvailable,
  Settings,
  Add,
} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import * as colors from '@material-ui/core/colors';
import styled from 'styled-components';
import mainLogo from '../../../assets/img/amperfii-logo.png';
import { UserAvatar } from './components/UserAvatar';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 66,
    height: '100%',
  },
  paperAnchorDockedLeft: {
    borderRight: 0,
  },
  paper: {
    background: '#292E4C',
    color: '#fff',
  },
  buttonIcon: {
    color: colors.grey[50],
  },
}));

const StyledIconButton = styled(IconButton)<{
  zoom: any;
  [key: string]: any;
}>`
  transition: all 0.3s;
  &:hover {
    background: rgba(0, 0, 0, 0.3);
    transform: ${props => (props.scale ? 'scale(.8)' : 'none')};
  }
`;

const StyledGrid = styled(Grid)`
  color: #fff;
`;

export const MiniSidebar = () => {
  const [isAuthen, setIsAuthen] = useState(
    !!window.localStorage.getItem('jwt'),
  );
  const classes = useStyles();
  let history = useHistory();

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: classes.paper,
        paperAnchorDockedLeft: classes.paperAnchorDockedLeft,
      }}
    >
      <StyledGrid
        container
        direction="column"
        justify="space-between"
        alignItems="flex-start"
        className={classes.drawer}
      >
        <Grid container direction="column" alignItems="center">
          <StyledIconButton color="inherit">
            <Apps color="primary" />
          </StyledIconButton>
          <StyledIconButton color="inherit">
            <Badge badgeContent={6} color="primary">
              <NotificationsNone />
            </Badge>
          </StyledIconButton>
          <StyledIconButton color="inherit">
            <Badge badgeContent={8} color="primary">
              <MailOutline />
            </Badge>
          </StyledIconButton>
          <StyledIconButton>
            <Add color="primary" fontSize="large" />
          </StyledIconButton>
        </Grid>
        {isAuthen && (
          <Grid container direction="column" alignItems="center">
            <StyledIconButton color="inherit">
              <EventAvailable />
            </StyledIconButton>
            <StyledIconButton color="inherit">
              <PersonAdd />
            </StyledIconButton>
            <StyledIconButton color="inherit">
              <Search />
            </StyledIconButton>
            <StyledIconButton color="inherit">
              <Help />
            </StyledIconButton>
            <StyledIconButton color="inherit">
              <Settings />
            </StyledIconButton>
            <StyledIconButton scale="true" aria-label="delete" color="inherit">
              <UserAvatar />
            </StyledIconButton>
            {/* <StyledIconButton
              aria-label="delete"
              color="inherit"
              onClick={() => {
                window.localStorage.removeItem('jwt');
                setIsAuthen(false);
                history.push('/login');
              }}
            >
              <Input />
            </StyledIconButton> */}
          </Grid>
        )}
      </StyledGrid>
    </Drawer>
  );
};
