import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import {
  AppBarProps,
  AppBar,
  Toolbar,
  IconButton,
  Menu,
  MenuItem,
  Box,
  Typography,
  Avatar,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Menu as MenuIcon } from '@material-ui/icons';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import VpnKeyOutlinedIcon from '@material-ui/icons/VpnKeyOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { MainLogo } from '../../../assets';
import { useDispatch, useSelector } from 'react-redux';

import { Dispatch } from 'redux';

import { useToggleValue } from '../../../hooks';
import { HelpDialog } from './HelpDialog';
import { ChangePasswordDialog } from './ChangePasswordDialog';
import { DialogWarningRole } from './DialogWarningRole';
import { useTranslation } from 'react-i18next';
import { isEmpty } from 'lodash';
import { UserProfileState } from '../../../store/user/selector';
import { UserLogout } from '../../../store/auth/actions';
import { IsLoggedIn } from '../../../store/auth/selector';
const useStyles = makeStyles(theme => ({
  root: {
    zIndex: theme.zIndex.drawer + 1,
    background: '#F4F4F4',
    // boxShadow = '0px',
  },
  toolbar: {
    paddingTop: '5px',
    paddingBottom: '5px',
  },
  menu: {
    marginTop: '35px',
  },
  flexGrow: {
    flexGrow: 1,
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
    color: '#000',
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  icon: {
    fontSize: '1.3rem',
  },
}));

export const Topbar: React.FC<AppBarProps> = ({ className, ...rest }) => {
  const dispatch = useDispatch<Dispatch<any>>();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const userProfileState = useSelector(UserProfileState);
  const isAuth = useSelector(IsLoggedIn);
  const [showWarningDialog, setWarningDialog] = useState<boolean>(false);
  const [showHelp, toggleHelp] = useToggleValue(false);
  const [showChangePassword, toggleChangePassword] = useToggleValue(false);
  const classes = useStyles();
  useEffect(() => {
    if (
      userProfileState &&
      (userProfileState.casbin_role.indexOf('role:expert') !== -1 ||
        userProfileState.casbin_role.indexOf('role:teamlead') !== -1) &&
      (userProfileState.location_id === 0 ||
        isEmpty(userProfileState.profile_role.profile_role_name) ||
        userProfileState.resource_team.resource_team_id === 0 ||
        userProfileState.business_unit.business_unit_id === 0)
    ) {
      setWarningDialog(true);
    }
  }, [userProfileState]);
  const handleClickMenuDropdown = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const { i18n } = useTranslation();

  const handleClickInsideMoreOption = (option: string) => () => {
    switch (option) {
      case 'help':
        toggleHelp();
        break;
      case 'logout':
        dispatch(UserLogout());
        break;
      case 'change-password':
        toggleChangePassword();
        break;
      default:
    }
    handleClose();
  };

  return (
    <AppBar
      position="fixed"
      {...rest}
      className={clsx(classes.root, className)}
      elevation={0}
    >
      <Toolbar className={classes.toolbar}>
        <Link to={'/'}>
          <img src={MainLogo} alt="mainLogo" style={{ maxHeight: '36px' }} />
        </Link>
        <div className={classes.flexGrow} />

        <Box>
          {isAuth && (
            <>
              <IconButton
                className={classes.signOutButton}
                onClick={handleClickMenuDropdown}
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="more-option"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                className={classes.menu}
              >
                <Box
                  padding={3}
                  pt={2}
                  pb={2}
                  display="flex"
                  alignItems="center"
                  flexDirection="column"
                >
                  <Box mb={2}>
                    <Avatar
                      alt={userProfileState.user_name}
                      className={classes.avatar}
                    >
                      {userProfileState.full_name.substr(0, 1)}
                    </Avatar>
                  </Box>
                  <Typography variant="h6" align="center">
                    {userProfileState.full_name}
                  </Typography>
                  <Typography variant="subtitle1" align="center">
                    {userProfileState.email}
                  </Typography>
                </Box>
                <MenuItem onClick={handleClickInsideMoreOption('help')}>
                  <HelpOutlineIcon className={classes.icon} />
                  <Box p={0.8} pl="10px" pr="10px">
                    {i18n.t('help')}
                  </Box>
                </MenuItem>
                <MenuItem onClick={handleClickInsideMoreOption('about')}>
                  <InfoOutlinedIcon className={classes.icon} />
                  <Box p={0.8} pl="10px" pr="10px">
                    {i18n.t('about')}
                  </Box>
                </MenuItem>
                <MenuItem
                  onClick={handleClickInsideMoreOption('change-password')}
                >
                  <VpnKeyOutlinedIcon className={classes.icon} />
                  <Box p={0.8} pl="10px" pr="10px">
                    {i18n.t('change_password')}
                  </Box>
                </MenuItem>
                <MenuItem onClick={handleClickInsideMoreOption('logout')}>
                  <ExitToAppIcon className={classes.icon} />
                  <Box p={0.8} pl="10px" pr="10px">
                    {i18n.t('logout')}
                  </Box>
                </MenuItem>
              </Menu>
            </>
          )}
        </Box>
      </Toolbar>
      <HelpDialog open={showHelp} onClose={toggleHelp} />
      {showWarningDialog && (
        <DialogWarningRole
          open={showWarningDialog}
          setOpen={setWarningDialog}
          data={{
            profile_role_id: userProfileState.profile_role.profile_role_id,
            location_id: userProfileState.location_id,
            proficiency_level_id: 1,
            business_unit_id: userProfileState.business_unit.business_unit_id,
            resource_team_id: userProfileState.resource_team.resource_team_id,
          }}
        />
      )}
      <ChangePasswordDialog
        open={showChangePassword}
        onClose={toggleChangePassword}
      />
    </AppBar>
  );
};
