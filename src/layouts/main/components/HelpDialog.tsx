import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  OutlinedInput,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { useCallback, useRef, useState } from 'react';
import { CommentReactiveList, CommentSkeleton } from '../../../components';
import { customQueryHelp } from '../../../utils';
import { addHelpFeedback } from '../../../services/comment.services';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import { useAutoHeightInput, useToggleValue } from '../../../hooks';
import { useTranslation } from 'react-i18next';

interface HelpDialogProps {
  open: boolean;
  onClose: () => void;
}

export const HelpDialog: React.FC<HelpDialogProps> = React.memo(
  ({ open, onClose }) => {
    let topCommentRef = useRef<HTMLDivElement>(null);
    const [keyword, setKeyword] = useState('');
    const [search, toggleSearch] = useToggleValue(false);
    const [lastRefresh, setLastRefresh] = useState(0);
    const { t } = useTranslation();
    const {
      value,
      onChange,
      textRows,
      setValue,
      setTextRows,
    } = useAutoHeightInput(1);

    const defaultQuery = useCallback(() => {
      return customQueryHelp(
        'feedback',
        {
          comment_parent_id: 0,
          comment_body: keyword,
        },
        lastRefresh,
      );
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search, lastRefresh]);

    // const onClearSearch = useCallback(() => {
    //   setKeyword('');
    //   toggleSearch();
    // }, [toggleSearch]);
    // const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    //   const { value } = event.target;
    //   if (value.split('\n').length > 0) setTextRows(value.split('\n').length);
    //   setComment(event.target.value);
    // };

    const onSubmit = useCallback(async () => {
      if (!value.trim()) return;
      const json = await addHelpFeedback(value);
      if (json.success) {
        setValue('');
        setTextRows(1);
        setTimeout(() => {
          setLastRefresh(Date.now());
          setTimeout(() => {
            topCommentRef?.current?.scrollIntoView({ behavior: 'smooth' });
          }, 200);
        }, 800);
      }
    }, [setTextRows, setValue, value]);

    return (
      <Dialog open={open} onClose={onClose} maxWidth="lg" fullWidth>
        <DialogTitle style={{ paddingBottom: 6 }}>
          <Box display="flex" justifyContent="space-between">
            <Typography variant="h2">{t('help')}</Typography>
            <Box display="flex">
              <OutlinedInput
                placeholder="Search a keyword..."
                value={keyword}
                name="keyword"
                onChange={event => setKeyword(event.target.value)}
                onKeyDown={event =>
                  event.keyCode === 13 ? toggleSearch() : false
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton type="submit" onClick={toggleSearch}>
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                }
              />
            </Box>
            <IconButton onClick={onClose} aria-label="close">
              <CloseIcon />
            </IconButton>
          </Box>
        </DialogTitle>
        <DialogContent style={{ minHeight: '62vh' }}>
          <div ref={topCommentRef} />
          <CommentReactiveList
            defaultQuery={defaultQuery}
            canReply
            loader={<CommentSkeleton limit={10} />}
          />
        </DialogContent>
        <Paper elevation={2}>
          <DialogActions>
            <Box flex={1} display="flex" flexDirection="column" margin={2}>
              <Box display="flex" flexDirection="row" flex={1}>
                <Box mr={2}>
                  <Avatar>A</Avatar>
                </Box>
                <TextField
                  fullWidth
                  placeholder="Type your feedback..."
                  name="feedback"
                  multiline
                  rows={textRows}
                  variant="outlined"
                  value={value}
                  onChange={onChange}
                />
              </Box>
              <Box flex={1} display="flex" justifyContent="flex-end" mt={2}>
                <Button color="primary" variant="contained" onClick={onSubmit}>
                  {t('add_feedback')}
                </Button>
              </Box>
            </Box>
          </DialogActions>
        </Paper>
      </Dialog>
    );
  },
);
