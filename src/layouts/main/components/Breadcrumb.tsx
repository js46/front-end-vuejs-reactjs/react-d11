import React from 'react';
import { useLocation } from 'react-router-dom';
import { Typography, Breadcrumbs, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    zIndex: theme.zIndex.drawer + 1,
    background: 'white',
  },

  breadcrumb: {
    color: 'gray',
    paddingTop: '20px',
    fontSize: '14px',
  },
  capitalize: {
    color: 'gray',
    textTransform: 'capitalize',
    fontSize: '14px',
  },
}));
const formatBreadcrumb = (bc: string) => bc.replace('-', ' ');
const pathCreation = (arr: Array<string>) => {
  return arr.map((item, index) => {
    return {
      name: item,
      path: '/'.concat(arr.slice(0, index + 1).join('/')),
    };
  });
};

export const Breadcrumb: React.FC<{ className?: string }> = ({ className }) => {
  let location = useLocation();
  const classes = useStyles();
  const pathArray = pathCreation(
    location.pathname.split('/').filter(item => item !== ''),
  );

  return location.pathname !== '/' && pathArray.length ? (
    <Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumb}>
      <Link
        color="inherit"
        className={classes.breadcrumb}
        component={RouterLink}
        to="/"
      >
        Home
      </Link>
      {pathArray.map((item, index) => {
        return index < pathArray.length - 1 ? (
          <Link
            component={RouterLink}
            key={index}
            color="inherit"
            to={item.path}
            className={classes.capitalize}
          >
            {formatBreadcrumb(item.name)}
          </Link>
        ) : (
          <Typography className={classes.capitalize} key={index}>
            {formatBreadcrumb(item.name)}
          </Typography>
        );
      })}
    </Breadcrumbs>
  ) : (
    <></>
  );
};
