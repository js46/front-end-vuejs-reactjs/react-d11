import React, { useState } from 'react';
import { Box, Select, MenuItem, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { MixTitle, FormDialog } from '../../../components';
import { useEffectOnlyOnce } from '../../../hooks';
import { useFormik } from 'formik';
import { StyledRating } from '../../../components/styled';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  IListRole,
  getLocations,
  UserLocation,
  getRoleProfile,
  updateCurrentUserProfile,
} from '../../../services/profile.services';
import {
  IBusinessUnit,
  IResourceTeam,
  getResourceTeam,
  getBusinessUnits,
} from '../../../services/references.services';
import { RequestNewUserProfile } from '../../../store/user/actions';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';

interface DialogWarningProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  data: {
    location_id: number;
    profile_role_id: number;
    proficiency_level_id: number;
    business_unit_id: number;
    resource_team_id: number;
  };
}
const useStyles = makeStyles(theme => ({
  iconMinimize: {
    borderRadius: 15,
    backgroundColor: '#E1E1E1',
    fontSize: '3rem',
    width: 38,
    height: 7,
    marginLeft: 4,
  },
}));

export const DialogWarningRole: React.FC<DialogWarningProps> = ({
  open,
  setOpen,
  data,
}) => {
  const initialDataForm = data;
  const [profileRole, setProfileRole] = useState<IListRole[]>();
  const [userLocation, setUserLocation] = useState<UserLocation[]>();
  const [resourceTeam, setResourceTeam] = useState<IResourceTeam[]>();
  const [businessUnit, setBusinessUnit] = useState<IBusinessUnit[]>();
  const dispatch = useDispatch();
  const { i18n } = useTranslation();
  const classes = useStyles();
  const formik = useFormik({
    initialValues: initialDataForm,
    validationSchema: Yup.object({
      location_id: Yup.number().required(i18n.t('validate.field_required')),
      profile_role_id: Yup.number().required(i18n.t('validate.field_required')),
      business_unit_id: Yup.number().required(
        i18n.t('validate.field_required'),
      ),
      resource_team_id: Yup.number().required(
        i18n.t('validate.field_required'),
      ),
    }),
    onSubmit: values => {
      handleSubmit(values);
    },
  });
  useEffectOnlyOnce(() => {
    Promise.all([
      invokeGetRole(),
      invokeGetLocation(),
      invokeGetResourceTeam(),
      invokeGetBusinessUnit(),
    ]);
  });
  const invokeGetRole = async () => {
    const response = await getRoleProfile();
    if (response.success) {
      setProfileRole(response.data.list);
    }
  };
  const invokeGetBusinessUnit = async () => {
    const response = await getBusinessUnits();
    if (response.success) {
      setBusinessUnit(response.data.list);
    }
  };
  const invokeGetResourceTeam = async () => {
    const response = await getResourceTeam();
    if (response.success) {
      setResourceTeam(response.data.list);
    }
  };
  const invokeGetLocation = async () => {
    const response = await getLocations();
    if (response.success) {
      setUserLocation(response.data.list);
    }
  };

  const handleSubmit = async (item: typeof initialDataForm) => {
    let formData = {
      location_id: item.location_id,
      profile_role: {
        profile_role_id: item.profile_role_id,
        proficiency_level_id: Number(item.proficiency_level_id),
      },
      business_unit_id: item.business_unit_id,
      resource_team_id: item.resource_team_id,
    };

    const response = await updateCurrentUserProfile(formData);
    if (response.success) {
      dispatch(RequestNewUserProfile());
      setOpen(false);
    }
  };
  return (
    <>
      <FormDialog
        title="Please update your profile"
        submit={formik.handleSubmit}
        open={open}
        setOpen={setOpen}
        isAddingForm={false}
        disable={!(formik.isValid && formik.dirty)}
        formContent={
          <Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Business Unit" isRequired />
              <Select
                fullWidth
                name="business_unit_id"
                id="business_unit_id"
                variant="outlined"
                autoComplete="business_unit_id"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.business_unit_id}
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {businessUnit?.map(state => (
                  <MenuItem
                    key={state.id}
                    disabled={state.archive}
                    value={state.id}
                  >
                    {state.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Resource Team" isRequired />
              <Select
                fullWidth
                name="resource_team_id"
                id="resource_team_id"
                variant="outlined"
                autoComplete="resource_team_id"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.resource_team_id}
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {resourceTeam?.map(state => (
                  <MenuItem key={state.id} value={state.id}>
                    {state.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="User Location" isRequired />
              <Select
                fullWidth
                name="location_id"
                id="location_id"
                variant="outlined"
                autoComplete="location_id"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.location_id}
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {userLocation?.map(state => (
                  <MenuItem key={state.id} value={state.id}>
                    {state.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Profile Role" isRequired />
              <Select
                fullWidth
                name="profile_role_id"
                id="profile_role_id"
                variant="outlined"
                autoComplete="profile_role_id"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                inputProps={{ style: { textAlign: 'center' } }}
                value={formik.values.profile_role_id}
                IconComponent={ExpandMoreIcon}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {profileRole?.map(state => (
                  <MenuItem key={state.id} value={state.id}>
                    {state.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box mb={3} mt={3}>
              <MixTitle title="Role Proficiency Level" isHelper={true} />

              <Grid item xs={10}>
                <StyledRating
                  name="proficiency_level_id"
                  value={formik.values.proficiency_level_id}
                  max={4}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  icon={<div className={classes.iconMinimize} />}
                />
              </Grid>
            </Box>
          </Box>
        }
      />
    </>
  );
};
