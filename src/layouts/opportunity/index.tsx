import React from 'react';
import { Toolbar } from '@material-ui/core';
import { Topbar } from '../main/components';
import { MainBox, SectionBox } from '../../components/styled';

export const OpportunityLayout: React.FC = ({ children }) => {
  return (
    <MainBox component="main">
      <Topbar />
      <SectionBox component="section">
        <Toolbar />
        {children}
      </SectionBox>
    </MainBox>
  );
};
