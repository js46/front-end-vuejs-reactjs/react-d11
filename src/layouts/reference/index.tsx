import { ReferenceSidebar } from './components';

import React from 'react';
import { Toolbar } from '@material-ui/core';
import { Topbar, Breadcrumb } from '../main/components';
import { MainBox, SectionBox, TStringBoolean } from '../../components/styled';
export const ReferenceLayout: React.FC<{
  hasSidebar?: boolean;
}> = ({ children, hasSidebar }) => {
  return (
    <MainBox component="main">
      <Topbar />

      <SectionBox
        data-has-sidebar={
          hasSidebar ? TStringBoolean.TRUE : TStringBoolean.FALSE
        }
        component="section"
      >
        <Toolbar />
        <Breadcrumb />
        {hasSidebar && <ReferenceSidebar />}

        {children}
      </SectionBox>
    </MainBox>
  );
};
export default ReferenceLayout;
