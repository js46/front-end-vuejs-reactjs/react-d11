import React from 'react';
import { Drawer, DrawerProps, Grid, Toolbar, List } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  ListAlt,
  LineWeightOutlined,
  PeopleAltOutlined,
  AccountTreeOutlined,
  // BrightnessLowOutlined,
  LowPriorityOutlined,
  FilterCenterFocusOutlined,
  AspectRatioOutlined,
  LabelOutlined,
  PeopleOutline,
  LocalAtmOutlined,
  AssessmentOutlined,
  DeveloperModeOutlined,
  Person,
  MergeType,
  FormatSize,
  DoneAll,
  MenuBook,
  Storage,
  LocalOffer,
  DeveloperBoard,
} from '@material-ui/icons';
import ListItemGroup from './ListItemGroup';
const useStyles = makeStyles(theme => ({
  drawer: {
    width: 256,
    flexShrink: 0,
  },
  activeLink: {
    color: theme.palette.primary.main,
    background: theme.palette.primary.light,
    fontWeight: 700,
    paddingTop: 10,
    borderRadius: 6,
    paddingBottom: 10,
  },
  paperAnchorDockedLeft: {
    borderRight: `1px solid ${theme.palette.grey[500]}`,
  },
  paper: {
    width: 256,
    backgroundColor: 'white',
    padding: '10px',
  },
  drawerPaper: {
    width: 256,
    backgroundColor: 'white',
  },
  upperButton: {
    padding: '5px 15px',
    fontWeight: 'bold',
    color: theme.palette.grey[800],
  },
  titleDialog: {
    color: theme.palette.text.primary,
  },
  dialog: {
    padding: '15px 0px',
  },
  activeItem: {
    color: theme.palette.primary.main,
    background: theme.palette.primary.light,
    fontWeight: 700,
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
  },
  sidebar: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      color: theme.palette.text.primary,
      backgroundColor: theme.palette.text.primary,
    },
    /* '&:hover': {
      color: theme.palette.text.primary,
      background: theme.palette.grey[400],
    }, */
  },
  paddingLeft: {
    paddingLeft: 16,
    paddingRight: 0,
  },
  hoverItem: {
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
    // color: theme.palette.grey[700],
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
}));
export enum SidebarList {
  Business = 'Business',
  CorporateObjective = 'Corporate Objectives',
  BenefitFocusCategory = 'Benefit Focus Category',
  BenefitFocusValue = 'Benefit Focus Value',
  Opportunities = 'Opportunities',
  BusinessUnit = 'Business Unit',
  BusinessProcess = 'Business Process',
  PriorityCategoryWeightings = 'Priority Category Weightings',
  PriorityLabel = 'Priority Label',
  Datasets = 'Datasets',
  OpportunityApproach = 'Opportunity Approach',
  OpportunitySize = 'Opportunity Size',
  ConfidenceLevel = 'Confidence Level',
  Experts = 'Experts',
  ExpertsRoles = 'Expert Roles',
  ExpertsSkills = 'Expert Skills',
  SkillGroups = 'Skill Groups',
  ProficiencyLevel = 'Proficiency Level',
  ResourceTeam = 'Resource Team',
  PlayBooks = 'Analyse Playbook Templates',
  ListTypes = 'List Types',
  Tags = 'Tags',
  // System = 'System',
}
export enum TypeItemSidebar {
  item = 'item',
  group = 'group',
}
const sidebarList = [
  {
    name: SidebarList.Experts,
    icon: <Person />,
    type: TypeItemSidebar.group,
    items: [
      {
        name: SidebarList.ExpertsRoles,
        icon: <AssessmentOutlined />,
        path: '/reference-data/experts-roles',
      },
      {
        name: SidebarList.ProficiencyLevel,
        icon: <DeveloperModeOutlined />,
        path: '/reference-data/proficiency-level',
      },
      {
        name: SidebarList.ExpertsSkills,
        icon: <Person />,
        path: '/reference-data/experts-skills',
      },
      {
        name: SidebarList.SkillGroups,
        icon: <PeopleAltOutlined />,
        path: '/reference-data/skill-groups',
      },
      {
        name: SidebarList.ResourceTeam,
        icon: <PeopleOutline />,
        path: '/reference-data/resource-team',
      },
    ],
  },

  {
    name: SidebarList.Opportunities,
    icon: <AspectRatioOutlined />,
    path: '/reference-data/opportunity',
    items: [
      {
        name: SidebarList.BusinessUnit,
        icon: <ListAlt />,
        path: '/reference-data/business-unit',
      },
      {
        name: SidebarList.BusinessProcess,
        icon: <AccountTreeOutlined />,
        path: '/reference-data/business-process',
      },
      {
        name: SidebarList.CorporateObjective,
        icon: <LowPriorityOutlined />,
        path: '/reference-data/corporate-objective',
      },
      {
        name: SidebarList.BenefitFocusCategory,
        icon: <FilterCenterFocusOutlined />,
        path: '/reference-data/benefit-focus-category',
      },
      {
        name: SidebarList.BenefitFocusValue,
        icon: <LocalAtmOutlined />,
        path: '/reference-data/benefit-focus-value',
      },
      {
        name: SidebarList.PriorityCategoryWeightings,
        icon: <LineWeightOutlined />,
        path: '/reference-data/priority-category-weightings',
      },
      {
        name: SidebarList.PriorityLabel,
        icon: <LabelOutlined />,
        path: '/reference-data/priority-label',
      },
      {
        name: SidebarList.Datasets,
        icon: <Storage />,
        path: '/reference-data/datasets',
      },
      {
        name: SidebarList.OpportunityApproach,
        icon: <MergeType />,
        path: '/reference-data/opportunity-approach',
      },
      {
        name: SidebarList.OpportunitySize,
        icon: <FormatSize />,
        path: '/reference-data/opportunity-size',
      },
      {
        name: SidebarList.ConfidenceLevel,
        icon: <DoneAll />,
        path: '/reference-data/confidence-level',
      },
      {
        name: SidebarList.PlayBooks,
        icon: <MenuBook />,
        path: '/reference-data/analyse-playbook-templates',
      },
      {
        name: SidebarList.ListTypes,
        icon: <DeveloperBoard />,
        path: '/reference-data/list-types',
      },
      {
        name: SidebarList.Tags,
        icon: <LocalOffer />,
        path: '/reference-data/tags',
      },
    ],
  },
  // {
  //   name: SidebarList.System,
  //   icon: <BrightnessLowOutlined />,
  //   path: '/reference-data/business',
  //   items: [],
  // },
];

type ISideBarProps = {} & DrawerProps;

const ReferenceSidebar: React.FC<ISideBarProps> = ({ variant, ...rest }) => {
  const classes = useStyles();

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{
        paper: classes.paper,
        paperAnchorDockedLeft: classes.paperAnchorDockedLeft,
      }}
      {...rest}
    >
      <Toolbar />
      <Grid container spacing={2}>
        <Grid item container justify="flex-start">
          <List
            component="nav"
            aria-label="opportunity sidebar"
            className={classes.drawerPaper}
          >
            {sidebarList.map((item, index) => (
              <ListItemGroup
                name={item.name}
                icon={item.icon}
                path={item.path}
                items={item.items}
                key={index}
              />
            ))}
          </List>
        </Grid>
      </Grid>
    </Drawer>
  );
};

export default ReferenceSidebar;
