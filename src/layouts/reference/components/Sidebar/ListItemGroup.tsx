import React, { useState } from 'react';
import { List, ListItem, Collapse, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ListItemLink from './ListItemLink';
import { useHistory } from 'react-router-dom';
import { useEffectOnlyOnce } from '../../../../hooks';

const useStyles = makeStyles(theme => ({
  nested: {
    paddingLeft: theme.spacing(1),
  },
  selectItem: {
    background: theme.palette.grey[100],
    fontWeight: 700,
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
  },
  itemGroup: {
    '&:hover': {
      borderRadius: 6,
    },
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
    // color: theme.palette.grey[700],
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
}));
export interface ItemLink {
  name: string;
  icon?: JSX.Element;
  path: string;
}
export interface ISideBarProps {
  name: string;
  icon?: JSX.Element;
  path?: string;
  items: ItemLink[];
}

const ListItemGroup: React.FC<ISideBarProps> = ({
  name,
  icon,
  path,
  items,
  ...rest
}) => {
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const handleClick = () => {
    setOpen(!open);
  };
  const history = useHistory();
  useEffectOnlyOnce(() => {
    checkExpand();
  });
  const checkExpand = () => {
    setTimeout(() => {
      items.forEach(item => {
        if (history.location.pathname.indexOf(item?.path) > -1) {
          setOpen(true);
          return false;
        }
      });
    }, 200);
  };
  return (
    <div>
      {items.length > 0 ? (
        <ListItem button onClick={handleClick} className={classes.itemGroup}>
          <div className={classes.icon}>{icon}</div>

          <ListItemText primary={name} />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      ) : (
        <ListItemLink path={path ?? ''} name={name} icon={icon} />
      )}
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={classes.nested}>
          {items.map((item, index) => (
            <ListItemLink
              path={item.path}
              name={item.name}
              icon={item.icon}
              key={index}
            />
          ))}
        </List>
      </Collapse>
    </div>
  );
};
export default ListItemGroup;
