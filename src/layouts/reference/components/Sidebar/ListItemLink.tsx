import React from 'react';
import { ListItem } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  activeItem: {
    color: theme.palette.primary.main,
    background: theme.palette.primary.light,
    fontWeight: 700,
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
  },
  sidebar: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      color: theme.palette.text.primary,
      backgroundColor: theme.palette.text.primary,
    },
  },
  paddingLeft: {
    paddingLeft: 16,
    paddingRight: 0,
  },
  hoverItem: {
    '&:hover': {
      backgroundColor: theme.palette.grey[100],
      borderRadius: 6,
    },
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
    // color: theme.palette.grey[700],
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
}));
interface ItemLink {
  name: string;
  icon?: JSX.Element;
  path: string;
}
const ListItemLink: React.FC<ItemLink> = ({ ...item }) => {
  const classes = useStyles();
  const history = useHistory();
  const activeRoute = (routeName: string) => {
    return history.location.pathname.indexOf(routeName) > -1 ? true : false;
  };
  return (
    <Link to={item.path} className={classes.sidebar}>
      <ListItem
        selected={activeRoute(item.path)}
        className={classes.hoverItem}
        classes={{
          selected: classes.activeItem,
          gutters: classes.paddingLeft,
        }}
      >
        <div className={classes.icon}>{item.icon}</div>
        {item.name}
      </ListItem>
    </Link>
  );
};
export default ListItemLink;
