import loadable from '@loadable/component';

export const ReferenceSidebar = loadable(() => import('./Sidebar'));
