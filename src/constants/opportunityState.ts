export enum OpportunityState {
  ReviewBacklog = 'Review Backlog',
  ExpertReview = 'Expert Review',
  ExpertReviewDone = 'Expert Review Done',
  Endorse = 'Endorse',
  EndorseDone = 'Endorse Done',
  Archive = 'Archive',
  Analysis = 'Analysis',
  Done = 'Done',
}
