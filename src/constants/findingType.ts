export enum FindingType {
  Recommend = 'opportunity_recommend',
  PlaybookItem = 'opportunity_playbook_item',
}
