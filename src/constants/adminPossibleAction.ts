export enum PossibleAction {
  AssignExpertToReview = 'AssignExpertToReview',
  ContinueExpertReview = 'ContinueExpertReview',
  RequestDetail = 'RequestDetail',
  RedirectAnotherTeam = 'RedirectAnotherTeam',
}
