export enum TaskState {
  Assigned = 'ASSIGNED',
  InProgress = 'INPROGRESS',
  Completed = 'COMPLETED',
  Cancelled = 'CANCELLED',
  Timeout = 'TIMEOUT',
}
export const TaskStateID = {
  [TaskState.Assigned]: 1,
  [TaskState.InProgress]: 2,
  [TaskState.Completed]: 3,
  [TaskState.Cancelled]: 4,
  [TaskState.Timeout]: 5,
};
export const StateLabels = {
  [TaskState.Assigned]: 'Assigned',
  [TaskState.InProgress]: 'In Progress',
  [TaskState.Completed]: 'Completed',
  [TaskState.Cancelled]: 'Cancelled',
  [TaskState.Timeout]: 'Timeout',
};
export const listTaskState = [
  {
    id: TaskStateID[TaskState.Assigned],
    name: TaskState.Assigned,
  },
  {
    id: TaskStateID[TaskState.InProgress],
    name: TaskState.InProgress,
  },
  {
    id: TaskStateID[TaskState.Completed],
    name: TaskState.Completed,
  },
  {
    id: TaskStateID[TaskState.Cancelled],
    name: TaskState.Cancelled,
  },
  {
    id: TaskStateID[TaskState.Timeout],
    name: TaskState.Timeout,
  },
];
export enum TaskType {
  System = 'SYSTEM',
  User = 'USER',
}
export enum TaskTypeID {
  System = 1,
  User = 2,
}
export enum TaskSubType {
  Review = 'REVIEW',
  Adhoc = 'ADHOC',
  Playbook = 'PLAYBOOK',
  Endorsement = 'ENDORSEMENT',
}
export enum TaskSubTypeID {
  Review = 10,
  Adhoc,
  Playbook,
  Endorsement,
}
