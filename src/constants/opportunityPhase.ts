export enum OpportunityPhase {
  Capture = 1,
  Shape,
  Analyse,
  Recommend,
  Implement,
  Track,
}
