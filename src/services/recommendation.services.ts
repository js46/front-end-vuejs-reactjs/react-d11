import { IListResponse, IResponse } from './common.services';
import { PlaybookItemFindingT } from './playbook.services';
import { ApiHelper } from '../utils';

const RecommendationServiceRoute = {
  index: '/recommend',
  opportunity_recommend_summary: '/recommend-summary',
  finding: '/finding',
  finding_for_option: '/recommend-option/finding',
};

export interface IRecommendationOption {
  id: number;
  title: string;
  description: string;
  pros: string;
  assumption: string;
  opportunity_id: number;
  effort?: number;
  metrics: string;
  implementation: string;
  is_recommend?: boolean;
  opportunity?: {
    id: number;
    title: string;
  };
  findings: PlaybookItemFindingT[];
}

export interface RecommendationOptionParam {
  title: string;
  description: string;
  pros: string;
  assumption: string;
  effort?: number;
  metrics: string;
  implementation: string;
}

export interface RecommendationSelectedOptionParam {
  summary: any;
  opportunity_id: number;
}

export const getRecommendationOptions = (opportunityID: number) => {
  return ApiHelper.get<IListResponse<IRecommendationOption>>(
    RecommendationServiceRoute.index,
    opportunityID,
  );
};

export const addRecommendationOption = (
  opportunityID: number,
  body: RecommendationOptionParam,
) => {
  return ApiHelper.post<
    IResponse<IRecommendationOption>,
    RecommendationOptionParam
  >(RecommendationServiceRoute.index, body, opportunityID);
};

export const updateRecommendationOption = (
  id: number,
  body: RecommendationOptionParam,
) => {
  return ApiHelper.put<
    IResponse<IRecommendationOption>,
    RecommendationOptionParam
  >(RecommendationServiceRoute.index, body, id);
};

export const deleteRecommendationOption = (id: number) => {
  return ApiHelper.delete<IResponse<any>>(
    RecommendationServiceRoute.index,
    undefined,
    id,
  );
};

export const sendRecommendationSelected = (
  params: {
    opportunity_id: number;
    summary: any;
  },
  option_id: number,
) => {
  return ApiHelper.post<
    IResponse<IRecommendationOption>,
    RecommendationSelectedOptionParam
  >(
    RecommendationServiceRoute.opportunity_recommend_summary,
    params,
    option_id,
  );
};

export const addOptionItemFinding = (
  entity_id: number,
  entity_type: string,
  title: string,
  description: string,
) => {
  const body = {
    title,
    description,
    entity_id,
    entity_type,
  };
  return ApiHelper.post<IResponse<PlaybookItemFindingT>, typeof body>(
    RecommendationServiceRoute.finding,
    body,
  );
};

export const getListOptionFinding = (
  entity_id: number,
  entity_type: string,
) => {
  return ApiHelper.get<IListResponse<PlaybookItemFindingT>>(
    RecommendationServiceRoute.finding,
    undefined,
    [
      {
        key: 'entity_id',
        value: entity_id,
      },
      {
        key: 'entity_type',
        value: entity_type,
      },
    ],
  );
};

export const addFindingForOption = (
  id: number,
  params: { finding: number[] },
) => {
  return ApiHelper.post<IResponse<IRecommendationOption>, typeof params>(
    RecommendationServiceRoute.finding_for_option,
    params,
    id,
  );
};

export const deleteFindingForOption = (id: number, findingID: number) => {
  const bodyRequest = {
    finding: [findingID],
  };
  return ApiHelper.delete<IResponse<IRecommendationOption>>(
    RecommendationServiceRoute.finding_for_option,
    bodyRequest,
    id,
  );
};
