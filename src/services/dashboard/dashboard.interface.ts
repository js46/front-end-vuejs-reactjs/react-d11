interface ICommonObj {
  id: number;
  name: string;
}
export interface ICapacityData {
  id: number;
  sponsor_business_unit: ICommonObj;
  profile_role: ICommonObj;
  resource_team: ICommonObj;
}

export interface IDemandData {
  date: Date | string;
  profile_role: {
    name: string;
    id: number;
  };
  resource_team: {
    id: number;
    name: string;
  };
  sponsor_business_unit: {
    id: number;
    name: string;
  };
  duration: number;
  effort: number;
  effort_by_day: number;
  demand?: number;
}
