import { IResponse, IQueryParam } from '../common.services';
import { ICapacityData, IDemandData } from './dashboard.interface';
import { ApiHelper } from '../../utils';

class DashboardServiceRoute {
  static readonly DEMAND = '/dashboard/demand';
  static readonly CAPACITY = '/dashboard/capacity';
}
export const getDemandDashboard = async (query?: IQueryParam[]) => {
  return ApiHelper.get<IResponse<IDemandData[]>>(
    DashboardServiceRoute.DEMAND,
    undefined,
    query,
  );
};
export const getCapacityDashboard = async () => {
  return ApiHelper.get<IResponse<ICapacityData[]>>(
    DashboardServiceRoute.CAPACITY,
  );
};
