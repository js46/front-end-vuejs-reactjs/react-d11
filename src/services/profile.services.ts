import { IListResponse, IResponse } from './common.services';
import { ApiHelper } from '../utils';

class ProfileServiceRoute {
  static readonly USER_PROFILE = '/user-profiles';
  static readonly GET_ROLE_PROFILE = '/profile-roles?page=1&limit=100';
  static readonly GET_ALL_SKILLS = '/skills?page=1&limit=100';
  static readonly GET_LOCATIONS = '/locations';
  static readonly FAVORITE = '/favorites';
  static readonly COMMENT = '/comments';
  static readonly USER_CHANGE_PASSWORD = '/user-profile/change-password';
  static readonly UPDATE_CURRENT_PROFILE = '/user-profile/current-user';
}

export interface IResourceTeamProps {
  resource_team_id: number;
  resource_team_name: string;
}

export interface IProfileRole {
  id: number;
  profile_role_id: number;
  profile_role_name: string;
  proficiency_level_id: number;
}

export interface Favorite {
  id: number;
  user_id: number;
}
export interface ISkills {
  id?: number;
  skill_id: number;
  skill_name: string;
  proficiency_level_id: number;
  favorites: Favorite[] | null;
  isChecked?: boolean | null;
  type?: string;
  group_skill?: {
    id: number;
    title: string;
  };
}
export interface IAccomplishments {
  id: number;
  name?: string;
  date?: Date | string;
  favorites: Favorite[] | null;
}
export interface IBusinessUnit {
  business_unit_id: number;
  business_unit_name: string;
}
export interface IAward extends IAccomplishments {}
export interface IPublication extends IAccomplishments {}
export interface ICertification extends IAccomplishments {}
export interface IKnowledgeBase extends IAccomplishments {}

export interface IProfileData {
  id: number;
  email: string;
  name: string;
  casbin_role: string[];
  assigned_opportunity_count: number;
  created_opportunity_count: number;
  location: { id: number; name: string };
  business_unit: IBusinessUnit;
  resource_team: IResourceTeamProps;
  recommendation_count: number;
  profile_role: IProfileRole;
  career_mission?: string;
  skills?: ISkills[];
  accomplishments?: IAccomplishments[];
  rewards?: IAward[];
  publications?: IPublication[];
  certifications?: ICertification[];
  knowledgebase_assets?: IKnowledgeBase[];
  time: Date;
}

export interface UpdateProfileType {
  resource_team_id?: number;
  location_id?: number;
  profile_role?: { proficiency_level_id?: number; profile_role_id?: number };
  career_mission?: string;
  skills?: { skill_id: number; proficiency_level_id: number }[];
  accomplishments?: { id: number; name?: string }[];
  rewards?: { id: number; name?: string }[];
  publications?: { id: number; name?: string }[];
  certifications?: { id?: number; name?: string; date?: Date | string }[];
  knowledgebase_assets?: { id?: number; name?: string; date?: Date | string }[];
}

export interface IProficiencyProps {
  key_accomplishments?: IAccomplishments[];
  awards?: IAward[];
  publications?: IPublication[];
  professional_certifications?: ICertification[];
  knowledge_assets?: IKnowledgeBase[];
}

export interface ISkillsInRole {
  id: number;
  name: string;
  description: string;
  type: string;
  group_skill?: {
    id: number;
    title: string;
  };
}
export interface IListRole {
  id: number;
  name: string;
  description: string;
  // skills: ISkillsInRole[];
}

export interface UserLocation extends IListRole {}

export interface IListLocations {
  id: number;
  name: string;
  description: string;
}

interface ISkillsUpdateProfile {
  skill_id: number;
  proficiency_level_id: number;
}
interface IAccomplishmentsUpdateProfile {
  id: number;
  name: string;
}
interface IRewardsUpdateProfile extends IAccomplishmentsUpdateProfile {}
interface IPublicationsUpdateProfile extends IAccomplishmentsUpdateProfile {}
interface ICertificationUpdateProfile extends IAccomplishmentsUpdateProfile {
  date: Date;
}
interface IKnowledgeBaseUpdateProfile extends ICertificationUpdateProfile {}

interface IFavoriteData {
  entity_id: number;
  entity_type: string;
}

interface IResponseFavorite {
  id?: number;
  entity_id?: number;
  entity_type?: string;
  created_by?: {
    id?: number;
    name?: string;
  };
  created_at?: Date;
  last_updated_at?: Date;
  delflg?: boolean;
}

interface RecommendationComment {
  entity_id: number;
  entity_type: string;
  comment_type: string;
  comment_body: string;
  comment_parent_id: number;
}

interface RecommendationCommentResponse {
  id: number;
  entity_id: number;
  entity_type: string;
  comment_type: string;
  comment_body: string;
  comment_parent_id: number;
  private_flg: false;
  created_at: Date;
  created_by: { id: number; name: string };
  last_updated_at: Date;
  last_updated_by: { id: number; name: string };
}
interface IParamChangePassword {
  password: string;
  new_password: string;
}
export const getUserProfile = async (id: number) => {
  return ApiHelper.get<IResponse<IProfileData>>(
    ProfileServiceRoute.USER_PROFILE,
    id,
  );
};

export const getRoleProfile = async () => {
  return ApiHelper.get<IListResponse<IListLocations>>(
    ProfileServiceRoute.GET_ROLE_PROFILE,
  );
};

export const getLocations = async () => {
  return ApiHelper.get<IListResponse<UserLocation>>(
    ProfileServiceRoute.GET_LOCATIONS,
  );
};

export const getAllSkills = async () => {
  return ApiHelper.get<IListResponse<ISkillsInRole>>(
    ProfileServiceRoute.GET_ALL_SKILLS,
  );
};

export const updateUserProfile = async (
  id: number,
  profileData: UpdateProfileType,
) => {
  return ApiHelper.put<IResponse<IProfileData>, UpdateProfileType>(
    ProfileServiceRoute.USER_PROFILE,
    profileData,
    id,
  );
};
export const updateCurrentUserProfile = async (
  profileData: UpdateProfileType,
) => {
  return ApiHelper.put<IResponse<IProfileData>, UpdateProfileType>(
    ProfileServiceRoute.UPDATE_CURRENT_PROFILE,
    profileData,
  );
};
export const addFavorite = async (favoriteData: IFavoriteData) => {
  return ApiHelper.post<IResponse<IResponseFavorite>, IFavoriteData>(
    ProfileServiceRoute.FAVORITE,
    favoriteData,
  );
};

export const removeFavorite = async (id: number) => {
  return ApiHelper.delete<IResponse<IResponseFavorite>>(
    ProfileServiceRoute.FAVORITE,
    undefined,
    id,
  );
};

export const addRecomendationComment = async (
  params: RecommendationComment,
) => {
  return ApiHelper.post<
    IResponse<RecommendationCommentResponse>,
    RecommendationComment
  >(ProfileServiceRoute.COMMENT, params);
};

export const updateRecommendationComment = async (
  id: number,
  body: { comment_body: string },
) => {
  return ApiHelper.put<
    IResponse<RecommendationCommentResponse>,
    { comment_body: string }
  >(ProfileServiceRoute.COMMENT, body, id);
};
export const ChangePasswordUser = async (param: IParamChangePassword) => {
  return ApiHelper.put<IResponse<IProfileData>, IParamChangePassword>(
    ProfileServiceRoute.USER_CHANGE_PASSWORD,
    param,
  );
};
