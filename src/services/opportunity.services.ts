import { IResponse } from './common.services';
import {
  IBenefitFocusValue,
  IOpportunitySuccessProbability,
  IBusinessUnit,
} from './references.services';
import { ApiHelper } from '../utils';
import { ICorporateObjective, IBusinessProcesses } from './references.services';
import { NoteT } from './playbook.services';
import { OpportunityPhase } from '../constants/opportunityPhase';
import { ITag, IParamTag } from './finding/finding.interface';

class OpportunityServiceRoute {
  static readonly OPPORTUNITIES = '/opportunities';
  static readonly OPPORTUNITIES_TITLE = '/opportunity/title';
  static readonly OPPORTUNITIES_DESCRIPTION = '/opportunity/description';
  static readonly OPPORTUNITIES_HYPOTHESIS = '/opportunity/hypothesis';
  static readonly OPPORTUNITIES_REQ_COMPLETION_DATE =
    '/opportunity/request-completion-date';
  static readonly OPPORTUNITIES_BUSINESS_UNIT = '/opportunity/business-unit';
  static readonly OPPORTUNITIES_SCREEN = '/opportunity/screen';
  static readonly OPPORTUNITIES_COMPLETED_STEP = '/opportunity/completed-step';
  static readonly OPPORTUNITIES_BPI = '/opportunity/processes';
  static readonly REMOVE_OPPORTUNITIES_BPI = '/opportunity/process';
  static readonly OPPORTUNITIES_PRIORITY_LEVEL = '/opportunity/priority-level';
  static readonly OPPORTUNITIES_CORPORATE_OBJECTIVE =
    '/opportunity/corporate-objective';
  static readonly OPPORTUNITIES_CORPORATE_OBJECTIVES =
    '/opportunity/corporate-objectives';
  static readonly OPPORTUNITIES_BENEFIT_FOCUSES =
    '/opportunity/benefit-focuses';
  static readonly OPPORTUNITIES_BENEFIT_FOCUS = '/opportunity/benefit-focus';
  static readonly OPPORTUNITIES_BENEFIT_METRIC = '/opportunity/benefit-metric';
  static readonly OPPORTUNITIES_BENEFIT_METRICS =
    '/opportunity/benefit-metrics';
  static readonly OPPORTUNITIES_CURRENT_EXPERT =
    '/opportunity-action/assign-expert-for-review';
  static readonly OPPORTUNITIES_ALLOCATE_RESOURCE =
    '/opportunity-action/allocate-resources';
  static readonly OPPORTUNITIES_RESOURCE_ALLOCATION_OPTION =
    '/opportunity-action/finish-resource-allocation';
  static readonly OPPORTUNITIES_UPDATE_CURRENT_EXPERT =
    '/opportunity-action/update-expert-for-review';
  static readonly SET_RESOURCE_TEAM = '/opportunity/resource-team';
  static readonly BUSINESS_UNIT = '/business-units';
  static readonly BENEFIT_FOCUS = '/benefit-focus';
  static readonly ATTACHMENTS_ADD = '/attachments';
  static readonly ATTACHMENTS_DELETE = '/attachments';
  static readonly GET_OPPORTUNITY_PERMISSIONS = '/opportunity/permissions';
  static readonly UPDATE_OPPORTUNITY_TYPE = '/opportunity/type';
  static readonly UPDATE_OPPORTUNITY_SIZE = '/opportunity/size';
  static readonly UPDATE_OPPORTUNITY_PHASE = '/opportunity/phase';
  static readonly OPPORTUNITY_STATE = '/opportunity/state';
  static readonly OPPORTUNITY_ROLES = '/opportunity/role';
  static readonly OPPORTUNITY_UPDATE_PROBABILITY_SUCCESS =
    '/opportunity/probability-success';
  static readonly OPPORTUNITY_REVIEW_SUMMARY = '/opportunity/review-summary';
  static readonly OPPORTUNITY_TASK = '/task-opportunities';
  static readonly OPPORTUNITY_EXPERTS =
    '/expert-opportunities/experts-for-opportunity';
  static readonly UPDATE_OPPORTUNITY_EXPERT = '/expert-opportunities';
  static readonly ADD_OPPORTUNITIES_CURRENT_EXPERT =
    '/expert-opportunities/add-expert';
  static readonly REMOVE_OPPORTUNITIES_CURRENT_EXPERT = '/expert-opportunities';
  static readonly OPPORTUNITY_DEFINITION = '/opportunity/definition';
  static readonly OPPORTUNITY_PRIORITY_SCORE = '/opportunity/priority-score';
  static readonly OPPORTUNITY_ACTIVITY_LOG = '/activities-opportunity';
  static readonly OPPORTUNITY_SPONSOR_BUSINESS_UNIT =
    '/opportunity/sponsor-business-unit';

  static readonly OPPORTUNITY_PRIVATE_ON = '/opportunity/private';
  static readonly OPPORTUNITY_PRIVATE_OFF = '/opportunity/un-private';
  static readonly UPDATE_OPPORTUNITY_DETAIL = '/opportunity/detail';
  static readonly ADD_OPPORTUNITY_DETAIL_NOTE = '/opportunity/note-detail';
  static readonly UPDATE_OPPORTUNITY_DETAIL_NOTE = '/opportunity/update-note';
  static readonly DELETE_OPPORTUNITY_DETAIL_NOTE = '/opportunity/del-note';
  static readonly DATASET_REQUIRED = '/opportunity/dataset-required';
  static readonly UPDATE_PHASE_FOR_OPPORTUNITY_ =
    '/opportunity-action/force-update-phase';
  static readonly OPPORTUNITY_TAGS = '/opportunity/tags';
}

export interface IOpportunityState {
  id: number;
  name: string;
  display_name: string;
}
export interface IOpportunityPhase {
  id: number;
  name: string;
  display_name: string;
}
export interface OpportunityType extends IOpportunityState {}
export interface ResourceTeam extends IOpportunityState {}
export interface OpportunityCreatedPerson {
  id?: number;
  name?: string;
  email?: string;
  business_unit?: string;
  profile_id?: number;
  profile_name?: string;
}
export interface IParamNote {
  title: string;
  content: string;
  user_note_id?: number;
}
export interface ICurrentExpertReview {
  expert_review?: {
    id: number;
    name: string;
  };
  expert_review_assign_date?: Date;
  expert_review_unassign_date?: Date;
}
export interface IOpportunityPriorityScore {
  priority_level?: string;
  priority_score?: number;
  priority_score_updated_log?: string;
  priority_score_updated_at?: string;
}
export interface IOpportunity {
  id: number;
  proposer_email: string;
  proposer_business_unit: string;
  title: string;
  detail: string;
  description?: string;
  definition?: string;
  hypothesis?: string;
  opportunity_phase_id?: IOpportunityPhase;
  opp_priority_score?: IOpportunityPriorityScore;
  requested_completion_date: string;
  estimated_completion_date: string;
  completion_date?: string;
  capture_submit_date?: Date;
  screen_id: number;
  completed_step: number;
  comment_count?: number;
  favorite_count?: number;
  favorites?: { id: number; user_id: number }[];
  opportunity_type?: OpportunityType;
  opportunity_size?: IOpportunitySizes;
  opportunity_roles?: IOpportunityInRole[];
  opportunity_state?: IOpportunityState;
  opportunity_phase?: IOpportunityPhase;
  resource_team?: ResourceTeam;
  business_units: IBusinessUnit[];
  sponsor_business_unit: IBusinessUnit;
  processes: IBusinessProcesses[];
  probability_success: IOpportunitySuccessProbability;
  current_experts?: IOpportunityCurrentExpert[];
  current_expert_review?: ICurrentExpertReview;
  opportunity_dataset?: IOpportunityDataset[];
  tags?: {
    id: number;
    name: string;
  }[];
  tasks?: { id: number; task_state: string; task_type: string }[];
  notes?: NoteT[];
  corporate_objectives: ICorporateObjective[];
  benefit_focuses: IOpportunityBenefitFocus[];
  benefit_metrics: string[];
  review_summary: IOpportunityReviewSummary;
  created_at?: string;
  created_by?: OpportunityCreatedPerson;
  last_updated_at?: Date;
  last_updated_by: { id: number; name: string };
  private_flag?: boolean;
  del_flag?: boolean;
  recommend_summary?: {
    id: number;
    content: string;
  };
  recommend_option?: {
    id: number;
    title: string;
  };
  data_sets?: {
    id: number;
    name: string;
  }[];
}

export interface IOpportunityBenefitFocus {
  id: number;
  benefit_focus_id: number;
  benefit_focus_name: string;
  benefit_focus_value: IBenefitFocusValue;
}

export interface ICreatedBy {
  user_id: number;
  full_name: string;
}

export interface IAttachments {
  id: number;
  attachments: any;
  entity_id?: number;
  description: string;
}

export interface IOpportunityParam {
  proposer_email?: string;
  proposer_business_unit?: string;
  screen_id?: number;
  completed_step?: number;
  business_units?: number[];
  title?: string;
  description?: string;
  hypothesis?: string;
  priority_level?: string;
  processes?: number[];
  experts?: number[];
  labels?: number[];
  opportunity_state_id?: number;
  opportunity_phase_id?: number;
  ranking_score?: number;
  resource_team_id?: number;
  corporate_objectives?: number[];
  requested_completion_date?: string;
  benefit_metrics?: string[];
  benefit_focuses?:
    | {
        benefit_focus_id: number;
        benefit_focus_value_id: number;
      }[]
    | null;
  comment?: string;
  opportunity_type_id?: number;
  opportunity_size_id?: number;
  probability_success_id?: number;
  sponsor_business_unit_id?: number;
  private_flag?: boolean;
  opportunity_dataset?: number[];
}

interface TaskNote {
  id?: number;
  comment_type?: string;
  comment_body?: string;
  created_at?: Date;
  created_by?: { id?: number; name?: string };
}
export interface ICustomParam {
  screen_id: number;
  completed_step?: number;
  opportunity_state_id?: number;
  opportunity_phase_id?: number;
  opportunity_prev_phase_id?: number;
  capture_submit_date?: string;
}

export interface IReviewSummaryParam {
  review_summary: {
    review_content: string;
  };
}

export interface OpportunityTask {
  id?: number;
  opportunity?: { id: number; phase: string; title: string };
  task_type?: string;
  task_state?: string;
  task_result?: string;
  machine_note?: string;
  task_request_note?: TaskNote;
  screen_id?: number;
  completed_step?: number;
  created_at?: Date;
  time?: Date;
  created_by_user?: {
    id?: number;
    name?: string;
    email?: string;
    business_unit_name?: string;
    profile_role_name?: string;
  };
  assignee_user?: { id?: number; name?: string; email?: string };
  last_updated_at?: Date;
  updated_user?: { id?: number; name?: string; email?: string };
}

export interface IOpportunityTypes {
  id?: number;
  name?: string;
  description?: string;
  complexity?: number;
  point?: number;
  weighting?: number;
  archive?: boolean;
}

export interface IOpportunitySizes {
  id?: number;
  t_shirt_size?: string;
  duration?: number;
  points?: number;
  weighting?: number;
}

interface IOpportunityRoleSkills {
  skill_id?: number;
  proficiency_level_id?: number;
}

export interface IOpportunitySkillsInRole {
  skill_id?: number;
  proficiency_level_id?: number;
  id: number;
  skill_name: string;
}

export interface IOpportunityRoles {
  id?: number;
  profile_role_id?: number;
  role_name: string;
  profile_role_level_id?: number;
  day?: number;
  effort?: number;
  location_id?: number;
  resource_team_id?: number;
  opportunity_role_skills?: IOpportunityRoleSkills[];
}

export interface IOpportunityInRole {
  day: number;
  id?: number;
  effort: number;
  location?: { id: number; name: string; description?: string };
  profile_role?: { id: number; name: string };
  profile_role_level_id?: number;
  resource_team?: { id: number; name: string };
  role_name: string;
  skills?: IOpportunitySkillsInRole[];
}

export interface IOpportunityCurrentExpert {
  id: number;
  user_id: number;
  name: string;
  assignment_date: Date;
  un_assignment_date: Date;
  opportunity_team_role: {
    id: number;
    role_name: string;
  };
  resource_team?: {
    id: number;
    name: string;
  };
  duration: number;
  effort: number;
}

export interface IOpportunityForExpertReviewParams {
  data_source_required: string[];
  dataSourceRequired: {
    id: number;
    choses: {
      key: number;
      name: string;
    }[];
    label: string;
  }[];
  opportunity_type?: number;
  opportunity_size?: number;
  probability_success: number;
  opportunity_roles: IOpportunityInRole[];
  review_summary: string;
}

export interface IOpportunityReviewSummary {
  id: number;
  review_content: string;
  created_by_user: {
    id?: number;
    name?: string;
  };
  updated_by_user: {
    id?: number;
    name?: string;
  };
}
export interface IParamOpportunityAction {
  action_id: number;
}
export interface IPermission {
  id: number;
  casbin_name: string;
  display_name: string;
  action_id: number;
  path: string;
  method: string;
  phase: string;
  state: string;
}
export interface IOpportunityPermission {
  phase: 'phase' | 'analyse';
  permissions: IPermission[];
}

export interface IOpportunityDataset {
  id: number;
  name: string;
  description?: string;
  manager: {
    id: number;
    full_name: string;
  };
  access_url: string;
  current_opportunities?: any;
}

interface AddCurrentExpertType {
  action_id: number;
  expert: {
    user_id: number;
    assignment_date: Date;
    // team_role_name: string;
    // day: number;
  };
  activity_message: string;
}

export interface AttachmentResponse {
  id: number;
  attachment_name: string;
  client_file_name: string;
  description: string;
  storage_prefix?: string;
  storage_type?: string;
  entity_type: string;
  del_flg: boolean;
  time?: Date;
  entity_id?: number;
  attachment_extension?: string;
  created_by_user: { name: string; id: number };
  opportunity_id?: number;
  opportunity_phase?: { id: number; name: string; display_name: string };
  highlight?: any;
}

export interface OpportunityExpertT {
  id: number;
  assignment_date: Date;
  delete_flg: boolean;
  duration: number;
  expert?: {
    id: number;
    name: string;
    resource_team: { id: number; name: string };
  };
  opportunity: {
    id: number;
    title: string;
    business_units?: { id: number; name: string }[];
    created_by: {
      id: number;
      name: string;
    };
    priority_level?: string;
    requested_completion_date?: Date;
    sponsor_business_unit: IBusinessUnitInExpertOpportunity;
  };
  opportunity_phase: {
    id: number;
    name: string;
    display_name: string;
  };
  opportunity_team_role: {
    id: number;
    name: string;
  };
  opportunity_size: {
    id: number;
    name: string;
  };
  time: Date;
  un_assignment_date: Date;
}

interface AddCurrentExpertInTeam {
  opportunity_id: number;
  assignment_date?: string;
  user_id: number;
  opportunity_team_role_id?: number;
  duration?: number;
  effort?: number;
}

export interface IBusinessUnitInExpertOpportunity {
  id: number;
  name: string;
}

export interface IOpportunityExpert {
  _doc_count?: number;
  id: number;
  opportunity: {
    id: number;
    title: string;
    created_by: {
      id: number;
      name: string;
    };
    business_units: IBusinessUnitInExpertOpportunity[];
    sponsor_business_unit: IBusinessUnitInExpertOpportunity;
  };
  expert: {
    id: number;
    name: string;
    resource_team: {
      id: number;
      name: string;
    };
  };
  duration: number;
  assignment_date: Date;
  un_assignment_date: Date;
  delete_flg: boolean;
  opportunity_team_role: {
    id: number;
    name: string;
  };
  effort: number;
}

interface IAddCorporateObjective {
  name: string;
  description?: string;
  priority?: number;
  weighting?: number;
  points?: number;
  metrics?: string[];
}
export enum IOpportunityLogActions {
  Endorse = 'endorse',
}
export interface IOpportunityActivityLog {
  id: number;
  opportunity?: {
    id: number;
    title: string;
  };
  user?: {
    id: number;
    name: string;
  };
  path: string;
  activity_definition: {
    id: number;
    title: string;
  };
  method: string;
  http_code_result: number;
  activity_message?: string;
  date: string;
}

export interface UpdateExpertReviewerT {
  expert: { user_id: number };
}

export const createOpportunity = async (params: IOpportunityParam) => {
  return ApiHelper.post<IResponse<IOpportunity>, IOpportunityParam>(
    OpportunityServiceRoute.OPPORTUNITIES,
    params,
  );
};

export const updateOpportunity = async (
  params: IOpportunityParam,
  id: number,
) => {
  return ApiHelper.put<IResponse<IOpportunity>, IOpportunityParam>(
    OpportunityServiceRoute.OPPORTUNITIES,
    params,
    id,
  );
};

export const submitOpportunity = async (params: ICustomParam, id: number) => {
  return ApiHelper.put<IResponse<IOpportunity>, ICustomParam>(
    OpportunityServiceRoute.OPPORTUNITIES,
    params,
    id,
  );
};

export const deleteOpportunity = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunity>, undefined>(
    OpportunityServiceRoute.OPPORTUNITIES,
    undefined,
    id,
  );
};

export const updateScreenId = async (params: IOpportunityParam, id: number) => {
  const bodyRequest = { screen_id: params.screen_id };
  return ApiHelper.put<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_SCREEN,
    bodyRequest,
    id,
  );
};

export const updateCompletedStep = async (
  params: IOpportunityParam,
  id: number,
) => {
  const bodyRequest = { completed_step: params.completed_step };
  return ApiHelper.put<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_COMPLETED_STEP,
    bodyRequest,
    id,
  );
};

export const updateOpportunityTitle = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = { title: params.title };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_TITLE,
    dataRequest,
    id,
  );
};

export const updateOpportunityBusinessUnit = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = {
    business_units: params.business_units && [
      params.business_units.reverse()[0],
    ],
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BUSINESS_UNIT,
    dataRequest,
    id,
  );
};

export const removeOpportunityBusinessUnit = async (
  params: number[],
  id: number,
) => {
  const dataRequest = { business_units: params };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BUSINESS_UNIT,
    dataRequest,
    id,
  );
};

export const addOpportunityCurrentExperts = async (
  id: number,
  params: AddCurrentExpertType,
) => {
  return ApiHelper.post<IResponse<IOpportunity>, AddCurrentExpertType>(
    OpportunityServiceRoute.OPPORTUNITIES_CURRENT_EXPERT,
    params,
    id,
  );
};

export const updateOpportunityCurrentExpert = async (
  id: number,
  params: UpdateExpertReviewerT,
) => {
  return ApiHelper.put<IResponse<IOpportunity>, UpdateExpertReviewerT>(
    OpportunityServiceRoute.OPPORTUNITIES_UPDATE_CURRENT_EXPERT,
    params,
    id,
  );
};

export const addOpportunityCurrentExpertsInTeam = async (
  params: AddCurrentExpertInTeam,
) => {
  return ApiHelper.post<IResponse<IOpportunityExpert>, AddCurrentExpertInTeam>(
    OpportunityServiceRoute.ADD_OPPORTUNITIES_CURRENT_EXPERT,
    params,
  );
};

export const deleteOpportunityCurrentExpertsInTeam = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunityExpert>>(
    `${OpportunityServiceRoute.REMOVE_OPPORTUNITIES_CURRENT_EXPERT}/${id}`,
  );
};

export const updateOpportunityDescription = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = { description: params.description };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_DESCRIPTION,
    dataRequest,
    id,
  );
};

export const updateOpportunityHypothesis = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = { hypothesis: params.hypothesis };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_HYPOTHESIS,
    dataRequest,
    id,
  );
};

export const updateOpportunityBPI = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = { processes: params.processes };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BPI,
    dataRequest,
    id,
  );
};

export const removeOpportunityBPI = async (params: number[], id: number) => {
  const dataRequest = { processes: params };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.REMOVE_OPPORTUNITIES_BPI,
    dataRequest,
    id,
  );
};

export const updateOpportunityReqCompleteDate = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = {
    requested_completion_date: params.requested_completion_date,
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_REQ_COMPLETION_DATE,
    dataRequest,
    id,
  );
};

export const updateOpportunityPriorityLevel = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = { priority_level: params.priority_level };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_PRIORITY_LEVEL,
    dataRequest,
    id,
  );
};

export const updateOpportunityCorpObj = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = {
    corporate_objectives: params.corporate_objectives?.filter(item => item),
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_CORPORATE_OBJECTIVES,
    dataRequest,
    id,
  );
};

export const addOpportunityCorpObj = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = {
    corporate_objectives: params.corporate_objectives?.filter(item => item),
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_CORPORATE_OBJECTIVE,
    dataRequest,
    id,
  );
};

export const removeOpportunityCorpObj = async (
  corporateOnjId: number[],
  id: number,
) => {
  const dataRequest = {
    corporate_objectives: corporateOnjId,
  };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_CORPORATE_OBJECTIVE,
    dataRequest,
    id,
  );
};

export const updateOpportunityBenefitFocus = async (
  params: IOpportunityParam,
  id: number,
) => {
  const dataRequest = {
    benefit_focuses: params.benefit_focuses?.filter(
      item => item.benefit_focus_id,
    ),
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BENEFIT_FOCUSES,
    dataRequest,
    id,
  );
};

export const addOpportunityBenefitFocus = async (
  params: IOpportunityParam,
  id: number,
) => {
  const addedBenefitFocus = params.benefit_focuses?.filter(
    item => item.benefit_focus_id || item.benefit_focus_value_id,
  );
  const reqBenefitFocus = {
    benefit_focuses: addedBenefitFocus && [
      addedBenefitFocus[addedBenefitFocus.length - 1],
    ],
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof reqBenefitFocus>(
    OpportunityServiceRoute.OPPORTUNITIES_BENEFIT_FOCUS,
    reqBenefitFocus,
    id,
  );
};

export const removeOpportunityBenefitFocus = async (
  benefitFocusId: { id: number }[],
  id: number,
) => {
  const bodyRequest = {
    benefit_focuses: benefitFocusId,
  };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BENEFIT_FOCUS,
    bodyRequest,
    id,
  );
};

export const updateOpportunityBenefitMetric = async (
  params: IOpportunityParam,
  id: number,
) => {
  const bodyRequest = { benefit_metrics: params.benefit_metrics };
  return ApiHelper.put<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BENEFIT_METRICS,
    bodyRequest,
    id,
  );
};

export const addOpportunityBenefitMetric = async (
  params: IOpportunityParam,
  id: number,
) => {
  const bodyRequest = {
    benefit_metrics: [
      params.benefit_metrics?.[params.benefit_metrics.length - 1],
    ],
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BENEFIT_METRIC,
    bodyRequest,
    id,
  );
};

export const removeOpportunityBenefitMetrics = async (
  benefitMetric: string[],
  id: number,
) => {
  const bodyRequest = {
    benefit_metrics: benefitMetric,
  };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITIES_BENEFIT_METRIC,
    bodyRequest,
    id,
  );
};

export const getAnOpportunity = async (id: number) => {
  return ApiHelper.get<IResponse<IOpportunity>>(
    OpportunityServiceRoute.OPPORTUNITIES,
    id,
  );
};

export const addAttachments = async (body: FormData) => {
  const headerConfig = {
    headers: {
      'content-type': 'multipart/form-data',
    },
  };
  return ApiHelper.post<IResponse<AttachmentResponse>, typeof body>(
    OpportunityServiceRoute.ATTACHMENTS_ADD,
    body,
    undefined,
    headerConfig,
  );
};

export const updateAttachment = async (
  id: number,
  body: { description: string },
) => {
  return ApiHelper.put<IResponse<AttachmentResponse>, { description: string }>(
    OpportunityServiceRoute.ATTACHMENTS_ADD,
    body,
    id,
  );
};

export const deleteAttachments = async (id: number) => {
  return ApiHelper.delete<IResponse<AttachmentResponse>, undefined>(
    OpportunityServiceRoute.ATTACHMENTS_DELETE,
    undefined,
    id,
  );
};

export const getOpportunityTask = async (id: number) => {
  return ApiHelper.get<IResponse<OpportunityTask>>(
    OpportunityServiceRoute.OPPORTUNITY_TASK,
    id,
  );
};

export const updateOpportunityTask = async (
  id: number,
  state: { task_state: string },
) => {
  return ApiHelper.put<IResponse<OpportunityTask>, typeof state>(
    OpportunityServiceRoute.OPPORTUNITY_TASK,
    state,
    id,
  );
};

export const updateOpportunityState = async (
  id: number,
  params: { opportunity_state_id: number },
) => {
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_STATE,
    params,
    id,
  );
};

export const addOpportunityRole = async (
  id: number,
  params: IOpportunityRoles,
) => {
  const dataRequest = {
    opportunity_roles: [params],
  };
  return ApiHelper.post<IResponse<IOpportunity>, typeof dataRequest>(
    OpportunityServiceRoute.OPPORTUNITY_ROLES,
    dataRequest,
    id,
  );
};

export const deleteOpportunityRole = async (
  id: number,
  values: { id: number },
) => {
  const params = {
    opportunity_roles: [values],
  };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_ROLES,
    params,
    id,
  );
};

export const updateOpportunityProbabilitySuccess = async (
  id: number,
  probability_success_id: number,
) => {
  const params = { probability_success_id };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_UPDATE_PROBABILITY_SUCCESS,
    params,
    id,
  );
};

export const updateOpportunitySize = async (
  id: number,
  opportunity_size_id: number,
) => {
  const params = { opportunity_size_id };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.UPDATE_OPPORTUNITY_SIZE,
    params,
    id,
  );
};

export const updateOpportunityType = async (
  id: number,
  opportunity_type_id: number,
) => {
  const params = { opportunity_type_id };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.UPDATE_OPPORTUNITY_TYPE,
    params,
    id,
  );
};

export const updateOpportunityRoles = async (
  id: number,
  params: IOpportunityRoles,
) => {
  const bodyRequest = {
    opportunity_roles: [params],
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.OPPORTUNITY_ROLES,
    bodyRequest,
    id,
  );
};

export const updateResourceTeam = async (
  id: number,
  params: { resource_team_id: number },
) => {
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.SET_RESOURCE_TEAM,
    params,
    id,
  );
};

export const addOpportunityReviewSummary = async (
  idOpportunity: number,
  content: string,
) => {
  const params: IReviewSummaryParam = {
    review_summary: {
      review_content: content,
    },
  };
  return ApiHelper.post<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_REVIEW_SUMMARY,
    params,
    idOpportunity,
  );
};

export const updateOpportunityReviewSummary = async (
  idOpportunity: number,
  content: string,
) => {
  const params: IReviewSummaryParam = {
    review_summary: {
      review_content: content,
    },
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_REVIEW_SUMMARY,
    params,
    idOpportunity,
  );
};

export const getOpportunityPermissions = async (id: number) => {
  return ApiHelper.get<IResponse<IOpportunityPermission[]>>(
    OpportunityServiceRoute.GET_OPPORTUNITY_PERMISSIONS,
    id,
  );
};

export const getExpertsOpportunity = async (id: number) => {
  return ApiHelper.get<IResponse<IOpportunityExpert[]>>(
    OpportunityServiceRoute.OPPORTUNITY_EXPERTS,
    id,
  );
};

export const updateExpertOpportunity = async (
  id: number,
  params: { assignment_date?: string; duration?: number; effort?: number },
) => {
  return ApiHelper.put<IResponse<IOpportunityExpert>, typeof params>(
    OpportunityServiceRoute.UPDATE_OPPORTUNITY_EXPERT,
    params,
    id,
  );
};

export const updateOpportunitySponsorBusinessUnit = async (
  businessUnit: number,
  id: number,
) => {
  const params = {
    sponsor_business_unit_id: businessUnit,
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_SPONSOR_BUSINESS_UNIT,
    params,
    id,
  );
};

export const updateOpportunityDefinition = async (
  id: number,
  content: string,
) => {
  const params = {
    definition: content,
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.OPPORTUNITY_DEFINITION,
    params,
    id,
  );
};

export const updateOpportunityPhase = async (id: number, phaseID: number) => {
  const params = {
    opportunity_phase_id: phaseID,
  };
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.UPDATE_OPPORTUNITY_PHASE,
    params,
    id,
  );
};

export const getOpportunityActivityLogs = async (
  id: number,
  action: IOpportunityLogActions,
  phase?: OpportunityPhase,
) => {
  const query = phase
    ? [
        {
          key: 'opportunity_phase_id',
          value: phase,
        },
      ]
    : undefined;
  return ApiHelper.get<IResponse<IOpportunityActivityLog[]>>(
    OpportunityServiceRoute.OPPORTUNITY_ACTIVITY_LOG + `/${action}`,
    id,
    query,
  );
};

export const setOpportunityPrivate = async (id: number, flag?: boolean) => {
  return ApiHelper.put<IResponse<IOpportunity>, undefined>(
    flag
      ? OpportunityServiceRoute.OPPORTUNITY_PRIVATE_ON
      : OpportunityServiceRoute.OPPORTUNITY_PRIVATE_OFF,
    undefined,
    id,
  );
};

export const updateOpportunityDetail = async (
  payload: {
    detail: any;
  },
  oppID: number,
) => {
  return ApiHelper.put<IResponse<IOpportunity>, typeof payload>(
    OpportunityServiceRoute.UPDATE_OPPORTUNITY_DETAIL,
    payload,
    oppID,
  );
};
export const addOpportunityDetailNote = async (
  payload: IParamNote,
  oppID: number,
) => {
  return ApiHelper.post<IResponse<IOpportunity>, typeof payload>(
    OpportunityServiceRoute.ADD_OPPORTUNITY_DETAIL_NOTE,
    payload,
    oppID,
  );
};
export const updateOpportunityDetailNote = async (
  payload: IParamNote,
  noteID: number,
) => {
  return ApiHelper.put<IResponse<IOpportunity>, typeof payload>(
    OpportunityServiceRoute.UPDATE_OPPORTUNITY_DETAIL_NOTE,
    payload,
    noteID,
  );
};
export const deleteOpportunityDetailNote = async (noteID: number) => {
  return ApiHelper.delete<IResponse<IOpportunity>, undefined>(
    OpportunityServiceRoute.DELETE_OPPORTUNITY_DETAIL_NOTE,
    undefined,
    noteID,
  );
};
export const updateOpportunityAllocateResource = async (
  id: number,
  params: IParamOpportunityAction,
) => {
  return ApiHelper.post<IResponse<IOpportunity>, IParamOpportunityAction>(
    OpportunityServiceRoute.OPPORTUNITIES_ALLOCATE_RESOURCE,
    params,
    id,
  );
};
export const updateOpportunityReourceAllocationOption = async (
  id: number,
  params: IParamOpportunityAction,
) => {
  return ApiHelper.post<IResponse<IOpportunity>, IParamOpportunityAction>(
    OpportunityServiceRoute.OPPORTUNITIES_RESOURCE_ALLOCATION_OPTION,
    params,
    id,
  );
};
export const addDatasetInOpportunity = async (
  id: number,
  params: {
    data_sets: number[];
  },
) => {
  return ApiHelper.post<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.DATASET_REQUIRED,
    params,
    id,
  );
};
export const updateDatasetInOpportunity = async (
  id: number,
  params: {
    data_sets: number[];
  },
) => {
  return ApiHelper.put<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.DATASET_REQUIRED,
    params,
    id,
  );
};

export const removeDatasetInOpportunity = async (
  id: number,
  dataSetID: number[],
) => {
  const bodyRequest = {
    data_sets: dataSetID,
  };
  return ApiHelper.delete<IResponse<IOpportunity>, typeof bodyRequest>(
    OpportunityServiceRoute.DATASET_REQUIRED,
    bodyRequest,
    id,
  );
};

export const updatePhaseForOpportunity = async (
  id: number,
  params: {
    action_id: number;
  },
) => {
  return ApiHelper.post<IResponse<IOpportunity>, typeof params>(
    OpportunityServiceRoute.UPDATE_PHASE_FOR_OPPORTUNITY_,
    params,
    id,
  );
};

export const updateOpportunityTag = async (id: number, payload: IParamTag) => {
  return ApiHelper.put<IResponse<ITag>, IParamTag>(
    OpportunityServiceRoute.OPPORTUNITY_TAGS,
    payload,
    id,
  );
};
