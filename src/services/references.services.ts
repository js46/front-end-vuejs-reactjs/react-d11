import { IListResponse, IResponse, IQueryParam } from './common.services';
import { ApiHelper } from '../utils';

class ReferenceServiceRoute {
  static readonly CORPORATE_OBJECTIVES = '/corporate-objectives';
  static readonly BUSINESS_PROCESSES = '/business-processes';
  static readonly GET_RESOURCE_TEAM = '/resource-teams?page=1&limit=100';
  static readonly RESOURCE_TEAM = '/resource-teams';
  static readonly BUSINESS_UNIT = '/business-units';
  static readonly GET_BUSINESS_UNIT = '/business-units?page=1&limit=100';
  static readonly OPPORTUNITY_SIZE = '/size-opportunities';
  static readonly GET_OPPORTUNITY_SIZE = '/size-opportunities?page=1&limit=100';
  static readonly BENEFIT_FOCUS_CATEGORY = '/benefit-focus';
  static readonly GET_BENEFIT_FOCUS_CATEGORY =
    '/benefit-focus?page=1&limit=100';
  static readonly BENEFIT_FOCUS_VALUE = '/benefit-focus-value';
  static readonly GET_BENEFIT_FOCUS_VALUE =
    '/benefit-focus-value?page=1&limit=100';
  static readonly OPPORTUNITY_TYPE = '/type-opportunities';
  static readonly GET_OPPORTUNITY_TYPE = '/type-opportunities?page=1&limit=100';
  static readonly PRIORITY_CATEGORY_WEIGHTING = '/priority-category-weighting';
  static readonly GET_SKILL_GROUPS = '/group-skills?page=1&limit=100';
  static readonly SKILL_GROUP = '/group-skills';
  static readonly GET_EXPERT_SKILLS = '/skills?page=1&limit=100';
  static readonly EXPERT_SKILL = '/skills';
  static readonly PRIORITY_CATEGORIES = '/priority-category';
  static readonly PRIORITY_LABEL = '/priority-label';
  static readonly OPP_SUCCESS_PROBABILITY = '/opp-success-probability';
  static readonly EXPERT_ROLES = '/profile-roles';
  static readonly GET_PROFICIENCY_LEVEL = '/proficiency-level?page=1&limit=100';
  static readonly PROFICIENCY_LEVEL = '/proficiency-level';
  static readonly DATASETS = '/datasets';
  static readonly TAGS = '/tags';
  static readonly LIST_TYPE = '/list-type';
}
export interface IBusinessUnit {
  id: number;
  name: string;
  description?: string;
  resource_team?: IResourceTeam;
  business_unit_parent_id?: number;
  archive?: boolean;
  has_opportunity?: boolean;
}
export interface IBusinessProcesses {
  id: number;
  name: string;
  description?: string;
  business_process_parent_id?: number;
  archive?: boolean;
  has_opportunity?: boolean;
}

interface AddBusinessProcess {
  name?: string;
  description?: string;
  business_process_parent_id?: number;
}
export interface ICorporateObjective {
  id: number;
  name: string;
  description?: string;
  points?: number;
  archive: boolean;
  has_opportunity?: boolean;
  //   priority?: number;
  //   weighting?: number;
  //   metrics?: string[];
}

export interface IBusinessUnit {
  id: number;
  name: string;
  description?: string;
  resource_team?: IResourceTeam;
  business_unit_parent_id?: number;
  archive?: boolean;
  has_opportunity?: boolean;
}

interface IAddCorporateObjective {
  id?: number;
  name: string;
  description?: string;
  points?: number;
  archive?: boolean;
}

export interface IResourceTeam {
  id: number;
  name: string;
  description: string;
}
export interface IParamsResourceTeam {
  name: string;
  description: string;
}
export interface IOpportunityTypes {
  id: number;
  name: string;
  description?: string;
  points?: number;
  archive?: boolean;
}
export interface IBenefitFocusCategory {
  id: number;
  name: string;
  description?: string;
  points?: number;
  archive?: boolean;
}
export interface IParamBenefitFocusCategory {
  id?: number;
  name: string;
  description?: string;
  points?: number;
  archive?: boolean;
}
export interface IBenefitFocusValue {
  id: number;
  band: string;
  nominal_value: number;
  points: number;
}
export interface IParamBenefitFocusValue {
  id?: number;
  band: string;
  nominal_value?: number;
  points?: number;
}
export interface IOpportunitySizes {
  id?: number;
  t_shirt_size?: string;
  duration?: number;
  points?: number;
  weighting?: number;
}

interface IParamBusinessUnit {
  id?: number;
  name: string;
  description?: string;
  business_unit_parent_id?: number;
  archive?: boolean;
  resource_team_id?: number;
}
interface IParamOpportunitySize {
  id?: number;
  t_shirt_size?: string;
  duration?: number;
  points?: number;
  weighting?: number;
}
interface IParamOpportunityType {
  id?: number;
  name?: string;
  description?: string;
  complexity?: number;
  point?: number;
  weighting?: number;
}

export interface IPriorityCategoryWeighting {
  id: number;
  weighting: number;
  category: {
    id: number;
    category_name: string;
  };
}

export interface IParamsPriorityCategoryWeighting {
  weighting: number;
  category_id: number;
}

export interface IPriorityCategories {
  id: number;
  category_name: string;
}

export interface IPriorityLabel {
  id: number;
  label: string;
  score_from: number;
  score_to: number;
  description: string;
}
export interface IParamsPriorityLabel {
  label: string;
  score_from: number;
  score_to: number;
  description: string;
}

export interface IOpportunitySuccessProbability {
  id: number;
  band: string;
  description: string;
  points: number;
}
export interface IParamsOpportunitySuccessProbability {
  band: string;
  description: string;
  points: number;
}

export interface IExpertRoles {
  id: number;
  name: string;
  description: string;
}

export interface IParamsExpertRole {
  name?: string;
  description?: string;
}
export interface IGroupSkill {
  id: number;
  name: string;
  description?: string;
}
export interface IParamsGroupSkill {
  id?: number;
  name: string;
  description?: string;
}
export interface IExpertSkill {
  id?: number;
  name: string;
  description?: string;
  group_skill?: IGroupSkill;
}
export interface IParamsExpertSkill {
  id?: number;
  name: string;
  description?: string;
  group_skill_id?: number;
}
export interface IProficiencyLevel {
  id: number;
  name: string;
  description?: string;
}
export interface IParamsProficiencyLevel {
  id?: number;
  name?: string;
  description?: string;
}
export interface IParamsExpertSkill {
  id?: number;
  name: string;
  description?: string;
}

export interface ICurrentOppInDataset {
  id: number;
  title: string;
  opportunity_phase: {
    id: number;
    name: string;
    display_name: string;
  };
  date: string;
  priority_score: number;
  priority_level: string;
  created_by_user: { id: number; name: string };
}

export interface IDataSet {
  id: number;
  name: string;
  description?: string;
  manager: { id: number; full_name: string };
  access_url: string;
  current_opportunities: ICurrentOppInDataset[];
}
export interface ITag {
  id?: number;
  name: string;
  description?: string;
}

export interface IParamsDataset {
  name: string;
  description?: string;
  manager_id?: number;
  current_opportunities?: number[];
}

export interface IListType {
  id?: number;
  name: string;
  description?: string;
}

// Start Business Process
export const getBusinessProcesses = async () => {
  return ApiHelper.get<IListResponse<IBusinessProcesses>>(
    ReferenceServiceRoute.BUSINESS_PROCESSES,
  );
};

export const addBusinessProcesses = async (params: AddBusinessProcess) => {
  return ApiHelper.post<IResponse<IBusinessProcesses>, AddBusinessProcess>(
    ReferenceServiceRoute.BUSINESS_PROCESSES,
    params,
  );
};

export const updateBusinessProcesses = async (
  id: number,
  params: AddBusinessProcess,
) => {
  return ApiHelper.put<IResponse<IBusinessProcesses>, AddBusinessProcess>(
    ReferenceServiceRoute.BUSINESS_PROCESSES,
    params,
    id,
  );
};
export const deleteBusinessProcesses = async (id: number) => {
  return ApiHelper.delete<IResponse<IBusinessProcesses>, undefined>(
    ReferenceServiceRoute.BUSINESS_PROCESSES,
    undefined,
    id,
  );
};
// End  Business Process

// Start Corporate Objectives
export const getCorporateObjectives = async () => {
  return ApiHelper.get<IListResponse<ICorporateObjective>>(
    ReferenceServiceRoute.CORPORATE_OBJECTIVES,
  );
};

export const deleteCorporateObjectives = async (id: number) => {
  return ApiHelper.delete<IResponse<ICorporateObjective>, undefined>(
    ReferenceServiceRoute.CORPORATE_OBJECTIVES,
    undefined,
    id,
  );
};

export const addCorporateObjective = async (params: IAddCorporateObjective) => {
  return ApiHelper.post<IResponse<ICorporateObjective>, IAddCorporateObjective>(
    ReferenceServiceRoute.CORPORATE_OBJECTIVES,
    params,
  );
};

export const updateCorporateObjective = async (
  id: number,
  params: IAddCorporateObjective,
) => {
  return ApiHelper.put<IResponse<ICorporateObjective>, IAddCorporateObjective>(
    ReferenceServiceRoute.CORPORATE_OBJECTIVES,
    params,
    id,
  );
};
// End Corporate Objectives

//Start Resource Team
export const getResourceTeam = async () => {
  return ApiHelper.get<IListResponse<IResourceTeam>>(
    ReferenceServiceRoute.GET_RESOURCE_TEAM,
  );
};

export const deleteResourceTeam = async (id: number) => {
  return ApiHelper.delete<IResponse<IResourceTeam>, undefined>(
    ReferenceServiceRoute.RESOURCE_TEAM,
    undefined,
    id,
  );
};

export const addResourceTeam = async (params: IParamsResourceTeam) => {
  return ApiHelper.post<IResponse<IResourceTeam>, IParamsResourceTeam>(
    ReferenceServiceRoute.RESOURCE_TEAM,
    params,
  );
};

export const updateResourceTeam = async (
  id: number,
  params: IParamsResourceTeam,
) => {
  return ApiHelper.put<IResponse<IResourceTeam>, IParamsResourceTeam>(
    ReferenceServiceRoute.RESOURCE_TEAM,
    params,
    id,
  );
};
//End Resource Team

//Start Priority Category Weighting
export const getPriorityCategories = async () => {
  return ApiHelper.get<IListResponse<IPriorityCategories>>(
    ReferenceServiceRoute.PRIORITY_CATEGORIES,
  );
};

export const getListPriorityCategoryWeighting = async () => {
  return ApiHelper.get<IListResponse<IPriorityCategoryWeighting>>(
    ReferenceServiceRoute.PRIORITY_CATEGORY_WEIGHTING,
  );
};

export const deletePriorityCategoryWeighting = async (id: number) => {
  return ApiHelper.delete<IResponse<IPriorityCategoryWeighting>, undefined>(
    ReferenceServiceRoute.PRIORITY_CATEGORY_WEIGHTING,
    undefined,
    id,
  );
};

export const addPriorityCategoryWeighting = async (
  params: IParamsPriorityCategoryWeighting,
) => {
  return ApiHelper.post<
    IResponse<IPriorityCategoryWeighting>,
    IParamsPriorityCategoryWeighting
  >(ReferenceServiceRoute.PRIORITY_CATEGORY_WEIGHTING, params);
};

export const updatePriorityCategoryWeighting = async (
  id: number,
  params: IParamsPriorityCategoryWeighting,
) => {
  return ApiHelper.put<
    IResponse<IPriorityCategoryWeighting>,
    IParamsPriorityCategoryWeighting
  >(ReferenceServiceRoute.PRIORITY_CATEGORY_WEIGHTING, params, id);
};
//End Priority Category Weighting

//Start Priority Label
export const getPriorityLabel = async () => {
  return ApiHelper.get<IListResponse<IPriorityLabel>>(
    ReferenceServiceRoute.PRIORITY_LABEL,
  );
};

export const deletePriorityLabel = async (id: number) => {
  return ApiHelper.delete<IResponse<IPriorityLabel>, undefined>(
    ReferenceServiceRoute.PRIORITY_LABEL,
    undefined,
    id,
  );
};

export const addPriorityLabel = async (params: IParamsPriorityLabel) => {
  return ApiHelper.post<IResponse<IPriorityLabel>, IParamsPriorityLabel>(
    ReferenceServiceRoute.PRIORITY_LABEL,
    params,
  );
};

export const updatePriorityLabel = async (
  id: number,
  params: IParamsPriorityLabel,
) => {
  return ApiHelper.put<IResponse<IPriorityLabel>, IParamsPriorityLabel>(
    ReferenceServiceRoute.PRIORITY_LABEL,
    params,
    id,
  );
};
//End Priority Label

//Start Opportunity Success Probability
export const getOppSuccessProbability = async () => {
  return ApiHelper.get<IListResponse<IOpportunitySuccessProbability>>(
    ReferenceServiceRoute.OPP_SUCCESS_PROBABILITY,
  );
};

export const deleteOppSuccessProbability = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunitySuccessProbability>, undefined>(
    ReferenceServiceRoute.OPP_SUCCESS_PROBABILITY,
    undefined,
    id,
  );
};

export const addOppSuccessProbability = async (
  params: IParamsOpportunitySuccessProbability,
) => {
  return ApiHelper.post<
    IResponse<IOpportunitySuccessProbability>,
    IParamsOpportunitySuccessProbability
  >(ReferenceServiceRoute.OPP_SUCCESS_PROBABILITY, params);
};

export const updateOppSuccessProbability = async (
  id: number,
  params: IParamsOpportunitySuccessProbability,
) => {
  return ApiHelper.put<
    IResponse<IOpportunitySuccessProbability>,
    IParamsOpportunitySuccessProbability
  >(ReferenceServiceRoute.OPP_SUCCESS_PROBABILITY, params, id);
};
//End Priority Label

//Start Expert Role
export const getExpertRoles = async () => {
  return ApiHelper.get<IListResponse<IExpertRoles>>(
    ReferenceServiceRoute.EXPERT_ROLES,
  );
};

export const deleteExpertRole = async (id: number) => {
  return ApiHelper.delete<IResponse<IExpertRoles>, undefined>(
    ReferenceServiceRoute.EXPERT_ROLES,
    undefined,
    id,
  );
};

export const addExpertRole = async (params: IParamsExpertRole) => {
  return ApiHelper.post<IResponse<IExpertRoles>, IParamsExpertRole>(
    ReferenceServiceRoute.EXPERT_ROLES,
    params,
  );
};

export const updateExpertRole = async (
  id: number,
  params: IParamsExpertRole,
) => {
  return ApiHelper.put<IResponse<IExpertRoles>, IParamsExpertRole>(
    ReferenceServiceRoute.EXPERT_ROLES,
    params,
    id,
  );
};
//End Expert Role

export const getBusinessUnits = async () => {
  return ApiHelper.get<IListResponse<IBusinessUnit>>(
    ReferenceServiceRoute.GET_BUSINESS_UNIT,
  );
};

export const addBusinessUnit = async (params: IParamBusinessUnit) => {
  return ApiHelper.post<IResponse<IBusinessUnit>, IParamBusinessUnit>(
    ReferenceServiceRoute.BUSINESS_UNIT,
    params,
  );
};
export const editBusinessUnit = async (
  id: number,
  params: IParamOpportunitySize,
) => {
  return ApiHelper.put<IResponse<IBusinessUnit>, IParamOpportunitySize>(
    ReferenceServiceRoute.BUSINESS_UNIT,
    params,
    id,
  );
};
export const deleteBusinessUnit = async (id: number) => {
  return ApiHelper.delete<IResponse<IBusinessUnit>, undefined>(
    ReferenceServiceRoute.BUSINESS_UNIT,
    undefined,
    id,
  );
};

export const getOpportunitySizes = async () => {
  return ApiHelper.get<IListResponse<IOpportunitySizes>>(
    ReferenceServiceRoute.GET_OPPORTUNITY_SIZE,
  );
};

export const addOpportunitySize = async (params: IParamOpportunitySize) => {
  return ApiHelper.post<IResponse<IBusinessUnit>, IParamOpportunitySize>(
    ReferenceServiceRoute.OPPORTUNITY_SIZE,
    params,
  );
};
export const updateOpportunitySize = async (
  id: number,
  params: IParamOpportunitySize,
) => {
  return ApiHelper.put<IResponse<IOpportunitySizes>, IParamOpportunitySize>(
    ReferenceServiceRoute.OPPORTUNITY_SIZE,
    params,
    id,
  );
};
export const deleteOpportunitySize = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunitySizes>, undefined>(
    ReferenceServiceRoute.OPPORTUNITY_SIZE,
    undefined,
    id,
  );
};
export const getOpportunityTypes = async () => {
  return ApiHelper.get<IListResponse<IOpportunityTypes>>(
    ReferenceServiceRoute.GET_OPPORTUNITY_TYPE,
  );
};

export const addOpportunityType = async (params: IParamOpportunityType) => {
  return ApiHelper.post<IResponse<IOpportunityTypes>, IParamOpportunityType>(
    ReferenceServiceRoute.OPPORTUNITY_TYPE,
    params,
  );
};
export const updateOpportunityType = async (
  id: number,
  params: IParamOpportunityType,
) => {
  return ApiHelper.put<IResponse<IOpportunityTypes>, IParamOpportunityType>(
    ReferenceServiceRoute.OPPORTUNITY_TYPE,
    params,
    id,
  );
};
export const deleteOpportunityType = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunityTypes>, undefined>(
    ReferenceServiceRoute.OPPORTUNITY_TYPE,
    undefined,
    id,
  );
};
export const getBenefitFocusCategories = async () => {
  return ApiHelper.get<IListResponse<IBenefitFocusCategory>>(
    ReferenceServiceRoute.GET_BENEFIT_FOCUS_CATEGORY,
  );
};
export const addBenefitFocusCategory = async (
  params: IParamBenefitFocusCategory,
) => {
  return ApiHelper.post<
    IResponse<IBenefitFocusCategory>,
    IParamBenefitFocusCategory
  >(ReferenceServiceRoute.BENEFIT_FOCUS_CATEGORY, params);
};
export const updateBenefitFocusCategory = async (
  id: number,
  params: IParamBenefitFocusCategory,
) => {
  return ApiHelper.put<
    IResponse<IBenefitFocusCategory>,
    IParamBenefitFocusCategory
  >(ReferenceServiceRoute.BENEFIT_FOCUS_CATEGORY, params, id);
};
export const deleteBenefitFocusCategory = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunityTypes>, undefined>(
    ReferenceServiceRoute.BENEFIT_FOCUS_CATEGORY,
    undefined,
    id,
  );
};

export const getBenefitFocusValues = async () => {
  return ApiHelper.get<IListResponse<IBenefitFocusValue>>(
    ReferenceServiceRoute.GET_BENEFIT_FOCUS_VALUE,
  );
};
export const addBenefitFocusValue = async (params: IParamBenefitFocusValue) => {
  return ApiHelper.post<IResponse<IBenefitFocusValue>, IParamBenefitFocusValue>(
    ReferenceServiceRoute.BENEFIT_FOCUS_VALUE,
    params,
  );
};
export const updateBenefitFocusValue = async (
  id: number,
  params: IParamBenefitFocusValue,
) => {
  return ApiHelper.put<IResponse<IBenefitFocusValue>, IParamBenefitFocusValue>(
    ReferenceServiceRoute.BENEFIT_FOCUS_VALUE,
    params,
    id,
  );
};
export const deleteBenefitFocusValue = async (id: number) => {
  return ApiHelper.delete<IResponse<IBenefitFocusValue>, undefined>(
    ReferenceServiceRoute.BENEFIT_FOCUS_VALUE,
    undefined,
    id,
  );
};

export const getGroupSkills = async () => {
  return ApiHelper.get<IListResponse<IGroupSkill>>(
    ReferenceServiceRoute.GET_SKILL_GROUPS,
  );
};
export const addGroupSkill = async (params: IParamsGroupSkill) => {
  return ApiHelper.post<IResponse<IGroupSkill>, IParamsGroupSkill>(
    ReferenceServiceRoute.SKILL_GROUP,
    params,
  );
};
export const updateGroupSkill = async (
  id: number,
  params: IParamsGroupSkill,
) => {
  return ApiHelper.put<IResponse<IGroupSkill>, IParamsGroupSkill>(
    ReferenceServiceRoute.SKILL_GROUP,
    params,
    id,
  );
};
export const deleteGroupSkill = async (id: number) => {
  return ApiHelper.delete<IResponse<IGroupSkill>, undefined>(
    ReferenceServiceRoute.SKILL_GROUP,
    undefined,
    id,
  );
};
export const getExpertSkills = async (query?: IQueryParam[]) => {
  return ApiHelper.get<IListResponse<IExpertSkill>>(
    ReferenceServiceRoute.EXPERT_SKILL,
    undefined,
    query,
  );
};
export const addExpertSkill = async (params: IParamsExpertSkill) => {
  return ApiHelper.post<IResponse<IExpertSkill>, IParamsExpertSkill>(
    ReferenceServiceRoute.EXPERT_SKILL,
    params,
  );
};
export const updateExpertSkill = async (
  id: number,
  params: IParamsExpertSkill,
) => {
  return ApiHelper.put<IResponse<IExpertSkill>, IParamsExpertSkill>(
    ReferenceServiceRoute.EXPERT_SKILL,
    params,
    id,
  );
};
export const deleteExpertSkill = async (id: number) => {
  return ApiHelper.delete<IResponse<IGroupSkill>, undefined>(
    ReferenceServiceRoute.EXPERT_SKILL,
    undefined,
    id,
  );
};
export const getProficiencyLevels = async () => {
  return ApiHelper.get<IListResponse<IProficiencyLevel>>(
    ReferenceServiceRoute.GET_PROFICIENCY_LEVEL,
  );
};
export const updateProficiencyLevel = async (
  id: number,
  params: IParamsProficiencyLevel,
) => {
  return ApiHelper.put<IResponse<IProficiencyLevel>, IParamsProficiencyLevel>(
    ReferenceServiceRoute.PROFICIENCY_LEVEL,
    params,
    id,
  );
};
export const getDataSets = async () => {
  return ApiHelper.get<IListResponse<IDataSet>>(
    ReferenceServiceRoute.DATASETS,
    undefined,
  );
};

export const addDataset = async (params: IParamsDataset) => {
  return ApiHelper.post<IResponse<IDataSet>, IParamsDataset>(
    ReferenceServiceRoute.DATASETS,
    params,
  );
};

export const deleteDataset = async (id: number) => {
  return ApiHelper.delete<IResponse<IDataSet>, undefined>(
    ReferenceServiceRoute.DATASETS,
    undefined,
    id,
  );
};
export const updateDataset = async (params: IParamsDataset, id: number) => {
  return ApiHelper.put<IResponse<IDataSet>, IParamsDataset>(
    ReferenceServiceRoute.DATASETS,
    params,
    id,
  );
};
//tags
export const getListTags = async (query?: IQueryParam[]) => {
  return ApiHelper.get<IListResponse<ITag>>(
    ReferenceServiceRoute.TAGS,
    undefined,
    query,
  );
};

export const addTag = async (params: ITag) => {
  return ApiHelper.post<IResponse<ITag>, ITag>(
    ReferenceServiceRoute.TAGS,
    params,
  );
};
export const updateTag = async (id: number, params: ITag) => {
  return ApiHelper.put<IResponse<ITag>, ITag>(
    ReferenceServiceRoute.TAGS,
    params,
    id,
  );
};
export const deleteTag = async (id: number) => {
  return ApiHelper.delete<IResponse<ITag>, undefined>(
    ReferenceServiceRoute.TAGS,
    undefined,
    id,
  );
};

export const getListTypes = async () => {
  return ApiHelper.get<IListResponse<IListType>>(
    ReferenceServiceRoute.LIST_TYPE,
    undefined,
  );
};
export const addListTypes = async (params: IListType) => {
  return ApiHelper.post<IResponse<IListType>, IListType>(
    ReferenceServiceRoute.LIST_TYPE,
    params,
  );
};
export const updateListType = async (id: number, params: IListType) => {
  return ApiHelper.put<IResponse<IListType>, IListType>(
    ReferenceServiceRoute.LIST_TYPE,
    params,
    id,
  );
};
export const deleteListType = async (id: number) => {
  return ApiHelper.delete<IResponse<IListType>, undefined>(
    ReferenceServiceRoute.LIST_TYPE,
    undefined,
    id,
  );
};
