import { ITag, IParamTag } from './finding.interface';
import { ApiHelper } from '../../utils';
import { IResponse } from '../common.services';

class FindingServiceRoute {
  static readonly FINDING_TAGS = '/findings/tags';
}
export const updateFindingTag = async (id: number, payload: IParamTag) => {
  return ApiHelper.put<IResponse<ITag>, IParamTag>(
    FindingServiceRoute.FINDING_TAGS,
    payload,
    id,
  );
};
