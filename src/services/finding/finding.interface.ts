export interface ITag {
  id: number;
  name: string;
  description: string;
}
export interface IFindingTag {
  tag_id: number | undefined;
}
export interface IParamTag {
  tags: IFindingTag[];
}
