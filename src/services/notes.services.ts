import { IResponse, IListResponse } from './common.services';
import { ApiHelper } from '../utils';

class NotesServiceRoute {
  static readonly USER_NOTES = '/user-notes';
  static readonly REFRESH_NOTES = './user-notes/refresh';
}

export interface UserNotesT {
  id: number;
  title: string;
  content: string;
  opportunities?: { id: number; title: string }[];
  delete_flg: boolean;
  created_by: {
    id: number;
    full_name: string;
  };
}

export interface OpportunityNoteT {
  title: string;
  content: string;
  opportunity_id: number;
}

export const getAllUserNotes = async (id: number) => {
  return ApiHelper.get<IListResponse<UserNotesT>>(NotesServiceRoute.USER_NOTES);
};

export const addNoteForUser = async (
  payload: Pick<UserNotesT, 'title' | 'content'>,
  id: number,
) => {
  return ApiHelper.post<
    IResponse<UserNotesT>,
    Pick<UserNotesT, 'title' | 'content'>
  >(NotesServiceRoute.USER_NOTES, payload, id);
};

export const updateNoteOfUser = async (
  payload: Pick<UserNotesT, 'title' | 'content'>,
  id: number,
) => {
  return ApiHelper.put<
    IResponse<UserNotesT>,
    Pick<UserNotesT, 'title' | 'content'>
  >(NotesServiceRoute.USER_NOTES, payload, id);
};

export const deleteNoteOfUser = async (id: number) => {
  return ApiHelper.delete<IResponse<UserNotesT>, undefined>(
    NotesServiceRoute.USER_NOTES,
    undefined,
    id,
  );
};

export const addOpportunityNote = async (
  payload: OpportunityNoteT,
  id: number,
) => {
  return ApiHelper.post<IResponse<UserNotesT>, OpportunityNoteT>(
    NotesServiceRoute.USER_NOTES,
    payload,
    id,
  );
};

export const updateOpportunityNote = async (
  payload: OpportunityNoteT,
  id: number,
) => {
  return ApiHelper.put<IResponse<UserNotesT>, OpportunityNoteT>(
    NotesServiceRoute.USER_NOTES,
    payload,
    id,
  );
};
