import { IResponse } from '../common.services';
import { ApiHelper } from '../../utils';
import { IParamLink } from './link.interface';

class LinkServiceRoute {
  static readonly LINK = '/links';
}
export const addLink = async (body: IParamLink) => {
  return ApiHelper.post<IResponse<any>, IParamLink>(
    LinkServiceRoute.LINK,
    body,
  );
};
export const updateLink = async (id: number, body: IParamLink) => {
  return ApiHelper.put<IResponse<any>, IParamLink>(
    LinkServiceRoute.LINK,
    body,
    id,
  );
};

export const deleteLink = async (id: number) => {
  return ApiHelper.delete(LinkServiceRoute.LINK, undefined, id);
};
