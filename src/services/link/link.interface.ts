export enum EntityTypeLink {
  opportunity_playbook_item = 'opportunity_playbook_item',
}
export interface ILink {
  id: 0;
  name: string;
  url: string;
  parent_id: number;
  description: string;
  is_deleted?: boolean;
  entity_type: string;
  entity_id?: number;
}
export interface IParamLink {
  id?: number;
  title: string;
  parent_id?: number;
  entity_type?: string;
  entity_id?: number;
  opportunity_playbook_item_id: number;
  links?: ILink[];
}

export interface IResponseLink {
  id: number;
  title: string;
  opportunity_playbook_item_id: number;
  item_link?: ILink[];
  created_by_user?: {
    id?: number;
    name?: string;
  };
  updated_by_user?: {
    id?: number;
    name?: string;
  };
  last_updated_at?: Date;
}
