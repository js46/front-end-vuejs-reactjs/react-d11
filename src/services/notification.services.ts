import { IResponse, IListResponse } from './common.services';
import { ApiHelper } from '../utils';

class notificationServiceRoute {
  static readonly NOTIFICATION = '/notifications';
  static readonly OPPORTUNITY_NOTIFICATION = '/notification';
  static readonly SEND_NOTIFICATION = '/notification/sent';
  static readonly RESEND_NOTIFICATION = '/notification/resent';
}

export interface NotificationT {
  id: number;
  notification_user_case: {
    id: number;
    name: string;
    type: string;
    message: string;
    email_template_path: string;
  };
  opportunity: { id: number; title: string };
  opportunity_phase: { id: number; name: string };
  opportunity_state: { id: number; name: string };
  status: number;
  delivery_details: string;
  message: string;
  subject: string;
  to_email: string;
  submit_date: string;
  sent_date: string;
  to_user_id: string;
  from_user_id: string;
}

export interface NotificationParamT {
  notification_user_case_id: number;
  opportunity_id: number;
  to_email: string;
}

export const getOpportunityNotification = (id: number) => {
  return ApiHelper.get<IListResponse<NotificationT>>(
    notificationServiceRoute.OPPORTUNITY_NOTIFICATION,
    id,
  );
};

export const addNotification = (params: NotificationParamT) => {
  return ApiHelper.post<IResponse<NotificationT>, NotificationParamT>(
    notificationServiceRoute.NOTIFICATION,
    params,
  );
};

export const sendNotification = (id: number) => {
  return ApiHelper.put<IResponse<NotificationT>, undefined>(
    notificationServiceRoute.SEND_NOTIFICATION,
    undefined,
    id,
  );
};

export const resendNotification = (id: number) => {
  return ApiHelper.put<IResponse<NotificationT>, undefined>(
    notificationServiceRoute.RESEND_NOTIFICATION,
    undefined,
    id,
  );
};

export const updateNotification = (params: NotificationParamT, id: number) => {
  return ApiHelper.put<IResponse<NotificationT>, NotificationParamT>(
    notificationServiceRoute.NOTIFICATION,
    params,
    id,
  );
};

export const deleteNotification = (id: number) => {
  return ApiHelper.delete<IResponse<NotificationT>, undefined>(
    notificationServiceRoute.NOTIFICATION,
    undefined,
    id,
  );
};
