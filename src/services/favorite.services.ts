import { ApiHelper } from '../utils';
import { IResponse } from './common.services';

const FavoriteRoute = {
  index: '/favorites',
};
export enum EntityType {
  skill = 'skill',
  opportunity = 'opportunity',
  findings = 'findings',
}
export const addFavoriteItem = async (
  entity_type: EntityType,
  entity_id: number,
) => {
  const body = {
    entity_id,
    entity_type,
  };
  return ApiHelper.post<IResponse<any>, typeof body>(FavoriteRoute.index, body);
};

export const deleteFavoriteItem = async (id: number) => {
  return ApiHelper.delete(FavoriteRoute.index, undefined, id);
};
