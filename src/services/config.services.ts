import { ApiHelper } from '../utils';
import { IResponse } from './common.services';
import { ES_INDEX_KEYS, GOOGLE_CONFIG } from '../config';
const ConfigRoute = {
  appEnv: '/private-env',
  appEnvPublic: '/app-env',
};
export interface IConfig {
  es_indexs: ES_INDEX_KEYS;
  google_login: GOOGLE_CONFIG;
  account_login: {
    active: boolean;
  };
  app_id_google: string;
  tinymce_api_key: string;
}

export interface IConfigPublic {
  default_login: 'account' | 'google' | 'okta';
  google_client_id: string;
  okta_login_url: string;
  okta_client_id: string;
  okta_redirect_uri: string;
}

export const getConfigEnv = async () => {
  return ApiHelper.get<IResponse<IConfig>>(ConfigRoute.appEnv);
};

export const getConfigPublic = async () => {
  return ApiHelper.get<IResponse<IConfigPublic>>(ConfigRoute.appEnvPublic);
};
