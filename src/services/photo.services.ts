import { IResponse } from './common.services';
import { ApiHelper } from '../utils';

class PhotoServiceRoute {
  static readonly UploadPhoto = '/attachments/upload';
}
export interface IPhotoImage {
  url: string;
}
export const uploadPhoto = async (formData: any) => {
  const headerConfig = {
    headers: {
      'content-type': 'multipart/form-data',
    },
  };
  return ApiHelper.post<IResponse<IPhotoImage>, any>(
    PhotoServiceRoute.UploadPhoto,
    formData,
    undefined,
    headerConfig,
  );
};
