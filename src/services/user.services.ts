import axios from '../commons/axios';
import { IResponse, apiRouteGenerator } from './common.services';
import { ApiHelper } from '../utils';

class UserServiceRoute {
  static readonly USERS = '/system/users';
  static readonly ROLES = '/sys/roles';
  static readonly GOOGLE_AUTH = '/auth/login-google';
  static readonly OKTA_AUTH = '/auth/login-okta';
  static readonly PERMISSIONS = '/users/permissions';
  static readonly USER_PROFILE = '/users/profile';
}
export interface IUser {
  id: number;
  email: string;
  password?: string;
  casbin_user: string;
  auth_source?: string;
  team_lead_flg: boolean;
  resource_team?: { id: number; name: string };
  business_unit?: { id: number; name: string };
  active: boolean;
  full_name: string;
  note?: string;
  roles: string[];
  internal: boolean;
  token?: string;
}

export interface IValue {
  value: string;
}
interface IKeyValue extends IValue {
  key?: number;
}
export interface IBusinessFocus extends IKeyValue {
  amount?: string;
}

export interface OpportunityAction {
  id: number;
  casbin_name: string;
  display_name: string;
  action_id: number;
  path: string;
  method: string;
  phase: string;
  state: string;
}
export interface Permission {
  permissions: string[][];
  opportunity_actions: OpportunityAction[];
}

export interface UserProfile {
  id: number;
  full_name: string;
  profile_name: string;
  profile_role: {
    id: number;
    profile_role_id: number;
    profile_role_name: string;
    proficiency_level_id: number;
  };
  casbin_role: string[];
  location_id: number;
  last_updated_at: string;
  email: string;
  user_name: string;
  updated_: string;
  resource_team: {
    resource_team_id: number;
    resource_team_name: string;
  };
  business_unit: {
    business_unit_id: number;
    business_unit_name: string;
  };
  isAdmin: boolean;
  internal: boolean;
  team_lead_flg: boolean;
}

export const getUsers = async (
  page = 1,
  limit = 10,
  search?: string,
  filters?: any,
) => {
  const { data } = await axios.get<{
    data: { list: IUser[]; total: number };
    success: boolean;
  }>(apiRouteGenerator(UserServiceRoute.USERS), {
    params: { page, limit, search, active: filters?.[0]?.value },
  });
  return data;
};

export const getRoles = async () => {
  return ApiHelper.get<IResponse<string[]>>(UserServiceRoute.ROLES);
};

export const updateUser = async (user: Partial<IUser>) => {
  return ApiHelper.put<IResponse<IUser>, typeof user>(
    UserServiceRoute.USERS,
    user,
    user?.id,
  );
};

export const addUser = async (user: Partial<IUser>) => {
  return ApiHelper.post<IResponse<IUser>, typeof user>(
    UserServiceRoute.USERS,
    user,
  );
};

export const deleteUser = async (id: number) => {
  return ApiHelper.delete<any, any>(UserServiceRoute.USERS, undefined, id);
};

export const getPermission = async () => {
  return ApiHelper.get<IResponse<Permission>>(UserServiceRoute.PERMISSIONS);
};
export const getUserProfile = async () => {
  return ApiHelper.get<IResponse<UserProfile>>(UserServiceRoute.USER_PROFILE);
};
