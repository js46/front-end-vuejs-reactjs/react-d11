import { IListResponse, IResponse, IQueryParam } from './common.services';
import { ApiHelper } from '../utils';
import { AttachmentResponse } from './opportunity.services';
import { IOpportunityTask } from './task.services';
import { IResponseLink } from './link';
import { IIssueItems } from './issue';

class PlaybookServiceRoute {
  static readonly PLAYBOOK = '/playbooks';
  static readonly OPPORTUNITY_PLAYBOOK = '/playbooks-opportunity';
  static readonly OPPORTUNITY_PLAYBOOK_DETAIL =
    '/playbooks-opportunity/playbook';
  static readonly PLAYBOOK_ITEM_LIST = '/list-item';
  static readonly ASSIGN_USER = '/playbook-opportunity/assign-item';
  static readonly UNASSIGN_USER = '/playbook-opportunity/unassign-item';
  static readonly ADD_PLAYBOOK_ITEM = '/playbook-opportunity/add-item';
  static readonly UPDATE_PLAYBOOK_ITEM = '/playbook-opportunity/update-item';
  static readonly REMOVE_PLAYBOOK_ITEM = '/playbook-opportunity/remove-item';
  static readonly ADD_NOTE = '/playbook-opportunity/add-item-note';
  static readonly UPDATE_NOTE = '/playbook-opportunity/update-item-note';
  static readonly REMOVE_NOTE = '/playbook-opportunity/remove-item-note';
  static readonly ALL_PLAYBOOK = '/playbooks-opportunity/opportunity';
  static readonly DONE_PLAYBOOK_ITEM = '/playbook-opportunity/done-item';
  static readonly UNDONE_PLAYBOOK_ITEM = '/playbook-opportunity/undone-item';
  static readonly UPDATE_ATTACHMENT = '/attachments';
  static readonly SELECT_PLAYBOOK = '/playbook-opportunity/selected';
  static readonly UPDATE_STATE_PLAYBOOK_ITEM =
    '/playbook-opportunity/update-state';
  static readonly PLAYBOOK_ITEM_FINDING = '/finding';
  static readonly FINDING = '/finding';
}

export const getPlaybookStatusItemNumberPresentation = (
  pbItemStatus: PlaybookItemStatusT,
) => {
  switch (pbItemStatus) {
    case 'UNASSIGNED':
      return 1;
    case 'ASSIGNED':
      return 2;
    case 'DONE':
      return 3;
    case 'UNDONE':
      return 2;
    default:
      return 1;
  }
};

export type PlaybookItemStatusT =
  | 'TODO'
  | 'ASSIGNED'
  | 'UNASSIGNED'
  | 'DONE'
  | 'UNDONE'
  | 'DELETE';
export interface PlaybookItemT {
  id: number;
  name: string;
  description: string;
  is_deleted?: boolean;
  playbook_item_parent_id: number;
  item_order: number;
  children?: PlaybookItemT[];
}
export interface PlaybookT {
  id: number;
  name: string;
  description: string;
  playbook_items: PlaybookItemT[];
  opportunity_type: { id: number; name: string };
  opportunity_count: number;
  created_by_user: { id: number; name: string };
  opportunity?: {
    id: number;
    title: string;
    opportunity_phase: {
      id: number;
      name: string;
      display_name: string;
    };
    date: string;
    priority_score: number;
    priority_level: string;
    created_by_user: { id: number; name: string };
  }[];
  archive?: number;
}
export type INewPlaybookItemTree = {
  children?: Array<INewPlaybookItemTree>;
} & { title: string; description: string; item_order: number };

export interface NewPlaybookItemT {
  title: string;
  description?: string;
  assignee_user_id?: string;
}
export interface TNewPlaybookItemForm {
  playbook_items: INewPlaybookItem[];
}
export interface NewPlayBookT {
  name: string;
  description?: string;
  origin_playbook_id?: number;
  playbook_items: INewPlaybookItemTree[];
}

export interface PlaybookItemReference {
  title: string;
  name: string;
  description: string;
  playbook_item_parent_id?: number;
  item_order: number;
  children: PlaybookItemReference[];
}
export interface PlayBookTReference {
  name: string;
  description?: string;
  playbook_items: PlaybookItemReference[];
}
export interface NoteT {
  id: number;
  opportunity?: { id: number; title: string };
  opportunity_playbook_item?: { id: number; title: string };
  title: string;
  content: string;
  user_note_id: string;
  created_by: { id: number; full_name: string };
  created_by_user: {
    id: number;
    name: string;
  };
  date: string;
}

export interface NoteResponseT {
  id: number;
  time: Date;
  title: string;
  content: string;
  opportunities: { id: number; title: string }[];
  created_by: { id: number; name: string };
  delete_flg: boolean;
  highlight: any;
}

export interface EditNoteT {
  title: string;
  content: string;
  created_by_user_id: number;
}
export interface ResponsePlayBookT {
  id: number;
  title: string;
  description: string;
  playbook_item_parent_id?: number;
  item_order?: number;
  playbook_item_state?: { id: number; name: PlaybookItemStatusT };
  assignee_user_id?: { id: number; full_name: string };
  notes?: NoteT[];
  attachments?: AttachmentResponse[];
  findings?: PlaybookItemFindingT[];
  tasks?: IOpportunityTask[];
  lists?: IListItem[];
  links?: IResponseLink[];
  due_date?: Date | null | undefined;
  issues?: IIssueItems[];
}

export interface OpportunityPlaybookT {
  id: number;
  name: string;
  description: string;
  opportunity: { id: number; title: string; completion_date: Date | string };
  playbook_items: ResponsePlayBookT[];
  selected_flg: boolean;
  archive?: boolean;
  issue_count: number;
  task_count: number;
  created_by_user: { id: number; name: string };
  updated_by_user: { id: number; name: string };
}

export interface AssignUserT {
  playbook_items: { assignee_user_id: number }[];
}

export interface UpdatePlaybookItemT {
  playbook_items: {
    title?: string;
    description?: string;
    assignee_user_id?: number;
    playbook_item_state_id?: number;
    notes?: { title?: string; content?: string; created_by_user_id?: number }[];
    due_date?: Date | null | undefined;
  }[];
}
export interface INewPlaybookItem {
  title: string;
  description: string;
  playbook_item_parent_id: number;
  item_order: number;
}

export interface ItemNoteT {
  title: string;
  content: string;
  created_by_user_id: number;
  user_note_id?: number;
}
export interface AddNoteT {
  notes: ItemNoteT[];
}

export interface PlaybookItemFindingT {
  id: number;
  title: string;
  playbook_item?: {
    id: number;
    name: string;
  };
  created_by_user: {
    id: number;
    name: string;
  };
  opportunity?: {
    id: number;
    name: string;
  };
  favorite_count?: number;
  favorites?: { id: number; user_id: number }[];
  description?: string;
  date?: string;
  entity_id?: number;
  entity_type?: string;
  public_flg?: boolean;
  updated_by_user?: {
    id: number;
    name: string;
  };
  tags?: ITagFinding[];
  last_updated_at?: Date;
  del_flg?: boolean;
  created_date_at?: Date;
  opportunity_playbook?: { id: number; name: string };
  opportunity_playbook_item?: { id: number; title: string };
}
export interface ITagFinding {
  entity_id: number;
  tag: { id: number; name: string };
  entity_type: string;
}
export interface IListItem {
  id: number;
  title: string;
  item_order: number;
  parent_id: number;
  opportunity_playbook_item_id?: number;
  is_deleted?: boolean;
  item_list: IChildrenListItem[];
  created_by_user?: {
    id: number;
    name: string;
  };
  created_date_at?: Date;
  updated_by_user?: {
    id: number;
    name: string;
  };
  last_updated_at?: Date;
  type?: {
    id: number;
    name: string;
    description: string;
  };
}
export interface IUpdateState {
  playbook_item_state_id: number;
}
export interface IChildrenListItem {
  id: number;
  title: string;
  is_deleted?: boolean;
  item_order: number;
  parent_id: number;
  parent_list_id: number;
}
export interface IPlaybookItemListPayloadAdd {
  title: string;
  item_list: IChildListTree[];
}
export interface IPlaybookItemListPayloadUpdate {
  title: string;
  item_list: IChildListTree[];
  // item_list: IListItem[];
}
export type IChildListTree = {
  children?: Array<IChildListTree>;
} & IChildrenListItem;
export interface IPlaybookItemListPayloadDelete {
  ids: number[];
}
export const getPlaybookList = (query?: IQueryParam[]) => {
  return ApiHelper.get<IListResponse<PlaybookT>>(
    PlaybookServiceRoute.PLAYBOOK,
    undefined,
    query,
  );
};

export const getPlayBookDetail = (param: number) => {
  return ApiHelper.get<IResponse<PlaybookT>>(
    PlaybookServiceRoute.PLAYBOOK,
    param,
  );
};
export const addNewPlaybookInReference = (payload: PlayBookTReference) => {
  return ApiHelper.post<IResponse<PlaybookT>, PlayBookTReference>(
    PlaybookServiceRoute.PLAYBOOK,
    payload,
  );
};
export const editPlaybookInReference = (
  payload: PlayBookTReference,
  param: number,
) => {
  return ApiHelper.put<IResponse<PlaybookT>, PlayBookTReference>(
    PlaybookServiceRoute.PLAYBOOK,
    payload,
    param,
  );
};
export const getPlayBookOppDetail = (param: number) => {
  return ApiHelper.get<IResponse<OpportunityPlaybookT>>(
    PlaybookServiceRoute.OPPORTUNITY_PLAYBOOK_DETAIL,
    param,
  );
};
export const addNewPlaybook = (payload: NewPlayBookT, param: number) => {
  return ApiHelper.post<IResponse<OpportunityPlaybookT>, NewPlayBookT>(
    PlaybookServiceRoute.OPPORTUNITY_PLAYBOOK,
    payload,
    param,
  );
};
export const updateStatePlaybookItem = (
  payload: IUpdateState,
  param: number,
) => {
  return ApiHelper.put<IResponse<any>, IUpdateState>(
    PlaybookServiceRoute.UPDATE_STATE_PLAYBOOK_ITEM,
    payload,
    param,
  );
};
export const deletePlaybookInReference = (id: number) => {
  return ApiHelper.delete<IResponse<any>, undefined>(
    PlaybookServiceRoute.PLAYBOOK,
    undefined,
    id,
  );
};
export const deletePlaybook = (id: number) => {
  return ApiHelper.delete<IResponse<any>, undefined>(
    PlaybookServiceRoute.OPPORTUNITY_PLAYBOOK,
    undefined,
    id,
  );
};

export const assignUserItem = (payload: AssignUserT, id: number) => {
  return ApiHelper.put<IResponse<OpportunityPlaybookT>, AssignUserT>(
    PlaybookServiceRoute.ASSIGN_USER,
    payload,
    id,
  );
};

export const unAssignUserItem = (id: number) => {
  return ApiHelper.put<IResponse<OpportunityPlaybookT>, undefined>(
    PlaybookServiceRoute.UNASSIGN_USER,
    undefined,
    id,
  );
};

export const addNoteToItem = (payload: AddNoteT, id: number) => {
  return ApiHelper.post<IResponse<OpportunityPlaybookT>, AddNoteT>(
    PlaybookServiceRoute.ADD_NOTE,
    payload,
    id,
  );
};

export const editNoteToItem = (payload: EditNoteT, id: number) => {
  return ApiHelper.put<IResponse<OpportunityPlaybookT>, EditNoteT>(
    PlaybookServiceRoute.UPDATE_NOTE,
    payload,
    id,
  );
};

export const getAllPlaybookOfOpportunity = (id: number) => {
  return ApiHelper.get<IResponse<OpportunityPlaybookT[]>>(
    PlaybookServiceRoute.ALL_PLAYBOOK,
    id,
  );
};

export const removeNoteOnPlaybook = (id: number) => {
  return ApiHelper.delete<IResponse<OpportunityPlaybookT>>(
    PlaybookServiceRoute.REMOVE_NOTE,
    undefined,
    id,
  );
};

export const addPlaybookItem = (payload: TNewPlaybookItemForm, id: number) => {
  return ApiHelper.post<IResponse<OpportunityPlaybookT>, any>(
    PlaybookServiceRoute.ADD_PLAYBOOK_ITEM,
    payload,
    id,
  );
};

export const updatePlaybookItem = (
  payload: UpdatePlaybookItemT,
  id: number,
) => {
  return ApiHelper.put<IResponse<OpportunityPlaybookT>, UpdatePlaybookItemT>(
    PlaybookServiceRoute.UPDATE_PLAYBOOK_ITEM,
    payload,
    id,
  );
};

export const removePlaybookItem = (id: number) => {
  return ApiHelper.delete<IResponse<OpportunityPlaybookT>, undefined>(
    PlaybookServiceRoute.REMOVE_PLAYBOOK_ITEM,
    undefined,
    id,
  );
};

export const donePlaybookItem = (id: number) => {
  return ApiHelper.put<IResponse<OpportunityPlaybookT>, undefined>(
    PlaybookServiceRoute.DONE_PLAYBOOK_ITEM,
    undefined,
    id,
  );
};

export const undonePlaybookItem = (id: number) => {
  return ApiHelper.put<IResponse<OpportunityPlaybookT>, undefined>(
    PlaybookServiceRoute.UNDONE_PLAYBOOK_ITEM,
    undefined,
    id,
  );
};

export const updateFileAttachment = (payload: FormData, id: number) => {
  return ApiHelper.put<IResponse<AttachmentResponse>, any>(
    PlaybookServiceRoute.UPDATE_ATTACHMENT,
    payload,
    id,
  );
};

export const selectPlaybook = (id: number) => {
  return ApiHelper.put<IResponse<any>, undefined>(
    PlaybookServiceRoute.SELECT_PLAYBOOK,
    undefined,
    id,
  );
};

export const addPlaybookItemFinding = (
  itemID: number,
  entity_type: string,
  title: string,
  description: string,
  public_flg?: boolean,
) => {
  const body = {
    title,
    entity_type,
    description,
    entity_id: itemID,
    public_flg: public_flg,
  };
  return ApiHelper.post<IResponse<PlaybookItemFindingT>, typeof body>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_FINDING,
    body,
  );
};

export const updatePlaybookItemFinding = (
  id: number,
  title: string,
  description: string,
  public_flg?: boolean,
) => {
  const body = {
    title,
    description,
    public_flg,
  };
  return ApiHelper.put<IResponse<PlaybookItemFindingT>, typeof body>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_FINDING,
    body,
    id,
  );
};

export const getFindingsByPlaybookItem = (itemID: number) => {
  return ApiHelper.get<IListResponse<PlaybookItemFindingT>>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_FINDING,
    itemID,
  );
};

export const deletePlaybookItemFinding = (id: number) => {
  return ApiHelper.delete<IResponse<PlaybookItemFindingT>>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_FINDING,
    undefined,
    id,
  );
};
export const addPlayBookItemList = (
  payload: IPlaybookItemListPayloadAdd,
  id: number | undefined,
) => {
  return ApiHelper.post<IResponse<any>, IPlaybookItemListPayloadAdd>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_LIST,
    payload,
    id,
  );
};
export const updatePlayBookItemList = (
  payload: IPlaybookItemListPayloadUpdate,
  id: number | undefined,
) => {
  return ApiHelper.put<IResponse<any>, IPlaybookItemListPayloadUpdate>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_LIST,
    payload,
    id,
  );
};
export const deletePlayBookItemList = (id: number | undefined) => {
  return ApiHelper.delete<IResponse<any>, undefined>(
    PlaybookServiceRoute.PLAYBOOK_ITEM_LIST,
    undefined,
    id,
  );
};
export const getFindingDetail = (id: number) => {
  return ApiHelper.get<IResponse<PlaybookItemFindingT>>(
    PlaybookServiceRoute.FINDING,
    id,
  );
};
