const apiVersion = '/api/v1';
export interface IQueryParam {
  key: string;
  value: string | number;
}
export const apiRouteGenerator = (
  route: string,
  idParam?: number,
  query?: IQueryParam[],
) => {
  let url = `${apiVersion}${route}`;
  if (idParam) {
    url = `${url}/${idParam}`;
  }
  if (query && query.length > 0) {
    var searchParams = new URLSearchParams();
    for (let item of query) {
      searchParams.append(item.key, item.value.toString());
    }
    return `${url}?${searchParams.toString()}`;
  }
  return url;
};

export interface IListResponse<T> {
  data: {
    list: T[];
    total: number;
  };
  error?: any;
  message?: string;
  success: boolean;
}

export interface IResponse<T> {
  data: T;
  error?: any;
  message?: string;
  success: boolean;
}
