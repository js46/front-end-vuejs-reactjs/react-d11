import { IResponse } from './common.services';
import {
  TaskState,
  TaskStateID,
  TaskSubType,
  TaskSubTypeID,
  TaskType,
  TaskTypeID,
} from '../constants';
import { ApiHelper } from '../utils';

class TaskServiceRoute {
  static readonly OPPORTUNITY_TASK = '/task-opportunities';
  static readonly OPPORTUNITY_TASK_STATE = '/task-opportunities/task-state';
}

export interface IOpportunityTask {
  id: number;
  opportunity?: { id: number; phase: string; title: string; state: string };
  title: string;
  description?: string;
  entity_id?: number;
  entity_type?: string;
  task_type: TaskType;
  task_state: TaskState;
  task_sub_type: TaskSubType;
  task_result?: string;
  machine_note?: string;
  task_request_note?: string;
  created_at?: Date;
  created_by_user?: { id?: number; name?: string };
  assignee_user: { id: number; name?: string };
  last_updated_at?: Date;
  updated_user?: { id?: number; name?: string };
  archive_flg: boolean;
  requested_completion_date?: Date | null;
}
export interface TExpertAssignment {
  opportunity_id: number;
  task_type: string;
  assignee_user_id: number;
  machine_note: string;
}
export interface ITaskParams {
  id?: number;
  title: string;
  description: string;
  task_type_id: TaskTypeID;
  task_sub_type_id: TaskSubTypeID;
  task_state_id: typeof TaskStateID[TaskState];
  assignee_user_id: number;
  machine_note?: string;
  requested_completion_date?: Date | null;
  entity_id: number;
  entity_type: string;
}
export const addTask = async (params: TExpertAssignment) => {
  return ApiHelper.post<IResponse<IOpportunityTask>, TExpertAssignment>(
    TaskServiceRoute.OPPORTUNITY_TASK,
    params,
  );
};
export const addTaskNew = async (params: ITaskParams) => {
  return ApiHelper.post<IResponse<IOpportunityTask>, ITaskParams>(
    TaskServiceRoute.OPPORTUNITY_TASK,
    params,
  );
};
export const updateTask = async (id: number, params: ITaskParams) => {
  return ApiHelper.put<IResponse<IOpportunityTask>, ITaskParams>(
    TaskServiceRoute.OPPORTUNITY_TASK,
    params,
    id,
  );
};
export const getTask = async (id: number) => {
  return ApiHelper.get<IResponse<IOpportunityTask>>(
    TaskServiceRoute.OPPORTUNITY_TASK,
    id,
  );
};

export const updateTaskState = async (
  id: number,
  stateID: typeof TaskStateID[TaskState],
) => {
  return ApiHelper.patch<
    IResponse<IOpportunityTask>,
    { task_state_id: number }
  >(TaskServiceRoute.OPPORTUNITY_TASK_STATE, { task_state_id: stateID }, id);
};

export const deleteTask = async (id: number) => {
  return ApiHelper.delete<IResponse<IOpportunityTask>, undefined>(
    TaskServiceRoute.OPPORTUNITY_TASK,
    undefined,
    id,
  );
};
