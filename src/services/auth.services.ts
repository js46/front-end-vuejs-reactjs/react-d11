import { IResponse } from './common.services';
import { ApiHelper } from '../utils';
import { IUser } from './user.services';

class AuthServiceRoute {
  static readonly GOOGLE_AUTH = '/auth/login-google';
  static readonly OKTA_AUTH = '/auth/login-okta';
  static readonly LOGIN_AUTH = '/auth/login';
  static readonly USER_PROFILE = '/users/profile';
  static readonly LOGOUT = '/logout';
}
export interface IParamLogin {
  email: string;
  password: string;
}
export const loginAuth = async (params: IParamLogin) => {
  return ApiHelper.post<IResponse<IUser>, IParamLogin>(
    AuthServiceRoute.LOGIN_AUTH,
    params,
  );
};
export const googleAuth = async (idToken: string) => {
  const bodyRequest = { token_id: idToken };
  return ApiHelper.post<IResponse<IUser>, typeof bodyRequest>(
    AuthServiceRoute.GOOGLE_AUTH,
    bodyRequest,
  );
};

export const oktaAuthentication = async (access_token: string) => {
  const bodyRequest = { access_token: access_token };
  return ApiHelper.post<IResponse<IUser>, typeof bodyRequest>(
    AuthServiceRoute.OKTA_AUTH,
    bodyRequest,
  );
};

export const logOut = async () => {
  return ApiHelper.post<IResponse<any>, undefined>(
    AuthServiceRoute.LOGOUT,
    undefined,
  );
};
