export interface IIssueItems {
  id: number;
  title: string;
  description: string;
  state: string;
  is_deleted: boolean;
  created_by_user?: {
    id: number;
    name: string;
  };
  updated_by_user?: {
    id: number;
    name: string;
  };
}

export interface IIssueParam {
  name: string;
  playbook_item_id: number;
  issue_items: {
    id?: number;
    title: string;
    description: string;
    is_deleted?: boolean;
    state?: string;
  };
}

export interface IAddIssueParam {
  entity_id?: number;
  entity_type?: string;
  title: string;
  description?: string;
  state: string;
}
