import { IIssueItems, IAddIssueParam } from './issue.interface';
import { ApiHelper } from '../../utils';
import { IResponse } from '../common.services';

class FindingServiceRoute {
  static readonly ISSUES = '/issues';
}
export const addIssue = async (payload: IAddIssueParam) => {
  return ApiHelper.post<IResponse<IIssueItems>, IAddIssueParam>(
    FindingServiceRoute.ISSUES,
    payload,
  );
};

export const updateIssue = async (id: number, payload: IAddIssueParam) => {
  return ApiHelper.put<IResponse<IIssueItems>, IAddIssueParam>(
    FindingServiceRoute.ISSUES,
    payload,
    id,
  );
};
