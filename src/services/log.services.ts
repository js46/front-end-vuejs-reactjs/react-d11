export interface ILogEvent {
  action_name: string;
  client_ip: string;
  duration: number;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  object_type: string;
  path: string;
  referrer: string;
  request_body?: object;
  request_id?: string;
  response_body?: any;
  status: number;
  time: string;
  user_id?: number;
  user_name: string;
}
