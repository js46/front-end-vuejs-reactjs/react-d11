import { IResponse } from './common.services';
import { ApiHelper } from '../utils';

class CommentServiceRoute {
  static readonly COMMENT = '/comments';
}

export interface IParent {
  id: number;
  name: string;
}
export interface IComment {
  id: number;
  entity_id: number;
  entity_name: string;
  entity_type: string;
  comment_body: string;
  comment_type: string;
  comment_parent_id: number;
  created_by?: IParent;
  created_at?: string;
  updated_at?: string;
  last_updated_at?: string;
  last_updated_by?: Array<IParent>;
  time: Date | string;
}

export interface ICommentParam {
  entity_id?: string | number;
  entity_type?: string;
  comment_type?: string;
  comment_body?: string;
  comment_parent_id?: number;
}

export const addOpportunityComment = async (params: ICommentParam) => {
  return ApiHelper.post<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
  );
};

export const updateOpportunityComment = async (
  params: ICommentParam,
  id: number,
) => {
  return ApiHelper.put<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
    id,
  );
};

export const addTaskComment = async (params: ICommentParam) => {
  return ApiHelper.post<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
  );
};

export const updateTaskComment = async (params: ICommentParam, id: number) => {
  return ApiHelper.put<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
    id,
  );
};

export const addHelpFeedback = async (content: string, replyTo = 0) => {
  const params: ICommentParam = {
    entity_id: 0,
    entity_type: 'help',
    comment_type: 'feedback',
    comment_parent_id: replyTo,
    comment_body: content,
  };
  return ApiHelper.post<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
  );
};

export const addExpertReviewComment = async (params: ICommentParam) => {
  return ApiHelper.post<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
  );
};

export const updateExpertReviewComment = async (
  params: ICommentParam,
  id: number,
) => {
  return ApiHelper.put<IResponse<IComment>, ICommentParam>(
    CommentServiceRoute.COMMENT,
    params,
    id,
  );
};
