import { IListResponse } from './common.services';
import { ApiHelper } from '../utils';

const HolidayServiceRoute = {
  list: '/holidays',
};

export interface IHoliday {
  id: number;
  name: string;
  description?: string;
  date: string;
}

export const getAllHolidays = async () => {
  return ApiHelper.get<IListResponse<IHoliday>>(HolidayServiceRoute.list);
};
