import React, { useEffect } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { routes } from './routes';
import { RouteWithLayout } from './components/RouteWithLayout';
import { Notification } from './components';
import { OktaCallback } from './components';
import theme from './theme';
import { useDispatch } from 'react-redux';
import { RequestUserProfile } from './store/user/actions';
import {
  RequestConfigPrivate,
  RequestConfigPublic,
} from './store/config/actions';
import { history } from './store';
import { IsLoggedIn } from './store/auth/selector';
import { DialogError } from './components/common/DialogError';
import './i18n';
import './App.scss';

export default function App() {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(IsLoggedIn);
  useEffect(() => {
    dispatch(RequestConfigPublic());
    if (isLoggedIn) {
      dispatch(RequestConfigPrivate());
      dispatch(RequestUserProfile());
    }
  }, [dispatch, isLoggedIn]);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline>
        <Notification />
        <DialogError />
        <Router history={history}>
          <Switch>
            <Route exact path="/auth/okta" component={OktaCallback} />
            {routes.map(
              (
                {
                  Component,
                  Layout,
                  Protected,
                  hasSidebar,
                  routePath,
                  exact,
                  path,
                },
                key,
              ) => {
                return (
                  <RouteWithLayout
                    key={key}
                    hasSidebar={hasSidebar}
                    component={Component}
                    exact={exact}
                    layout={Layout}
                    path={path}
                    routePath={routePath}
                    protect={Protected}
                  />
                );
              },
            )}
          </Switch>
        </Router>
      </CssBaseline>
    </ThemeProvider>
  );
}
