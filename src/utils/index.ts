import { IComment } from '../services/comment.services';
import { apiRouteGenerator } from '../services/common.services';
import axios from '../commons/axios';
import { PlaybookItemStatusT } from '../services/playbook.services';
import moment, { Moment } from 'moment';
import dateFnsParse from 'date-fns/parse';
import { DateUtils } from 'react-day-picker';
import dateFnsFormat from 'date-fns/format';
import { store } from '../store';
import { commonRequestError } from '../store/common/actions';
import { AuthActionTypes } from '../store/auth/types';
import httpStatus from '../commons/httpStatus';
export const formatNumber = (number: number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const GetTotalAmountOfMoney = (data: Array<any>) => {
  let sum = 0;
  for (let item of data) {
    if (item.amount !== '') {
      sum += parseInt(item.amount);
    }
  }
  return formatNumber(sum);
};

export const commonFilterList = (data: Array<any>, index: number) => {
  const newList = data.filter((item, indexNumber) => {
    if (indexNumber === index) {
      return false;
    } else {
      return item;
    }
  });
  return newList;
};

interface IRetrievedData {
  id: number;
  description: string;
  name: string;
  priority?: number;
  weighting: number;
  points: number;
  metrics?: Array<string>;
}

export const commonChangedList = (
  data: Array<any>,
  index: number,
  field: string,
  updatedValue: string,
  retrievedData: Array<IRetrievedData>,
) => {
  const chosenBusinessObjective = retrievedData.filter(
    (item: IRetrievedData) => item.name === updatedValue,
  );
  const newList = data.map((item, indexInList) => {
    if (indexInList === index) {
      if (field === 'amount') {
        return { ...item, [field]: updatedValue };
      }
      return {
        ...item,
        id: chosenBusinessObjective[0].id,
        [field]: updatedValue,
      };
    }
    return item;
  });
  return newList;
};

export const isWeekend = (date: string | Date | Moment) => {
  return moment(date).weekday() === 6 || moment(date).weekday() === 0;
};

export const a11yProps = (tabName: string, index: string | number) => {
  return {
    id: `${tabName}-${index}`,
    'aria-controls': `${tabName}panel-${index}`,
    value: index,
  };
};

/**
 * @param entityID
 * @param params:
 * * entity_type?: opportunity | task (undefined = both)
 * * comment_type?: default | review
 * @param lastTimeFresh
 * @param mustNot
 * @default ordered DESC by Created_at
 */
export const customQueryComment = (
  entityID: number | string,
  params?: Partial<
    Pick<IComment, 'comment_type' | 'entity_type' | 'comment_parent_id'>
  >,
  lastTimeFresh?: number,
  mustNot?: object[],
) => {
  const queryMust: object[] = params?.comment_type
    ? [
        {
          term: {
            comment_type: params.comment_type,
          },
        },
      ]
    : [];
  if (params?.comment_parent_id !== undefined) {
    queryMust.push({
      term: {
        comment_parent_id: params.comment_parent_id,
      },
    });
  }
  if (!params?.entity_type) {
    return {
      query: {
        bool: {
          must: [
            {
              term: {
                entity_id: entityID,
              },
            },
            {
              bool: {
                should: [
                  {
                    term: {
                      entity_type: 'opportunity',
                    },
                  },
                  {
                    term: {
                      entity_type: 'task',
                    },
                  },
                ],
              },
            },
            ...queryMust,
          ],
          must_not: mustNot ? [...mustNot] : [],
          should: [
            {
              term: {
                dummy: lastTimeFresh || 0,
              },
            },
          ],
        },
      },
      sort: [
        {
          created_at: {
            order: 'DESC',
          },
        },
      ],
    };
  }
  return {
    query: {
      bool: {
        must: [
          {
            term: {
              entity_id: entityID,
            },
          },
          {
            term: {
              entity_type: params.entity_type,
            },
          },
          ...queryMust,
        ],
        should: [
          {
            term: {
              dummy: lastTimeFresh || 0,
            },
          },
        ],
        must_not: mustNot ? [...mustNot] : [],
      },
    },
    sort: [
      {
        created_at: {
          order: 'DESC',
        },
      },
    ],
  };
};
/**
 * @param comment_parent_id
 * @param lastTimeFresh
 * @default ordered DESC by Created_at
 */
export const customChildCommentQuery = (
  id: number | string,
  lastTimeFresh?: number,
) => {
  return {
    query: {
      bool: {
        must: [
          {
            term: {
              comment_parent_id: id,
            },
          },
        ],
        should: [
          {
            term: {
              dummy: lastTimeFresh || 0,
            },
          },
        ],
      },
    },
  };
};
/**
 * @param comment_type
 * @param params:
 * * entity_type: help
 * * comment_type: feedback | string
 * * comment_body: search for keyword
 * @param lastTimeFresh
 * @param mustNot
 * @default ordered DESC by Created_at
 */
export const customQueryHelp = (
  comment_type: string,
  params?: Partial<Pick<IComment, 'comment_parent_id' | 'comment_body'>>,
  lastTimeFresh?: number,
  mustNot?: object[],
) => {
  const queryParams: object[] =
    params?.comment_parent_id !== undefined
      ? [
          {
            term: {
              comment_parent_id: params.comment_parent_id,
            },
          },
        ]
      : [];
  if (params?.comment_body) {
    queryParams.push({
      match: {
        comment_body: params.comment_body,
      },
    });
  }
  return {
    query: {
      bool: {
        must: [
          {
            term: {
              entity_id: 0,
            },
          },
          {
            term: {
              entity_type: 'help',
            },
          },
          {
            term: {
              comment_type,
            },
          },
          ...queryParams,
        ],
        should: [
          {
            term: {
              dummy: lastTimeFresh || 0,
            },
          },
        ],
        must_not: mustNot ? [...mustNot] : [],
      },
    },
  };
};

export const commonQueryOpportunity = (id: number) => {
  return {
    query: {
      bool: {
        must: [
          {
            term: {
              entity_id: id,
            },
          },
          {
            term: {
              entity_type: 'opportunity',
            },
          },
        ],
      },
    },
  };
};

export const customQuerySearchExpert = (keyword: string, index?: string) => {
  let must: object[] = [
    { term: { casbin_role: 'role:expert' } },
    {
      multi_match: {
        query: keyword,
        fields: ['name', 'profile_role.profile_role_name', 'created_by.name'],
        type: 'cross_fields',
        operator: 'and',
      },
    },
  ];
  if (index) {
    must.push({
      term: {
        _index: index,
      },
    });
  }
  return {
    query: {
      bool: {
        must: must,
      },
    },
  };
};
export const customQuerySearchOpportunity = (
  queryOpp: object[],
  keyword: string,
  index?: string,
) => {
  let must = [
    ...queryOpp,
    {
      multi_match: {
        query: keyword,
        fields: [
          'title',
          'description',
          'opportunity_type.name',
          'created_by.name',
        ],
        type: 'cross_fields',
        operator: 'and',
      },
    },
  ];
  if (index) {
    must.push({
      term: {
        _index: index,
      },
    });
  }
  return {
    query: {
      bool: {
        must: must,
      },
    },
  };
};
export const customQuerySearchFinding = (keyword: string, index?: string) => {
  let must: object[] = [
    {
      multi_match: {
        query: keyword,
        fields: ['title', 'description', 'created_by.name'],
        type: 'cross_fields',
        operator: 'and',
      },
    },
  ];
  if (index) {
    must.push({
      term: {
        _index: index,
      },
    });
  }
  return {
    query: {
      bool: {
        must: must,
      },
    },
  };
};
export const customCombineSearchQuery = (
  type: number,
  queryOpp: object[],
  keyword?: string,
) => {
  if (!keyword) return {};
  switch (type) {
    case 1: //opp
      return customQuerySearchOpportunity(queryOpp, keyword);
    case 2: //expert
      return customQuerySearchExpert(keyword);

    default:
      return customQuerySearchFinding(keyword);
  }
};
export const customCombineQueryDefault = (type: number, queryOpp: object) => {
  switch (type) {
    case 1: //opp
      return { query: queryOpp };
    case 2: //expert
      return {
        query: {
          term: { casbin_role: 'role:expert' },
        },
      };
    default:
      return {
        query: {
          bool: {},
        },
      };
  }
};
/**
 * @description: Navigate to opportunity details with query params
 * @param id
 * @param phase
 * @param state
 */
export const generateOpportunityLink = (
  id?: number,
  phase?: string,
  state?: string,
) => {
  if (!id) return '';
  return `/opportunity/${id}?phase=${phase ?? ''}&state=${state ?? ''}`;
};

export const checkRestrictedWord = (inputText: string) => {
  return inputText.replace(/[\s+|\n]{1,}/gm, '-').split('-').length;
};

export function capitalizeFirstLetter(text: PlaybookItemStatusT) {
  if (text == null) {
    return '';
  }
  return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
}

export const delay = (ms: number = 500) => {
  return new Promise((resolve, reject) => setTimeout(resolve, ms));
};

export const getRoleLabels = (roles: string | string[]) => {
  let labels: string[] = [];
  const roleLabelMap = new Map([
    ['role:user', 'User'],
    ['role:admin', 'Admin'],
    ['role:expert', 'Expert'],
    ['role:teamlead', 'Team Lead'],
    ['role:super-admin', 'Super Admin'],
  ]);
  if (typeof roles === 'string') {
    return [roleLabelMap.get(roles) ?? ''];
  }
  for (let item of roles) {
    if (roleLabelMap.has(item)) labels.push(roleLabelMap.get(item) as string);
  }
  return labels;
};

// 1 - Submitted,
// 2 - Sent-Success,
// 3 - Sent-Failure,
// 4 - Resent-Success,
// 5 - Resent-Failure;

const mailStatusNumber = [
  {
    status: 1,
    name: 'Submitted',
  },
  {
    status: 2,
    name: 'Sent-Success',
  },
  {
    status: 3,
    name: 'Sent-Failure',
  },
  {
    status: 4,
    name: 'Resent-Success',
  },
  {
    status: 5,
    name: 'Resent-Failure',
  },
];

export function genStringForMailingServiceStatus(status: number) {
  return mailStatusNumber.find(item => item.status === status)?.name ?? '';
}

export function genNotificationType(type?: string) {
  if (!type) return '';
  if (type === 'capture-sponsor-review') {
    return 'sponsor_review';
  }
  if (type === 'capture-peer-review') {
    return 'peer_review';
  }
  return '';
}

interface queryParam {
  key: string;
  value: string | number;
}
class HttpClientHelper {
  async get<T>(path: string, idParam?: number, query?: queryParam[]) {
    try {
      const endpoint = apiRouteGenerator(path, idParam, query);
      const response = await axios.get<T>(endpoint);
      return response.data;
    } catch (ex) {
      handleError(ex);
      return {} as T;
    }
  }

  async post<T, U>(
    path: string,
    payload: U,
    idParam?: number,
    headerConfig?: any,
  ) {
    try {
      const endpoint = apiRouteGenerator(path, idParam);
      const { data } = await axios.post<T>(endpoint, payload, headerConfig);
      return data;
    } catch (ex) {
      handleError(ex);
      return {} as T;
    }
  }

  async put<T, U>(path: string, payload: U, idParam?: number) {
    try {
      const endpoint = apiRouteGenerator(path, idParam);
      const { data } = await axios.put<T>(endpoint, payload);
      return data;
    } catch (ex) {
      handleError(ex);
      return {} as T;
    }
  }

  async patch<T, U>(path: string, payload: U, idParam?: number) {
    try {
      const endpoint = apiRouteGenerator(path, idParam);
      const { data } = await axios.patch<T>(endpoint, payload);
      return data;
    } catch (ex) {
      handleError(ex);
      return {} as T;
    }
  }
  async delete<T, U = {}>(path: string, payload?: U, idParam?: number) {
    try {
      const endpoint = apiRouteGenerator(path, idParam);
      const { data } = await axios.delete<T>(endpoint, { data: payload });
      return data;
    } catch (ex) {
      handleError(ex);
      return {} as T;
    }
  }
}

export function FormatNumber(num: number, decimals: number) {
  var t = Math.pow(10, decimals);
  return (
    Math.round(
      num * t +
        (decimals > 0 ? 1 : 0) *
          (Math.sign(num) * (10 / Math.pow(100, decimals))),
    ) / t
  ).toFixed(decimals);
}

export const ApiHelper = new HttpClientHelper();

function handleError(error: any) {
  if (error && error.status === httpStatus.StatusNotFound) return;
  const action =
    error && error.status === httpStatus.StatusUnauthorized
      ? AuthActionTypes.USER_LOGOUT
      : '';
  const msg =
    error && error.data ? error.data.message : 'Can not connect server';
  store.dispatch(commonRequestError({ message: msg, action: action }));
}
interface IParseDateProps {
  str: string;
  format: string;
  locale: number | Date;
}

function parseDate(date: IParseDateProps) {
  const parsed = dateFnsParse(date.str, date.format, date.locale);
  if (DateUtils.isDate(parsed)) {
    return parsed;
  }
  return undefined;
}

function formatDate(date: Date, format: string, locale: Locale) {
  return dateFnsFormat(date, format, { locale });
}

export const datePickerInputProps = {
  formatDate,
  format: 'dd-MM-yyyy',
  parseDate,
};
