import React from 'react';
import { StyledSelectedFilters } from './styled';
import { Button, Box } from '@material-ui/core';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  selectedItems: {
    backgroundColor: theme.palette.grey[100],
    marginRight: 3,
    paddingRight: 8,
    borderRadius: 5,
  },
  textSelectorItem: {
    color: theme.palette.grey.A100,
    fontSize: 13.6,
    fontFamily:
      '-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Noto Sans", sans-serif',
  },
  xButton: {
    borderLeftWidth: 1,
    borderLeftStyle: 'solid',
    borderLeftColor: theme.palette.grey.A200,
    paddingLeft: 8,
    marginLeft: 8,
  },
  buttonClearAll: {
    backgroundColor: theme.palette.grey[100],
    color: theme.palette.grey.A100,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 5,
  },
  textItems: {
    paddingRight: 5,
    paddingLeft: 5,
  },
}));

interface SelectedFiltersCustomProps {
  onClearSearch?: () => void;
}

export const SelectedFiltersCustom: React.FC<SelectedFiltersCustomProps> = ({
  onClearSearch,
}) => {
  const classes = useStyles();

  return (
    <StyledSelectedFilters
      innerClass={{
        button: 'buttonResult',
      }}
      render={props => {
        return (
          <Box>
            <Box display="flex" flexDirection="row" flexWrap="wrap">
              {Object.keys(props.selectedValues).map(component => {
                const { selectedValues, setValue } = props;
                const clearFilter = (component: string) => {
                  setValue(component, null);
                  if (component === 'Search') onClearSearch?.();
                };

                if (
                  !selectedValues[component].value ||
                  selectedValues[component].showFilter === false ||
                  selectedValues[component].value.length < 1
                ) {
                  return null;
                }

                return (
                  <Button
                    key={component}
                    onClick={() => clearFilter(component)}
                    size="small"
                    className={classes.selectedItems}
                  >
                    <Box component="a" className={classes.textSelectorItem}>
                      {() => {
                        switch (selectedValues[component].componentType) {
                          case 'DATERANGE':
                            return (
                              <Box display="flex" flexDirection="row">
                                <Box component="span">
                                  {selectedValues[component].label}:{' '}
                                  {moment(
                                    selectedValues[component].value[0],
                                  ).format('DD-MM-YYYY')}
                                </Box>
                                <Box mr={1}>,</Box>
                                <Box component="span">
                                  {moment(
                                    selectedValues[component].value[1],
                                  ).format('DD-MM-YYYY')}
                                </Box>
                              </Box>
                            );
                          case 'DATEPICKER':
                            return (
                              <Box display="flex" flexDirection="row">
                                <Box component="span">
                                  {selectedValues[component].label}:{' '}
                                  {moment(
                                    selectedValues[component].value,
                                  ).format('DD-MM-YYYY')}
                                </Box>
                                <Box mr={1}>,</Box>
                              </Box>
                            );
                          default:
                            return (
                              <Box>
                                {selectedValues[component].label}:
                                {typeof selectedValues[component].value ===
                                'string' ? (
                                  <Box
                                    component="span"
                                    className={classes.textItems}
                                  >
                                    {selectedValues[component].value}
                                  </Box>
                                ) : (
                                  selectedValues[component].value.map(
                                    (selected: string, index: number) => {
                                      return (
                                        <Box
                                          key={index}
                                          component="span"
                                          className={classes.textItems}
                                        >
                                          {selected},
                                        </Box>
                                      );
                                    },
                                  )
                                )}
                              </Box>
                            );
                        }
                      }}
                    </Box>
                    <Box component="span" className={classes.xButton}>
                      x
                    </Box>
                  </Button>
                );
              })}
              <Button
                size="small"
                onClick={() => {
                  props.clearValues();
                  onClearSearch?.();
                }}
                className={classes.buttonClearAll}
              >
                <Box component="a" className={classes.textSelectorItem}>
                  Clear All
                </Box>
              </Button>
            </Box>
          </Box>
        );
      }}
    />
  );
};
