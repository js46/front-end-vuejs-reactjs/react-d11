import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Box,
  Grid,
} from '@material-ui/core';

interface ConfirmDialogProps {
  header: string | JSX.Element;
  content?: string | JSX.Element;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  onConfirm?: () => void;
}
export const ConfirmDialog: React.FC<ConfirmDialogProps> = props => {
  const { header, content, open, setOpen, onConfirm } = props;
  return (
    <Dialog
      open={open}
      onClose={() => setOpen(false)}
      aria-labelledby="confirm-dialog"
    >
      <DialogTitle>
        <Box display="flex" alignItems="center">
          {header}
        </Box>
      </DialogTitle>
      <DialogContent dividers={false}>{content}</DialogContent>
      <DialogActions>
        {onConfirm ? (
          <div>
            <Button variant="outlined" onClick={() => setOpen(false)}>
              No
            </Button>
            <Button
              variant="contained"
              onClick={() => {
                setOpen(false);
                onConfirm();
              }}
              color="primary"
            >
              Yes
            </Button>
          </div>
        ) : (
          <Grid container justify="center">
            {' '}
            <Button
              color="default"
              variant="outlined"
              onClick={() => {
                setOpen(false);
              }}
            >
              OK
            </Button>
          </Grid>
        )}
      </DialogActions>
    </Dialog>
  );
};
