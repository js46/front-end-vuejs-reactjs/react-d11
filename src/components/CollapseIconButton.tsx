import React from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { IconButton } from '@material-ui/core';
import { useToggleValue } from '../hooks';
import { makeStyles } from '@material-ui/core/styles';

type CollapseIconButtonProps = {
  onToggle?: (isCollapse: boolean) => void;
  defaultCollapse?: boolean;
  disabled?: boolean;
};
export const CollapseIconButton: React.FC<CollapseIconButtonProps> = ({
  onToggle,
  defaultCollapse = true,
  disabled,
}) => {
  const [isCollapse, toggleCollapse] = useToggleValue(defaultCollapse);
  const classes = useStyles({ isCollapse });

  const onClick = () => {
    onToggle?.(!isCollapse);
    toggleCollapse();
  };

  return (
    <IconButton size="small" onClick={onClick} disabled={disabled}>
      <ArrowForwardIosIcon className={classes.icon} />
    </IconButton>
  );
};
const useStyles = makeStyles<any, { isCollapse: boolean }>({
  icon: {
    fontSize: 15,
    transition: '0.3s all',
    transform: props => (!props.isCollapse ? 'rotate(90deg)' : 'rotate(0deg)'),
  },
});
