import React from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { NotificationState } from '../../store/common/selector';
import { useSelector, useDispatch } from 'react-redux';
import { closeNotify } from '../../store/common/actions';
export type NotificationType = 'error' | 'warning' | 'info' | 'success';

export const Notification: React.FC = () => {
  const dispatch = useDispatch();
  const notificationItem = useSelector(NotificationState);
  const onClose = () => dispatch(closeNotify());
  return (
    <Snackbar
      open={!!notificationItem.open}
      onClose={onClose}
      autoHideDuration={notificationItem.duration}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
    >
      <Alert severity={notificationItem.type}>{notificationItem.message}</Alert>
    </Snackbar>
  );
};
