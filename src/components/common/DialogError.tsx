import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ErrorState } from '../../store/common/selector';
import { commonCloseError } from '../../store/common/actions';
import {
  Dialog,
  DialogContent,
  Box,
  DialogActions,
  Button,
  DialogTitle,
} from '@material-ui/core';
import { ErrorOutline } from '@material-ui/icons';
import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(1),
      },
      fontSize: 15,
    },
    dialogError: {
      //color: 'rgb(97, 26, 21)',
      //backgroundColor: 'rgb(253, 236, 234)',
    },
    center: {
      textAlign: 'center',
    },
  }),
);

export const DialogError: React.FC = () => {
  const dispatch = useDispatch();
  const error = useSelector(ErrorState);
  const closeDiaLog = () => {
    dispatch(commonCloseError());
    if (!!error.action) {
      dispatch({ type: error.action });
    }
  };
  const classes = useStyles();
  return (
    <Dialog
      onClose={() => closeDiaLog()}
      open={error.open}
      maxWidth="xs"
      aria-labelledby="form-dialog"
      fullWidth
    >
      <Box className={classes.dialogError}>
        <DialogTitle>
          <Box display="flex" alignItems="center">
            <Box component="span" mr={1}>
              <ErrorOutline color="error" />
            </Box>
            Error
          </Box>
        </DialogTitle>
        <DialogContent>
          <Box
            mb={2}
            display="flex"
            alignItems="center"
            justifyContent="center"
            className={classes.root}
          >
            {error.message}
          </Box>
        </DialogContent>
        <DialogActions>
          <Box mb={1} mr={1}>
            <Button onClick={closeDiaLog} variant="outlined">
              Ok
            </Button>
          </Box>
        </DialogActions>
      </Box>
    </Dialog>
  );
};
