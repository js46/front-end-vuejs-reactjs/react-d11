import * as React from 'react';
import { BarChart, Bar, XAxis, Tooltip, Cell } from 'recharts';
import { Box, Paper, Typography } from '@material-ui/core';
import { COLORS } from '../constants';
const BAR_WIDTH = 30;

interface CustomGanttChartItemProps {
  data: { name: string; disabled?: boolean }[]; // Array of dates
  activeColor?: string; // Fill color on active date
  title?: string; // Title on hover
  from?: Date; // Start chart from a date
  activeDates: string[]; // Active dates for highlight
  customTooltip?: { label: string; value: string | number }[]; // Custom tooltip on hover
}
const BAR_COLORS = {
  primary: '#2C54E3',
  secondary: '#d2d7ec',
  active: 'red',
  disabled: '#eee',
};
export const CustomGanttChartItem: React.FC<CustomGanttChartItemProps> = ({
  data,
  activeColor,
  title,
  activeDates,
  customTooltip,
}) => {
  const CustomTooltip = React.useCallback(
    ({ active }: any) => {
      if (active) {
        return (
          <Paper elevation={5} style={{ zIndex: 99, marginTop: -28 }}>
            <Box p={2} textAlign="left">
              <Typography variant="h6">{title}</Typography>
              {customTooltip?.map((item, index) => {
                return (
                  <Typography
                    component="p"
                    variant="body1"
                    style={{
                      fontSize: 13,
                      color: COLORS[0],
                    }}
                  >
                    {item.label}: {item.value}
                  </Typography>
                );
              })}
            </Box>
          </Paper>
        );
      }
      return null;
    },
    [customTooltip, title],
  );
  const fillActive = activeColor ?? BAR_COLORS.primary;

  return (
    <Box display="flex">
      {title && (
        <Box
          pr={1}
          minWidth={'120px'}
          width={'120px'}
          mt={'10px'}
          style={{
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
          }}
        >
          <Typography style={{ color: fillActive }}>{title}</Typography>
        </Box>
      )}
      <BarChart
        width={data.length * BAR_WIDTH - 20}
        height={BAR_WIDTH * 2.3}
        data={data}
        barCategoryGap={'0%'}
        barGap={0}
      >
        <XAxis dataKey="name" tick={{ fontSize: 13 }} />
        <Tooltip content={<CustomTooltip />} />
        <Bar dataKey="name" stackId="a">
          {data.map((item, key) => (
            <Cell
              key={key}
              strokeWidth={0}
              width={BAR_WIDTH}
              fill={
                item.disabled
                  ? BAR_COLORS.disabled
                  : activeDates.includes(item.name)
                  ? fillActive
                  : BAR_COLORS.secondary
              }
            />
          ))}
        </Bar>
      </BarChart>
    </Box>
  );
};
