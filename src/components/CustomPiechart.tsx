import React from 'react';
import { Pie, PieChart, ResponsiveContainer, Cell, Tooltip } from 'recharts';
import { COLORS } from '../constants';
import { Box, Typography } from '@material-ui/core';

export interface PieChartProps {
  data: IChartItem[];
  color?: string;
  type: string;
  title: string;
  pieLimit?: number;
}

export interface IChartItem {
  name: string;
  value: number;
  color?: string;
}

const RADIAN = Math.PI / 180;
interface labelProps {
  name: string;
  percent?: number;
  stroke: string;
  index?: number;
  textAnchor: string;
  x: number;
  y: number;
  [key: string]: any;
}

const renderCustomizedLabel: React.FC<labelProps> = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  if (percent === undefined) {
    percent = 0;
  }
  return (
    <text
      x={x}
      // dy={25}
      // dx={10}
      y={y}
      fill="white"
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline="central"
      fontSize={13}
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

export const PieChartComponent: React.FC<PieChartProps> = ({
  data,
  color,
  type,
  title,
  pieLimit,
}) => {
  let displayData = [];
  if (pieLimit && pieLimit < data.length) {
    let total = 0,
      limitSum = 0;
    for (let i = 0; i < pieLimit; i++) {
      limitSum += 1;
      displayData.push(data[i]);
    }
    for (let i = 0; i < data.length; i++) {
      total += 1;
    }
    displayData.push({ name: 'Other', value: total - limitSum });
  } else {
    displayData = data;
  }

  const width = window.innerWidth;

  return (
    <Box mb={3} pb={3}>
      <Box display="flex" justifyContent="center" mb={3}>
        <Typography
          style={{ fontSize: 14, color: '#424242', fontWeight: 'bold' }}
        >
          {title}
        </Typography>
      </Box>
      <ResponsiveContainer height={200} width={(width * 20) / 100}>
        <PieChart>
          <Tooltip />
          <Pie
            data={displayData}
            dataKey={type}
            labelLine={false}
            label={renderCustomizedLabel}
            outerRadius={100}
            fill={color ?? '#8884d8'}
            // paddingAngle={1}
          >
            {data.map((entry, index) => {
              return (
                <Cell
                  key={`cell-${index}`}
                  fill={entry.color ? entry.color : COLORS[index]}
                />
              );
            })}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    </Box>
  );
};
