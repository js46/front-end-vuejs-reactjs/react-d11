import React, { useState } from 'react';
import { Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

interface ITruncateText {
  text: string;
}

const useStyles = makeStyles(theme => ({
  actionButton: {
    color: theme.palette.grey[900],
    fontWeight: 'bold',
    marginLeft: 5,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  comment: {
    color: theme.palette.grey[500],
  },
}));
export const TruncateText: React.FC<ITruncateText> = ({ text }) => {
  const classes = useStyles();
  const [collapse, setCollapse] = useState(false);
  const { t } = useTranslation();
  if (text.length < 350) {
    return <Typography component="span">{text}</Typography>;
  }
  return collapse ? (
    <>
      <Typography component="span">{text}</Typography>
      <Typography
        className={classes.actionButton}
        onClick={() => setCollapse(false)}
      >
        {t('see_less')}
      </Typography>
    </>
  ) : (
    <>
      <Typography component="span">{`${text.slice(0, 350)}...`}</Typography>
      <Typography
        className={classes.actionButton}
        onClick={() => setCollapse(true)}
      >
        {t('see_more')}
      </Typography>
    </>
  );
};
