import {
  Box,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Tab,
  TableRow,
  Button,
  Menu,
} from '@material-ui/core';
import { styled, Theme } from '@material-ui/core/styles';

export enum TStringBoolean {
  TRUE = 'true',
  FALSE = 'false',
}

export const SectionBox = styled(Box)(
  (props: { theme: Theme; 'data-has-sidebar'?: TStringBoolean }) => ({
    padding: '8px 8px 8px 12px',
    marginLeft: props['data-has-sidebar'] === TStringBoolean.TRUE ? '256px' : 0,
  }),
);

export const MainBox = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
  minHeight: '100vh',
}));

export const WrapperBox = styled(Box)(({ theme }) => ({
  marginTop: theme.spacing(6),
  border: '1px solid #ccc',
  borderRadius: '6px',
  width: '100%',
  padding: '40px 25px',
}));

// export const Typography = styled(Typography)(
//   fontSize: '16px',
//   fontFamily: 'Helvetica Neue, Arial',
// );
export const TabFluid = styled(Tab)({
  flex: 1,
});

export const ExpansionPanelStyled = styled(ExpansionPanel)({
  '&.Mui-expanded': {
    margin: 0,
  },
});

export const ExpansionPanelSummaryStyled = styled(ExpansionPanelSummary)(
  ({ theme }) => ({
    borderBottom: `1px solid ${theme.palette.grey!['300']}`,
    '&.Mui-expanded': {
      backgroundColor: theme.palette.grey!['300'],
    },
    flexDirection: 'row-reverse',
    '& > .MuiExpansionPanelSummary-content': {
      marginLeft: 12,
    },
  }),
);

export const ExpansionPanelFilter = styled(ExpansionPanel)({
  margin: '0px!important',
  boxShadow: 'none',
  '&::before': {
    background: 'none!important',
  },
});

export const ExpansionPanelSummaryFilter = styled(ExpansionPanelSummary)({
  padding: '0px 7px 0px 0px',
  margin: '0px',
  '& > .MuiExpansionPanelSummary-content': {
    margin: 0,
    padding: 0,
  },
  '& > .MuiExpansionPanelSummary-expandIcon': {
    margin: '0px!important',
  },
});

export const ExpansionPanelDetailFilter = styled(ExpansionPanelDetails)({
  margin: '0px',
  padding: '0px 30px 0px 0px',
  fontSize: '14px!important',
  display: 'block',
});

export const StyledTableRow = styled(TableRow)(
  (props: { theme: Theme; title?: boolean }) => ({
    // style: (React.CSSProperties & false) | (React.CSSProperties & true);
    // backgroundColor: props.style ? props.theme.palette.grey[100] : 'red',
  }),
);

export const StyledButton = styled(Button)({
  padding: '0 20px',
});

export const StyledMenu = styled(Menu)(props => ({
  marginTop: props.theme.spacing(5),
  maxWidth: 600,
}));
