import {
  SingleDropdownList,
  MultiDropdownList,
  DatePicker,
  DateRange,
  MultiList,
  RangeSlider,
  DataSearch,
  SelectedFilters,
  ReactiveComponent,
  ReactiveList,
  SingleList,
} from '@appbaseio/reactivesearch';
import { styled } from '@material-ui/core/styles';

export const StyledSingleDropdownList = styled(SingleDropdownList)({
  '& button': {
    borderRadius: 6,
    minHeight: 36,
  },
  '& ul': {
    top: 36,
  },
});

export const StyledMultiDropdownList = styled(MultiDropdownList)({
  '& button': {
    borderRadius: 6,
    minHeight: 36,
  },
  '& ul': {
    top: 36,
  },
});

export const StyledDatePicker = styled(DatePicker)({
  marginBottom: 20,
  '& div': {
    borderRadius: '6px',
    border: 'none',
  },
  '& input': {
    border: '1px solid #ccc !important',
    borderRadius: '6px!important',
    height: '36px!important',
  },
});

export const StyledDateRange = styled(DateRange)(
  (props: { flexRow?: boolean }) => ({
    '& .dateMain': {
      width: '100%!important',
    },
    '& .input-date': {
      display:
        props['flexRow'] === true ? 'flex !important' : 'block !important',
      '& > div': {
        border: 'none !important',
        '& input': {
          borderRadius: '10px !important',
          border: '1px solid #ccc !important',
          marginBottom: props['flexRow'] ? 0 : '10px',
        },
        '&:nth-child(2)': {
          display: props['flexRow'] !== true ? 'none' : 'flex',
        },
      },
      border: 'none !important',
    },
  }),
);

export const StyledMultiList = styled(MultiList)({
  '& .multiList': {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
});
export const StyledRangSlider = styled(RangeSlider)({});
export const StyledDataSearch = styled(DataSearch)({
  '& .searchInput': {
    border: ' 1px solid #C2C2C2',
    borderRadius: '10px',
    background: 'none!important',
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
    height: '36px',
  },
  '& .titleSearch': {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
  '& .css-16nr2ca': {
    position: 'absolute',
    top: '10px',
  },
});
export const StyledSelectedFilters = styled(SelectedFilters)({
  '& .buttonResult': {
    border: '1px solid  #F4F4F4!important',
    borderRadius: '5px!important',
    backgroundColor: '#F4F4F4!important',
  },
});
export const StyledReactiveComponent = styled(ReactiveComponent)({});
export const StyledReactiveList = styled(ReactiveList)({
  '& .sortSelect': {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
    borderRadius: '6px',
    border: '1px solid #C2C2C2',
    outline: 'none',
    height: '36px!important',
    backgroundColor: '#FFF',
  },

  '& .resultsInfo': {
    '@media screen and (min-width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
    '@media screen and (width: 1024px)': {
      '&:nth-child(1)': {
        marginTop: '10px',
        marginBottom: '10px',
      },
    },
  },
  '& .buttonPagination': {
    borderRadius: '10px!important',
    backgroundColor: 'none!important',
    border: '1px solid #C2C2C2!important',
  },
  '& .css-fdtpv0': {
    backgroundColor: '#2C54E3',
  },
});
export const StyledSingleList = styled(SingleList)({
  '& .css-1664km7 + label::before, .css-1664km7 + label::after': {
    borderRadius: 2,
  },
  '& .css-1664km7:checked + label::before': {
    backgroundColor: '#0B6AFF',
  },
  '& .css-1664km7:checked + label::after': {
    transform: 'rotate(-45deg) scale(1)',
  },
  '& .css-1664km7 + label::after': {
    background: 'transparent',
    top: 10,
    left: 'calc(1px + 16px / 5)',
    width: 'calc(16px / 2)',
    height: 'calc(16px / 5)',
    marginTop: 'calc(16px / -2 / 2 * 0.8)',
    borderStyle: 'solid',
    borderColor: '#fff',
    borderWidth: '0 0 2px 2px',
    borderRadius: 0,
    borderImage: 'none',
  },
  '& span': {
    fontSize: '14px!important',
    fontFamily: 'Helvetica Neue,Arial',
  },
});
