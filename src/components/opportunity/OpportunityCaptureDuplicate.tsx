import React, { useState } from 'react';
import {
  Box,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  Typography,
} from '@material-ui/core';
import {
  ReactiveBase,
  ReactiveList,
  SelectedFilters,
} from '@appbaseio/reactivesearch';
import moment from 'moment';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import { IOpportunity } from '../../services/opportunity.services';
import { makeStyles } from '@material-ui/core/styles';
import { getComparator, stableSort } from '../../commons/util';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../store/config/selector';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(5),
  },
  notFoundIcon: {
    fontSize: '200px',
    margin: '10px 0',
  },
  opporTitle: {
    fontSize: '14px',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  mb20: {
    marginBottom: 20,
  },
  mb40: {
    marginBottom: 10,
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

interface Data {
  id: string;
  title: string;
  business_unit: string;
  current_experts: string;
  phase: string;
  last_update_at: string;
}
type Order = 'asc' | 'desc';
interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => void;
  order: Order;
  orderBy: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'Opp',
    disablePadding: true,
  },
  {
    id: 'title',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'business_unit',
    label: 'Business unit',
    disablePadding: true,
  },
  {
    id: 'current_experts',
    label: 'Experts',
    disablePadding: true,
  },
  {
    id: 'phase',
    label: 'Phase',
    disablePadding: true,
  },
  {
    id: 'last_update_at',
    label: 'Last updated',
    disablePadding: true,
  },
];

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>,
  ) => {
    onRequestSort(event, property);
  };
  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

export const OpportunityCaptureDuplicate: React.FC<{
  opportunity?: IOpportunity;
}> = ({ opportunity }) => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const classes = useStyles();
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof Data>('id');
  // const [isComment] = useState(false);
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };
  const customQuery = () => {
    return {
      query: {
        match: { title: opportunity?.title },
      },
    };
  };
  const { t } = useTranslation();
  const NoResponse: React.FC = () => {
    return (
      <Box>
        <Typography component="h1">{t('no_similar_or_duplicate')}</Typography>
        <SentimentVeryDissatisfiedIcon className={classes.notFoundIcon} />
      </Box>
    );
  };
  return (
    <ReactiveBase
      app={ES_INDICES.opportunity_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{ Authorization: window.localStorage.getItem('jwt') }}
    >
      <Grid container>
        <Grid item lg={12} xl={12} xs={12} sm={12}>
          <SelectedFilters />
          <ReactiveList
            componentId="SearchResult"
            dataField="title"
            loader="Loading comments..."
            showResultStats={false}
            renderNoResults={() => <></>}
            // size={10}
            // pages={5}
            defaultQuery={customQuery}
          >
            {({ data }) => {
              if (data && data.length === 1) {
                return <NoResponse />;
              }
              return (
                opportunity &&
                opportunity.id &&
                data &&
                data.length > 1 && (
                  <Grid container>
                    <Grid item xs={12}>
                      <Box className={classes.mb40}>
                        <Typography>
                          <Typography component="span">
                            {t('no_similar_or_duplicate_for')}
                          </Typography>
                          <Typography
                            component="span"
                            className={classes.opporTitle}
                          >{` ${opportunity?.title ?? ''}`}</Typography>
                        </Typography>
                        <Typography component="h1">
                          {t('click_opp_view_details')}
                        </Typography>
                      </Box>
                    </Grid>
                    <Grid item lg={12} xl={12} xs={12} sm={12}>
                      <TableContainer>
                        <Table>
                          <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                          />
                          <TableBody>
                            {stableSort(
                              data.filter(
                                (item: any) => item.id !== opportunity.id,
                              ),
                              getComparator(order, orderBy),
                            ).map((item: any, index: number) => (
                              <TableRow key={index}>
                                <TableCell component="th" scope="row">
                                  {item.id}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {item.title}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {item.business_units
                                    ? item.business_units.map(
                                        (bu: any, eindex: number) => (
                                          <div key={eindex}>{bu.name}</div>
                                        ),
                                      )
                                    : ''}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {item.current_experts
                                    ? item.current_experts.map(
                                        (expert: any, eindex: number) => (
                                          <div key={eindex}>{expert.name}</div>
                                        ),
                                      )
                                    : ''}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {item.opportunity_phase.name}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {moment(item.last_updated_at).format(
                                    'DD/MM/YYYY HH:mm:ss',
                                  )}
                                </TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </Grid>
                  </Grid>
                )
              );
            }}
          </ReactiveList>
        </Grid>
      </Grid>
    </ReactiveBase>
  );
};
