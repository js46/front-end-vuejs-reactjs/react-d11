import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import moment from 'moment';

import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  TableBody,
  Link,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { IOpportunity } from '../../services/opportunity.services';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
}));

interface Data {
  id: number;
  title: string;
  proposer: string;
  date_submitted: number;
  team: string;
  status_waiting: string;
}
interface HeadCell {
  id: keyof Data;
  label: string;
  disablePadding: boolean;
}
const headCells: HeadCell[] = [
  {
    id: 'id',
    label: 'Opp #',
    disablePadding: true,
  },
  {
    id: 'title',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'proposer',
    label: 'Proposer',
    disablePadding: true,
  },
  {
    id: 'date_submitted',
    label: 'Date submitted',
    disablePadding: true,
  },
  {
    id: 'team',
    label: 'Team',
    disablePadding: true,
  },
  {
    id: 'status_waiting',
    label: 'Status - waiting',
    disablePadding: true,
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes } = props;
  return (
    <TableHead classes={{ root: classes.headTable }}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            // sortDirection={orderBy === headCell.id ? order : false}
          >
            {headCell?.label ?? ''}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface OppDetailTableProps {
  opportunity?: IOpportunity;
  isDisableLink?: boolean;
  isSubmitAll?: boolean;
}

export const OppDetailTable: React.FC<OppDetailTableProps> = ({
  opportunity,
  isDisableLink,
  isSubmitAll,
}) => {
  const classes = useStyles();
  return (
    <Grid container>
      <Grid item xs={12}>
        <TableContainer className={classes.rootTableContainer}>
          <Table className={classes.rootTable}>
            <EnhancedTableHead classes={classes} />
            <TableBody>
              <TableRow>
                <TableCell component="th" scope="row">
                  {opportunity?.id ?? ''}
                </TableCell>
                <TableCell component="th" scope="row">
                  {isDisableLink ? (
                    <Typography component="p">
                      {opportunity?.title ?? ''}
                    </Typography>
                  ) : (
                    <Link
                      component={RouterLink}
                      color="inherit"
                      to={`/shape-opportunity/${opportunity?.id}`}
                    >
                      {opportunity?.title ?? ''}
                    </Link>
                  )}
                </TableCell>
                <TableCell component="th" scope="row">
                  {isDisableLink ? (
                    <Typography component="p">
                      {opportunity?.created_by?.name ?? ''}
                    </Typography>
                  ) : (
                    <Link
                      component={RouterLink}
                      color="inherit"
                      to={`/profile/${opportunity?.created_by?.id}`}
                    >
                      {opportunity?.created_by?.name ?? ''}
                    </Link>
                  )}
                </TableCell>
                <TableCell component="th" scope="row">
                  {!isSubmitAll && opportunity?.capture_submit_date
                    ? moment(opportunity?.capture_submit_date).format(
                        'DD/MM/YYYY, h:mma',
                      )
                    : moment(new Date()).format('DD/MM/YYYY')}
                </TableCell>
                <TableCell component="th" scope="row">
                  {opportunity?.resource_team?.name ?? ''}
                </TableCell>
                <TableCell component="th" scope="row">
                  {opportunity?.opportunity_state?.name ?? ''}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};
