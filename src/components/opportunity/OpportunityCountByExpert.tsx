import React from 'react';
import { MultiDropdownList, ReactiveBase } from '@appbaseio/reactivesearch';
import { Box } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../../store/config/selector';
import { isEqual } from 'lodash';

interface OpportunityCountByExpertProps {
  setOpportunityCount: (data: Map<number, number>) => void;
  opportunityCount?: Map<number, number>;
  isExpertMenu?: boolean;
}
export const OpportunityCountByExpert: React.FC<OpportunityCountByExpertProps> = React.memo(
  props => {
    const { setOpportunityCount, isExpertMenu, opportunityCount } = props;
    const ES_INDICES = useSelector(ConfigESIndex);
    const defaultInTeamExpertsQuery = () => {
      return {
        query: {
          bool: {
            must: [
              {
                term: {
                  delete_flg: false,
                },
              },
              {
                range: {
                  assignment_date: {
                    lte: new Date(),
                  },
                },
              },
              {
                range: {
                  un_assignment_date: {
                    gte: new Date(),
                  },
                },
              },
            ],
          },
        },
      };
    };
    const defaultExpertQuery = () => {
      return {
        query: {
          bool: {
            must: [
              {
                term: {
                  delete_flg: false,
                },
              },
              {
                range: {
                  un_assignment_date: {
                    gte: 'now',
                  },
                },
              },
              {
                range: {
                  assignment_date: {
                    lte: 'now+90d/d',
                  },
                },
              },
            ],
          },
        },
      };
    };

    return (
      <ReactiveBase
        app={ES_INDICES.opportunity_expert_index_name}
        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
        headers={{ Authorization: window.localStorage.getItem('jwt') }}
      >
        <Box display="none">
          <MultiDropdownList
            dataField={'expert.id'}
            componentId={'Opportunity Count by Expert'}
            showCount
            transformData={args => {
              let data = new Map<number, number>();
              if (args.length) {
                for (let item of args) {
                  data.set(parseInt(item.key), item.doc_count);
                }
              }
              if (!isEqual(opportunityCount, data)) setOpportunityCount(data);
              return args;
            }}
            defaultQuery={
              isExpertMenu ? defaultExpertQuery : defaultInTeamExpertsQuery
            }
            render={() => null}
            size={Math.pow(10, 4)}
          />
        </Box>
      </ReactiveBase>
    );
  },
);
