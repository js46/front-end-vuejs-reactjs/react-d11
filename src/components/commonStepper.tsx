import React from 'react';
import clsx from 'clsx';
import { Step, Stepper, StepLabel, StepConnector } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { StepIconProps } from '@material-ui/core/StepIcon';

const CustomConnector = withStyles({
  root: {
    left: 'calc(-50% + 10px)',
    right: 'calc(50% + 10px)',
    top: 10,
  },
  active: {
    '& $line': {
      borderTopWidth: '3px',
    },
  },
  completed: {
    '& $line': {
      borderTopWidth: '3px',
    },
  },
  line: {
    // borderColor: 'red',
  },
})(StepConnector);

const useStepIconStyles = makeStyles({
  root: {
    color: '#eaeaf0',
    display: 'flex',
    height: 22,
    alignItems: 'center',
  },
  active: {
    color: '#2C54E3',
    '& > div': {
      backgroundColor: '#2C54E3',
      border: '2px solid #2C54E3',
    },
  },
  circle: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    backgroundColor: '#fff',
    border: '2px solid #ccc',
  },
  completed: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    border: '2px solid #ccc',
    backgroundColor: '#ccc',
    zIndex: 1,
    fontSize: 18,
  },
});

function StepIcon(props: StepIconProps) {
  const classes = useStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? (
        <div className={classes.completed} />
      ) : (
        <div className={classes.circle} />
      )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  stepper: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

interface Stepper {
  stepsArray: string[];
  activeStep: number;
  completed: any;
  handleStep: (index: number) => void;
}
export const CommonStepper: React.FC<Stepper> = ({
  stepsArray,
  activeStep,
  completed,
  handleStep,
}) => {
  const classes = useStyles();

  function isStepComplete(step: number) {
    return completed.has(step);
  }
  return (
    <Stepper
      alternativeLabel
      activeStep={activeStep}
      connector={<CustomConnector />}
    >
      {stepsArray.map((label, index) => {
        const stepProps: { completed?: boolean } = {};
        return (
          <Step key={label} {...stepProps}>
            <StepLabel
              onClick={() => handleStep(index)}
              completed={isStepComplete(index)}
              className={isStepComplete(index) ? classes.stepper : ''}
              StepIconComponent={StepIcon}
            >
              {label}
            </StepLabel>
          </Step>
        );
      })}
    </Stepper>
  );
};
