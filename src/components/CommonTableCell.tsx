import React from 'react';
import { TableCell, Box, Typography } from '@material-ui/core';
import moment from 'moment';

export const CommonTableCell: React.FC<{
  value: string | number | Date | undefined;
  isDateValue?: boolean;
}> = ({ value, isDateValue }) => {
  let renderedValue: string | number | Date = '';
  if (isDateValue && value && value !== '0001-01-01T00:00:00Z') {
    renderedValue = moment(value).format('DD/MM/YYYY');
  }
  if (!isDateValue && value) {
    renderedValue = value;
  }
  if (!value || value === '0001-01-01T00:00:00Z') {
    renderedValue = '';
  }
  return (
    <TableCell>
      <Box>
        <Typography variant="body1" component="span">
          {renderedValue}
        </Typography>
      </Box>
    </TableCell>
  );
};
