import React, { Suspense } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { UserMenuFullState } from '../store/user/selector';
import { IsLoggedIn } from '../store/auth/selector';

export interface RouteWithLayoutProps {
  component: React.FC<any>;
  layout: React.FC<any>;
  path?: string | string[];
  from?: string;
  to?: string;
  exact?: boolean;
  protect: boolean;
  hasSidebar?: boolean;
  routePath?: string;
}

export const RouteWithLayout: React.FC<RouteWithLayoutProps> = props => {
  const {
    layout: Layout,
    component: Component,
    hasSidebar,
    path,
    protect,
    routePath,
    ...rest
  } = props;
  const isLoggedIn = useSelector(IsLoggedIn);
  const userMenuState = useSelector(UserMenuFullState);
  if (
    protect &&
    isLoggedIn &&
    userMenuState.is_fetch &&
    !userMenuState.menus.filter(item => {
      return item.includes(routePath ?? '');
    }).length
  ) {
    return <Redirect to="/401" />;
  }
  if (protect && !isLoggedIn) {
    return <Redirect to="/login" />;
  }
  return (
    <Route
      {...rest}
      render={matchProps => {
        return (
          <Layout hasSidebar={hasSidebar}>
            <Suspense fallback={<></>}>
              <Component {...matchProps} />
            </Suspense>
          </Layout>
        );
      }}
    />
  );
};
