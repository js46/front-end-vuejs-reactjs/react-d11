import React from 'react';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogProps,
  Typography,
} from '@material-ui/core';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import WarningOutlinedIcon from '@material-ui/icons/WarningOutlined';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import NotificationsOutlinedIcon from '@material-ui/icons/NotificationsOutlined';
import { makeStyles } from '@material-ui/core/styles';

interface MessageDialogProps extends DialogProps {
  mode?: 'default' | 'success' | 'warning' | 'error';
  message: string;
}
export const MessageDialog: React.FC<MessageDialogProps> = props => {
  const { mode, message } = props;
  const classes = useStyles();

  function renderDialogIcon() {
    switch (mode) {
      case 'success':
        return (
          <CheckCircleOutlineIcon
            className={`${classes.icon}`}
            color="primary"
          />
        );
      case 'warning':
        return <WarningOutlinedIcon className={classes.icon} color="primary" />;
      case 'error':
        return <ErrorOutlineIcon className={classes.icon} color="secondary" />;
      default:
        return (
          <NotificationsOutlinedIcon className={classes.icon} color="primary" />
        );
    }
  }

  function renderDialogTitle() {
    switch (mode) {
      case 'success':
        return (
          <Typography variant="h5" component="p" color="primary">
            Success!
          </Typography>
        );
      case 'warning':
        return (
          <Typography component="p" variant="caption">
            Warning!
          </Typography>
        );
      case 'error':
        return (
          <Typography variant="h5" component="p" color="secondary">
            Error!
          </Typography>
        );
      default:
        return (
          <Typography variant="h5" component="p" color="primary">
            Information
          </Typography>
        );
    }
  }

  return (
    <Dialog {...props}>
      <DialogContent>
        <Box
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          minWidth={250}
          maxWidth={500}
        >
          {renderDialogIcon()}
          {renderDialogTitle()}
          <Typography variant="body1" component="p" className={classes.message}>
            {message}
          </Typography>
          <Button
            variant="contained"
            color="primary"
            onClick={() => props.onClose?.({}, 'backdropClick')}
          >
            OK
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};
const useStyles = makeStyles({
  icon: {
    fontSize: 50,
    marginBottom: 6,
  },
  success: {
    color: '#0f9d58',
  },
  message: {
    marginTop: 20,
    paddingBottom: 15,
  },
});
