import React from 'react';
import { Box, Typography } from '@material-ui/core';

export class ErrorBoundary extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error: any, errorInfo: any) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
  }

  render() {
    if (this.state.errorInfo) {
      // Error path
      return (
        <Box mt={2}>
          <Typography component="h4" variant="h4">
            User doesn't exist
          </Typography>
        </Box>
      );
    }
    return this.props.children;
  }
}
