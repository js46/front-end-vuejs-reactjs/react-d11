import React from 'react';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
} from '@material-ui/core';
import { PictureAsPdfOutlined } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { PreviewFinding } from '../pages/Playbook/components/StepDetails/components/PreviewFinding';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { PlaybookItemFindingT } from '../services/playbook.services';
const useStyles = makeStyles(theme => ({
  actionDialog: {
    '& .MuiDialog-paper': {
      minWidth: 765,
    },
  },
}));

interface ExportFindingProps {
  open: any;
  close: () => void;
  finding?: PlaybookItemFindingT;
}

const MM: number = 0.2645833333;

export const ExportFinding: React.FC<ExportFindingProps> = ({
  open,
  close,
  finding,
}) => {
  const classes = useStyles();

  const getPDF = async () => {
    const findingID = document.getElementById('finding');
    const elsFinding = [
      document.getElementById('title-top'),
      document.getElementById('finding-title'),
      document.getElementById('private'),
      document.getElementById('finding-description'),
    ];
    console.log('elsFinding', elsFinding);
    if (findingID) {
      const Finding_Width = findingID.offsetWidth;
      const finding_top_left_margin = 12;
      const Finding_PDF_Width = Finding_Width + finding_top_left_margin * 2;
      const Finding_PDF_Height =
        Finding_PDF_Width * 1.5 + finding_top_left_margin * 2;

      let pdf = new jsPDF('p', 'mm', 'a4');
      pdf.setFontSize(10);
      pdf.setFontType('normal');
      pdf.setTextColor(51, 51, 51);
      pdf.setFont('times');
      let heightContentFinding = 0;

      if (elsFinding) {
        for (const el of elsFinding) {
          if (el) {
            if (
              heightContentFinding +
                el.offsetHeight +
                finding_top_left_margin / MM >=
              Finding_PDF_Height
            ) {
              const canvasElLast = await html2canvas(el, {
                allowTaint: false,
                useCORS: true,
                logging: true,
              });
              const imgDataElLast = canvasElLast.toDataURL('image/png');
              pdf.addPage();
              pdf.text('', 0, finding_top_left_margin);

              pdf.addImage(
                imgDataElLast,
                'PNG',
                finding_top_left_margin,
                finding_top_left_margin,
              );
              pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);

              if (el !== elsFinding[elsFinding.length - 1]) {
                heightContentFinding =
                  el.offsetHeight + finding_top_left_margin / MM;

                const canvas = await html2canvas(el, {
                  allowTaint: false,
                  useCORS: true,
                  logging: true,
                });
                const imgData = canvas.toDataURL('image/png');
                pdf.addImage(
                  imgData,
                  'PNG',
                  finding_top_left_margin,
                  heightContentFinding * MM,
                );
                pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);
              }
            } else {
              const canvas = await html2canvas(el, {
                allowTaint: false,
                useCORS: true,
                logging: true,
              });
              const imgData = canvas.toDataURL('image/png');
              pdf.addImage(
                imgData,
                'PNG',
                finding_top_left_margin,
                heightContentFinding * MM,
              );
              pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);
              heightContentFinding += el.offsetHeight;
            }
          }
        }
      }

      pdf.save(`Finding-${finding?.title}-${finding?.id}.pdf`);
    }
  };

  return (
    <>
      <Dialog
        open={open}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className={classes.actionDialog}
      >
        <DialogContent>
          <div id="finding">
            {finding && <PreviewFinding finding={finding} />}
          </div>
        </DialogContent>
        <DialogActions>
          <Box
            mt={1}
            mb={1}
            display="flex"
            justifyContent="flex-end"
            alignItems="flex-end"
          >
            <Button color="default" variant="outlined" onClick={close}>
              Cancel
            </Button>
            <Box ml={2} mr={4}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => getPDF()}
              >
                <PictureAsPdfOutlined /> <b>Export to PDF</b>
              </Button>
            </Box>
          </Box>
        </DialogActions>
      </Dialog>
    </>
  );
};
