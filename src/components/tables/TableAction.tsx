import React from 'react';
import { Box, IconButton } from '@material-ui/core';
import { EditIcon, DeleteIcon } from '../../assets/icon';
import { Visibility } from '@material-ui/icons';

interface TableActionProps {
  hiddenItem?: ('edit' | 'delete' | 'view')[];
  onClickEdit?: (
    event?: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => void;
  onClickDelete?: (
    event?: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => void;
  onClickView?: (
    event?: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => void;
  disabledEdit?: boolean;
  disabledDelete?: boolean;
  disabledView?: boolean;
  children?: React.ReactNode;
}

export const TableAction: React.FC<TableActionProps> = ({
  hiddenItem,
  onClickEdit,
  onClickDelete,
  disabledEdit,
  disabledDelete,
  children,
  onClickView,
  disabledView,
}) => {
  return (
    <Box display="flex" justifyContent="center">
      {!hiddenItem?.includes('view') && (
        <IconButton onClick={onClickView} disabled={disabledView}>
          <Visibility fill={disabledEdit ? 'gray' : undefined} />
        </IconButton>
      )}
      {!hiddenItem?.includes('edit') && (
        <IconButton onClick={onClickEdit} disabled={disabledEdit}>
          <EditIcon fill={disabledEdit ? 'gray' : undefined} />
        </IconButton>
      )}
      {children}
      {!hiddenItem?.includes('delete') && (
        <IconButton onClick={onClickDelete} disabled={disabledDelete}>
          <DeleteIcon fill={disabledDelete ? 'gray' : 'red'} />
        </IconButton>
      )}
    </Box>
  );
};
