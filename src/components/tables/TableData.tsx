import React, { useEffect, useState, MouseEvent } from 'react';
import { Grid, Box, TableContainer, Table, TableBody } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import { IListResponse, IQueryParam } from '../../services/common.services';
import { HeadCellProps, TableHeadSorter } from './TableHeadSorter';
import { useTableHeadSorter } from '../../hooks';
const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  rootTableContainer: {
    marginTop: 15,
  },
  rootTable: {
    minWidth: 732,
  },
  headTable: {
    minWidth: 732,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
}));

export interface TableDataProps<T> {
  fetchData: (params?: IQueryParam[]) => Promise<IListResponse<T>>;
  headCells: HeadCellProps<string>[];
  action?: boolean;
  setData: React.Dispatch<React.SetStateAction<T[]>>;
  body: string | JSX.Element[];
}
const PAGE_SIZE = 10;

export const TableData: React.FC<TableDataProps<any>> = ({
  headCells,
  fetchData,
  action,
  setData,
  body,
}) => {
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  const tableFieldsSort = headCells
    .filter(item => {
      return !item.disableSorting && item.id;
    })
    .map(item => item.id);
  const sortDefault = headCells.find(item => item?.defaultSort)?.id ?? '';
  const { order, orderBy, handleRequestSort } = useTableHeadSorter(
    tableFieldsSort,
    sortDefault,
  );

  useEffect(() => {
    let params: IQueryParam[] = [
      { key: 'page', value: page },
      { key: 'limit', value: PAGE_SIZE },
    ];
    if (order && orderBy) {
      params.push({ key: 'order', value: order });
      params.push({ key: 'order_by', value: orderBy });
    }
    fetchData(params).then(res => {
      if (res.success) {
        setData(res.data.list ?? []);
        setTotal(res.data.total);
      }
    });
  }, [fetchData, page, order, orderBy, setData]);

  const classes = useStyles();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const onRefreshData = () => {
    setPage(1);
  };

  const handleChangePage = (
    event: MouseEvent<HTMLButtonElement>,
    newPage: number,
  ) => {
    setPage(newPage);
  };
  const totalPages = Math.ceil(total / PAGE_SIZE);
  return (
    <>
      <Grid item xs={12}>
        <TableContainer className={classes.rootTableContainer}>
          <Table className={classes.rootTable}>
            <TableHeadSorter
              onRequestSort={handleRequestSort}
              order={order}
              orderBy={orderBy}
              cells={headCells}
              action={true}
            />
            <TableBody>{body}</TableBody>
          </Table>
        </TableContainer>
        {totalPages > 1 && (
          <Box display="flex" justifyContent="center" p={3}>
            <Pagination
              page={page}
              count={totalPages}
              variant="outlined"
              shape="rounded"
              onChange={handleChangePage}
            />
          </Box>
        )}
      </Grid>
    </>
  );
};
