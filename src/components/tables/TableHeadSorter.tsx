import React from 'react';
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  headTable: {
    minWidth: 732,
  },
  hidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 1,
    width: 1,
  },
  tableCell: {
    whiteSpace: 'nowrap',
  },
});
export enum SortOrder {
  ASC = 'asc',
  DESC = 'desc',
}
export interface HeadCellProps<T> {
  id: T;
  label: string;
  disablePadding?: boolean;
  disableSorting?: boolean;
  defaultSort?: boolean;
  width?: number;
  center?: boolean;
}
interface TableHeadSorterProps {
  onRequestSort: (event: React.MouseEvent<unknown>, property: any) => void;
  order: SortOrder;
  orderBy: string;
  cells: HeadCellProps<string>[];
  action?: boolean;
  public_status?: boolean;
}
export const TableHeadSorter: React.FC<TableHeadSorterProps> = React.memo(
  props => {
    const classes = useStyles();
    const {
      onRequestSort,
      order,
      orderBy,
      cells,
      action,
      public_status,
    } = props;
    const createSortHandler = (property: string) => (
      event: React.MouseEvent<unknown>,
    ) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead classes={{ root: classes.headTable }}>
        <TableRow>
          {cells.map(cell => (
            <TableCell
              key={cell.id}
              sortDirection={orderBy === cell.id ? order : false}
              align={cell.center ? 'center' : 'inherit'}
              style={{ width: cell.width ?? 'auto' }}
            >
              {cell.disableSorting ? (
                <label className={classes.tableCell}>{cell.label}</label>
              ) : (
                <TableSortLabel
                  active={orderBy === cell.id}
                  direction={orderBy === cell.id ? order : SortOrder.ASC}
                  onClick={createSortHandler(cell.id)}
                  className={classes.tableCell}
                >
                  {cell.label}
                  {orderBy === cell.id && (
                    <span className={classes.hidden}>
                      {order === SortOrder.DESC
                        ? 'sorted descending'
                        : 'sorted ascending'}
                    </span>
                  )}
                </TableSortLabel>
              )}
            </TableCell>
          ))}
          {public_status && <TableCell align="center">Public</TableCell>}
          {action && <TableCell align="center">Action</TableCell>}
        </TableRow>
      </TableHead>
    );
  },
);
