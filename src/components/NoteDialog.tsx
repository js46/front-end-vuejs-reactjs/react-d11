import React, { useRef, useState } from 'react';
import {
  Box,
  Typography,
  TextField,
  Button,
  DialogActions,
  Dialog,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { TinyMCEForm } from './form/TinyMCEForm';
const useStyles = makeStyles(theme => ({
  noteWrapper: {
    '& .tox .tox-statusbar': {
      display: 'none',
    },
  },
}));

interface NoteDialogProps {
  type: 'add' | 'edit';
  isOpen: boolean;
  noteContent: any;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleClose: () => void;
  handleSubmit: (content: any) => void;
  disabled?: boolean;
}

export const NoteDialog: React.FC<NoteDialogProps> = ({
  type,
  isOpen,
  noteContent,
  handleChange,
  handleClose,
  handleSubmit,
  disabled,
}) => {
  const classes = useStyles();
  const holdContentValueRef = useRef('');
  const [loadingEditor, setLoadingEditor] = useState(true);
  const handleChangeNoteContent = (content: string) => {
    holdContentValueRef.current = content;
  };

  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      fullWidth
      maxWidth="md"
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <Box padding={5}>
        <Box
          width="880px"
          height="550px"
          style={loadingEditor ? { display: 'flex' } : { display: 'none' }}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <CircularProgress />
        </Box>
        <Box mb={3} style={loadingEditor ? { display: 'none' } : undefined}>
          <Box textAlign="center" mb={3}>
            <Typography component="p" variant="h4">
              {type === 'add' ? 'Add Note' : 'Edit Note'}
            </Typography>
          </Box>
          <Box
            mb={2}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <Typography
              component="span"
              variant="caption"
              style={{ minWidth: '80px' }}
            >
              Name:
            </Typography>
            <TextField
              value={noteContent?.name ?? ''}
              name="note_name"
              fullWidth
              variant="outlined"
              onChange={handleChange}
            />
          </Box>
          <Box
            mb={2}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <Typography
              component="span"
              variant="caption"
              style={{ minWidth: '80px' }}
            >
              Author:
            </Typography>
            <TextField
              value={noteContent?.author ?? ''}
              name="note_author"
              fullWidth
              variant="outlined"
              onChange={handleChange}
              disabled={true}
            />
          </Box>
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            className={classes.noteWrapper}
          >
            <Typography
              component="span"
              variant="caption"
              style={{ minWidth: '80px' }}
            >
              Content:
            </Typography>
            <Box minHeight="400px" minWidth="90%">
              <TinyMCEForm
                value={noteContent?.content ?? ''}
                disabled={false}
                setLoadingEditor={setLoadingEditor}
                setValue={(content: string) => {
                  handleChangeNoteContent(content);
                }}
              />
            </Box>
          </Box>
        </Box>
        <DialogActions>
          <Button onClick={handleClose} variant="outlined">
            Cancel
          </Button>
          <Button
            onClick={() => {
              if (loadingEditor) {
                return;
              }
              handleSubmit(holdContentValueRef.current);
            }}
            color="primary"
            variant="contained"
            disabled={disabled}
          >
            {type === 'add' ? 'Add' : 'Update'}
          </Button>
        </DialogActions>
      </Box>
    </Dialog>
  );
};
