import React from 'react';
import Chart from 'react-google-charts';
import { Box } from '@material-ui/core';
import styled from 'styled-components';
import moment from 'moment';

const ChartStyled = styled(Chart)`
  svg {
    /* Background Color */
    g:nth-child(2) {
      rect {
        fill: transparent;
      }
    }

    /* Chart Settings */
    g:nth-child(3) {
      rect:nth-child(odd) {
        fill: white;
      } /* Rows Odd */
      rect:nth-child(even) {
        fill: transparent;
      } /* Rows Even */
      text {
        /* Hotizontal Labels */
        font-weight: normal !important;
      }
      line {
        /* Row Lines */
        stroke: transparent;
        stroke-width: 0;
      }
    }

    /* Arrows */
    g:nth-child(4) {
      path {
        stroke-width: 1;
        stroke: black;
      }
    }

    /* Shadow */
    g:nth-child(6) {
      rect {
        fill: black;
      }
    }

    /* Bars */
    g:nth-child(7) {
      rect:nth-child(1) {
        fill: #f4f4f4;
      }
      rect {
        fill: white;
      }
    }

    /* Percent Complete */
    g:nth-child(8) {
      path {
        fill: rgba(0, 0, 0, 0.2);
      }
    }

    /* Side Labels */
    g:nth-child(9) {
      text {
        /* fill: white; */
      }
    }

    /* Tooltips */
    g:nth-child(10) {
      rect {
        stroke: white;
      }
      text {
        fill: rgba(0, 0, 0, 0.6);
      }
      text:nth-child(2) {
        /* Date */
        /* fill: darkred; */
      }
    }
  }
`;

const columns = [
  { type: 'string', label: 'ID' },
  { type: 'string', label: 'Title' },
  { type: 'date', label: 'Start Date' },
  { type: 'date', label: 'End Date' },
  { type: 'number', label: 'Duration' },
  { type: 'number', label: 'Percentage Done' },
  { type: 'string', label: 'Dependencies' },
];

interface IDefaultGanttChartProps {
  startDate: Date;
  endDate: Date;
  duration?: number;
  assignmentDate?: Date | null;
}

export const DefaultGanttChart: React.FC<IDefaultGanttChartProps> = ({
  startDate,
  endDate,
  duration,
  assignmentDate,
}) => {
  const dataGanttChart = [
    [
      'Research',
      '',
      assignmentDate ? assignmentDate : startDate,
      duration
        ? moment(endDate)
            .add(duration, 'days')
            .toDate()
        : endDate,
      null,
      0,
      null,
    ],
  ];

  return (
    <Box>
      <ChartStyled
        chartType="Gantt"
        data={[columns, ...dataGanttChart]}
        width="100%"
        height={(dataGanttChart.length + 1) * 55}
        options={{
          gantt: {
            criticalPathEnabled: false,
            defaultStartDate: new Date(),
          },
        }}
      />
    </Box>
  );
};
