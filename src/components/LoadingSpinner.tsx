import React from 'react';
import { css } from '@emotion/core';
import BeatLoader from 'react-spinners/BeatLoader';

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

interface SubmmittingLoaderProps {
  color?: string;
}

export const SubmmittingLoader: React.FC<SubmmittingLoaderProps> = ({
  color = '#fff',
}) => {
  return (
    <div className="sweet-loading">
      <BeatLoader size={8} css={override} color={color} loading={true} />
    </div>
  );
};
