import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Box, Button, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  saveForLater: {
    textTransform: 'capitalize',
  },
}));

interface GroupButton {
  stepsArray: string[];
  activeStep: number;
  handleNextStep: () => void;
  handleBackStep: () => void;
  saveLaterEndpoint: string;
}

export const FooterGroupButton: React.FC<GroupButton> = ({
  stepsArray,
  activeStep,
  handleNextStep,
  handleBackStep,
  saveLaterEndpoint,
}) => {
  const classes = useStyles();
  return (
    <Grid container>
      <Grid item xs={6}>
        <Box
          display="flex"
          flexDirection="row"
          p={1}
          m={1}
          bgcolor="background.paper"
        >
          <Box p={1}>
            <Button variant="outlined">Cancel</Button>
          </Box>
          <Box p={1}>
            <Button
              variant="outlined"
              component={RouterLink}
              to={saveLaterEndpoint}
              // disabled={isDisableNext}
              className={classes.saveForLater}
            >
              Save For Later
            </Button>
          </Box>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <Box
          display="flex"
          flexDirection="row-reverse"
          p={1}
          m={1}
          bgcolor="background.paper"
        >
          <Box p={1}>
            <Button
              variant="contained"
              onClick={handleNextStep}
              // disabled={isDisableNext}
              color="primary"
            >
              {activeStep === stepsArray.length - 1 ? 'Submit' : 'Next'}
            </Button>
          </Box>
          <Box p={1}>
            <Button
              onClick={handleBackStep}
              variant="outlined"
              disabled={activeStep === 0}
            >
              Back
            </Button>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};
