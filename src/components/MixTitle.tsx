import React from 'react';
import clsx from 'clsx';
import { Box, Typography, Tooltip } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import HelpIcon from '@material-ui/icons/Help';
const useStyles = makeStyles<
  any,
  { textSmall?: boolean; noMargin?: boolean; bold?: boolean; large?: boolean }
>(theme => ({
  fieldMargin: {
    marginBottom: props => (props.noMargin ? 0 : props.large ? 0 : 5),
  },
  fieldTitle: {
    fontSize: 16,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  title: {
    fontSize: props => (props.textSmall ? 13 : props.bold ? 15 : 16),
    fontWeight: props => (props.bold ? 300 : 'bold'),
  },
  largeTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  header: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  required: {
    color: 'red',
  },
  helpIcon: {
    marginLeft: 20,
    marginBottom: -5,
    fill: '#ddd',
  },
  helpText: {
    marginLeft: 30,
    fontSize: '13px',
    color: '#6F747A',
  },
}));

const StyledTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: '#fff',
    color: 'rgba(0, 0, 0, 0.87)',
    // maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '2px solid #dadde9',
    padding: theme.spacing(2),
    '& .MuiTooltip-arrow': {
      color: '#ccc',
    },
  },
}))(Tooltip);

interface IMixTitle {
  isRequired?: boolean;
  isHelper?: boolean;
  title: string;
  isHeader?: boolean;
  helperText?: string | React.ReactNode; //tooltip hover on helper icon
  moreText?: string; //text description for below field
  textSmall?: boolean;
  noMargin?: boolean;
  bold?: boolean;
  large?: boolean;
}

export const MixTitle: React.FC<IMixTitle> = ({
  isRequired,
  isHelper,
  title,
  isHeader,
  helperText,
  moreText,
  textSmall,
  noMargin,
  bold,
  large,
}) => {
  const classes = useStyles({ textSmall, noMargin, bold, large });
  return (
    <Box className={classes.fieldMargin}>
      <Typography component="span" className={classes.fieldTitle}>
        <Typography
          className={
            large
              ? classes.largeTitle
              : clsx(classes.title, {
                  [classes.header]: isHeader,
                })
          }
        >
          {title}
        </Typography>

        {isRequired && (
          <Typography component="span" className={classes.required}>
            *
          </Typography>
        )}
        {moreText && (
          <Typography component="span" className={classes.helpText}>
            {moreText}
          </Typography>
        )}
      </Typography>
      {isHelper && helperText && (
        <StyledTooltip title={helperText} arrow>
          <HelpIcon className={classes.helpIcon} fontSize="small" />
        </StyledTooltip>
      )}
      {isHelper && !helperText && (
        <HelpIcon className={classes.helpIcon} fontSize="small" />
      )}
    </Box>
  );
};
