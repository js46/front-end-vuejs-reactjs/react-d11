import React, { useCallback, useState } from 'react';
import { Avatar, Box, Grid, Tooltip, Typography } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import UpdateIcon from '@material-ui/icons/Update';
import EventIcon from '@material-ui/icons/Event';
import { StateLabels, TaskState, TaskType } from '../constants';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import { kebabCase } from 'lodash';
import {
  DragDropContext,
  Draggable,
  DragStart,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import { getTask, IOpportunityTask } from '../services/task.services';
import theme from '../theme';
import { Link } from 'react-router-dom';
import { generateOpportunityLink } from '../utils';
import { useSelector } from 'react-redux';
import { IsAdmin, UserID } from '../store/user/selector';
import { useEffectOnlyOnce } from '../hooks';
import { TaskDetailDialog } from './TaskDetailDialog';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
  mb24: {
    marginBottom: theme.spacing(3),
  },
  disableTextDecoration: {
    textDecoration: 'none',
  },
  commonBox: {
    backgroundColor: '#fff',
    transition: 'all .3s',
    '&:hover': {
      boxShadow: '1px 1px 12px #ddd',
    },
  },
  avatar: {
    height: theme.spacing(3.5),
    width: theme.spacing(3.5),
    fontSize: 15,
  },
  bgSystem: {
    backgroundColor: theme.palette.primary.dark,
  },
  bgUser: {
    backgroundColor: theme.palette.secondary.dark,
  },
  iconText: {
    marginLeft: 6,
  },
  overlayDrop: {
    backgroundColor: 'rgb(210,228,250)',
    border: '2px dashed ' + theme.palette.primary.dark,
  },
  overlayDropHover: {
    backgroundColor: 'rgb(203,238,253)',
    border: '2px dashed #45C3EC',
  },
  overlayDropDisabled: {
    backgroundColor: '#fff',
    opacity: 0.6,
  },
  hashLink: {
    textDecoration: 'none',
  },
}));
const getDragItemStyle = (isDragging?: boolean) => ({
  backgroundColor: isDragging ? theme.palette.grey['300'] : '#fff',
});

interface TaskColumnProps {
  data: IOpportunityTask[];
  title: string;
  onClickItem?: (id: number) => void;
  first?: boolean;
  last?: boolean;
  state: TaskState;
  inDialog?: boolean;
  isDropDisabled?: boolean;
  linkOpportunity?: boolean;
}
const TaskColumn: React.FC<TaskColumnProps> = ({
  data,
  title,
  onClickItem,
  first,
  last,
  state,
  inDialog,
  isDropDisabled,
  linkOpportunity,
}) => {
  const classes = useStyles();
  // const paddingValue = first ? '0 15px 0 0' : last ? '0 0 0 15px' : '0 15px';
  const paddingValue = '0 15px';
  const border = last ? 'unset' : '1px dashed #c2c2c2';
  const containerHeight = `calc(100vh - 189px - ${inDialog ? 60 : 24}px)`;
  const height = `calc(100vh - 189px - 30px - ${inDialog ? 60 : 24}px)`;

  return (
    <Grid item container xs="auto" md>
      <Box
        display="flex"
        flexDirection="column"
        width="100%"
        borderRight={border}
        padding={paddingValue}
        height={containerHeight}
      >
        <Box
          borderRadius="6px"
          bgcolor="#f4f4f4"
          display="flex"
          justifyContent="space-between"
          width="100%"
          padding="10px 15px"
          minHeight="44px"
          id={'kanban-' + kebabCase(title)}
        >
          <Typography component="p" variant="h4">
            {title}
          </Typography>
          <Typography color="textSecondary" component="span" variant="body1">
            {data.length}
          </Typography>
        </Box>
        <Droppable droppableId={state} isDropDisabled={isDropDisabled}>
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              {...provided.droppableProps}
              style={{ position: 'relative' }}
            >
              <Box height={height} overflow="auto">
                {data.map((item, index: number) => {
                  return (
                    <Draggable
                      draggableId={`${item.id}`}
                      index={index}
                      key={item.id}
                      isDragDisabled={
                        item.task_type === TaskType.System ||
                        item.task_state === TaskState.Timeout
                      }
                    >
                      {(provided, snapshot) => {
                        return (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            onClick={() => onClickItem?.(item.id)}
                          >
                            <Box
                              borderRadius="6px"
                              border="2px solid #ddd"
                              padding="16px"
                              marginTop="16px"
                              className={classes.commonBox}
                              style={getDragItemStyle(snapshot.isDragging)}
                            >
                              <Box
                                display="flex"
                                flexDirection="row"
                                justifyContent="space-between"
                                alignItems="center"
                                pb={2}
                              >
                                {item.task_type === TaskType.System ? (
                                  <Tooltip
                                    title="Task generated by System"
                                    arrow
                                  >
                                    <Avatar
                                      className={`${classes.avatar} ${classes.bgSystem}`}
                                    >
                                      {item.task_type.substr(0, 1)}
                                    </Avatar>
                                  </Tooltip>
                                ) : (
                                  <Tooltip title="Task created by User" arrow>
                                    <Avatar
                                      className={`${classes.avatar} ${classes.bgUser}`}
                                    >
                                      {item.task_type.substr(0, 1)}
                                    </Avatar>
                                  </Tooltip>
                                )}
                                <Typography
                                  children={item.task_sub_type}
                                  color="primary"
                                />
                              </Box>
                              <Typography component="p" variant="h6">
                                {item.title ? item.title : item.task_type}
                              </Typography>
                              <Box
                                flexDirection="row"
                                alignItems="center"
                                display="flex"
                                marginTop={2}
                              >
                                <PersonIcon fontSize="small" />
                                <Typography
                                  component="p"
                                  variant="body2"
                                  className={classes.iconText}
                                >
                                  {item.assignee_user.name}
                                </Typography>
                              </Box>
                              <Box
                                flexDirection="row"
                                alignItems="center"
                                display="flex"
                                marginTop={0.4}
                              >
                                <UpdateIcon fontSize="small" />
                                <Typography
                                  component="p"
                                  variant="body2"
                                  className={classes.iconText}
                                >
                                  {moment(item?.last_updated_at).format(
                                    'DD/MM/YYYY, h:mma',
                                  )}
                                </Typography>
                              </Box>
                              {item?.requested_completion_date &&
                                item.requested_completion_date.toString() !==
                                  '0001-01-01T00:00:00Z' && (
                                  <Box
                                    flexDirection="row"
                                    alignItems="center"
                                    display="flex"
                                    marginTop={0.4}
                                  >
                                    <EventIcon
                                      fontSize="small"
                                      color="secondary"
                                    />
                                    <Typography
                                      component="p"
                                      variant="body2"
                                      className={classes.iconText}
                                      color="secondary"
                                    >
                                      {moment(
                                        item.requested_completion_date,
                                      ).format('DD/MM/YYYY')}
                                    </Typography>
                                  </Box>
                                )}

                              <Box
                                marginTop={2}
                                display="flex"
                                justifyContent="space-between"
                              >
                                <Box
                                  padding="3px 10px"
                                  bgcolor={'#45C3EC'}
                                  borderRadius="6px"
                                  color="#fff"
                                >
                                  {item?.opportunity?.phase
                                    ? item?.opportunity?.phase
                                    : 'empty'}
                                </Box>
                                {linkOpportunity ? (
                                  <Link
                                    to={generateOpportunityLink(
                                      item.opportunity?.id,
                                      item.opportunity?.phase,
                                      item.opportunity?.state,
                                    )}
                                    className={classes.hashLink}
                                  >
                                    <Typography
                                      color="textSecondary"
                                      variant="body2"
                                      component="span"
                                    >
                                      Oppor#{item.opportunity?.id}
                                    </Typography>
                                  </Link>
                                ) : (
                                  <Typography
                                    color="textSecondary"
                                    variant="body2"
                                    component="span"
                                  >
                                    Oppor#{item.opportunity?.id}
                                  </Typography>
                                )}
                              </Box>
                            </Box>
                          </div>
                        );
                      }}
                    </Draggable>
                  );
                })}
              </Box>
              {isDropDisabled !== undefined && (
                <Box
                  height={height}
                  top={6}
                  position="absolute"
                  width="100%"
                  borderRadius={6}
                  className={
                    isDropDisabled
                      ? classes.overlayDropDisabled
                      : snapshot.isDraggingOver
                      ? classes.overlayDropHover
                      : classes.overlayDrop
                  }
                />
              )}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </Box>
    </Grid>
  );
};

export const TaskKanban: React.FC<{
  data: IOpportunityTask[];
  todoTitle?: string;
  visibleStates?: TaskState[];
  onStateUpdated?: (id: number, newState: TaskState) => void;
  inDialog?: boolean;
  linkOpportunity?: boolean;
}> = ({
  data,
  todoTitle = 'To Do List',
  visibleStates,
  onStateUpdated,
  inDialog,
  linkOpportunity,
}) => {
  const isAdmin = useSelector(IsAdmin);
  const uid = useSelector(UserID);
  const taskStates = visibleStates ?? Object.values(TaskState);
  const [dropDisabled, setDropDisabled] = useState<TaskState[]>([]);
  const [draggingState, setDraggingState] = useState<TaskState | undefined>();
  const [disabledDragIDs, setDisabledDragIDs] = useState<number[]>([]);
  const [taskDetail, setTaskDetail] = useState<IOpportunityTask | undefined>();

  const onClick = async (taskId: number) => {
    const json = await getTask(taskId);
    setTaskDetail(json.data);
  };

  useEffectOnlyOnce(() => {
    // filter items not draggable
    if (!isAdmin) {
      let disabledIDs: number[] = [];
      for (let item of data) {
        // ** only creator or assignee can drag
        if (uid !== item.created_by_user?.id && uid !== item.assignee_user.id) {
          disabledIDs.push(item.id);
        }
      }
      setDisabledDragIDs(disabledIDs);
      console.log('disabledIDs', disabledIDs);
    }
  });

  function getStateLabel(state: TaskState): string {
    if (state === TaskState.Assigned) {
      return todoTitle ?? 'Assigned';
    }
    return StateLabels[state];
  }
  function onDragEnd(result: DropResult) {
    const { source, destination } = result;
    setDraggingState(undefined);
    setDropDisabled([]);
    if (!destination || source.droppableId === destination.droppableId) return;
    const taskID = +result.draggableId;
    if (!taskID) return;
    onStateUpdated?.(taskID, destination.droppableId as TaskState);
  }

  const onDragStart = useCallback(
    (start: DragStart) => {
      // console.log('Dragging item: ', start);
      setDraggingState(start.source.droppableId as TaskState);
      if (disabledDragIDs.includes(+start.draggableId)) {
        setDropDisabled([
          TaskState.Assigned,
          TaskState.InProgress,
          TaskState.Completed,
          TaskState.Cancelled,
          TaskState.Timeout,
        ]);
      } else {
        setDropDisabled([TaskState.Timeout]);
      }
    },
    [disabledDragIDs],
  );

  // console.log('[Task Kanban] rendering', data);
  data = data.map(item => {
    if (
      item.task_state === TaskState.Assigned &&
      moment(item.requested_completion_date).isBefore(moment(), 'day') &&
      item?.requested_completion_date?.toString() !== '0001-01-01T00:00:00Z'
    ) {
      item.task_state = TaskState.Timeout;
    }
    return item;
  });
  // console.log('[Task Kanban] filter timeout tasks', data);
  return (
    <>
      <DragDropContext onDragEnd={onDragEnd} onDragStart={onDragStart}>
        <Grid container>
          {taskStates.map((state, index) => (
            <TaskColumn
              state={state}
              data={data.filter(item => item.task_state === state)}
              title={getStateLabel(state)}
              onClickItem={onClick}
              first={index === 0}
              last={index === taskStates.length - 1}
              key={index}
              inDialog={inDialog}
              isDropDisabled={
                !draggingState || state === draggingState
                  ? undefined
                  : dropDisabled.includes(state)
              }
              linkOpportunity={linkOpportunity}
            />
          ))}
        </Grid>
      </DragDropContext>
      {!!taskDetail && (
        <TaskDetailDialog
          open={!!taskDetail}
          toggle={() => setTaskDetail(undefined)}
          task={taskDetail}
          hideOpportunityLink={!linkOpportunity}
        />
      )}
    </>
  );
};
