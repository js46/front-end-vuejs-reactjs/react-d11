import React from 'react';
import { css } from '@emotion/core';
import { useHistory, useLocation } from 'react-router-dom';
import { useEffectOnlyOnce } from '../hooks';
import { Box, Typography } from '@material-ui/core';
import FadeLoader from 'react-spinners/FadeLoader';

import { useSelector, useDispatch } from 'react-redux';
import { ConfigPubic } from '../store/config/selector';
import { IsLoggedIn } from '../store/auth/selector';
import { RequestUserLoginOkta } from '../store/auth/actions';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

export const OktaCallback = () => {
  let history = useHistory();
  const publicConfig = useSelector(ConfigPubic);
  const isLoggedIn = useSelector(IsLoggedIn);
  const nonce =
    'nonce=AmperfiicfEy1BNFONReV7bUYv1FJ35tK1a7ogMR8qTONBxSdHB8vvOGRrUVkO1Qns9NtGB';
  const client_id = `client_id=${publicConfig.okta_client_id}`;
  const redirect_uri = `redirect_uri=${publicConfig.okta_redirect_uri}`;
  const response_type = 'response_type=token';
  const state =
    'state=aYHoHJZNGYyESEEDTWJcs9vIEPwndt5V9XrHJYu9uUTVu9SEG8DwZtGMUX9l0eeT';
  const scope = 'scope=openid%20profile%20email';
  const prompt = 'prompt=consent';
  const authenticateLink = `${publicConfig.okta_login_url}/v1/authorize?${client_id}&${response_type}&${redirect_uri}&${state}&${nonce}&${scope}&${prompt}`;
  const query_string = new URLSearchParams(
    useLocation().hash.replace(/#/gm, '?'),
  );
  const dispatch = useDispatch();
  const location = useLocation();
  useEffectOnlyOnce(() => {
    if (isLoggedIn) {
      history.push('/');
    }
    if (!isLoggedIn && location.hash) {
      handleOktaAuthentication();
    }
    if (!isLoggedIn && !location.hash) {
      window.location.href = authenticateLink;
    }
  });

  const handleOktaAuthentication = async () => {
    const access_token = query_string.get('access_token');
    if (access_token) {
      dispatch(RequestUserLoginOkta(access_token));
      //const response = await oktaAuthentication(access_token);
      /* if (response.success && response.data) {
        window.localStorage.setItem('jwt', response.data.token!);
        history.push('/');
      } else {
        notification.openError(Message.Error);
      } */
    }
  };

  if (!location.hash) {
    return <></>;
  }
  return (
    <Box
      width="100vw"
      height="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Box>
        <FadeLoader css={override} color={'#2C54E3'} loading={true} />
        <Box mt={1}>
          <Typography component="p" variant="caption">
            Authenticating...
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};
