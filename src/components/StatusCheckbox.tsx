import { Checkbox } from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

interface StatusCheckboxProps {
  checked?: boolean;
}
export const StatusCheckbox: React.FC<StatusCheckboxProps> = ({ checked }) => {
  const classes = useStyles();
  return (
    <Checkbox
      checked={checked}
      disabled={true}
      color="primary"
      className={classes.item}
    />
  );
};
const useStyles = makeStyles({
  item: {
    padding: 0,
    '&:hover': {
      cursor: 'default',
    },
  },
});
