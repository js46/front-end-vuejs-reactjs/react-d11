import { Box } from '@material-ui/core';
import React from 'react';
import { Skeleton } from '@material-ui/lab';

interface CommentSkeletonProps {
  limit?: number;
}
export const CommentSkeleton: React.FC<CommentSkeletonProps> = ({ limit }) => {
  const list = new Array(limit ?? 1).fill('');

  return (
    <>
      {list.map((item, index) => (
        <Box display="flex" flex={1} pb={3} key={index}>
          <Skeleton animation="wave" variant="circle" width={35} height={35} />
          <Box display="flex" flexDirection="column" flex={1} ml={1}>
            <Skeleton
              animation="wave"
              variant="text"
              height={15}
              width={100}
              style={{ marginBottom: 10 }}
            />
            <Skeleton animation="wave" variant="text" height={20} />
            <Skeleton animation="wave" variant="text" height={20} />
          </Box>
        </Box>
      ))}
    </>
  );
};
