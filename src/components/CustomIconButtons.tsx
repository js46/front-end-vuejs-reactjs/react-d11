import React from 'react';
import { IconButton } from '@material-ui/core';
import { EditIcon, DeleteIcon } from '../assets/icon';
import { CancelOutlined, SaveOutlined } from '@material-ui/icons';

interface CustomIconButtonsProps {
  onClick?: () => void;
  disabled?: boolean;
  size?: 'small' | 'medium';
}

export const EditIconButton: React.FC<CustomIconButtonsProps> = ({
  onClick,
  size,
  disabled,
}) => (
  <IconButton size={size ?? 'small'} onClick={onClick} disabled={disabled}>
    <EditIcon />
  </IconButton>
);

export const DeleteIconButton: React.FC<CustomIconButtonsProps> = ({
  onClick,
  size,
  disabled,
}) => (
  <IconButton size={size ?? 'small'} onClick={onClick} disabled={disabled}>
    <DeleteIcon />
  </IconButton>
);

export const CancelIconButton: React.FC<CustomIconButtonsProps> = ({
  onClick,
  size,
  disabled,
}) => (
  <IconButton
    color="default"
    size={size ?? 'small'}
    onClick={onClick}
    disabled={disabled}
  >
    <CancelOutlined />
  </IconButton>
);

export const SaveIconButton: React.FC<CustomIconButtonsProps> = ({
  onClick,
  size,
  disabled,
}) => (
  <IconButton
    color="primary"
    size={size ?? 'small'}
    onClick={onClick}
    disabled={disabled}
  >
    <SaveOutlined />
  </IconButton>
);
