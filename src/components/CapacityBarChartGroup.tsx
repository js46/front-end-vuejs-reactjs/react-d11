import React from 'react';
import { CapacityBarChart, CapacityBarChartProps } from './CapacityBarChart';
import { Box } from '@material-ui/core';

interface CapacityBarChartGroupProps {
  dataGroup: CapacityBarChartProps[];
  maxWidth?: number;
}
export const CapacityBarChartGroup: React.FC<CapacityBarChartGroupProps> = ({
  dataGroup,
  maxWidth,
}) => {
  // console.log('maxWidth', maxWidth);
  return (
    <Box
      paddingTop={2}
      paddingBottom={2}
      maxWidth={maxWidth || '100%'}
      style={{
        overflowX: 'scroll',
        overflowY: 'hidden',
      }}
    >
      {dataGroup.map((group, index) => (
        <CapacityBarChart key={index} {...group} />
      ))}
    </Box>
  );
};
