import React, { useCallback, useEffect, useState } from 'react';
import { Box, Paper, Typography } from '@material-ui/core';
import { Bar, BarChart, Cell, LabelList, Tooltip, XAxis } from 'recharts';

const BAR_WIDTH = 40;
const BAR_COLORS = {
  primary: '#466bee',
  secondary: '#cad5fb',
  active: '#0f2a81',
  disabled: '#eee',
  overloaded: '#df9405',
};
export type CapacityBarChart = {
  name: string; // DD/MM
  occupied: number; // [0, 100]
  occupiedLabel: number | string;
  idle: number; // [0, 100]
  value: Date; // Date
  disabled?: boolean; // Weekend | holiday
  count?: number; // Opportunity count
  customTooltip?: string; // Override default tooltip
}[];
export interface CapacityBarChartProps {
  title?: string;
  data: CapacityBarChart;
  disabledSelect?: boolean;
  defaultRange?: number; // auto highlight a fixed date range
  onDurationChange?: (from?: Date, to?: Date) => void;
  defaultActiveIndex?: number;
}
export const CapacityBarChart: React.FC<CapacityBarChartProps> = React.memo(
  ({
    data,
    title,
    disabledSelect,
    onDurationChange,
    defaultRange,
    defaultActiveIndex,
  }) => {
    const [durationIndices, setDurationIndices] = useState<[number, number?]>();
    const renderLabel = useCallback((props: any) => {
      const { x, y, value } = props;
      // console.log('value', value);
      if (value === 0) return null;
      const styleValue = value < 100 ? value : 100;
      return (
        <svg>
          <text
            x={x + (styleValue > 9 ? BAR_WIDTH / 2 - 1 : BAR_WIDTH / 2)}
            y={y + (((styleValue / 100) * BAR_WIDTH) / 2 + 1)}
            fill="#fff"
            textAnchor="middle"
            dominantBaseline="middle"
            fontWeight="bold"
          >
            <tspan fontSize={10}>{value}</tspan>
          </text>
        </svg>
      );
    }, []);

    useEffect(() => {
      // Handle duration updated
      if (onDurationChange && !disabledSelect) {
        if (!durationIndices) {
          onDurationChange(undefined, undefined);
        } else {
          const from = data[durationIndices[0]].value;
          const to = durationIndices[1]
            ? data[durationIndices[1]].value
            : undefined;
          onDurationChange(from, to);
        }
      }
    }, [data, disabledSelect, durationIndices, onDurationChange]);

    // ** Auto highlight {defaultRange} starting from {defaultActiveIndex}
    useEffect(() => {
      if (defaultActiveIndex !== undefined && defaultRange) {
        let endIndex = defaultActiveIndex,
          range = defaultRange;
        while (range > 1) {
          endIndex += 1;
          if (!data[endIndex]?.disabled) range -= 1;
        }
        if (endIndex <= data.length - 1) {
          setDurationIndices([defaultActiveIndex, endIndex]);
        }
      }
    }, [data, defaultActiveIndex, defaultRange]);

    const getStrokeByIndex = useCallback(
      (index: number): string => {
        let color = 'transparent';
        if (durationIndices) {
          if (!durationIndices[1]) {
            if (index === durationIndices[0]) color = BAR_COLORS.active;
          } else {
            if (index >= durationIndices[0] && index <= durationIndices[1])
              color = BAR_COLORS.active;
          }
        }
        return color;
      },
      [durationIndices],
    );

    const handleClickCell = useCallback(
      (props: any) => {
        if (disabledSelect || !props) return;
        const index: number = props.activeTooltipIndex;
        if (data[index].disabled) return;
        // ** Case 1: defaultRange is set, then auto highlight a fixed range
        if (defaultRange) {
          let endIndex = index,
            range = defaultRange;
          while (range > 1) {
            endIndex += 1;
            if (!data[endIndex]?.disabled) range -= 1;
          }
          if (endIndex > data.length - 1) return false;
          return setDurationIndices([index, endIndex]);
        }
        // ** Case 2: defaultRange is undefined, then set from - to manually
        if (
          durationIndices &&
          durationIndices[0] >= 0 &&
          durationIndices[1] === undefined
        ) {
          if (index >= durationIndices[0]) {
            return setDurationIndices(prevState => [prevState![0], index]);
          }
        }
        return setDurationIndices([index, undefined]);
      },
      [data, defaultRange, disabledSelect, durationIndices],
    );

    return (
      <Box display="flex">
        {title && (
          <Box mt={'15px'} pr={1} minWidth={'80px'} width={'80px'}>
            <Typography style={{ color: BAR_COLORS.primary }}>
              {title}
            </Typography>
          </Box>
        )}
        <BarChart
          width={data.length * BAR_WIDTH}
          height={BAR_WIDTH + 40}
          data={data}
          // margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
          barCategoryGap={3}
          barSize={BAR_WIDTH}
          onClick={handleClickCell}
        >
          <XAxis dataKey="name" tick={{ fontSize: 12 }} />
          <Tooltip content={<CustomTooltip title={title} />} />
          <Bar
            isAnimationActive={false}
            dataKey="occupied"
            stackId="a"
            fill={BAR_COLORS.primary}
          >
            {data.map((entry, index) => {
              if (entry.disabled) {
                return (
                  <Cell
                    key={`idle-${index}`}
                    fill={BAR_COLORS.disabled}
                    stroke="transparent"
                    strokeWidth={0}
                    cursor="not-allowed"
                  />
                );
              }
              return (
                <Cell
                  key={`occupied-${index}`}
                  fill={
                    entry.occupied >= 100
                      ? BAR_COLORS.overloaded
                      : BAR_COLORS.primary
                  }
                  stroke={getStrokeByIndex(index)}
                  strokeWidth={2}
                />
              );
            })}
            <LabelList
              dataKey="occupiedLabel"
              position="center"
              content={renderLabel}
            />
          </Bar>
          <Bar dataKey="idle" stackId="a" fill={BAR_COLORS.secondary}>
            {data.map((entry, index) => {
              if (entry.disabled)
                return (
                  <Cell
                    key={`idle-${index}`}
                    fill={BAR_COLORS.disabled}
                    stroke="transparent"
                    strokeWidth={0}
                    cursor="not-allowed"
                  />
                );
              return (
                <Cell
                  key={`idle-${index}`}
                  fill={BAR_COLORS.secondary}
                  stroke={getStrokeByIndex(index)}
                  strokeWidth={2}
                />
              );
            })}
          </Bar>
        </BarChart>
      </Box>
    );
  },
);

const CustomTooltip = React.memo(({ active, payload, label, title }: any) => {
  if (active) {
    // console.log('hover', payload);
    return (
      <Paper elevation={3} style={{ marginTop: -25 }}>
        {payload[0].payload.customTooltip ? (
          <Box p={2} textAlign="left" minWidth="130px">
            <Typography variant="body1" component="p">
              <b>{label}</b>
            </Typography>
            <Typography variant="body2" component="p">
              <b>{payload[0].payload.customTooltip}</b>
            </Typography>
          </Box>
        ) : (
          <Box p={2} textAlign="left" minWidth="180px">
            <Typography variant="body1" component="p">
              <b>
                {title && title + "'s"} Capacity on {label}
              </b>
            </Typography>
            <Typography variant="body2" component="p">
              Based on effort
            </Typography>
            <Typography
              variant="subtitle1"
              style={{ color: BAR_COLORS.primary }}
              component="h5"
            >
              {payload[0].payload.occupiedLabel}% full
            </Typography>
            <Typography variant="body2" component="p">
              Opportunity count: <b>{payload[0].payload.count}</b>
            </Typography>
          </Box>
        )}
      </Paper>
    );
  }
  return null;
});
