import React from 'react';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';
import { Field, FieldProps, Form, Formik, FormikBag } from 'formik';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { useTranslation } from 'react-i18next';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { useSelector } from 'react-redux';
import { MixTitle } from './index';
import { StyledDataSearch } from './styled';
import { useDisableMultipleClick } from '../hooks';
import { ConfigESIndex } from '../store/config/selector';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  errorMsg: {
    color: theme.palette.secondary.main,
  },
}));

interface TaskFormDialogProps {
  open: boolean;
  toggle: () => void;
  onSubmit: (
    title: string,
    assignee: number,
    description?: string,
    dueDate?: Date,
  ) => void;
}

export const TaskFormDialog: React.FC<TaskFormDialogProps> = ({
  open,
  toggle,
  onSubmit,
}) => {
  const classes = useStyles();
  const ES_INDICES = useSelector(ConfigESIndex);
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();

  const validationSchema = () =>
    Yup.object().shape({
      title: Yup.string().required('Required'),
      description: Yup.string(),
      assignee: Yup.number().required('Required'),
      dueDate: Yup.date().nullable(),
    });
  const { t } = useTranslation();

  return (
    <Dialog open={open} onClose={toggle} maxWidth="sm" fullWidth>
      <Box mt={2} mb={1}>
        <Typography variant="h4" align="center">
          {t('create_task')}
        </Typography>
      </Box>
      <DialogContent style={{ minHeight: 560 }}>
        <Formik
          initialValues={{
            title: '',
            description: '',
            assignee: '',
            dueDate: null,
          }}
          validationSchema={validationSchema}
          onSubmit={async values => {
            if (isSubmitting) {
              return;
            }
            await debounceFn();
            await onSubmit(
              values.title,
              parseInt(values.assignee),
              values.description,
              values.dueDate ?? undefined,
            );
            endRequest();
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <Grid container direction="column" spacing={3}>
                <Grid item>
                  <MixTitle title="Title" isRequired />
                  <Field name="title">
                    {({ field }: { field: FieldProps }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        margin="normal"
                        {...field}
                        error={touched.title && !!errors?.title}
                      />
                    )}
                  </Field>
                  {errors.title && touched.title ? (
                    <Box mt={1}>
                      <Typography className={classes.errorMsg}>
                        {errors.title}
                      </Typography>
                    </Box>
                  ) : null}
                </Grid>
                <Grid item>
                  <MixTitle title="Description" />
                  <Field name="description">
                    {({ field }: { field: FieldProps }) => (
                      <TextField
                        {...field}
                        variant="outlined"
                        fullWidth
                        margin="normal"
                        multiline
                        rows={5}
                      />
                    )}
                  </Field>
                </Grid>
                <Grid item>
                  <MixTitle title="Assignee" isRequired />
                  <Field name="assignee">
                    {({
                      field,
                      form,
                    }: {
                      field: FieldProps;
                      form: FormikBag<any, any>;
                    }) => (
                      <ReactiveBase
                        app={ES_INDICES.user_profile_index_name}
                        url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
                        headers={{
                          Authorization: window.localStorage.getItem('jwt'),
                        }}
                      >
                        <StyledDataSearch
                          componentId="UserSelect"
                          dataField={['email', 'name']}
                          queryFormat="or"
                          placeholder="Search for user..."
                          showIcon={false}
                          showClear={true}
                          showDistinctSuggestions
                          innerClass={{
                            input: 'searchInput',
                            title: 'titleSearch',
                          }}
                          parseSuggestion={suggestion => ({
                            label: (
                              <div>
                                <Grid container direction={'column'}>
                                  <Grid item>
                                    <Box fontWeight="fontWeightBold">
                                      {suggestion.source?.name}
                                    </Box>
                                  </Grid>
                                  <Grid item>
                                    <span style={{ fontStyle: 'italic' }}>
                                      Email:{' '}
                                    </span>
                                    {suggestion.source?.email}
                                    {' | '}
                                    <span style={{ fontStyle: 'italic' }}>
                                      Business unit:{' '}
                                    </span>
                                    {
                                      suggestion.source?.business_unit
                                        ?.business_unit_name
                                    }
                                  </Grid>
                                </Grid>
                              </div>
                            ),
                            value: suggestion.source.email,
                            source: suggestion.source,
                          })}
                          onValueSelected={(value, cause, source) => {
                            form.setFieldValue(
                              'assignee',
                              source?.id ?? '',
                              true,
                            );
                          }}
                        />
                      </ReactiveBase>
                    )}
                  </Field>
                  {errors.assignee && touched.assignee ? (
                    <Box mt={1}>
                      <Typography className={classes.errorMsg}>
                        {errors.assignee}
                      </Typography>
                    </Box>
                  ) : null}
                </Grid>
                <Grid item>
                  <MixTitle title="Due date" />
                  <Field name="dueDate">
                    {({
                      field,
                      form,
                    }: {
                      field: any;
                      form: FormikBag<any, any>;
                    }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          disableToolbar
                          disablePast
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          id="due-date"
                          label=""
                          value={field.value}
                          onChange={(date: MaterialUiPickersDate) =>
                            form.setFieldValue(field.name, date)
                          }
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                          error={touched.dueDate && !!errors.dueDate}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  </Field>
                </Grid>
                <Grid item>
                  <Box
                    display="flex"
                    flexDirection="row"
                    justifyContent="flex-end"
                    mt={1.6}
                  >
                    <Button
                      type="reset"
                      onClick={toggle}
                      style={{ marginRight: 6 }}
                      variant="outlined"
                    >
                      Cancel
                    </Button>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      disabled={isSubmitting}
                    >
                      Save
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </DialogContent>
    </Dialog>
  );
};
