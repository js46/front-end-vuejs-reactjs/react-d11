import React, { ReactElement, useEffect, useState } from 'react';
import {
  MultiDropdownList,
  ReactiveBase,
  ReactiveList,
} from '@appbaseio/reactivesearch';
import { Box } from '@material-ui/core';
import { IComment } from '../services/comment.services';
import { isEqual } from 'lodash';
// import { useTranslation } from 'react-i18next';
import { customQueryHelp } from '../utils';
import { CommentListItem } from './CommentListItem';
import { useSelector } from 'react-redux';
import { ConfigESIndex } from '../store/config/selector';

interface CommentReactiveListProps {
  defaultQuery?: (...args: any[]) => any;
  canEdit?: boolean;
  canReply?: boolean;
  loader?: ReactElement;
}

export const CommentReactiveList: React.FC<CommentReactiveListProps> = props => {
  const ES_INDICES = useSelector(ConfigESIndex);
  const { defaultQuery, canEdit, canReply, loader } = props;
  const [data, setData] = useState<IComment[]>([]);
  const [replyCount, setReplyCount] = useState<Map<number, number>>();
  const [loadingCount, setLoadingCount] = useState(true);

  useEffect(() => {
    setLoadingCount(false);
  }, [replyCount]);

  // const { t } = useTranslation();
  const defaultQueryCount = () =>
    customQueryHelp('feedback', undefined, 0, [
      {
        term: {
          comment_parent_id: 0,
        },
      },
    ]);

  return (
    <ReactiveBase
      app={ES_INDICES.comment_index_name}
      url={`${process.env.REACT_APP_API_URL}/api/v1/search/es`}
      headers={{
        Authorization: window.localStorage.getItem('jwt'),
      }}
    >
      {canReply && (
        <Box display="none">
          <MultiDropdownList
            dataField={'comment_parent_id'}
            componentId={'Comment Reply count'}
            defaultQuery={defaultQueryCount}
            showCount
            transformData={args => {
              let countData = new Map();
              if (args && args.length) {
                for (let item of args) {
                  countData.set(parseInt(item.key), item.doc_count);
                }
              }
              if (!isEqual(countData, replyCount)) setReplyCount(countData);
              return args;
            }}
            render={() => null}
          />
        </Box>
      )}
      <ReactiveList
        componentId="Comment List"
        dataField="created_at"
        sortBy="desc"
        renderResultStats={() => null}
        renderNoResults={() => null}
        defaultQuery={defaultQuery}
        showLoader={!!loader}
        loader={loader}
        scrollOnChange={false}
        stream={false}
        onData={result =>
          !isEqual(result.data, data) ? setData(result.data) : undefined
        }
      >
        {({ loading }) => {
          if (loadingCount || (loading && loader)) return null;
          // if (!data.length)
          //   return (
          //     <Typography component="p" variant="body2">
          //       {t('no_data')}
          //     </Typography>
          //   );

          return (
            <React.Fragment>
              {data?.map((item: IComment, index) => (
                <CommentListItem
                  data={item}
                  canEdit={canEdit}
                  canReply={canReply}
                  key={item.id}
                  bordered={index > 0}
                  replyCount={replyCount?.get(item.id) ?? 0}
                />
              ))}
            </React.Fragment>
          );
        }}
      </ReactiveList>
    </ReactiveBase>
  );
};
