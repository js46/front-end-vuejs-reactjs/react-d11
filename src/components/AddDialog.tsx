import React from 'react';
import { Dialog, Box, DialogActions, Button } from '@material-ui/core';

interface AddDialogProps {
  isOpen: boolean;
  message: string | JSX.Element;
  handleClose: () => void;
  handleAdd: () => void;
  disable: boolean;
}
export const AddDialog: React.FC<AddDialogProps> = ({
  isOpen,
  message,
  handleClose,
  handleAdd,
  disable,
}) => {
  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <Box padding={4} minWidth="578px">
        <Box mb={2}>{message}</Box>
        <DialogActions>
          <Button onClick={handleClose} color="default" variant="outlined">
            Cancel
          </Button>
          <Button
            onClick={handleAdd}
            color="primary"
            variant="contained"
            disabled={disable}
          >
            Add
          </Button>
        </DialogActions>
      </Box>
    </Dialog>
  );
};
