import { Box } from '@material-ui/core';
import React from 'react';

interface ESLoader {
  pageSize?: number;
}
export const ESLoader: React.FC<ESLoader> = ({ pageSize }) => {
  pageSize = pageSize ?? 10;
  return (
    <Box
      display="flex"
      justifyContent="center"
      mt={2}
      mb={2}
      // minHeight={pageSize * 44.5 + 'px'}
    >
      Loading...
    </Box>
  );
};
