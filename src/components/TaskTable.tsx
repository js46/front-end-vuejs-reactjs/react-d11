import React, { useState } from 'react';
import {
  Box,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@material-ui/core';
import moment from 'moment';
import { getTask, IOpportunityTask } from '../services/task.services';
import { makeStyles } from '@material-ui/core/styles';
import {
  HeadCellProps,
  SortOrder,
  TableHeadSorter,
} from './tables/TableHeadSorter';
import TaskStateChip from './TaskStateChip';
import { TaskDetailDialog } from './TaskDetailDialog';

interface TaskTableProps {
  data: IOpportunityTask[];
  onRequestSort: (e: any, field: any) => void;
  order: SortOrder;
  orderBy: string;
  styleContainer?: React.CSSProperties;
  clickDisabled?: boolean;
}
const headCells: HeadCellProps<string>[] = [
  {
    id: 'title.keyword',
    label: 'Title',
    disablePadding: true,
  },
  {
    id: 'task_type.keyword',
    label: 'Type',
    disablePadding: true,
  },
  {
    id: 'entity_type.keyword',
    label: 'Entity',
    disablePadding: true,
  },
  {
    id: 'assignee_user.name.keyword',
    label: 'Assignee',
    disablePadding: true,
  },
  {
    id: 'created_by_user.name.keyword',
    label: 'Created by',
    disablePadding: true,
  },
  {
    id: 'created_at',
    label: 'Assigned at',
    disablePadding: true,
  },
  {
    id: 'last_updated_at',
    label: 'Updated at',
    disablePadding: true,
  },
  {
    id: 'requested_completion_date',
    label: 'Due date',
    disablePadding: true,
  },
  {
    id: 'task_state.keyword',
    label: 'State',
    disablePadding: true,
  },
];
export const TaskTable: React.FC<TaskTableProps> = ({
  data,
  onRequestSort,
  orderBy,
  order,
  styleContainer,
  clickDisabled,
}) => {
  const classes = useStyles();
  const [taskDetail, setTaskDetail] = useState<IOpportunityTask | undefined>();

  const onClick = (taskId: number) => async () => {
    if (clickDisabled) return;
    const json = await getTask(taskId);
    setTaskDetail(json.data);
  };

  return (
    <>
      <Box style={styleContainer}>
        <Grid container>
          <TableContainer>
            <Table>
              <TableHeadSorter
                onRequestSort={onRequestSort}
                order={order}
                orderBy={orderBy}
                cells={headCells}
              />
              <TableBody>
                {data.map(item => (
                  <TableRow
                    hover
                    key={item.id}
                    onClick={onClick(item.id)}
                    className={classes.rowLink}
                  >
                    <TableCell>{item.title}</TableCell>
                    <TableCell>{item.task_type}</TableCell>
                    <TableCell>
                      {item?.entity_type
                        ? item?.entity_type === 'opportunity_playbook_item'
                          ? 'Opportunity Playbook Item ' + item?.entity_id
                          : `${item?.entity_type.charAt(0).toUpperCase() +
                              item?.entity_type.slice(1)} ${item?.entity_id}`
                        : ''}
                    </TableCell>
                    <TableCell>{item.assignee_user?.name}</TableCell>
                    <TableCell>{item.created_by_user?.name}</TableCell>
                    <TableCell>
                      {moment(item.created_at).format('DD/MM/YYYY, h:mma')}
                    </TableCell>
                    <TableCell>
                      {moment(item.last_updated_at).format('DD/MM/YYYY, h:mma')}
                    </TableCell>
                    <TableCell>
                      {item?.requested_completion_date?.toString() !==
                      '0001-01-01T00:00:00Z'
                        ? moment(item.requested_completion_date).format(
                            'DD/MM/YYYY',
                          )
                        : '-'}
                    </TableCell>
                    <TableCell>
                      <TaskStateChip taskState={item.task_state} />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Box>
      {!!taskDetail && (
        <TaskDetailDialog
          open={!!taskDetail}
          toggle={() => setTaskDetail(undefined)}
          task={taskDetail}
          hideOpportunityLink
        />
      )}
    </>
  );
};
const useStyles = makeStyles({
  rowLink: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
});
