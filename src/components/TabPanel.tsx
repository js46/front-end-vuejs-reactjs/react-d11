import React from 'react';
import { Box } from '@material-ui/core';
import theme from '../theme';

interface TabPanelProps {
  children?: React.ReactNode;
  index: string | number;
  value: string | number;
  noOutlined?: boolean;
}

export const TabPanel: React.FC<TabPanelProps> = props => {
  const { children, value, index, noOutlined, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      style={{ marginTop: '16px' }}
      {...other}
    >
      {value === index && (
        <Box
          style={{
            border: noOutlined ? '' : `1px solid ${theme.palette.grey['300']}`,
            borderRadius: 6,
          }}
        >
          {children}
        </Box>
      )}
    </div>
  );
};
