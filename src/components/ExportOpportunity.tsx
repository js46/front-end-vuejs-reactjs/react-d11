import React from 'react';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
} from '@material-ui/core';
import { PictureAsPdfOutlined } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { ViewDetailOpp } from '../pages/Opportunity/components/ViewDetailOpp';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { IOpportunity } from '../services/opportunity.services';

const useStyles = makeStyles(theme => ({
  actionDialog: {
    '& .MuiDialog-paper': {
      minWidth: 765,
    },
  },
}));

interface ExportOpportunityProps {
  open: any;
  close: () => void;
  opportunity: IOpportunity;
}

const MM: number = 0.2645833333;

export const ExportOpportunity: React.FC<ExportOpportunityProps> = ({
  open,
  close,
  opportunity,
}) => {
  const classes = useStyles();

  const getPDF = async () => {
    const capture = document.getElementById('capture');
    const shape = document.getElementById('shape');
    const elsCapture = [
      document.getElementById('title-top'),
      document.getElementById('title-capture'),
      document.getElementById('capture-proposer'),
      document.getElementById('capture-description'),
      document.getElementById('capture-hypothesis'),
      document.getElementById('capture-bu'),
      document.getElementById('capture-bp'),
      document.getElementById('capture-completion-date'),
      document.getElementById('capture-private'),
      document.getElementById('capture-alignment'),
      document.getElementById('capture-bf-category'),
      document.getElementById('capture-metrics'),
    ];
    const elsShape = [
      document.getElementById('title-top'),
      document.getElementById('title-shape'),
      document.getElementById('opp-definition'),
      document.getElementById('dataset-required'),
      document.getElementById('opp-parameters'),
      document.getElementById('title-resource'),
    ];
    opportunity?.opportunity_roles?.map((item, index) => {
      if (item) {
        elsShape.push(document.getElementById(`role-${index}`));
      }
    });
    elsShape.push(document.getElementById('review-summary'));
    elsShape.push(document.getElementById('priority-score'));

    if (capture && shape) {
      const Capture_Width = capture.offsetWidth;
      const capture_top_left_margin = 12;
      const Capture_PDF_Width = Capture_Width + capture_top_left_margin * 2;
      const Capture_PDF_Height =
        Capture_PDF_Width * 1.5 + capture_top_left_margin * 2;

      let pdf = new jsPDF('p', 'mm', 'a4');
      pdf.setFontSize(10);
      pdf.setFontType('normal');
      pdf.setTextColor(51, 51, 51);
      pdf.setFont('times');
      // pdf.setFontType("italic");
      let heightContentCapture = 0;
      let heightContentShape = 0;

      if (elsCapture) {
        for (const el of elsCapture) {
          if (el) {
            if (
              heightContentCapture +
                el.offsetHeight +
                capture_top_left_margin / MM >=
              Capture_PDF_Height
            ) {
              const canvasElLast = await html2canvas(el, {
                allowTaint: false,
                useCORS: true,
                logging: true,
              });
              const imgDataElLast = canvasElLast.toDataURL('image/png');
              pdf.addPage();
              pdf.text('', 0, capture_top_left_margin);

              pdf.addImage(
                imgDataElLast,
                'PNG',
                capture_top_left_margin,
                capture_top_left_margin,
              );
              pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);

              if (el !== elsCapture[elsCapture.length - 1]) {
                heightContentCapture =
                  el.offsetHeight + capture_top_left_margin / MM;

                const canvas = await html2canvas(el, {
                  allowTaint: false,
                  useCORS: true,
                  logging: true,
                });
                const imgData = canvas.toDataURL('image/png');
                pdf.addImage(
                  imgData,
                  'PNG',
                  capture_top_left_margin,
                  heightContentCapture * MM,
                );
                pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);
              }
            } else {
              const canvas = await html2canvas(el, {
                allowTaint: false,
                useCORS: true,
                logging: true,
              });
              const imgData = canvas.toDataURL('image/png');
              pdf.addImage(
                imgData,
                'PNG',
                capture_top_left_margin,
                heightContentCapture * MM,
              );
              pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);
              heightContentCapture += el.offsetHeight;
            }
          }
        }
      }

      if (elsShape) {
        pdf.addPage();
        for (const el of elsShape) {
          if (el) {
            if (
              heightContentShape +
                el.offsetHeight +
                capture_top_left_margin / MM >=
              Capture_PDF_Height
            ) {
              const canvasElLast = await html2canvas(el, {
                allowTaint: false,
                useCORS: true,
                logging: true,
              });
              const imgDataElLast = canvasElLast.toDataURL('image/png');
              pdf.addPage();
              pdf.text('', 0, capture_top_left_margin);
              pdf.addImage(
                imgDataElLast,
                'PNG',
                capture_top_left_margin,
                capture_top_left_margin,
              );
              pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);

              if (el !== elsShape[elsShape.length - 1]) {
                heightContentShape =
                  el.offsetHeight + capture_top_left_margin / MM;

                const canvas = await html2canvas(el, {
                  allowTaint: false,
                  useCORS: true,
                  logging: true,
                });
                const imgData = canvas.toDataURL('image/png');
                pdf.addImage(
                  imgData,
                  'PNG',
                  capture_top_left_margin,
                  heightContentShape * MM,
                );
                pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);
              }
            } else {
              const canvas = await html2canvas(el, {
                allowTaint: false,
                useCORS: true,
                logging: true,
              });
              const imgData = canvas.toDataURL('image/png');
              pdf.addImage(
                imgData,
                'PNG',
                capture_top_left_margin,
                heightContentShape * MM,
              );
              pdf.text(`${pdf.internal.getNumberOfPages()}`, 100, 290);
              heightContentShape += el.offsetHeight;
            }
          }
        }
      }
      pdf.save(`Opportunity-${opportunity?.id}.pdf`);
    }
  };

  return (
    <>
      <Dialog
        open={open}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className={classes.actionDialog}
      >
        <DialogContent>
          <div id="opportunity">
            <ViewDetailOpp />
          </div>
        </DialogContent>
        <DialogActions>
          <Box
            mt={1}
            mb={1}
            display="flex"
            justifyContent="flex-end"
            alignItems="flex-end"
          >
            <Button color="default" variant="outlined" onClick={close}>
              Cancel
            </Button>
            <Box ml={2} mr={4}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => getPDF()}
              >
                <PictureAsPdfOutlined /> <b>Export to PDF</b>
              </Button>
            </Box>
          </Box>
        </DialogActions>
      </Dialog>
    </>
  );
};
