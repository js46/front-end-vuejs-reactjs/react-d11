import React from 'react';
import { MenuItem, Select, SelectProps } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

interface CustomSelectProps extends SelectProps {
  options: {
    id: number | string;
    name: string;
    style?: React.CSSProperties;
    duration?: number;
  }[];
  enabledAllOption?: boolean;
  multiple?: boolean;
  showLabelForOption?: boolean;
}
export const CustomSelect: React.FC<CustomSelectProps> = props => {
  const {
    options,
    multiple,
    showLabelForOption,
    enabledAllOption,
    ...rest
  } = props;
  return (
    <Select
      multiple={multiple ? true : false}
      variant="outlined"
      MenuProps={{
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'top',
          horizontal: 'left',
        },
        getContentAnchorEl: null,
      }}
      IconComponent={ExpandMoreIcon}
      {...rest}
    >
      {enabledAllOption && <MenuItem value={'all'}>All</MenuItem>}
      {options.map(item => {
        return (
          <MenuItem key={item.id} value={item.id} style={item.style}>
            {showLabelForOption
              ? `${item.name} - ${item.duration} Days`
              : item.name}
          </MenuItem>
        );
      })}
    </Select>
  );
};
