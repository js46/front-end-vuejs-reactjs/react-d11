import { Box } from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';

export const ESNoResult = (props: { msgKey?: string }) => {
  const { t } = useTranslation();
  return (
    <Box display="flex" justifyContent="center" mt={2} mb={2}>
      {t(props.msgKey || 'no_data')}
    </Box>
  );
};
