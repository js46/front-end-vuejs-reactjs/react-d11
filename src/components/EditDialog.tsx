import React from 'react';
import { Dialog, Box, DialogActions, Button } from '@material-ui/core';

interface EditDialogProps {
  isOpen: boolean;
  message: string | JSX.Element;
  handleClose: () => void;
  handleSubmit: () => void;
  // disable: boolean;
}
export const EditDialog: React.FC<EditDialogProps> = ({
  isOpen,
  message,
  handleClose,
  handleSubmit,
  // disable,
}) => {
  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <Box padding={3} minWidth="578px">
        <Box mb={2}>{message}</Box>
        <DialogActions>
          <Button onClick={handleClose} color="default" variant="outlined">
            Cancel
          </Button>
          <Button
            onClick={handleSubmit}
            color="primary"
            variant="contained"
            // disabled={disable}
          >
            Update
          </Button>
        </DialogActions>
      </Box>
    </Dialog>
  );
};
