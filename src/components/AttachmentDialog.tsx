import React, { useRef, useState, useContext, useEffect } from 'react';
import {
  Box,
  Typography,
  TextField,
  Button,
  DialogActions,
  Dialog,
  Grid,
  Input,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import {
  IAttachments,
  addAttachments,
  updateAttachment,
  // deleteAttachments,
  AttachmentResponse,
} from '../services/opportunity.services';
import {
  ResponsePlayBookT,
  updateFileAttachment,
  getAllPlaybookOfOpportunity,
} from '../services/playbook.services';
import {
  AnalyseStateContext,
  AnalyseDispatchContext,
  updatePlaybookList,
} from '../pages/Opportunity/components/AnalysisTab/AnalyseContext';
import { useDisableMultipleClick } from '../hooks';
import { Message } from '../constants';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { notifyError, notifySuccess } from '../store/common/actions';

const useStyles = makeStyles(theme => ({
  noteWrapper: {
    '& .tox .tox-statusbar': {
      display: 'none',
    },
  },
  fileName: {
    // maxWidth: '240px',
    display: 'inline-block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    // marginBottom: -6,
  },
  attachmentDescription: {
    '& input': {
      height: '100%',
    },
  },
  dialog: {
    '& .MuiPaper-root': {
      height: '330px',
    },
  },
}));

interface AttachmentDialogProps {
  isAddingDialog: boolean;
  isOpen: boolean;
  data: ResponsePlayBookT;
  handleClose: () => void;
  attachmentsProp: any;
  index?: number;
  entity_type: string;
  isInPlaybook?: boolean;
  attachmentFormulate?: AttachmentResponse;
}

export const AttachmentDialog: React.FC<AttachmentDialogProps> = ({
  isAddingDialog,
  isOpen,
  data,
  handleClose,
  index,
  attachmentsProp,
  entity_type,
  isInPlaybook,
  attachmentFormulate,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const attachFileRef = useRef<HTMLInputElement[]>([]);
  const { t } = useTranslation();

  const [uploadedAttachmentId, setUploadedAttachmentId] = useState<number[]>(
    [],
  );
  const [attachments, setAttachments] = useState<IAttachments[]>(
    attachmentsProp &&
      data &&
      data.attachments &&
      data.attachments.length > 0 &&
      index != null
      ? [
          {
            id: data.attachments[index]?.id,
            attachments: data.attachments[index]?.client_file_name,
            description: data.attachments[index]?.description,
          },
        ]
      : [
          {
            id: 0,
            attachments: '',
            description: '',
          },
        ],
  );
  useEffect(() => {
    if (data && data.attachments && index != null) {
      setAttachments([
        {
          id: data.attachments[index]?.id,
          attachments: data.attachments[index]?.client_file_name,
          description: data.attachments[index]?.description,
        },
      ]);
    }
    // eslint-disable-next-line
  }, [data]);

  const analyseState = useContext(AnalyseStateContext);
  const analyseDispatch = useContext(AnalyseDispatchContext);

  const handleUpdatePlaybookOfOpportunity = async () => {
    const response = await getAllPlaybookOfOpportunity(
      analyseState?.opportunity?.id,
    );
    if (response && response.success) {
      analyseDispatch(updatePlaybookList(response.data));
    }
  };
  const handleClickButtonFile = (index: number) => () => {
    if (attachFileRef && attachFileRef.current) {
      attachFileRef.current[index].click();
    }
  };

  const handleChangeFile = async (index: number, event: any) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].attachments = event.target.files[0];
    setAttachments(updatedAttachments);
  };

  const handleUploadFile = async (index: number) => {
    if (isSubmitting) return;
    await debounceFn();
    const formData = new FormData();
    formData.append('attachment', attachments[index].attachments);
    formData.append('entity_id', data.id.toString());
    formData.append('description', attachments[index].description);
    formData.append('entity_type', entity_type);
    const response = await addAttachments(formData);
    if (response.success) {
      dispatch(
        notifySuccess({
          message: t('attachment_notification.successful_upload'),
        }),
      );
      setTimeout(() => {
        setAttachments([
          {
            id: 0,
            attachments: '',
            description: '',
          },
        ]);
        handleClose();
      }, 800);
      setUploadedAttachmentId(uploadedAttachmentId.concat(response.data.id));
    } else {
      dispatch(
        notifyError({
          message: Message.Error,
        }),
      );
    }
    endRequest();
  };

  const handleUpdateFileFromEdit = async () => {
    if (isSubmitting) return;
    if (
      data &&
      data.attachments &&
      typeof attachments[0].attachments === 'string'
    ) {
      const response = await updateAttachment(attachments[0].id, {
        description: attachments[0].description,
      });
      if (response.success) {
        dispatch(
          notifySuccess({
            message: t('attachment_notification.successful_update'),
          }),
        );
        setTimeout(() => {
          setAttachments([
            {
              id: 0,
              attachments: '',
              description: '',
            },
          ]);
          handleClose();
        }, 800);
      } else {
        dispatch(
          notifyError({
            message: Message.Error,
          }),
        );
      }
    }

    if (
      data &&
      data.attachments &&
      index != null &&
      typeof attachments[0].attachments !== 'string'
    ) {
      await debounceFn();
      const formData = new FormData();
      formData.append('attachment', attachments[0].attachments);
      formData.append('entity_id', data.id.toString());
      formData.append('description', attachments[0].description);
      formData.append('entity_type', entity_type);
      const response = await updateFileAttachment(
        formData,
        data?.attachments[index].id,
      );
      if (response.success) {
        dispatch(
          notifySuccess({
            message: t('attachment_notification.successful_update'),
          }),
        );
        setAttachments([
          {
            id: response.data.id,
            attachments: response.data.client_file_name,
            description: response.data.description,
          },
        ]);
        if (entity_type === 'opportunity_playbook_item' && isInPlaybook) {
          handleUpdatePlaybookOfOpportunity();
        }
        setTimeout(() => {
          setAttachments([
            {
              id: 0,
              attachments: '',
              description: '',
            },
          ]);
          handleClose();
        }, 800);
      }
    }
  };

  const handleUpdateFile = async (event: any) => {
    if (data && data.attachments && data.attachments.length > 0) {
      const updatedAttachments = [...attachments];
      updatedAttachments[0].attachments = event.target.files[0];

      setAttachments(updatedAttachments);
    }
  };

  const handleChangeDescription = (index: number, value: string) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].description = value;
    setAttachments(updatedAttachments);
  };
  return (
    <React.Fragment>
      <Dialog
        open={isOpen}
        onClose={handleClose}
        fullWidth
        maxWidth="sm"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Box padding={3}>
          <Box mb={3}>
            <Box textAlign="center" mb={3}>
              <Typography component="p" variant="h4">
                {isAddingDialog ? t('add_attachment') : t('edit_attachment')}
              </Typography>
            </Box>
          </Box>
          <Box>
            <Grid container>
              {attachments &&
                attachments.length > 0 &&
                attachments.map((item, index) => (
                  <Box key={index} width="100%" mb={1.5}>
                    {isAddingDialog && (
                      <Box display="flex" mb={2}>
                        <Button
                          variant="contained"
                          startIcon={<AttachFileIcon />}
                          onClick={handleClickButtonFile(index)}
                        >
                          Browse file
                        </Button>
                      </Box>
                    )}
                    {isAddingDialog && (
                      <Input
                        type="file"
                        inputRef={el => (attachFileRef.current[index] = el)}
                        onChange={event => handleChangeFile(index, event)}
                        style={{ display: 'none' }}
                      />
                    )}
                    {!isAddingDialog && (
                      <Box display="flex" mb={2}>
                        <Button
                          variant="contained"
                          startIcon={<AttachFileIcon />}
                          onClick={handleClickButtonFile(index)}
                        >
                          Change file
                        </Button>
                      </Box>
                    )}
                    {!isAddingDialog && (
                      <Input
                        type="file"
                        inputRef={el => (attachFileRef.current[index] = el)}
                        onChange={handleUpdateFile}
                        style={{ display: 'none' }}
                      />
                    )}

                    {item.attachments && (
                      <Grid container>
                        <Box mb={1}>
                          <Typography component="p" variant="caption">
                            {t('file_name')}:
                          </Typography>
                        </Box>
                        <Grid item xs={12}>
                          <Box width="100%" mb={2}>
                            {item?.attachments && (
                              <Box display="flex">
                                {isAddingDialog &&
                                  item &&
                                  item.attachments &&
                                  item.attachments.name && (
                                    <Typography
                                      component="span"
                                      className={classes.fileName}
                                    >
                                      {item.attachments.name}
                                    </Typography>
                                  )}
                                {!isAddingDialog && (
                                  <Typography
                                    component="span"
                                    className={classes.fileName}
                                  >
                                    {item.attachments?.name || item.attachments}
                                  </Typography>
                                )}
                              </Box>
                            )}
                          </Box>
                        </Grid>
                        <Box mb={1}>
                          <Typography component="p" variant="caption">
                            {t('description')}:
                          </Typography>
                        </Box>
                        <Grid item xs={12}>
                          {item && (
                            <Grid container>
                              <Grid item xs={10}>
                                <TextField
                                  fullWidth
                                  variant="outlined"
                                  name={`attachment_${item.id}`}
                                  value={item.description}
                                  onChange={event =>
                                    handleChangeDescription(
                                      index,
                                      event.target.value,
                                    )
                                  }
                                  className={classes.attachmentDescription}
                                />
                              </Grid>
                            </Grid>
                          )}
                        </Grid>
                      </Grid>
                    )}
                  </Box>
                ))}
            </Grid>
          </Box>

          <DialogActions>
            <Box display="flex">
              <Box mr={1}>
                <Button
                  onClick={() => {
                    setAttachments([
                      {
                        id: 0,
                        attachments: '',
                        description: '',
                      },
                    ]);

                    handleClose();
                  }}
                  variant="outlined"
                >
                  Close
                </Button>
              </Box>
              {isAddingDialog ? (
                <Button
                  onClick={() => handleUploadFile(0)}
                  variant="contained"
                  color="primary"
                  startIcon={<CloudUploadIcon />}
                  disabled={isSubmitting || !attachments[0].attachments}
                >
                  Upload
                </Button>
              ) : (
                <Button
                  onClick={handleUpdateFileFromEdit}
                  variant="contained"
                  color="primary"
                  startIcon={<CloudUploadIcon />}
                  disabled={isSubmitting}
                >
                  Update
                </Button>
              )}
            </Box>
          </DialogActions>
        </Box>
      </Dialog>
    </React.Fragment>
  );
};
