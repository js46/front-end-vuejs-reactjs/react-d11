import React from 'react';
import { Avatar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Admin, User100 } from '../assets';
const useStyles = makeStyles(theme => ({
  avatar: {
    color: theme.palette.getContrastText(theme.palette.grey[500]),
    backgroundColor: theme.palette.grey[500],
    width: 105,
    height: 105,
    fontSize: 24,
    margin: 0,
  },
}));
interface UserInfo {
  id: number;
  name?: string;
}

export const UserAvatar: React.FC<UserInfo> = ({ id }) => {
  const classes = useStyles();
  if (id === 2) {
    return <Avatar className={classes.avatar} src={Admin}></Avatar>;
  } else {
    return <Avatar className={classes.avatar} src={User100}></Avatar>;
  }
};
