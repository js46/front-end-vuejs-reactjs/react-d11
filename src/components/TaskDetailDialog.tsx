import React, { useRef, useState } from 'react';
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  Divider,
  Grid,
  IconButton,
  TextField,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import LinkIcon from '@material-ui/icons/Link';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import { StateLabels, TaskState } from '../constants';
import { CommentReactiveList } from './index';
import { customQueryComment, generateOpportunityLink } from '../utils';
import { IOpportunityTask } from '../services/task.services';
import { addTaskComment } from '../services/comment.services';
import { useTranslation } from 'react-i18next';

interface TaskDetailDialogProps {
  open: boolean;
  toggle: () => void;
  task?: IOpportunityTask;
  hideOpportunityLink?: boolean;
}
export const TaskDetailDialog: React.FC<TaskDetailDialogProps> = ({
  open,
  toggle,
  task,
  hideOpportunityLink,
}) => {
  let history = useHistory();
  const [showCommentBtn, setCommentBtn] = useState(false);
  const [comment, setComment] = useState('');
  const [lastRefresh, setLastRefresh] = useState(0);
  let messagesEndRef = useRef<HTMLDivElement>(null);

  const navigateToOpportunity = (
    id: number,
    phase?: string,
    state?: string,
  ) => () => {
    const pathname = generateOpportunityLink(id, phase, state);
    history.push(pathname);
  };

  const toggleCommentBtn = (show: boolean) => () => {
    setCommentBtn(show);
    if (show) {
      // console.log(messagesEndRef);
      setTimeout(() => {
        messagesEndRef?.current?.scrollIntoView({ behavior: 'smooth' });
      }, 400);
    }
  };

  const onBlurComment = () => {
    if (!comment.trim()) toggleCommentBtn(false)();
  };

  const onSubmitComment = async () => {
    if (!comment.trim()) return;
    const json = await addTaskComment({
      entity_type: 'task',
      entity_id: task?.id,
      comment_type: 'DEFAULT',
      comment_body: comment,
      comment_parent_id: 0,
    });
    if (json.success) {
      setComment('');
      setCommentBtn(false);
      setTimeout(() => setLastRefresh(Date.now()), 1500);
    }
  };

  const queryComment = () => {
    if (!task?.id) return;
    return customQueryComment(
      task.id,
      {
        entity_type: 'task',
        comment_type: 'default',
      },
      lastRefresh,
    );
  };

  const { t } = useTranslation();

  return (
    <Dialog
      open={open}
      onClose={toggle}
      maxWidth="md"
      fullWidth
      aria-labelledby="task-dialog"
    >
      {!task && (
        <DialogContent>
          <Typography paragraph>{t('no_task_found')}</Typography>
        </DialogContent>
      )}
      {task && (
        <>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            mt={2}
            mb={1}
            ml={3}
            mr={1}
          >
            <Typography variant="h2">{task.title || task.task_type}</Typography>
            <IconButton onClick={toggle} aria-label="close">
              <CloseIcon />
            </IconButton>
          </Box>
          <Divider />
          <DialogContent>
            <Box mt={2} mb={2}>
              <Grid container spacing={5}>
                <Grid item xs sm={8}>
                  <Typography variant="h6">Description</Typography>
                  <Typography paragraph gutterBottom>
                    {task.description || 'None'}
                  </Typography>
                  <Typography variant="h6">{t('comments')}</Typography>
                  <Box mt={2} mb={2}>
                    <CommentReactiveList defaultQuery={queryComment} />
                  </Box>
                  <TextField
                    multiline
                    placeholder="Add a comment..."
                    rows={3}
                    fullWidth
                    variant="outlined"
                    onFocus={toggleCommentBtn(true)}
                    onBlur={onBlurComment}
                    onChange={event => setComment(event.target.value)}
                    value={comment}
                  />
                  {showCommentBtn && (
                    <Box pb={2} pt={2} display="flex" flexDirection="row">
                      <Button
                        variant="contained"
                        color="primary"
                        size="small"
                        onClick={onSubmitComment}
                      >
                        Save
                      </Button>
                      <Box ml={1}>
                        <Button variant="outlined" color="default" size="small">
                          Cancel
                        </Button>
                      </Box>
                    </Box>
                  )}
                  <div ref={messagesEndRef} />
                </Grid>
                <Grid item xs sm={4}>
                  <Typography variant="h6">Task type</Typography>
                  <Typography paragraph>{task.task_type}</Typography>
                  <Typography variant="h6">Task state</Typography>
                  <Typography paragraph>
                    {task.task_state
                      ? StateLabels[task.task_state as TaskState]
                      : 'N/A'}
                  </Typography>
                  <Typography variant="h6">Assignee</Typography>
                  <Typography paragraph>{task.assignee_user?.name}</Typography>
                  <Typography variant="h6">Created by</Typography>
                  <Typography paragraph>
                    {task.created_by_user?.name}
                  </Typography>
                  <Typography variant="h6">Assigned at</Typography>
                  <Typography paragraph>
                    {moment(task.created_at).format('DD/MM/YYYY, h:mma')}
                  </Typography>
                  <Typography variant="h6">Due date</Typography>
                  <Typography paragraph>
                    {task?.requested_completion_date?.toString() !==
                      '0001-01-01T00:00:00Z' &&
                      moment(task.requested_completion_date).format(
                        'DD/MM/YYYY',
                      )}
                  </Typography>
                  <Typography variant="h6">Updated at</Typography>
                  <Typography paragraph>
                    {moment(task.last_updated_at).format('DD/MM/YYYY, h:mma')}
                  </Typography>
                  <Box mb={2}>
                    <Divider />
                  </Box>
                  <Typography variant="h6">
                    Opportunity #{task.opportunity?.id}
                  </Typography>
                  <Typography paragraph>{task.opportunity?.title}</Typography>
                  <Typography variant="h6">Phase</Typography>
                  <Box display="flex" mb={2}>
                    <Box
                      padding="3px 10px"
                      bgcolor={'#45C3EC'}
                      borderRadius="6px"
                      color="#fff"
                      mt={0.4}
                    >
                      {task.opportunity?.phase || 'empty'}
                    </Box>
                  </Box>
                  {!hideOpportunityLink && task.opportunity && (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={navigateToOpportunity(
                        task.opportunity.id,
                        task.opportunity.phase,
                        task.opportunity.state,
                      )}
                      startIcon={<LinkIcon />}
                      size="small"
                    >
                      {t('view_opp')}
                    </Button>
                  )}
                </Grid>
              </Grid>
            </Box>
          </DialogContent>
        </>
      )}
    </Dialog>
  );
};
