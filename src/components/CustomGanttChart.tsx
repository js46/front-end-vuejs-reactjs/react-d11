import * as React from 'react';
import { Box } from '@material-ui/core';
import { CustomGanttChartItem } from './CustomGanttChartItem';
import moment from 'moment';
import { isWeekend } from '../utils';

export type CustomGanttChartDataT = {
  title?: string;
  activeDates: string[];
  activeColor?: string;
  customTooltip?: { label: string; value: string | number }[];
  startDate?: Date;
};
interface CustomGanttChartProps {
  data: CustomGanttChartDataT[];
  from?: Date;
  duration?: number;
  maxWidth?: number | string;
  holidays?: Map<string, string>;
}
export const CustomGanttChart: React.FC<CustomGanttChartProps> = ({
  data,
  maxWidth,
  from,
  duration,
  holidays,
}) => {
  from = from || new Date();
  duration = duration || 60;
  return (
    <Box
      paddingTop={1}
      style={{
        maxWidth: maxWidth ?? '100%',
        overflowX: 'scroll',
      }}
    >
      {data.map((item, index) => {
        let chartData: { name: string; disabled?: boolean }[] = [];
        for (let i = 0; i < (duration as number); i++) {
          let date = moment(item.startDate ? item.startDate : from).add(
            i,
            'day',
          );
          let name = date.format('DD/MM');
          chartData.push({
            name,
            disabled: isWeekend(date) || holidays?.has(name),
          });
        }
        return (
          <Box
            key={index}
            // pt={2}
            // pb={2}
            position="relative"
            zIndex={99 - index}
          >
            <CustomGanttChartItem
              {...item}
              data={chartData}
              from={from as Date}
            />
          </Box>
        );
      })}
    </Box>
  );
};
