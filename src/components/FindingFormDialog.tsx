import React, { useState } from 'react';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  TextField,
  Typography,
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { MixTitle } from './MixTitle';
import { Formik } from 'formik';
import { useDisableMultipleClick } from '../hooks';
import {
  addPlaybookItemFinding,
  updatePlaybookItemFinding,
} from '../services/playbook.services';
import { addOptionItemFinding } from '../services/recommendation.services';
import { object, string } from 'yup';
import { useTranslation } from 'react-i18next';
import { TinyMCEForm } from './form/TinyMCEForm';

interface FindingFormDialogProps {
  itemID: number;
  id?: number;
  open: boolean;
  title: string;
  description: string;
  onClose: () => void;
  disabled?: boolean;
  onSuccess?: () => void;
  entity_type?: string;
  getOptionFindings?: (option_id: number) => Promise<void>;
}

export const FindingFormDialog: React.FC<FindingFormDialogProps> = props => {
  const {
    open,
    onClose,
    title,
    description,
    id,
    disabled,
    itemID,
    onSuccess,
    entity_type,
    getOptionFindings,
  } = props;
  const { t } = useTranslation();
  const [loadingEditor, setLoadingEditor] = useState(true);
  const classes = useStyles();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  return (
    <Dialog
      open={open}
      onClose={onClose}
      fullWidth
      maxWidth="md"
      aria-labelledby="add-play-book-item-finding"
      aria-describedby="alert-dialog-description"
    >
      <Formik
        initialValues={{ title, description, id }}
        validationSchema={object().shape({
          title: string()
            .trim()
            .required(),
        })}
        enableReinitialize
        onSubmit={async values => {
          // console.log(values);
          if (isSubmitting) return;
          await debounceFn();
          try {
            if (values.id) {
              await updatePlaybookItemFinding(
                values.id,
                values.title,
                values.description,
              );
              getOptionFindings?.(itemID);
            } else {
              if (entity_type === 'opportunity_recommend') {
                const response = await addOptionItemFinding(
                  itemID,
                  entity_type,
                  values.title,
                  values.description,
                );
                if (response.success) {
                  getOptionFindings?.(itemID);
                }
              } else {
                await addPlaybookItemFinding(
                  itemID,
                  'opportunity_playbook_item',
                  values.title,
                  values.description,
                );
              }
            }
            onSuccess?.();
          } catch (e) {
            console.log('Error submitting finding', e);
          }
          endRequest();
        }}
      >
        {({
          values,
          handleChange,
          setFieldValue,
          handleSubmit,
          handleBlur,
          errors,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box padding={5}>
              <Box
                width="880px"
                height="550px"
                style={
                  loadingEditor ? { display: 'flex' } : { display: 'none' }
                }
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <CircularProgress />
              </Box>
              <Box
                mb={3}
                style={loadingEditor ? { display: 'none' } : undefined}
              >
                <Box textAlign="center" mb={3}>
                  <Typography component="p" variant="h4">
                    {!id ? t('finding.add') : t('finding.edit')}
                  </Typography>
                </Box>
                <Box mb={2}>
                  <MixTitle title="Title" isRequired />
                  <TextField
                    value={values.title}
                    name="title"
                    fullWidth
                    onChange={handleChange}
                    onBlur={handleBlur}
                    variant="outlined"
                    required
                    disabled={isSubmitting}
                    error={errors && !!errors.title}
                  />
                </Box>
                <Box className={classes.noteWrapper}>
                  <MixTitle title="Description" />
                  <Box minHeight="400px" minWidth="90%">
                    <TinyMCEForm
                      value={values.description}
                      disabled={false}
                      setLoadingEditor={setLoadingEditor}
                      setValue={(content: string) => {
                        setFieldValue('description', content);
                      }}
                    />
                  </Box>
                </Box>
              </Box>
              <DialogActions>
                <Button onClick={onClose} variant="outlined">
                  Cancel
                </Button>
                <Button
                  color="primary"
                  variant="contained"
                  disabled={disabled || loadingEditor || isSubmitting}
                  type="submit"
                >
                  {!id ? 'Add' : 'Update'}
                </Button>
              </DialogActions>
            </Box>
          </form>
        )}
      </Formik>
    </Dialog>
  );
};
const useStyles = makeStyles(theme => ({
  noteWrapper: {
    '& .tox .tox-statusbar': {
      display: 'none',
    },
  },
}));
