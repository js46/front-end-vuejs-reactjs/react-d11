import React from 'react';
import { Chip } from '@material-ui/core';
import { TaskState, StateLabels } from '../constants/task';
import { makeStyles } from '@material-ui/core/styles';

interface ITaskStateChipProps {
  taskState: string;
}
const renderBgColorChip = (taskState: string): string => {
  if (taskState === TaskState.Assigned) return '#4dbef1';
  else if (taskState === TaskState.InProgress) return '#fdd835';
  else if (taskState === TaskState.Completed) return '#4EE24E';
  else if (taskState === TaskState.Cancelled) return '#E1E1E1';
  else return '#ff2929';
};
const TaskStateChip: React.FC<ITaskStateChipProps> = props => {
  const { taskState } = props;
  const useStyles = makeStyles(theme => ({
    chip: {
      background: renderBgColorChip(taskState),
      marginLeft: 8,
    },
  }));
  const classes = useStyles();
  return (
    <Chip
      label={StateLabels[taskState as TaskState]}
      size="small"
      className={classes.chip}
    />
  );
};

export default TaskStateChip;
