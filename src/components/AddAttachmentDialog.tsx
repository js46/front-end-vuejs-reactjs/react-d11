import React, { useRef, useState } from 'react';
import {
  Box,
  Typography,
  TextField,
  Button,
  DialogActions,
  Dialog,
  Grid,
  Input,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { IAttachments, addAttachments } from '../services/opportunity.services';

import { useDisableMultipleClick } from '../hooks';
import { Message } from '../constants';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { notifyError, notifySuccess } from '../store/common/actions';

const useStyles = makeStyles(theme => ({
  fileName: {
    display: 'inline-block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  attachmentDescription: {
    '& input': {
      height: '100%',
    },
  },
}));

interface AddAttachmentDialogProps {
  isOpen: boolean;
  handleClose: () => void;
  entity_id: number;
  entity_type: string;
}

export const AddAttachmentDialog: React.FC<AddAttachmentDialogProps> = ({
  isOpen,
  handleClose,
  entity_id,
  entity_type,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isSubmitting, debounceFn, endRequest } = useDisableMultipleClick();
  const attachFileRef = useRef<HTMLInputElement[]>([]);
  const { t } = useTranslation();

  const [attachments, setAttachments] = useState<IAttachments[]>([
    {
      id: 0,
      attachments: '',
      description: '',
    },
  ]);

  const handleClickButtonFile = (index: number) => () => {
    if (attachFileRef && attachFileRef.current) {
      attachFileRef.current[index].click();
    }
  };

  const handleChangeFile = async (index: number, event: any) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].attachments = event.target.files[0];
    setAttachments(updatedAttachments);
  };

  const handleUploadFile = async () => {
    if (isSubmitting) return;
    await debounceFn();
    const formData = new FormData();
    formData.append('attachment', attachments[0].attachments);
    formData.append('entity_id', entity_id.toString());
    formData.append('description', attachments[0].description);
    formData.append('entity_type', entity_type);
    const response = await addAttachments(formData);
    if (response.success) {
      dispatch(
        notifySuccess({
          message: t('attachment_notification.successful_upload'),
        }),
      );
      setTimeout(() => {
        setAttachments([
          {
            id: 0,
            attachments: '',
            description: '',
          },
        ]);
        handleClose();
      }, 800);
    } else {
      dispatch(notifyError({ message: Message.Error }));
    }
    endRequest();
  };

  const handleChangeDescription = (index: number, value: string) => {
    const updatedAttachments = [...attachments];
    updatedAttachments[index].description = value;
    setAttachments(updatedAttachments);
  };

  return (
    <React.Fragment>
      <Dialog
        open={isOpen}
        onClose={handleClose}
        fullWidth
        maxWidth="sm"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Box padding={3}>
          <Box mb={3}>
            <Box textAlign="center" mb={3}>
              <Typography component="p" variant="h4">
                Add Attachment
              </Typography>
            </Box>
          </Box>
          <Box>
            <Grid container>
              {attachments &&
                attachments.length > 0 &&
                attachments.map((item, index) => (
                  <Box key={index} width="100%" mb={1.5}>
                    <Box display="flex" mb={2}>
                      <Button
                        variant="contained"
                        startIcon={<AttachFileIcon />}
                        onClick={handleClickButtonFile(index)}
                      >
                        Browse file
                      </Button>
                    </Box>

                    <Input
                      type="file"
                      inputRef={el => (attachFileRef.current[index] = el)}
                      onChange={event => handleChangeFile(index, event)}
                      style={{ display: 'none' }}
                    />
                    {item.attachments && (
                      <Grid container>
                        <Box mb={1}>
                          <Typography component="p" variant="caption">
                            File Name:
                          </Typography>
                        </Box>
                        <Grid item xs={12}>
                          <Box width="100%" mb={2}>
                            {item?.attachments && (
                              <Box display="flex">
                                {item &&
                                  item.attachments &&
                                  item.attachments.name && (
                                    <Typography
                                      component="span"
                                      className={classes.fileName}
                                    >
                                      {item.attachments.name}
                                    </Typography>
                                  )}
                              </Box>
                            )}
                          </Box>
                        </Grid>
                        <Box mb={1}>
                          <Typography component="p" variant="caption">
                            Description:
                          </Typography>
                        </Box>
                        <Grid item xs={12}>
                          {item && (
                            <Grid container>
                              <Grid item xs={10}>
                                <TextField
                                  fullWidth
                                  variant="outlined"
                                  name={`attachment_${item.id}`}
                                  value={item.description}
                                  onChange={event =>
                                    handleChangeDescription(
                                      index,
                                      event.target.value,
                                    )
                                  }
                                  className={classes.attachmentDescription}
                                />
                              </Grid>
                            </Grid>
                          )}
                        </Grid>
                      </Grid>
                    )}
                  </Box>
                ))}
            </Grid>
          </Box>

          <DialogActions>
            <Box display="flex">
              <Box mr={1}>
                <Button
                  onClick={() => {
                    setAttachments([
                      {
                        id: 0,
                        attachments: '',
                        description: '',
                      },
                    ]);
                    handleClose();
                  }}
                  variant="outlined"
                >
                  Close
                </Button>
              </Box>
              <Button
                onClick={handleUploadFile}
                variant="contained"
                color="primary"
                startIcon={<CloudUploadIcon />}
                disabled={isSubmitting || !attachments[0].attachments}
              >
                Upload
              </Button>
            </Box>
          </DialogActions>
        </Box>
      </Dialog>
    </React.Fragment>
  );
};
