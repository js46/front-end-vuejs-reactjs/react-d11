import React from 'react';
import {
  Dialog,
  Box,
  DialogActions,
  Button,
  Typography,
} from '@material-ui/core';
interface FormDialogProps {
  title: string | JSX.Element;
  formContent?: string | JSX.Element;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  submit: () => void;
  disable: boolean;
  labelBtn?: string;
  isAddingForm: boolean;
  maxWidth?: any;
  configLabel?: string;
  disableSubmitButton?: boolean;
}
export const FormDialog: React.FC<FormDialogProps> = props => {
  const {
    title,
    formContent,
    open,
    setOpen,
    submit,
    disable,
    isAddingForm,
    labelBtn,
    maxWidth,
    configLabel,
    disableSubmitButton,
  } = props;
  return (
    <Dialog
      open={open}
      onClose={() => setOpen(false)}
      aria-labelledby="form-dialog"
      maxWidth={maxWidth ? maxWidth : 'sm'}
      fullWidth
    >
      <Box padding={4}>
        <Box>
          <Box textAlign="center">
            <Typography component="p" variant="h4">
              {title}
            </Typography>
          </Box>
          <form onSubmit={submit}>
            {formContent}
            <DialogActions style={{ padding: 0, marginTop: 5 }}>
              <Button
                onClick={() => {
                  setOpen(false);
                }}
                color="default"
                variant="outlined"
              >
                Cancel
              </Button>
              {!disableSubmitButton && (
                <Button
                  type="submit"
                  color="primary"
                  variant="contained"
                  disabled={disable}
                >
                  {labelBtn ?? ''}
                  {!labelBtn &&
                    (isAddingForm
                      ? `${configLabel ? configLabel : 'Add'}`
                      : 'Update')}
                </Button>
              )}
            </DialogActions>
          </form>
        </Box>
      </Box>
    </Dialog>
  );
};
