import React from 'react';
import { Box, IconButton, MenuItem } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { StyledMenu } from './styled';
import Fade from '@material-ui/core/Fade';

export type CustomContextMenuOption = {
  name: string;
  onClick?: () => any;
  disabled?: boolean;
};
type CustomContextMenuProps = {
  options: CustomContextMenuOption[];
  size?: 'small' | 'medium';
};
export const CustomContextMenu: React.FC<CustomContextMenuProps> = ({
  options,
  size,
}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  return (
    <Box width="40px" display="flex" justifyContent="flex-end">
      <IconButton
        aria-controls="fade-menu"
        aria-haspopup="true"
        onClick={(event: any) => setAnchorEl(event.target)}
        aria-label="Action"
        size={size}
      >
        <MoreVertIcon />
      </IconButton>
      <StyledMenu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
        TransitionComponent={Fade}
      >
        {options.map((option, index) => (
          <MenuItem
            key={index}
            onClick={() => {
              setAnchorEl(null);
              option.onClick?.();
            }}
            disabled={option.disabled}
          >
            {option.name}
          </MenuItem>
        ))}
      </StyledMenu>
    </Box>
  );
};
