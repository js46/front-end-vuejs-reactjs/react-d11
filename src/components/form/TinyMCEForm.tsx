import React from 'react';
import { Editor } from '@tinymce/tinymce-react/lib/cjs/main/ts';
import { useSelector } from 'react-redux';
import { ConfigPrivate } from '../../store/config/selector';
import { uploadPhoto } from '../../services/photo.services';

interface TinyMCEFormPros {
  value: string;
  disabled: boolean;
  setLoadingEditor?: (value: boolean) => void;
  setValue: (value: string) => void;
  height?: number;
}
export const TinyMCEForm: React.FC<TinyMCEFormPros> = ({
  value,
  disabled,
  setLoadingEditor,
  setValue,
  height,
}) => {
  const Key = useSelector(ConfigPrivate).tinymce_api_key;
  const uploadImage = (blobInfo: any, success: Function, failure: any) => {
    const formData = new FormData();
    formData.append('photo', blobInfo.blob(), blobInfo.filename());
    uploadPhoto(formData)
      .then(res => {
        if (res.success) {
          success(res.data.url);
        }
      })
      .catch(err => console.log('err', err));
  };
  return (
    <Editor
      apiKey={Key}
      init={{
        height: height ? height : 400,
        width: '100%',
        branding: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code image help wordcount',
        ],
        toolbar:
          //eslint-disable-next-line
          'undo redo | formatselect | bold italic backcolor | \
                        alignleft aligncenter alignright alignjustify | \
                        bullist numlist outdent indent | removeformat | image | help',
        default_link_target: '_blank',
        images_upload_handler: uploadImage,
        init_instance_callback: function() {
          setLoadingEditor?.(false);
        },
      }}
      value={value}
      onEditorChange={(content: string) => setValue(content)}
      disabled={disabled}
    />
  );
};
