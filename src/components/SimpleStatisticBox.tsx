import React from 'react';
import { Box, Typography } from '@material-ui/core';

export const SimpleStatisticBox: React.FC<{
  label: string;
  value: number | string;
}> = ({ label, value }) => {
  return (
    <Box display="flex" alignItems="center" flexDirection="column">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        height={120}
        width={120}
        border={'1px solid grey'}
        borderRadius={'50%'}
      >
        <Typography variant="h1">{value}</Typography>
      </Box>
      <Typography variant="body1">{label}</Typography>
    </Box>
  );
};
