import React from 'react';
import { first } from 'lodash';

export const ReactiveError: React.FC<{ error: any }> = ({ error }) => {
  if (!error) return null;
  return (
    <div>
      Something went wrong!
      <br />
      <code>
        Error details: {first(error.responses as any[])?.error?.reason}
      </code>
    </div>
  );
};
